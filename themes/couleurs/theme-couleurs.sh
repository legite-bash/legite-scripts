echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.19
# Description: 


# Surcharge apps/themes/defaut/defaut-vars.sh
RESET_COLOR="$(tput sgr0)";
BOLD="$(tput smso)";
NOBOLD="$(tput rmso)";

BLACK="$(tput setaf 0)";
RED="$(tput setaf 1)";
GREEN="$(tput setaf 2)";
YELLOW="$(tput setaf 3)";
CYAN="$(tput setaf 4)";
MAGENTA="$(tput setaf 5)";
BLUE="$(tput setaf 6)";
WHITE="$(tput setaf 7)";

COLOR_NORMAL="$WHITE";
COLOR_INFO="$BLUE";
COLOR_CMD="$YELLOW";
COLOR_WARN="$RED";
COLOR_OK="$GREEN";
COLOR_TITRE1="$GREEN";
COLOR_TITRE2="$MAGENTA";
COLOR_TITRE3="$CYAN";
COLOR_TITRE4="$CYAN";
COLOR_DEBUG="$MAGENTA";
COLOR_FUNCTION="$CYAN";
COLOR_TRAP="$YELLOW";

