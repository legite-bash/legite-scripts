#echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


declare -g RESET_COLOR='';
declare -g BOLD='';
declare -g NOBOLD='';

declare -g BLACK='';
declare -g RED='';
declare -g GREEN='';
declare -g YELLOW='';
declare -g CYAN='';
declare -g MAGENTA='';
declare -g BLUE='';
declare -g WHITE='';

#  BLACK="\[\033[01;30m\]"
#    RED="\[\033[01;31m\]"
#  GREEN="\[\033[01;32m\]"
# YELLOW="\[\033[01;33m\]"
#   CYAN="\[\033[01;34m\]"
#MAGENTA="\[\033[01;35m\]"
#   BLUE="\[\033[01;36m\]"
#  WHITE="\[\033[01;37m\]"

declare -g COLOR_NORMAL="$WHITE";
declare -g COLOR_INFO="$BLUE";
declare -g COLOR_CMD="$YELLOW";
declare -g COLOR_WARN="$RED";
declare -g COLOR_OK="$GREEN";
declare -g COLOR_TITRE1="$GREEN";
declare -g COLOR_TITRE2="$MAGENTA";
declare -g COLOR_TITRE3="$CYAN";
declare -g COLOR_TITRE4="$CYAN";
declare -g COLOR_DEBUG="$MAGENTA";
declare -g COLOR_FUNCTION="$CYAN";
declare -g COLOR_TRAP="$YELLOW";



##########
# TITRES #
##########

    function TITRE0() { titre0 "${1^^}"; }
    function TITRE0-D() {
        local -i _debug_gtt_level=${2:-$DEBUG_GTT_LEVEL_DEFAUT};
        #display-vars '_debug_gtt_level' "$_debug_gtt_level";
        if [ $debug_gtt_level -eq 0 ];then return 0;fi
        if [ $debug_gtt_level -ge $_debug_gtt_level ]
        then
            TITRE0 "${1^^}";
        fi
    }

    function titre0() {
        if [ $# -eq 0 ];then return 0;fi
        local -i _texte_carNb=$(( ${#1} + 4 ))
        if [ $_texte_carNb -gt $terminal_largeur ]; then _texte_carNb=$terminal_largeur; fi
        local C='#'
        echo '';
        texte-centre "$1" $(($terminal_largeur -4));
        car-duplique-max_term $terminal_largeur "$C";
        echo -e "$C${COLOR_TITRE1} $texte_centre ${COLOR_NORMAL}$C";
        car-duplique-max_term $terminal_largeur "$C";
        return 0;
    }

    function titre0-D() {
        local    _txt="$1";
        local -i _debug_gtt_level=${2:-$DEBUG_GTT_LEVEL_DEFAUT};
        if [ $debug_gtt_level -eq 0 ];then return 0;fi
        if [ $debug_gtt_level -ge $_debug_gtt_level ]
        then
            titre0 "${_txt}";
        fi
    }



    function TITRE1() { titre1 "${1^^}"; }
    function TITRE1_D() {
        local -i _debug_gtt_level=${2:-$DEBUG_GTT_LEVEL_DEFAUT};
        #display-vars '_debug_gtt_level' "$_debug_gtt_level";
        if [ $debug_gtt_level -eq 0 ];then return 0;fi
        if [ $debug_gtt_level -ge $_debug_gtt_level ]
        then
            titre1 "${1^^}";
        fi
        }

    function titre1() {
        if [ $# -eq 0 ];then return 0;fi
        local -i _fct_pile_app_level=5;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local    _txt="$1";
        local -i _texte_carNb=$(( ${#1} ))
        local -i _sep_carNb=$(( ${terminal_largeur} /2  ))
        if [ $_sep_carNb -gt $terminal_largeur ]; then _sep_carNb=$terminal_largeur; fi
        texte-centre "$_txt" $((_sep_carNb - 4));

        local C='#'
        echo '';
        car-duplique-max_term $_sep_carNb "$C";
        echo -e "$C${COLOR_TITRE1} $texte_centre ${COLOR_NORMAL}$C";
        car-duplique-max_term $_sep_carNb "$C";

        fct-pile-app-out 0; return 0;
    }

    #function titre1_D( '_txt' [$_debug_gtt_level] )
    function titre1_D(){
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE;fi

        local    _txt="$1";
        local -i _debug_gtt_level=${2:-$DEBUG_GTT_LEVEL_DEFAUT};
        #display-vars '_debug_gtt_level' "$_debug_gtt_level";
        if [ $debug_gtt_level -eq 0 ];then return 0;fi
        if [ $debug_gtt_level -ge $_debug_gtt_level ]
        then
            titre1 "$_txt";
        fi
        return 0;
    }

    #function titre1_d( '_txt' [$_debug_level] )
    function titre1_d(){
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE;fi

        local    _txt="$1";
        local -i _debug_level=${2:-$DEBUG_LEVEL_DEFAUT};
        #display-vars '_debug_level' "$_debug_level";
        if [ $debug_app_level -eq 0 ];then return 0;fi
        if [ $debug_app_level -ge $_debug_level ]
        then
            titre1 "$_txt";
        fi
        return 0;
    }


    function TITRE2() { titre2 "${1^^}"; }
    function titre2() {
        if [ $# -eq 0 ];then return 0;fi
        local -i _carNb=$(( ${#1} + 0 ));
        if [ $_carNb -gt $terminal_largeur ]; then _carNb=$terminal_largeur; fi
        local C='=';
        local _sep=$(car-duplique-max_term $_carNb "$C");
        echo '';
        echo "  $_sep";
        echo -e "  $COLOR_TITRE2$1$COLOR_NORMAL";
        echo "  $_sep";
        return 0;
    }

    #function titre2_D( '_txt' [$_debug_gtt_level] )
    function titre2_D(){
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE;fi

        local    _txt="$1";
        local -i _debug_gtt_level=${2:-$DEBUG_GTT_LEVEL_DEFAUT};
        #display-vars '_debug_gtt_level' "$_debug_gtt_level";
        if [ $debug_gtt_level -eq 0 ];then return 0;fi
        if [ $debug_gtt_level -ge $_debug_gtt_level ]
        then
            titre2 "$_txt"
        fi
        return 0;
    }

    #function titre2_d( '_txt' [$_debug_level] )
    function titre2_d(){
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE;fi

        local    _txt="$1";
        local -i _debug_level=${2:-$DEBUG_LEVEL_DEFAUT};
        #display-vars '_debug_level' "$_debug_level";
        if [ $debug_app_level -eq 0 ];then return 0;fi
        if [ $debug_app_level -ge $_debug_level ]
        then
            titre2 "$_txt";
        fi
        return 0;
    }

    function TITRE3() { titre3 "${1^^}"; }
    function titre3() {
        if [ $# -eq 0 ];then return 0;fi
        echo '';
        echo -e $COLOR_TITRE3"    ###  $1  ###"$COLOR_NORMAL
    }
    #function titre3_D( '_txt' [$_debug_gtt_level] )
    function titre3_D(){
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE;fi

        local    _txt="$1";
        local -i _debug_gtt_level=${2:-$DEBUG_GTT_LEVEL_DEFAUT};
        #display-vars '_debug_gtt_level' "$_debug_gtt_level";
        if [ $debug_gtt_level -eq 0 ];then return 0;fi
        if [ $debug_gtt_level -ge $_debug_gtt_level ]; then titre3 "$_txt"; fi
        return 0;
    }
    #function titre3_d( '_txt' [$_debug_level] )
    function titre3_d(){
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE;fi

        local    _txt="$1";
        local -i _debug_level=${2:-$DEBUG_LEVEL_DEFAUT};
        #display-vars '_debug_level' "$_debug_level";
        if [ $debug_app_level -eq 0 ];then return 0;fi
        if [ $debug_app_level -ge $_debug_level ]
        then
            titre3 "$_txt";
        fi
        return 0;
    }

    function TITRE4() { titre4 "${1^^}"; }
    function titre4() {
        if [ $# -eq 0 ];then return 0;fi
        echo -e $COLOR_TITRE4"      ====  $1  ===="$COLOR_NORMAL
        return 0;
    }
    #function titre4_D( '_txt' [$_debug_gtt_level] )
    function titre4_D(){
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE;fi

        local    _txt="$1";
        local -i _debug_gtt_level=${2:-$DEBUG_GTT_LEVEL_DEFAUT};
        #display-vars '_debug_gtt_level' "$_debug_gtt_level";
        if [ $debug_gtt_level -eq 0 ];then return 0;fi
        if [ $debug_gtt_level -ge $_debug_gtt_level ]
        then
            titre4 "$_txt"
        fi
        return 0;
    }
    #function titre4_d( '_txt' [$_debug_level] )
    function titre4_d(){
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE;fi

        local    _txt="$1";
        local -i _debug_level=${2:-$DEBUG_LEVEL_DEFAUT};
        #display-vars '_debug_level' "$_debug_level";
        if [ $debug_app_level -eq 0 ];then return 0;fi
        if [ $debug_app_level -ge $_debug_level ]
        then
            titre4 "$_txt";
        fi
        return 0;
    }


    function titreUsage() {
        if [ $# -eq 0 ];then return 0;fi
        echo -e $MAGENTA"=============="
        echo "$1"$COLOR_NORMAL
        return 0;
    }

    function titreCmd() {
        if [ $# -eq 0 ];then return 0;fi
        echo -e $COLOR_CMD"$1"$COLOR_NORMAL
        return 0;
    }
    function titreCodeInline() {
        if [ $# -eq 0 ];then return 0;fi
        echo -e -n $COLOR_CMD"$1"$COLOR_NORMAL
        return 0;
    }

    function titreInfo() {
        if [ $# -eq 0 ];then return 0;fi
        echo -e $COLOR_INFO"$1"$COLOR_NORMAL
        return 0;
    }

    function TITREINFO() {
        if [ $# -eq 0 ];then return 0;fi
        echo -e $COLOR_INFO"${1^^}"$COLOR_NORMAL
        return 0;
    }

    function titreWarn() {
        if [ $# -eq 0 ];then return 0;fi
        echo -e $COLOR_WARN"$1"$COLOR_NORMAL
        return 0;
    }

    function titreLib() {
        #if [ $# -eq 0 ];then return 0;fi
        local _libNom="${1:-""}";
        echo -e "\n${COLOR_INFO}=== LIBRAIRIE: $_libNom ==="$COLOR_NORMAL
        return 0;
    }

    function titreLib_D() {
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE;fi

        local    _txt="$1";
        local -i _debug_gtt_level=${2:-$DEBUG_GTT_LEVEL_DEFAUT};

        if [ $debug_gtt_level -eq 0 ];then return 0;fi
        if [ $debug_gtt_level -ge $_debug_gtt_level ]
        then
            echo -e "\n${COLOR_INFO}=== LIBRAIRIE: $_libNom ==="$COLOR_NORMAL
        fi
        return 0;
    }
#


###########
# texte-* #
###########
    # Centre un texte d'apres _largeur
    function texte-centre-echo(){
        if [ $# -eq 0 ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]} '_txt' [largeur=\$terminal_largeur]";
            return $E_ARG_BAD;
        fi

        local    _txt="${1:-""}";
        local -i _txt_lg=${#1};
        local -i _larg=${2:-$terminal_largeur};
        local espaceNb_before=$(( (_larg-_txt_lg)/2 ));               # display-vars 'espaceNb_before' "$espaceNb_before";
        local espaceNb_after=$((_larg-_txt_lg-espaceNb_before));      # display-vars 'espaceNb_after' "$espaceNb_after";
        echo "$(car-duplique-n $espaceNb_before ' ')$_txt$(car-duplique-n $espaceNb_after ' ')";
    }


    # Centre un texte d'apres _largeur
    declare  texte_centre="";
    function texte-centre(){
        local -i _fct_pile_app_level=3;
        fct-pile-app-in "$*" $_fct_pile_app_level;
        if [ $# -eq 0 ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]} '_txt' [largeur=\$terminal_largeur]";
            fct-pile-app-out $E_ARG_BAD; return $E_ARG_BAD;
        fi

        local    _txt="${1:-""}";
        local -i _txt_lg=${#1};
        local -i _larg=${2:-$terminal_largeur};
        local espaceNb_before=$(( (_larg-_txt_lg)/2 ));               # display-vars 'espaceNb_before' "$espaceNb_before";
        local espaceNb_after=$((_larg-_txt_lg-espaceNb_before));      # display-vars 'espaceNb_after' "$espaceNb_after";
        texte_centre="$(car-duplique-n $espaceNb_before ' ')$_txt$(car-duplique-n $espaceNb_after ' ')";
        #display-vars 'texte_centre' "$texte_centre";
        fct-pile-app-out $E_TRUE; return $E_TRUE;

    }
#

return 0;