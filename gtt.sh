#!/bin/bash
set -u;

declare -r SCRIPT_PATH="$(realpath "$0")";  # chemin  absolu complet du script rep + nom
declare -r SCRIPT_REP="${SCRIPT_PATH%/*}";  # Repertoire absolu du script (pas de slash de fin)
declare -r GTT_VERSION='2024.11.30';        # derniere maj
#declare CONF_ROOT="$HOME/.config/legite/";
#declare CONF_REP="$CONF_ROOT";
declare -r -i SCRIPT_DEBUT=$(date +%s);
declare    -i SCRIPT_FIN=0;
declare    -i SCRIPT_DUREE=0;


######################################################## #
# Version de gtt                                         #
# (selon $HOME/.config/legite/gtt/gtt-versionSelect.ini) #
##########################################################
    declare GTT_VERSION_SELECT='sid';
    # - Chargement de la version défini par l'utilisateur - #
    if [ -f "$HOME/.config/legite/gtt/gtt-versionSelect.ini" ]
    then
        GTT_VERSION_SELECT="$(cat "$HOME/.config/legite/gtt/gtt-versionSelect.ini")";
        #echo 'GTT_VERSION_SELECT' "$GTT_VERSION_SELECT";
    fi

    declare GTT_VERSION_SELECT_REP="$SCRIPT_REP/apps/gtt/$GTT_VERSION_SELECT";
    declare GTT_CORE_REP="$GTT_VERSION_SELECT_REP/core";
    #declare gtt_core_pathname="$GTT_CORE_REP/gtt-core.sh";
    #if [ ! -f "$gtt_core_pathname" ]
    #then
    #    echo "'$gtt_core_pathname' introuvable -> quit";
    #    exit 1;
    #fi
#


# ####################### #
# FONCTIONS CORE PRIMAIRE #
# ####################### #
source "$GTT_CORE_REP/gtt-core-erreur.sh";
source "$GTT_CORE_REP/gtt-vars.sh";
source "$SCRIPT_REP/themes/defaut/theme-defaut.sh";
source "$GTT_CORE_REP/gtt-core-debug.sh";
#source "$GTT_CORE_REP/gtt-core-PSP-analyse.sh";
source "$GTT_CORE_REP/gtt-core-fct-pile.sh";        # ex-fctIn/ex-fctOut
source "$GTT_CORE_REP/gtt-core-divers.sh";

source "$GTT_CORE_REP/gtt-core-PSP.sh";         
psp-analyse;                                        # -D est accessible
source "$GTT_CORE_REP/gtt-core-PSP-file.sh";
source "$GTT_CORE_REP/gtt-core-PSP-rep-cfg.sh";
source "$GTT_CORE_REP/gtt-core-theme.sh";
source "$GTT_CORE_REP/gtt-core-tmp.sh";
source "$GTT_CORE_REP/gtt-core-log.sh";


#echo " === CHARGEMENT CONFS ===";
exec-file "$CONF_ORI_PATH";                         # Peut redéfinir: theme,tmp,log, lanceur
exec-file "$CONF_USR_PATH";                         # Fonctions utilisateurs (peut surcharger les fonctions précédentes)

#echo " === CHARGEMENT DES THEMES ===";
theme-load;

source "$GTT_CORE_REP/gtt-core-notifs-pile.sh";
#source "$GTT_CORE_REP/gtt-core-update.sh";
source "$GTT_CORE_REP/gtt-core-trap.sh";



#############################
# CHARGEMENT DU SYSTEME PSP #
#############################
source "$GTT_CORE_REP/gtt-core-apps.sh";
source "$GTT_CORE_REP/gtt-core-collections.sh";
source "$GTT_CORE_REP/gtt-core-libs.sh";                #


titre4_D "Librairie de tests ajoutés par gtt.sh" 2;
    # clear gtt.sh test-lib1; 
    addLibrairie 'test-lib1';
    function      test-lib1() {
        echo "JE SUIS DANS LA FONCTION ${FUNCNAME[0]}() du fichier: '${BASH_SOURCE[0]}'";
    }
    addLibrairie 'test-lib2';
    function      test-lib2() {
        echo "JE SUIS DANS LA FONCTION ${FUNCNAME[0]}() du fichier: '${BASH_SOURCE[0]}'";
    }
titre4_D "Librairie de tests ajoutés par gtt.sh:END" 2;

source "$GTT_CORE_REP/gtt-core-inode.sh";                  # 
source "$GTT_CORE_REP/gtt-core-fs.sh";                  # filesystem
source "$GTT_CORE_REP/gtt-core-fs-recursif.sh";
#source "$GTT_CORE_REP/gtt-core-fs-recursif-methode";   # Obsolete?
source "$GTT_CORE_REP/gtt-core-dateTime.sh";            # gestion du temps
source "$GTT_CORE_REP/gtt-core-texte-normalize.sh"
source "$GTT_CORE_REP/gtt-core-inode-normalize.sh"
source "$GTT_CORE_REP/gtt-core-colonnes.sh";

exec-file "$COLLECTIONS_REP/_ini.cfg.sh";               # Chargement systématique

# = LANCEUR = #
source "$GTT_CORE_REP/gtt-core-lanceur-standard.sh";    # declare -gr TMP_ROOT


########
# MAIN #
########
lanceur-standard-begin;
lanceur-standard-end;
if $isTrapCAsk;then exit $E_CTRL_C;fi
exit 0;