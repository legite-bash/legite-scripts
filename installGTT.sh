#!/bin/bash
set -u;
declare -ri E_FALSE=88;

#INSTALL_REP="${1:-"$HOME/bin"}";
INSTALL_REP="${1:-"$HOME/bin"}";        # TESTER DANS UN REP jail


echo "Ce programme va installer le programme gtt.sh dans '$INSTALL_REP'.";
echo "Pour changer le repertoire d'installation: '$0 repertoire'";
echo -n "Pret pour l'installation? [O/n]";
read isInstall;

if [ "${isInstall,,}" = 'n' ]
then
    echo "Installation interrompu.";
    exit $E_FALSE;
fi

echo '';
if mkdir -p "$INSTALL_REP"
then
    echo "Création de '$INSTALL_REP'";
else
    echo "Erreur: Impossible de créer le repertoire d'installation '$INSTALL_REP'. Interruption de l'installation.";
    exit $E_FALSE;
fi

echo '';
if cd "$INSTALL_REP"
then
    echo "Déplacement dans $INSTALL_REP";
else
    echo "Erreur: Impossible d'aller dans le repertoire d'installation '$INSTALL_REP'. Interruption de l'installation.";
    exit $E_FALSE;
fi
echo "PWD:$PWD";

echo '';
echo -n "Téléchargement de la derniere version de gtt:";
if wget -O "$INSTALL_REP/gtt-last.tar.gz"  "http://updates.legite.org/gtt/gtt-last.tar.gz"
then
    echo 'ok';
else
    echo "Erreur: Interruption de l'installation.";
    exit $E_FALSE;
fi


echo '';
echo -n "Decompression:";
if [ -f 'gtt-last.tar' ];then rm 'gtt-last.tar';fi
if gunzip -d "gtt-last.tar.gz"
then
    echo 'ok';
else
    echo "Erreur: Interruption de l'installation.";
    exit $E_FALSE;
fi

echo '';
echo "tar extraction: Extraire les fichiers dans $INSTALL_REP/gtt";
#echo -n "tar extraction: Extraire les fichiers dans $INSTALL_REP/gtt. Continuer[O/n]?";
#read isExtract;

#if [ "${isExtract,,}" = 'n' ]
#then
#    echo "Installation interrompu.";
#    exit $E_FALSE;
#fi

if tar  --overwrite-dir -v -xf gtt-last.tar
then
    echo 'ok';
else
    echo "Erreur: Interruption de l'installation.";
    exit $E_FALSE;
fi

echo "Création du répertoire des collections '$HOME/.config/legite/gtt/collections'";
if [ ! -d "$HOME/.config/legite/gtt/collections" ]
then
    if mkdir -p "$HOME/.config/legite/gtt/collections"
    then echo "Le repertoire personnel des collections a bien été crée";
    else echo  "Erreur lors de la création du repertoire personnel des collections";
    fi
fi
#if [ ! -f "$HOME/.config/legite/gtt/gtt.ori.sh" ];then touch "$HOME/.config/legite/gtt/gtt.ori.sh";fi 
if [ ! -f "$HOME/.config/legite/gtt/gtt.cfg.sh" ];then touch "$HOME/.config/legite/gtt/gtt.cfg.sh";fi 


echo '';
echo 'Création du lien symbolique';
echo "ln -s $INSTALL_REP/gtt/gtt.sh   $INSTALL_REP/gtt.sh";
if [ ! -e "$INSTALL_REP/gtt.sh" ];
then
    ln -s "$INSTALL_REP/gtt/gtt.sh" "$INSTALL_REP/gtt.sh";
fi

echo '';
echo "Pensez à mettre à jours votre PATH"
echo "path actuel: $PATH";
echo "$PATH:$INSTALL_REP";
