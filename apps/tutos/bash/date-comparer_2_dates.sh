echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


local _source_pathname="${BASH_SOURCE[0]}";         display-vars '_source_pathname' "$_source_pathname";
local _source_fullRep="${_source_pathname%/*.*}";   display-vars '_source_fullRep ' "$_source_fullRep";
local _source_lastRep="${_source_fullRep##*/}";     display-vars '_source_lastRep ' "$_source_lastRep";
local _source_name="${_source_pathname##*/}";       display-vars '_source_name    ' "$_source_name";
local _source_nom="${_source_name%.*}";             display-vars '_source_nom     ' "$_source_nom";
local _source_ext="${_source_name##*.}";            display-vars '_source_ext     ' "$_source_ext";


#local _self_time="$(ls -l --full-time $_source_pathname)";
#display-vars '_self_time' "$_self_time";

local _self_time="$(ls -l --full-time $_source_pathname | cut -f 6-7 -d ' ')";
display-vars '_self_time' "$_self_time";


local _now="$(date "+%Y-%m-%d %H:%I:%S.%N")";
display-vars '_now      ' "$_now";

if [ "$_self_time" != "$_now" ]
then echo "Les dates sont diffèrentes"
else echo "Les dates sont identiques"
fi

# clear; gtt.sh -d -v apps/tutos/bash/date-comparer_2_dates - ;

#######
# END #
#######
echo-d "${BASH_SOURCE[0]}:END" 1;
return 0;