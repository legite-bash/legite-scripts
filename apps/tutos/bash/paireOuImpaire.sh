echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

local _source_pathname="${BASH_SOURCE[0]}";         display-vars '_source_pathname' "$_source_pathname";
local _source_fullRep="${_source_pathname%/*.*}";   display-vars '_source_fullRep ' "$_source_fullRep";
local _source_lastRep="${_source_fullRep##*/}";     display-vars '_source_lastRep ' "$_source_lastRep";
local _source_name="${_source_pathname##*/}";       display-vars '_source_name    ' "$_source_name";
local _source_nom="${_source_name%.*}";             display-vars '_source_nom     ' "$_source_nom";
local _source_ext="${_source_name##*.}";            display-vars '_source_ext     ' "$_source_ext";



function paireOuImpaire(){
    echo 'paire ou impaire?'

    _x=9
    if [ $(( _x%2)) = 1 ]
    then echo "$LINENO:$_x:impaire";    # impaire
    else echo "$LINENO:$_x:paire";
    fi

    _x=10
    if [ $(( _x%2)) = 1 ]
    then echo "$LINENO:$_x:impaire";    # paire
    else echo "$LINENO:$_x:paire";
    fi

    _x=9
    if [ $(( _x%2)) = 0 ]
    then echo "$LINENO:$_x:paire";    
    else echo "$LINENO:$_x:impaire";  # impaire
    fi

    _x=10
    if [ $(( _x%2)) = 0 ]
    then echo "$LINENO:$_x:paire";    # paire
    else echo "$LINENO:$_x:impaire";
    fi

    echo '---'
    _x=9
    if [ $(( _x%2)) ]
    then echo "$LINENO:$_x:paire";    # paire !!! erreur
    else echo "$LINENO:$_x:impaire";  
    fi

    _x=10
    if [ $(( _x%2)) ]
    then echo "$LINENO:$_x:paire";    # paire
    else echo "$LINENO:$_x:impaire";
    fi
}