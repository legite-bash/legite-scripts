echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 
# clear; gtt.sh apps/tutos/bash/SOURCE_BASH.sh

local _source_pathname="${BASH_SOURCE[0]}";         display-vars '_source_pathname' "$_source_pathname";
local _source_fullRep="${_source_pathname%/*.*}";   display-vars '_source_fullRep ' "$_source_fullRep";
local _source_lastRep="${_source_fullRep##*/}";     display-vars '_source_lastRep ' "$_source_lastRep";
local _source_name="${_source_pathname##*/}";       display-vars '_source_name    ' "$_source_name";
local _source_nom="${_source_name%.*}";             display-vars '_source_nom     ' "$_source_nom";
local _source_ext="${_source_name##*.}";            display-vars '_source_ext     ' "$_source_ext";



#################
## SOURCE_BASH ##
#################

display-vars 'BASH_SOURCE' "${BASH_SOURCE[0]}";

local _source_fullRep="${BASH_SOURCE%/*.*}";
local _source_lastRep="${_source_fullRep##*/}";
local _source_fullname="${BASH_SOURCE##*/}";
local _source_name="${_source_fullname%.*}";
local _source_ext="${_source_fullname##*/}";


display-vars '_source_fullRep="${BASH_SOURCE%/*.*}'       "${BASH_SOURCE%/*.*}";
display-vars '_source_lastRep="${_source_fullRep##*/}'    "${_source_fullRep##*/}";
display-vars '_source_fullname=${BASH_SOURCE##*/}'        "${BASH_SOURCE##*/}";
display-vars '_source_name=${_source_fullname%.*}'        "${_source_fullname%.*}"
display-vars '_source_ext=${_source_fullname##*.}'        "${_source_fullname##*.}"

hr;

display-vars '${FUNCNAME}   '       "${FUNCNAME}";
display-vars '${FUNCNAME[0]}'       "${FUNCNAME[0]}";
display-vars '${FUNCNAME[1]}'       "${FUNCNAME[1]}";

display-vars '${LINENO}     '       "${LINENO}";
display-vars '${LINENO[0]}  '       "${LINENO[0]}";
display-vars '${LINENO[1]}  '       "variable sans liaison";
