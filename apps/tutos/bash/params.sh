echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

local _source_pathname="${BASH_SOURCE[0]}";         display-vars '_source_pathname' "$_source_pathname";
local _source_fullRep="${_source_pathname%/*.*}";   display-vars '_source_fullRep ' "$_source_fullRep";
local _source_lastRep="${_source_fullRep##*/}";     display-vars '_source_lastRep ' "$_source_lastRep";
local _source_name="${_source_pathname##*/}";       display-vars '_source_name    ' "$_source_name";
local _source_nom="${_source_name%.*}";             display-vars '_source_nom     ' "$_source_nom";
local _source_ext="${_source_name##*.}";            display-vars '_source_ext     ' "$_source_ext";



#display-vars 'varInclude' "$BASH_SOURCE:$varInclude";

#addLibrairie 'params': # pas d'execution -All
function       params(){
    fctIn "$*";
    fctOut "${FUNCNAME{0]}(0)"; return 0;
}


addLibrairie 'test-porte';
function      test-porte(){
    fctIn "$*";

    titreInfo "La variable 'varInclude' vient du fichier core requis";
    display-vars 'varInclude' "$BASH_SOURCE:$FUNCNAME:$varInclude";

    fctOut "${FUNCNAME{0]}(0)";return 0;
}


addLibrairie 'test-AfficherParams';
function      test-AfficherParams(){
    fctIn "$*";

    display-vars 'varInclude' "$BASH_SOURCE:$FUNCNAME:$varInclude";

    function       fTxt1(){
        fctIn "$*";

        echo  '$@' "$@"
        display-vars '$*' "$*"
        local p1="${1:-''}";display-vars "${FUNCNAME{0]}($@)-p1" "$p1";
        local p2="${2:-''}";display-vars "${FUNCNAME{0]}($@)-p2" "$p2";
        local p3="${3:-''}";display-vars "${FUNCNAME{0]}($@)-p3" "$p3";
        local p4="${4:-''}";display-vars "${FUNCNAME{0]}($@)-p4" "$p4";
        
        fTxt2 "$p1" "$p2" "$p3" "$p4"
        fctOut "${FUNCNAME{0]}(0)";return 0;
    }

    function      fTxt2(){
        fctIn "$*";
        local p1="${1:-''}";display-vars "${FUNCNAME{0]}($@)-p1" "$p1";
        local p2="${2:-''}";display-vars "${FUNCNAME{0]}($@)-p2" "$p2";
        local p3="${3:-''}";display-vars "${FUNCNAME{0]}($@)-p3" "$p3";
        local p4="${4:-''}";display-vars "${FUNCNAME{0]}($@)-p4" "$p4";
        fctOut "${FUNCNAME{0]}(0)";return 0;
    }

    isDebug=1;
    eval-echo 'fTxt1 "TXT1" "TXT A" "TXT B"'
    eval-echo 'fTxt1 "TXT1" 10 "TXT A" "TXT B"'
    eval-echo 'fTxt1 "TXT1" 10 "TXT A" 20'

    fctOut "${FUNCNAME{0]}(0)";return 0;
}

function      test-PSP-supp(){
    fctIn "$*";
    fctOut "${FUNCNAME{0]}(0)";return 0;
}