echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh apps/tutos/bash/tableaux.sh
# date de création    : 2024.04.23
# date de modification: 
# Description: 

local _source_pathname="${BASH_SOURCE[0]}";         display-vars '_source_pathname' "$_source_pathname";
local _source_fullRep="${_source_pathname%/*.*}";   display-vars '_source_fullRep ' "$_source_fullRep";
local _source_lastRep="${_source_fullRep##*/}";     display-vars '_source_lastRep ' "$_source_lastRep";
local _source_name="${_source_pathname##*/}";       display-vars '_source_name    ' "$_source_name";
local _source_nom="${_source_name%.*}";             display-vars '_source_nom     ' "$_source_nom";
local _source_ext="${_source_name##*.}";            display-vars '_source_ext     ' "$_source_ext";

##################
## Les tableaux ##
##################
titre1 'tableaux de fonctions'
    echo "declare -a pour un tableau indicé"
    echo "declare -A pour un tableau associatif"

declare -A fonctionsTbl;

titre2 'Une fonction dans un tableau'
function fonction1() {
    echo 'Ceci est la fonction1';
}
eval-echo "fonctionsTbl['fct1']=fonction1";
eval-echo "${fonctionsTbl['fct1']}";


titre2 'Manipulation de tableaux'
    #eval-echo "declare -A tableauTest;"
    declare -A tableauTest;
    eval-echo 'tableauTest["defini"]="ok";'

    titre2 "# - echo sur un champ defini - #"
    echo '${tableauTest["defini"]}='${tableauTest["defini"]};

    titre2 "# - echo sur un champ NON defini - #"
    echo '${tableauTest["NonDefini"]};'
    echo "Le programme s'arrete si set -u"

    titre2 "# - Valeur par défaut si non défini - #"
    echo '${tableauTest["NonDefini"]:-"Cet indice n'est pas défini"}';
    ${tableauTest["NonDefini"]:-"Cet indice n'est pas défini"};         # Fait un echo sans la commande echo


    titre2 "# - Tester si champ defini - #"
    echo 'if [ ${tableauTest["defini"]+x} ]'
    if [ ${tableauTest["defini"]+x} ]
    then
        echo "Champ défini";
    else
        echo "Champ NON défini";
    fi


titre1 'Declaration d un tableau pour les tests suivants'
    echo "declare -r -a a=('un' 'deux' 'trois' 'quatre et cing')";
    declare -r -a a=('un' 'deux' 'trois' 'quatre et cing')

    titre2 "Nombre d'element d'un tableau"
    echo '${#a[@]}='${#a[@]}

    titre2 'Lecture de tableau'
    echo '${a[*]}='${a[*]}
    echo 'for valeur in ${a[*]}'
          for valeur in ${a[*]}
    do
        display-vars "$valeur" "$valeur"
    done

    echo ''
    echo '${a[@]}='${a[@]}
    echo 'for valeur in "${a[@]}"'
          for valeur in "${a[@]}"
    do
        display-vars "$valeur" "$valeur"
    done

    echo ''
    echo 'for ((index=0; index < ${#a[@]}; ++index))'
          for ((index=0; index < ${#a[@]}; ++index))
    do
        display-vars "${a['$index']}" "${a[$index]}"
    done
