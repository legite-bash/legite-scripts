echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

local _source_pathname="${BASH_SOURCE[0]}";         display-vars '_source_pathname' "$_source_pathname";
local _source_fullRep="${_source_pathname%/*.*}";   display-vars '_source_fullRep ' "$_source_fullRep";
local _source_lastRep="${_source_fullRep##*/}";     display-vars '_source_lastRep ' "$_source_lastRep";
local _source_name="${_source_pathname##*/}";       display-vars '_source_name    ' "$_source_name";
local _source_nom="${_source_name%.*}";             display-vars '_source_nom     ' "$_source_nom";
local _source_ext="${_source_name##*.}";            display-vars '_source_ext     ' "$_source_ext";


#addLibrairie 'syntaxe': # pas d'execution -Auto
function       syntaxe(){
    fctIn "$*";

    fctOut "$FUNCNAME(0)"; return 0;
}

# Bon résumé des tableaux
#http://www.ixany.org/docs/Shell_Les_tableaux_en_bash.html

# entre guillemets doubles:
## ${nom[*]} se développe en un seul mot contenant les valeurs de chaque élément du tableau séparées par le premier caractère de la variable spéciale IFS
## ${nom[@]} développe chaque élément de nom en un mot distinct.

#https://fr.embracingthegrace.com/641475-bash-declare-typeset-without-assignment-STWLVN


# Passer unetableau en parametre
#https://code-examples.net/fr/q/383e52


# Surcharge de fonction
addLibrairie 'surchargeFunction';
function      surchargeFunction(){
    function appeller1fois(){
        echo "Premiere forme de cette fonction";
    }

    function appeller1fois(){
        echo "Deuxiememe forme de cette fonction";
    }

    appeller1fois;
    # renvoie: Deuxiememe forme de cette fonction
    return 0;
}


#########
# ECHOS #
#########
    addLibrairie 'test_echos'
    function test_echos(){
        fctIn;

        function ecrit_une_ligne(){
        #fctIn "$*";            # laisser commenté
        echo '1ere ligne'
        #fctOut;return 0;       # laisser commenté
        }

        function ecrit_deux_lignes(){
        #fctIn "$*";            # laisser commenté
        echo '2-1ere ligne'
        echo '2-2eme ligne'
        #fctOut;return 0;       # laisser commenté
        }


        echo "-------"
        echo ========
        local text='texte a ecrire'
        echo $text;                 # retour chariot AVANT
        echo "$text";               # retour chariot AVANT
        echo $(ecrit_une_ligne)     # retour chariot AVANT
        echo "$(ecrit_une_ligne)"   # retour chariot AVANT
        echo ********
        echo $(ecrit_deux_lignes)   # les 2 lignes inlines
        echo "$(ecrit_deux_lignes)" # chaque ligne sur une ligne
        fctOut;return 0;
    }

#############
# LES PATHS #
#############
    addLibrairie 'cheminsRelatifs'
    function cheminsRelatifs() {
        fctIn "$*"
        display-vars '$0' "$0"
        display-vars '${BASH_SOURCE}' "${BASH_SOURCE}"
        display-vars '${BASH_SOURCE[*]}' "${BASH_SOURCE[*]}"
        display-vars '${BASH_SOURCE[0]}' "${BASH_SOURCE[0]}"
        display-vars '${FUNCNAME[*]}' "${FUNCNAME[*]}"
        display-vars '${FUNCNAME[0]}' "${FUNCNAME[0]}"
        fctOut; return 0;
    }

addLibrairie 'test-bash-syntaxe-if1'
function      test-bash-syntaxe-if1(){
    fctIn "$*";
    local _fichier="_test-PSP"
    ls -l "$_fichier";

    titre4 'Syntaxe 1:fonction -> $?';
    isCollectionExist "$_fichier";
    erreur-no-description-echo $?;

    titre4 'Syntaxe 2: if fonction "parametre; then';
    if isCollectionExist "$_fichier"
    then
        erreur-no-description-echo $?;
        echo 'La collection existe';
    else
        erreur-no-description-echo $?;
        echo 'La collection existe pas.';
    fi
    fctOut "$FUNCNAME(0)"; return 0;
}

addLibrairie 'test-bash-syntaxe-for_avec_tableau'
function      test-bash-syntaxe-for_avec_tableau(){
        fctIn "$FUNCNAME($*)"
        echo 'Tbleau associatif'
        local books=('In Search of Lost Time' 'Don Quixote' 'Ulysses' 'The Great Gatsby')
        echo '@ ';
        for book in "${books[@]}";      do  display-vars 'Book' "$book";done
        echo '$@3: affiche un element';
        for book in "${books[$@3]}";    do  display-vars 'Book' "$book";done
        echo '*: applatit le tableau';
        for book in "${books[*]}";      do  display-vars 'Book' "$book";done
        echo '$*3';
        for book in "${books[$*3]}";    do  display-vars 'Book' "$book";done

        echo 'Tableau indexé'
        declare -a books;
        books[0]='In Search of Lost Time';
        books[1]='Don Quixote';
        books[2]='Ulysses';
        books[3]='The Great Gatsby';
        echo '@ ';
        for book in "${books[@]}";      do  display-vars 'Book' "$book";done
        echo '$@3: affiche un element';
        for book in "${books[$@3]}";    do  display-vars 'Book' "$book";done
        echo '*: applatit le tableau';
        for book in "${books[*]}";      do  display-vars 'Book' "$book";done
        echo '$*3';
        for book in "${books[$*3]}";    do  display-vars 'Book' "$book";done
        fctOut "$FUNCNAME(0)";return 0;
    }

##################
## TEST SYNTAXE ##
##################
    addLibrairie 'test_syntaxes'
    function test_syntaxes(){
        fctIn "$*"        

        # --------- #
        argumentDeFonctions 1 2 'trois pas tout seul' "quatre accompagné"

        # --------- #
        local names=( 'Anderson da Silva' 'Wayne Gretzky' 'David Beckham' 'Long
        Name');

        test_syntaxes_ArobaseEtoile

        # --------- #
        test_syntaxe_FonctionArobaseEtoile un deux 'trois pas     tout seul' "quatre contenant un 
            retour chariot"
        fctOut;return 0;
    }


    addLibrairie 'argumentDeFonctions'
    function argumentDeFonctions(){
        fctIn "$*"
        echo 'for "'
        local _argNb=0;
        while true
        do
            ((_argNb++))
            p=${1:-''}
            if [ "$p" == '' ];then break;fi
            display-vars "$_argNb" "$1"; shift
        fctOut;return 0;
        done
    }

    addLibrairie 'test_syntaxes_ArobaseEtoile'
    function test_syntaxes_ArobaseEtoile(){
        fctIn "$*"
        function showDataArobase(){
            fctIn "$*"
            for name in ${names[@]};    do display-vars 'names[@]' "$name"; done
            fctOut;return 0;
        }
        function showDataDollarArobase(){
            fctIn "$*"
            for name in ${names[$@]};   do display-vars 'names[$@]' "$name"; done
            fctOut;return 0;
        }
        function showDataEtoile(){
            fctIn "$*"
            for name in ${names[*]};    do display-vars 'names[*]' "$name"; done
            fctOut;return 0;
        }
        function showDataDollarEtoile(){
            fctIn "$*"
            for name in ${names[$*]};   do display-vars 'names[$*]' "$name"; done
            fctOut;return 0;
        }


        echo "Quand c'est un tableau:"
        echo "Il n'y a aucune difference entre \$@ et \$*: les 2 renvoient le 1er indice"
        echo "Il n'y a aucune difference entre @ et $*: les 2 renvoient TOUS les indices"
        echo "Avec l'IFS avec espace: chaque mot devient un indice"

        echo -e "\nWith default IFS value..."
        IFS=$' \n\t'  # ' \n\t' is the default IFS value
        #IFS=$' \n\t'

        showDataArobase
        showDataEtoile
        showDataDollarArobase
        showDataDollarEtoile

        echo -e "With strict-mode IFS value..."
        IFS=$'\n\t'
        showDataArobase
        showDataEtoile
        showDataDollarArobase
        showDataDollarEtoile
        fctOut;return 0;

    }

    addLibrairie 'test_syntaxe_FonctionArobaseEtoile'
    function test_syntaxe_FonctionArobaseEtoile(){
        fctIn "$*"        
        echo "Quand ce sont les parametres d'une fonction:"
        echo "Il n'y a aucune difference entre \$@ et \$*: les 2 renvoient le 1er indice"
        echo "Il n'y a aucune difference entre  @ et  *: les 2 renvoient TOUS les indices"
        echo "Avec l'IFS avec espace: chaque mot devient un indice"

        #echo '@ est le caractere @'
        #for parametre in  @;       do display-vars 'parametre[@] ' "$parametre"; done
        echo '$@ est le tableau de parametres'
        for parametre in $@;        do display-vars 'parametre[$@]' "$parametre"; done
        echo ''

        #echo '* est le repertoire courant'
        #for parametre in  *;       do display-vars 'parametre[*] ' "$parametre"; done
        echo '$* est le tableau de parametres'
        for parametre in $*;        do display-vars 'parametre[$*]' "$parametre"; done
        fctOut;return 0;
    }

    
