echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

local _source_pathname="${BASH_SOURCE[0]}";         display-vars '_source_pathname' "$_source_pathname";
local _source_fullRep="${_source_pathname%/*.*}";   display-vars '_source_fullRep ' "$_source_fullRep";
local _source_lastRep="${_source_fullRep##*/}";     display-vars '_source_lastRep ' "$_source_lastRep";
local _source_name="${_source_pathname##*/}";       display-vars '_source_name    ' "$_source_name";
local _source_nom="${_source_name%.*}";             display-vars '_source_nom     ' "$_source_nom";
local _source_ext="${_source_name##*.}";            display-vars '_source_ext     ' "$_source_ext";



#addLibrairie 'math'
#function       math(){
    fctIn "$*";

    titre3 "Des maths avec Bash";
    titreInfo "";
    #display-vars '' "";

    titre4 "Les variables";
    local _chiffre1=1000;   display-vars '_chiffre1' "$_chiffre1";
    local _chiffre2=50;     display-vars '_chiffre2' "$_chiffre2";


    titre4 "Incrémentation d'une variable";
    eval-echo "(( _chiffre1++ ))";

    display-vars ' (( _chiffre1++ ))' " (( _chiffre1++ ))";
    display-vars '$(( _chiffre1++ ))' "$(( _chiffre1++ ))";


    titre4 "Addition";

    display-vars '$(( _chiffre1 + _chiffre2 ))' "$(( _chiffre1 + _chiffre2 ))";


#    fctOut "$FUNCNAME(0)"; return 0;
#}

    
