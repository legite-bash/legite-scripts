echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

local _source_pathname="${BASH_SOURCE[0]}";         display-vars '_source_pathname' "$_source_pathname";
local _source_fullRep="${_source_pathname%/*.*}";   display-vars '_source_fullRep ' "$_source_fullRep";
local _source_lastRep="${_source_fullRep##*/}";     display-vars '_source_lastRep ' "$_source_lastRep";
local _source_name="${_source_pathname##*/}";       display-vars '_source_name    ' "$_source_name";
local _source_nom="${_source_name%.*}";             display-vars '_source_nom     ' "$_source_nom";
local _source_ext="${_source_name##*.}";            display-vars '_source_ext     ' "$_source_ext";


##############
## TEST FOR ##
##############

    addLibrairie 'test-for'
    function      test-for(){
        fctIn "$*"
        libExecAuto 'test-tableau_iteration';

        fctOut; return 0;
    }

    # --------- #
    addLibrairie 'test-tableau_iteration'
    function      test-tableau_iteration(){
        fctIn "$*"
        titre2 'Tableau associatif'
        local books=('In Search of Lost Time' 'Don Quixote' 'Ulysses' 'The Great Gatsby')
        echo '@ ';
        for book in "${books[@]}";      do  display-vars 'Book' "$book";done
        echo '$@3: affiche un element';
        for book in "${books[$@3]}";    do  display-vars 'Book' "$book";done
        echo '*: applatit le tableau';
        for book in "${books[*]}";      do  display-vars 'Book' "$book";done
        echo '$*3';
        for book in "${books[$*3]}";    do  display-vars 'Book' "$book";done

        titre2 'Tableau associatif: itération sur key'
        declare -A livres;
        livres['livre1']='livre premier';
        livres['livre2']='livre second';
        livres['livre3']='livre troisième';
        livres['livre4']='livre 4';
        for key in "${!livres[@]}";     do
            display-vars "$key" "${livres[$key]}"
        done

        titre2 'Tableau indexé'
        declare -a books;
        books[0]='In Search of Lost Time';
        books[1]='Don Quixote';
        books[2]='Ulysses';
        books[3]='The Great Gatsby';
        echo '@ ';
        for book in "${books[@]}";      do  display-vars 'Book' "$book";done
        echo '$@3: affiche un element';
        for book in "${books[$@3]}";    do  display-vars 'Book' "$book";done
        echo '*: applatit le tableau';
        for book in "${books[*]}";      do  display-vars 'Book' "$book";done
        echo '$*3';
        for book in "${books[$*3]}";    do  display-vars 'Book' "$book";done
        fctOut; return 0;
    }

    function      test-for_avec_variable(){
        fctIn "$*"
        #for val in {1..${end}}         do  display-vars 'val' "$val";done
        #ne fonctionne pas
        fctOut; return 0;
    }
#

