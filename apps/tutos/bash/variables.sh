echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

local _source_pathname="${BASH_SOURCE[0]}";         display-vars '_source_pathname' "$_source_pathname";
local _source_fullRep="${_source_pathname%/*.*}";   display-vars '_source_fullRep ' "$_source_fullRep";
local _source_lastRep="${_source_fullRep##*/}";     display-vars '_source_lastRep ' "$_source_lastRep";
local _source_name="${_source_pathname##*/}";       display-vars '_source_name    ' "$_source_name";
local _source_nom="${_source_name%.*}";             display-vars '_source_nom     ' "$_source_nom";
local _source_ext="${_source_name##*.}";            display-vars '_source_ext     ' "$_source_ext";


###################
## Les variables ##
###################

titre1 "Les variables bash"$NORMAL
echo "\$0:Contient le nom du script tel qu'il a été invoqué: $0"
echo "\$*: L'ensembles des paramètres sous la forme d'un seul argument: $*"
echo "\$@: L'ensemble des arguments, un argument par paramètre: $@"
echo "\$#: Le nombre de paramètres passés au script: $#"
echo "\$?: Le code retour de la dernière commande: $?"
echo "\$$: Le PID su shell qui exécute le script: $$"
echo "\$\!: Le PID du dernier processus lancé en arrière-plan: ${!:-'aucun'}"
display-vars '$FUNCNAME' "$FUNCNAME"

display-vars '$BASH_SOURCE. Path du fichier (inclus):'   "$BASH_SOURCE";
display-vars 'rep courant: $PWD'              "$PWD";
display-vars 'fonction actuelle: $FUNCNAME'   "$FUNCNAME";
display-vars 'Ligne courante: $LINENO'        "$LINENO";
display-vars 'rep HOME: $HOME'                "$HOME";


titre2 'true'

echo 'declare VRAI=true';
declare VRAI=true;
if true
then echo "$LINENO: true is true";
else echo "$LINENO: true is NOT true";
fi

if $VRAI
then echo "$LINENO:\$VRAI is true";
else echo "$LINENO:\$VRAI is NOT true";
fi

if [ $VRAI = true ]
then echo "$LINENO:\$VRAI = true is true";
else echo "$LINENO:\$VRAI = true is NOT true";
fi

if $VRAI && $VRAI
then echo "$LINENO:\$VRAI && \$VRAI = true is true";
else echo "$LINENO:\$VRAI && \$VRAI = true is NOT true";
fi

if ! $VRAI 
then echo "$LINENO: ! \$VRAI  is true";
else echo "$LINENO: ! \$VRAI  is NOT true";
fi



titre2 'false'

echo 'declare FAUX=false';
declare FAUX=false;
if false
then
    echo "$LINENO: false is true";
else
    echo "$LINENO: false is NOT true";
fi

if $FAUX
then echo "$LINENO:\$FAUX is true";
else echo "$LINENO:\$FAUX is NOT true";
fi

if [ $FAUX = false ]
then echo "$LINENO:\$FAUX = false is true";
else echo "$LINENO:\$FAUX = false is NOT true";
fi


if [ "$FAUX" = false ]
then echo "$LINENO:\"\$FAUX\" = false is true";
else echo "$LINENO:\"\$FAUX\" = false is NOT true";
fi

if [ "$FAUX" = "false" ]
then echo "$LINENO:\"\$FAUX\" = \"false\" is true";
else echo "$LINENO:\"\$FAUX\" = \"false\" is NOT true";
fi

if ! $FAUX
then echo "$LINENO: ! \$FAUX  is true";
else echo "$LINENO: ! \$FAUX  is NOT true";
fi

