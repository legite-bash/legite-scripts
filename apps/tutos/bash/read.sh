echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

local _source_pathname="${BASH_SOURCE[0]}";         display-vars '_source_pathname' "$_source_pathname";
local _source_fullRep="${_source_pathname%/*.*}";   display-vars '_source_fullRep ' "$_source_fullRep";
local _source_lastRep="${_source_fullRep##*/}";     display-vars '_source_lastRep ' "$_source_lastRep";
local _source_name="${_source_pathname##*/}";       display-vars '_source_name    ' "$_source_name";
local _source_nom="${_source_name%.*}";             display-vars '_source_nom     ' "$_source_nom";
local _source_ext="${_source_name##*.}";            display-vars '_source_ext     ' "$_source_ext";



#############
# -  MAIN - #
#############
titre1 "$_source_fullname";

#https://www.educba.com/linux-read/

echo 'read [options] [name...]';

titre3 "read";
echo 'Sans nom de varable donné le resultat sera écrit dans $REPLY par défaut'
read;
display-vars 'REPLY' "$REPLY";

titre3 "read resultat";
echo 'la valeur sera mit dans la variable resultat.'
read resultat;
display-vars 'resultat' "$resultat";


titre3 "echo -n :Pas de retour chariot avant read";
echo -n 'Le -n empechera le retour chariot. Etes-vous daccord? [O/n]?';
read resultat;
display-vars 'resultat' "$resultat";


titre2 'Les options de read';

echo "
      -a tableau	affecte les mots lus séquentiellement aux indices de la variable
    		tableau ARRAY en commençant à 0
      -d délim	continue jusqu'à ce que le premier caractère de DELIM soit lu,
    		au lieu du retour à la ligne
      -e		utilise « Readline » pour obtenir la ligne
      -i texte	Utilise TEXTE comme texte initial pour « Readline »
      -n n	termine après avoir lu N caractères plutôt que d'attendre
    		un retour à la ligne, mais obéi à un délimiteur si moins de N caractères
    		sont lus avant le délimiteur
      -N n	termine seulement après avoir lu exactement N caractères, à moins
    		que le caractère EOF soit rencontré ou que le délai de lecture n'expire.
    		Les délimiteurs sont ignorés
      -p prompt	affiche la chaîne PROMPT sans retour à la ligne final, avant de
    		tenter une lecture
      -r	ne pas permettre aux barres obliques inverses de se comporter comme
    		des caractères d'échappement
      -s	ne pas répéter l'entrée provenant d'un terminal
      -t timeout	expire et renvoie un code d'échec si une ligne d'entrée complète
    		n'est pas lue en moins de TIMEOUT secondes.  La valeur de la variable TIMEOUT
    		est le délai d'expiration par défaut.  TIMEOUT peut être un nombre décimal.
    		Si TIMEOUT est à zéro, la lecture se termine immédiatement sans essayer de
    		lire la moindre donnée mais elle renvoie un code de succès seulement
    		si l'entrée est disponible sur le descripteur de fichier.  Le code
    		de sortie est supérieur à 128 si le délai a expiré
      -u fd	lit depuis le descripteur de fichier FD plutôt que l'entrée standard
";

titre3 "read -l;";
read -l;

titre3 "read -p;";
read -p;


#######
# END #
#######
echo-d "$BASH_SOURCE:END";
return 0;