echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

local _source_pathname="${BASH_SOURCE[0]}";         display-vars '_source_pathname' "$_source_pathname";
local _source_fullRep="${_source_pathname%/*.*}";   display-vars '_source_fullRep ' "$_source_fullRep";
local _source_lastRep="${_source_fullRep##*/}";     display-vars '_source_lastRep ' "$_source_lastRep";
local _source_name="${_source_pathname##*/}";       display-vars '_source_name    ' "$_source_name";
local _source_nom="${_source_name%.*}";             display-vars '_source_nom     ' "$_source_nom";
local _source_ext="${_source_name##*.}";            display-vars '_source_ext     ' "$_source_ext";



titre2 "Portée des fonctions et variables";

addLibrairie 'syntaxe';
function      porte(){
    fctIn "$*";

    fctOut "${FUNCNAME{0]}(0)"; return 0;
}


# Surcharge de fonction
addLibrairie 'surchargeFunction';
function      surchargeFunction(){
    function appeller1fois(){
        echo "Premiere forme de cette fonction";
    }

    function appeller1fois(){
        echo "Deuxiememe forme de cette fonction";
    }

    appeller1fois;
    # renvoie: Deuxiememe forme de cette fonction
    return 0;
}

surchargeFunction;


titre3 "Porté des variables"

declare var_niv1=1;
declare var_existeDeja=1;
function inNiv1(){

    display-vars 'var_niv1' "$var_niv1 (dans parent)";
    declare var_niv1=2;
    display-vars 'var_niv1' "$var_niv1 (dans fct)";
    display-vars 'var_existeDeja' "$var_existeDeja (dans fct)";


    local       var_local='1';
    declare     var_declareInFct='1';       # equivaut à local
    declare  -g var_declareInFctWithG='1';  # rend la variable globale
    var_existeDeja=444;
} 

    inNiv1;

    display-vars 'var_niv1' "$var_niv1 (dans parent)";            # valeur non modifiée
    #display-vars 'var_local' "$var_local";                       # erreur: variable sans liaison
    #display-vars 'var_declareInFct' "$var_declareInFct";         # erreur: variable sans liaison
    display-vars 'var_declareInFctWithG' "$var_declareInFctWithG";
    display-vars 'var_existeDeja' "$var_existeDeja (dans parent)";            # valeur non modifiée




echo ""
varLocal="global"
function createVarLocal(){
    display-vars "varLocal" "$varLocal"
    local varLocal="local"
    display-vars "varLocal" "$varLocal"
    callVarLoval
    display-vars "varLocal" "$varLocal"
    declare variableDeclarerDansFonction=1
    variableCreeNonDeclarerDansFonction=1
}
function callVarLoval(){
    varLocal="sous fonction"
}

createVarLocal;
echo "varLocal=$varLocal"
echo "variableDeclarerDansFonction=${INFO}variable sans liaison$NORMAL"
echo "variableCreeNonDeclarerDansFonction=$variableCreeNonDeclarerDansFonction"
