echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

local _source_pathname="${BASH_SOURCE[0]}";         display-vars '_source_pathname' "$_source_pathname";
local _source_fullRep="${_source_pathname%/*.*}";   display-vars '_source_fullRep ' "$_source_fullRep";
local _source_lastRep="${_source_fullRep##*/}";     display-vars '_source_lastRep ' "$_source_lastRep";
local _source_name="${_source_pathname##*/}";       display-vars '_source_name    ' "$_source_name";
local _source_nom="${_source_name%.*}";             display-vars '_source_nom     ' "$_source_nom";
local _source_ext="${_source_name##*.}";            display-vars '_source_ext     ' "$_source_ext";


function findRep(){
    local _datas='';

    titre4 "find";
    _datas="$(find $APPS_REP -type d)";     display-vars '_datas' "$_datas";
    #_datas=$( find $APPS_REP -type d);      display-vars '_datas' "$_datas"; ~ aucune différence

    titre4 "trie en sens inverse";
    local _dt=$(echo "$_datas" | sort --reverse);     display-vars '_dt' "$_dt";


    return 0;
}


#############
# -  MAIN - #
#############
titre1 "$_source_fullname";


findRep;


#######
# END #
#######
echo;
echo-d "${BASH_SOURCE[0]}:END" 1;
return 0;