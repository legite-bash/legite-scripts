echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

local _source_pathname="${BASH_SOURCE[0]}";         display-vars '_source_pathname' "$_source_pathname";
local _source_fullRep="${_source_pathname%/*.*}";   display-vars '_source_fullRep ' "$_source_fullRep";
local _source_lastRep="${_source_fullRep##*/}";     display-vars '_source_lastRep ' "$_source_lastRep";
local _source_name="${_source_pathname##*/}";       display-vars '_source_name    ' "$_source_name";
local _source_nom="${_source_name%.*}";             display-vars '_source_nom     ' "$_source_nom";
local _source_ext="${_source_name##*.}";            display-vars '_source_ext     ' "$_source_ext";

# https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html

addLibrairie 'text-substitution'; # pas d'execution -All
function      text-substitution(){
    echo "$LINENO:$FUNCNAME";

    titreInfo "ATTENTION: Un texte ne peut pas etre substitué directement il faut passer par une variable!";
    return 0;
}

addLibrairie 'text-dupliqueNCar';
function      text-dupliqueNCar(){
    eval-echo "dupliqueNCar";
    eval-echo "dupliqueNCar 0 '*'";
    eval-echo "dupliqueNCar 9 '*'";
    return 0;
}

addLibrairie 'upperCase'
function      upperCase(){
    echo "${1^^}" # = UPERCASE $1
    return 0;
}

addLibrairie 'lowerCase'
function      lowerCase(){
    echo "${1,,}" # lower $1
    return 0;
}

addLibrairie 'substitution_delete-n-Car'
function      substitution_delete-n-Car(){
    eval-echo "variable='123456789'";
    echo -n 'Suppression du   1er caractere       :'; display-vars '${variable#?}'   "${variable#?}";
    echo -n 'Suppression de n 1er caractere       :'; display-vars '${variable#???}' "${variable#???}";
    echo -n 'Suppression du   dernier caractere   :'; display-vars '${variable%?}'   "${variable%?}";
    echo -n 'Suppression de n dernier caractere   :'; display-vars '${variable%???}' "${variable%???}";

    eval-echo "variable='ABCD'";
    echo -n 'Suppression du dernier caractere si D:'; display-vars '${variable%D}'   "${variable%D}";
    return 0;
}

addLibrairie 'suppressionNcar';
function      suppressionNcar(){
    local _txt='';

    titre3 'Suppression n caractères';

    _txt='fichier.jpeg';    display-vars '_txt' "$_txt" '${_txt: -4}' "${_txt: -4}";
    _txt='repertoire/';     display-vars '_txt' "$_txt" '${_txt: 4}'  "${_txt: 4}";
    return 0;
}


addLibrairie 'text-subString';
function      text-subString(){
    local _txt='';

    titre3 "Recherche d'une sous chaine";

    _txt='fichier.jpeg';    display-vars '_txt' "$_txt" '${_txt:0:6}' "${_txt:0:7}";  # debut:longueur    # 'fichier'
    _txt='fichier.jpeg';    display-vars '_txt' "$_txt" '${_txt:1:6}' "${_txt:8:4}";  # debut:longueur    # 'jpeg'

    _txt='fichier.jpeg';    display-vars '_txt:${#_txt}' "$_txt:${#_txt}" '${_txt:((${#_txt} - 1)):1}' "${_txt:((${#_txt} - 1)):1}";  # dernier caractere ?'
    return 0;
    
}

addLibrairie 'text-minMAJ';
function      text-minMAJ(){
    local _txt='';

    #titreInfo "ATTENTION: Un texte ne peut pas etre substitué directement il faut passer par une variable!";
    
    #titre3 "UPPERCASE et lowercase";

    local _var='de la minuscule à la MAJUSCULE';
    display-vars '_var' "$_var";

    titreCmd '${_var^^}';       echo   "${_var^^}";
    titreCmd '${_var^^}';       echo   "${_var^^e};    # camelecase avec 1 caractères spécifique";
    titreCmd '${_var^^}';       echo   "${_var^^[el]}; # camelecase avec plusieurs caractères spécifiques";

    titreCmd '${_var^} ';        echo   "${_var^}"; # Premiere lettre en majuscule
    titreCmd '${_var^d}';       echo   "${_var^d};# Premiere lettre en majuscule si c'est un d"; 
    titreCmd '${_var^m}';       echo   "${_var^m};# Premiere lettre en majuscule si c'est un m"; 
    titreCmd '${_var,,}';       echo   "${_var,,}";

    echo '';
    titreCmd "local _var='De la minuscule à la MAJUSCULE et reciproquement.';"
    local _var='De la minuscule à la MAJUSCULE';
    titreCmd '${_var,,}';       echo   "${_var,,}";
    titreCmd '${_var,} ';       echo   "${_var,}";
    titreCmd '${_var,d}';       echo   "${_var,d}; # Ne fonctionne pas.";
    titreCmd '${_var,m}';       echo   "${_var,m}; # Ne fonctionne pas.";

    return 0;
}