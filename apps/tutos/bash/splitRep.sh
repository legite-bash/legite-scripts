echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

local _source_pathname="${BASH_SOURCE[0]}";         display-vars '_source_pathname' "$_source_pathname";
local _source_fullRep="${_source_pathname%/*.*}";   display-vars '_source_fullRep ' "$_source_fullRep";
local _source_lastRep="${_source_fullRep##*/}";     display-vars '_source_lastRep ' "$_source_lastRep";
local _source_name="${_source_pathname##*/}";       display-vars '_source_name    ' "$_source_name";
local _source_nom="${_source_name%.*}";             display-vars '_source_nom     ' "$_source_nom";
local _source_ext="${_source_name##*.}";            display-vars '_source_ext     ' "$_source_ext";

# extraction des sous repertoire
# splitter les repertoires

local -a repTbl=();
local reps="rep1/rep2/rep3";
IFS='/';read -ra repTbl <<< "$reps";IFS="$IFS_DEFAULT";

displayTableau 'repTbl' ""${repTbl[@]}"";

for rep in "${repTbl[@]}"
do
    display-vars 'rep' "$rep";
done

# appeller chaque rep en ajoutant le sprecedent
local _repCumul='ROOT';
for rep in "${repTbl[@]}"
do
    _repCumul+="/$rep";
    display-vars '_repCumul' "$_repCumul";
done
