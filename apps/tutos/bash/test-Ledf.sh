echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

local _source_pathname="${BASH_SOURCE[0]}";         display-vars '_source_pathname' "$_source_pathname";
local _source_fullRep="${_source_pathname%/*.*}";   display-vars '_source_fullRep ' "$_source_fullRep";
local _source_lastRep="${_source_fullRep##*/}";     display-vars '_source_lastRep ' "$_source_lastRep";
local _source_name="${_source_pathname##*/}";       display-vars '_source_name    ' "$_source_name";
local _source_nom="${_source_name%.*}";             display-vars '_source_nom     ' "$_source_nom";
local _source_ext="${_source_name##*.}";            display-vars '_source_ext     ' "$_source_ext";


# reférences:
# https://www.web-development-kb-eu.site/fr/bash/comment-verifier-si-un-lien-symbolique-existe/972977430/

function creerFichierstest(){
    if ! mkdir -p "$tmp_rep/Ledf"
    then
        erreurs-pile-add-echo "Impossible d'aller dans le répertoire temporaire '$TEMP_ROOT'";
        return $E_INODE_NOT_EXIST;
    fi
    cd "$tmp_rep/Ledf";

    mkdir "repertoire";
    ln -s "repertoire" "repertoire-ln";
    mkdir "repertoire_inexist"
    ln -s "repertoire_inexist/" "repertoire_inexist-ln";
    eval-echo 'rmdir "repertoire_inexist"';
    display-vars '$?' "$?";
    touch "fichier_simple.txt";
    ln -s "fichier_simple.txt" "fichier_simple-ln.txt";
    link  "fichier_simple.txt" "fichier_simple-link.txt";

    touch "fichier_inexist.txt";
    ln -s "fichier_inexist.txt" "fichier_inexist-ln.txt";
    link  "fichier_inexist.txt" "fichier_inexist-link.txt";
    rm    "fichier_inexist.txt";
    return 0;
}

############
# - MAIN - #
############
titre1 "$_source_fullname\nTest le type de fichier avec Ledf";

if ! creerFichierstest; then return $E_FALSE;fi

ls -l
local _inode='';


################
titre2 "Test -e"
################
    echo "VRAI si repertoire/fichier/lien existants";

    _inode='repertoire';
    titreCodeInline 'if [ -e '"$_inode"' ]'"\t"
    if                  [ -e "$_inode" ]
    then echo "VRAI: existe.";
    else echo "FAUX: N'existe PAS";
    fi

    _inode='repertoire-ln';
    titreCodeInline 'if [ -e '"$_inode"' ]'"\t"
    if                  [ -e "$_inode" ]
    then echo "VRAI: existe.";
    else echo "FAUX: N'existe PAS";
    fi

    _inode='repertoire_inexist-ln';
    titreCodeInline 'if [ -e '"$_inode"' ]'"\t"
    if                  [ -e "$_inode" ]
    then echo "VRAI: existe.";
    else echo "FAUX: N'existe PAS";
    fi

    _inode='fichier_simple.txt';
    titreCodeInline 'if [ -e '"$_inode"' ]'"\t"
    if                  [ -e "$_inode" ]
    then echo "VRAI: existe.";
    else echo "FAUX: N'existe PAS";
    fi

    _inode="fichier_simple-ln.txt";
    titreCodeInline 'if [ -e '"$_inode"' ]'"\t"
    if                  [ -e "$_inode" ]
    then echo "VRAI: existe.";
    else echo "FAUX: N'existe PAS";
    fi

    _inode="fichier_simple-link.txt";
    titreCodeInline 'if [ -e '"$_inode"' ]'"\t"
    if                  [ -e "$_inode" ]
    then echo "VRAI: existe.";
    else echo "FAUX: N'existe PAS";
    fi


    _inode='fichier_inexist.txt';
    titreCodeInline 'if [ -e '"$_inode"' ]'"\t"
    if                  [ -e "$_inode" ]
    then echo "VRAI: existe.";
    else echo "FAUX: N'existe PAS";
    fi

    _inode='fichier_inexist-ln.txt';
    titreCodeInline 'if [ -e '"$_inode"' ]'"\t"
    if                  [ -e "$_inode" ]
    then echo "VRAI: existe.";
    else echo "FAUX: N'existe PAS";
    fi

    _inode='fichier_inexist-link.txt';
    titreCodeInline 'if [ -e '"$_inode"' ]'"\t"
    if                  [ -e "$_inode" ]
    then echo "VRAI: existe.";
    else echo "FAUX: N'existe PAS";
    fi
#


################
titre2 "Test -d"
################
    echo "VRAI si repertoire oulien vers répertoire existant"
    _inode='repertoire';
    titreCodeInline 'if [ -d '"$_inode"' ]'"\t"
    if                  [ -d "$_inode" ]
    then echo "VRAI: EST un répertoire.";
    else echo "FAUX: N'est PAS un répertoire";
    fi

    _inode='repertoireInexiste';
    titreCodeInline 'if [ -d '"$_inode"' ]'"\t"
    if                  [ -d "$_inode" ]
    then echo "VRAI: EST un répertoire.";
    else echo "FAUX: N'est PAS un répertoire";
    fi

    _inode='repertoire-ln';
    titreCodeInline 'if [ -d '"$_inode"' ]'"\t"
    if                  [ -d "$_inode" ]
    then echo "VRAI: EST un répertoire.";
    else echo "FAUX: N'est PAS un répertoire";
    fi

    _inode='repertoire_inexist-ln';
    titreCodeInline 'if [ -d '"$_inode"' ]'"\t"
    if                  [ -d "$_inode" ]
    then echo "VRAI: existe.";
    else echo "FAUX: N'existe PAS";
    fi


    _inode='fichier_simple.txt';
    titreCodeInline 'if [ -d '"$_inode"' ]'"\t"
    if                  [ -d "$_inode" ]
    then echo "VRAI: EST un répertoire.";
    else echo "FAUX: N'est PAS un répertoire";
    fi
#


################
titre2 "Test -f"
################
    echo "VRAI si fichier ou lien vers fichier existant"
    _inode='repertoire';
    titreCodeInline 'if [ -f '"$_inode"' ]'"\t"
    if                  [ -f "$_inode" ]
    then echo "VRAI: EST un fichier.";
    else echo "FAUX: N'est PAS un fichier";
    fi

    _inode='repertoire-ln';
    titreCodeInline 'if [ -f '"$_inode"' ]'"\t"
    if                  [ -f "$_inode" ]
    then echo "VRAI: EST un fichier.";
    else echo "FAUX: N'est PAS un fichier";
    fi

    _inode='repertoire-ln';
    titreCodeInline 'if [ -f '"$_inode"' ]'"\t"
    if                  [ -f "$_inode" ]
    then echo "VRAI: EST un répertoire.";
    else echo "FAUX: N'est PAS un répertoire";
    fi

    _inode='repertoire_inexist-ln';
    titreCodeInline 'if [ -f '"$_inode"' ]'"\t"
    if                  [ -f "$_inode" ]
    then echo "VRAI: existe.";
    else echo "FAUX: N'existe PAS";
    fi


    _inode='fichier_simple.txt';
    titreCodeInline 'if [ -f '"$_inode"' ]'"\t"
    if                  [ -f "$_inode" ]
    then echo "VRAI: EST un fichier.";
    else echo "FAUX: N'est PAS un fichier";
    fi

    _inode="fichier_simple-ln.txt";
    titreCodeInline 'if [ -f '"$_inode"' ]'"\t"
    if                  [ -f "$_inode" ]
    then echo "VRAI: EST un fichier.";
    else echo "FAUX: N'est PAS un fichier";
    fi

    _inode="fichier_simple-link.txt";
    titreCodeInline 'if [ -f '"$_inode"' ]'"\t"
    if                  [ -f "$_inode" ]
    then echo "VRAI: EST un fichier.";
    else echo "FAUX: N'est PAS un fichier";
    fi


    _inode='fichier_inexist.txt';
    titreCodeInline 'if [ -f '"$_inode"' ]'"\t"
    if                  [ -f "$_inode" ]
    then echo "VRAI: EST un fichier.";
    else echo "FAUX: N'est PAS un fichier";
    fi

    _inode='fichier_inexist-ln.txt';
    titreCodeInline 'if [ -f '"$_inode"' ]'"\t"
    if                  [ -f "$_inode" ]
    then echo "VRAI: EST un fichier.";
    else echo "FAUX: N'est PAS un fichier";

    fi

    _inode='fichier_inexist-link.txt';
    titreCodeInline 'if [ -f '"$_inode"' ]'"\t"
    if                  [ -f "$_inode" ]
    then echo "VRAI: EST un fichier.";
    else echo "FAUX: N'est PAS un fichier";
    fi
#


################
titre2 "Test -L"
################
    echo "VRAI si fichier ou lien vers fichier existant"
    _inode='repertoire';
    titreCodeInline 'if [ -L '"$_inode"' ]'"\t"
    if                  [ -L "$_inode" ]
    then echo "VRAI: EST un lien vers un répertoire existant.";
    else echo "FAUX: N'est PAS un lien.";
    fi

    _inode='repertoire_inexist';
    titreCodeInline 'if [ -L '"$_inode"' ]'"\t"
    if                  [ -L "$_inode" ]
    then echo "VRAI: EST un lien vers un répertoire NON existant.";
    else echo "FAUX";
    fi

    _inode='repertoire-ln';
    titreCodeInline 'if [ -L '"$_inode"' ]'"\t"
    if                  [ -L "$_inode" ]
    then echo "VRAI: EST un lien vers un répertoire existant.";
    else echo "FAUX: N'est PAS un lien.";
    fi

    _inode='repertoire_inexist-ln';
    titreCodeInline 'if [ -L '"$_inode"' ]'"\t"
    if                  [ -L "$_inode" ]
    then echo "VRAI. Mais la cible n'existe pas!";  # -> réponse
    else echo "FAUX";
    fi

    _inode='fichier_simple.txt';
    titreCodeInline 'if [ -L '"$_inode"' ]'"\t"
    if                  [ -L "$_inode" ]
    then echo "VRAI: EST un lien vers un fichier existant.";
    else echo "FAUX: N'est PAS un lien vers un fichier existant";
    fi

    _inode="fichier_simple-ln.txt";
    titreCodeInline 'if [ -L '"$_inode"' ]'"\t"
    if                  [ -L "$_inode" ]
    then echo "VRAI: EST un lien vers un fichier existant.";
    else echo "FAUX: N'est PAS un lien vers un fichier existant";
    fi

    _inode="fichier_simple-link.txt";
    titreCodeInline 'if [ -L '"$_inode"' ]'"\t"
    if                  [ -L "$_inode" ]
    then echo "VRAI: EST un lien vers un fichier existant.";
    else echo "FAUX: N'est PAS un lien";
    fi


    _inode='fichier_inexist.txt';
    titreCodeInline 'if [ -L '"$_inode"' ]'"\t"
    if                  [ -L "$_inode" ]
    then echo "VRAI: EST un lien.";
    else echo "FAUX: N'est PAS un lien";
    fi

    _inode='fichier_inexist-ln.txt';
    titreCodeInline 'if [ -L '"$_inode"' ]'"\t"
    if                  [ -L "$_inode" ]
    then echo "VRAI: EST un lien vers un fichier inexistant.";
    else echo "FAUX: N'est PAS un lien (vers un fichier existant)";
    fi

    _inode='fichier_inexist-link.txt';
    titreCodeInline 'if [ -L '"$_inode"' ]'"\t"
    if                  [ -L "$_inode" ]
    then echo "VRAI: EST un lien vers un fichier existant.";
    else echo "FAUX: N'est PAS un lien (vers un inode existant)";
    fi
#


#######
# END #
#######
echo-d "$BASH_SOURCE:END";

return 0;