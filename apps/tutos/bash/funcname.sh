echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

local _source_pathname="${BASH_SOURCE[0]}";         display-vars '_source_pathname' "$_source_pathname";
local _source_fullRep="${_source_pathname%/*.*}";   display-vars '_source_fullRep ' "$_source_fullRep";
local _source_lastRep="${_source_fullRep##*/}";     display-vars '_source_lastRep ' "$_source_lastRep";
local _source_name="${_source_pathname##*/}";       display-vars '_source_name    ' "$_source_name";
local _source_nom="${_source_name%.*}";             display-vars '_source_nom     ' "$_source_nom";
local _source_ext="${_source_name##*.}";            display-vars '_source_ext     ' "$_source_ext";


#addLibrairie '_test-funcname': # pas d'execution -Auto
function       _test-funcname(){
}


addLibrairie 'test-funcname';
function      test-funcname(){

    function funcname1(){
        fctIn "$*";
        display-vars '${FUNCNAME[@]}' "${FUNCNAME[@]}"
        display-vars '$FUNCNAME' "$FUNCNAME"
        display-vars '${FUNCNAME[0]}' "${FUNCNAME[0]}"
        display-vars '${FUNCNAME[1]}' "${FUNCNAME[1]}"
        echo '$@' "$@"
        echo '$*' "$*"
        fctOut "$FUNCNAME(0)";return 0;
    }
    function funcname2(){
        fctIn "$*";
        echo '$@' "$@"
        echo '$*' "$*"
        fctOut "$FUNCNAME(0)";return 0;
    }

    funcname1 'F1P1' 'F1 P2'
    funcname2 'F2PA' 'F2 PB'
    return 0;
}

