echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


local _source_pathname="${BASH_SOURCE[0]}";         display-vars '_source_pathname' "$_source_pathname";
local _source_fullRep="${_source_pathname%/*.*}";   display-vars '_source_fullRep ' "$_source_fullRep";
local _source_lastRep="${_source_fullRep##*/}";     display-vars '_source_lastRep ' "$_source_lastRep";
local _source_name="${_source_pathname##*/}";       display-vars '_source_name    ' "$_source_name";
local _source_nom="${_source_name%.*}";             display-vars '_source_nom     ' "$_source_nom";
local _source_ext="${_source_name##*.}";            display-vars '_source_ext     ' "$_source_ext";


#############
# -  MAIN - #
#############
titre1 "$_source_fullname";

titreInfo "https://www.gnu.org/software/bash/manual/html_node/Programmable-Completion-Builtins.html#Programmable-Completion-Builtins";
titreInfo "https://opensource.com/article/18/3/creating-bash-completion-script";

titreInfo "https://github.com/git/git/blob/master/contrib/completion/git-completion.bash";
titreInfo "https://www.gnu.org/software/bash/manual/html_node/Programmable-Completion.html#Programmable-Completion";
titreInfo "https://www.gnu.org/software/bash/manual/html_node/Programmable-Completion-Builtins.html#Programmable-Completion-Builtins";
titreInfo "https://www.gnu.org/software/bash/manual/html_node/A-Programmable-Completion-Example.html#A-Programmable-Completion-Example";
titreInfo "https://www.gnu.org/software/bash/manual/html_node/Bash-Variables.html#Bash-Variables";
titreInfo "https://www.thegeekstuff.com/2013/12/bash-completion-complete/";



#######
# END #
#######
echo-d "${BASH_SOURCE[0]}:END" 1;
return 0;