echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

local _source_pathname="${BASH_SOURCE[0]}";         display-vars '_source_pathname' "$_source_pathname";
local _source_fullRep="${_source_pathname%/*.*}";   display-vars '_source_fullRep ' "$_source_fullRep";
local _source_lastRep="${_source_fullRep##*/}";     display-vars '_source_lastRep ' "$_source_lastRep";
local _source_name="${_source_pathname##*/}";       display-vars '_source_name    ' "$_source_name";
local _source_nom="${_source_name%.*}";             display-vars '_source_nom     ' "$_source_nom";
local _source_ext="${_source_name##*.}";            display-vars '_source_ext     ' "$_source_ext";


titre1 "if Bash";
titreInfo "";
#display-vars '' "";

local -i _chiffre1=1000;  display-vars '_chiffre1' "$_chiffre1";
local -i _chiffre2=50;    display-vars '_chiffre2' "$_chiffre2";
local    _txtA="texte A";
local    _txtB="texte B";

eval-echo "function vrai() { return 0;}";
eval-echo "function faux() { return $E_FALSE; }";

titre2 "Equivalence: vrai/true et faux/false";
    echo "vrai se comporte comme true";
    echo "faux se comporte comme false";


titre2 "Interprétation des résultats vrai/faux";
echo "vrai: c'est l'embranchement 'then' qui est activée";
echo "faux: c'est l'embranchement 'else' qui est activée";

titre3 "[ vrai = true ]";
if [ vrai = true ]
then echo 'vrai'
else echo 'faux'
fi


titre2 " tests par résultats";
    # -- vrai -- #
    titre3 "vrai/true";

        echo -n "if vrai";
        if vrai;            then echo ': vrai';else echo ': faux'; fi
        echo -n "if true";
        if true;            then echo ': vrai';else echo ': faux'; fi


        echo -n "if [ vrai ]";
        if [ vrai ];        then echo ': vrai';else echo ': faux'; fi
        echo -n "if [ true ]";
        if [ true ];        then echo ': vrai';else echo ': faux'; fi


        echo -n "if ! vrai";
        if ! vrai;          then echo ': vrai';else echo ': faux'; fi
        echo -n "if ! true";
        if ! true;          then echo ': vrai';else echo ': faux'; fi

        echo -n "if [ ! vrai ] ";
        if [ ! vrai ];      then echo ': vrai';else echo ': faux'; fi
        echo -n "if [ ! true ] ";
        if [ ! true ];      then echo ': vrai';else echo ': faux'; fi

        echo "if vrai = faux: ${COLOR_WARN}Syntaxe invalide"$COLOR_NORMAL;

        echo -n "if [ vrai = vrai ] ";
        if [ vrai = vrai ]; then echo ': vrai';else echo ': faux'; fi
        echo -n "if [ true = true ] ";
        if [ true = true ]; then echo ': vrai';else echo ': faux'; fi

        echo -n "if [ vrai = faux ] ";
        if [ vrai = faux ]; then echo ': vrai';else echo ': faux'; fi
        echo -n "if [ true = false ] ";
        if [ true = false ];  then echo ': vrai';else echo ': faux'; fi

    #

    # -- faux/false -- #
        titre3 "faux/false";
        echo -n "if faux";
        if faux;            then echo ': vrai';else echo ': faux'; fi
        echo -n "if false";
        if false;           then echo ': vrai';else echo ': faux'; fi


        echo -n "if [ faux ]";
        if [ faux ];        then echo ': vrai';else echo ': faux'; fi
        echo -n "if [ false ]";
        if [ false ];       then echo ': vrai';else echo ': faux'; fi


        echo -n "if ! faux";
        if ! faux;          then echo ': vrai';else echo ': faux'; fi
        echo -n "if ! false";
        if ! false;          then echo ': vrai';else echo ': faux'; fi

        echo -n "if [ ! faux ] ";
        if [ ! faux ];      then echo ': vrai';else echo ': faux'; fi
        echo -n "if [ ! false ] ";
        if [ ! false ];      then echo ': vrai';else echo ': faux'; fi

        echo "if vrai = faux: ${COLOR_WARN}Syntaxe invalide"$COLOR_NORMAL;

        echo -n "if [ vrai = vrai ] ";
        if [ vrai = vrai ]; then echo ': vrai';else echo ': faux'; fi
        echo -n "if [ true = true ] ";
        if [ true = true ]; then echo ': vrai';else echo ': faux'; fi


    #
#

titre2 " tests par constructions";

titre2 "ATTENTION: if true/!true/false/!false et [ true/!true/false/!false ]";

    echo "Les construction if   false et if [   false ] ne renvoie pas le même résultat."
    echo "Les construction if ! false et if [ ! false ] ne renvoie pas le même résultat."

    #titre3 "Construction: if false/ [ false ]";

        echo -n " if   false";
        if false
        then
            echo -n ': vrai';  
        else
            echo -n ': faux';   # renvoi faux
        fi

        echo -n " if [   false ]";
        if [ false ]
        then
            echo ': vrai';      # renvoie vrai
        else
            echo ': faux';
        fi

    #titre3 "Construction: if ! false / [ ! false ]";
        echo -n " if ! false";
        if ! false 
        then
            echo -n ': vrai';  # renvoi vrai
        else
            echo -n ': faux';
        fi

        echo -n " if [ ! false ]";
        if [ ! false ]
        then
            echo ': vrai';
        else
            echo ': faux';    # renvoie faux
        fi
    #

    titre2 'Ecriture similaire: "if prog" et "programme; if [ $? -eq 0 ]"';

    titre3 "if programme";
        echo 'if programme';
        echo "then #pas d'erreur (=0)";
        echo "else #le programme a renvoyé une ereur (!=0)";
        echo 'fi';

    titre3 "Exemple: if cd /tmp";
        if cd /tmp
        then echo "(then)Changement de repertoire ok";
        else echo "(else)Erreur lors du changement de repertoire.";
        fi

    titre3 "Exemple: if cd /repertoireInexistant";
        if cd /repertoireInexistant
        then echo "(then)Changement de repertoire ok";
        else echo "(else)Erreur lors du changement de repertoire.";
        fi

    echo '';
    cd /;
    titre3 "programme; if [ $? -eq 0 ]";
        echo 'programme; if [ $? -eq 0 ]';
        echo " if [ $? -eq 0 ]"
        echo "then #pas d'erreur (=0)";
        echo "else #le programme a renvoyé une ereur (!=0)";
        echo 'fi';

    titre3 "Exemple: cd /tmp; if [ $? -eq 0 ]";
        cd /tmp;
        if [ $? -eq 0 ]
        then echo "(then)Changement de repertoire ok";
        else echo "(else)Erreur lors du changement de repertoire.";
        fi

    titre3 "Exemple: if cd /repertoireInexistant; if [ $? -eq 0 ]";
        cd /repertoireInexistant;
        if [ $? -eq 0 ]
        then echo "(then)Changement de repertoire ok";
        else echo "(else)Erreur lors du changement de repertoire.";
        fi




#