echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

local _source_pathname="${BASH_SOURCE[0]}";         display-vars '_source_pathname' "$_source_pathname";
local _source_fullRep="${_source_pathname%/*.*}";   display-vars '_source_fullRep ' "$_source_fullRep";
local _source_lastRep="${_source_fullRep##*/}";     display-vars '_source_lastRep ' "$_source_lastRep";
local _source_name="${_source_pathname##*/}";       display-vars '_source_name    ' "$_source_name";
local _source_nom="${_source_name%.*}";             display-vars '_source_nom     ' "$_source_nom";
local _source_ext="${_source_name##*.}";            display-vars '_source_ext     ' "$_source_ext";


######################
## Les déclarations ##
######################


titre2 'Declaration de constantes'
    eval-echo "declare -r constanteNonType='variable de test'"
    eval-echo "readonly varEnLectureSeul=10"

titre2 'Declaration d une variable non typée'
    eval-echo "declare varNonType"

titre2 'Declaration d une [constante] variable integer'
    eval-echo "declare [-r] -i varInt"


titre2 "Declaration d'un tableau"
    eval-echo "declare -a tableauOrdonnee=()";
    echo "'=()' initialise le tableau vide avec zéro élément. ${#tableauOrdonnee} est donc égal à 0. Sinon il est non défini et provoque une erreur."

titre2 "Declaration d'un tableau associatif"
    eval-echo "declare -A tableauAssociatif=()"


titre2 "Initialiser une variable avec une autre ou une valeur par défaut si vide"
    eval-echo "declare varExistant='varExist'";
    eval-echo "heriteOuDef=${varExistant:-'valeurParDefaut'}"
    display-vars 'heriteOuDef' "$heriteOuDef"

titre2 'Vérifier si une variable est vide ou non'
    echo 'if [ -z ${varATester+inconnu} ]';
    if [ -z ${varATester+inconnu} ]
    then echo "var is unset";
    else echo "var is set to '$var'"
    fi
    titreWarn "Attention: si la variable est non définie cela provoque une erreur avec set -u";
    echo "Utiliser de préférence la forme précédente ";


titre2 'Vérifier si un parametre est défini';
    echo 'if [ -z ${2+x} ]'
    if [ -z ${2+x} ]
    then echo "parametre 2 is unset";
    else echo "parametre 2 is set to '$2'";
    fi

titre2  'chaine vide';
    eval-echo "chaineVide='';"
    echo 'if [ "$chaineVide" == "" ]'
    if [ "$chaineVide" == "" ];
    then echo "$chaineVide est vide";
    else echo "$chaineVide est PAS vide";
    fi


titre1 'Comparaisons'

titre2 "Chaine egales"
    a="aaa";
    if [ "$a" == "$a" ]
    then
        echo "equivalence avec '=='"
    fi

    if [ "$a" = "$a" ]
    then
        echo "equivalence avec '='"
    fi


titre1 'operateur ternaire'
    eval-echo '[ "$(id -u)" = "0" ] && isRoot=1 || isRoot=0'



titre1 "Portabilité d'une variable local dans une sous fonction"

echo ""
TITRE1 "type"
function test_type(){
    function test_fonction(){
        echo "ceci est une fonction"
    }
    # https://bash.cyberciti.biz/guide/Type_command
    #type varA  # ne fonctionne pas avec les variables
    #echo "type test_fonction"=$(type test_fonction)
    type -t test_fonction
}
test_type

function fonctionExist(){
    echo $LINENO 'oui'
}

if [ "$(type -t fonctionExist)" = 'function' ]
then
    echo $LINENO "oui"
fi


titre1 'isNumeric';

#function isNumeric(){
#    local -i numeric=$1
#    local errNu=$?
#    display-vars "errNu" "$errNu" "numeric" "$numeric"
#}

function testIsNumeric(){
    isNumeric 1000
    isNumeric "1001"
    isNumeric "NoInteger"
}
#testIsNumeric






