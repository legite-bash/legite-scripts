echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

#################
# * LOGICIELS * #
#################

addLibrairie 'install-discord' 'Install discord(ne fonctionne pas avec Debian 11)'
function      install-discord(){
    titre3 'Installation de Discord'
    if [ $IS_ROOT  = false ];then titreWarn "$FUNCNAME doit être lancé en root!"; fctOut "$FUNCNAME($E_NOT_ROOT)"; return $E_NOT_ROOT; fi

    titreInfo "DEBIAN VERSION: $DEBIAN_VERSION"
    if [ $DEBIAN_VERSION_MAJOR -eq 10 ]
    then
        evalCmd "aptitude -y install libc6 libasound2 libatomic1 libgconf-2-4  libnotify4  libnspr4  libnss3  libstdc++6  libxss1  libxtst6  libappindicator1 libc++1";
    elif [ $DEBIAN_VERSION_MAJOR -eq 11 ]
    then
        eval-echo "aptitude -y install libc6 libasound2 libatomic1 libgconf-2-4  libnotify4  libnspr4  libnss3  libstdc++6  libxss1  libxtst6   libayatana-appindicator1 libc++1";
    fi
    eval-echo "wget -O '/dev/shm/discord.deb' 'https://discord.com/api/download?platform=linux&format=deb'"
    eval-echo "dpkg -i /dev/shm/discord.deb"
    rm '/dev/shmmp/discord.deb';

    return 0;
}
