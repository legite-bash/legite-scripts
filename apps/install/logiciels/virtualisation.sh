echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

#################
# * LOGICIELS * #
#################

function signer_module(){
    readonly hash_algo='sha256'
    readonly key='/root/module-signing/MOK.priv'
    readonly x509='/root/module-signing/MOK.der'

    #readonly name="$(basename $0)"
    readonly name="$FUNCTION";

    readonly esc='\\e'
    readonly reset="${esc}[0m"

    green() { local string="${1}"; echo "${esc}[32m${string}${reset}"; }
    blue()  { local string="${1}"; echo "${esc}[34m${string}${reset}"; }
    log()   { local string="${1}"; echo "[$(blue $name)] ${string}"; }

    # The exact location of `sign-file` might vary depending on your platform.
    #alias sign-file="/usr/src/kernels/$(uname -r)/scripts/sign-file"
    alias sign-file="/usr/src/linux-kbuild-6.5.0-0.deb12.4/scripts/sign-file";

    # Récupération/Création du mot de passe
    [ -z "${KBUILD_SIGN_PIN}" ] && read -p "Passphrase for ${key}: " KBUILD_SIGN_PIN
    export KBUILD_SIGN_PIN

    for module in $(dirname $(modinfo -n vboxdrv))/*.ko; do
    log "Signing $(green ${module})..."
    sign-file "${hash_algo}" "${key}" "${x509}" "${module}"
    done

    return 0;
}


addLibrairie 'install-virtualBox-7-0'
function      install-virtualBox-7-0(){
    titre3 'Installation de VirtualBox 7.0'
    if [ $IS_ROOT = false ];then titreWarn "Le paquet doit être installé en root!"fctOut "${FUNCNAME[0]}($E_NOT_ROOT)"; return $E_NOT_ROOT; fi

    titreInfo 'Doc: https://www.virtualbox.org/wiki/Linux_Downloads'

    # - Ajout de la clef gpg - #
    titre4 'Ajout de la clef gpg'
    wget -O- https://www.virtualbox.org/download/oracle_vbox_2016.asc | sudo gpg --yes --output /usr/share/keyrings/oracle-virtualbox-2016.gpg --dearmor

    # - Ajout du depot - #
    titre4 'Ajout du depot'
    sourceListFile="/etc/apt/sources.list.d/virtualbox.list";
    if [ ! -f "$sourceListFile" ];
    then
        #eval-echo "echo 'deb https://download.virtualbox.org/virtualbox/debian bullseye contrib non-free' | tee --append $sourceListFile"
        eval-echo "echo 'deb https://download.virtualbox.org/virtualbox/debian bookworm contrib non-free' | tee --append $sourceListFile"
    else    
        echo "Le depot $sourceListFile existe deja"
        echo "${INFO} rm $sourceListFile #pour supprimer l'ancienne version${NORMAL}"
    fi

    # - Installation du paquet - #
    titre4 'Installation du paquet'
    eval-echo "aptitude update"
    eval-echo "aptitude install virtualbox-7.0"
    
    echo "UEFI + MOK"

    echo "Creer une clef MOK"
    mkdir /root/UEFI;
    #eval-echo 'openssl req -new -x509 -newkey rsa:2048 -keyout /root/UEFI/MOK.priv -outform DER -out /root/UEFI/MOK.der -days 36500 -subj "/CN=LeGite/" -nodes;';
    eval-echo 'openssl req -new -x509 -newkey rsa:2048 -nodes -keyout /root/UEFI/MOK2.priv -outform DER -out /root/UEFI/MOK2.der -days 36500 -subj "/CN=LeGite/" ;';
    #-nodes = no passhrase

    # importer la clef MOK
    eval-echo "mokutil --import /root/UEFI/MOK2.der";


    echo "https://www.lilian-benoit.fr/2020/02/SecureBoot-Signer-ses-modules-et-pourquoi.html"

    # signer les modules
    #/usr/src/linux-kbuild-6.5.0-0.deb12.4/scripts/sign-file rsa:2048 ./UEFI/MOK.priv ./UEFI/MOK.der $(modinfo -n vboxdrv)
    #/usr/src/linux-kbuild-6.5.0-0.deb12.4/scripts/sign-file rsa:2048 ./UEFI/MOK.priv ./UEFI/MOK.der $(modinfo -n vboxnetflt)
    #/usr/src/linux-kbuild-6.5.0-0.deb12.4/scripts/sign-file rsa:2048 ./UEFI/MOK.priv ./UEFI/MOK.der $(modinfo -n vboxnetadp)
    /usr/src/linux-kbuild-6.5.0-0.deb12.4/scripts/sign-file rsa:2048 ./UEFI/MOK2.priv ./UEFI/MOK2.der $(modinfo -n vboxdrv)
    /usr/src/linux-kbuild-6.5.0-0.deb12.4/scripts/sign-file rsa:2048 ./UEFI/MOK2.priv ./UEFI/MOK2.der $(modinfo -n vboxnetflt)
    /usr/src/linux-kbuild-6.5.0-0.deb12.4/scripts/sign-file rsa:2048 ./UEFI/MOK2.priv ./UEFI/MOK2.der $(modinfo -n vboxnetadp)

    eval-echo "/sbin/vboxconfig"
    # aptitude install virtualbox-guest-additions-iso

    return 0;
}


#addLibrairie 'install-virtualBox-6-1'
function      install-virtualBox-6-1(){
    titre3 'Installation de VirtualBox'
    if [ $IS_ROOT = false ];then titreWarn "Le paquet doit être installé en root!"; return $E_NOT_ROOT; fi

    titreInfo 'Doc: https://www.virtualbox.org/wiki/Linux_Downloads'

    # - Ajout de la clef gpg - #
    titre4 'Ajout de la clef gpg'
        eval-echo "wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | apt-key add -"
        eval-echo "wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | apt-key add -"

    # - Ajout du depot - #
    titre4 'Ajout du depot'
    sourceListFile="/etc/apt/sources.list.d/virtualbox.list";
    if [ ! -f "$sourceListFile" ];
    then
        eval-echo "echo 'deb https://download.virtualbox.org/virtualbox/debian bookworm contrib non-free' | tee --append $sourceListFile"
    else    
        echo "Le depot $sourceListFile existe deja"
        echo "${INFO} rm $sourceListFile #pour supprimer l'ancienne version${NORMAL}"
    fi

    # - Installation du paquet - #
    titre4 'Installation du paquet'
    eval-echo "aptitude update"
    eval-echo "aptitude install virtualbox-6.1"
    return 0;
}


#addLibrairie 'install-virtualBox-6-1'
function      install-docker(){
    titre3 'Installation de Docker'
    aptitude_install "docker docker-clean docker-compose docker-doc"
    echo "Modifier le repertoire de Docker";
    echo "https://blog.stephane-robert.info/docs/conteneurs/moteurs-conteneurs/docker-bonnes-pratiques/";
    echo "https://docs.docker.com/desktop/settings/linux/#docker-engine"
    echo "https://docs.docker.com/desktop/settings/linux/#docker-engine"
    echo "https://docs.docker.com/engine/reference/commandline/dockerd/"
    echo ""
    echo ""
    echo ""
    echo ""
    docker-create-daemon.json;
}

addLibrairie 'docker-create-daemon.json' 'Creation du fichier /etc/docker/daemon.json';
function      docker-create-daemon.json() {
    if [ -f '/etc/docker/daemon.json' ]
    then
        mv /etc/docker/daemon.json /etc/docker/daemon.json.bak
    fi

    echo <<EOL
    {
  "builder": {
    "gc": {
      "defaultKeepStorage": "20GB",
      "enabled": true
    }
  },
  "experimental": false
 }
EOL
 > /etc/docker/daemon.json

}


#gtt.sh apps/install/logiciels/virtualisation.sh install-wine
addLibrairie 'install-wine' "Installe wine";
function      install-wine(){
    titre3 'Installation de VirtualBox'
    if [ $IS_ROOT = false ];then titreWarn "Le paquet doit être installé en root!"fctOut "${FUNCNAME[0]}($E_NOT_ROOT)"; return $E_NOT_ROOT; fi

    aptitude_install 'wine wine64 wine32 winbind winetricks';

}
