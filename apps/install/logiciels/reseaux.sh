echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.10.16
# Description: 

addLibrairie 'install-unbound';
function      install-unbound() {
    titre1 'Installation de unbound'
    if [ $IS_ROOT = false ];then titreWarn "Le paquet doit être installé en root!"fctOut "${FUNCNAME[0]}($E_NOT_ROOT)"; return $E_NOT_ROOT; fi
    aptitude-install "unbound unbound-anchor ";

    # problème: 
    # unbound[4675:0] warning: so-rcvbuf 1048576 was not granted. Got 425984. To fix
    # https://github.com/pi-hole/docs/issues/539
    sysctl -w net.core.rmem_max=1048576;    # taille memoire à adapté au systeme (ok pour msi)

    titreInfo 'Doc: ';

    return 0;
}


# clear; gtt.sh apps/install/logiciels/reseaux.sh install-yacy
addLibrairie 'install-yacy' "Install le moteur de recherche décentraliser";
function      install-yacy() {
    local -i _fct_pile_gtt_level=2;
    fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

    titre1 'Installation de unbound'
    if [ $IS_ROOT = false ]
    then
            titreWarn "Le paquet doit être installé en root!"
            fct-pile-gtt-out $E_NOT_ROOT $_fct_pile_gtt_level; return $E_NOT_ROOT;
        fi

    titreInfo 'Doc: https://yacy.net/installation/debianinstall/';

    if [ ! -f "/etc/apt/sources.list.d/yacy.list" ]
    then
        eval-echo "echo 'deb http://debian.yacy.net ./' > /etc/apt/sources.list.d/yacy.list";
        wget http://debian.yacy.net/yacy_orbiter_key.asc -O- | apt-key add -
        apt-key advanced --keyserver pgp.net.nz --recv-keys 03D886E7
    fi

    apt-get update
    aptitude-install openjdk-7-jre-headless # java 7 is sufficient, only a headless version is needed
    aptitude-install yacy

    fct-pile-gtt-out 0 $_fct_pile_gtt_level; return 0;
}
