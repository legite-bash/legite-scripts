echo-d "$BASH_SOURCE" 1 ;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


#######################
# * LOGICIELS - EDI * #
#######################

addLibrairie 'install-sublimeText'
function      install-sublimeText(){
    titre3 'Installation de Sublime Text'
    if [ $IS_ROOT $ false ];then titreWarn "$FUNCNAME doit être lancé en root!"; fi
    if [ -e /usr/bin/subl ]
    then
        notifs-pile-add-echo '/usr/bin/subl est déjà installé'
    else
        titreInfo 'https://www.sublimetext.com/docs/3/linux_repositories.html'
        eval-echo "wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | apt-key add -"
        eval-echo "apt-get install apt-transport-https"
        eval-echo 'echo "deb https://download.sublimetext.com/ apt/stable/" | tee /etc/apt/sources.list.d/sublime-text.list'
        eval-echo "apt-get update && apt-get install sublime-text" 
    fi
    return 0;
}


#addLibrairie 'install-visualCode'
function      install-visualCode(){
    titre3 'Installation de Visual Code'
    if [ $IS_ROOT = false ];then titreWarn "Le paquet doit être installé en root!"; fi

    # - Telechargement - #
    if [ ! -f "$TMP_REP/visualCode.deb" ]
    then
        wget -O $TMP_REP/visualCode.deb "https://go.microsoft.com/fwlink/?LinkID=760868";
    else
        echo "$TMP_REP/visualCode.deb existe deja"
        echo "${COLOR_INFO} rm $TMP_REP/visualCode.deb #pour supprimer une ancienne version${COLOR_NORMAL}"
    fi

    # - Installation du paquet - #
    eval-echo "dpkg -i $TMP_REP/visualCode.deb"
    eval-echo "code --user-data-dir=$TMP_REP -v"
    return 0;
}


addLibrairie 'install-VSCodium' 'Ajoute la clerf GPG et le depot pour installer VSCodium'
function      install-VSCodium(){
    titre3 'Installation du compagnion Videodownloader'
    if [ $IS_ROOT = false ];then titreWarn "Le paquet doit être installé en root!"; fi

    titreInfo 'Doc: https://vscodium.com/'

    # - Ajout de la clef gpg - #
    titre4 'Ajout de la clef gpg'NORMAL
    if [ ! -f "/etc/apt/trusted.gpg.d/vscodium.gpg" ];
    then
        eval-echo "wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | dd of=/etc/apt/trusted.gpg.d/vscodium.gpg"
    else
        echo "Le clef gpg /etc/apt/trusted.gpg.d/vscodium.gpg existe deja"
        echo "${COLOR_INFO} rm /etc/apt/trusted.gpg.d/vscodium.gpg #pour supprimer l'ancienne version${COLOR_NORMAL}"
    fi

    # - Ajout du depot - #
    titre4 "Ajout du depot"
    if [ ! -f "/etc/apt/sources.list.d/vscodium.list" ];
    then
        eval-echo "echo 'deb https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs/ vscodium main' | tee /etc/apt/sources.list.d/vscodium.list"
    else    
        echo "Le depot /etc/apt/sources.list.d/vscodium.list existe deja"
        echo "${COLOR_INFO} rm /etc/apt/sources.list.d/vscodium.list #pour supprimer l'ancienne version${COLOR_NORMAL}"
    fi

    # - Installation du paquet - #
    titre4 'Installation'
    eval-echo "aptitude update"
    eval-echo "aptitude install codium"
    return 0;
}

addLibrairie 'install-apache_netBeans-20.1' 'Installe Apache netbeans'
function      install-apache_netBeans-20.1(){
    titre3 'Installation d Apache NetBeans'
    if [ $IS_ROOT = false ];then titreWarn "Le paquet doit être installé en root!"; fi

    titreInfo 'Doc: https://www.apache.org/dyn/closer.lua/netbeans/netbeans-installers/20/apache-netbeans_20-1_all.deb'

    if [ ! -f /usr/bin/netbeans ];then
        wget -O /dev/shm/apache-netbeans_20-1_all.deb https://www.apache.org/dyn/closer.lua/netbeans/netbeans-installers/20/apache-netbeans_20-1_all.deb
        evalCmd "dpkg -i /dev/shm/apache-netbeans_20-1_all.deb";
    fi


    return 0;
}
