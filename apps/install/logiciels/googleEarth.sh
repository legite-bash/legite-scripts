echo-d "${BASH_SOURCE[0]}" 1 ;

#################
# * LOGICIELS * #
#################


addLibrairie 'install-googleEarth-0-prepa'
function      install-googleEarth-0-prepa(){
    titre3 'Installation de google Earth'
    #if [ $IS_ROOT = false ];then titreWarn "$FUNCNAME doit être lancé en root!"fctOut "$FUNCNAME($E_NOT_ROOT)"; return $E_NOT_ROOT; fi
    # https://fr.linuxcapable.com/how-to-install-google-earth-on-debian-linux/

    titre4 'Mettre à jour votre système Debian Linux';
    eval-echo "aptitude update & aptitude safe-upgrade";

    titre4 'Installation des packages prérequis';
    eval-echo "aptitude install curl software-properties-common apt-transport-https ca-certificates -y";

    titre4 'Ajouter le référentiel Google Earth';
    eval-echo "curl -fSsL https://dl.google.com/linux/linux_signing_key.pub | gpg --dearmor | sudo tee /usr/share/keyrings/google-earth.gpg > /dev/null"

    titre4 '';
    eval-echo "echo \"deb [arch=amd64 signed-by=/usr/share/keyrings/google-earth.gpg] http://dl.google.com/linux/earth/deb/ stable main\" | sudo tee /etc/apt/sources.list.d/google-earth.list"

    titre4 'Mise à jour de la liste des référentiels APT';
    eval-echo "aptitude update & aptitude safe-upgrade";

    titre4 'Installer Google Earth Pro';
    eval-echo "aptitude install google-earth-pro"

    titre4 'Installer Google Earth Enterprise';
    eval-echo "aptitude install google-earth-ec"

    return 0;
}

addLibrairie 'install-googleEarth-1-pro'
function      install-googleEarth-1-pro(){
 
    install-googleEarth-0-prepa;
    titre4 'Mise à jour de la liste des référentiels APT';
    eval-echo "aptitude update & aptitude safe-upgrade";

    titre4 'Installer Google Earth Pro';
    eval-echo "aptitude install google-earth-pro"

    return 0;
}


addLibrairie 'install-googleEarth-1-EC'
function      install-googleEarth-1-EC(){
 
    install-googleEarth-0-prepa;

    titre4 'Mise à jour de la liste des référentiels APT';
    eval-echo "aptitude update & aptitude safe-upgrade";


    titre4 'Installer Google Earth Enterprise';
    eval-echo "aptitude install google-earth-ec"

    return 0;
}

