echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


################
# * SURY-PHP * #
################

addLibrairie 'install-sury-php' 'Installe le package sury-PHP';
function      install-sury-php(){
    local -i _pile_fct_app_level=1;
    pile-fct-app-in "$*" $_pile_fct_app_level;

    # https://deb.sury.org/
    # https://packages.sury.org/php/
    # https://packages.sury.org/php/README.txt
    apt-get -y    install lsb-release ca-certificates curl;
    curl    -sSLo /usr/share/keyrings/deb.sury.org-php.gpg https://packages.sury.org/php/apt.gpg
    sh   -c 'echo "deb [signed-by=/usr/share/keyrings/deb.sury.org-php.gpg] https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'
    apt-get update;

    pile-fct-app-out $E_TRUE $_pile_fct_app_level; return $E_TRUE;
}


addLibrairie "install-sury-php8.0";
function      install-sury-php8.0() {
    local -i _pile_fct_app_level=1;
    pile-fct-app-in "$*" $_pile_fct_app_level;

    # https://deb.sury.org/
    # https://packages.sury.org/php/
    # https://packages.sury.org/php/README.txt

    aptitude_install "php8.0 php8.0 php8.0-cgi php8.0-cli php8.0-exif php8.0-fileinfo php8.0-fpm php8.0-gettext php8.0-gd php8.0-mysql php8.0-mysqli php8.0-opcache php8.0-readline php8.0-uuid  php8.0-xml php8.0-mbstring";

    pile-fct-app-out $E_TRUE $_pile_fct_app_level; return $E_TRUE;
}

addLibrairie "install-sury-php8.1";
function      install-sury-php8.1() {
    local -i _pile_fct_app_level=1;
    pile-fct-app-in "$*" $_pile_fct_app_level;

    # https://deb.sury.org/
    # https://packages.sury.org/php/
    # https://packages.sury.org/php/README.txt

    aptitude_install "php8.1 php8.1 php8.1-cgi php8.1-cli php8.1-exif php8.1-fileinfo php8.1-fpm php8.1-gettext php8.1-gd php8.1-mysql php8.1-mysqli php8.1-opcache php8.1-readline php8.1-uuid  php8.1-xml php8.1-mbstring";
    
    pile-fct-app-out $E_TRUE $_pile_fct_app_level; return $E_TRUE;
}


function      install-sury-php8.2() {
    local -i _pile_fct_app_level=1;
    pile-fct-app-in "$*" $_pile_fct_app_level;

    # https://deb.sury.org/
    # https://packages.sury.org/php/
    # https://packages.sury.org/php/README.txt
    aptitude_install "php8.2 php8.2 php8.2-cgi php8.2-cli php8.2-exif php8.2-fileinfo php8.2-fpm php8.2-gettext php8.2-gd php8.2-mysql php8.2-mysqli php8.2-opcache php8.2-readline php8.2-uuid  php8.2-xml php8.2-mbstring" libapache2-mod-php8.2;

    pile-fct-app-out $E_TRUE $_pile_fct_app_level; return $E_TRUE;
}



addLibrairie 'install-php_cgi7' 'Install php-cgi php-pear et les active dans lighhtpd'
function      install-php_cgi7(){
    titre3 'php-cgi7_0'
    if [ $IS_ROOT = false ];then titreWarn "${FUNCNAME[0]} doit être lancé en root!"; return $E_NOT_ROOT; fi
    eval-echo "aptitude -y install php-cgi php-pear";

    eval-echo "lighty-enable-mod fastcgi";
    eval-echo "lighty-enable-mod fastcgi-php";

    titreInfo 'http://pear.php.net/manual/en/package.fileformats.mp3-id.php';
    eval-echo "pear -y install MP3_Id";
    eval-echo "service lighttpd stop;service lighttpd start;service lighttpd status"
    return 0;
}

addLibrairie 'install-pharo9' 'Installe le langage Pharo en 32bits'
function      install-pharo9(){
    titreInfo 'Installe le langage Pharo sur une debian'
    if [ $IS_ROOT = false ];then titreWarn "${FUNCNAME[0]} doit être lancé en root!"; return $E_NOT_ROOT; fi

    titreInfo 'https://software.opensuse.org/download.html?project=devel:languages:pharo:latest&package=pharo9-ui'
    local DEBIAN_VERSION=$(cat /etc/debian_version)
    local DEBIAN_VERSION_MAJEUR=${DEBIAN_VERSION%.*}
    display-vars 'DEBIAN_VERSION' "$DEBIAN_VERSION" 'DEBIAN_VERSION_MAJEUR' "$DEBIAN_VERSION_MAJEUR"
    echo  "deb  http://download.opensuse.org/repositories/devel:/languages:/pharo:/latest/Debian_$DEBIAN_VERSION_MAJEUR/ /" | tee /etc/apt/sources.list.d/devel:languages:pharo:latest.list
    curl -fsSL https://download.opensuse.org/repositories/devel:languages:pharo:latest/Debian_$DEBIAN_VERSION_MAJEUR/Release.key | gpg --dearmor | tee /etc/apt/trusted.gpg.d/devel_languages_pharo_latest.gpg > /dev/null
    aptitude update
    aptitude install pharo9-ui
    return 0;
}

addLibrairie 'install-pharoZC' 'Installe le langage Pharo avec la méthode Zero Conf'
function      install-pharoZC(){
    titre3 'Installation de Pharo avec la méthode Zero Conf dans ~/bin/pharo'
    mkdir -p ~/bin/pharo
    cd ~/bin/pharo
    wget -O- https://get.pharo.org/64 | bash
}


addLibrairie 'install-jre'
function      install-java17() {
    aptitude_install "openjdk-17-jre openjdk-17-jdk";
}



addLibrairie 'install-kvm'
function      install-kvm() {
    # https://developer.android.com/studio/run/emulator-acceleration?utm_source=android-studio&hl=fr#vm-linux
    aptitude_install "qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils";
}


addLibrairie 'install-androidStudio'
function      install-androidStudio() {
    titre1 "${FUNCNAME[0]}";
    if [ $IS_ROOT = false ];then titreWarn "${FUNCNAME[0]} doit être lancé en root!"; return $E_NOT_ROOT; fi

    local _ANDROID_STUDIO_ROOT="/home/pascal/bin";
    local _FILE_TAR="/dev/shm/android-studio-2023.1.1.28-linux.tar";
    local _FILE_TAR_GZ="/dev/shm/android-studio-2023.1.1.28-linux.tar.gz";
    
    # https://developer.android.com/studio/install?hl=fr
    
    if [ ! -f "$_FILE_TAR_GZ" ]
    then
        wget -O $_FILE_TAR_GZ https://redirector.gvt1.com/edgedl/android/studio/ide-zips/2023.1.1.28/android-studio-2023.1.1.28-linux.tar.gz
    fi
    cd "/dev/shm";
    eval-echo "gunzip $_FILE_TAR_GZ";
    cd "$_ANDROID_STUDIO_ROOT"
    eval-echo "tar -xf \"$_FILE_TAR_GZ\";"
    eval-echo "$_ANDROID_STUDIO_ROOT/android-studio/bin/studio.sh;";

    # Acceleration materiel KVM
    # https://developer.android.com/studio/run/emulator-acceleration?utm_source=android-studio&hl=fr#vm-linux
}


# gtt.sh apps/install/logiciels/langages.sh install-flutter-3.22
addLibrairie 'install-flutter-3.22' "Install Flutter (en root /usr/local/bin/"
function      install-flutter-3.22() {
    local -i _pile_fct_app_level=1;
    pile-fct-app-in "$*" $_pile_fct_app_level;

    # https://docs.flutter.dev/get-started/install

    titre1 "${FUNCNAME[0]}" ;

    if ! $IS_ROOT
    then
        erreursAdd "${FUNCNAME[0]}() doit être lancé en root!"; 
        titreWarn  "${FUNCNAME[0]}() doit être lancé en root!";
        pile-fct-app-out $E_NOT_ROOT $_pile_fct_app_level; return $E_NOT_ROOT;
    fi


    local    _FLUTTER_INSTALL_ROOT="/usr/local/bin/";
    local    _FLUTTER_INSTALL_APP="/usr/local/bin/flutter"; # synchro avec install-flutter-cfg()
    local -r _FLUTTER_VERSION='3.22.0';

    
    titre2 "Entrer dans le repertoire temporaire '$tmp_rep'";
    if ! tmp-cd
    then
        pile-fct-app-out $E_INODE_NOT_EXIST $_pile_fct_app_level; return $E_INODE_NOT_EXIST;
    fi

    titre2 "Téléchargement du tar.xz de flutter";
    if [ ! -f "$tmp_rep/flutter_linux_${_FLUTTER_VERSION}-stable.tar.xz" ]
    then
        wget -O "$tmp_rep/flutter_linux_${_FLUTTER_VERSION}-stable.tar.xz" "https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_${_FLUTTER_VERSION}-stable.tar.xz";
    else
        echo "Déjà téléchargé";
    fi


    titre2 "Création du répertoire d'installation de flutter: '$_FLUTTER_INSTALL_ROOT'";
    if ! mkdir -p "$_FLUTTER_INSTALL_ROOT"
    then
        erreurs-pile-add-echo "${FUNCNAME[0]}: impossible de créer le répertoire '${_FLUTTER_INSTALL_ROOT}'";
        pile-fct-app-out $E_INODE_NOT_EXIST $_pile_fct_app_level; return $E_INODE_NOT_EXIST;
    fi
    
    titre2 "Entrer dans le repertoire d'installation '$_FLUTTER_INSTALL_ROOT'";
    if ! cd "$_FLUTTER_INSTALL_ROOT";
    then
        erreurs-pile-add-echo "${FUNCNAME[0]}(): impossible d'entrer dans le répertoire '${_FLUTTER_INSTALL_ROOT}'";
        pile-fct-app-out $E_INODE_NOT_EXIST $_pile_fct_app_level; return $E_INODE_NOT_EXIST;
    fi

    titre2 "Décompression du tar.xz de flutter dans le repertoire courant";
    if [ -f "$_FLUTTER_INSTALL_APP/bin/flutter" ]
    then
        echo "Fichiers déjà décompressé";
    else
        eval-echo "tar -xf '$tmp_rep/flutter_linux_${_FLUTTER_VERSION}-stable.tar.xz';"
    fi

    titre2 "Vérification de la décompression";
    if [ -f "$_FLUTTER_INSTALL_APP/bin/flutter" ]
    then
        file "$_FLUTTER_INSTALL_APP/bin/flutter";
    else
        erreurs-pile-add-echo "${FUNCNAME[0]}(): Le fichier '$_FLUTTER_INSTALL_APP/bin/flutter' est introuvable";
        pile-fct-app-out $E_INODE_NOT_EXIST $_pile_fct_app_level; return $E_INODE_NOT_EXIST;
    fi

    titre2 "Installation des dépendances Debian";
    aptitude_install "git unzip xz-utils zip sdkmanager clang cmake ninja-build pkg-config libgtk-3-dev google-android-cmdline-tools-9.0-installer";


    titre2 "Lancer la configuration de Flutter en mode utilisateur:";
    titreCmd "gtt.sh apps/install/logiciels/langages.sh install-flutter-cfg"

    pile-fct-app-out $E_TRUE $_pile_fct_app_level; return $E_TRUE;
}


# gtt.sh apps/install/logiciels/langages.sh install-flutter-cfg
addLibrairie 'install-flutter-cfg' "Install Flutter (Cfg Fluter pour l'utilisateur)"
function      install-flutter-cfg() {
    local -i _pile_fct_app_level=1;
    pile-fct-app-in "$*" $_pile_fct_app_level;

    local    _FLUTTER_INSTALL_APP="/usr/local/bin/flutter";

    titre2 "Ajout du PATH" 
    titre3 "Ajout du PATH dans l'environnement"; 
    echo "export PATH+=\":$_FLUTTER_INSTALL_APP/bin\"";
    export PATH+=":$_FLUTTER_INSTALL_APP/bin";

    local _bash_path="$HOME/bashrc.lan";
    if [ -f "$_bash_path"  ]
    then
        titre3 "Ajout du PATH dans $_bash_path"; 
        echo "    PATH+=\":$_FLUTTER_INSTALL_APP/bin\"" >> $_bash_path;
    fi


    titre2 "Installation de l'outil Flutter: cmdline-tools";
    # correction du bug:
    # https://stackoverflow.com/questions/76407257/android-sdkmanager-not-found-update-to-the-latest-android-sdk-and-ensure-that
    eval-echo '/usr/bin/sdkmanager --install "cmdline-tools;latest"';

    titreInfo "Pour voir la list des paquets du sdk installé:"
    titreCmd "/usr/bin/sdkmanager --list";

    titre2 "Configuration de Flutter";

    titre3 "Indiquer le chemin du sdk android";
    eval-echo "flutter config --android-sdk $HOME/Android/Sdk";  # indique le chemin vers le sdk android

    titre2 "Désactivation des metric dart et flutter";
    eval-echo 'dart --disable-analytics'
    eval-echo 'flutter config --no-analytics --suppress-analytics '

    titre2 "Activation des suport de sortie";
    eval-echo 'flutter config  --enable-linux-desktop';
    #eval-echo 'flutter config  --enable-windows-desktop';
    eval-echo 'flutter config  --enable-android';
    #eval-echo 'flutter config  --enable-fushia';
    eval-echo 'flutter config  --enable-web';


    titre3 "Animer le terminal";
    eval-echo "flutter config --cli-animations";
   

    titre3 "precache";
    eval-echo 'flutter precache';

    titre2 "Acceptation des licences"
    eval-echo 'flutter doctor --android-licenses';


    titre2 "Vérification de Flutter";
    eval-echo 'flutter doctor -v';

    titre2 "Liste des émulateurs";
    eval-echo 'flutter emulators';
    
    titre2 "Liste des devices";
    eval-echo 'flutter devices';

    pile-fct-app-out $E_TRUE $_pile_fct_app_level; return $E_TRUE;
}


addLibrairie 'install-nodeJS-nvm' "Installe Node Version Manager. pour connaitre la derniere version";
function      install-nodeJS-nvm() {
    local -i _pile_fct_app_level=1;
    pile-fct-app-in "$*" $_pile_fct_app_level;


    # installs nvm (Node Version Manager)
    # https://nodejs.org/en/download/package-manager
    titre1 "${FUNCNAME[0]}";

    if ! $IS_ROOT
    then
        erreursAdd "${FUNCNAME[0]}() doit être lancé en root!"; 
        titreWarn  "${FUNCNAME[0]}() doit être lancé en root!";
        pile-fct-app-out $E_NOT_ROOT $_pile_fct_app_level; return $E_NOT_ROOT;
    fi

    curl -o- "https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh" | bash

    # download and install Node.js
    nvm install 20

    # verifies the right Node.js version is in the environment
    node -v # should print `v20.13.1`

    # verifies the right NPM version is in the environment
    npm -v # should print `10.5.2`

    pile-fct-app-out $E_TRUE $_pile_fct_app_level; return $E_TRUE;
}


# gtt.sh apps/install/logiciels/langages.sh install-flutter-3.22
addLibrairie 'install-nodeJS-20.13.1' "Compile et installe nodeJE (en root /usr/local/bin/"
function      install-nodeJS-20.13.1() {
    local -i _pile_fct_app_level=1;
    pile-fct-app-in "$*" $_pile_fct_app_level;

    # https://docs.flutter.dev/get-started/install

    titre1 "${FUNCNAME[0]}" ;

    if ! $IS_ROOT
    then
        erreursAdd "${FUNCNAME[0]}() doit être lancé en root!"; 
        titreWarn  "${FUNCNAME[0]}() doit être lancé en root!";
        pile-fct-app-out $E_NOT_ROOT $_pile_fct_app_level; return $E_NOT_ROOT;
    fi


    local -r _NODEJS_INSTALL_ROOT="/usr/local/bin/";
    local -r _NODEJS__INSTALL_APP="/usr/local/bin/flutter"; # synchro avec install-flutter-cfg()
    local -r _NODEJS_VERSION='20.13.1';


    titre2 "Entrer dans le repertoire temporaire '$tmp_rep'";
    if ! tmp-cd
    then
        pile-fct-app-out $E_INODE_NOT_EXIST $_pile_fct_app_level; return $E_INODE_NOT_EXIST;
    fi


    titre2 "Installer les dépendances";
    eval-echo "install python3 g++ make python3-pip";


    titre2 "Téléchargement de la signature";
    eval-echo "curl -O https://nodejs.org/dist/v${_NODEJS_VERSION}/SHASUMS256.txt";
    eval-echo "curl -O https://nodejs.org/dist/v${_NODEJS_VERSION}/SHASUMS256.txt.sig";
    
    
    titre2 "Téléchargement du tar.xz de flutter";


    titre2 "Compilation de node JS";
    // https://github.com/nodejs/node/blob/main/BUILDING.md


    titre2 "Téléchargement du tar.xz de flutter";
    if [ ! -f "$tmp_rep/flutter_linux_${_FLUTTER_VERSION}-stable.tar.xz" ]
    then
    curl -O https://nodejs.org/dist/vx.y.z/SHASUMS256.txt

        wget -O "$tmp_rep/flutter_linux_${_FLUTTER_VERSION}-stable.tar.xz" "https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_${_FLUTTER_VERSION}-stable.tar.xz";
    else
        echo "Déjà téléchargé";
    fi


    titre2 "Création du répertoire d'installation de flutter: '$_FLUTTER_INSTALL_ROOT'";
    if ! mkdir -p "$_FLUTTER_INSTALL_ROOT"
    then
        erreurs-pile-add-echo "${FUNCNAME[0]}: impossible de créer le répertoire '${_FLUTTER_INSTALL_ROOT}'";
        pile-fct-app-out $E_INODE_NOT_EXIST $_pile_fct_app_level; return $E_INODE_NOT_EXIST;
    fi
    
    titre2 "Entrer dans le repertoire d'installation '$_FLUTTER_INSTALL_ROOT'";
    if ! cd "$_FLUTTER_INSTALL_ROOT";
    then
        erreurs-pile-add-echo "${FUNCNAME[0]}(): impossible d'entrer dans le répertoire '${_FLUTTER_INSTALL_ROOT}'";
        pile-fct-app-out $E_INODE_NOT_EXIST $_pile_fct_app_level; return $E_INODE_NOT_EXIST;
    fi

    titre2 "Décompression du tar.xz de flutter dans le repertoire courant";
    if [ -f "$_FLUTTER_INSTALL_APP/bin/flutter" ]
    then
        echo "Fichiers déjà décompressé";
    else
        eval-echo "tar -xf '$tmp_rep/flutter_linux_${_FLUTTER_VERSION}-stable.tar.xz';"
    fi

    titre2 "Vérification de la décompression";
    if [ -f "$_FLUTTER_INSTALL_APP/bin/flutter" ]
    then
        file "$_FLUTTER_INSTALL_APP/bin/flutter";
    else
        erreurs-pile-add-echo "${FUNCNAME[0]}(): Le fichier '$_FLUTTER_INSTALL_APP/bin/flutter' est introuvable";
        pile-fct-app-out $E_INODE_NOT_EXIST $_pile_fct_app_level; return $E_INODE_NOT_EXIST;

    fi

    titre2 "Installation des dépendances Debian";
    aptitude_install "git unzip xz-utils zip sdkmanager clang cmake ninja-build pkg-config libgtk-3-dev google-android-cmdline-tools-9.0-installer";

    titre2 "Lancer la configuration de Flutter en mode utilisateur:";
    titreCmd "gtt.sh apps/install/logiciels/langages.sh install-flutter-cfg"

    pile-fct-app-out $E_TRUE $_pile_fct_app_level; return $E_TRUE;
}

return 0;