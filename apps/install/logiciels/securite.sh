echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


#################
# * LOGICIELS * #
#################

addLibrairie 'install-metasploit'
function      install-metasploit(){
    titre3 'Installation de metasploit-framework'
    titreInfo "https://docs.metasploit.com/docs/using-metasploit/getting-started/nightly-installers.html";

    if [ $IS_ROOT = false ];then titreWarn "${FUNCNAME[0]} doit être lancé en root!"fctOut "${FUNCNAME[0]}($E_NOT_ROOT)"; return $E_NOT_ROOT; fi

    local _rep="$tmp_rep/metasploit"
    if ! mkdir  "$_rep"
    then
        erreurs-pile-add-echo "Erreur lors de la création du répertoire '$_rep'";
        retunr $E_INODE_NOT_EXIST;
    fi
    
    if ! cd  "$_rep"
    then
        erreurs-pile-add-echo "Erreur lors de l'ientrée dans le répertoire '$_rep'";
        retunr $E_INODE_NOT_EXIST;
    fi
    

    Titre2 "Télchargement de l'installateur";
    eval-echo "curl https://raw.githubusercontent.com/rapid7/metasploit-omnibus/master/config/templates/metasploit-framework-wrappers/msfupdate.erb > msfinstall";


    Titre2 "Execution de l'installateur: Ajout du dépot, installation du dépot";
    eval-echo "chmod 755 msfinstall && ./msfinstall";
    titre3 "Les programmes commencent par msf*"

    return 0;
}

