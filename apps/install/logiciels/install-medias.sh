echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


#################
# * LOGICIELS * #
#################
addLibrairie 'inbstall-medias' 'Installation de Youtube-dl VDCompagnon libdvdcss e mode auto'
function      inbstall-medias() {
    #titreLib;
    libExecAuto 'install-yt-dlp';
    libExecAuto 'install-VDCompagnon';
    libExecAuto 'install-libdvdcss';
}


addLibrairie 'install-youtubeDl' 'Installation de Youtube-dl'
function      install-youtubeDl() {
    #titreLib;
    eval-echo "sudo aptitude install curl"
    eval-echo "sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl"
    eval-echo "sudo chmod a+rx /usr/local/bin/youtube-dl"
    return 0;
}

addLibrairie 'install-yt-dlp' 'Installation de yt-dlp (fix loader-id)'
function      install-yt-dlp() {
    #titreLib;
    #titreInfo "https://appuals.com/youtube-dl-error-unable-to-extract-uploader-id/"
    #eval-echo "sudo aptitude install pip"
    #eval-echo "python3 -m pip install --force-reinstall https://github.com/yt-dlp/yt-dlp/archive/master.tar.gz"

    if [ ! -f /usr/local/bin/yt-dlp ]
    then
        eval-echo "sudo wget -O /usr/local/bin/yt-dlp https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp_linux"
        eval-echo "sudo chmod +x /usr/local/bin/yt-dlp"
    fi
    echo "yt-dlp déjà installé.";
    return 0;
}


# - compagnon de Videodownloader - #
addLibrairie 'install-VDCompagnon' 'Installe Le compagnon de Videodownloader'
function      install-VDCompagnon() {
    #titreLib;
    titre3 'Installation du compagnion Videodownloader'

    titreInfo 'Le dernier numéro de version se trouve à: https://github.com/mi-g/vdhcoapp/releases/'

    # - Telechargement - #
    titreInfo "site: https://github.com/mi-g/vdhcoapp/":

    local _path="$TMP_REP/coapp_amd64.deb";
    if [ ! -f "$_path" ]
    then
        titre4 'wget';
        wget -O "$_path" "https://github.com/mi-g/vdhcoapp/releases/download/v1.6.3/net.downloadhelper.coapp-1.6.3-1_amd64.deb";
        local -i _errNu=$?;
        if [ $? -ne 0 ]
        then
            erreurs-pile-add-echo "Erreur lors du téléchargement (err wget:$_errNu)."
            return $E_INODE_NOT_EXIST;
        fi
    else
        echo "'$_path' existe deja";
        titreInfo "rm '$_path' #pour supprimer une ancienne version.";
    fi

    # - Installation du paquet - #
    titre4 'Installation';
    eval-echo "sudo dpkg -i $_path";
    return 0;
}


addLibrairie 'install-libdvdcss'
function      install-libdvdcss() {
    titre3 'Installation de libdvdcss'

    titreInfo 'Installation de libdvd-pkd';
    titreInfo "http://www.videolan.org/developers/libdvdcss.html";
    titreInfo "Ce paquet va telecharger le libdvdcss puis le compiler et ensuite l'installer"
    eval-echo "sudo aptitude -y install libdvd-pkg";
    eval-echo "sudo dpkg-reconfigure libdvd-pkg";
    return 0;
}

