echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: Install firefox selon le canal stable|beta|dev


###############
# * FIREFOX * #
###############

# clear; gtt.sh --libExecAuto apps/install/logiciels/firefox.sh   install-firefox - canal=beta
# clear; gtt.sh --libExecAuto apps/install/logiciels/firefox-Auto install-firefox
addLibrairie 'install-firefox' 'gtt.sh apps/install/logiciels/firefox.sh install-firefox - canal=stable|beta|dev';
function      install-firefox(){
    local -i _pile_fct_app_level=2;
    fct-pile-app-in "$*" $_pile_fct_app_level;


    # - Afficher l'aide - #
    local _canal="${PSPParams['canal']:-""}";
    if [ $isLibExecAuto == $E_FALSE ]
    then
        if [ "$_canal" == "" ]
        then
            titreInfo "gtt.sh apps/install/logiciels/firefox-Auto";
            titreInfo "gtt.sh apps/install/logiciels/firefox.sh install-firefox - canal=stable|beta|dev";
        fi
    fi

    if [ "$_canal" == "stable" ]; then install-firefox-stable; fi
    if [ "$_canal" == "beta"   ]; then install-firefox-beta; fi
    if [ "$_canal" == "dev"    ]; then install-firefox-dev; fi

    if [ "$_canal" == "" ]
    then
        libExecAuto 'install-firefox-stable';
        libExecAuto 'install-firefox-beta';
        libExecAuto 'install-firefox-dev';
    fi

    fct-pile-app-out $E_TRUE $_pile_fct_app_level; return $E_TRUE;
}


addLibrairie 'install-firefox-stable' 'Installe firefox stable'
function      install-firefox-stable(){
    local -i _pile_fct_app_level=2;
    fct-pile-app-in "$*" $_pile_fct_app_level;

    install-firefox-version 'stable';
    erreur-set;

    fct-pile-app-out $erreur_no $_pile_fct_app_level; return $erreur_no;
}

addLibrairie 'install-firefox-beta' 'Installe firefox beta'
function      install-firefox-beta(){
    local -i _pile_fct_app_level=2;
    fct-pile-app-in "$*" $_pile_fct_app_level;

    install-firefox-version 'beta';
    erreur-set;

    fct-pile-app-out $erreur_no $_pile_fct_app_level; return $erreur_no;
}

addLibrairie 'install-firefox-dev' 'Installe firefox Dev'
function      install-firefox-dev(){
    local -i _pile_fct_app_level=2;
    fct-pile-app-in "$*" $_pile_fct_app_level;

    install-firefox-version 'dev';
    erreur-set;

    fct-pile-app-out $erreur_no $_pile_fct_app_level; return $erreur_no;
}


# - - #
addLibrairie 'install-firefox-version ' 'Installe firefox selon la version'
function      install-firefox-version(){
    local -i _pile_fct_app_level=2;
     "$*" $_pile_fct_app_level;

    if [ $# -ne 1 ];then fct-pile-app-out $E_ARG_REQUIRE $_pile_fct_app_level; return $E_ARG_REQUIRE; fi

    local _canal="${1:-"stable"}";
    local _url='';
    case "$_canal" in
        'stable')   _url='https://download.mozilla.org/?product=firefox-latest-ssl&os=linux64&lang=fr';             ;;
        'beta')     _url='https://download.mozilla.org/?product=firefox-beta-latest-ssl&os=linux64&lang=fr';        ;;
        'dev')      _url='https://download.mozilla.org/?product=firefox-devedition-latest-ssl&os=linux64&lang=fr';  ;;
        *)
            erreurs-pile-add-echo "Cette version de firefox '$_canal' n'est pas supportée.";
            fct-pile-app-out $E_ARG_BAD $_pile_fct_app_level; return $E_ARG_BAD;
    esac


    titre1 "Installation de firefox: canal $_canal";
    if $IS_ROOT
    then
        local -r install_ROOT="/usr/share";
    else
        local -r install_ROOT="$HOME/bin";
        mkdir -p "$install_ROOT";
        if [ ! -d "$install_ROOT" ]
        then
            erreurs-pile-add-echo "Impossible de créer le repertoire '$install_ROOT'.";
            fct-pile-app-out $E_INODE_NOT_EXIST $_pile_fct_app_level; return $E_INODE_NOT_EXIST;
        fi
    fi
    display-vars 'install_ROOT' "$install_ROOT";
    local _progNom="firefox-${_canal}";                     display-vars '_progNom' "$_progNom";
    local _install_REP="$install_ROOT/${_progNom}";         display-vars '_install_REP'    "$_install_REP";
    local ancienneVersion='';
    if [ -f "$_install_REP/firefox" ];then ancienneVersion=$($_install_REP/firefox -v); fi
    display-vars 'ancienneVersion' "$ancienneVersion";

    titre2 'Backup';
    if [ ! -d "$_install_REP" ]
    then
        titreInfo "Pas de repertoire d'install existant. Pas de backup à faire.";        
    else
        if [ ! -d "${_install_REP}-${ancienneVersion}" ]
        then
            eval-echo "mv $_install_REP '${_install_REP}-${ancienneVersion}'" ; # copie en backup du repertoire existant
        else
            titreInfo "La version actuel est déjà backupé: -${ancienneVersion}";
        fi
    fi

    # Suppression du rep de dest (si pas depalcer par le backup)
    if [ -d "${_install_REP}" ]
    then
        titre2 "Suppression de '${_install_REP}'.";
        rm -R "${_install_REP}";
    fi


    titre2 'Telechargement';
    if [ -f "$tmp_rep/$_progNom.tar.gz" ]
    then
        echo "$tmp_rep/$_progNom.tar.gz existe deja" 
        titreInfo "rm $tmp_rep/$_progNom.tar.gz #pour supprimer une ancienne version";
    else
        if mkdir -p "$tmp_rep"
        then
            eval-echo "wget -O $tmp_rep/$_progNom.tar.gz '$_url'";
            if [ $erreur_no -ne 0 ]
            then
                erreurs-pile-add-echo "Impossible de Télécharger '$tmp_rep/$_progNom.tar.gz'.";
                fct-pile-app-out $E_FALSE $_pile_fct_app_level; return $E_FALSE;
            fi
        else
            erreurs-pile-add-echo "Impossible de créer le repertoire '$tmp_rep'.";
            fct-pile-app-out $E_INODE_NOT_EXIST $_pile_fct_app_level; return $E_INODE_NOT_EXIST;
        fi
    fi


    titre2 'Extraction';
    if mkdir -p "$_install_REP"
    then
        echo "Création du repertoire '$_install_REP'";
    else
        erreurs-pile-add-echo "Impossible de créer le repertoire '$_install_REP'.";
        fct-pile-app-out $E_INODE_NOT_EXIST $_pile_fct_app_level; return $E_INODE_NOT_EXIST;
        fi

    eval-echo "tar -xvf $tmp_rep/$_progNom.tar.gz --directory=$_install_REP >/dev/null"
    if [ $erreur_no -ne 0 ]
    then
        erreurs-pile-add-echo "La decompression n'a pas produit $_install_REP/firefox";
        fct-pile-app-out $E_INODE_NOT_EXIST $_pile_fct_app_level; return $E_INODE_NOT_EXIST;
    fi
    #eval-echo "rm $tmp_rep/$_progNom.tar.gz"

    if [ ! -f $_install_REP/firefox/firefox ]
    then
        erreurs-pile-add-echo "La decompression n'a pas produit le fichier $_install_REP/firefox/firefox";
        fct-pile-app-out $E_INODE_NOT_EXIST $_pile_fct_app_level; return $E_INODE_NOT_EXIST;
    fi

    # renome le repertoire telecharger
    eval-echo "mv $_install_REP/firefox $_install_REP/$_canal";
    eval-echo "mv $_install_REP/$_canal/* $_install_REP/";

    if [ -d "$_install_REP/$_canal/" ]
    then
        rm -R "${_install_REP}/${_canal}";
    fi

    if $IS_ROOT
    then
        titre2 'Creation du lien symbolique vers /usr/local/bin';
        if [ -L "/usr/local/bin/$_progNom" ]
        then
            titreInfo "Le lien symbolique existe /usr/local/bin/firefox -> suppression" ;
            eval-echo "unlink /usr/local/bin/$_progNom";
        fi
        eval-echo "ln -s $_install_REP/firefox /usr/local/bin/$_progNom";
    fi

    # - Affichage de la version - #
    #if $IS_ROOT
    #then
    #    newVersion=$(/usr/local/bin/firefox -v)
    #else
        newVersion="$($_install_REP/firefox -v)";
    #fi
    notifs-pile-add-echo "Mise a jours de la version ($_canal) $ancienneVersion vers $newVersion"
    fct-pile-app-out $E_TRUE $_pile_fct_app_level; return $E_TRUE;
}



# - Autoexecution - #
#install-firefox # la librairei est executé 2 fois
##execLib "install-firefox";