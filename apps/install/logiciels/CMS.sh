echo-d "$BASH_SOURCE" 1 ;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


#######################
# * LOGICIELS - CMS * #
#######################

addLibrairie 'install-wp-cli'
function      install-wp-cli(){
    titre3 'Installation de wp-cli'
    if [ $IS_ROOT $ false ];then titreWarn "$FUNCNAME doit être lancé en root!"; return $E_NOT_ROOT; fi
    #if [ -e /usr/bin/wp-cli.phar ] # desactivé pour l'update
    #then
    #    notifs-pile-add-echo '/usr/bin/wp-cli.phar est déjà installé' 
    #else
        titreInfo 'https://wp-cli.org/fr/'
        
        if  cd /usr/local/bin
        then
            eval-echo "curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar";
            eval-echo "chmod +x wp-cli.phar";
            eval-echo "ln -s wp-cli.phar wp";
            titreInfo "Autocompletion"
            eval-echo "wget -O /etc/bash_completion.d/wp-completion.bash https://raw.githubusercontent.com/wp-cli/wp-cli/v2.10.0/utils/wp-completion.bash"

        else
            erreurs-pile-add-echo "Entré dans le repertoire '/usr/local/bin' impossible";
            return $E_INODE_NOT_EXIST;
        fi
    #fi
    return 0;
}