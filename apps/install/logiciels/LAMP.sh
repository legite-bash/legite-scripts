echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


#################
# * LOGICIELS * #
#################


addLibrairie 'install-lighttpd'
function      install-lighttpd(){
    titre3 'Installation de lighttpd '
    if [ $IS_ROOT = false ];then titreWarn "${FUNCNAME[0]} doit être lancé en root!"; fi
    eval-echo "aptitude -y install lighttpd";
    if [ -f /www ]
    then
        eval-echo "mkdir -p /home/datas/www; chown pascal:www-data /home/datas/www; ln -s /home/datas/www /www";
    fi
    eval-echo "service lighttpd stop&&service lighttpd start&&service lighttpd status";

    return 0;
}


addLibrairie 'install-mariaDB'
function      install-mariaDB(){
    titre3 'Installation de mariaDB';
    if [ $IS_ROOT = false ];then titreWarn "${FUNCNAME[0]} doit être lancé en root!"fctOut "${FUNCNAME[0]}($E_NOT_ROOT)"; return $E_NOT_ROOT; fi

    titre4 'Création de la redirection'

    if [ ! -d /home/datas/mysql ];then mkdir -p /home/datas/mysql;fi
    eval-echo "chown -R mysql:mysql /home/datas/mysql";
    eval-echo "chmod -R 770 /home/datas/mysql/*";
    

    # Ajoute le montage dans /etc/fstab 
    local _isMysqlInFstab=$(cat /etc/fstab  | grep "/home/datas/mysql/")
    if [ -z  $_isMysqlInFstab ]
    then
        echo "# DO NOT EDIT #";
        echo "/home/datas/mysql/  /var/lib/mysql/   none   bind 0 0" >> /etc/fstab;
    fi

    #umount /var/lib/mysql/
    #mount  --bind   /home/datas/mysql/ /var/lib/mysql/ 
    #if [ ! -L /var/lib/mysql ]
    #then
    #    eval-echo "mv /var/lib/mysql /var/lib/mysql.ori";
    #    eval-echo "ln -s /home/datas/mysql /var/lib/mysql"
    #fi

    titre4 'Installation'
    eval-echo "aptitude -y install mariadb-server";

    mkdir -m 2750 /var/log/mysql
    chown mysql /var/log/mysql

    titre4 'Édition de la configuration'
    #eval-echo "nano /etc/mysql/mariadb.conf.d/50-server.cnf";

    titre4 'Rechargement des services';
    eval-echo "service mariadb stop;service mariadb start;service mariadb force-reload";
    titreInfo "mariadb-secure-installation";

    eval-echo "dpkg -i /home/datas/install/dbeaver/dbeaver-ce_23.3.4_amd64.deb";

    return 0;
}



addLibrairie 'install-apache2'
function      install-apache2(){
    titre3 'Installation de apache2'
    if [ $IS_ROOT = false ];then titreWarn "${FUNCNAME[0]} doit être lancé en root!"fctOut "${FUNCNAME[0]}($E_NOT_ROOT)"; return $E_NOT_ROOT; fi

    titre4 'Création de la redirection'
    
    titre4 'Installation'
    aptitude_install "apache2";

    titre4 'Copier les sites available';
    if [ ! -z $BACKUP_ROOT ]
    then
        cp $BACKUP_ROOT/msi/etc/apache2/sites-available/* /etc/apache2/sites-available/;
        a2ensite 000-monitoring;
        a2ensite 001-dev;
        a2ensite 020-scriptcase;
        a2ensite 001-apt-mirror;
        systemctl reload apache2;
    fi

    titre4 'Rechargement des services'
    eval-echo "service apache2 stop;service apache2 start;service apache2 force-reload";
    return 0;
}


