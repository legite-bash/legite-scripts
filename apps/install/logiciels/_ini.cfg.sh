echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


#################
# * LOGICIELS * #
#################

echo "Chargement des fichiers de configuration du repertoire";
local _rep="$APPS_REP/install/logiciels";
for _config_pathname in $_rep/*.sh
do
    if [ "$_config_pathname" = "$_rep/_ini.cfg.sh" ]; then  continue;fi
    echo "Chargement de $_config_pathname";
    . $_config_pathname;
done




addLibrairie 'install-radioAmateur'
function      install-radioAmateur(){
    titre3 'Installation de gqrx-sdr'
    if [ $IS_ROOT = false ];then titreWarn "$FUNCNAME doit être lancé en root!"fctOut "$FUNCNAME($E_NOT_ROOT)"; return $E_NOT_ROOT; fi

    eval-echo "aptitude install gqrx-sdr"
    return 0;
}


addLibrairie 'install-mariaDB-rescue' "Connexion d'urgence au serveur mariaDB"
function      install-mariaDB-rescue(){
    if [ $IS_ROOT = false ];then titreWarn "$FUNCNAME doit être lancé en root!"fctOut "$FUNCNAME($E_NOT_ROOT)"; return $E_NOT_ROOT; fi

    titreInfo "https://www.digitalocean.com/community/tutorials/how-to-reset-your-mysql-or-mariadb-root-password"
    mariadb --version
    eval-echo "systemctl stop mariadb";
    if $isTrapCAsk;then return $E_CTRL_C;fi

    titreCmd "mysqld_safe --skip-grant-tables --skip-networking &"
    mysqld_safe --skip-grant-tables --skip-networking &
    if $isTrapCAsk;then
        kill $(cat /var/run/mysqld/mysqld.pid)
        return $E_CTRL_C;
    fi


    echo '----';
    titreInfo "FLUSH PRIVILEGES;"
    titreInfo "ALTER USER 'root'@'localhost' IDENTIFIED BY 'new_password';";
    titreInfo "FLUSH PRIVILEGES;"
    titreInfo 'exit'
    echo '----';


    eval-echo "mysql -u root"
    titreCmd 'kill $(cat /var/run/mysqld/mysqld.pid)'
    kill $(cat /var/run/mysqld/mysqld.pid)
    eval-echo "systemctl start mysql"

}
