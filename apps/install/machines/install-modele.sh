echo-d "${BASH_SOURCE[0]}" 1;

# install-modele.sh
# date de création    : ?
# date de modification: 2024.08.10
# Description: 
# est telechargé a chaque mise a jours: toutes modifications sera écrasées



addLibrairie 'install-modele' 'Install un système à la carte';
function      install-modele(){
    local -i _pile_fct_app_level=1;
    pile-fct-app-in "$*" $_pile_fct_app_level;

    if [ $IS_ROOT = false ]
    then
        titreWarn "${FUNCNAME[0]} doit être lancé en root!";
        pile-fct-app-out $E_NOT_ROOT $_pile_fct_app_level; return $E_NOT_ROOT;
    fi


    install-aptitude;
    read  -p "Appuyer sur une touche."
    eval-echo "aptitude safe-upgrade"
    
    install-deb localpurge

    configure_clavier
    configure_french_console


    titre2 'Programmes Console de base'
    hitEnter
    install-deb  "vim mc sudo"


    titre2 serveur X
    hitEnter
    install-deb  "xorg xinit menu menu-l10n menu-xdg"
    #install-deb  "xwayland"


    titre2 'gestionnaire de fenetre'
    hitEnter
    install-deb  "openbox openbox-menu"
    configure_french_desktop

    titre2 'gestionnaire de programme graphique'
    hitEnter
    install-deb  "synaptic"


    titre2 'Réseau'
    hitEnter
    #install-deb  "apt-cacher-ng"
    #install-deb  "tor"


    titre2 'Outils systeme'
    hitEnter
    #install-deb  "dphys-swapfile"
    install-deb  "nmon"
    install-deb  "sshfs"
    install-deb  "reiserfsprogs xfsprogs"
    install-deb  "nmon hddtemp lshw"
    eval-echo "dpkg-reconfigure hddtemp"

    #install-deb  "wakeonlan"

    #install-deb  "smartmontools"
    #install-deb  "arandr"
    install-deb  "conky"
    install-deb  "gparted"
    install-deb  "baobab"

    #install-deb  "wicd-gtk"
    install-deb  "lxterminal"
    #update-alternatives  --get-selections
    #eval-echo "update-alternatives  --list x-terminal-emulator"
    eval-echo "update-alternatives  --config x-terminal-emulator"


    titre2 'CUPS'
    hitEnter
    install-deb  'cups printer-driver-hpcups hplip'
    titreInfo 'http://127.0.0.1:631/'


    titre2 'serveurWeb/PHP/mySql'
    hitEnter
    #verifier les dernieres versions
    #install-deb lighttpd
    #install-deb php_cgi7_0
    #install-deb mySQL_php7_0


    titre2 'Gestionnaire de fichier'
    hitEnter
    install-deb  "pcmanfm"
    install-deb  "unrar"


    titre2 'Programmation'
    #if [ $DEBIAN_VERSION_MAJOR -eq 11  ]
    #then
    #    if  [ ! -e /usr/bin/python ]
    #    then
    #        ln -s /usr/bin/python3 /usr/bin/python
    #    fi
    #fi

    install-deb  "git"
    install-deb  "build-essential"
    install-deb  "live-build";

    install-deb  "vim-gtk"
    install-deb  "meld"

    install-deb VSCodium
    #install-deb sublimeText

    read -p "Appuyez squr une touche."

    titre2 'Telechargement'
    #install-deb  "httrack";
    #install-deb  "webhttrack"
    install-deb filezilla
    install-deb youtubeDl


    titre2 'Multimedia'
    hitEnter
    install-deb  "pulseaudio pavucontrol pavumeter"
    install-deb  "id3v2 easytag"
    #install-deb  "minidlna mpd";
    #install-deb  "moc"
    #install-deb  "mpc"
    install-deb  "sox" #play
    install-deb  "ffmpeg"

    install-deb  libdvdcss
    install-deb  "alsamixergui"
    install-deb  "vlc"
    install-deb  "ripperx"

    #install-deb  "dvdbackup";


    install-deb  "simple-scan"
    install-deb  'evince'

    #install-deb  "sonata"
    #install-deb  "eog"
    #install-deb  "josm"


    read -p "Appuyer sur une touche"

    titre2 'Graphisme'
    hitEnter
    install-deb  "gimp"
    install-deb  "inkscape"
    #install-deb  "mypaint"


    titre2 'Surveillance'
    hitEnter
    #install-deb  "camorama"


    titre2 'Bureautique'
    hitEnter
    install-deb  "libreoffice libreoffice-l10n-fr"
    install-deb  "thunderbird thunderbird-l10n-fr"
    install-deb firefoxDev
    install-deb firefoxBeta


    titre2 'Jeux'
    hitEnter
    #install-deb  "lightyears"

    titre2 'legralNet'
    hitEnter
    #install-deb legralNet

    # - Modificatioon de fstab - #
    #add_fstab "/tmp"  "/home/datas/tmp"

    #local r="$RANDOM"
    #add_fstab "/dev/shm/tmpSrc$r" "/dev/shm/tmpDest$r"

    #eval-echo "mount --all"

    #install-deb discord

    titre 'Nettoyage'
    eval-echo "aptitude autoclean";

    pile-fct-app-out $E_TRUE $_pile_fct_app_level; return $E_TRUE;
}

