echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

##########
# DEBIAN #
##########
declare -gr DEBIAN_VERSION="$(cat /etc/debian_version)";
declare -gr DEBIAN_VERSION_MAJOR=${DEBIAN_VERSION%.*};


#######
# APT #
#######

    # clear; sudo gtt.sh -d apps/install/ install-aptitude
    addLibrairie 'install-aptitude' "Installe le paquet aptitude - version mono-paquet" 
    function      install-aptitude(){
        local -i _pile_fct_app_level=1;
        pile-fct-app-in "$*" $_pile_fct_app_level;

        if [ $IS_ROOT = false ]
        then
            titreWarn "${FUNCNAME[0]} doit être lancé en root!";
            pile-fct-app-out $E_NOT_ROOT $_pile_fct_app_level; return $E_NOT_ROOT;
        fi
        if [ -x /usr/bin/aptitude ]
        then echo 'aptitude déjà installé.';
        else eval-echo "apt-get install aptitude";
        fi

        pile-fct-app-out $E_TRUE $_pile_fct_app_level; return $E_TRUE;
    }


    # clear; sudo gtt.sh apps/install/ install-deb - deb=gimp
    addLibrairie 'install-deb' "- deb=paquet # Install un paquet avec aptitude";
    function      install-deb(){
        local -i _pile_fct_app_level=1;
        pile-fct-app-in "$*" $_pile_fct_app_level;

        if [ $IS_ROOT = false ]
        then
            titreWarn "${FUNCNAME[0]} doit être lancé en root!";
            pile-fct-app-out $E_NOT_ROOT $_pile_fct_app_level; return $E_NOT_ROOT;
        fi


        # - Controle des entrées - #
        local _deb="${1:-""}";
        if [ "$_deb" = "" ]
        then
            _deb="${PSPParams['deb']:-""}";
            if [ "$_deb" = '' ]
            then
                erreurs-pile-add-echo "${FUNCNAME[0]} - deb=paquet_nom";
                pile-fct-app-out $E_ARG_REQUIRE $_pile_fct_app_level; return $E_ARG_REQUIRE; return $E_ARG_REQUIRE;
            fi
        fi


        # - Le paquet est il déjà installé? - #
        if [ -z "$(aptitude search \^$_deb\$)" ]
        then eval-echo "aptitude -y install $_deb";
        else echo "$_deb: déjà installé.";
        fi

        pile-fct-app-out $E_TRUE $_pile_fct_app_level; return $E_TRUE;
    }

#


