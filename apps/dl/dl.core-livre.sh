echo_D "${BASH_SOURCE[0]}" 1;

###########
## LIVRE ##
###########
    
    ##
    # Création d'un répertoire livre en utilisant les valeurs calculés automatiquement 
    function createLivreAuto(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "$*";
        if [[ $# -ne 0 ]];then erreursAddEcho "${FUNCNAME[0]} (' titre' )."; fctOut "${FUNCNAME[0]}";return $E_ARG_REQUIRE;fi

        createLivre "$1" "$livreTitreNormalize_Auto";
        local -i retour=$?;
        fctOut "${FUNCNAME[0]}"; return $retour;
    }

    # - creer un nouveau livre - #
    # -- creer un repertoire (dans le rep courant) et va dans ce repertoire -- #
    # $1: nom de du livre (normalisation)
    # $2: repertoire du livre (normalisation, facultatif, si absent = nom normalisé)
    # modifie la var global (ou local) titreNormalize
    function createLivre(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "$*";
        if [[ $# -eq 0 ]];then setCollectionFail 1; erreursAddEcho "${FUNCNAME[0]} ('livre' ['repertoire'] ).";fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;fi

        #tomeNo=0;  # ne pas initialisé ici car il est utilisé en amont pour la creation du reperotire-livre
        #partieNb=0; # mettre 0 pour neutraliser l'affichage dans le nom du fichier (fonction non implementé dans DeuxTroisChapitre) 
        #partieNo=0;
        #initPartie  0 0 # 0 pour neutraliser dans les valeurs auto
        initChapitre 0 0 # 0 pour neutraliser dans les valeurs auto

        local _libreNom="$1";
        titre2 "Création du livre: $_libreNom";
        setTitle "$_libreNom";

        local rep=${2:-"$titreNormalize"};
        getNormalizeText "$rep"; rep="$normalizeText";

        mkdir -p "$rep" && cd "$rep";
        if [[ $? -eq 0 ]]
        then
            notifsAddEcho "Création du livre: '$_libreNom' ($PWD)";
        else
            erreursAddEcho "Le repertoire '$rep' du livre '$_libreNom' ne peut etre crée ou ouvert";
            setCollectionFail 1;
            fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    function setTagNoteLivre(){
        fctIn "$*";
        local _LuPar='';      if [[ "$dl_voix"       != '' ]]; then _LuPar="Lu Par $dl_voix."       ;fi
        local _editeur='';    if [[ "$dl_editeur"    != '' ]]; then _editeur="Editeur $dl_editeur." ;fi
        local _sortieLe='';   if [[ "$dl_date"       != '' ]]; then _sortieLe="Sortie le $dl_date." ;fi
        local _duree='';      if [[ "$dl_duree"      != '' ]]; then _duree="Durée $dl_duree."       ;fi
        local _source='';     if [[ "$dl_source"     != '' ]]; then _source="Source $dl_source."    ;fi
        local _traduction=''; if [[ "$dl_traduction" != '' ]]; then _traduction="Traduction de $dl_traduction." ;fi
        local _url='';        if [[ "$tag_url"       != '' ]]; then _url="$tag_url."                ;fi  # ajouté a postnote selon le format id3


        local _tomeTxt='';
        if [[ $tomeNb -gt 1 ]];then _tomeTxt="Tome $tomeNo/$tomeNb.";fi
        local _chapitreTitre='';
        if [[ "$chapitreTitre" != '' ]]
        then
            _chapitreTitre="Chapitre $chapitreNo/$chapitreNb:$chapitreTitre."
        fi
        tag_note="$_tomeTxt$_chapitreTitre$_LuPar$_editeur$_sortieLe$_duree$_source$_traduction" # $_url est ajouté par saveTags
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function closeLivre(){
        fctIn;
        dl_prenote=''; tag_note=''; dl_postnote='';
        initPartie;
        initChapitre;
        initFichier;
        setDl_date;
        setDl_voix;
        setDl_source;
        dl_editeur='';
        cd ..;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


###########
## TOMES ##
###########
    function initTome(){
        fctIn "$*";
        setTomeNo ${1:-0};
        setTomeNb ${2:-$tomeNo};
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function incTomeNb(){
        fctIn;
        ((tomeNb++))
        setTomeNb $tomeNb;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setTomeNb(){
        fctIn "$*";
        tomeNb=${1:-0};
        if [[ "$trackLevel" == 'Tome' ]];then setTrackNb $tomeNb;fi
        #lzTomeNb; #existe pas
        tomeNoNbT='';if [[ $tomeNb -gt 1 ]];then tomeNoNbT="-T$tlz$tomeNo-$tomeNb";fi
        setNameAuto;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function incTomeNo(){
        fctIn;
        (( tomeNo++ ))
        setTomeNo $tomeNo;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setTomeNo(){
        fctIn "$*";
        tomeNo=${1:-0};
        if [[ "$trackLevel" == 'Tome' ]];then tag_track=$tomeNo;fi
        lzTomeNo;
        tomeNoNbT='';if [[ $tomeNb -gt 1 ]];then tomeNoNbT="-T$tlz$tomeNo-$tomeNb";fi
        setNameAuto
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #prefixe tomeNo avec un zero si 0..9
    function lzTomeNo(){
        fctIn "$*";
        tlz=''
        if [[ $tomeNb -lt 10 ]];then return $E_ARG_BAD;fi
        if [[ $tomeNo -lt 10 ]];then tlz='0';fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setTomeTitre(){ 
        fctIn "$*";
        displayVar '1' "$1"
        tomeTitre="${1:-''}";
        displayVar 'tomeTitre' "$tomeTitre"

        tomeTitreNormalize='';
        tomeTitreTN='';
        if [[ "$tomeTitre" != '' ]]
        then
            getNormalizeText "$tomeTitre"; tomeTitreNormalize="$normalizeText"; 
            tomeTitreTN="-$tomeTitreNormalize"
        fi
       if [[ "$titreLevel" == 'Tome' ]]; then setTitle "$tomeTitre"; fi
        setNameAuto;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #Ajoute un Tome sans changer la valeur de tomeNo
    #addTome( 'titre' ){
    function addTome(){
        fctIn "$*";
        if [[ $# -ne 1 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($@) n'a pas le bon nombre de parametre($#/1). ${FUNCNAME[0]}( \"tomeTitre\" )"
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi
        incTome "$1" $tomeNo;     # ne change pas la valeur actuelle (utilise)
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #defini le tomeNo ou augmente tomeNo de 1 si non defini en 2eme arg
    #defini le titre du tome
    #incTome "tomeTitre" [tomeNo]
    function incTome(){
        fctIn "$*";
        setTomeTitre "${1:-''}";

        if [[ $# -eq 2 ]]
        then tomeNo=$2
        else (( tomeNo++ ));
        fi
        setTomeNo $tomeNo;

        # avertissement en cas de depassement
        if [[ $tomeNo -gt $tomeNb ]]
        then
            erreursAddEcho "Tome actuel $tomeNo/$tomeNb"
            fctOut "${FUNCNAME[0]}($E_FALSE)"; return $E_FALSE;
        fi
        setNameAuto;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function closeTome(){
        fctIn "$*";
        # avertissement en cas de manque de tome
        if [ $tomeNb -gt 0 -a  $tomeNo -lt $tomeNb ]
        then
            erreursAddEcho "Il manque un ou plusieurs tomes $tomeNo$WARN/$tomeNb$tomeTitreTN"
        fi
        initTome;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


#############
## PARTIES ##
#############
    #initPartie [No=0] [Nb=No]
    function initPartie(){
        fctIn "$*";
        setPartieNo ${1:-0};
        setPartieNb ${2:-$partieNo};
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function incPartieNb(){
        fctIn "$*";
        ((partieNb++))
        setPartieNb $partieNb;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setPartieNb(){
        fctIn "$*";
        partieNb=${1:-0};
        if [[ "$trackLevel" == 'Partie' ]];then setTrackNb $partieNb;fi
        #lzPartieNb; #existe pas
        partieNoNbT='';if [[ $partieNb -gt 1 ]];then partieNoNbT="-P$plz$partieNo-$partieNb";fi
        setNameAuto;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function incPartieNo(){
        fctIn "$*";
        ((partieNo++))
        setPartieNo $partieNo;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setPartieNo(){
        fctIn "$*";
        partieNo=${1:-0};
        if [[ "$trackLevel" == 'Partie' ]];then tag_track=$partieNo;fi
        lzPartieNo;
        partieNoNbT='';if [[ $partieNb -gt 1 ]];then partieNoNbT="-P$plz$partieNo-$partieNb";fi
        setNameAuto;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function lzPartieNo(){
        fctIn;
        plz='';
        if [[ $partieNb -lt 10 ]];then return $E_FALSE;fi
        if [[ $partieNo -lt 10 ]];then plz='0';fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setPartieTitre(){
        fctIn "$*";
        partieTitre="${1:-''}";
        partieTitreNormalize='';
        partieTitreTN=''
        if [[ "$partieTitre" != '' ]]
        then
            getNormalizeText "$partieTitre"; partieTitreNormalize="$normalizeText"; 
            partieTitreTN="-$partieTitreNormalize"
        fi
        if [[ "$titreLevel" == 'Partie' ]]; then setTitle "$partieTitre"; fi
        setNameAuto;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #Ajoute une partie sans changer la valeur de partieNo
    #addPartie( 'titre' ){
    function addPartie(){
        fctIn "$*";
        if [[ $# -ne 1 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($@) n'a pas le bon nombre de parametre($#/1). ${FUNCNAME[0]}( \"partieTitre\" )"
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi
        incPartie "$1" $partieNo;     # ne change pas la valeur actuelle (utilise)
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #defini le partieNo ou augmente partieNo de 1 si non defini en 2eme arg
    #defini le titre de la partie
    #addPartie "partieTitre" [partieNo]
    function incPartie(){
        fctIn "$*";
        setPartieTitre "${1:-''}"

        if [[ $# -eq 2 ]]
        then partieNo=$2
        else (( partieNo++ ));
        fi
        setPartieNo $partieNo

        # avertissement en cas de depassement
        if [[ $partieNo -gt $partieNb ]]
        then
            erreursAddEcho "Partie actuelle $partieNo/$partieNb$partieTitreTN"
        fi
        setNameAuto;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function closePartie(){
        fctIn "${FUNCNAME[0]}"
        if [ $partieNb -gt 0 -a  $partieNo -lt $partieNb ]
        then
            erreursAddEcho "Il manque un ou plusieurs parties $partieNo/$partieNb$partieTitreTN"
        fi
        initPartie;
        #cd ..  #  (une partie reste dans le meme repertoire)
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


###############
## CHAPITRES ##
###############
    #initChapitre [chapitreNo=0] [chapitreNb=chapitreNo]
    function initChapitre(){
        fctIn "$*";
        setChapitreNo ${1:-0};
        setChapitreNb ${2:-$chapitreNo};
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function incChapitreNb(){
        fctIn;
        ((chapitreNb++))
        setChapitreNb $chapitreNb
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setChapitreNb(){
        fctIn "$*";
        chapitreNb=${1:-0};
        if [[ "$trackLevel" == 'Chapitre' ]];then setTrackNb $chapitreNb;fi
        #lzChapitreNb; #existe pas
        chapitreNoNbT='';if [[ $chapitreNb -gt 1 ]];then chapitreNoNbT="-C$clz$chapitreNo-$chapitreNb";fi
        setNameAuto;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function incChapitreNo(){
        fctIn "$*";
        ((chapitreNo++))
        setChapitreNo $chapitreNo;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setChapitreNo(){
        fctIn "$*";
        chapitreNo=${1:-0};
        if [[ "$trackLevel" == 'Chapitre' ]];then tag_track=$chapitreNo;fi
        lzChapitreNo;
        chapitreNoNbT='';if [[ $chapitreNb -gt 1 ]];then chapitreNoNbT="-C$clz$chapitreNo-$chapitreNb";fi
        setNameAuto;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #prefixe chapitreNo avec un zero si 0..9
    function lzChapitreNo(){
        clz='';
        if [[ $chapitreNb -lt 10 ]];then return 0;fi
        if [[ $chapitreNo -lt 10 ]];then clz='0' ;fi
        return 0;
    }

    function setChapitreTitre(){ 
        fctIn "$*";
        chapitreTitre="${1:-''}";
        chapitreTitreNormalize='';
        chapitreTitreTN='';
        if [[ "$chapitreTitre" != '' ]]
        then
            getNormalizeText "$chapitreTitre";          chapitreTitreNormalize="$normalizeText"; 
            chapitreTitreTN="-$chapitreTitreNormalize"
        fi
        if [[ "$titreLevel" == 'Chapitre' ]]; then setTitle "$chapitreTitre"; fi
         setNameAuto;
         fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #Ajoute un Chapitre sans changer la valeur de chapitreNo
    #addChapitre( 'titre' ){
    function addChapitre(){
        fctIn "$*";
        if [[ $# -ne 1 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}('$@') n'a pas le bon nombre de parametre($#/1). ${FUNCNAME[0]}( \"chapitreTitre\" )"
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi
        incChapitre "$1" $chapitreNo;     # ne change pas la valeur actuelle (utilise)
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #defini le chapitreNo ou augmente chapitreNo de 1 si non ndefini en 2eme arg
    #defini le titre du chapitre
    #incChapitre "chapitreTitre" [chapitreNo]
    function incChapitre(){
        fctIn "$*";
        setChapitreTitre "${1:-""}";

        titreChapitre "$chapitreTitre";

        if [[ $# -eq 2 ]]
        then chapitreNo=$2
        else (( chapitreNo++ ));
        fi
        setChapitreNo $chapitreNo;

        # avertissement en cas de depassement de chapitre
        if [[ $chapitreNo -gt $chapitreNb ]]
        then
            erreursAddEcho "Chapitre actuel $chapitreNo/$chapitreNb$chapitreTitreTN";
        fi
        setNameAuto;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function closeChapitre(){
        fctIn "$*";
        # avertissement en cas de manque de chapitre        
        if [ $chapitreNb -gt 0 -a  $chapitreNo -lt $chapitreNb ]
        then
            erreursAddEcho "Il manque un ou plusieurs chapitres $chapitreNo/$chapitreNb$chapitreTitreTN";
        fi
        initChapitre;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


##############
## FICHIERS ##
##############
    function initFichier(){
    fctIn "$*";
        setFichierNo ${1:-0};
        setFichierNb ${2:-$fichierNo};
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function incFichierNb(){
        fctIn "$*";
        ((fichierNb++))
        setFichierNb $fichierNb;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setFichierNb(){
        fctIn "$*";
        fichierNb=${1:-0};
        if [[ "$trackLevel" == 'Fichier' ]];then setTrackNb $fichierNb;fi
        #lzTomeNb; #existe pas
        fichierNoNbT='';if [[ $fichierNb -gt 1 ]];then fichierNoNbT="-F$flz$fichierNo-$fichierNb";fi
        setNameAuto;
        fctOut "${FUNCNAME[0]}(0)";return 0;
   }

    function incFichierNo(){
        fctIn "$*";
        ((fichierNo++))
        setFichierNo $fichierNo;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setFichierNo(){
        fctIn "$*";
        fichierNo=${1:-0};
        if [[ "$trackLevel" == 'Fichier' ]];then tag_track=$fichierNo;fi
        lzFichierNo;
        fichierNoNbT='';if [[ $fichierNb -gt 1 ]];then fichierNoNbT="-F$flz$fichierNo-$fichierNb";fi
        setNameAuto;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #prefixe fichierNo avec un zero si 0..9
    function lzFichierNo(){
        fctIn "$*";
        flz='';
        if [[ $fichierNb -lt 10 ]];then return 0;fi
        if [[ $fichierNo -lt 10 ]];then flz="0" ;fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setFichierTitre(){ 
        fctIn "$*";
        fichierTitre="${1:-''}";
        fichierTitreNormalize='';
        fichierTitreTN='';

        if [[ "$fichierTitre" != '' ]]
        then
            getNormalizeText "$fichierTitre"; fichierTitreNormalize="$normalizeText"; 
            fichierTitreTN="-$fichierTitreNormalize"
        fi
        if [[ "$titreLevel" == 'Fichier' ]]; then setTitle "$fichierTitre"; fi
        setNameAuto;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #Ajoute un Fichier sans changer la valeur de fichiereNo
    #addFichier( 'titre' ){
    function addFichier(){
        fctIn "$*";
        if [[ $# -ne 1 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($@) n'a pas le bon nombre de parametre($#/1). ${FUNCNAME[0]}( \"fichierTitre\" )"
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi
        incFichier "$1" $fichierNo;     # ne change pas la valeur actuelle (utilise)
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #defini le fichierNo ou augmente fichierNo de 1 si non defini en 2eme arg
    #defini le titre du chapitre
    #incFichier "chapitreTitre" [chapitreNo]
    function incFichier(){
        fctIn "$*";
        setFichierTitre "${1:-''}";

        if [[ $# -eq 2 ]]
        then fichierNo=$2
        else (( fichierNo++ ));
        fi
        setFichierNo $fichierNo;

        echo -e "\n---Ajout du Fichier no $fichierNo:$fichierTitre---";

        # avertissement en cas de depassement de fichier
        if [[ $fichierNo -gt $fichierNb ]]
        then
            erreursAddEcho "Fichier actuel $fichierNo/$fichierNb$fichierTitreTN";
        fi
        setNameAuto;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function closeFichier(){
        fctIn "$*";
        # avertissement en cas de manque de fichier        
        if [ $fichierNb -gt 0 -a  $fichierNo -lt $fichierNb ]
        then
            erreursAddEcho "Il manque un ou plusieurs fichiers $fichierNo/$fichierNb";
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#

