echo_D "${BASH_SOURCE[0]}" 1;

##########
# FFMPEG #
##########

    # function ffmpeg_mono "sourcePath" ["$destination(avec ou sans extension)='sourceNom-mono.ext'"}
    # si la destination n'est pas definit alors elle aura les valeurs de la source suffixé par -mono (mais avant l'extention)
    function ffmpeg_mono(){
        if $isTrapCAsk ; then return $E_CTRL_C ; fi
        fctIn "$*";
        titre4 "${FUNCNAME[0]}($*)";

        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($#/1: 'Chemin de la source' ['destination(avec ou sans extension)='sourceNom-mono.ext']) ($*)";
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi

        if [[ $isMono -eq 0 ]]
        then
            titreInfo 'mono:off';
            fctOut "${FUNCNAME[0]}(0)";return 0;
        fi # isMono = 0 donc sortie normal

        local _sourcePath="$1";
        if [[ ! -f "$_sourcePath" ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($*): Le fichier '$_sourcePath' existe pas.";
            fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
        fi

        local _sourceRep=${_sourcePath%/*};
        if [[ "$_sourceRep" == "$_sourcePath" ]];then _sourceRep='.';fi # s'ils sont identique alors on est dans le rep courant
        local _sourceFilename=${_sourcePath##*/};
        local _sourceName=${_sourceFilename%.*};
        local _sourceExt=${_sourceFilename##*.};
        displayVarDebug '_sourcePath' "$_sourcePath" '_sourceRep' "$_sourceRep" '_sourceFilename' "$_sourceFilename" '_sourceName' "$_sourceName" '_sourceExt' "$_sourceExt"

        local _destinationPathPSP="${2:-"$_sourcePath"}"; # avec ou sans ext. Par defaut egale a la source

        local _destinationRep=${_destinationPathPSP%/*};
        # la destination a t'il un repertoire.?
        if [[ "$_destinationRep" == "$_destinationPathPSP" ]]
        then # la destination n'a pas de repertoire
            _destinationRep="$_sourceRep";
        fi
        displayVarDebug '_destinationRep' "$_destinationRep";

        local _destinationExt=${_destinationPathPSP##*.};

        # la destination a t'il une extention?
        if [[ "$_destinationExt" == "$_destinationPathPSP" ]]
        then # la destination n'a pas d'extention
            _destinationExt="$_sourceExt";
        fi
        displayVarDebug '_destinationExt' "$_destinationExt";

        local _destinationPath="$_destinationRep/$_sourceName-mono.$_destinationExt"; #sans ext
        displayVarDebug '_destinationPath' "$_destinationPath";

        if [[ -f "$_destinationPath" ]]
        then
            # Le fichier existe: test de la taille
            local -i _tailleFichier=$(stat -c "%s" "$_destinationPath")
            displayVarDebug '_tailleFichier' "$_tailleFichier";
            if [[ $_tailleFichier -eq 0 ]]
            then
                notifsAddEcho "${FUNCNAME[0]}($*): Le fichier '$_destinationPath' a une taille nulle -> suppression.";
                rm "$_destinationPath";
            else # le fichier existe et a une taille non nulle. Donc le fichier est considéré comme déjà convertit-> on quitte la fonction
                notifsAddEcho "Le fichier mono '$_destinationPath' existe déjà.";
                fctOut "${FUNCNAME[0]}(0)";return 0;
            fi
        fi

        # - CONVERTIR EN MONO - #
        echo "Lancer 'tail -f $LOG_PATH' sur un autre termninal pour voir l'avancement de la convertion";
        #ffmpeg $ffmpeg_params_globaux  -v quiet # ffmpeg -v help
        evalEcho "ffmpeg $ffmpeg_params_globaux -i \"$_sourcePath\" -c:v copy $FFMpegMono \"$_destinationPath\" 1>$LOG_PATH 2>$LOG_PATH";
        local -i _erreur=$?;

        if [ $_erreur -eq 0 ]
        then
            notifsAddEcho "${FUNCNAME[0]}($*): la convertion vers la mono est bonne";
        else
            erreursAddEcho "${FUNCNAME[0]}($*): Problème lors de la convertion vers la mono (ffmpeg: $_erreur)";
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#

    #ffmpeg_extract_audio ( "src_file_path" "dest_filePath" "time_beg(HH:MM:SS.sss)" "time_end(HH:MM:SS.sss)" )
    function ffmpeg_extract_audio(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "$*";
        if [[ $# -lt 3 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}[$#/3] ('src_file_path' 'dest_filePath' 'time_beg(HH:MM:SS.sss)' 'time_end(HH:MM:SS.sss)') ($*)";
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi

        local src_file_path="$1";
        local dest_filePath="$2";
        local time_beg="$3";
        local time_fin=${4:-''};
        time_fin_Param='';
        if [[ "$time_fin" != '' ]];then time_fin_Param=" -to $time_fin" ;fi
        
        if [[ ! -f "$src_file_path" ]]
        then
            erreursAddEcho "Le fichier source: $src_file_path existe pas.";
            fctOut "${FUNCNAME[0]}(0)";return 0;
        fi

        if [[ ! -f "$dest_filePath" ]] # et si long = 0
        then
            evalEcho "ffmpeg $ffmpeg_params_globaux -i $src_file_path -ss $time_beg$time_fin_Param -c copy -vn $dest_filePath";
            if [[ $? -eq 0 ]]
                then notifsAddEcho  "EXTRACTION de $src_file_path \nvers $dest_filePath: OK";
                else erreursAddEcho "EXTRACTION de $src_file_path \nvers $dest_filePath: FAIL";
                fctOut "${FUNCNAME[0]}(0)";return 0;
            fi
        else
            notifsAddEcho "EXTRACTION de $src_file_path \nvers $dest_filePath: déjà faite";
        fi

        #evalEcho "chDroit \"$dest_filePath\""
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    # ffmpeg_concat ( 'destPath' 'filePath1' .. 'filePathN' )
    function ffmpeg_concat(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "$*";
        if [[ $# -lt 2 ]]
        then
            erreursAddEcho "$FUNCNAME ('dest_filePath' 'filePath1' .. 'filePathN' )";
            fctOut "${FUNCNAME[0]}($E_ARG_BAD)"; return $E_ARG_BAD;
        fi

        local dest_filePath="$1"; shift;

        if [[ ! -f "$2" ]]
        then
            erreursAddEcho "Le fichier source: $2 existe pas.";
            fctOut "${FUNCNAME[0]}(0)";return 0;
        fi

        if [[ ! -f "$dest_filePath" ]] # et si long = 0
        then
            evalEcho "ffmpeg $ffmpeg_params_globaux -i "concat:$2|$3"  $dest_filePath";
            if [[ $? -eq 0 ]]
            then
                notifsAddEcho  "CONCATENATION vers $dest_filePath";
            else
                erreursAddEcho "CONCATENATION vers $dest_filePath";
                fctOut "${FUNCNAME[0]}(0)";return 0;
            fi
        else
            notifsAddEcho "CONCATENATION vers '$dest_filePath': déjà faite";
        fi

        #evalEcho "chDroit \"$dest_filePath\""
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    # convertFile( 'fichierSansExt' 'extSource' 'extDest' )
    function convertFile(){
        if $isTrapCAsk ; then return $E_CTRL_C ; fi
        fctIn "$*";
        if [[ $? -ne 3 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($*): Nombre de parametre insuffisant $#/3."
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi
        local fichier="$1";
        local ext1="$2";
        local ext2="$3";
        if [[ ! -f "$fichier" ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($*): Le fichier '$fichier' existe pas.";
            fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
        fi
        evalEcho "ffmpeg $ffmpeg_params_globaux -loglevel error -i \"$fichier.$ext1\" \"$fichier.$ext2\""
        fctOut "$FUNCNAME";return 0
    }


    #concat
    #fmpeg -i concat:"partie1.avi|partie2.avi" -c copy film.avi


    #extractToMP4( src.srcExt [dest])
    function extractToMP4(){
        if $isTrapCAsk ; then return $E_CTRL_C ; fi
        fctIn "$*";
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($*): Nombre de parametre insuffisant $#/3. ${FUNCNAME[0]}( src.srcExt [dest])"
            fctOut "$FUNCNAME";return $E_ARG_REQUIRE;
        fi
        local srcPath="$1";
        local srcSExt="${srcPath%.*}";
        local destSExt="${2:-"$srcSExt"}";
        local destExt='mp4';

        #displayVar "srcPath" "$srcPath";
        #displayVar "srcSExt" "$srcSExt";
        #displayVar "destSExt" "$destSExt";
        #displayVar "destExt" "$destExt";

        if [[ ! -f "$srcPath" ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($*): Le fichier '$srcPath' existe pas.";
            fctOut "$FUNCNAME";return E_INODE_NOT_EXIST;
        fi
        evalEcho "ffmpeg -i \"$srcPath\" -codec copy -bsf:v mpeg4_unpack_bframes  \"$destSExt.$destExt\"";
        fctOut "$FUNCNAME";return 0;
    }


    #extractToOGG( src.srcExt [dest])
    function extractToOGG(){
        if $isTrapCAsk ; then return $E_CTRL_C; fi
        fctIn "$*";
        if [[ $# -eq 0 ]]
        then
            erreurAddEcho "Nombre de parametre insuffisant $#/1:usage: extractToOGG( src.srcExt [dest])";
            fctOut "$FUNCNAME";return $E_ARG_REQUIRE;
        fi
        local srcPath="$1";
        local srcSExt="${srcPath%.*}";
        local destSExt="${2:-"$srcSExt"}";
        local destExt="ogg";

        #displayVar "srcPath" "$srcPath";
        #displayVar "srcSExt" "$srcSExt";
        #displayVar "destSExt" "$destSExt";
        #displayVar "destExt" "$destExt";

        if [[ ! -f "$srcPath" ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($*): Le fichier '$srcPath' existe pas.";
            fctOut "$FUNCNAME";return $E_INODE_NOT_EXIST;
        fi
        evalEcho "ffmpeg $ffmpeg_params_globaux -i \"$srcPath\" -vn -acodec libvorbis  \"$destSExt.$destExt\"";
        fctOut "$FUNCNAME";return 0;
    }

##########
## MONO ##
##########
    #function setMono [isMono=1]
    function setMono(){
        fctIn "$*";
        isMono=${1:-1}
        FFMpegMono='';
        if [[ $isMono -eq 1 ]];then FFMpegMono=' -ac 1';fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


#############
## CONVERT ##
#############
    # bacckup la variable convert
    function saveConvert(){
        fctIn "$*";
        #displayVar 'convert' "$convert"
        convertBak="$convert";
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    # supprime la demande de convertion et sauvegarde la valeur precedente.
    # doit etre restauté avec restaureConvert
    function noConvert(){
        fctIn "$*";
        convertBak="$convert";
        convert='';
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function restaureConvert(){
        fctIn "$*";
        convert="$convertBak";
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setConvert(){
        fctIn "$*";

        case "$1" in
            "ogg" )
                saveConvert
                convert='ogg';
            ;;
            *)
                saveConvert
                convert='';
            ;;
        esac
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #convertir( fichierSource )
    function convertir(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "${FUNCNAME[0]}($*, convert='$convert')";
        # si pas de convertion demandé alors on quitte normalement
        if [[ "$convert" == '' ]]; then fctOut "${FUNCNAME[0]}(0)";return 0;fi 

        titre4 'Convertir'
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho ". ${FUNCNAME[0]}[$#/2](\"$url\" \"$destination\")";
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi

        ((stats['cv_total']++))

        local fileName="$1";

        local notifTxt='';
        local erreurTxt='';

        fileNameSansExt=${fileName%.*};
        local fileNameConvert="$fileNameSansExt.$convert";
        # si le fichier est deja convertit
        if [[ -f "$fileNameConvert" ]]
        then
            notifsAddEcho "${FUNCNAME[0]}($*): Le fichier '$fileNameConvert' existe deja.";
            ((stats['cv_exist']++))

            if [[ $isSaveTags -eq 1 ]]
            then
                saveTags "$fileNameConvert";
            fi

            fctOut "$FUNCNAME";return 0;
        fi

        # si le fichier a convertir existe pas
        if [[ ! -f "$fileName" ]]
        then
            ((stats['cv_fail']++))
            fctOut "$FUNCNAME";return $E_INODE_NOT_EXIST;
        fi

        #canaux audio
        # https://trac.ffmpeg.org/wiki/AudioChannelManipulation
        # https://superuser.com/questions/702552/ffmpeg-audio-stereo-to-mono-using-only-left-channel
        # ffmpeg -i stereo.flac -ac 1 mono.flac
        # Left channel to mono:    ffmpeg -i video.mp4 -map_channel 0.1.0 -c:v copy mono.mp4
        # Left channel to stereo:  ffmpeg -i video.mp4 -map_channel 0.1.0 -map_channel 0.1.0 -c:v copy stereo.mp4
        # right channel to mono:   ffmpeg -i video.mp4 -map_channel 0.1.1 -c:v copy mono.mp4
        # right channel to stereo: ffmpeg -i video.mp4 -map_channel 0.1.1 -map_channel 0.1.1 -c:v copy stereo.mp4


        # - CONVERTION - #  
        evalEcho "ffmpeg $ffmpeg_params_globaux -loglevel $ffmpeg_loglevel -i '$fileName'$FFMpegMono '$fileNameConvert'";
        if [[ $? -eq 0 ]]
        then
            saveTags "$fileNameConvert";
            notifTxt='';
            notifsAddEcho "${FUNCNAME[0]}($*): '$fileNameConvert'";
            ((stats['cv_ok']++))
        else
            ereurAddEcho "${FUNCNAME[0]}($*): '$fileNameConvert'";
            ((stats['cv_fail']++))
        fi
        fctOut "$FUNCNAME";return 0;
    }
#

##############
## SLPITTER ##
##############
    #https://sebastien-lhuillier.com/index.php/ffmpeg/item/148-extraire-une-partie-de-la-video-a-deux-temps-donnes

    #function splitter( 'sourcePath' 'destinationPath' 'debut HH:MM:SS.000' 'fin HH:MM:SS.000' ){
    function splitter(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "$*";
        if [[ $# -ne 4 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}[$#/4](\"sourcePath\" \"destinationPath(sans ext)\" \"debut HH:MM:SS.000\" \"fin HH:MM:SS.000\")";
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi

        titre4 "Découpage ($FUNCNAME $*)";
        local _sourcePath="$1";
        local _srcExtension=${_sourcePath##*.};
        local _destinationPath="$2";    # sans extension
        local _debut="$3";
        local _fin="$4";
        #echo_D "_destinationPath" "$_destinationPath";
        if [[ -f "$_destinationPath.$_srcExtension" ]]
        then
            notifsAddEcho "Le fichier $_destinationPath est déjà splitter";
        else            

            evalEcho "ffmpeg $ffmpeg_params_globaux -i $_sourcePath -ss $_debut -to $_fin -c copy -q:a 0 -q:v 0 \"$_destinationPath.$_srcExtension\"";
            if [[ $? -eq 0 ]]
            then
                notifsAddEcho "Découpage de '$_sourcePath' vers '$_destinationPath.$_srcExtension'";
                saveTags "$_destinationPath.$_srcExtension";
            else
                erreursAddEcho "Découpage de '$_sourcePath' vers '$_destinationPath.$_srcExtension'";
            fi
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#

###########
## MERGE ##
###########
    # function dlyt_merge $referenceVideoYoutube" "$destination(sans extension)" [videoFormat] [audioFormat]
    # Telecharge le flux video au format 'videoFormat' et le flux audio au format 'audioFormat' séparement et les fusionnent
    function dlyt_merge(){
        if $isTrapCAsk ; then return $E_CTRL_C ; fi
        fctIn "$*";
        if [[ $# -lt 2 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($@) n'a pas le bon nombre de parametre($#/2-4). ${FUNCNAME[0]}(\"$referenceVideoYoutube\" \"$destination\" [videoFormat] [audioFormat])"
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi

        local codeYT="$1";
        local file_name="$2"; #sans ext
        local -i videoFormat=${3:-399};
        local -i audioFormat=${4:-140};

        setTitle "$file_name";
        setDureeYT "$codeYT";
        local -i _err=$?;
        
        dlyt "$codeYT" "$file_name";
        #displayVarDebug 'titreNormalize' "$titreNormalize" # titreNormalize initialisé par dlyt()
        if [[ -f "$titreNormalize.mp4" ]]
        then
            fctOut "${FUNCNAME[0]}(0)";return 0;
        fi

        # dl video
        titre4 "Télécharger: dlyt video($videoFormat)";
            saveFormat $videoFormat;
            dlyt "$codeYT" "$titreNormalize-$videoFormat";
            restaureFormat;
        # dl audio
        titre4 "Télécharger: dlyt audio($audioFormat)";
            saveFormat $audioFormat;
            dlyt "$codeYT" "$titreNormalize-$audioFormat";
            restaureFormat;

        # merge
        titre4 'MERGE: fusion';
        #if [[ $isMono -eq 0 ]]
        #then evalEcho "ffmpeg $ffmpeg_params_globaux  -i \"$titreNormalize-$videoFormat.mp4\"  -i \"$titreNormalize-$audioFormat.m4a\" -c:v copy  \"$titreNormalize.mp4\"";
        evalEcho "ffmpeg $ffmpeg_params_globaux -i \"$titreNormalize-$videoFormat.mp4\"  -i \"$titreNormalize-$audioFormat.m4a\"$FFMpegMono -c:v copy  \"$titreNormalize.mp4\"";
        #fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#