echo_D "${BASH_SOURCE[0]}" 1;



##########
## TAGS ##
##########
    #id3tool [<options>] <filename>
    #  -t, --set-title=WORD		Sets the title to WORD
    ##  -a, --set-album=WORD		Sets the album to WORD
    #  -r, --set-artist=WORD		Sets the artist to WORD
    #  -y, --set-year=YEAR		Sets the year to YEAR [4 digits]
    #  -n, --set-note=WORD		Sets the note to WORD
    #  -g, --set-genre=INT		Sets the genre code to INT
    # http://id3.org/id3v2-00 #A.3.   Genre List
    #28.Vocal
    #  -G, --set-genre-word=WORD	Sets the genre to WORD
    #  -c, --set-track=INT		Sets the track number to INT
    #  -l, --genre-list		Shows the Genre's and their codes
    #  -v, --version			Displays the version
    #  -h, --help			Displays this message


    function showTags(){
        fctIn "$*";
        titre3 'Les tags:';
        displayVar 'tag_language' "$tag_language";

        displayVar 'tag_album   ' "$tag_album"      'albumTitreNormalize' "$albumTitreNormalize" 'albumTitreTN' "$albumTitreTN";
        displayVar 'tag_title   ' "$tag_title"      'titreNormalize'      "$titreNormalize" 'titreTN' "$titreTN";
        displayVar 'tag_track   ' "$tag_track"      'tag_trackNb'         "$tag_trackNb";

        displayVar 'tag_artist  ' "$tag_artist"     'auteurNormalize'     "$auteurNormalize"          'auteurTN'       "$auteurTN";
        displayVar 'tag_genreNo ' "$tag_genreNo"    'tag_genre'           "$tag_genre";

        displayVar 'tag_year    ' "$tag_year"       'dl_date'             "$dl_date";
        displayVar 'tag_url     ' "$tag_url";

        displayVar 'dl_voix     ' "$dl_voix"        'dl_voixNormalize'    "$dl_voixNormalize"         'dl_voixTN'      "$dl_voixTN";
        displayVar 'dl_source   ' "$dl_source"      'dl_sourceNormalize'  "$dl_sourceNormalize"       'dl_sourceTN'    "$dl_sourceTN";
        displayVar 'dl_duree    ' "$dl_duree";

        displayVar 'dl_editeur  ' "$dl_editeur";
        displayVar 'dl_prenote  ' "$dl_prenote"     'tag_note'           "$tag_note" 'dl_postnote' "$dl_postnote";
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    # Affiche kes tags utilisés par les programmes
    function showTagsProg(){
        fctIn "$*";
        
        if [ $verbosity -ge 1 ]
        then
            titre3 'Les tags des programmes:';
            displayVar 'vorbis_tags' "$vorbis_tags"
            displayVar 'vorbis_tag_comment' "$vorbis_tag_comment"
            displayVar 'id3v2_tags' "$id3v2_tags"
            displayVar 'id3v2_tag_comment' "$id3v2_tag_comment"
            displayVar 'ffmpeg_tags' "$ffmpeg_tags"

            displayVar 'coverFrontPathTmpFile' "$coverFrontPathTmpFile"
            displayVar 'coverFFmpegTags' "$coverFFmpegTags"
            displayVar 'ffmpeg_tags' "$ffmpeg_tags"
            displayVar 'tag_note' "$tag_note"
            displayVar 'tag_note_mp3' "$tag_note_mp3"
            displayVarDebug 'tag_comment' "$tag_comment"
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function clearTags(){
        fctIn;
        # if use "declare" -> local
        tag_language='fr';
        # - collection - #
        setArtist '';  # auteurs
        tag_genreNo=0;  tag_genre='';

        # - livre - #
        setAlbumTitre '';   # titre du cycle
        setTitle '';   # titre du livre/chanson/etc
        setTrack;
        setYear 0;

        tag_url='';

        dl_prenote=''; tag_note=''; dl_postnote='';

        setDl_date '';
        setDl_voix '';
        setDl_source '';
        setDl_duree '';
        dl_editeur='';

        initCovers;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    # - Calcul les tag - #
    # entrée unique pour calculer les tagsProgs
    # https://ffmpeg.org/doxygen/trunk/group__metadata__api.html
    # https://wiki.multimedia.cx/index.php?title=FFmpeg_Metadata (les metadatas acceptés par FFMPEG selon le format)
    # https://www.abonnel.fr/informatique/cfp/ffmpeg_mp3_tag_metadata
    # https://write.corbpie.com/adding-metadata-to-a-video-or-audio-file-with-ffmpeg/
    function setTags(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "$*";
        local _extension=${1:-''};
        titre4 "${FUNCNAME[0]}";

        vorbis_tags='';
        id3v2_tags='';
        ffmpeg_tags=' -map 0:0 ';

        if [ "$tag_artist" != '' ]
        then
            vorbis_tags="$vorbis_tags --tag \"ARTIST=$tag_artist\"";
            id3v2_tags="$id3v2_tags --artist \"$tag_artist\"";
            #ffmpeg_tags="$ffmpeg_tags -metadata artist=\"$tag_artist\"";
            case "$_extension" in
                'm4a'|'mp4'|'mov' )
                    ffmpeg_tags="$ffmpeg_tags -metadata artist=\"$tag_artist\"";    # verif l 2024.04.23 c'est bien 'artist et non "author'
                    ;;
                'asf'|'wma'|'wmv'|'rm'|'nut')
                    ffmpeg_tags="$ffmpeg_tags -metadata author=\"$tag_artist\"";
                    ;;
                'mp3'|'avi')
                    ffmpeg_tags="$ffmpeg_tags -metadata artist=\"$tag_artist\"";
                    ;;
            esac
        fi

        # https://fr.wikipedia.org/wiki/ISO_8601 (AAAA-MM-JJTHH:MM:SS,ss-/+FF:ff)
        if [ $tag_year -ne 0 ]
        then
            vorbis_tags="$vorbis_tags --tag \"DATE=$tag_year\"";
            id3v2_tags="$id3v2_tags --year $tag_year";
            #ffmpeg_tags="$ffmpeg_tags -metadata date=\"$tag_year\"";
            case "$_extension" in
                'm4a'|'mp4'|'mov')
                    #ffmpeg_tags="$ffmpeg_tags -metadata year=\"$tag_year\"";
                    ffmpeg_tags="$ffmpeg_tags -metadata date=\"$tag_year\"";    # api ffmpeg
                    #ffmpeg_tags="$ffmpeg_tags -metadata date=\"$dl_date\"";     
                    
                    ;;
                'mp3'|'avi')
                    ffmpeg_tags="$ffmpeg_tags -metadata date=\"$tag_year\"";
                    ;;
            esac
        fi

        if [ "$tag_album"    != '' ]
        then
            vorbis_tags="$vorbis_tags --tag \"ALBUM=$tag_album\"";
            id3v2_tags="$id3v2_tags --album \"$tag_album\"";
            ffmpeg_tags="$ffmpeg_tags -metadata album=\"$tag_album\"";
        fi

        if [ "$tag_title"    != '' ];
        then
            vorbis_tags="$vorbis_tags --tag \"TITLE=$tag_title\"";
            id3v2_tags="$id3v2_tags --song \"$tag_title\"";
            ffmpeg_tags="$ffmpeg_tags -metadata title=\"$tag_title\"";
        fi

        if [ "$tag_genre"    != '' ]
        then
            vorbis_tags="$vorbis_tags --tag \"GENRE=$tag_genre\"";
            id3v2_tags="$id3v2_tags --genre $tag_genreNo";
            ffmpeg_tags="$ffmpeg_tags -metadata genre=\"$tag_genre\"";
        fi

        if [  $tag_track     -ne 0 ]
        then
            vorbis_tags="$vorbis_tags --tag \"TRACKNUMBER=$tag_track\"";
            id3v2_tags="$id3v2_tags --track $tag_track/$tag_trackNb"; 
            #ffmpeg_tags="$ffmpeg_tags -metadata track=\"$tag_track/$tag_trackNb\"";
            case "$extension" in
                'm4a'|'mp4'|'mov'|'avi')
                    ffmpeg_tags="$ffmpeg_tags -metadata track=\"$tag_track/$tag_trackNb\"";     # m4a:ok
                    #ffmpeg_tags="$ffmpeg_tags -metadata track=\"$tag_track\"";
                    ffmpeg_tags="$ffmpeg_tags -metadata episode_id=\"$tag_track\"";
                    ;;
                'mp3')
                    ffmpeg_tags="$ffmpeg_tags -metadata track=\"$tag_track/$tag_trackNb\"";
                    ;;
            esac
        fi

        if [  $tag_trackNb   -ne 0 ]
        then
            vorbis_tags="$vorbis_tags --tag \"TRACKTOTAL=$tag_trackNb\"";
            ffmpeg_tags="$ffmpeg_tags -metadata tracktotal=\"$tag_trackNb\"";           # Pas reconnu ffmpg
            case "$extension" in
                'm4a'|'mp4'|'mov'|'avi')
                    ffmpeg_tags="$ffmpeg_tags -metadata tracknb=\"$tag_trackNb\"";      # m4a/?:Pas reconnu ffmpg
                    ffmpeg_tags="$ffmpeg_tags -metadata tracks=\"$tag_trackNb\"";       # m4a/?:Pas reconnu ffmpg
                    ;;
            esac
        fi

        if [ "$tag_language" != '' ]
        then
            vorbis_tags="$vorbis_tags --tag \"language=$tag_language\"";
            #id3v2_tags="$id3v2_tags --language \"$tag_language\"";                     # Pas reconnu par idv3
            ffmpeg_tags="$ffmpeg_tags -metadata language=\"$tag_language\"";            # m4a/?:Pas reconnu ffmpg
            ffmpeg_tags="$ffmpeg_tags -metadata lang=\"$tag_language\"";                # m4a/?:Pas reconnu ffmpg
        fi

        # - Commentaire: preparation url - #
        if [ "$tag_url"      != '' ]
        then
            local tag_url_mp3="$tag_url";

            vorbis_tags="$vorbis_tags --tag \"url=$tag_url\"";
            ffmpeg_tags="$ffmpeg_tags -metadata url=\"$tag_url\"";

            tag_url_mp3=$(echo $tag_url_mp3 | tr -d ':');
            tag_url_mp3=$(echo $tag_url_mp3 | tr '/' '-');
            #id3v2_tags="$id3v2_tags --url \"$tag_url_mp3\"";           # Pas reconnu par idv3

            # on ajoute l'url a la note car peu de logiciel lisent le tag url
            tag_note_mp3="$tag_note $tag_url_mp3.";
            tag_note="$tag_note $tag_url.";             # doit etre apres: tag_note_mp3="$tag_note $tag_url_mp3.";
        fi

        # - Commentaire :Finalisation - #

        tag_comment="$dl_prenote$tag_note$dl_postnote"
        if [[ "$tag_comment" != '' ]]
        then
            # VORBIS
            vorbis_tag_comment='';
            #tag_comment="$dl_prenote$tag_note$dl_postnote"             # deja calculer
            vorbis_tag_comment=" --tag \"DESCRIPTION=$tag_comment\"";   # tag comment seul
            vorbis_tags=" $vorbis_tags$vorbis_tag_comment ";            # tous les tags + comment

            # MP3
            id3v2_tag_comment=' '
            tag_comment="$dl_prenote$tag_note_mp3$dl_postnote";
            id3v2_tag_comment=" --comment \"$tag_comment\"";            # tag comment seul
            id3v2_tags="$id3v2_tags$id3v2_tag_comment";                 # tous les tags + comment

            # FFMPEG
            #ffmpeg_tag_comment=" -metadata comment=\"$tag_comment\""
            case "$_extension" in
                'm4a'|'mp4'|'mov'|'avi'|\
                'asf'|'wma'|'wmv'|\
                'rm' |'sox')
                    ffmpeg_tags="$ffmpeg_tags -metadata comment=\"$tag_comment\"";
                    ;;
                'mp3')  # non pris officiellement en charge
                    ffmpeg_tags="$ffmpeg_tags -metadata comment=\"$tag_comment\"";
                    ;;
                'mkv')
                    ffmpeg_tags="$ffmpeg_tags -metadata description=\"$tag_comment\"";
                ;;
            esac
        fi

        # - Cover: ajout dans les tags - #
        setCoverFFmpegTags;
        if [[ "$coverFFmpegTags" != '' ]]
        then
            ffmpeg_tags="$coverFFmpegTags$ffmpeg_tags";
            displayVarDebug $LINENO'ffmpeg_tags' "$ffmpeg_tags";
        fi

        #showTagsProg;
        showTags;

        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    # - ecrit les tags dans le fichier - #   
    # saveTags
    # $1: fichier a taguer
    function saveTags(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        #echo "$LINENO";return 0; # pour test
        fctIn "$*";
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho " ${FUNCNAME[0]}[$#/1]('Fichier a tagguer') ($*)"
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi

        # https://id3.org/id3v2.3.0
        # https://id3.org/id3v2.3.0#Attached_picture
        # https://stackoverflow.com/questions/18710992/how-to-add-album-art-with-ffmpeg
        #   ffmpeg $ffmpeg_params_globaux -i in.mp3 -i cover.png -codec copy         -map 0:0 -map 1:0 -id3v2_version 3 -metadata:s:v title="Album cover" -metadata:s:v comment="Cover (front)" out.mp3
        #   ffmpeg $ffmpeg_params_globaux -i in.mp3 -i cover.png -c:a copy -c:v copy -map 0:0 -map 1:0 -id3v2_version 3 -metadata:s:v title="Album cover" -metadata:s:v comment="Cover (front)" out.mp3
        # https://superuser.com/questions/1208273/add-new-and-non-defined-metadata-to-a-mp4-file
        # https://wiki.multimedia.cx/index.php/FFmpeg_Metadata
        titre4 'Sauvegarde des tags';
        local fileName="$1";
        local extension="${fileName##*.}";
        local tagVersion='';
        setTags "$extension";
        #showDatas

        case "$extension" in 
            #format valide 
            #vorbiscomment: https://www.xiph.org/vorbis/doc/v-comment.html

            'ogg' | 'ogm' | 'flac' )
                # https://wiki.xiph.org/index.php?title=VorbisComment&mobileaction=toggle_view_desktop#Cover_art
                # https://dogphilosophy.net/?page_id=66

                # - ajout des tags sauf cover - #
                if [[ "$vorbis_tags" != '' ]]
                then
                    echo_D $LINENO': ajout des tags sauf cover';
                    ls -l "$fileName";
                    evalEcho "vorbiscomment --raw -w $vorbis_tags \"$fileName\"";
                fi

                saveCover "$fileName";
                #getcoverLevel
                #showCovers

                # - Verification - #
                #vorbiscomment --list $fileName
            ;;


            "mp4")
                local _tags='';
                if [[ "$tag_artist" != '' ]]; then _tags="$_tags -metadata Artist=\"$tag_artist\""; fi
                if [[ "$tag_year"   != '' ]]; then _tags="$_tags -metadata date=\"$tag_year\""; fi
                if [[ "$tag_album"  != '' ]]; then _tags="$_tags -metadata Album=\"$tag_album\""; fi
                if [[ "$tag_title"  != '' ]]; then _tags="$_tags -metadata Title=\"$tag_title\""; fi
                #if [[ $tag_genreNo  -ne 0 ]]; then _tags="$_tags -metadata genre=$tag_genreNo"; fi
                if [[ "$tag_genre"  != '' ]]; then _tags="$_tags -metadata Genre=\"$tag_genre\""; fi
                if [[ "$tag_track"  -ne 0 ]]; then _tags="$_tags -metadata Track=$tag_track"; fi
                #if [[  $tag_trackNb   -ne 0 ]]; then _tags="$_tags -metadata \"TRACKCOUNT=$tag_trackNb\""; fi
                #if [[ "$tag_language" != '' ]]; then _tags="$_tags -metadata \"language=$tag_language\""; fi
                #/$tag_trackNb"; fi

                local _comment="$tag_note";

                # ajout de l'url en fin de tag_note
                if [[ "$tag_url" != '' ]]
                then
                    #echo_D "$LINENO: ajout de l'url($tag_url)"
                    _comment="$tag_comment URL=$tag_url";
                fi

                # filtrage du comment
                if [[ "$tag_note" != '' ]]
                then
                    echo_D "$LINENO:Filtrage du commentaire";
                    #_comment=$(echo $tag_comment | tr -d ':');
                    #_comment=$(echo $tag_comment | tr '/' '-'); #ne fonctionne pas
                fi

                # - comment final - #
                _comment="$dl_prenote$tag_comment$dl_postnote"
                if [[ "$tag_comment" != '' ]]
                then
                    echo_D "$LINENO: création du commentaire finalisé";
                    _tags="$_tags -metadata Comment=\"$tag_comment\"";
                fi

                evalEcho "ffmpeg $ffmpeg_params_globaux -loglevel $ffmpeg_loglevel -i \"$fileName\" $_tags -codec copy \"$fileName.metadata.$extension\"";
                if [[ -f "$fileName.metadata.$extension" ]]
                then
                    mv "$fileName.metadata.$extension" "$fileName";
                else
                    erreursAddEcho "${FUNCNAME[0]}($*) Erreur lors de la convertion du fichier '$fileNameConvert'";
                fi
            ;;


            #format non compatible
            #* )
            #    erreursAddEcho "${FUNCNAME[0]}($*): Format $extension non compatible id3";
            #   
            * )
            echo "$LINENO: Format autre";
                # https://help.mp3tag.de/main_tags.html (mapping des tags)

                # ecriture du cover (Avant d'ecrite les autres tags)
                #addCoverLocal

                tagVersion=' -id3v2_version 3';

                if [[ "$id3v2_tags" != '' ]]
                then
                    displayVarDebug 'LINENO' "$LINENO";
                    #showDatas
                    # - ajout des tags sauf cover (avec id3v2) - #
                    #evalEcho "id3v2 --delete-all  \"$fileName\"";
                    #evalEcho "id3v2 $id3v2_tags \"$fileName\"";
                    #evalEcho "id3v2 $id3v2_tag_comment \"$fileName\"";

                    # utilisation de ffmpeg plutot que idv3 trop limmité
                    evalEcho "ffmpeg $ffmpeg_params_globaux -loglevel $ffmpeg_loglevel -i \"$fileName\" $ffmpeg_tags -codec copy \"$fileName.metadata.$extension\"";
                    if [[ $? -eq 0 ]]
                    then
                        eval_echo_D "mv \"$fileName.metadata.$extension\" \"$fileName\"";
                    else
                        erreursAddEcho "${FUNCNAME[0]}($*): Erreur lors de la création du fichier temporaire '$fileName.metadata.$extension'";
                    fi

                    #echo  "Verification des tags";
                    #evalEcho "id3v2 --list \"$fileName\""

                else
                    echo 'Pas de tag de défini.';
                fi
                ;;
        esac;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    } #saveTags


    function saveTagsLivreAuto(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "$*";
        saveTags "$livreTitreNormalize_Auto"
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#



    #setArtist( "$tag_title" ) si '' = ''
    function setArtist(){
        fctIn "$*";
        tag_artist="${1:-""}";
        auteurNormalize='';
        auteurTN='';
        if [ "$tag_artist" != '' ]
        then
            getNormalizeText "$tag_artist"; auteurNormalize="$normalizeText";
            auteurTN="-$auteurNormalize"
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #setTitle( "$tag_title" )
    function setTitle(){
        fctIn "$*";
        tag_title=${1:-''};
        titreNormalize='';
        titreTN='';
        if [[ "$tag_title" != '' ]]
        then
            getNormalizeText "$tag_title";  titreNormalize="$normalizeText";
            titreTN="-$titreNormalize";
        fi
        setNameAuto;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #setYear( tag_year )
    function setYear(){
        fctIn "$*";
        if [[ $# -eq 0 ]]
        then
            tag_year=0;
            dl_date='';
        else
            if [[ $1 -ne 0 ]]
            then
                tag_year=$1;
                dl_date="$tag_year";
            fi
        fi
        #echo $LINENO;displayVar 'tag_year' "$tag_year" 'dl_date' "$dl_date"
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #setDl_date( 'mm.jj' [year] )
    function setDl_date(){
        fctIn "$*";
        local mmjj=${1:-''}
        if [[ $# -eq 2 ]];then tag_year=$2; fi
        dl_date='';
        if [[ $tag_year -gt 0 ]];then   dl_date="$tag_year";fi
        if [ "$dl_date" != '' -a "$mmjj" != '' ]; then dl_date="$dl_date.$mmjj";fi
        #displayVar $LINENO':tag_year' "$tag_year" 'dl_date' "$dl_date"
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    function setTrackNb(){
        fctIn "$*";
        tag_trackNb=$1
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #incTrackNb
    function incTrackNb(){
        fctIn;
        ((tag_trackNb++));
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #incTrack 
    function incTrack(){
        fctIn;
        ((tag_track++));
        if [[ $tag_track -gt $tag_trackNb ]]
        then
            erreursAddEcho "Track $tag_track/$tag_trackNb"
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #setTrack trackNo [trackNb]
    function setTrack(){
        fctIn "$*";
        if [[ $# -eq 0 ]]
        then
            tag_track=0;
            tag_trackNb=0;
            fctOut "${FUNCNAME[0]}($E_ARG_NONE)"; return $E_ARG_NONE;
        fi
        tag_track=$1;

        if [[ $# -eq 2 ]]; then setTrackNb $2; fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    function setDl_voix(){
        fctIn "$*";
        dl_voix=${1:-''}
        dl_voixNormalize='';
        dl_voixTN='';
        if [[ "$dl_voix" != '' ]]
        then
            getNormalizeText "$dl_voix";  dl_voixNormalize="$normalizeText";
            dl_voixTN="-$dl_voixNormalize";
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setDl_source(){
        fctIn "$*";
        dl_source=${1:-''}
        dl_sourceNormalize=''
        dl_sourceTN='';
        if [[ "$dl_source" != '' ]]
        then
            getNormalizeText "$dl_source";  dl_sourceNormalize="$normalizeText";
            dl_sourceTN="-$dl_sourceNormalize"
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setDl_duree(){
        fctIn "$*";
        dl_duree="${1:-""}";
        dl_dureeT='';
        if [ "$dl_duree" != '' ];then dl_dureeT="-$dl_duree";fi
        setNameAuto;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #setDureeYT( codeYoutube)
    # recupere la duree de la video YT et apelle setDl_duree()
    function setDureeYT(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "$*";
        if [[ $# -ne 1 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}() n'a pas le bon nombre de parametre($#/1). ${FUNCNAME[0]}(\"codeYoutube\")"
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi

        local _referenceVideoYoutube="$1";
        #displayVar '_referenceVideoYoutube' "$_referenceVideoYoutube";
        if [ "$_referenceVideoYoutube" == '' ]
        then
            erreursAddEcho "${FUNCNAME[0]}() n'a pas le bon nombre de parametre($#/1). ${FUNCNAME[0]}(\"codeYoutube\")"
            fctOut "${FUNCNAME[0]}($E_ARG_BAD)"; return $E_ARG_BAD;
        fi

        titre4 'Recherche de la durée de la vidéo YT:'$_referenceVideoYoutube
        dl_duree='';
        #spliter la duree
        local _duration=$(yt-dlp --get-duration "https://www.youtube.com/watch?v=$_referenceVideoYoutube");
        if [[ $? -ne 0 ]];then return $E_INODE_NOT_EXIST;fi
        local _dureeTbl=($(echo $_duration | tr ":" "\n"))
        local _dureeTblNb=${#_dureeTbl[@]}

        local -i h=0
        local -i m=0
        local -i s=0

        #displayVar '_duration' "$_duration"
        #displayVar '_dureeTblNb' "$_dureeTblNb"
        #echo '_dureeTbl[@]' "${_dureeTbl[@]}"
        if [[ $_dureeTblNb -eq 1 ]]
        then
            #s=${_dureeTbl[0]}
            #displayVar 's0' "$s"
            fctOut "${FUNCNAME[0]}($E_ARG_BAD)"; $E_ARG_BAD; # il y a que des secondes dl_duree=''
        fi

        if [[ $_dureeTblNb -eq 2 ]]
        then
            m=${_dureeTbl[0]};s=${_dureeTbl[1]}
            #displayVar 'm0' "$m" 's1' "$s"
        fi

        if [[ $_dureeTblNb -eq 3 ]]
        then
            h=${_dureeTbl[0]};
            #displayVar '_dureeTbl[1]' "${_dureeTbl[1]}"
            m=10#${_dureeTbl[1]};
            #s=${_dureeTbl[2]};
            #displayVar 'h0' "$h" 'm1' "$m" 's2' "$s"
        fi


        local _mlz="$m"  # 
        if [[ $m -lt 10 ]]; then _mlz="0$m"; fi
        setDl_duree "${h}h$_mlz"
         fctOut "${FUNCNAME[0]}(0)";return 0;
    }
