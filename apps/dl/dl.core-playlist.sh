echo_D "${BASH_SOURCE[0]}" 1;

##############
## PLAYLIST ##
##############

function initPlaylist(){ 
    playlistNo=0;playlistNu=0;playlistNb=0;
    playlist_codes=();
    playlist_titres=();
    playlist_auteur=();
    playlist_chaptires_deb=();
    playlist_chaptires_fin=();

    return 0;
}


# Affioche les informations de l'item de la playlist en cours
function showPlaylist(){

    displayVar 'playlistNo            ' "$playlistNo";
    displayVar 'playlistNu            ' "$playlistNu";
    displayVar 'playlistNb            ' "$playlistNb";
    displayVar 'playlist_codes(nu)    ' "${playlist_codes[$playlistNu]-""}";
    displayVar 'playlist_titres(nu)   ' "${playlist_titres[$playlistNu]-""}";
    displayVar 'playlist_auteur(nu)   ' "${playlist_auteur[$playlistNu]-""}";
    displayVar 'playlist_dl_date(nu)  ' "${playlist_dl_date[$playlistNu]-""}";
    displayVar 'albumTitreTbl(nu)     ' "${albumTitreTbl[$playlistNu]-""}";
    displayVar 'episodeTitreTbl(nu)   ' "${episodeTitreTbl[$playlistNu]-""}";
    displayVar 'playlist_item_deb(nu) ' "${playlist_item_deb[$playlistNu]-""}";
    displayVar 'playlist_item_fin(nu) ' "${playlist_item_fin[$playlistNu]-""}";
}


#incPlaylist ('code' 'titre' (dl_date))
function incPlaylist(){ 
    #fctIn;
    ((playlistNo++));       # la playlist est quand même incrémenté pour garder le bon nombre
    if [[ $# -lt 2 ]]
    then
        erreursAddEcho "${FUNCNAME[0]}($@)  (\"code\" \"titre\" [dl_date]).";
        return $E_ARG_REQUIRE;
    fi
    playlistNb=$playlistNo;
    playlist_codes[$playlistNo]="$1";
    playlist_titres[$playlistNo]="$2";
    playlist_dl_date[$playlistNo]="${3:-""}";
    #fctOut "${FUNCNAME[0]}(0)";
    return 0;
}

##
# setPlaylist_dl_date('code' 'dl_date' 'titre' )
# anciennement: incPlaylist_dl_date()
function setPlaylist_dl_date(){ 
    #fctIn;
    if [[ $# -ne 3 ]]
    then
        erreursAddEcho "${FUNCNAME[0]}($@)  (\"code\" \"titre\" [dl_date]).";
        return $E_ARG_REQUIRE;
    fi
    #((playlistNo++))
    #playlistNb=$playlistNo;
    playlist_codes[$playlistNo]="$1";
    playlist_dl_date[$playlistNo]="$2";
    playlist_titres[$playlistNo]="$3";
    #fctOut "${FUNCNAME[0]}(0)";
    return 0;
}


function setPlaylist_item_time(){ 
    if [[ $# -eq 0 ]]
    then
        erreursAddEcho "${FUNCNAME[0]}($@)  (\"debut:HH,MM,SS.cccc\" \"fin:HH,MM,SS.cccc).\"";
        return $E_ARG_REQUIRE;
    fi
    playlist_item_deb[$playlistNo]="$1";
    playlist_item_fin[$playlistNo]="${2:-"$1"}";
    #fctOut "${FUNCNAME[0]}(0)";
    return 0;
}
