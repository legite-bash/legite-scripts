echo_D "${BASH_SOURCE[0]}" 1;

###########
## COVER ##
###########
function initCovers(){
    fctIn "$*";
    setCoverLevelSelect
    coverLevelUrls['Cover (front)']='';
    coverLevelUrls['yt_file']='';
    coverLevelUrls['yt_file_JPG']='';
    coverLevelUrls['yt_file_PNG']='';
    coverLevelUrls['yt_page']='';

    coverLevelPaths['Cover (front)']='';
    coverLevelPaths['yt_file']='';
    coverLevelPaths['yt_file_JPG']='';
    coverLevelPaths['yt_file_PNG']='';
    coverLevelPaths['yt_page']='';

    coverLevelDescs['Cover (front)']='';
    coverLevelDescs['yt_file']='';
    coverLevelDescs['yt_file_JPG']='';
    coverLevelDescs['yt_file_PNG']='';
    coverLevelDescs['yt_page']='';
    fctOut "${FUNCNAME[0]}(0)";return 0;
}

function showCovers(){
    fctIn "$*";
    titre2 'LES COVERS'

    titre3 'LEVELS'
    displayVar 'COVER_LEVEL_DEFAULT'  "$COVER_LEVEL_DEFAULT" 
        echo '  coverLevelListe[@]  ' "${coverLevelListe[@]}"
    displayVar 'coverLevelSelect   '  "$coverLevelSelect"

    for _coverName in "${coverLevelListe[@]}"; 
    do
        echo "cover: $_coverName";
        displayVar "coverLevelUrls"  "${coverLevelUrls[$_coverName]}"

        local _path="${coverLevelPaths[$_coverName]}";
        local _isPathOK="$_path"
        if [[ ! -f "$_path" ]];then _isPathOK="$WARN$_path$NORMAL"; fi
        displayVar "coverLevelPaths" "$_isPathOK"
    done;

    titre3 'TYPES'

    displayVar 'COVER_TYPE_DEFAULT'  "$COVER_TYPE_DEFAULT" 
        echo '  coverTypesListe[@]' "${coverTypesListe[@]}"
    displayVar 'coverTypeSelect   '  "$coverTypeSelect"

    for _coverName in "${coverTypesListe[@]}"; 
    do
        _path=${coverTypesPaths[$_coverName]:-''}
        displayVar "coverTypesPaths[$_coverName]" "$_path";
        _description=${coverTypesDescs[$_coverName]:-''}
        displayVar "coverDescriptions[$_coverName]" "$_description";
    done;
    fctOut "${FUNCNAME[0]}(0)";return 0;
}

# setCoverTypeSelect ( [coverTypeSelect] )
function setCoverTypeSelect(){
    fctIn "$*";
    coverTypeSelect="$COVER_TYPE_DEFAULT"
    if [[ $# -eq 1 ]]
    then
        local _type="$1"
        for level in "${coverTypesListe[@]}"; 
        do
            if [[ "$level" == "$_type" ]]
            then
                coverTypeSelect="$_type"
                fctOut "${FUNCNAME[0]}($E_TRUE)"; return $E_TRUE;
            fi
        done;
    fi
    fctOut "${FUNCNAME[0]}(0)";return 0;
}

# setCoverLevelSelect ( [coverLevel] )
function setCoverLevelSelect(){
    fctIn "$*";
    coverLevelSelect="$COVER_LEVEL_DEFAULT";
    if [[ $# -eq 1 ]]
    then
        local _lvl="$1"
        for level in "${coverLevelListe[@]}";
        do
            if [[ "$level" == "$_lvl" ]]
            then
                coverLevelSelect="$_lvl"
                fctOut "${FUNCNAME[0]}($E_TRUE)"; return $E_TRUE;
            fi
        done;
    fi
    fctOut "${FUNCNAME[0]}(0)";return 0;
}


## -- Telechargement de cover -- ##

# dlCover "lien" "nomFichier[.ext]" ["description"] ["coverLevel"]
# ajoute un cover (copie l'extension du lien source au fichier destination)
# et l'associe a un type de cover 
# https://unix.stackexchange.com/questions/84915/add-album-art-cover-to-mp3-ogg-file-from-command-line-in-batch-mode
# lame --ti logo.jpg file.mp3
# https://id3.org/id3v2.3.0#Attached_picture
function dlCover(){
    if $isTrapCAsk ;then return $E_CTRL_C;fi
    fctIn "$*";

    if [[ $# -eq 0 ]]
    then
        erreursAddEcho "$FUNCNAME[$?/1-4](lien nomFichier[.ext] [description] [coverType=front}]) ($*)";
        fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
    fi
    local source=${1:-''}   #url
    if [[ "$source"  == '' ]];then fctOut "${FUNCNAME[0]}($E_ARG_BAD)"; return $E_ARG_BAD;fi

    titre4 "Téléchargement du cover: $source"
    local sourceExt=${source##*.}

    local destination=${2:-"$titreNormalize"} #avec sou sans ext
    local destExt=${destination##*.}

    setCoverLevelSelect ${4:-"$coverLevelSelect"}

    coverLevelUrls["$coverLevelSelect"]="$source"

    coverLevelDescs["$coverLevelSelect"]=${3:-''}

    # gestion des extentions non valide
    isExtensionValide "$sourceExt"
    if [[ $? -eq 0 ]]
    then
        notifsAddEcho "Extension $destination.$sourceExt non valide."
        fctOut "${FUNCNAME[0]}($E_ARG_BAD)"; return $E_ARG_BAD;
    fi

    # Si destination n'a pas d'extension (nom==ext) 
    if [[ "$destination" == "$destExt" ]]
    then
        destExt="${sourceExt,,}"; #lowercase
        destination="$destination.$destExt";
        fi

    #normalize le chemin de la destination
    local repCourant=$(pwd);
    getNormalizeText "$destination";
    coverLevelPaths["$coverLevelSelect"]="$repCourant/$normalizeText";
    local _path="${coverLevelPaths["$coverLevelSelect"]}"

    if [[ ! -f "$_path" ]]
    then
        #showCovers
        titre2 "wget \"$_path\" \"$source\""
        wget       --no-verbose --continue --show-progress --progress=bar --output-document="$_path" "$source"
        if [[ $? -ne 0 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($destination): Erreur lors du téléchargement"
        fi
    else
        echo "Cover '$destination' déja téléchargé."
    fi

    # mise a jours de type
    coverTypesPaths["$coverTypeSelect"]="${coverLevelPaths["$coverLevelSelect"]}"
    coverTypesDescs["$coverTypeSelect"]="${coverLevelDescs["$coverLevelSelect"]}"

    fctOut "${FUNCNAME[0]}(0)";return 0;
}


# dlCoverPageYT( URLLogo )
# telecharge le logo (fournis en parametre) d'une page YT
function dlCoverPageYT(){
    if $isTrapCAsk ;then return $E_CTRL_C;fi
    fctIn "$*";
    if [[ $# -ne 1 ]]
    then
        erreursAddEcho "${FUNCNAME[0]}() n'a pas le bon nombre de parametre(?#/1). $FUNCNAME ( URLLogo )";
        fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
    fi

    local url="$1";
    local destinationName="$auteurNormalize-logoYT"
    local destinationPath="$PWD/$destinationName.webp"; #VERIFIER SI format wep systematique!
    titre4 "Téléchargement du cover youtube: $auteurNormalize"

    coverLevelUrls['yt_page']='';
    coverLevelPaths['yt_page']='';

    if [[ -f "$destinationPath" ]]
    then
        notifsAddEcho "${FUNCNAME[0]}($*): $destinationPath déjà téléchargé."
        coverLevelUrls['yt_page']="$url";
        coverLevelPaths['yt_page']="$destinationPath";
    else
        if [[ "${url:0:4}" == 'http' ]]
        then
            coverLevelUrls['yt_page']="$url"
            echo wget --output-document="$destinationPath"  "$url";
                    wget --output-document="$destinationPath"  "$url";
            if [[ $? -eq 0 ]]
            then
                coverLevelPaths['yt_page']="$destinationPath";
            else
            erreursAddEcho "Erreur lors du téléchargement du cover yt: $url"
            fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
            fi
        else
            erreursAddEcho "Le lien du thumbnail du cover est NI un fichier NI un http: $url"
            fctOut "${FUNCNAME[0]}($E_ARG_BAD)"; return $E_ARG_BAD;
        fi
    fi

    #file "${coverLevel['yt_page_path']}"
    #displayVar 'url'                "$url"
    #displayVar 'destinationPath'    "$destinationPath"
    #showCovers

    fctOut "${FUNCNAME[0]}(0)";return 0;
}


# dlCoverFileYT( 'youtube-code' 'destinationPath(sans extension)' )
# telecharge la miniature d'une video youtube et remplit les variable:
# coverLevelUrls['yt_file'];coverLevelPaths['yt_file'];
function dlCoverFileYT(){
    if $isTrapCAsk ;then return $E_CTRL_C;fi
    fctIn "$*";
    if [[ $isDlCoverYT -eq 0 ]];then return; fi
    if [[ $# -ne 2 ]]
    then
        erreursAddEcho "$FUNCNAME[$?/2]( youtube-code destinationPath )($*)";
        fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
    fi

    # on recupere l'adresse http de la miniature
    local _codeYT="$1";
    titre4 "Téléchargement de la miniature (cover) de la video YT: $_codeYT";
    local url=$(yt-dlp --get-thumbnail https://www.youtube.com/watch?v=$_codeYT);
    titreInfo "url: '$url'";

    local URLextension=${url##*.};    #displayVar 'URLextension' "$URLextension"
    
    local destinationExtention="$URLextension";
    local _ext="${destinationExtention:0:3}";#    _ext=${_ext,,}; {lowercase}
    #displayVar  'URLextension' "$URLextension" '_ext' "$_ext"
    if [[ "$_ext" == 'jpg' ]];then destinationExtention='jpg';fi
    local destinationName="$2-$_codeYT";
    local destinationPath="$PWD/$destinationName.$destinationExtention";

    coverLevelUrls['yt_file']='';
    coverLevelPaths['yt_file']='';


    if [[ -f "$destinationPath" ]]
    then
        notifsAddEcho "${FUNCNAME[0]}(): $destinationName déjà téléchargé.";
        coverLevelUrls['yt_file']="$url";
        coverLevelPaths['yt_file']="$destinationPath";
    else
        if [[ "${url:0:4}" == 'http' ]]
        then
            coverLevelPaths['yt_file']="$url";
            #echo wget --output-document="$destinationPath"  "$url";
            wget      --output-document="$destinationPath"  "$url";
            if [[ $? -eq 0 ]]
            then
                coverLevelPaths['yt_file']="$destinationPath";
            else
            erreursAddEcho "${FUNCNAME[0]}(): Erreur lors du téléchargement du cover yt_file: $url";
            fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
            fi
        else
            erreursAddEcho "${FUNCNAME[0]}():Le lien du thumbnail du cover est NI un fichier NI un http: $url";
            fctOut "${FUNCNAME[0]}($E_ARG_BAD)"; return $E_ARG_BAD;
        fi

        coverLevelPaths['yt_file_PNG']='';
        coverLevelPaths['yt_file_JPG']='';

        #si webp -> convertir en jpg (pour ogg)
        if [[ "$URLextension" == 'webp' ]]
        then
            local destinationJPGPath="$PWD/$destinationName.jpg";
            titre4 "Convertion du cover $destinationName.webp vers jpg";
            if [[ -f "$destinationJPGPath" ]]
            then
                coverLevelPaths['yt_file_JPG']="$destinationJPGPath";
            else
                ffmpeg $ffmpeg_params_globaux -loglevel quiet -i \"$destinationPath\" \"$destinationJPGPath\";
                if [[ $? -eq 0 ]]
                then
                    coverLevelPaths['yt_file_JPG']="$destinationJPGPath";
                fi
            fi
        fi
    fi
    #showCovers

    fctOut "${FUNCNAME[0]}(0)";return 0;
}


## -- COVER: GESTION DES TYPES -- ##
# setCoverLevelFront: ([coverFrontPath])
# defini le chemin du coverFront manuellement ou automatiquement selon un algorythme
# du + precis au + global
function setCoverFront(){
    fctIn "$*";

    # set manuel
    if [[ $# -eq 1 ]]
    then
        coverTypesPaths['Cover (front)']="$1"
        fctOut "${FUNCNAME[0]}(0)";return 0;
    fi

    # set automatique
    coverTypesPaths['Cover (front)']='';

    local _cover="${coverLevelPaths['yt_file_JPG']}"
    if [[ -f "$_cover" ]];then coverTypesPaths['Cover (front)']="$_cover"; fctOut "${FUNCNAME[0]}(0)";return 0;  fi

    local _cover="${coverLevelPaths['yt_file_PNG']}"
    if [[ -f "$_cover" ]];then coverTypesPaths['Cover (front)']="$_cover"; fctOut "${FUNCNAME[0]}(0)";return 0;  fi

    local _cover="${coverLevelPaths['yt_file']}"
    if [[ -f "$_cover" ]];then coverTypesPaths['Cover (front)']="$_cover"; fctOut "${FUNCNAME[0]}(0)";return 0;  fi

    local _cover="${coverLevelPaths['yt_page']}"
    if [[ -f "$_cover" ]];then coverTypesPaths['Cover (front)']="$_cover"; fctOut "${FUNCNAME[0]}(0)";return 0;  fi
    fctOut "${FUNCNAME[0]}(0)";return 0;
}


## -- AJOUT DU COVER DANS LE FICHIER -- ##
# saveCover( coverLevel [coverType] ){
# sauvegarde la cover dans le fichier en tant que metadata
function saveCover(){
    if $isTrapCAsk ;then return $E_CTRL_C;fi
    fctIn "$*";

    if [[ $# -eq 0 ]]
    then
        erreursAddEcho "$LINENO: ${FUNCNAME[0]}() n'a pas le bon nombre de parametre($#/1-2). $FUNCNAME (filename [coverLevel])";
        fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
    fi

    local _coverLevel="$1",
    local _coverType=${2:-"$COVER_TYPE_DEFAULT"}

    #getcoverLevel

    titre4 "Sauvegarde du cover comme metadatas: $_coverPath"
    if [[ "$_coverLevel" != '' ]]
    then
        local _coverPath="${coverTypesPaths[$_coverType]}";
        if [[ ! -f "$_coverPath" ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($*): Erreur lors de l'ajout du cover $_coverPath"
            addOggCover "$fileName" "$_coverPath";
        fi
    else
        erreursAddEcho "${FUNCNAME[0]}($*): '$_coverLevel non définit'"
    fi
    fctOut "${FUNCNAME[0]}(0)";return 0;
}

# calcul du tag FFMpeg pour ajouter le cover 
function setCoverFFmpegTags(){
    fctIn "$*";
    titre4 "${FUNCNAME[0]}($*)";
    coverFFmpegTags='';
    if [[ ! -f "${coverLevelPaths["Cover (front)"]}" ]]
    then
        erreursAddEcho "$FUNCNAME: Chemin du cover invalide (${coverLevelPaths["Cover (front)"]})";
        fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
    fi

    coverFrontPathTmpFile="$fileName.tags.$extension"; # $fileName.tags.$extension
    if [[ -f "$coverFrontPathTmpFile" ]];then rm "$coverFrontPathTmpFile";fi


    local _coverFrontTags='';
    #local _coverFrontPath="${coverLevelPaths["Cover (front)"]}\";
    #_coverFrontTags="  -i \"${coverLevelPaths["Cover (front)"]}\" -map 1:0 -metadata:s:1 title=\"${coverTypesDescs["Cover (front)"]}\" -metadata:s:1 comment=\"Cover (front)\""

    # https://stackoverflow.com/questions/54717175/how-do-i-add-a-custom-thumbnail-to-a-mp4-file-using-ffmpeg
    #Using ffmpeg 4.0, released Apr 20 2018 or newer,
    #ffmpeg -i video.mp4 -i image.png -map 1 -map 0 -c copy -disposition:0 attached_pic out.mp4;
    _coverFrontTags="ffmpeg $ffmpeg_params_globaux -i \"video.mp4\" -i \"image.png\" -map 1 -map 0 -c copy -disposition:0 attached_pic \"out.mp4\";";

    coverFFmpegTags="$_coverFrontTags";
    displayVarDebug 'coverFrontPathTmpFile' "$coverFrontPathTmpFile";
    displayVarDebug 'coverFFmpegTags' "$coverFFmpegTags";
    fctOut "${FUNCNAME[0]}(0)";return 0;
}

# Ajout du cover #
function addCoverLocal(){
    fctIn "$*";
    setCoverFFmpegTags

    local _coverFrontPath="${coverLevelPaths["Cover (front)"]}"
    titre4 "Sauvegarde du cover comme metadatas(_coverFrontPath): $_coverFrontPath"
    #evalEcho "ffmpeg $ffmpeg_params_globaux$ffmpeg_loglevelTags -i \"$fileName\"$_coverFrontPath -codec copy$tagVersion $ffmpeg_tags$_coverFrontTags \"$tmpFile\""
    evalEcho "ffmpeg $ffmpeg_params_globaux$ffmpeg_loglevelTags -i \"$fileName\"$_coverFrontPath -codec copy$tagVersion $coverFFmpegTags \"$coverFrontPathTmpFile\""
    if [[ $? -ne 0 ]]
    then
        erreursAddEcho "$FUNCNAME:$WARN erreur avec $coverFrontPathTmpFile ($_coverFrontPath)"
        #ls -s "$tmpFile"
    else #ajout du cover sans erreur
        echo "$FUNCNAME:Ajout du cover $coverFrontPathTmpFile ($_coverFrontPath)"
        #ls -l "$tmpFile"
        eval_echo_D "mv \"$coverFrontPathTmpFile\" \"$fileName\""
    fi
    fctOut "${FUNCNAME[0]}(0)";return 0;
}

#addOggCover ( fichier.ogg cover.png|cover.jpg)
function addOggCover(){
    if $isTrapCAsk ;then return $E_CTRL_C;fi
    fctIn "$*";

    local oggPath="$1"
    local coverLevel="$2"

    titre4 "Sauvegarde du cover comme metadatas(_coverFrontPath): $oggPath"
    if [[ ! -f "${oggPath}" ]]
    then
        erreursAddEcho "$FUNCAME($*): fichier $oggPath introuvable."
        fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
    fi

    if [[ ! -f "${coverLevel}" ]]
    then
        erreursAddEcho "$FUNCAME($*): fichier $coverLevel introuvable."
        fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
    fi

    local imageMimeType=$(file -b --mime-type "${coverLevel}")

    if [[ "${imageMimeType}" != "image/jpeg" ]] && [ "${imageMimeType}" != "image/png" ]]
    then
        erreursAddEcho "$FUNCAME($*): Le cover doit etre au format jpg ou png."
        fctOut "${FUNCNAME[0]}($E_ARG_BAD)"; return $E_ARG_BAD;
    fi

    local oggMimeType=$(file -b --mime-type "${oggPath}")

    if [[ "${oggMimeType}" != "audio/x-vorbis+ogg" ]] && [ "${oggMimeType}" != "audio/ogg" ]]
    then
        erreursAddEcho "$FUNCAME($*): fichier $oggPath n'est pas au format ogg."
        fctOut "${FUNCNAME[0]}($E_ARG_BAD)"; return $E_ARG_BAD;
    fi

    # Export existing comments to file
    local commentsPath="$(mktemp -t "tmp.XXXXXXXXXX")"
    vorbiscomment --list --raw "${oggPath}" > "${commentsPath}"

    # Remove existing images
    sed -i -e '/^metadata_block_picture/d' "${commentsPath}"

    # Insert cover image from file

    # metadata_block_picture format
    # See: https://xiph.org/flac/format.html# metadata_block_picture
    local imageWithHeader="$(mktemp -t "tmp.XXXXXXXXXX")"
    local description=''

    # Reset cache file
    echo -n '' > "${imageWithHeader}"

    # Picture type <32>
    printf "0: %.8x" 3 | xxd -r -g0 >> "${imageWithHeader}"

    # Mime type length <32>
    printf "0: %.8x" $(echo -n "${imageMimeType}" | wc -c) | xxd -r -g0 >> "${imageWithHeader}"

    # Mime type (n * 8)
    echo -n "${imageMimeType}" >> "${imageWithHeader}"

    # Description length <32>
    printf "0: %.8x" $(echo -n "${description}" | wc -c) | xxd -r -g0 >> "${imageWithHeader}"

    # Description (n * 8)
    echo -n "${description}" >> "${imageWithHeader}"

    # Picture with <32>
    printf "0: %.8x" 0 | xxd -r -g0  >> "${imageWithHeader}"

    # Picture height <32>
    printf "0: %.8x" 0 | xxd -r -g0 >> "${imageWithHeader}"

    # Picture color depth <32>
    printf "0: %.8x" 0 | xxd -r -g0 >> "${imageWithHeader}"

    # Picture color count <32>
    printf "0: %.8x" 0 | xxd -r -g0 >> "${imageWithHeader}"

    # Image file size <32>
    printf "0: %.8x" $(wc -c "${coverLevel}" | cut --delimiter=' ' --fields=1) | xxd -r -g0 >> "${imageWithHeader}"

    # Image file
    cat "${coverLevel}" >> "${imageWithHeader}"

    echo "metadata_block_picture=$(base64 --wrap=0 < "${imageWithHeader}")" >> "${commentsPath}"

    # Update vorbis file comments
    vorbiscomment --write --raw --commentfile "${commentsPath}" "${oggPath}"

    # Delete temp files
    rm "${imageWithHeader}"
    rm "${commentsPath}"
    fctOut "${FUNCNAME[0]}(0)";return 0;
}
