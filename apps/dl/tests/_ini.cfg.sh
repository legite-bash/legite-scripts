echo "${BASH_SOURCE[0]}";

# clear;gtt.sh --show-libsLevel2 apps/dl/tests/

local _source_fullRep="${BASH_SOURCE%/*.*}";
#local _source_lastRep="${_source_fullRep##*/}";
#local _source_fullname="${BASH_SOURCE##*/}";
#local _source_name="${_source_fullname%.*}";
#local _source_ext="${_source_fullname##*.}";

#displayVar '_source_fullRep' "$_source_fullRep";
#displayVar '_source_lastRep' "$_source_lastRep";
#displayVar '_source_fullname' "$_source_fullname";
#displayVar '_source_name' "$_source_name";
#displayVar '_source_ext' "$_source_ext";



setTMP_ROOT "$TMP_ROOT/test-ffmpg";
declare -g VIDEOTEST_NOM='charge_teaser06wide-720p';
declare -g VIDEOTEST_EXT='mp4';
declare -g VIDEOTEST_NAME="${VIDEOTEST_NOM}.${VIDEOTEST_EXT}";
declare -g VIDEOTEST_REP="/media/mediatheques";
declare -g VIDEOTEST_PATHNAME="$VIDEOTEST_REP/$VIDEOTEST_NAME";


########################################
# - CHARGEMENT DES FICHIERS DE TESTS - #
########################################

for _fichier_pathname in $_source_fullRep/*.sh
do
    #displayVar '_fichier_pathname' "$_fichier_pathname";
    #displayVar '${_fichier_pathname##*/}' "${_fichier_pathname##*/}";
    if [ "${_fichier_pathname##*/}" = '_ini.cfg.sh' ];then continue;fi  
    requireOnce "$_fichier_pathname";
done


#############
# -  MAIN - #
#############
#debugGTT_level-save $DEBUG_GTT_LEVEL_DEFAUT;
#debug_level-save $DEBUG_LEVEL_DEFAUT;

#debugGTT_level-restaure;
#debug_level-restaure;

return 0;
