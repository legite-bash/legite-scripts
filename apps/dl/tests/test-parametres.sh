#!/bin/bash
set -u;

declare -r VERSION='2024.04.23';

# Chemin du fichier : /www/bash/gtt/apps/dl/tests/test-parametres.sh

# #################### #
# EXEMPLE DE LANCEMENT #
# #################### #
function dl-test-1(){
    #clear;gtt.sh dl;
    #clear;gtt.sh dl --show-vars;
    #clear;gtt.sh dl --show-libsLevel1;
    clear;gtt.sh dl --show-libsLevel2;

}


########
# MAIN #
########

#dl-test-1



echo '== FIN DES TESTS ==';
#if $isTrapCAsk;then exit $E_CTRL_C;fi
exit 0;

