echo_D "${BASH_SOURCE[0]}" 1;

################
## DOWNLOADHD ##
################
    # telecharge l'audio et la vidéo séparement et les fusionnent  
    # version simplifié de MERGE (dlyt_merge)
    # Ne gere que les formats mp4 et m4a
    function downloadHD(){
        if $isTrapCAsk ; then return $E_CTRL_C ; fi
        fctIn "$*";
        local _codeYT="$1";
        local _fileName="${2:-""}";
        local _videoFormat=${3:-136};
        local _audioFormat=${4:-140};

        titre3 "${FUNCNAME[0]}($_codeYT '$_fileName $_videoFormat $_audioFormat)";
        if [[ "$_fileName" == "" ]]
        then
            _fileName="$(yt-dlp --get-filename https://www.youtube.com/watch?v=$_codeYT)";
        fi
        
        _fileName=${_fileName%.*};  # SUPPRESSION DE L'EXTENTION
        _fileName=$(echoNormalizeText "$_fileName");
        #displayVar '_fileName' "$_fileName";

        evalEcho "yt-dlp $YTDL_PARAM_GLOBAL --format $_audioFormat --output \"$_fileName.m4a\" https://www.youtube.com/watch?v=$_codeYT";
        if $isTrapCAsk ; then fctOut "${FUNCNAME[0]}($E_CTRL_C)"; return $E_CTRL_C ; fi
        evalEcho "yt-dlp --format $_videoFormat --output \"$_fileName.mp4\" https://www.youtube.com/watch?v=$_codeYT";
        evalEcho "FusionHD \"$_fileName\"";
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    # downloadHD 313 140
    function download4K(){
        if $isTrapCAsk ; then return $E_CTRL_C ; fi
        fctIn "$*";
        local _codeYT="$1";
        local _fileName="${2:-""}";
        downloadHD "$_codeYT" "$_fileName" 313 140;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    ##
    # fusionne $1.mp4 et $1.m4a en $1-HD.mp4
    # utilise: ffmpeg
    function FusionHD(){
        if $isTrapCAsk ; then return $E_CTRL_C ; fi
        fctIn "$*";

        local _videoName="$1";
        titre3 "$FUNCNAME '$_videoName'";
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($*): Nombre de parametre insuffisant $#/1: ${FUNCNAME[0]}( videoName )"
            fctOut "${FUNCNAME[0]}($E_ARG_BAD)";return $E_ARG_BAD;
	    fi
        if [[ -f "$_videoName-HD.mp4" ]]
        then
            notifsAddEcho "Le fichier '$_videoName' est déjà fusionné.";
        else
            evalEcho "ffmpeg $ffmpeg_params_globaux -i \"$_videoName\".mp4 -i \"$_videoName\".m4a -codec copy \"$_videoName-HD\".mp4";
            if [ $? -eq 0 ]
            then
                notifsAddEcho "Le fichier '$_videoName' est fusionné.";
            else
                erreursAddEcho "Problème lors de la fusion des fichiers '$_videoName'";
            fi
            file "$_videoName-HD.mp4";
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;			    
    }
#

