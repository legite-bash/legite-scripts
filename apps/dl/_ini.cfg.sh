# Controle d'accè: Si $isShowLibs -> on quitte après chargement des libs 
if $isShowLibs; then return;fi

echo_D "${BASH_SOURCE[0]}" 1;
declare -gr DL_VERSION_SELECT="${DL_VERSION_SELECT:-""}";
if [ "$DL_VERSION_SELECT" = '' ]
then
    declare -gr DL_APP_REP="$APPS_REP/dl";
else
    declare -gr DL_APP_REP="$APPS_REP/dl/$DL_VERSION_SELECT";
fi

requireOnce "${DL_APP_REP}/dl.vars.sh";
requireOnce "${DL_APP_REP}/dl.core.sh";
requireOnce "${DL_APP_REP}/dl.core-setName.sh";
requireOnce "${DL_APP_REP}/dl.core-tags.sh";
requireOnce "${DL_APP_REP}/dl.core-livre.sh";
requireOnce "${DL_APP_REP}/dl.core-musique.sh";
requireOnce "${DL_APP_REP}/dl.core-cover.sh";
requireOnce "${DL_APP_REP}/dl.core-playlist.sh";
requireOnce "${DL_APP_REP}/dl.core-ffmpeg.sh";
requireOnce "${DL_APP_REP}/dl.core-downloadHD.sh";

requireOnce "$APPS_REP/ffmpeg/_ini.cfg.sh"

#requireOnce "${DL_APP_REP}/dl.sh";
