echo_D "${BASH_SOURCE[0]}" 1;


################
## Set....... ##
# variables communs au programmes
# permettant de calculés les vlaurs auto
    ## SETTING DES tag, dl_ ##

    function clearTPCF(){
        fctIn;
        initTome;
        initPartie;
        initChapitre;
        initFichier;
        initMusique;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function clearTags(){
        fctIn;
        # if use "declare" -> local
        tag_language='fr';
        # - collection - #
        setArtist '';  # auteurs
        tag_genreNo=0;  tag_genre='';

        # - livre - #
        setAlbumTitre '';   # titre du cycle
        setTitle '';   # titre du livre/chanson/etc
        setTrack;
        setYear 0;

        tag_url='';

        dl_prenote=''; tag_note=''; dl_postnote='';

        setDl_date '';
        setDl_voix '';
        setDl_source '';
        setDl_duree '';
        dl_editeur='';

        initCovers;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
    #setArtist( "$tag_title" ) si '' = ''


    function setArtist(){
        fctIn "$*";
        tag_artist=${1:-''};
        auteurNormalize='';
        auteurTN='';
        if [[ "$tag_artist" != '' ]]
        then
            getNormalizeText "$tag_artist"; auteurNormalize="$normalizeText";
            auteurTN="-$auteurNormalize"
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #setTitle( "$tag_title" )
    function setTitle(){
        fctIn "$*";
        tag_title=${1:-''};
        titreNormalize='';
        titreTN='';
        if [[ "$tag_title" != '' ]]
        then
            getNormalizeText "$tag_title";  titreNormalize="$normalizeText";
            titreTN="-$titreNormalize";
        fi
        setNameAuto;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #setYear( tag_year )
    function setYear(){
        fctIn "$*";
        if [[ $# -eq 0 ]]
        then
            tag_year=0;
            dl_date='';
        else
            if [[ $1 -ne 0 ]]
            then
                tag_year=$1;
                dl_date="$tag_year";
            fi
        fi
        #echo $LINENO;displayVar 'tag_year' "$tag_year" 'dl_date' "$dl_date"
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    function setTrackNb(){
        fctIn "$*";
        tag_trackNb=$1
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #incTrackNb
    function incTrackNb(){
        fctIn;
        ((tag_trackNb++));
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #incTrack 
    function incTrack(){
        fctIn;
        ((tag_track++));
        if [[ $tag_track -gt $tag_trackNb ]]
        then
            erreursAddEcho "Track $tag_track/$tag_trackNb"
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #setTrack trackNo [trackNb]
    function setTrack(){
        fctIn "$*";
        if [[ $# -eq 0 ]]
        then
            tag_track=0;
            tag_trackNb=0;
            fctOut "${FUNCNAME[0]}($E_ARG_NONE)"; return $E_ARG_NONE;
        fi
        tag_track=$1;

        if [[ $# -eq 2 ]]; then setTrackNb $2; fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    #setDl_date( 'mm.jj' [year] )
    function setDl_date(){
        fctIn "$*";
        local mmjj=${1:-''}
        if [[ $# -eq 2 ]];then tag_year=$2; fi
        dl_date='';
        if [[ $tag_year -gt 0 ]];then   dl_date="$tag_year";fi
        if [ "$dl_date" != '' -a "$mmjj" != '' ]; then dl_date="$dl_date.$mmjj";fi
        #displayVar $LINENO':tag_year' "$tag_year" 'dl_date' "$dl_date"
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    function setDl_voix(){
        fctIn "$*";
        dl_voix=${1:-''}
        dl_voixNormalize='';
        dl_voixTN='';
        if [[ "$dl_voix" != '' ]]
        then
            getNormalizeText "$dl_voix";  dl_voixNormalize="$normalizeText";
            dl_voixTN="-$dl_voixNormalize";
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setDl_source(){
        fctIn "$*";
        dl_source=${1:-''}
        dl_sourceNormalize=''
        dl_sourceTN='';
        if [[ "$dl_source" != '' ]]
        then
            getNormalizeText "$dl_source";  dl_sourceNormalize="$normalizeText";
            dl_sourceTN="-$dl_sourceNormalize"
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setDl_duree(){
        fctIn "$*";
        dl_duree=${1:-''}
        dl_dureeT='';
        if [[ "$dl_duree" != '' ]];then     dl_dureeT="-$dl_duree";fi
        setNameAuto
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #setDureeYT( codeYoutube)
    # recupere la duree de la video YT et apelle setDl_duree()
    function setDureeYT(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "$*";
        if [[ $# -ne 1 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}() n'a pas le bon nombre de parametre($#/1). ${FUNCNAME[0]}(\"codeYoutube\")"
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi

        local _referenceVideoYoutube="$1";
        #displayVar '_referenceVideoYoutube' "$_referenceVideoYoutube";
        if [ "$_referenceVideoYoutube" == '' ]
        then
            erreursAddEcho "${FUNCNAME[0]}() n'a pas le bon nombre de parametre($#/1). ${FUNCNAME[0]}(\"codeYoutube\")"
            fctOut "${FUNCNAME[0]}($E_ARG_BAD)"; return $E_ARG_BAD;
        fi

        titre4 'Recherche de la durée de la vidéo YT:'$_referenceVideoYoutube
        dl_duree='';
        #spliter la duree
        local _duration=$(yt-dlp --get-duration "https://www.youtube.com/watch?v=$_referenceVideoYoutube");
        if [[ $? -ne 0 ]];then return $E_INODE_NOT_EXIST;fi
        local _dureeTbl=($(echo $_duration | tr ":" "\n"))
        local _dureeTblNb=${#_dureeTbl[@]}

        local -i h=0
        local -i m=0
        local -i s=0

        #displayVar '_duration' "$_duration"
        #displayVar '_dureeTblNb' "$_dureeTblNb"
        #echo '_dureeTbl[@]' "${_dureeTbl[@]}"
        if [[ $_dureeTblNb -eq 1 ]]
        then
            #s=${_dureeTbl[0]}
            #displayVar 's0' "$s"
            fctOut "${FUNCNAME[0]}($E_ARG_BAD)"; $E_ARG_BAD; # il y a que des secondes dl_duree=''
        fi

        if [[ $_dureeTblNb -eq 2 ]]
        then
            m=${_dureeTbl[0]};s=${_dureeTbl[1]}
            #displayVar 'm0' "$m" 's1' "$s"
        fi

        if [[ $_dureeTblNb -eq 3 ]]
        then
            h=${_dureeTbl[0]};
            #displayVar '_dureeTbl[1]' "${_dureeTbl[1]}"
            m=10#${_dureeTbl[1]};
            #s=${_dureeTbl[2]};
            #displayVar 'h0' "$h" 'm1' "$m" 's2' "$s"
        fi


        local _mlz="$m"  # 
        if [[ $m -lt 10 ]]; then _mlz="0$m"; fi
        setDl_duree "${h}h$_mlz"
         fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    # -- tableau de noms -- #
    function clearFichierTbl(){
        fctIn "$*";
        unset fichierNomTbl; # le tableau est vidé sans détruire la variable
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function addFichierTbl(){
        fctIn "$*";
        if [[ $# -eq 0 ]];then return $E_ARG_REQUIRE;fi
        (( fichierTblNb++ ))
        fichierNomTbl[$fichierTblNb]="$1"
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setNote(){
        fctIn "$*";
        if [[ $# -eq 0 ]];then return $E_ARG_REQUIRE;fi
        tag_note="$1"
        setTags
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


###############
## NAME AUTO ##
###############
    #clearNameAuto(){
    #    livreTitreNormalize_Auto=''
    #    livreTitreNormalize_Album_Fichier=''
    #    livreTitreNormalize_Album_Chapitre=''
    #}

    function displayNameAuto(){
        fctIn;
        displayVar 'livreTitreNormalize_Auto'       "$livreTitreNormalize_Auto"
        displayVar 'albumTitreTN'                   "$albumTitreTN";
        displayVar 'tomeNoNbT'                      "$tomeNoNbT"        'tomeTitreTN'       "$tomeTitreTN";
        displayVar 'partieNoNbT'                    "$partieNoNbT"      '$partieTitreTN'    "$partieTitreTN";
        displayVar 'titreTN'                        "$titreTN";
        displayVar 'chapitreNoNbT'                  "$chapitreNoNbT"    'chapitreTitreTN'   "$chapitreTitreTN";
        displayVar 'fichierNoNbT'                   "$fichierNoNbT"     'fichierTitreTN'    "$fichierTitreTN";
        displayVar 'auteurTN$dl_sourceTN$dl_voixTN$dl_dureeT' "$auteurTN$dl_sourceTN$dl_voixTN$dl_dureeT"
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    # remplit automatiquement les differentes variables
    function setNameAuto(){
        fctIn;

        #local dateTN='';
        #                             if [[ "$dl_date" != '' ]]; then dateTN="$dl_date";fi
        #lzTomeNo;tomeNoNbT='';       if [[ $tomeNb -gt 1 ]];    then tomeNoNbT="-T$tlz$tomeNo-$tomeNb";fi
        #lzPartieNo;partieNoNbT='';    if [[ $partieNb -gt 1 ]];  then partieNoNbT="-P$partieNo-$partieNb";fi
        #lzChapitreNo;chapitreNoNbT='';if [[ $chapitreNb -gt 1 ]];then chapitreNoNbT="-C$clz$chapitreNo-$chapitreNb";fi
        #lzChapitreNo;fichierNoNbT=''; if [[ $fichierNb -gt 1 ]]; then fichierNoNbT="-F$flz$fichierNo-$fichierNb";fi
        #dl_dureeT='';                 if [[ "$dl_duree" != '' ]];then dl_dureeT="-$dl_duree";fi

        # - generique - #
        lzGeneriqueNo;trackNoNbT='';  if [[ $tag_trackNb -gt 1 ]]; then trackNoNbT="-$tlz$tag_track-$tag_trackNb";fi 
        generique_Glz_TitreNormalize="$trackNoNbT$titreTN";
        generique_date_Glz_TitreNormalize="$dl_date$trackNoNbT$titreTN$dl_dureeT";
        generique_date_TitreNormalize="$dl_date$titreTN";


        # - livre:auto - #
        setLivreNameAuto;

        # - musique - #
        setMusiqueNameAuto;

        #displayNameAuto
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setLivreNameAuto(){
        fctIn;

        local v="$dl_date";
        v="$v$albumTitreTN";
        v="$v$tomeNoNbT$tomeTitreTN";
        v="$v$partieNoNbT$partieTitreTN";
        if [ "$titreTN" == "$albumTitreTN" -o "$titreTN" == "$tomeTitreTN" -o "$titreTN" == "$partieTitreTN" -o "$titreTN" == "$chapitreTitreTN" -o "$titreTN" == "$fichierTitreTN" ]
        then
            #echo 'Le titre est egale a une categorie on ne rajoute pas le titre'
            local void=0;
        else
            #echo 'on rajoute pas le titre'
            v="$v$titreTN";
        fi
        
        v="$v$chapitreNoNbT$chapitreTitreTN";
        v="$v$fichierNoNbT$fichierTitreTN";
        v="$v$auteurTN$dl_sourceTN$dl_voixTN$dl_dureeT"
        # https://linuxhint.com/trim_string_bash/ ${v##*(-)} # ne fonctionne pas ici !

        #displayVarDebug $LINENO'dl_dureeT' "$dl_dureeT"
        #suppression des tirets de debut
        if [[ "${v:0:1}" == '-' ]];then     v="${v:1}";fi #  ${v:0:1}= First Car
        livreTitreNormalize_Auto="$v"
        setTagNoteLivre;

        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    function setMusiqueNameAuto(){
        fctIn;

        # - generique - #
        lzGeneriqueNo;trackNoNbT='';  if [[ $tag_trackNb -gt 1 ]]; then trackNoNbT="-$tlz$tag_track-$tag_trackNb";fi 
        generique_Glz_TitreNormalize="$trackNoNbT$titreTN";
        generique_date_Glz_TitreNormalize="$dl_date$trackNoNbT$titreTN$dl_dureeT";
        generique_date_TitreNormalize="$dl_date$titreTN";

        # - musique - #
        local v='';
        #v="$dl_date";
        v="$v$albumTitreTN";
        v="$v$musiqueNoNbT";
        #        if [[ "$titreTN" == "$albumTitreTN" -o "$titreTN" == "$musiqueTitreTN" -o "$titreTN" == "$fichierTitreTN" ]]
        #        then
        #            #echo 'Le titre est egale à une categorie on ne rajoute pas le titre'
        #            local void=0;
        #        else
        #            v="$v$titreTN";
        #        fi

        v="$v$musiqueTitreTN";
        v="$v$fichierNoNbT$fichierTitreTN";
        v="$v$auteurTN$dl_sourceTN$dl_dureeT"
        # https://linuxhint.com/trim_string_bash/ ${v##*(-)} # ne fonctionne pas ici !

        #suppression des tirets de debut
        if [[ "${v:0:1}" == '-' ]];then     v="${v:1}";fi #  ${v:0:1}= First Car
        musiqueTitreNormalize_Auto="$v"
        #displayVarDebug 'musiqueTitreNormalize_Auto' "$musiqueTitreNormalize_Auto"
        #setTagNoteLivre
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#
