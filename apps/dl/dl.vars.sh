echo_D "${BASH_SOURCE[0]}" 1;

#############
# VARIABLES #
# DE LA LIB #
#############

    
# - PSP - #
    #declare -gr DL_COLLECTIONS_ROOT="${COLLECTIONS_REP}/dl";
    declare -g  dl_collectionPath=""    # chemin relatif de la collection 'dl/LivreAudios/L_heure_du_mystere.sh' # pose des problème de cohérence de rep

    declare -gi isListeFormats=0;
    declare -gi isNoDownload=0;         # ne pas telecharger
    declare -gi isSaveTags=0;           # force la sauvegarde des tags même hors convertion

# - COMMUNS - #

    declare -gr COVER_ROOT='https://legral.fr/docufiles/covers';
    
    #declare    collectionRoot='';
    #declare   collectionRep="$CONF_REP";
    #declare -ga collectionsTbl;        # (array) liste des collections appellées
    #declare -gi collectionsTblIndex=0; # 
    #declare    collection='';          # collection en cours   # obsolete
    #declare     collectionNom='';      # nom de la collection en cours

# - FORMAT - #
    declare -gi formatNo=0;             # a renommer en ytFormatNo
    declare -g  formatExt='';           # a renommer en ytFormaExt
    declare -g  odFormat='hls-525';     # format des fichier odysee

    #declare -gi isCollectionNeeded=1;  # par defaut il faut indiquer une collection
    declare -gi isCodeYoutube=1;        # =1 si le parametre ne contient que le code youtube (sinon c'est l'url complete)
    declare -gi isCodeOdysee=1;         # =1 si le parametre ne contient que le code odysee (sinon c'est l'url complete)
    declare -g  showTagsPath=''         # Affiche les tags du chemin (repertoire ou fichier) donné en parametre

# - LEVELS - #
    declare -gr TITLE_LEVEL_DEFAULT='titre';
    declare -ga titreLevelListe=( 'Tome' 'Album' 'Partie' 'Chapitre' 'Titre' 'Fichier' 'Musique' );
    declare -g  titreLevel='Titre';     # niveau a partir duquel le title sera setter
    declare -g  trackLevel='';          # niveau a partir duquel le track sera setter

# - DLYT - #
    declare -g  dlyt_filename='';       # nom de fichier calculé par le dernier de ytdl() (nom+ext)
    declare -g  dlyt_file_name='';      # nom de fichier calculé par le dernier de ytdl() (nom)
    declare -g  dlyt_file_ext='';       # nom de fichier calculé par le dernier de ytdl() (ext)
    declare -g  YTDL_PARAM_GLOBAL='';   # paramètres globales de yt-dlp


# - FFMPEG - #
    declare -g  ffmpeg_loglevel='error'  # quiet,panic,fatal,error,warning,info,verbose,debug
    declare -g  ffmpeg_loglevelTags=" -loglevel $ffmpeg_loglevel"  # quiet,panic,fatal,error,warning,info,verbose,debug
    declare -g  ffmpeg_params_globaux=' -hide_banner -n';

# - MONO - #
    declare -gi isMono=0;               #
    declare -g  FFMpegMono='';          #  ffmpeg: " -ac 1" via setMono()

# - CONVERT - #
    declare -g  convert='';             # format de convertion (ogg)
    declare -g  convertBak=''; # sauvegarde de la valeur de convert


# - stats - #
    declare -gA stats;
    declare -gi stats['dl_total']=0; # compteur de telechargement total
    declare -gi stats['dl_ok']=0;    # compteur de telechargement reussi
    declare -gi stats['dl_exist']=0; # compteur de telechargement pre-existant
    declare -gi stats['dl_fail']=0;  # compteur de telechargement echoué

    declare -gi stats['cv_total']=0; # compteur de convertion total
    declare -gi stats['cv_ok']=0;    # compteur de convertion réussis
    declare -gi stats['cv_exist']=0; # compteur de convertion pre-existant
    declare -gi stats['cv_fail']=0;  # compteur de convertion echoué

    declare -gi stats['yt_dl_total']=0; # compteur de telechargement yt total
    declare -gi stats['yt_dl_ok']=0;    # compteur de telechargement yt reussi
    declare -gi stats['yt_dl_exist']=0; # compteur de telechargement yt pre-existant
    declare -gi stats['yt_dl_fail']=0;  # compteur de telechargement yt echoué

    declare -gi stats['dlyt_cv_total']=0; # compteur de convertion yt total
    declare -gi stats['dlyt_cv_ok']=0;    # compteur de convertion yt réussis
    declare -gi stats['dlyt_cv_exist']=0; # compteur de convertion yt pre-existant
    declare -gi stats['dlyt_cv_fail']=0;  # compteur de convertion yt echoué


    declare -gi stats['wg_dl_total']=0; # compteur de telechargement wget total
    declare -gi stats['wg_dl_ok']=0;    # compteur de telechargement wget reussi
    declare -gi stats['wg_dl_exist']=0; # compteur de telechargement wget pre-existant
    declare -gi stats['wg_dl_fail']=0;  # compteur de telechargement wget echoué

    declare -gi stats['dlwg_cv_total']=0; # compteur de convertion wget total
    declare -gi stats['dlwg_cv_ok']=0;    # compteur de convertion wget réussis
    declare -gi stats['dlwg_cv_exist']=0; # compteur de convertion wget pre-existant
    declare -gi stats['dlwg_cv_fail']=0;  # compteur de convertion wget echoué

    declare -gi stats['od_dl_total']=0; # compteur de telechargement odysee total
    declare -gi stats['od_dl_ok']=0;    # compteur de telechargement odysee reussi
    declare -gi stats['od_dl_exist']=0; # compteur de telechargement odysee pre-existant
    declare -gi stats['od_dl_fail']=0;  # compteur de telechargement odysee echoué

    declare -gi stats['dlod_cv_total']=0; # compteur de convertion odysee total
    declare -gi stats['dlod_cv_ok']=0;    # compteur de convertion odysee réussis
    declare -gi stats['dlod_cv_exist']=0; # compteur de convertion odysee pre-existant
    declare -gi stats['dlod_cv_fail']=0;  # compteur de convertion odysee echoué
#


# - TAGS - #

    # - collection - #
    declare -g  tag_language='';
    declare -g  tag_artist='';  # auteurs
    declare -gi tag_genreNo=0;
    declare -g  tag_genre='';

    # - commun - #
    declare -g  tag_album='';    # titre du cycle/livre/album
    declare -g  tag_title='';    # titre du chapitre/chanson/etc
    declare -gi tag_track=0;
    declare -gi tag_trackNb=0;
    declare -g  trackNoNbT=''; 
    declare -g  tlz='';
    declare -g  tracksTN='';     # -$glz$trackNo$trackNb

    declare -gi tag_year=0;

    declare -g  tag_url='';

    declare -g  dl_prenote='';   # user
    declare -g  tag_note='';     # calculé par setTagNoteLivre() 
    declare -g  dl_postnote='';  # user

    #*TN= TiretNormalisé
    declare -g  dl_date='';      # MM/JJ. Ne contient pas l'année;
    declare -g  dl_voix='';      declare -g dl_voixNormalize='';    declare -g dl_voixTN='';
    declare -g  dl_source='';    declare -g dl_sourceNormalize='';  declare -g dl_sourceTN='';
    declare -g  dl_duree='';     declare -g dl_dureeT='';
    declare -g  dl_editeur='';
    declare -g  dl_traduction='';

    # - tags calculer (setTags) - #
    declare -g  vorbis_tags='';
    declare -g  id3v2_tags=''
    declare -g  ffmpeg_tags='-map 0:0';
    declare -g  tag_comment=''               # dl_prenote+tag_note+dl_postnote
    declare -g  vorbis_tag_comment=''
    declare -g  id3v2_tag_comment=''
    declare -g  tag_note_mp3=''
#

# - COVER - #

    # - la reference du cover dans le fichier - #
    declare -gr COVER_TYPE_DEFAULT='Cover (front)';
    declare -g  coverTypeSelect="$COVER_TYPE_DEFAULT";           # le type en cours
    declare -ga coverTypesListe=('Cover (front)' 'Cover (back)')
    declare -gA coverTypesPaths; #contient les paths du fichier a intégrer
    coverTypesPaths['Cover (front)']='';
    coverTypesPaths['Cover (back)']='';

    # - la description du cover dans le fichier (delon le type)- #
    declare -gA coverTypesDescs;                    # coverDescriptions["Cover (front)"]="Illustration de ..."
    coverTypesDescs['Cover (front)']='';
    coverTypesDescs['Cover (back)']='';

    # les levels de cover du + précis au + global
    declare -gr COVER_LEVEL_DEFAULT='Cover (front)'
    declare -g  coverLevelSelect="$COVER_LEVEL_DEFAULT"
    declare -ga coverLevelListe=('Cover (front)' 'yt_file' 'yt_file_WEBP' 'yt_file_JPG' 'yt_file_PNG' 'yt_page')
    declare -gA coverLevelUrls;              # coverLevelUrls["Cover (front)"]="url"
    coverLevelUrls['Cover (front)']='';
    coverLevelUrls['yt_file']='';
    coverLevelUrls['yt_file_WEBP']='';      # format yt natif pour les minatures des videos
    coverLevelUrls['yt_file_JPG']='';       # dlCoverYT convertit yt_file_WEBP_path en yt_file_JPG_path
    coverLevelUrls['yt_file_PNG']='';
    coverLevelUrls['yt_page']='';
    
    declare -gA coverLevelPaths;             # coverLevelPaths["Cover (front)"]="fileName"
    coverLevelPaths['Cover (front)']='';
    coverLevelPaths['yt_file']='';
    coverLevelPaths['yt_file_WEBP']='';
    coverLevelPaths['yt_file_JPG']='';
    coverLevelPaths['yt_file_PNG']='';
    coverLevelPaths['yt_page']='';

    declare -gA coverLevelDescs;             # coverLevelDescs["Cover (front)"]="texte"
    coverLevelDescs['Cover (front)']='';
    coverLevelDescs['yt_file']='';
    coverLevelDescs['yt_file_WEBP']='';
    coverLevelDescs['yt_file_JPG']='';
    coverLevelDescs['yt_file_PNG']='';
    coverLevelDescs['yt_page']='';

    declare -g  coverFrontPathTmpFile='';       # fichier temporaire pour ffmpeg pour cover Front
    declare -g  coverFFmpegTags='';             # chemin du cover Front (calculer part set)
    declare -gi isDlCoverYT=1;               # par defaut on telecharge le cover yt avec dlyt ()
#

# - DATAS - #

# - PLAYLIST des albums, musiques, codeYT, etc -- #
    declare -gi playlistNo=0;
    declare -gi playlistNu=0;
    declare -gi playlistNb=0;
    declare -ga playlist_codes=();
    declare -ga playlist_titres=();
    declare -ga playlist_dl_date=();
    declare -ga playlist_auteur=();
    #declare -ga albumTitreTbl=();
    #declare -ga episodeTitreTbl=();
    declare -ga playlist_item_deb=();
    declare -ga playlist_item_fin=();


    # -- Tableau de noms -- #
    # Contient les noms (normalisé ou non des livres/fichiers/etc lors d'utilisations successives
    # a remplacer par playlist?
    declare -ga livreNomTbl;
    declare -gi livreNb=0;
    declare -ga fichierNomTbl;
    
    #declare -g   titre='';                    # egal tag_title
    declare -g  titreNormalize=''            # initialisé par create*
    declare -g  titreTN=''                   # Tiret Normalize ajoute un tiret devant titreNormalize

    #declare   auteur='';                   # egal tag_artist
    declare -g  auteurNormalize=''
    declare -g  auteurTN=''

    #declare   albumTitre=''                # egal tag_album
    declare -g  albumTitreNormalize=''
    declare -g  albumTitreTN=''

    # - FICHIER - #
    declare -gi fichierNb=0;                 # si>0 affiche no-nb dans le titreAuto
    declare -gi fichierNo=0;                 # no/nb de fichier qui découpe un livre
    declare -g  fichierNoNbT=''; 
    declare -g  flz='';
    declare -g  fichierTitre='';
    declare -g  fichierTitreNormalize='';
    declare -g  fichierTitreTN='';

    # -- GENERIQUE -- #
    declare -gi generiqueNb=0;
    declare -gi generiqueNo=0;
    declare -g  generiqueNoNbT=''; 
    declare -g  glz='';
    #declare -g   generiqueTitre='';
    #declare    generiqueTitreNormalize='';
    #declare    generiqueTitreTN='';
    declare -g  generique_Glz_TitreNormalize='';
    declare -g  generique_date_Glz_TitreNormalize='';
    declare -g  generique_date_TitreNormalize='';       # yyyy?dl_data-titreNormalize

    # -- LIVRE -- #
    declare -gi tomeNb=0;                    # si>0 affiche Tno-nb dans le titreAuto
    declare -gi tomeNo=0;
    declare -gi tomeNoNbT='';
    declare -g  tlz='';
    declare -g  tomeTitre='';
    declare -g  tomeTitreNormalize='';
    declare -g  tomeTitreTN='';

    declare -gi partieNb=0;                  # si>0 affiche Pno-nb dans le titre Auto
    declare -gi partieNo=0;
    declare -g  partieNoNbT='';
    declare -g  plz='';
    declare -g  partieTitre='';
    declare -g  partieTitreNormalize='';
    declare -g  partieTitreTN='';

    # local -a chapitreTitreTbl;            # utilisé en local: contient les titres des chapitres
    declare -gi chapitreNo=0;                # createCollection#createLivre
    declare -gi chapitreNb=0; 
    declare -gi chapitreNoNbT='';
    declare -g  clz='';                      # Chapitre Leadinbg Zero (text) "0" ''
    declare -g  chapitreTitre='';
    declare -g  chapitreTitreNormalize='';   # addChapitre
    declare -g  chapitreTitreTN='';          # Prefixé et normalisé -$chapitreTitreNormalize"

    # -- MUSIQUE -- #
    declare -gi musiqueNb=0;
    declare -gi musiqueNo=0;
    declare -g  musiqueNoNbT='';
    declare -g  mlz='';
    declare -g  musiqueTitre='';
    declare -g  musiqueTitreNormalize='';
    declare -g  musique_Mlz_TitreNormalize='';
    declare -g  musiqueTitreTN='';


    # Chaines calculées par ->setNameAuto setNameLivreAuto setNameMusiqueAuto
    declare -g  livreTitreNormalize_Auto='';
    declare -g  livreTitreNormalizeTomeChapitre='';
    declare -g  musiqueTitreNormalize_Auto='';


    # youtube
    declare -g  YTDL_PARAM_GLOBAL='';