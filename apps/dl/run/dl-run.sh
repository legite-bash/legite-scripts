echo_D "${BASH_SOURCE[0]}" 1;


# #################### #
# FONCTIONS GENERIQUES #
# #################### #
    function dl-usage() {
        if [ $aideLevel -gt 0 ]
        then
            echo '';
            titreInfo "$SCRIPT_FILENAME  dl  [-R] [--dironly] [--fileonly] [--profondeur_max=n] [- [src='repertoire_source'] [dest='repertoire_destination']]\
            \n   --convertir [--convertirFrom=m4a] [--convertirTo=ogg] [repertoire='./']";
            echo -e "\nCommandes multiples:"
            echo 'repSource et repDestination ont, par défaut la valeur du chemin courant "./"';
            return 0;
        fi
    }

    function dl-usageLocal(){
        if [[ $argNb -eq 0 ]]
        then
            titreUsage "$SCRIPT_FILENAME:; [options] collection - parametre_de_collection"
            echo 'options:'
            echo '--no-download: Ne pas telecharger'
            echo '--saveTags : force l écriture des tags meme si le fichier est deja telechargé'
            echo '--saveTags --convert=ogg ; pour forcer l écriture des tags sur un fichier convertit existant'
            echo '--showTags=dir|fichier. Afficher les tags du chemin (repertoire ou fichier) donné en parametre'
            echo '--mono: convertit le fichier en mono'
            exit $E_ARG_NONE;
        fi
    }


    function showVarsLocal(){
        fctIn;
        #displayVar 'DL_COLLECTIONS_ROOT' "$DL_COLLECTIONS_ROOT";   # pose des problème de cohérence de rep
        displayVar 'dl_collectionPath  ' "$dl_collectionPath";
        displayVar 'convert            ' "$convert";
        displayVar 'isNoDownload       ' "$isNoDownload";
        displayVar 'isSaveTags         ' "$isSaveTags";
        displayVar 'isMono             ' "$isMono";
        displayVar 'showTagsPath       ' "$showTagsPath";
        displayVar 'isListeFormats     ' "$isListeFormats";
        displayVar 'formatExt          ' "$formatExt" 'formatNo' "$formatNo";
        #displayVar 'isCollectionNeeded' "$isCollectionNeeded";
        displayVar 'isCodeYoutube      ' "$isCodeYoutube";
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function showVarsPostLocal(){
        fctIn;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


########
# MAIN #
########
#saveDebugGTTLevel $DEBUG_GTT_LEVEL_DEFAUT;
#saveDebugLevel $DEBUG_LEVEL_DEFAUT;
TITRE1_D "         APP:DL           " 1;



# - Backup de l'app - #
if [ $backupLevel -ne 0 ]
then
    loadRepertoiresCfg '_backup' "$GTT_REP/";
    backup-sidToToday 'dl';  # backup l'app dl
fi

#loadRepertoiresCfg '_PSP' "$DL_APP_REP";

if [ $testLevel -ne 0 ]
then
    displayVar 'DISKORGA_TMP_REP' "$DISKORGA_TMP_REP";
    loadRepertoiresCfg '_tests' "$DISKORGA_REP";
fi


#restaureDebugGTTLevel;
#restaureDebugLevel;


################
## PARAMETRES ##
################
    function dl_PSP_arg(){
       fctIn "$@";

        local -i _argNu=-1;
        while true
        do
            if $isTrapCAsk;then break;fi
            ((_argNu++))
            if [ $_argNu -ge $ARG_NB ];then break;fi

            #displayVar '${ARG_TBL['$_argNu']}' "${ARG_TBL[$_argNu]}";
            case "${ARG_TBL[$_argNu]}" in
                -)  # indicateur des parametres pour les librairies/collections
                    break;  # Les PSPParams sont déjà traités
                    ;;

                #-V|--version) echo "$VERSION"; exit 0;      ;;

                # - Fonctions specifiques au programme - #
                --listeFormats)     isListeFormats=1;        ;;

                --no-download)      isNoDownload=1;          ;;
                --convert)          setConvert "$2";   shift ;;
                --saveTags)         isSaveTags=1;            ;;
                --showTags)         showTagsPath="$2"; shift ;;
                --mono)             setMono 1;               ;;
                #--collection)      $PSPParams['collection'] ;;
            esac

            shift 1; #renvoie une valeur non null
            if [[ -z "$parametre" ]];then break;fi
        done
        unset librairie param;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
    dl_PSP_arg;

    
    ##
    # analyse le tableau PSPCall[]

    function dl_PSP_call(){
       fctIn "$@";

        titre2 "Chargement des collections par dl";
        local -i _argNu=-1;
        while true
        do
            if $isTrapCAsk;then break;fi
            ((_argNu++))
            if [ $_argNu -ge $ARG_NB ];then break;fi
            local _collection="${PSPCall[$_argNu]:=""}";
            #titre2 "Chargement de la collection: '$_collection'";
            if [ "$_collection" = "" ];then continue;fi

            displayVar '${PSPCall['$_argNu']}' "$_collection";
            loadCollection "$_collection"

        done
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
    #dl_PSP_call;
#


##########
## MAIN ##
##########

if [ $aideLevel -gt 0 ]
then
    dl-usage;
    return 0;
fi

TITRE1 "         APP:DL:BEGIN           "
#((isShowLibs++)) # les libs sont systèmatiquesment vues
isShowLibs=2;


dl_PSP_call;

listeFormats;
rapportShow;
showTagsShow;


showFreedisk '/media/';
TITRE1 "         APP:DL:END           "

#if $isTrapCAsk; then exit $E_CTRL_C;fi
#exit 0;