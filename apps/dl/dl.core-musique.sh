echo_D "${BASH_SOURCE[0]}" 1;


#############
## MUSIQUE ##
#############
#createMusiqueAlbum "nom album" ["rep"]
# Crée le repertoire et initialisae les variables
function createMusiqueAlbum(){
    if $isTrapCAsk ;then return $E_CTRL_C;fi
    fctIn "$@";
    if [ $# -eq 0 ]
    then
        erreursAddEcho "$LINENO: ${FUNCNAME[0]}($@) n'a pas le bon nombre de parametre(1). $FUNCNAME (albumTitre [repertoire])"
        setCollectionFail 1;
        fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
    fi
    setLevels 'Musique';
    initMusique 1 1;

    setAlbumTitre "$1";
    setTagNoteMusique;

    titre4 "Création de l'album: $tag_album";
    local rep="${2:-"$albumTitreNormalize"}";
    getNormalizeText "$rep";
    rep="$normalizeText";

    mkdir -p "$rep" && cd "$rep"
    if [[ $? -eq 0 ]]
    then
        notifsAddEcho "Création de l'album: '$tag_album' ($PWD)";
    else
        erreursAddEcho "Le repertoire '$rep' de l'album '$tag_album' ne peut etre crée ou ouvert";
        setCollectionFail 1;
        fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
    fi
    fctOut "${FUNCNAME[0]}(0)";return 0;
}

function closeMusiqueAlbum(){
    fctIn "$@";
    # avertissement en cas de manque de chapitre        
    if [ $musiqueNb -gt 0 -a  $musiqueNo -lt $musiqueNb ]
    then
        erreursAddEcho "Il manque une ou plusieurs musiques $musiqueNo$WARN/$musiqueNb"
    fi
    setAlbumTitre;
    dl_prenote=''; tag_note=''; dl_postnote='';
    cd ..
    fctOut "${FUNCNAME[0]}(0)";return 0;
}

function setTagNoteMusique(){
    local _editeur='';    if [[ "$dl_editeur"    != '' ]]; then _editeur="Editeur $dl_editeur." ;fi
    local _sortieLe='';   if [[ "$dl_date"       != '' ]]; then _sortieLe="Sortie le $dl_date." ;fi
    local _duree='';      if [[ "$dl_duree"      != '' ]]; then _duree="Durée $dl_duree."       ;fi
    local _source='';     if [[ "$dl_source"     != '' ]]; then _source="Source $dl_source."    ;fi
    local _traduction=''; if [[ "$dl_traduction" != '' ]]; then _traduction="Traduction de $dl_traduction." ;fi
    local _url='';        if [[ "$tag_url"       != '' ]]; then _url="$tag_url."                ;fi  # ajouté a postnote selon le format id3

    tag_note="$_editeur$_sortieLe$_duree$_source$_traduction"; # $_url est ajouté par saveTags

}

function initMusique(){
    fctIn "$@";
    setMusiqueNo ${1:-0};
    setMusiqueNb ${2:-$musiqueNo};
    fctOut "${FUNCNAME[0]}(0)";return 0;
}

function showMusique(){
    fctIn "$@";
    displayVar 'musiqueNb                   ' "$musiqueNb";
    displayVar 'musiqueNo                   ' "$musiqueNo";
    displayVar 'musiqueNoNbT                ' "$musiqueNoNbT";
    displayVar 'mlz                         ' "$mlz";
    displayVar 'musiqueTitre                ' "$musiqueTitre";
    displayVar 'musiqueTitreNormalize_Auto  ' "$musiqueTitreNormalize_Auto";
    displayVar 'musiqueTitreNormalize       ' "$musiqueTitreNormalize";
    displayVar 'musique_Mlz_TitreNormalize  ' "$musique_Mlz_TitreNormalize";
    displayVar 'musiqueTitreTN              ' "$musiqueTitreTN";

    fctOut "${FUNCNAME[0]}(0)";return 0;
}



function incMusiqueNb(){
    fctIn "$@";
    ((musiqueNb++))
    setMusiqueNb $musiqueNb;
    fctOut "${FUNCNAME[0]}(0)";return 0;
}

function setMusiqueNb(){
    fctIn "$@";
    musiqueNb=${1:-0};
    if [[ "$trackLevel" == 'Musique' ]];then setTrackNb $musiqueNb;fi
    #lzMusiqueNb; #existe pas   
    musiqueNoNbT='';
    if [ $musiqueNb -gt 1 ]
    then
        musiqueNoNbT="-$mlz$musiqueNo-$musiqueNb";
    fi
    setMusiqueNameAuto;
    fctOut "${FUNCNAME[0]}(0)";return 0;
}

function incMusiqueNo(){
    fctIn "$@";
    ((musiqueNo++))
    setMusiqueNo $musiqueNo;
    fctOut "${FUNCNAME[0]}(0)";return 0;
}

function setMusiqueNo(){
    fctIn "$@";
    musiqueNo=${1:-0};
    if [[ "$trackLevel" == 'Musique' ]];then tag_track=$musiqueNo;fi
    lzMusiqueNo;
    musiqueNoNbT='';
    if [ $musiqueNb -gt 1 ]
    then
        musiqueNoNbT="-$mlz$musiqueNo-$musiqueNb";
    fi
    setMusiqueNameAuto
    fctOut "${FUNCNAME[0]}(0)";return 0;
}

#prefixe musiqueNo avec un zero si 0..9
function lzMusiqueNo(){
    #fctIn "$@";
    mlz='';
    if [[ $musiqueNb -lt 10 ]];then return 0;fi
    if [[ $musiqueNo -lt 10 ]];then mlz="0" ;fi
    #fctOut "${FUNCNAME[0]}(0)";
    return 0;
}

function setMusiqueTitre(){ 
    fctIn "$@";
    musiqueTitre="${1:-''}";
    musiqueTitreNormalize='';
    musiqueTitreTN='';

    if [[ "$musiqueTitre" != '' ]]
    then
        getNormalizeText "$musiqueTitre"; musiqueTitreNormalize="$normalizeText"; 
        musiqueTitreTN="-$musiqueTitreNormalize"
    fi
    if [[ "$titreLevel" == 'Musique' ]]; then setTitle "$musiqueTitre"; fi
    setMusiqueNameAuto;
    fctOut "${FUNCNAME[0]}(0)";return 0;
}

#Ajoute une musique sans changer la valeur de musiqueeNo
#addMusique( 'titre' ){
function addMusique(){
    fctIn "$@";
    if [[ $# -ne 1 ]]
    then
        erreursAddEcho "${FUNCNAME[0]}() n'a pas le bon nombre de parametre($#/1). ${FUNCNAME[0]}( \"musiqueTitre\" )"
        fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
    fi
    incMusique "$1" $musiqueNo;     # ne change pas la valeur actuelle (utilise)
    fctOut "${FUNCNAME[0]}(0)";return 0;
}


#defini le fichierNo ou augmente fichierNo de 1 si non defini en 2eme arg
#defini le titre de le musique setMusiqueTitre
#incMusique "musiqueTitre" [musiqueNo]
function incMusique(){
    fctIn "$@";
    setMusiqueTitre "${1:-''}";

    if [ $# -eq 2 ]
    then musiqueNo=$2;
    else (( musiqueNo++ ));
    fi
    setMusiqueNo $musiqueNo;

    titre2 "Ajout de la musique no $musiqueNo/$musiqueNb:$musiqueTitre";

    # avertissement en cas de depassement de fichier
    if [ $musiqueNo -gt $musiqueNb ]
    then
        erreursAddEcho "Musique actuel $musiqueNo/$musiqueNb$musiqueTitreTN";
    fi

    setMusiqueNameAuto;
    fctOut "${FUNCNAME[0]}(0)";return 0;
}
