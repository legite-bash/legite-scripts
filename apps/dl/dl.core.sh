echo_D "${BASH_SOURCE[0]}" 1;



##########################
# COLORISATION:TITRE,ETC #
##########################
    function titreChapitre() { # herite de titre2
        if [ $# -eq 0 ];then return 0;fi
        local -i _carNb=$(( ${#1} + 0 ));
        #local C='=';
        #local _sep=$(dupliqueNCar $_carNb "$C");
        echo '';
        #echo "    Chapitre: $_sep";
        echo -e $TITRE2"    Chapitre: '${1^}'"$NORMAL;
    }

    function titreYear() { # herite de titre2
        if [ $# -eq 0 ];then return 0;fi
        local -i _carNb=$(( ${#1} + 0 ));
        #local C='=';
        #local _sep=$(dupliqueNCar $_carNb "$C");
        echo '';
        #echo "    Chapitre: $_sep";
        echo -e $TITRE2"    Year: '${1^}'"$NORMAL;
    }
#


# ###### #
# DIVERS #
# ###### #
    function listeFormats(){
        fctIn;
        if [[ $isListeFormats -eq 1 ]]
        then
            local url=${VIDEO_INFO_URL:-''}
            if [[ "$url" == '' ]]
            then
                erreursAddEcho "${FUNCNAME[0]}(): variable VIDEO_INFO_URL non définie."
            else
                evalEcho "yt-dlp --list-formats $VIDEO_INFO_URL"
                exit
            fi
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    # isExtensionValide( 'element a tester' )
    function isExtensionValide(){
        fctIn "$*";
        if [[ $# -eq 0 ]];then erreursAddEcho "${FUNCNAME[0]}[$#/1]('extention') ($*)";fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;fi

        local -a ExtentsonTbl=('ogg' 'mp3' 'mkv' 'png' 'jpg' 'jpeg' 'ogm');
        for element in "${ExtentsonTbl[@]}";
        do
            #echo "$element - $1"
            #https://linuxhint.com/bash_lowercase_uppercase_strings/ (bash 4)
            #${1^^} = UPERCASE $1
            #${1,,} = lower $1
            if [[ "$element" == "${1,,}" ]]; then return $E_FALSE; fi
        done
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


###########
# RAPPORT #
###########
    function rapportShow(){
        fctIn;
        #if [[ $librairiesCallIndex -eq 0 ]];then fctOut "${FUNCNAME[0]}(0)";return 0;fi
        #displayVar 'Repertoire des collections' "$CONF_REP"
        titre1 "RAPPORT    ok         existant       fail   total"
        echo -e "Telecharements ${stats['dl_ok']}\t/ ${stats['dl_exist']}\t\t/ ${stats['dl_fail']}\t/ ${stats['dl_total']}"$NORMAL
        #echo "dl_yt"
        echo -e "youtube        ${stats['yt_dl_ok']}\t/ ${stats['yt_dl_exist']}\t\t/ ${stats['yt_dl_fail']}\t/ ${stats['yt_dl_total']}"
        #echo -e "Convertions    ${stats['dlyt_cv_ok']}\t/ ${stats['dlyt_cv_exist']}\t\t/ ${stats['dlyt_cv_fail']}\t/ ${stats['dlyt_cv_total']}"
        #echo "dl_wget"
        echo -e "wget           ${stats['wg_dl_ok']}\t/ ${stats['wg_dl_exist']}\t\t/ ${stats['wg_dl_fail']}\t/ ${stats['wg_dl_total']}"
        #echo -e "Convertions    ${stats['dlwg_cv_ok']}\t/ ${stats['dlwg_cv_exist']}\t\t/ ${stats['dlwg_cv_fail']}\t/ ${stats['dlwg_cv_total']}"
        echo -e "Convertions    ${stats['cv_ok']}\t/ ${stats['cv_exist']}\t\t/ ${stats['cv_fail']}\t/ ${stats['cv_total']}"$NORMAL
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    # Affiche les tags du repertoire|fichier donné ($1|showTagsPath)
    function showTagsShow(){
        fctIn "$*";

        local pathRoot=${1:-"$showTagsPath"}
        if [[ "$pathRoot" == ''  ]];then fctOut "${FUNCNAME[0]}(0)";return 0;fi # sortie normal. La fonction est optionnel
        if [[ ! -d  "$pathRoot"  ]];
        then
            erreursAddEcho "${FUNCNAME[0]}()  '$showTagsPath' n'est pas un repertoire'";
            fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
        fi

        titre2 "Rapport des tags($pathRoot)";

        function showTagsShowScan(){
            fctIn "$*";
            if $isTrapCAsk ; then return $E_CTRL_C;fi
            if [[ $# -eq 0 ]];then return $E_ARG_REQUIRE;fi
            echo ''
            local path="$1"

            if [[ ! -r "$path" ]]
            then
                erreurAddEcho "${FUNCNAME[0]}($*): Le chemin \"$path\" n'est pas lisible."
                fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
            fi

            if [[ -d "$path" ]]
            then
                echo_D "$LINENO: le path \"$path\" est un repertoire";
                for inode in $path/*
                do
                    echo ''
                    if [[ -d "$inode" ]]
                    then
                        echo_D "$LINENO: L inode $inode est un repertoire";
                        showTagsShowScan "$inode"
                    elif [[ -f "$inode" ]]
                    then
                        echo_D "$LINENO: $inode est un fichier";
                        showTagsShowFile "$inode"
                    fi
                done
            elif [[ -f "$path" ]]
            then
                echo_D "$LINENO: $path est un fichier";
                showTagsShowFile "$path"
            fi
        }
        showTagsShowScan "$pathRoot"

        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    # Affiche les tags du repertoire|fichier donné ($1 ou showTagsPath)
    function showTagsShowFile(){
        fctIn "$*";

        if [[ ! -r "$showTagsPath" ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($*): Le chemin $showTagsPath n'est pas lisible."
            fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
        fi
        local path=${1:-"$showTagsPath"}
        filename=${path##*/};  # nom du fichier uniquement (sans le chemin)

        local extension=${filename##*.}
        #displayVar "path"      "$path"
        #displayVar "filename"  "$filename"
        #displayVar "extension" "$extension"
        case "$extension" in 
            #format valide 
            #vorbiscomment: https://www.xiph.org/vorbis/doc/v-comment.html
            "ogg" | "ogm" | "flac" )
                evalEcho "vorbiscomment --list $path"
                ;;
            "mp3")
                evalEcho "id3v2 --list \"$path\""
                ;;
            #format non compatible
            * )
                erreursAddEcho "${FUNCNAME[0]}($*): Format '$extension' non compatible id3";
                ;;
        esac;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    #affiche les differentes variables calculés
    function showDatas(){
        fctIn;
        titre4 'TOUTES LES DATAS';

        showDatasGenerique;
        showDatasMusique;
        showDatasLivre;
        showCovers;
        showTags;
        showTagsProg;

        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function showDatasGenerique(){
        fctIn;

        echo '## GENERIQUE ##'
        displayVar 'generiqueNo' "$generiqueNo" '' "$generiqueNb" 'glz' "$glz";
        displayVar 'tag_track  ' "$tag_track"   '' "$tag_trackNb" 'glz' "$glz" 'tracksTN' "$tracksTN"

        displayVar 'fichierNo  ' "$fichierNo" '' "$fichierNb" 'flz' "$flz";
        if [[ "$fichierTitre" != '' ]]
        then
            displayVar 'fichierTitre         ' "$fichierTitre"
            displayVar 'fichierTitreNormalize' "$fichierTitreNormalize";
            displayVar 'fichierTitreTN      '  "$fichierTitreTN";
        fi

        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function showDatasLivre(){
        fctIn;

        echo '## LIVRE ##'
        displayVar 'tomeNo     ' "$tomeNo" '' "$tomeNb" 'tlz' "$tlz";
        if [[ "$tomeTitre" != '' ]]
        then
            displayVar 'tomeTitre           ' "$tomeTitre"
            displayVar 'tomeTitreNormalize  ' "$tomeTitreNormalize";
            displayVar 'tomeTitreTN        '  "$tomeTitreTN";
        fi
        displayVar 'partieNo   ' "$partieNo" '' "$partieNb" 'plz' "$plz";
        if [[ "$partieTitre" != '' ]]
        then
            displayVar 'partieTitre         ' "$partieTitre"
            displayVar 'partieTitreNormalize'  "$partieTitreNormalize" 
            displayVar 'partieTitreTN      '  "$partieTitreTN" 
        fi

        displayVar 'chapitreNo ' "$chapitreNo" '' "$chapitreNb" 'clz' "$clz";
        # local -a chapitreTitreTbl;                    # utilisé en local: contient les titres des chapitres
        if [[ "$chapitreTitre" != '' ]]
        then
            displayVar 'chapitreTitre         ' "$chapitreTitre"
            displayVar 'chapitreTitreNormalize' "$chapitreTitreNormalize";
            displayVar 'chapitreTitreTN      '  "$chapitreTitreTN";
        fi

        displayVarNotNull 'generique_Glz_TitreNormalize       ' "$generique_Glz_TitreNormalize"
        displayVarNotNull 'generique_date_Glz_TitreNormalize  ' "$generique_date_Glz_TitreNormalize"

        if [[ "$titreNormalize" != '' ]]
        then
            displayVar 'livreTitreNormalize_Auto           ' "$livreTitreNormalize_Auto"
        fi
        #displayVar 'livreTitreNormalize_Album_Fichier ' "$livreTitreNormalize_Album_Fichier"
        #displayVar 'livreTitreNormalize_Album_Chapitre' "$livreTitreNormalize_Album_Chapitre"

        titre3 'MUSIQUE'
        displayVar 'musiqueNo  ' "$musiqueNo" '' "$musiqueNb" 'mlz' "$mlz";
        if [[ "$musiqueTitre" != '' ]]
        then
            displayVar 'musiqueTitre               ' "$musiqueTitre";
            displayVar 'musiqueTitreNormalize      ' "$musiqueTitreNormalize";
            displayVar 'musiqueTitreTN             ' "$musiqueTitreTN";
            displayVar 'musique_Mlz_TitreNormalize'  "$musique_Mlz_TitreNormalize";
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function showDatasMusique(){
        fctIn;

        titre3 'MUSIQUE'
        displayVar 'musiqueNo  ' "$musiqueNo" '' "$musiqueNb" 'mlz' "$mlz";
        if [[ "$musiqueTitre" != '' ]]
        then
            displayVar 'musiqueTitre               ' "$musiqueTitre";
            displayVar 'musiqueTitreNormalize      ' "$musiqueTitreNormalize";
            displayVar 'musiqueTitreTN             ' "$musiqueTitreTN";
            displayVar 'musique_Mlz_TitreNormalize'  "$musique_Mlz_TitreNormalize";
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


# #################### #
# parametres de ffmpeg #
# #################### #
    function setFfmpeg_loglevelTags(){
        fctIn "$*";
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        local levelNu=${1:-$verbosity}

        # quiet,panic,fatal,error,warning,info,verbose,debug
        case $levelNu in
        0)
            ffmpeg_loglevel='quiet';
            ffmpeg_loglevelTags=" -loglevel $ffmpeg_loglevel"
            ;;
        1)
            ffmpeg_loglevel='error';
            ffmpeg_loglevelTags=" -loglevel $ffmpeg_loglevel"
            ;;
        esac
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


# ################# #
# parametres de ytd #
# ################# #
    #declare -g YTDL_PARAM_GLOBAL='';       # déclaré dans dl-vars.sh
    YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --ignore-config --restrict-filenames" #ignore les fichiers de conf
    YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --prefer-free-formats" 
    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --merge-output-format" 
    YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --prefer-ffmpeg"
    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} ---prefer-avconv"
 
    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --recode-video" 
    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --get-format" # 
    #Video Format Options:
    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --list-formats" #liste les formats videos disponibles


    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --list-thumbnails" #liste les miniatures du lien
    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --get-thumbnail"

    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --quiet"
    YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --console-title"
    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --all-formats" #telecharge tous les formats possibles 

    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --simulate"
    #YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --skip-download"

    declare PARAM_TITRE='';   # contient les parametres pour le titre en cours
    #PARAM_TITRE="${PARAM_TITRE} --format FORMAT SELECTION" 
    #PARAM_TITRE="${PARAM_TITRE} --format 43"  #webm medium
#


################
##   FORMAT   ##
################
    function setFormat(){
        fctIn "$*";
        if [[ $# -eq 0 ]]
        then
            erreurAddEcho "Pas de parametre! setFormat formatNo formatExt"
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi

        if [[ $# -ge 1 ]]
        then
            formatNoOld=$formatNo;
            formatNo=$1;
        fi

        # - surcharge de l'extension du fichier si defini- #
        if [[ $# -ge 2 ]]
        then
            formatExtOld=$formatExt;
            formatExt=$2;
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    function setYoutubeFormat(){
        fctIn "$*";
        if [[ $# -eq 0 ]]
        then
            titreWarn "Le nombre d'argument requis est: $#/"
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;

            titreWarn "${FUNCNAME[0]}:Nombre de parametre requis insuffisant ($#/1)!";
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi

        case $1 in
            43 | 171 | 242 | 243 | 244 | 247 | 248 | 249 | 250 | 251 | 278 )
                saveFormatToOld
                formatNo=$1;
                formatExt="webm"
                ;;


            18 | 22 | 133 | 134 | 135 | 136 | 137 | 160 )
                saveFormatToOld
                formatNo=$1;
                formatExt="mp4"
                ;;

            140 )
                saveFormatToOld
                formatNo=$1;
                formatExt="m4a"
                ;;

            *)
                erreursAddEcho "${FUNCNAME[0]}($*): Ce Numéro de format est inconnu!"
                ;;
        esac
        displayVarDebug "formatNo" ":$formatNo" "formatExt" "$formatExt"
        fctOut "${FUNCNAME[0]}";return 0;
    }

    function saveFormatToOld(){
        fctIn
        formatNoOld=$formatNo;
        formatExtOld=$formatExt;
        displayVarDebug 'formatNoOld' "$formatNoOld" 'formatExtOld' "$formatExtOld"
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function restaureFormat(){
        fctIn
        formatNo=$formatNoOld;
        formatExt=$formatExtOld;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setYTF_convert(){
        fctIn "$*";
        if [[ $# -lt 2 ]]
        then
            echo $WARN"${FUNCNAME[0]}:Nombre de parametre requis insuffisant ($#/2)!"$NORMAL
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi
        setYoutubeFormat "$1"
        setConvert "$2"
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function restaureAll(){
        fctIn "$*";
        restaureFormat
        restaureConvert
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


################
##   LEVEL    ##
################
    function setLevels(){
        fctIn "$*";        # laisser echo information importante a preciser
        local _level=${1:-''}
        setTitreLevel "$_level"
        setTrackLevel "$_level"
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #setTitreLevel( 'level')
    # en cas d'erreur titreLevel="$TITLE_LEVEL_DEFAULT"
    function setTitreLevel(){
        fctIn "$*";
        local _levelAsk=${1:-"$TITLE_LEVEL_DEFAULT"};
        if [[ "$_levelAsk" == '' ]];then _levelAsk="$TITLE_LEVEL_DEFAULT";fi
        local _level="$TITLE_LEVEL_DEFAULT"
        #displayVar '_levelAsk' "$_levelAsk"
        for element in "${titreLevelListe[@]}";
        do
            #displayVar  '_levelAsk' "$_levelAsk" 'element' "$element"
            if [[ "$_levelAsk" == "$element" ]]; then _level="$element";break; fi
        done
        titreLevel="$_level";
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setTrackLevel(){
        fctIn "$*";
        local _levelAsk=${1:-"$TITLE_LEVEL_DEFAULT"};
        if [[ "$_levelAsk" == '' ]];then _levelAsk="$TITLE_LEVEL_DEFAULT";fi
        local _level="$TITLE_LEVEL_DEFAULT"
        #displayVar '_levelAsk' "$_levelAsk"
        for element in "${titreLevelListe[@]}";
        do
            #displayVar  '_levelAsk' "$_levelAsk" 'element' "$element"
            if [[ "$_levelAsk" == "$element" ]]; then _level="$element";break; fi
        done
        trackLevel="$_level";
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


################
## Set....... ##
# variables communs aux programmes
# permettant de calculés les valeurs auto
    ## SETTING DES tag, dl_ ##

    function clearTPCF(){
        fctIn;
        initTome;
        initPartie;
        initChapitre;
        initFichier;
        initMusique;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    # -- tableau de noms -- #
    function clearFichierTbl(){
        fctIn "$*";
        unset fichierNomTbl; # le tableau est vidé sans détruire la variable
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function addFichierTbl(){
        fctIn "$*";
        if [[ $# -eq 0 ]];then return $E_ARG_REQUIRE;fi
        (( fichierTblNb++ ))
        fichierNomTbl[$fichierTblNb]="$1"
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setNote(){
        fctIn "$*";
        if [[ $# -eq 0 ]];then return $E_ARG_REQUIRE;fi
        tag_note="$1"
        setTags
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


###############
## NAME AUTO ##
###############
    #clearNameAuto(){
    #    livreTitreNormalize_Auto=''
    #    livreTitreNormalize_Album_Fichier=''
    #    livreTitreNormalize_Album_Chapitre=''
    #}

    function displayNameAuto(){
        fctIn;
        displayVar 'livreTitreNormalize_Auto'       "$livreTitreNormalize_Auto"
        displayVar 'albumTitreTN'                   "$albumTitreTN";
        displayVar 'tomeNoNbT'                      "$tomeNoNbT"        'tomeTitreTN'       "$tomeTitreTN";
        displayVar 'partieNoNbT'                    "$partieNoNbT"      '$partieTitreTN'    "$partieTitreTN";
        displayVar 'titreTN'                        "$titreTN";
        displayVar 'chapitreNoNbT'                  "$chapitreNoNbT"    'chapitreTitreTN'   "$chapitreTitreTN";
        displayVar 'fichierNoNbT'                   "$fichierNoNbT"     'fichierTitreTN'    "$fichierTitreTN";
        displayVar 'auteurTN$dl_sourceTN$dl_voixTN$dl_dureeT' "$auteurTN$dl_sourceTN$dl_voixTN$dl_dureeT"
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    # remplit automatiquement les differentes variables
    function setNameAuto(){
        fctIn;

        #local dateTN='';
        #                             if [[ "$dl_date" != '' ]]; then dateTN="$dl_date";fi
        #lzTomeNo;tomeNoNbT='';       if [[ $tomeNb -gt 1 ]];    then tomeNoNbT="-T$tlz$tomeNo-$tomeNb";fi
        #lzPartieNo;partieNoNbT='';    if [[ $partieNb -gt 1 ]];  then partieNoNbT="-P$partieNo-$partieNb";fi
        #lzChapitreNo;chapitreNoNbT='';if [[ $chapitreNb -gt 1 ]];then chapitreNoNbT="-C$clz$chapitreNo-$chapitreNb";fi
        #lzChapitreNo;fichierNoNbT=''; if [[ $fichierNb -gt 1 ]]; then fichierNoNbT="-F$flz$fichierNo-$fichierNb";fi
        #dl_dureeT='';                 if [[ "$dl_duree" != '' ]];then dl_dureeT="-$dl_duree";fi

        # - generique - #
        lzGeneriqueNo;trackNoNbT='';  if [[ $tag_trackNb -gt 1 ]]; then trackNoNbT="-$tlz$tag_track-$tag_trackNb";fi 
        generique_Glz_TitreNormalize="$trackNoNbT$titreTN";
        generique_date_Glz_TitreNormalize="$dl_date$trackNoNbT$titreTN$dl_dureeT";
        generique_date_TitreNormalize="$dl_date$titreTN";

        # - livre:auto - #
        setLivreNameAuto;

        # - musique - #
        setMusiqueNameAuto;

        #displayNameAuto
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function setLivreNameAuto(){
        fctIn;

        local v="$dl_date";
        v="$v$albumTitreTN";
        v="$v$tomeNoNbT$tomeTitreTN";
        v="$v$partieNoNbT$partieTitreTN";
        if [ "$titreTN" == "$albumTitreTN" -o "$titreTN" == "$tomeTitreTN" -o "$titreTN" == "$partieTitreTN" -o "$titreTN" == "$chapitreTitreTN" -o "$titreTN" == "$fichierTitreTN" ]
        then
            #echo 'Le titre est egale a une categorie on ne rajoute pas le titre'
            local void=0;
        else
            #echo 'on rajoute pas le titre'
            v="$v$titreTN";
        fi
        
        v="$v$chapitreNoNbT$chapitreTitreTN";
        v="$v$fichierNoNbT$fichierTitreTN";
        v="$v$auteurTN$dl_sourceTN$dl_voixTN$dl_dureeT"
        # https://linuxhint.com/trim_string_bash/ ${v##*(-)} # ne fonctionne pas ici !

        #displayVarDebug $LINENO'dl_dureeT' "$dl_dureeT"
        #suppression des tirets de debut
        if [[ "${v:0:1}" == '-' ]];then     v="${v:1}";fi #  ${v:0:1}= First Car
        livreTitreNormalize_Auto="$v"
        setTagNoteLivre;

        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    function setMusiqueNameAuto(){
        fctIn;

        # - generique - #
        lzGeneriqueNo;trackNoNbT='';  if [[ $tag_trackNb -gt 1 ]]; then trackNoNbT="-$tlz$tag_track-$tag_trackNb";fi 
        generique_Glz_TitreNormalize="$trackNoNbT$titreTN";
        generique_date_Glz_TitreNormalize="$dl_date$trackNoNbT$titreTN$dl_dureeT";
        generique_date_TitreNormalize="$dl_date$titreTN";

        # - musique - #
        local v='';
        #v="$dl_date";
        v="$v$albumTitreTN";
        v="$v$musiqueNoNbT";
        #        if [[ "$titreTN" == "$albumTitreTN" -o "$titreTN" == "$musiqueTitreTN" -o "$titreTN" == "$fichierTitreTN" ]]
        #        then
        #            #echo 'Le titre est egale a une categorie on ne rajoute pas le titre'
        #            local void=0;
        #        else
        #            v="$v$titreTN";
        #        fi

        v="$v$musiqueTitreTN";
        v="$v$fichierNoNbT$fichierTitreTN";
        v="$v$auteurTN$dl_sourceTN$dl_dureeT"
        # https://linuxhint.com/trim_string_bash/ ${v##*(-)} # ne fonctionne pas ici !

        #suppression des tirets de debut
        if [[ "${v:0:1}" == '-' ]];then     v="${v:1}";fi #  ${v:0:1}= First Car
        musiqueTitreNormalize_Auto="$v"
        #displayVarDebug 'musiqueTitreNormalize_Auto' "$musiqueTitreNormalize_Auto"
        #setTagNoteLivre
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


################
## CREATE.... ##
################
    #hierarchie: collection -> auteur -> year -> album -> livre -> partie -> chapitre -> fichier
    # collection: repertoire contenant des album (chemin absolu)
    # auteur
    # year
    # tome
    # album: repertoire contenant des livres,etc
    # livre: repertoire contenant des fichiers (tome, parties,chapitres,images,etc)
    # fichier: fichier faisant partie d'une suite 


    # - creer une nouvelle collection - #
    # -- creer un repertoire (avec son chemin) du meme nom que la collection et va dans ce repertoire -- #
    # $1: nom du repertoire
    # s2: nom de la collection
    function createCollection(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "$*";

        if [ $# -eq 0 ]
        then
            erreursAddEcho 'Nom du repertoire de la collection manquant.'
            setCollectionFail 1;
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi


        #collectionRoot="$1";
        getNormalizeText "$1";
        collectionPath="$normalizeText"; # global
        collectionNom="${2:-""}";        # global
        setLogPath "$collectionNom";
        clearTags;
        clearTPCF;

        titre1 "Nouvelle collection: '$collectionNom'";
   

        if [ ! -d "$collectionPath" ]
        then
            mkdir -p "$collectionPath";
            if [[ $? -eq 0 ]]
            then
                notifsAddEcho "Création du repertoire de la collection: $collectionNom $collectionPath";
            else
                erreursAddEcho "Le repertoire de collection $collectionNom '$collectionPath' ne peut etre créé";
                setCollectionFail 1;
                fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
            fi
        fi

        cd "$collectionPath";
        if [[ $? -eq 0 ]]
        then
            notifsAddEcho "Entrer dans la collection: $collectionNom $collectionPath";
        else
            erreursAddEcho "Le repertoire de collection $collectionNom '$collectionPath' ne peut etre crée ou ouvert";
            setCollectionFail 1;
            fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
        fi

        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function closeCollection(){
        collectionNom='';
        cd ..;
    }


    # - creer un nouvel auteur - #
    # -- creer un repertoire (dans le rep courant) et va dans ce repertoire -- #
    # $1: nom de de l'auteur (normalisé)
    # $2: repertoire de l'auteur (normalisé, facultatif, si absent = nom normalisé)
    # modifie la var global (ou local) auteurNormalize
    function createAuteur(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "$*";
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "${FUNCNAME[0]} ('auteur' ['repertoire'] )."
            setCollectionFail 1;
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi

        setArtist "$1"
        
        local rep="$auteurNormalize"
        if [[ $# -ge 2 ]]
        then
            getNormalizeText "$2"
            rep="$normalizeText"
        fi

        mkdir -p "$rep" && cd "$rep"
        local cdError=$?
        if [[ $cdError -eq 0 ]]
        then
            notifsAddEcho "Création de l'auteur: $tag_artist ($PWD)"
        else
            erreursAddEcho "Le repertoire '$rep' de l'auteur '$tag_artist' ne peut etre crée ou ouvert"
            setCollectionFail 1;
            fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
        fi

        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function closeAuteur(){
        fctIn "$*";
        setArtist '';
        cd ..;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    function createYear(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "$*";
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "${FUNCNAME[0]} (YYYY)";
            setCollectionFail 1;
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi
        local annee=$1;
        titreYear "$annee";
        setYear $annee;
        mkdir -p "$annee" && cd "$annee";
        local cdError=$?;
        if [[ $cdError -eq 0 ]]
        then 
            notifsAddEcho "Création de l'année: '$annee' ($PWD)";
        else
            erreursAddEcho "Le repertoire '$annee' ne peut etre crée ou ouvert"
            setCollectionFail 1;
            fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function closeYear(){
        fctIn "$*";
        setYear;
        #setDl_date ;
        cd ..
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


############
## ALBUMS ##
############
    function setAlbumTitre(){
        fctIn "$*";
        tag_album="${1:-""}";
        albumTitreNormalize='';
        albumTitreTN='';
        if [ "$tag_album" != '' ]
        then
            getNormalizeText "$tag_album";
            albumTitreNormalize="$normalizeText";
            if [ "$albumTitreNormalize" != "" ]
            then
                albumTitreTN="-$albumTitreNormalize";
            fi
        fi
        if [ "$titreLevel" = 'Album' ]; then setTitle "albumTitre"; fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    # - creer un nouvel album - #
    # -- creer un repertoire (dans le rep courant) et va dans ce repertoire -- #
    # $1: nom de de l'album (normalisé)
    # $2: repertoire de l'album (normalisé, facultatif, si absent = nom normalisé)
    # modifie la var global (ou local) albumTitreNormalize
    function createAlbum(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "$*";
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "${FUNCNAME[0]} ('album' ['repertoire'] )."
            setCollectionFail 1;
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi
        titre1 "Création d'un album: '$1'"


        local nom="$1"
        setAlbumTitre "$nom"
        local rep="$albumTitreNormalize"

        if [[ $# -ge 2 ]]
        then
            getNormalizeText "$2"; rep="$normalizeText"
        fi

        mkdir -p "$rep" && cd "$rep"
        local cdError=$?
        if [[ $cdError -eq 0 ]]
        then
            notifsAddEcho "Création de l'album: '$nom' ($PWD)";
        else
            erreursAddEcho "Le repertoire '$rep' de l'album '$nom' ne peut etre crée ou ouvert"
            setCollectionFail 1;
            fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return E_INODE_NOT_EXIST;
        fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function closeAlbum(){
        fctIn "$*";
        setAlbumTitre '';
        cd ..;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


#################
## REPERTOIRES ##
#################
    #createRepertoire() -> inexistant le generique ne crait pas de repertoire
    #Creation d'un repertoire neutre (ex: chronologie)
    function createRepertoire(){
        fctIn "$*";
        if [ $# -eq 0 ]
        then
            erreursAddEcho "${FUNCNAME[0]} ('repertoire' ).";
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi

        local _rep="$1";
        #getNormalizeText "$1"; _rep="$normalizeText";
        _rep="$(echoNormalizeText "$_rep")";
        titre1 "Création d'un repertoire: '$_rep'";

        mkdir -p "$_rep";
        if cd "$_rep"
        then
            notifsAddEcho "Création du repertoire générique'$_rep' ($PWD)";
        else
            erreursAddEcho "Le repertoire générique '$_rep' ne peut etre crée ou ouvert";
            fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
        fi

       fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function closeRepertoire(){
        fctIn;
        cd ..
        fctOut "${FUNCNAME[0]}";return 0;
    }

    function setTagNoteGenerique(){
        local _editeur='';    if [[ "$dl_editeur"    != '' ]]; then _editeur="Editeur $dl_editeur." ;fi
        local _sortieLe='';   if [[ "$dl_date"       != '' ]]; then _sortieLe="Sortie le $dl_date." ;fi
        local _duree='';      if [[ "$dl_duree"      != '' ]]; then _duree="Durée $dl_duree."       ;fi
        local _source='';     if [[ "$dl_source"     != '' ]]; then _source="Source $dl_source."    ;fi
        local _traduction=''; if [[ "$dl_traduction" != '' ]]; then _traduction="Traduction de $dl_traduction." ;fi
        local _url='';        if [[ "$tag_url"       != '' ]]; then _url="$tag_url."                ;fi  # ajouté a postnote selon le format id3
        tag_note="$_editeur$_sortieLe$_duree$_source$_traduction" # $_url est ajouté par saveTags
        return 0;
    }
#


###############
## GENERIQUE ##
###############
    #initGenerique generiqueNo generiqueNb artiste nom_de_l_album
    function initGenerique(){ 
        fctIn;

        generiqueNo=${1:-1};
        tag_track=$generiqueNo;

        generiqueNb=${2:-$generiqueNo};
        tag_trackNb=$generiqueNb;

        if [[ $# -ge 3 ]]; then setArtist "$3";fi
        if [[ $# -eq 4 ]]; then setAlbum "$4";fi
    
        #showTags
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #prefixe generiqueNo avec un zero si 0..9
    function lzGeneriqueNo(){
        fctIn "$*";
        glz=''
        if [[ $generiqueNb -lt 10 ]];then fctOut "${FUNCNAME[0]}(0)";return 0;fi
        if [[ $generiqueNo -lt 10 ]];then glz="0" ;fi
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #Ajoute un generique sans changer la valeur de generiqueNo
    #addGenerique( 'dl_date' 'titre' ){
    function addGenerique(){
        fctIn "$*";
        if [[ $# -ne 2 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}() n'a pas le bon nombre de parametre($#/2). ${FUNCNAME[0]}(\"generiqueTitre\" [generiqueNo] )"
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi

        incGenerique "$1" "$2" $generiqueNo;     # ne change pas la valeur actuelle (utilise)
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #defini le generiqueNo ou augmente generiqueNo de 1 si non defini en 2eme arg
    #defini le titre
    #incGenerique (dl_date) ("titre"|'') (generiqueNo|"+1") # titre==''->=album
    function incGenerique(){
        fctIn "$*";
        if [[ $# -lt 1 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}() n'a pas le bon nombre de parametre(1). ${FUNCNAME[0]} ( \"generiqueTitre\" GeneriqueNo|\"+1\" )"
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi

        setDl_date "$1"
        setTitle "$2"
        local numeroAsk=${3:-'+1'};
        #displayVar 'dl_date' "$dl_date" 'tag_titile' "$tag_title" 'numeroAsk' "$numeroAsk"
        if [[ "$numeroAsk" == '+1' ]] || [[ "$numeroAsk" == '' ]] # demande de forcage de l'incrementation?
        then (( generiqueNo++ ))
        #elif (( "$numeroAsk" > 0 )) #s'il est  numérique
        #then
        else
            generiqueNo=$numeroAsk
        fi
        tag_track=$generiqueNo;

        titre3 "Téléchargement d'un générique: $generiqueNo/$generiqueNb"

        # avertissement en cas de depassement de numero de generiqueNo
        if [[ $generiqueNo -gt $generiqueNb ]]
        then
            titreWarn "Générique actuel $generiqueNo/$generiqueNb"
            return
        fi

        lzGeneriqueNo;
        setNameAuto
        setTagNoteGenerique
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function closeGenerique(){
        fctIn;
        # avertissement en cas de manque d'entrée
        if [ $generiqueNb -gt 0 -a $generiqueNo -lt $generiqueNb ]
        then
            erreursAddEcho "$tag_title: Il manque une ou plusieurs entrées $generiqueNo/$generiqueNb";
        fi
        initGenerique;
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


##########
## LIER ##
##########
    # lier cible [newTitre]
    # original: chemin du fichier ou sera pointer le lien
    function lier(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "$*";
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($*): Cible du lien manquant."
        fi

        original="$1";
        newTitre="$original" #par defaut l'alias a le meme nom
        if [[ $# -ge 2 ]]
        then
            newTitre="$2"
        fi

        # suppression d'un eventuel alias prexistant (peut etre obsolete)
        unlink "$newTitre";
        evalEcho "ln -s \"$original\" \"$newTitre\"";
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


#################
## TELECHARGER ##
#################
    # telecharge l'url dans le repertoire courant (changer par createCollection/createAlbum/createTitre)
    # usage:
    # dlwget "$url" "$destination"
    function dlwget(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "$*";

        titre3 'Téléchargement via ${FUNCNAME[0]}()';

        if [[ $# -eq 0 ]]
        then
            erreursAddEcho ". ${FUNCNAME[0]}[$#/2](\"$url\" \"$destination\")";
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi

        url="$1";
        if [[ "$url" == '' ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($*): Pas d'url fournis.";
            fctOut "${FUNCNAME[0]}($E_ARG_BAD)"; return $E_ARG_BAD;
        fi
        ((stats['dl_total']++))
        ((stats['wg_dl_total']++))

        if [[ $# -lt 2 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($*): $url sans destination";
            ((stats['dl_fail']++))
            ((stats['wg_dl_fail']++))
            fctOut "${FUNCNAME[0]}($E_ARG_BAD)"; return $E_ARG_BAD;
        fi

        local infoProgression="$INFO"[$chapitreNo/$chapitreNb]"$NORMAL";
        echo -n "$infoProgression";

        getNormalizeText "$2";
        destination="$normalizeText";

        # ajout auto de l'extension sur destination avec celle de l'url (si pas defini dans destination)
        # cette fonctionalité esr désactivée à cause des extnetion de type php
        #local URLextension=${url##*.}
        #local DESTextension=${destination##*.} # si pas d'extension alors égale à destination
        #if [[ "$URLextension" != "$DESTextension" ]]
        #then
        #    destination="$destination.$URLextension";
        #fi

        #displayVarDebug "URLextension" "$URLextension" "DESTextension" "$DESTextension"
        displayVarDebug 'destination' "$destination"
        displayVarDebug 'url' "$url";

        # Suppression d'un événtel fichier de taille zero (venant de wget ou autre)
        if [[ ! -s "$destination"  ]]     # -s: -e + non vide
        then
            erreursAddEcho "Un fichier '$destination' de taille zero pré-existe : Suppression.";
            evalEcho "rm \"$destination\";";
        fi

        # le fichier existe t'il (-f) ?
        if [[ -f "$destination" ]]
        then
            notifsAddEcho "${FUNCNAME[0]}(): $destination pré-existe.";

            # - Force la sauvegarde des tags si demander - #
            if [[ $isSaveTags -eq 1 ]]
            then
                saveTags "$destination";
            fi

            ((stats['dl_exist']++))
            ((stats['wg_dl_exist']++))

        elif [[ $isNoDownload -eq 0 ]]
        then

            local _verbose='--no-verbose';
            local _verbose='';
            if [[ $verbosity -gt 0 ]]; then _verbose='';  fi

            titreCmd "wget $_verbose --continue --show-progress --progress=bar --output-document=\"$destination\" \"$url\"";
            wget $_verbose --continue --show-progress --progress=bar --output-document="$destination" "$url";
            local -i  _wget_erreur=$?;
            displayVar '_wget_erreur' "$_wget_erreur";
            # wget crait un fichier de taille zero et le laisse si le telechargement a échoué
            if [[ ! -s "$destination"  ]] # -s: -e + non vide
            then
                erreursAddEcho "Le fichier téléchargé '$destination' est vide: Suppression.";
                evalEcho "rm \"$destination\";";
                _wget_erreur=$E_INODE_NOT_EXIST;
            fi

            if [[ $_wget_erreur -eq 0 ]]  # wget=?
            then
                notifsAddEcho "${FUNCNAME[0]}(): '$destination'";
                ((stats['dl_ok']++))
                ((stats['wg_dl_ok']++))
                saveTags "$destination";
            else 
                erreursAddEcho "${FUNCNAME[0]}(): '$destination'";
                ((stats['dl_fail']++))
                ((stats['wg_dl_fail']++))
            fi

        fi

        convertir "$destination";
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #dlwgetDeuxChapitre $chapitreNu $episodeComplet
    function dlwgetUnChapitres(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "$*";
        if [[ $# -lt 2 ]];  then return $E_ARG_REQUIRE; fi
        local -i chapitreNu=$1;
        local url="$2";
        local __postnote=${3:-''};
        local partieTN='';
        dl_postnote=" $__postnote";
        addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
        dlwget "$url" "${livreTitreNormalize_Auto}.mp3";
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    #dlwgetDeuxChapitre $chapitreNu $episodeComplet
    function dlwgetDeuxChapitres(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "$*";
        if [[ $# -lt 2 ]];  then return $E_ARG_REQUIRE;fctOut "${FUNCNAME[0]}"; fi
        local -i chapitreNu=$1; local chapitreNuInc1=$((chapitreNu+1));
        local url="$2";
        local __postnote=${3:-''};
        dl_postnote="et chapitre "$chapitreNuInc1" ${chapitreTitreTbl[$chapitreNuInc1]}. $__postnote";
        addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
        dlwget "$url" "${livreTitreNormalize_Auto}_et_chapitre${chapitreNuInc1}-${chapitreTitreTbl[$chapitreNuInc1]}.mp3";
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    function dlwgetTroisChapitres(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "$*";
        if [[ $# -lt 2 ]];  then return $E_ARG_REQUIRE;fctOut "${FUNCNAME[0]}"; fi
        local -i chapitreNu=$1; local chapitreNuInc1=$((chapitreNu+1)); local chapitreNuInc2=$((chapitreNu+2));
        local url="$2";
        local __postnote=${3:-''};
        dl_postnote="et chapitre "$chapitreNuInc1" ${chapitreTitreTbl[$chapitreNuInc1]} et chapitre $chapitreNuInc2 ${chapitreTitreTbl[$chapitreNuInc2]}. $__postnote";
        addChapitre "${chapitreTitreTbl[$chapitreNu]}" $chapitreNu;
        dlwget "$url" "${livreTitreNormalize_Auto}_et_chapitre${chapitreNuInc1}-${chapitreTitreTbl[$chapitreNuInc1]}_et_chapitre${chapitreNuInc2}-${chapitreTitreTbl[$chapitreNuInc2]}.mp3";
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    # Telecharge via dlyt en cherchant la durée et met à jour $livreTitreNormalize_Auto
    function dlytLivreAuto(){
        if $isTrapCAsk ; then return $E_CTRL_C ; fi
        fctIn "$*";
        if [[ $# -eq 0 ]];          then fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;fctOut "${FUNCNAME[0]}"; fi
        local referenceVideoYoutube="$1";
        setDureeYT "$referenceVideoYoutube";
        dlyt "$referenceVideoYoutube"  "$livreTitreNormalize_Auto";
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }

    # Telecharge via dlyt en cherchant la durée et l'ajoutant au titre
    function dlytDuree(){
        if $isTrapCAsk ; then return $E_CTRL_C ; fi
        fctIn "$*";
        if [[ $# -eq 0 ]];          then fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;fctOut "${FUNCNAME[0]}"; fi
        local referenceVideoYoutube="$1";
        setDureeYT "$referenceVideoYoutube";
        dlyt "$referenceVideoYoutube"  "$livreTitreNormalize_Auto";
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    # Utilise la variable globale 'formatNo'
    # usage:
    # dlyt "$referenceVideoYoutube" "$destination"
    # variables globales mises à jours: dlyt_filename, dlyt_file_name, dlyt_file_ext
    function dlyt(){
        if $isTrapCAsk ; then return $E_CTRL_C ; fi
        fctIn "$*";

        #retour commun en cas de fail pour la fonction parent (Ne pas utiliser fctIn) mais laisser fctOut "${FUNCNAME[0]}";
        function retourDlFail(){
            ((stats['dl_fail']++))
            ((stats['yt_dl_fail']++))
            fctOut "${FUNCNAME[0]}(0)";          # LAISSER CAR ELLE EST DESTINÉ A LA FONCTION PARENT
            return 0;
        }
        
        #  - Reinitialise des variables globales - #
        dlyt_filename='';                   
        dlyt_file_name='';     #displayVar 'dlyt_file_name' "$dlyt_file_name";
        dlyt_file_ext=''

        local destination='';
        local _destinationFileName='';

        if [ $# -ne 2 ]
        then
            erreursAddEcho "${FUNCNAME[0]}[$#/2: 'referenceVideo' 'destination') ($*)";
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi

        titre3 "Téléchargement via ${FUNCNAME[0]}($*)";

        local referenceVideoYoutube="${1:-""}";
        if [ "$referenceVideoYoutube" = '' ]
        then
            erreursAddEcho "${FUNCNAME[0]}($*): Pas de reference YT"
            retourDlFail; return $E_ARG_REQUIRE;
        fi

        ((stats['dl_total']++))
        ((stats['yt_dl_total']++))

        if [ $# -lt 2 ]
        then
            erreursAddEcho "${FUNCNAME[0]}: '$referenceVideoYoutube' sans destination"
            retourDlFail; return $E_ARG_REQUIRE;
        fi
        destination="$(echoNormalizeText "$2")";
        displayVarDebug 'destination' "$destination";

        dlCoverFileYT "$referenceVideoYoutube" "$destination";

        PARAM_TITRE='';
        PARAM_TITRE="$PARAM_TITRE --format $formatNo";

        # - Calcul du nom du fichier et de la refHHTP - #
        local _refHTTP='';
        local destinationConvertName='';
        if [ $isCodeYoutube -eq 1 ]
        then
            _destinationFileName="$destination-$referenceVideoYoutube.$formatExt";
            destinationConvertName="$destination-$referenceVideoYoutube.$convert";
            _refHTTP="https://www.youtube.com/watch?v=$referenceVideoYoutube";
        else
            _destinationFileName="$destination.$formatExt";
            destinationConvertName="$destination.$convert";
            _refHTTP="$referenceVideoYoutube";
        fi

        # - Téléchargement demandé - #
        if [ $isNoDownload -eq 0 ]
        then
            # Si convertion demandé, Le fichier de convertion existe t'il?
            local fileNameSansExt="${_destinationFileName%.*}";
            local fileNameConvert="$fileNameSansExt.$convert";
            #displayVar "convert" "$convert" "fileNameConvert" "$fileNameConvert"

            # - Le fichier est il deja convertit? - #
            if [ "$convert" != '' ] && [ -f "$fileNameConvert" ]
            then
                notifsAddEcho "${FUNCNAME[0]}(): '$referenceVideoYoutube' '$_destinationFileName': déja convertit($convert)."
                ((stats['cv_exist']++))
                #((stats['yt_cv_exist']++))

                # - Force la sauvegarde des tags si demander - #
                if [ $isSaveTags -eq 1 ]; then saveTags "$fileNameConvert"; fi
                fctOut "${FUNCNAME[0]}(0)";return 0;
            fi

            titre4 'Téléchargement';
            # - Le fichier est-il deja téléchargé? - #
            if [ -f "$_destinationFileName" ]
            then
                notifsAddEcho "${FUNCNAME[0]}(): '$referenceVideoYoutube' '$_destinationFileName' pré-existe":
                ((stats['dl_exist']++))
                ((stats['yt_dl_exist']++))

                # - Force la sauvegarde des tags si demandé - #
                if [ $isSaveTags -eq 1 ]; then saveTags "$_destinationFileName"; fi

            else     ## - Telechargement - ##

                # - test erreur de parametre - #
                if [ $formatNo -eq 0 ]
                then
                    erreursAddEcho "${FUNCNAME[0]}(): '$referenceVideoYoutube' Format=0.";
                    fctOut "${FUNCNAME[0]}($E_ARG_BAD)"; return $E_ARG_BAD;
                fi

                # -- TELECHARGEMENT -- #
                if $isTrapCAsk 
                then
                    retourDlFail;
                    return $E_CTRL_C;
                    fi

                local _param='';
                #_param="$YTDL_PARAM_GLOBAL $_param --print-json"
                _param="$YTDL_PARAM_GLOBAL $_param --newline";
                titreCmd "yt-dlp \"$_destinationFileName\" \n-> \"$_refHTTP\"";
                titreInfo "Lancer 'tail -f $LOG_PATH' sur un autre termninal pour voir l'avancement du téléchargement";
                
                # Obligé de passer par eval sinon yt-dlp génére une erreur
                #eval_echo_D "yt-dlp $_param $PARAM_TITRE --output \"$_destinationFileName\" \"$_refHTTP\" 1>> $LOG_PATH";
                evalEcho "yt-dlp $_param $PARAM_TITRE --output \"$_destinationFileName\" \"$_refHTTP\" 1>> $LOG_PATH";
                
                # - Verifications du fichier recut - #                    
                if [ ! -f  "$_destinationFileName" ]
                then
                    #displayVar "yt-dlp sortie" "<>0"
                    erreursAddEcho "${FUNCNAME[0]}($*): Le fichier '$_destinationFileName' n'a pas été téléchargé.";
                    retourDlFail;return $E_INODE_NOT_EXIST;
                fi

                # Le fichier existe: test de la taille
                # [ ! -s ] #existe non null
                local -i _tailleFichier=$(stat -c "%s" "$_destinationFileName");
                displayVarDebug '_tailleFichier' "$_tailleFichier";
                if [[ $_tailleFichier -eq 0 ]]
                then
                    erreursAddEcho "${FUNCNAME[0]}($*): Le fichier '$_destinationFileName' à une taille nulle.";
                    retourDlFail; return $E_INODE_NOT_EXIST;
                fi


                titre4 'Sauvegarde des tags';
                # - Action finale - #
                file      "$_destinationFileName";
                saveTags  "$_destinationFileName";
                notifsAddEcho "${FUNCNAME[0]}($*):  '$referenceVideoYoutube' $_destinationFileName";
                ((stats['dl_ok']++))
                ((stats['yt_dl_ok']++))
            fi
        fi

        # - Conversion vers mono - #
        ffmpeg_mono "$_destinationFileName";

        ## - Convertion - ##
        convertir "$_destinationFileName";

        # Sauvegarde le nom de fichier calculé
        dlyt_filename="$_destinationFileName";           #displayVar 'dlyt_filename' "$dlyt_filename";
        dlyt_file_name="${_destinationFileName%.*}";     #displayVar 'dlyt_file_name' "$dlyt_file_name";
        dlyt_file_ext="${_destinationFileName##*.}";     #displayVar 'dlyt_file_ext' "$dlyt_file_ext";
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    # dlod "$referenceVideoOdysee" "$destination"
    function dlod(){
        if $isTrapCAsk ; then return $E_CTRL_C ; fi
        fctIn "$*";
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho ". ${FUNCNAME[0]}[$#/2](\"$referenceVideo\" \"$destination\")";
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi

        titre3 'Téléchargement via ${FUNCNAME[0]}()';

        local referenceVideoOdysee="$1";
        if [[ "$referenceVideoOdysee" == '' ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($*): Pas de reference Odysee";
            fctOut "${FUNCNAME[0]}($E_ARG_BAD)"; return $E_ARG_BAD;
        fi

        local destination='';
        local _destinationFileName='';

        ((stats['dl_total']++))
        ((stats['od_dl_total']++))

        if [[ $# -lt 2 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($*): $referenceVideoOdysee sans destination";
            ((stats['dl_fail']++))
            ((stats['od_dl_fail']++))
            fctOut "${FUNCNAME[0]}($E_ARG_BAD)"; return $E_ARG_BAD;
        fi

        getNormalizeText "$2"
        local destination="$normalizeText"
        
        #dlCover "$referenceVideoOdysee" "$destination"

        PARAM_TITRE='';
        PARAM_TITRE="$PARAM_TITRE --format $odFormat";

        # - Calcul du nom du fichier et de la refHHTP - #
        local _refHTTP='';
        local destinationConvertName='';
        if [[ $isCodeOdysee -eq 1 ]]
        then
            #_destinationFileName="$destination-$referenceVideoOdysee.$formatExt"
            #destinationConvertName="$destination-$referenceVideoOdysee.$convert"

            _destinationFileName="$destination.$formatExt";
            destinationConvertName="$destination.$convert";

            _refHTTP="https://odysee.com/$referenceVideoOdysee";
        else
            _destinationFileName="$destination.$formatExt";
            destinationConvertName="$destination.$convert";
            _refHTTP="$referenceVideoOdysee";
        fi

        # - Téléchargement demandé - #
        if [[ $isNoDownload -eq 0 ]]
        then
            # Si convertion demandé, Le fichier de convertion existe t'il?
            local fileNameSansExt=${_destinationFileName%.*};
            local fileNameConvert="$fileNameSansExt.$convert";
            #displayVar "convert" "$convert" "fileNameConvert" "$fileNameConvert";

            # - Le fichier est il deja convertit? - #
            if [ "$convert" != '' -a -f "$fileNameConvert" ]
            then
                notifsAddEcho "${FUNCNAME[0]}($*): '$referenceVideoOdysee' '$_destinationFileName': déja convertit($convert).";
                ((stats['cv_exist']++))
                #((stats['yt_cv_exist']++))

                # - Force la sauvegarde des tags si demander - #
                if [[ $isSaveTags -eq 1 ]]; then saveTags "$fileNameConvert"; fi
                return
            fi

            # - Si le fichier est deja téléchargé - #
            if [[ -f "$_destinationFileName" ]]
            then
                notifsAddEcho "${FUNCNAME[0]}($*): '$referenceVideoOdysee' '$_destinationFileName'; pré-existe.";
                ((stats['dl_exist']++))
                ((stats['od_dl_exist']++))

                # - Force la sauvegarde des tags si demander - #
                if [[ $isSaveTags -eq 1 ]]; then saveTags "$_destinationFileName"; fi

            else     ## - Telechargement - ##
                #echo $LINENO;showDatas

                local _param=''
                #_param="$YTDL_PARAM_GLOBAL $_param --print-json"
                _param="$YTDL_PARAM_GLOBAL $_param --newline";
                echo_D  "yt-dlp $_param $PARAM_TITRE --output \"$_destinationFileName\" \"$_refHTTP\"";
                titreCmd "yt-dlp \"$_destinationFileName\" \n --> \"$_refHTTP\"";
                local ydl_erreur=$(yt-dlp $_param $PARAM_TITRE --output "$_destinationFileName" "$_refHTTP" 2>&1 >/dev/null);
                if [[ "$ydl_erreur" == '' ]]
                then
                    displayVar 'yt-dlp sortie' '0';
                    file      "$_destinationFileName";
                    saveTags  "$_destinationFileName";
                    notifsAddEcho "${FUNCNAME[0]}($*): '$referenceVideoOdysee' '$_destinationFileName'";
                    ((stats['dl_ok']++))
                    ((stats['yt_dl_ok']++))
                else 
                    displayVar "yt-dlp sortie" "<>0";
                    erreursAddEcho "${FUNCNAME[0]}($*): '$referenceVideoOdysee' '$destinationFileName' -> $ydl_erreur";
                    ((stats['dl_fail']++))
                    ((stats['od_dl_fail']++))
                fi
            fi
        fi

        ## - Convertion - ##
        convertir "$destinationFileName";

        # Sauvegarde le nom de fichier calculé
        dlyt_filename="$_destinationFileName";           #displayVar 'dlyt_filename' "$dlyt_filename";
        dlyt_file_name="${_destinationFileName%.*}";     #displayVar 'dlyt_file_name' "$dlyt_file_name";
        dlyt_file_ext="${_destinationFileName##*.}";     #displayVar 'dlyt_file_ext' "$dlyt_file_ext";

        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    # dlArchiveOrg "url "destination"
    # telecharge dans le repertoire courant
    function dlAO(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        fctIn "$*";
        if [[ $# -eq 0 ]]
        then
            erreursAddEcho ". ${FUNCNAME[0]}[$#/2](\"$url\" \"$destination\")";
            fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi

        titre3 'Téléchargement via ${FUNCNAME[0]}()';
        local url="$1";
        if [[ "$url" == '' ]]
        then
            erreursAddEcho "${FUNCNAME[0]}($*): Pas d'url fournis";
            fctOut "${FUNCNAME[0]}($E_ARG_BAD)"; return $E_ARG_BAD;
        fi

        #((stats['ao_total']++))
        #((stats['ao_dl_total']++))

        if [[ $# -lt 2 ]]
        then
            erreursAddEcho "${FUNCNAME[0]}: $url sans destination";
            ((stats['dl_fail']++))
            ((stats['ao_dl_fail']++))
            fctOut "${FUNCNAME[0]}($E_ARG_BAD)"; return $E_ARG_BAD;
        fi

        getNormalizeText "$2"
        destinationFileName="$normalizeText"


        PARAM_TITRE='';
        #PARAM_TITRE="$PARAM_TITRE --format $formatNo"
        PARAM_TITRE="$PARAM_TITRE --format 1"

        ## - Telechargement - ##
        if [[ -f "$destinationFileName" ]]
        then
            notifsAddEcho "${FUNCNAME[0]}($*): '$url' '$destinationFileName': pré-existe.";
            ((stats['dl_exist']++))
            ((stats['ao_dl_exist']++))

            # - Force la sauvegarde des tags si demander - #
            if [[ $isSaveTags -eq 1 ]]
            then
                saveTags "$destinationFileName";
            fi

        elif [[ $isNoDownload -eq 0 ]]
        then
            fileNameSansExt=${destinationFileName%.*};
            local fileNameConvert="$fileNameSansExt.$convert";
            if [ "$convert" != '' -a -f "fileNameConvert" ]
            then
                echo_D "Deja convertit";
                notifsAddEcho "${FUNCNAME[0]}($*):  '$destinationFileName': déja convertit.";
                #((stats['cv_exist']++))
                #((stats['ao_cv_exist']++))
            else
                #PARAM="$YTDL_PARAM_GLOBAL --format $formatNo"
                PARAM="$YTDL_PARAM_GLOBAL --format 1";
                titreCmd "yt-dlp \"$destinationFileName\" \n --> \"$url\"";
                local ydl_erreur=$(yt-dlp ${YTDL_PARAM_GLOBAL} ${PARAM_TITRE} --output "$destinationFileName" "$url" 2>&1 >/dev/null);
                if [[ "$ydl_erreur" == '' ]]
                then
                    file      "$destinationFileName";
                    saveTags  "$destinationFileName";
                    notifsAddEcho "${FUNCNAME[0]}($*): yt-dlp '$url' '$destinationFileName'";
                    ((stats['dl_ok']++))
                    ((stats['ao_dl_ok']++))
                else 
                    erreursAddEcho "${FUNCNAME[0]}($*):  '$url' '$destinationFileName' -> yt-dlp: $ydl_erreur";
                    ((stats['dl_fail']++))
                    ((stats['ao_dl_fail']++))
                fi
            fi
        fi
        ## - Convertion - ##
        convertir "$destinationFileName";
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#
