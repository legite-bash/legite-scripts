echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.13
# Description: 

declare -gr RELEASE_NAME="release";
declare -gr RELEASE_APPEND_NAME="release-append.txt";
declare -gr RELEASE_APPEND_MODELE_NAME="release-append-modele.txt";
declare -g  release_append_content="";         # contenu du fichier de $RELEASE_APPEND_NAME

##Configuration globale de Git

#git config --global user.name "fenwe"
#git config --global user.email "pascal.toledo@legral.fr"

##Créer un nouveau dépôt

#git clone git@framagit.org:legite-bash/collections.git
#cd collections
#git switch --create main
#touch README.md
#git add README.md
#git commit -m "add README"
#git push --set-upstream origin main

##Pousser un dossier existant

#cd existing_folder
#git init --initial-branch=main
#git remote add origin git@framagit.org:legite-bash/collections.git
#git add .
#git commit -m "Initial commit"
#git push --set-upstream origin main

##Pousser un dépôt Git existant
#cd existing_repo
#git remote rename origin old-origin
#git remote add origin git@framagit.org:legite-bash/collections.git
#git push --set-upstream origin --all
#git push --set-upstream origin --tags


#############
# release-* #
#############

# clear; gtt.sh git release-rolling
addLibrairie 'release-rolling' "Ajoute le contenu du fichier '$RELEASE_APPEND_NAME' dans release.txt du répertoire local";
function      release-rolling(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    echo "***********************************";
     if [ ! -f "$RELEASE_APPEND_NAME" ]
    then
        release-append-clean;
    else
        release_append_content+="$(cat "$RELEASE_APPEND_NAME")\n";
        release_append_content+="=== FIN DU COMMIT: $(date +'%Y.%m.%d-%HH%M') ===\n";
        echo -e "$release_append_content" >> "$RELEASE_NAME";
        release-append-clean;
    fi

    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}


# Réinitialise le fichier release-append
function      release-append-clean(){
    local -i _fct_pile_app_level=3;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    echo "=== NOUVEAU COMMIT ===" > "$RELEASE_APPEND_NAME";

    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}

#############
# git-commit* #
#############
#function git-commit()
# execute un commit dans le repertoire local
# avec les données du fichier 'release-append.txt'
# gtt.sh apps/git git-commit
addLibrairie 'git-commit' 'execute un commit dans le repertoire local avec les données du fichier release-append.txt puis le vide'
function      git-commit(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    local _rep="${1:-"$PWD"}";

    if ! cd "$_rep"
    then
        erreurs-pile-add-echo "${FUNCNAME[0]}: Impossible d'aller dans le repertoire '$_rep'.";
        fct-pile-app-out $E_INODE_NOT_EXIST $_fct_pile_app_level; return $E_INODE_NOT_EXIST;
    fi

    if [ ! -d '.git' ]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]}: Pas de git dans ce repertoire";
        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    fi    

    
    release-rolling;


    git add .
    git commit -m "$release_append_content";
    _erreurNo=$?;
    release_append_content="";

    if [ $_erreurNo -ne 0 ]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]}: Erreur($_erreurNo) lors du commit.";
        fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
    fi

    git push;
    _erreurNo=$?;

    if [ $_erreurNo -ne 0 ]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]}: Erreur($_erreurNo) lors du push.";
    fi

    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}

return 0;
