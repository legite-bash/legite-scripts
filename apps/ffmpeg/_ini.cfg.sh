echo-d "${BASH_SOURCE[0]}" 1;


#saveDebugLevel $DEBUG_LEVEL_DEFAUT;

# https://www.malekal.com/comment-utiliser-ffmpeg-exemples/
# recadrage: 
# https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Vector_Video_Standards.png/1024px-Vector_Video_Standards.png
# ratio
# 

declare -gr FFMPEG_APP_REP="$APPS_REP/ffmpeg";
declare -r VERSION_FFMPEG='2024.08.13';
if  $isVersion;then display-vars 'VERSION_FFMPEG' "$VERSION_FFMPEG";fi

require-once "${FFMPEG_APP_REP}/core/ffmpeg-vars.sh";
require-once "${FFMPEG_APP_REP}/core/ffmpeg-core.sh";
require-once "${FFMPEG_APP_REP}/core/ffmpeg-core-metadata.sh";
require-once "${FFMPEG_APP_REP}/core/ffmpeg-core-cover.sh";
require-once "${FFMPEG_APP_REP}/core/ffmpeg-core-ffmpeg-infos-pathname.sh";
require-once "${FFMPEG_APP_REP}/core/ffmpeg-core-ffmpeg-infos.sh";
require-once "${FFMPEG_APP_REP}/core/ffmpeg-core-inOut.sh";  # depend de ffmpeg-core-ffmpeg-infos
#require-once "${FFMPEG_APP_REP}/core/ffmpeg-core-ffmpeg_infos-recursif.sh";
require-once "${FFMPEG_APP_REP}/core/ffmpeg-core-methode.sh";
require-once "${FFMPEG_APP_REP}/core/ffmpeg-core-mappage.sh";
require-once "${FFMPEG_APP_REP}/core/ffmpeg-core-exec.sh";
#require-once "${FFMPEG_APP_REP}/core/ffmpeg-core-profil-load.sh";
require-once "${FFMPEG_APP_REP}/core/ffmpeg-core-outils.sh";
require-once "${FFMPEG_APP_REP}/core/ffmpeg-core-outils-extraire.sh";


return 0;
