echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh --show-libsLevel2 ffmpeg lib
# date de création    : 2024.08.04
# date de modification: 2024.10.16

#########
# ratio #
#########

    # clear; gtt.sh -d --libExecAuto   apps/ffmpeg/tests/ffmpeg-core-outils/ test-ffmpeg-fct-ffmpeg-scale-show
    # ffmpeg-scale-show X Y
    addLibrairie 'ffmpeg-scale-show' "ffmpeg-scale-show - X= Y= #Affiche les ratios sous différentes définitions" ;
    # function      ffmpeg-scale-show(  X [Y] ){
    function      ffmpeg-scale-show() {
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local -i  _X="${1:-0}";
        if [ "$_X" = 0  ]
        then
            _X=${PSP_params['X']:-0};
            if [ "$_X" = 0  ]
            then
                erreurs-pile-add-echo "${FUNCNAME[0]} - X=0 Y=y # X ne peut être égal à 0";
                fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level;return $E_ARG_BAD;
            fi
        fi
        local -i  _Y="${2:-0}";
        if [ "$_Y" = 0  ]
        then
            _Y=${PSP_params['Y']:-0};
            if [ "$_Y" = 0  ]
            then
                erreurs-pile-add-echo "${FUNCNAME[0]} - X=$_X Y=0 # X ne peut être égal à 0";
                fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
            fi
        fi
        local     _rt=0;    # decimal not integer
        local     _rF=0;    # ratio de un format
        local -i  x1=0 x2=0 x3=0 x4=0 x5=0 x6=0 x6=0 x7=0 x8=0 x9=0;
        local     y=0:  # Valeur de Y dans le format local
        if [ $_X -eq 0 ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]} - X=largeur [Y=hauteur]";
            fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level;return $E_ARG_BAD;
        fi

        if [ $_Y -ne 0 ];    then _rt=$(echo "scale=2;$_X/$_Y" | bc   );fi

        colonnes-init;
        colonnes-setOut '/dev/stdout';

        #colonnes-lgtMax=100;Y
        colonnes-lg    -4 -5 -5 -5 -5 -5 -5 -5 -5 -5 -5 -5;
        colonnes-dir    L  R  R  R  R  R  R  R  R  R  R  R;
        colonnes-sep   '|' ;

        titreInfo "ratio:$_rt - 16/9=1.77 - 16/10=1.60 - 3/2=1.50 - 4/3=1.33 - 5/4=1,25";

        if [ $_Y -ne 0 ]
        then
            x1=854;x2=1280;x3=1920;x4=0;x5=480;x6=640;x7=0;x8=0;x9=0;
            titre4 "$_rt";
            colonnes-datas-values-show 'X'    $_X         $x1                   $x2                   $x3                   $x4                   $x5                   $x6                   $x7                   $x8                   $x9          ;
            colonnes-datas-values-show "$_rt" $_Y $(echo "$x1/$_rt"|bc) $(echo "$x2/$_rt"|bc) $(echo "$x3/$_rt"|bc) $(echo "$x4/$_rt"|bc) $(echo "$x5/$_rt"|bc) $(echo "$x6/$_rt"|bc) $(echo "$x7/$_rt"|bc) $(echo "$x8/$_rt"|bc) $(echo "$x9/$_rt"|bc);
        fi


        _rF=$(echo "scale=2;16/9"|bc -l)
        x1=854;x2=1280;x3=1920;x4=0;x5=480;x6=640;x7=0;x8=0;x9=0;
        titre4 "16/9:$_rF";
        colonnes-datas-values-show 'X'    $_X         $x1                   $x2                   $x3                   $x4                   $x5                   $x6                   $x7                   $x8                   $x9          ;
        colonnes-datas-values-show "$_rt" $_Y $(echo "$x1/$_rt"|bc) $(echo "$x2/$_rt"|bc) $(echo "$x3/$_rt"|bc) $(echo "$x4/$_rt"|bc) $(echo "$x5/$_rt"|bc) $(echo "$x6/$_rt"|bc) $(echo "$x7/$_rt"|bc) $(echo "$x8/$_rt"|bc) $(echo "$x9/$_rt"|bc);
        if [ $_Y -ne 0 ]
        then
            _y=$(echo "$_Y/$_rF"|bc)
        colonnes-datas-values-show "$_rF" $_y $(echo "$x1/$_rF"|bc) $(echo "$x2/$_rF"|bc) $(echo "$x3/$_rF"|bc) $(echo "$x4/$_rF"|bc) $(echo "$x5/$_rF"|bc) $(echo "$x6/$_rF"|bc) $(echo "$x7/$_rF"|bc) $(echo "$x8/$_rF"|bc) $(echo "$x9/$_rF"|bc);
        fi

        _rF=$(echo "scale=2;16/10"|bc -l)
        x1=320;x2=1680;x3=1920;x4=2560;x5=0;x6=480;x7=640;x8=0;x9=0;
        titre4 "16/10:$_rF";
        colonnes-datas-values-show 'X'    $_X         $x1                   $x2                   $x3                   $x4                   $x5                   $x6                   $x7                   $x8                   $x9          ;
        colonnes-datas-values-show "$_rt" $_Y $(echo "$x1/$_rt"|bc) $(echo "$x2/$_rt"|bc) $(echo "$x3/$_rt"|bc) $(echo "$x4/$_rt"|bc) $(echo "$x5/$_rt"|bc) $(echo "$x6/$_rt"|bc) $(echo "$x7/$_rt"|bc) $(echo "$x8/$_rt"|bc) $(echo "$x9/$_rt"|bc);
        if [ $_Y -ne 0 ]
        then
            y=$(echo "$_Y/$_rF"|bc)
        colonnes-datas-values-show "$_rF" $_y $(echo "$x1/$_rF"|bc) $(echo "$x2/$_rF"|bc) $(echo "$x3/$_rF"|bc) $(echo "$x4/$_rF"|bc) $(echo "$x5/$_rF"|bc) $(echo "$x6/$_rF"|bc) $(echo "$x7/$_rF"|bc) $(echo "$x8/$_rF"|bc) $(echo "$x9/$_rF"|bc);
        fi

        _rF=$(echo "scale=2;3/2"|bc -l)
        x1=720;x2=1152;x3=1280;x4=1440;x5=0;x6=0;x7=0;x8=0;x9=0;
        titre4 "3/2:$_rF";
        colonnes-datas-values-show 'X'    $_X         $x1                   $x2                   $x3                   $x4                   $x5                   $x6                   $x7                   $x8                   $x9          ;
        colonnes-datas-values-show "$_rt" $_Y $(echo "$x1/$_rt"|bc) $(echo "$x2/$_rt"|bc) $(echo "$x3/$_rt"|bc) $(echo "$x4/$_rt"|bc) $(echo "$x5/$_rt"|bc) $(echo "$x6/$_rt"|bc) $(echo "$x7/$_rt"|bc) $(echo "$x8/$_rt"|bc) $(echo "$x9/$_rt"|bc);
        if [ $_Y -ne 0 ]
        then
            y=$(echo "$_Y/$_rF"|bc)
        colonnes-datas-values-show "$_rF" $_y $(echo "$x1/$_rF"|bc) $(echo "$x2/$_rF"|bc) $(echo "$x3/$_rF"|bc) $(echo "$x4/$_rF"|bc) $(echo "$x5/$_rF"|bc) $(echo "$x6/$_rF"|bc) $(echo "$x7/$_rF"|bc) $(echo "$x8/$_rF"|bc) $(echo "$x9/$_rF"|bc);
        fi

        _rF=$(echo "scale=2;4/3"|bc -l)
        x1=320;x2=640;x3=768;x4=800;x5=1024;x6=1280;x7=1400;x8=1600;x9=2048;
        titre4 "3/2:$_rF";
        colonnes-datas-values-show 'X'    $_X         $x1                   $x2                   $x3                   $x4                   $x5                   $x6                   $x7                   $x8                   $x9          ;
        colonnes-datas-values-show "$_rt" $_Y $(echo "$x1/$_rt"|bc) $(echo "$x2/$_rt"|bc) $(echo "$x3/$_rt"|bc) $(echo "$x4/$_rt"|bc) $(echo "$x5/$_rt"|bc) $(echo "$x6/$_rt"|bc) $(echo "$x7/$_rt"|bc) $(echo "$x8/$_rt"|bc) $(echo "$x9/$_rt"|bc);
        if [ $_Y -ne 0 ]
        then
            _y=$(echo "$_Y/$_rF"|bc)
        colonnes-datas-values-show "$_rF" $_y $(echo "$x1/$_rF"|bc) $(echo "$x2/$_rF"|bc) $(echo "$x3/$_rF"|bc) $(echo "$x4/$_rF"|bc) $(echo "$x5/$_rF"|bc) $(echo "$x6/$_rF"|bc) $(echo "$x7/$_rF"|bc) $(echo "$x8/$_rF"|bc) $(echo "$x9/$_rF"|bc);
        fi

        _rF=$(echo "scale=2;5/4"|bc -l)
        x1=1280;x2=0;x3=360;x4=640;x5=0;x6=0;x7=0;x8=0;x9=0;
        titre4 "5/4:$_rF";
        colonnes-datas-values-show 'X'    $_X         $x1                   $x2                   $x3                   $x4                   $x5                   $x6                   $x7                   $x8                   $x9          ;
        colonnes-datas-values-show "$_rt" $_Y $(echo "$x1/$_rt"|bc) $(echo "$x2/$_rt"|bc) $(echo "$x3/$_rt"|bc) $(echo "$x4/$_rt"|bc) $(echo "$x5/$_rt"|bc) $(echo "$x6/$_rt"|bc) $(echo "$x7/$_rt"|bc) $(echo "$x8/$_rt"|bc) $(echo "$x9/$_rt"|bc);
        if [ $_Y -ne 0 ]
        then
            _y=$(echo "$_Y/$_rF"|bc)
        colonnes-datas-values-show "$_rF" $_y $(echo "$x1/$_rF"|bc) $(echo "$x2/$_rF"|bc) $(echo "$x3/$_rF"|bc) $(echo "$x4/$_rF"|bc) $(echo "$x5/$_rF"|bc) $(echo "$x6/$_rF"|bc) $(echo "$x7/$_rF"|bc) $(echo "$x8/$_rF"|bc) $(echo "$x9/$_rF"|bc);
        fi

        fct-pile-app-out 0 $_fct_pile_app_level;return 0;
    }
#


#######################
# ffmpeg-streams-show #
#######################
    # clear;gtt.sh apps/ffmpeg/ ffmpeg-streams-show - src=pathname
    # clear; gtt.sh -d --libExecAuto apps/ffmpeg/tests/ffmpeg-core-outils/ test-ffmpeg-fct-ffmpeg-streams-show
    # ffmpeg-scale-show 'src'
    addLibrairie 'ffmpeg-streams-show' "- src=pathname; # Affiche les streams du fichier";
    function      ffmpeg-streams-show(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _src="${1:-""}";
        local _streams_brut='';

        # _src, priorité
        # 1- $1
        # 2 - ${PSP_params['src'] 
        # 3-  valeur de $ffmpeg_in_pathname
        local _src="${1:-""}";
        ffmpeg_infos_refresh=true;
        if [ "$_src" = '' ]
        then
            _src="${PSP_params['src']:-""}";
            if [ "$_src" = '' ]
            then
                _src="$ffmpeg_infos_pathname";
                if [ "$_src" = '' ]
                then
                    erreurs-pile-add-echo "${FUNCNAME[0]} - src=";
                    fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
                fi
            fi
        fi
        #display-vars '_src' "$_src";

        if [ ! -f "$_src" ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}('$_src'):Fichier introuvable.";
            fct-pile-gtt-out $E_INODE_NOT_EXIST $_pile_fct_gtt_level; return $E_INODE_NOT_EXIST;
        fi

        ffprobe -hide_banner -i "$_src" 2>"$tmp_rep/ffp${ffmpeg_uuid}.txt";
        _streams_brut="$(grep Stream "$tmp_rep/ffp${ffmpeg_uuid}.txt")";
        rm "$tmp_rep/ffp${ffmpeg_uuid}.txt";

        local _stream='';
        for _stream_brut in $_streams_brut
        do
            _stream=" ${_stream_brut#*#}";
            _stream="${_stream%fps*}";
            #_stream+='fps';
            echo $_stream;
        done
        
        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }
#


###################
# ffmpeg-crop-add #
###################
    # ffmpeg-crop-add-add( _pathname HHMMSS.mmm  [_crop_value]){
    # si _crop_value est vide: detect les dimensions du crop à partir d HHMMSS.mmm
    # Demande à l'utilisateur si mise à jours de ffmpeg_filtres_params
    # https://ffmpeg.org/ffmpeg-filters.html#toc-crop
    # format crop: largeur:hauteur:x0:y0
    # retour:
    # E_TRUE: si l'utilisateur accepte le rendu avec les valeurs du crop
    # E_FALSE si l'utilisateur refuse  le rendu avec les valeurs du crop

    # https://www.baeldung.com/linux/ffmpeg-crop-resize-video

    addLibrairie 'ffmpeg-crop-add' " - src=\"\" time=\"HH:MM:SS.sss\" [crop=\"W:H:X:Y\"] # Supprime les bandes noires";
    function      ffmpeg-crop-add(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        titre3 "${FUNCNAME[0]}";


        # - src - #
            # priorité:
            # 1- $1
            # 2- $PSP_params['src']
            # 3 - ffmpeg_in_pathname
            local _src="${1:-""}";
            if [ -z "$_src" ]
            then
                _src="${PSP_params['src']:-""}";
                if [ -z "$_src" ]
                then
                    _src="$ffmpeg_in_pathname";
                    if [ -z "$_src" ]
                    then
                        erreurs-pile-add-echo "${FUNCNAME[0]} - src='pathname' | ffmpeg_in_pathname";
                        fct-pile-app-out $E_ARG_REQUIRE $_fct_pile_app_level; return $E_ARG_REQUIRE;
                    fi
                fi
            fi

        # - time - #
            local _time="${2:-""}";
            if [ "$_time" = '' ]
            then
                _time="${PSP_params['time']:-""}";
                if [ "$_time" = '' ]
                then
                    #erreurs-pile-add-echo "${FUNCNAME[0]} - time='HH:MM:SS.sss'";
                    #fct-pile-app-out $E_ARG_REQUIRE $_fct_pile_app_level; return $E_ARG_REQUIRE;
                    _time='00:01:00';
                fi
            fi

        #hr;
        # - crop value - #
            local _crop_value="${3:-""}";
            if [ "$_crop_value" = '' ]
            then
                _crop_value="${PSP_params['crop']:-""}";
            fi
            display-vars '_crop_value' "$_crop_value";
        #

        titre4 "crop-1-Rechercher des valeurs du crop";
        titreInfo "Pour suivre le log: tail -f '$ffmpeg_log_pathname'";

        hr;
        titre4 "crop-1-Rechercher des valeurs du crop";
            local _crop_cmd="";             display-vars '_crop_cmd' "$_crop_cmd";
            local _ffmpeg_crop_value="";


            if [ "$_crop_value" = "" ]
            then
                # - Recherche du crop auto - #
                titreCmd "ffmpeg -hide_banner -i "$_src" -t $_time -vf cropdetect -f null - 2>&1 | awk '/crop/ { print \$NF }' | tail -1";
                _ffmpeg_crop_value="$(ffmpeg -hide_banner -i "$_src" -t $_time -vf cropdetect -f null - 2>&1 | awk '/crop/ { print $NF }' | tail -1)";
                #ffmpeg_profil_crop_WHXY="${_ffmpeg_crop_value#*=}";             display-vars 'ffmpeg_profil_crop_WHXY' "$ffmpeg_profil_crop_WHXY";
                #ffmpeg_profil_crop_WHXY="${ffmpeg_profil_crop_WHXY%\"*}";       display-vars 'ffmpeg_profil_crop_WHXY' "$ffmpeg_profil_crop_WHXY";
            else
                # - Crop manuel - #
                display-vars '_ffmpeg_crop_value' "$_ffmpeg_crop_value";
            fi

            ffmpeg_profil_crop_WHXY="${_ffmpeg_crop_value#crop=}";
            display-vars '_ffmpeg_crop_value' "$_ffmpeg_crop_value";

            _ffmpeg_crop_value="\"crop=$_crop_value\"";

            _crop_cmd="-vf \"crop=${_crop_value}\"";

            _ffmpeg_crop_value="\"$_ffmpeg_crop_value\"";
            display-vars '_ffmpeg_crop_value' "$_ffmpeg_crop_value";


            display-vars '_crop_cmd  ' "$_crop_cmd";
            display-vars '_crop_value' "$_crop_value";
            display-vars 'ffmpeg_profil_crop_WHXY' "$ffmpeg_profil_crop_WHXY";

        #

        hr;
        titre4 "crop-2-Afficher la difference ";
        titreInfo "image originale: '$tmp_rep/crop-A.png'";

        local _dest="$tmp_rep/crop-A.png";
        echo "$_dest">> "$ffmpeg_log_pathname";
        eval-echo "ffmpeg -hide_banner -y -ss $_time -i \"$_src\" -frames:v 1 -update -fps_mode:v passthrough -f image2 \"$_dest\" 2>> \"$ffmpeg_log_pathname\"";
        if [ $erreur_no -ne 0 ]
        then
            erreur-no-description-echo;
            #fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
        fi

        if [ -f "$_dest" ]
        then
            erreurs-add-echo "${FUNCNAME[0]} Erreur lors de la création du crop '"$_dest"' ";
            fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
        fi

        display-im6.q16 "$_dest" &


        titreInfo "image croppée  : '$tmp_rep/crop-B.png'";
        local _dest="$tmp_rep/crop-B.png";
        echo "$_dest">> "$ffmpeg_log_pathname";
        rm   "$_dest";

        eval-echo "ffmpeg -hide_banner -y -ss $_time -i \"$_src\" -frames:v 1 -update -fps_mode:v passthrough -f image2 -$_crop_cmd  \"$_dest\" 2>> \"$ffmpeg_log_pathname\"";
        if [ $erreur_no -ne 0 ]
        then
            erreur-no-description-echo;
            fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
        fi

        if [ -f "$_dest" ]
        then
            erreurs-add-echo "${FUNCNAME[0]} Erreur lors de la création du crop '"$_dest"' ";
            fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
        fi
        display-im6.q16 "$_dest" &


        titre4 "crop-3-Confirmation";
        read -n 1 -p "Ajouter le crop aux paramêtres? (-filter:v \"crop=$ffmpeg_profil_crop_WHXY\")[N/y]: " _isCrop;
        echo "";
        if [ "${_isCrop,,}" = 'y' ]
        then ffmpeg-profil-params-add " $_crop_cmd";
        else fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE ;
        fi
        
        
        display-vars 'ffmpeg_profil_params' "${ffmpeg_profil_params}";

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }
#


##########
# CONCAT #
##########
    declare -g ffmpeg_concat_file_pathname="";  # Fichier contenant les pathname à concaténer
    # ffmpeg-concat-file-add "Fichier a ajouter dans le fichier de concatenation"
    # Utilise ffmpeg_in_*
    function ffmpeg-concat-file(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        titre2 "${FUNCNAME[0]}";
        erreur_no=0;
        
        local _file='';
        local -i _arg_nb=$#;
        ffmpeg_concat_file_pathname="$tmp_rep/${ffmpeg_in_name}-concat.txt";
        echo "" > "$ffmpeg_concat_file_pathname";   # Création du fichier vide
        while [ $# -gt 0 ]
        do
            _file="${1}";
            if [ ! -f "$_file" ]; then 
                erreurs-pile-add-echo "${FUNCNAME[0]}: Fichier '$_file' introuvable";
                erreur_no=$E_INODE_NOT_EXIST;
            fi

            echo "file '$_file'" >> "$ffmpeg_concat_file_pathname";
            shift;
        done

        if [ $erreur_no -eq 0 ]
        then
            echo "$(hr)"    >> "$ffmpeg_log_pathname";
            titre0 "CONCAT" >> "$ffmpeg_log_pathname";
            
            eval-echo "ffmpeg -hide_banner $ffmpeg_params_global_y -f concat -safe 0 -i \"$ffmpeg_concat_file_pathname\" -fflags +genpts -max_muxing_queue_size 1024 -movflags +faststart -c copy \"$ffmpeg_out_pathname\"";
        fi

        fct-pile-app-out $erreur_no $_fct_pile_app_level; return $erreur_no;
    }

#

###########
# COMPARE #
###########
    # ffmpeg-images-compare( src src2 ss )
    # gtt.sh apps/ffmpeg ffmpeg_changeContainerWeb - src=pathname )
    # Utilise compare (paquet imagick)
    addLibrairie 'ffmpeg-images-compare' '- src="pathname" src2="pathname" ss='HH:MM:SS.sss' # Extrait des images de _src et _dest avec le même timestamp pour comparer la qualité.'
    function      ffmpeg-images-compare(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        # - Controle des entrées - #
            local _src="${1:-""}";
            local _src2="${2:-""}";
            local _ss="${3:-""}";

            # - src - #
            if [ "$_src" = '' ]
            then
                _src="${PSP_params['src']:-""}";
                if [ "$_src" = '' ]
                then
                    erreurs-pile-add-echo "${FUNCNAME[0]}( _src _dest ) | - src= src2= : Aucune source de définit.";
                    fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
                fi
            fi
            src-pathname "$_src";
            #src_display-vars;

            # - src2 - #
            if [ "$_src2" = '' ]
            then
                _src2="${PSP_params['src2']:-""}";
                if [ "$_src2" = '' ]
                then
                    erreurs-pile-add-echo "${FUNCNAME[0]}( _src2 _dest ) | - src= src2= : Aucune source de définit.";
                    fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
                fi
            fi
            dest-pathname "$_src2";
            #dest_display-vars;
            titre3 "${FUNCNAME[0]}($@)"

            # - ss - #
            if [ "$_ss" = '' ]
            then
                _ss="${PSP_params['ss']:-"00:01:00"}";
            fi
        #

        # - Vérification de l'existance des sources - #
        if [ ! -f "$_src" ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}: src1: '$_src'  Introuvable.";
            fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
        fi
        #echo "src:$_src: ok";

        if [ ! -f "$dest_pathname" ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}: _src2: '$dest_pathname'  Introuvable.";
            fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
        fi
        #echo "dest_pathname:$dest_pathname: ok";


        #display-vars 'TMP_ROOT' "$TMP_ROOT";
        local _src_compare_pathname="$TMP_ROOT/${src_name}-${_ss//:/-}.png";
        #ffmpeg -hide_banner -y -ss $_ss -i "$src_pathname"  -frames:v 1 -vsync 0 -f image2 "$_out1_pathname" 2>/dev/null;
        ffmpeg -hide_banner -y -ss $_ss -i "$src_pathname"  -vframes 1 -q:v 2 "$_src_compare_pathname" 2>/dev/null;
        if [ ! -f "$_src_compare_pathname" ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}: compare1: '$_src_compare_pathname'  Introuvable.";
            fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
        fi

        local dest_compare_pathname="$TMP_ROOT/${dest_name}-${_ss//:/-}.png";
        #eval-echo "ffmpeg -hide_banner -y -ss $_ss -i \"$dest_pathname\" -frames:v 1 -vsync 0 -f image2 \"$_out2_pathname\" 2>/dev/null";
        eval-echo "ffmpeg -hide_banner -y -ss $_ss -i \"$dest_pathname\"  -vframes 1 -q:v 2 \"$dest_compare_pathname\" 2>/dev/null";
        if [ ! -f "$dest_compare_pathname" ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}: compare2: '$dest_compare_pathname'  Introuvable.";
            fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
        fi


        local _out1_wc="$(wc -c $_src_compare_pathname)"; _out1_wc=${_out1_wc%% *};
        local _out2_wc="$(wc -c $dest_compare_pathname)"; _out2_wc=${_out2_wc%% *};
        #echo "${_src_compare_pathname} - $_ss - $_out1_wc";
        if [ -f "$_src_compare_pathname" ]
        then
            display "$_src_compare_pathname" &
        fi   # imagemagok 

        #echo "${dest_compare_pathname} - $_ss - $_out2_wc";
        if [ -f "$dest_compare_pathname" ]
        then
            display "$dest_compare_pathname" &

            local _metric='';
            local -A _compare_result;
            local fichier_compare_pathname='';

            _metric='NCC';
            fichier_compare_pathname="$TMP_ROOT/${src_nom}-compare.png";
            _compare_result["$_metric"]="$(compare -fuzz 0% -metric $_metric "$_src_compare_pathname" "$dest_compare_pathname" "$fichier_compare_pathname" 2>&1)";
            #display-vars "metric:$_metric" "$_compare_result";
            display "$fichier_compare_pathname" &

            _metric='AE';
            fichier_compare_pathname="$tmp_rep/${src_nom}-compare_$_metric.png";
            _compare_result["$_metric"]="$(compare -fuzz 0% -metric $_metric "$_src_compare_pathname" "$dest_compare_pathname" null: 2>&1)";
            #display-vars "metric:$_metric" "$_compare_result";

            _metric='MAE';
            fichier_compare_pathname="$tmp_rep/${src_nom}-compare_$_metric.png";
            _compare_result["$_metric"]="$(compare -fuzz 0% -metric $_metric "$_src_compare_pathname" "$dest_compare_pathname" null: 2>&1)";
            #display-vars "metric:$_metric" "$_compare_result";

            _metric='MSE';
            fichier_compare_pathname="$tmp_rep/${src_nom}-compare_$_metric.png";
            _compare_result["$_metric"]="$(compare -fuzz 0% -metric $_metric "$_src_compare_pathname" "$dest_compare_pathname" null: 2>&1)";
            #display-vars "metric:$_metric" "$_compare_result";

            _metric='PSNR';
            fichier_compare_pathname="$tmp_rep/${src_nom}-compare_$_metric.png";
            _compare_result["$_metric"]="$(compare -fuzz 0% -metric $_metric "$_src_compare_pathname" "$dest_compare_pathname" null: 2>&1)";
            #display-vars "metric:$_metric" "$_compare_result";

            _metric='RMSE';
            fichier_compare_pathname="$tmp_rep/${src_nom}-compare_$_metric.png";
            _compare_result["$_metric"]="$(compare -fuzz 0% -metric $_metric "$_src_compare_pathname" "$dest_compare_pathname" null: 2>&1)";
            #display-vars "metric:$_metric" "$_compare_result";

            local _compare_result_str='';
            for _compare_result_key in "${!_compare_result[@]}"; do
                _compare_result_str+="$_compare_result_key: '${_compare_result[$_compare_result_key]}' ";
            done
            echo "$_compare_result_str";
            #void;
        fi   # imagemagok

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }
#


##########################
# ffmpeg-changeContainer #
##########################
    #ffmpeg-changeContainerWeb( [_src] [ffmpeg_out_ext] )
    #gtt.sh apps/ffmpeg ffmpeg_changeContainerWeb - src=pathname )
    
    # Execute ffmpeg_changeContainerWeb() sur le repertoire donné en parametre et les sous repertoires
    # Utilise: ffmpeg_in_*
    # return
    # $E_ARG_BAD:source non definit
    # #E_FALSE: les liens ne sont pas convertit
    addLibrairie 'ffmpeg-changeContainerWeb' " - src=dir|pathname: Change le container (sans transcoder), du fichier ou des fichiers du ss rep, pour l'extention 'ffmpeg_out_ext' si les codecs A/V sont compatibles web";
    function      ffmpeg-changeContainerWeb(){
        if $isTrapCAsk;then return $E_CTRL_C;fi

        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        titre1 "${FUNCNAME[0]}()";
        # _src, priorité
        # 1- $1
        # 2 - ${PSP_params['src'] 
        # 3-  valeur de $ffmpeg_in_pathname
        local _src="${1:-""}";
        ffmpeg_out_ext="${2:-"mp4"}";
        
        if [ "$_src" = '' ]
        then
            _src="${PSP_params['src']:-""}";
            if [ "$_src" = '' ]
            then
                _src="$ffmpeg_in_pathname";
                if [ "$_src" = '' ]
                then
                    erreurs-pile-add-echo "${FUNCNAME[0]}(): Aucune source de définit.";
                    fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
                fi
            fi
        fi
        #display-vars $LINENO'_src' "$_src";


        if [ -d "$_src" ]
        then
            titre4 "$_src";
            ffmpeg_infos_show "$_src";
            #display-vars 'LINENO' "$LINENO";
            if $isTrapCAsk;then fct-pile-app-out $E_CTRL_C $_fct_pile_app_level; return $E_CTRL_C; fi

            for _ffmpeg_pathname in $_src/*
            do
                #display-vars 'LINENO' "$LINENO";

                if $isTrapCAsk;then return $E_CTRL_C;fi
                if [ -d "$_ffmpeg_pathname" ] && [ $recursifLevel -ne 0 ]
                then
                    #ffmpeg_in_pathname "$_ffmpeg_pathname";
                    #display-vars 'LINENO' "$LINENO";
                    ffmpeg_changeContainerWeb "$_ffmpeg_pathname";
                elif [ -f "$_ffmpeg_pathname" ]
                then
                    #display-vars 'LINENO' "$LINENO";
                    ffmpeg_in_pathname "$_ffmpeg_pathname";
                    ffmpeg_changeContainerWeb-code "$ffmpeg_out_ext";
                fi
            done
        elif [ -L "$_src" ]  #lien
        then
            erreurs-pile-add-echo "Les liens ne sont pas convertis ($_src)";
            fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;

        elif [ -f "$_src" ]
        then
            #display-vars 'LINENO' "$LINENO";
            ffmpeg-in-pathname "$_src";
            ffmpeg-changeContainerWeb-code "$ffmpeg_out_ext";
        fi
        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }


    # ffmpeg-changeContainerWeb-code( ffmpeg_out_ext )
    # Change le container de ffmpeg_infos_pathname en mp4 si le codec video (et la definition) sont compatibles
    function  ffmpeg-changeContainerWeb-code(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        titre2 "${FUNCNAME[0]}()";
        ffmpeg_out_ext="${1:-"mp4"}";
        local _sortiePathname="";
        local _isChangeOk=false;
        local _isCodecVideoOk=false;
        local _isCodecAudioOk=false
        local _isDefinitionOk=false;

        # exclure les formats video deja compatible
        if [ "$ffmpeg_in_ext" = 'ogg' ]\
        || [ "$ffmpeg_in_ext" = 'ogv' ]\
        || [ "$ffmpeg_in_ext" = 'oga' ]
        then
            notifs-pile-add-echo "${FUNCNAME[0]}: $ffmpeg_in_pathname: Format vidéo '$ffmpeg_in_ext' déjà compatible.";
            fct-pile-app-out 0 $_fct_pile_app_level;return 0;
        fi

        # - Les codecs videos compatibles - #
        case "$ffmpeg_infos_cv" in
            'vp9')  ffmpeg_out_ext='mp4';_isCodecVideoOk=true; ;;
            'h264')  ffmpeg_out_ext='mp4';_isCodecVideoOk=true; ;;
            'mjpeg') ffmpeg_out_ext='mp4';_isCodecVideoOk=true; ;;
            'ogm')   ffmpeg_out_ext='mp4';_isCodecVideoOk=true; ;;
            'ogv')   ffmpeg_out_ext='mp4';_isCodecVideoOk=true; ;;
        esac

        # - Les codecs audios compatibles - #
        case "$ffmpeg_infos_ca" in
            'aac')  ffmpeg_out_ext='mp4';_isCodecAudioOk=true; ;;
            'mp3')  ffmpeg_out_ext='mp4';_isCodecAudioOk=true; ;;
            'ogg')  ffmpeg_out_ext='mp4';_isCodecAudioOk=true; ;;
        esac

        # - Les definition compatibles - #
        local -i _y=$ffmpeg_infos_definitionY;

        _isDefinitionOk=true;
        #if [ $_y -lt 720 ]
        #then
        #    _isDefinitionOk=true;
        #fi

        if [ $_isCodecVideoOk = true ] && [ $_isCodecAudioOk = true ] && [ $_isDefinitionOk = true ]
        then
            _isChangeOk=true;
        fi
        #display-vars '_isChangeOk' "$_isChangeOk" '_isCodecVideoOk' "$_isCodecVideoOk" '_isDefinitionOk' "$_isDefinitionOk";

        if ! $_isChangeOk
        then
            if ! $_isCodecVideoOk; then erreurs-pile-add-echo "${FUNCNAME[0]}: $ffmpeg_in_pathname: format d'entrée '$ffmpeg_infos_cv' incompatible avec le web ";fi
            if ! $_isCodecAudioOk; then erreurs-pile-add-echo "${FUNCNAME[0]}: $ffmpeg_in_pathname: format d'entrée '$ffmpeg_infos_ca' incompatible avec le web ";fi
            if ! $_isDefinitionOk; then erreurs-pile-add-echo "${FUNCNAME[0]}: $ffmpeg_in_pathname: Definition hors cadre '$ffmpeg_infos_definition'";fi
            fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
        fi

        ffmpeg-changeContainer "$ffmpeg_out_ext";

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }


    #function ffmpeg_changeContainer( ffmpeg_out_ext)
    addLibrairie 'ffmpeg-changeContainer' "ffmpeg_changeContainer [_src] [ffmpeg_out_ext]: Change le container, du fichier ou des fichiers du ss rep, pour l'extention 'ffmpeg_out_ext'";
    function      ffmpeg-changeContainer(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        titre2 "${FUNCNAME[0]}()";
        ffmpeg_out_ext="${1:-"mp4"}";
        local _sortiePathname="";

        if [ -L "$ffmpeg_in_pathname" ]  #lien
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}($@): Les liens ne sont pas convertis";
            fct-pile-app-out $E_INODE_NOT_EXIST $_fct_pile_app_level; return $E_INODE_NOT_EXIST;
        fi

        if [ ! -f "$ffmpeg_in_pathname" ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}($@): fichier introuvable";
            fct-pile-app-out $E_INODE_NOT_EXIST $_fct_pile_app_level; return $E_INODE_NOT_EXIST;
        fi

        if ! cd "$ffmpeg_in_rep"
        then
            erreurs-pile-add-echo "Erreur lors de l'entrée dans le repertoire '$ffmpeg_in_rep";
            fct-pile-app-out $E_INODE_NOT_EXIST $_fct_pile_app_level; return $E_INODE_NOT_EXIST;
        fi

        #ffmpeg_in_display-vars;
        _sortiePathname="${ffmpeg_in_nom}.${ffmpeg_out_ext}";

        echo "############################" >> "$ffmpeg_log_pathname";
        echo "$ffmpeg_in_pathname"          >> "$ffmpeg_log_pathname";
        ffmpeg-infos-show "$ffmpeg_in_pathname";

        # - Recherche des parametre contextuels - #
        local _ffmpeg_params='';    # n'est pas lié à la variable globale
        if [ "$ffmpeg_out_ext" = 'mp4' ];then _ffmpeg_params=' -movflags +faststart';fi 

        # - go - #
        titreInfo "tail -f --lines=200 $ffmpeg_log_pathname";
        if [ -f "$_sortiePathname" ]
        then
            notifs-pile-add-echo "${FUNCNAME[0]}(): La destination existe déjà ($_sortiePathname)";
        else
            if $isSimulation
            then eval-echo "ffmpeg -n -hide_banner -i \"$ffmpeg_in_pathname\"$_ffmpeg_params -c:v copy -c:a copy \"$_sortiePathname\" 2>> \"$ffmpeg_log_pathname\"";
            else eval-echo "ffmpeg -n -hide_banner -i \"$ffmpeg_in_pathname\"$_ffmpeg_params -c:v copy -c:a copy \"$_sortiePathname\" 2>> \"$ffmpeg_log_pathname\"";
            fi
        fi
        lastErrNu=$?;

        ffmpeg-infos-show "$_sortiePathname";

        if [ $lastErrNu -ne 0 ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}(): $ffmpeg_in_pathname: ffmpeg erreur: $lastErrNu";
            fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
        fi

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }
#


return 0;
