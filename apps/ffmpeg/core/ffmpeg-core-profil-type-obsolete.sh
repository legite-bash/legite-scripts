echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh --show-libsLevel2 apps/ffmpeg/ffmpeg.sh lib
# date de création    : 2024.08.13
# date de modification: 2024.09.04


####################
# PROFILS COMPLETS #
####################
    function ffmpeg-profil-load-change_container_to_mp4(){
        local -i _fct_pile_app_level=1;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        display-vars 'recursif_find1_pathname' "$recursif_find1_pathname";

        fichier_setPathname "$recursif_find1_pathname"; 
        #fichier-displayVars;
        ffmpeg-ffmpeg-profil-init 'vcopy-acopy-scopy';
        ffmpeg-ffmpeg-profil-global-raw-add '-hide_banner -n  -fflags +genpts';
        ffmpeg_profil_input-raw-add  "-i \"../covert.webp\"";
        ffmpeg-ffmpeg-profil-params-add    faststart ; 
        ffmpeg-ffmpeg-profil-params-add    metadatas-del;
        ffmpeg-ffmpeg-profil-params-add    metadatas;
        ffmpeg-ffmpeg-profil-params-add    '-map 0:0 -map 0:1 -map 0:2' vcopy acopy 
        ffmpeg-ffmpeg-profil-params-add    '-map 0:2' scopy '-disposition:s:0 default' '-metadata:s:a:0 language=eng'
        ffmpeg-ffmpeg-profil-params-add    '-map 1:0' covert;
        local _suf="${ffmpeg_profils_suf}";
        ffmpeg_exec_profil "$fichier_pathname" "$ffmpeg_profil_nom" "${fichier_nom}${_suf}.mp4" ;

        fct-pile-app-out 0 $_fct_pile_app_level;return 0;
    }

    function ffmpeg-profil-load-crop(){
        local -i _fct_pile_app_level=1;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        fichier_pathname="$1";
        fct-pile-app_in;
        display-vars 'recursif_find1_pathname' "$recursif_find1_pathname";

        fichier_setPathname "$recursif_find1_pathname"; 
        #fichier-display-vars;
        ffmpeg-ffmpeg-profil-init 'vcopy-aac-ar44100';
        ffmpeg-ffmpeg-profil-global-raw-add '-hide_banner -n -ss 00:01:00 -to 00:02:00';
        ffmpeg_profil_input-raw-add  "-i \"./covert.webp\"";
        ffmpeg-ffmpeg-profil-params-add    faststart ; 
        ffmpeg-ffmpeg-profil-params-add    metadatas-del;
        ffmpeg-ffmpeg-profil-params-add    metadatas;
        #ffmpeg-ffmpeg-profil-params-add    "-metadata language=\"fra\"";

        ffmpeg-ffmpeg-profil-params-add    '-map 0:0' x264-crf-preset '-vf scale=640:-2' '-metadata:s:a:0 language=eng'
        ffmpeg-ffmpeg-profil-params-add    '-map 0:1' aac ar44100     '-metadata:s:a:0 language=eng'
        ffmpeg-ffmpeg-profil-params-add    '-map 0:2' scopy           '-metadata:s:s:0 language=fra'
        #ffmpeg-ffmpeg-profil-params-add    '-map 0:2' "-metadata:s:a language=\"fra\"";
        ffmpeg-ffmpeg-profil-params-add    '-map 1:0' covert;
        local _suf="${ffmpeg_profils_suf}";
        ffmpeg_exec_profil "$fichier_pathname" "$ffmpeg_profil_nom" "${fichier_nom}${_suf}.mp4" ;

        fct-pile-app-out 0 $_fct_pile_app_level;return 0;
    }


    function ffmpeg-profil-load-x264-scale640-aac44100-scopy-covert(){
        local -i _fct_pile_app_level=1;
        fct-pile-app-in "$*" $_fct_pile_app_level;


        fct-pile-app_in;
        display-vars 'recursif_find1_pathname' "$recursif_find1_pathname";

        fichier_setPathname "$recursif_find1_pathname"; 
        #fichier-display-vars;
        ffmpeg-ffmpeg-profil-init 'vcopy-aac-ar44100';
        #ffmpeg-ffmpeg-profil-global-raw-add '-hide_banner -n -ss 00:01:00 -to 00:02:00';
        ffmpeg_profil_input-raw-add  "-i \"./covert.webp\"";
        ffmpeg-ffmpeg-profil-params-add    faststart ; 
        ffmpeg-ffmpeg-profil-params-add    metadatas-del;
        ffmpeg-ffmpeg-profil-params-add    metadatas;
        #ffmpeg-ffmpeg-profil-params-add    "-metadata language=\"fra\"";

        ffmpeg-ffmpeg-profil-params-add    '-map 0:0' x264-crf-preset '-vf scale=640:-2' '-metadata:s:a:0 language=eng'
        ffmpeg-ffmpeg-profil-params-add    '-map 0:1' aac ar44100     '-metadata:s:a:0 language=eng'
        ffmpeg-ffmpeg-profil-params-add    '-map 0:2' scopy           '-metadata:s:s:0 language=fra'
        #ffmpeg-ffmpeg-profil-params-add    '-map 0:2' "-metadata:s:a language=\"fra\"";
        ffmpeg-ffmpeg-profil-params-add    '-map 1:0' covert;
        local _suf="${ffmpeg_profils_suf}";
        ffmpeg_exec_profil "$fichier_pathname" "$ffmpeg_profil_nom" "${fichier_nom}${_suf}.mp4" ;

        fct-pile-app-out 0 $_fct_pile_app_level;return 0;
    }


    function ffmpeg-profil-load-vcopy-aac-ar4410-covert(){
        local -i _fct_pile_app_level=1;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        display-vars 'recursif_find1_pathname' "$recursif_find1_pathname";

        fichier_setPathname "$recursif_find1_pathname"; 
        #fichier-display-vars;
        ffmpeg-ffmpeg-profil-init 'vcopy-aac-ar44100';
        ffmpeg-ffmpeg-profil-global-raw-add '-hide_banner -n  -fflags +genpts';
        ffmpeg_profil_input-raw-add  "-i \"./covert.webp\"";
        ffmpeg-ffmpeg-profil-params-add    faststart ; 
        ffmpeg-ffmpeg-profil-params-add    metadatas-del;
        ffmpeg-ffmpeg-profil-params-add    metadatas;
        ffmpeg-ffmpeg-profil-params-add    "-metadata language=\"fra\"";

        ffmpeg-ffmpeg-profil-params-add    '-map 0:0' vcopy
        ffmpeg-ffmpeg-profil-params-add    '-map 0:1' aac ar44100
        #ffmpeg-ffmpeg-profil-params-add    '-map 0:2' "-metadata:s:a language=\"fra\"";
        ffmpeg-ffmpeg-profil-params-add    '-map 1:0' covert;
        local _suf="${ffmpeg_profils_suf}";
        ffmpeg_exec_profil "$fichier_pathname" "$ffmpeg_profil_nom" "${fichier_nom}${_suf}.mp4" ;

        fct-pile-app-out 0 $_fct_pile_app_level;return 0;
    }

    function ffmpeg-profil-load-x264-aac-ar4410-ss_covert(){
        local -i _fct_pile_app_level=1;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        display-vars 'recursif_find1_pathname' "$recursif_find1_pathname";

        fichier_setPathname "$recursif_find1_pathname"; 
        #fichier-display-vars;
        ffmpeg-ffmpeg-profil-init 'x264-crf-preset-aac-ar44100';
        ffmpeg-ffmpeg-profil-global-raw-add '-hide_banner -n';
        ffmpeg_profil_input-raw-add  "-i \"../covert.webp\"";
        ffmpeg-ffmpeg-profil-params-add    faststart ; 
        ffmpeg-ffmpeg-profil-params-add    metadatas-del;
        ffmpeg-ffmpeg-profil-params-add    metadatas;
        ffmpeg-ffmpeg-profil-params-add    "-metadata language=\"fra\"";

        ffmpeg-ffmpeg-profil-params-add    '-map 0:0' x264-crf-preset
        ffmpeg-ffmpeg-profil-params-add    '-map 0:1' aac ar44100
        #ffmpeg-ffmpeg-profil-params-add    '-map 0:2' "-metadata:s:a language=\"fra\"";
        ffmpeg-ffmpeg-profil-params-add    '-map 1:0' covert;
        local _suf="${ffmpeg_profils_suf}";
        ffmpeg_exec_profil "$fichier_pathname" "$ffmpeg_profil_nom" "${fichier_nom}${_suf}.mp4" ;

        fct-pile-app-out 0 $_fct_pile_app_level;return 0;
    }

    function ffmpeg-profil-load-x264-aac-ss_covert(){
        local -i _fct_pile_app_level=1;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        display-vars 'recursif_find1_pathname' "$recursif_find1_pathname";

        fichier_setPathname "$recursif_find1_pathname"; 
        #fichier-display-vars;
        ffmpeg-ffmpeg-profil-init 'x264-crf-preset-aac-ar44100';
        ffmpeg-ffmpeg-profil-global-raw-add '-hide_banner -n';
        ffmpeg_profil_input-raw-add  "-i \"../covert.webp\"";
        ffmpeg-ffmpeg-profil-params-add    faststart ; 
        ffmpeg-ffmpeg-profil-params-add    metadatas-del;
        ffmpeg-ffmpeg-profil-params-add    metadatas;
        ffmpeg-ffmpeg-profil-params-add    "-metadata language=\"fra\"";

        ffmpeg-ffmpeg-profil-params-add    '-map 0:0' x264-crf-preset
        ffmpeg-ffmpeg-profil-params-add    '-map 0:1' aac
        #ffmpeg-ffmpeg-profil-params-add    '-map 0:2' "-metadata:s:a language=\"fra\"";
        ffmpeg-ffmpeg-profil-params-add    '-map 1:0' covert;
        local _suf="${ffmpeg_profils_suf}";
        ffmpeg_exec_profil "$fichier_pathname" "$ffmpeg_profil_nom" "${fichier_nom}${_suf}.mp4" ;

        fct-pile-app-out 0 $_fct_pile_app_level;return 0;
    }
#

return 0;