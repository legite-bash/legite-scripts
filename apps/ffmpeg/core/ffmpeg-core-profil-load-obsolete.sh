echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh --show-libsLevel2 apps/ffmpeg/ffmpeg.sh lib
# date de création    : 2024.08.13
# date de modification: 2024.10.04: obsolete: remplacé par mappage


############################
# CHARGE UN PROFIL COMPLET #
############################
    # ffmpeg-profil-load-defaut ( pathname )
    # Utilise ffmpeg-in-*/ffmpeg-out-*
    function ffmpeg-profil-load-defaut(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _in_pathname="${1:-""}";
        if [ -z "$_in_pathname" ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}: Aucun fichier entrant founis":
            fct-pile-gtt-out $E_ARG_REQUIRE $_pile_fct_gtt_level; return $E_ARG_REQUIRE;
        fi
    

        ffmpeg-in-pathname "$_in_pathname"; 
        #fichier-display-vars;
        ffmpeg-init;
        ffmpeg-params-global-add '-hide_banner';
        ffmpeg-params-add faststart; 

        fct-pile-app-out 0 $_fct_pile_app_level;return 0;
    }


    # ffmpeg-profil-load-container_to_mp4 ( pathname )
    # Utilise ffmpeg-in-*/ffmpeg-out-*
    function ffmpeg-profil-load-container-to-mp4(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _in_pathname="${1:-""}";
        if [ -z "$_in_pathname" ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}: Aucun fichier entrant founis":
            fct-pile-gtt-out $E_ARG_REQUIRE $_pile_fct_gtt_level; return $E_ARG_REQUIRE;
        fi
    

        ffmpeg-in-pathname "$_in_pathname"; 
        ffmpeg-out-ext "mp4"; 

        ffmpeg-init;
        ffmpeg-params-global-y-enable
        ffmpeg-params-global-add '-hide_banner  -fflags +genpts';
        ffmpeg-params-add faststart vcopy acopy; 


        fct-pile-app-out 0 $_fct_pile_app_level;return 0;
    }


    # ffmpeg-profil-load-container_to_mkv ( pathname )
    # Utilise ffmpeg-in-*/ffmpeg-out-*
    function ffmpeg-profil-load-container-to-mkv(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _in_pathname="${1:-""}";
        if [ -z "$_in_pathname" ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}: Aucun fichier entrant founis":
            fct-pile-gtt-out $E_ARG_REQUIRE $_pile_fct_gtt_level; return $E_ARG_REQUIRE;
        fi
    

        ffmpeg-in-pathname "$_in_pathname"; 
        ffmpeg-out-ext "mkv"; 

        ffmpeg-init;
        ffmpeg-params-global-add '-hide_banner -n -fflags +genpts';
        ffmpeg-params-add faststart vcopy acopy; 


        fct-pile-app-out 0 $_fct_pile_app_level;return 0;
    }



    function ffmpeg-profil-load-crop(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        _in_pathname="$1";
        fct-pile-app_in;
        display-vars 'recursif_find1_pathname' "$recursif_find1_pathname";

        fichier_setPathname "$recursif_find1_pathname"; 
        #fichier-display-vars;
        ffmpeg-init 'vcopy-aac-ar44100';
        ffmpeg-params-global-add '-hide_banner -n -ss 00:01:00 -to 00:02:00';
        ffmpeg_profil_input-add  "-i \"./covert.webp\"";
        ffmpeg-params-add    faststart ; 
        ffmpeg-params-add    metadatas-del;
        ffmpeg-params-add    metadatas;
        #ffmpeg-params-add    "-metadata language=\"fra\"";

        ffmpeg-params-add    '-map 0:0' x264-crf-preset '-vf scale=640:-2' '-metadata:s:a:0 language=eng'
        ffmpeg-params-add    '-map 0:1' aac ar44100     '-metadata:s:a:0 language=eng'
        ffmpeg-params-add    '-map 0:2' scopy           '-metadata:s:s:0 language=fra'
        #ffmpeg-params-add    '-map 0:2' "-metadata:s:a language=\"fra\"";
        ffmpeg-params-add    '-map 1:0' covert;
        local _suf="${ffmpeg_profils_suf}";
        ffmpeg_exec_profil "$_in_pathname" "$ffmpeg_profil_nom" "${fichier_nom}${_suf}.mp4" ;

        fct-pile-app-out 0 $_fct_pile_app_level;return 0;
    }


    function ffmpeg-profil-load-x264-scale640-aac44100-scopy-covert(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;


        fct-pile-app_in;
        display-vars 'recursif_find1_pathname' "$recursif_find1_pathname";

        fichier_setPathname "$recursif_find1_pathname"; 
        #fichier-display-vars;
        ffmpeg-init 'vcopy-aac-ar44100';
        #ffmpeg-params-global-add '-hide_banner -n -ss 00:01:00 -to 00:02:00';
        ffmpeg_profil_input-add  "-i \"./covert.webp\"";
        ffmpeg-params-add    faststart ; 
        ffmpeg-params-add    metadatas-del;
        ffmpeg-params-add    metadatas;
        #ffmpeg-params-add    "-metadata language=\"fra\"";

        ffmpeg-params-add    '-map 0:0' x264-crf-preset '-vf scale=640:-2' '-metadata:s:a:0 language=eng'
        ffmpeg-params-add    '-map 0:1' aac ar44100     '-metadata:s:a:0 language=eng'
        ffmpeg-params-add    '-map 0:2' scopy           '-metadata:s:s:0 language=fra'
        #ffmpeg-params-add    '-map 0:2' "-metadata:s:a language=\"fra\"";
        ffmpeg-params-add    '-map 1:0' covert;
        local _suf="${ffmpeg_profils_suf}";
        ffmpeg_exec_profil "$_in_pathname" "$ffmpeg_profil_nom" "${fichier_nom}${_suf}.mp4" ;

        fct-pile-app-out 0 $_fct_pile_app_level;return 0;
    }


    function ffmpeg-profil-load-vcopy-aac-ar4410-covert(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        display-vars 'recursif_find1_pathname' "$recursif_find1_pathname";

        fichier_setPathname "$recursif_find1_pathname"; 
        #fichier-display-vars;
        ffmpeg-init 'vcopy-aac-ar44100';
        ffmpeg-params-global-add '-hide_banner -n  -fflags +genpts';
        ffmpeg_profil_input-add  "-i \"./covert.webp\"";
        ffmpeg-params-add    faststart ; 
        ffmpeg-params-add    metadatas-del;
        ffmpeg-params-add    metadatas;
        ffmpeg-params-add    "-metadata language=\"fra\"";

        ffmpeg-params-add    '-map 0:0' vcopy
        ffmpeg-params-add    '-map 0:1' aac ar44100
        #ffmpeg-params-add    '-map 0:2' "-metadata:s:a language=\"fra\"";
        ffmpeg-params-add    '-map 1:0' covert;
        local _suf="${ffmpeg_profils_suf}";
        ffmpeg_exec_profil "$_in_pathname" "$ffmpeg_profil_nom" "${fichier_nom}${_suf}.mp4" ;

        fct-pile-app-out 0 $_fct_pile_app_level;return 0;
    }

    function ffmpeg-profil-load-x264-aac-ar4410-ss_covert(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        display-vars 'recursif_find1_pathname' "$recursif_find1_pathname";

        fichier_setPathname "$recursif_find1_pathname"; 
        #fichier-display-vars;
        ffmpeg-init 'x264-crf-preset-aac-ar44100';
        ffmpeg-params-global-add '-hide_banner -n';
        ffmpeg_profil_input-add  "-i \"../covert.webp\"";
        ffmpeg-params-add    faststart ; 
        ffmpeg-params-add    metadatas-del;
        ffmpeg-params-add    metadatas;
        ffmpeg-params-add    "-metadata language=\"fra\"";

        ffmpeg-params-add    '-map 0:0' x264-crf-preset
        ffmpeg-params-add    '-map 0:1' aac ar44100
        #ffmpeg-params-add    '-map 0:2' "-metadata:s:a language=\"fra\"";
        ffmpeg-params-add    '-map 1:0' covert;
        local _suf="${ffmpeg_profils_suf}";
        ffmpeg_exec_profil "$_in_pathname" "$ffmpeg_profil_nom" "${fichier_nom}${_suf}.mp4" ;

        fct-pile-app-out 0 $_fct_pile_app_level;return 0;
    }

    function ffmpeg-profil-load-x264-aac-ss_covert(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        display-vars 'recursif_find1_pathname' "$recursif_find1_pathname";

        fichier_setPathname "$recursif_find1_pathname"; 
        #fichier-display-vars;
        ffmpeg-init 'x264-crf-preset-aac-ar44100';
        ffmpeg-params-global-add '-hide_banner -n';
        ffmpeg_profil_input-add  "-i \"../covert.webp\"";
        ffmpeg-params-add    faststart ; 
        ffmpeg-params-add    metadatas-del;
        ffmpeg-params-add    metadatas;
        ffmpeg-params-add    "-metadata language=\"fra\"";

        ffmpeg-params-add    '-map 0:0' x264-crf-preset
        ffmpeg-params-add    '-map 0:1' aac
        #ffmpeg-params-add    '-map 0:2' "-metadata:s:a language=\"fra\"";
        ffmpeg-params-add    '-map 1:0' covert;
        local _suf="${ffmpeg_profils_suf}";
        ffmpeg_exec_profil "$_in_pathname" "$ffmpeg_profil_nom" "${fichier_nom}${_suf}.mp4" ;

        fct-pile-app-out 0 $_fct_pile_app_level;return 0;
    }
#

return 0;