echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh --show-vars ffmpeg
# date de création    : 202?
# date de modification: 2024.08.29


declare -g   ffmpeg_isValideContainer=false;        # resultat de la fonction ffmpeg-isValidCOntainer
declare -ga  ffmpeg_valideContainers=( mp4 mov avi ogm mkv ogm ogg wav mp2 mp3 mp4 m4a mpg mpeg divx h264 h265 opus aac);
declare -ga  ffmpeg_preset=( ultrafast superfast veryfast faster fast medium slow slower veryslow placebo );
#declare -g  ffmpeg_isValideMedia=false;
#declare -g  ffmpeg_isValideVideo=false;
#declare -g  ffmpeg_isValideAudio=false;
#declare -ga ffmpeg_valideVideos=( mp4 mov avi ogm mkv);
#declare -ga ffmpeg_valideAudios=( mp2 mp3 mp4 ogg wav);

# - objet: ffmpeg_params_* - #
# alimentés par les ffmpeg_filtres (ffmpeg-filtres.sh #
declare -g   ffmpeg_params_all='';                  # variable calculée dans ffmpeg-exec()

declare -g   ffmpeg_params_global='';
declare -g   ffmpeg_params_global_y='';
declare -g   ffmpeg_params_global_ss='';
declare -g   ffmpeg_params_global_to='';

declare -g   ffmpeg_params_input='';                # fichier d'entrées de ffmpeg (eg,: ' -i fichier1 -i fichier2')

declare -g   ffmpeg_params='';

declare -g   ffmpeg_params_suf='';

declare -g   ffmpeg_params_crf='';                  # string
declare -g   ffmpeg_params_preset='';
declare -g   ffmpeg_params_tune='';



#function ffmpeg-init(  )
function ffmpeg-init(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    ffmpeg_params_nom="${1:-""}";
    ffmpeg_params_global='';  
    ffmpeg-params-global-n-enable;

    ffmpeg_params_input='';
    ffmpeg_params_params='';
    ffmpeg_params_suf='';    # ne pas mettre d'espace

    ffmpeg_params_crop_WHXY='0:0:0:0';
    ffmpeg-params-crf        18;
    ffmpeg-params-preset    'veryslow';
    ffmpeg-params-tune      'film';

    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}


function ffmpeg-display-vars(){
    local -i _fct_pile_app_level=5;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    #display-vars 'FFMPEG_PROFIL_NOM_DEFAUT   ' "$FFMPEG_PROFIL_NOM_DEFAUT";
    display-vars 'ffmpeg_params_all      ' "$ffmpeg_params_all";

    display-vars 'ffmpeg_params_nom      ' "$ffmpeg_params_nom";
    display-vars 'ffmpeg_params_global   ' "${ffmpeg_params_global}";
    display-vars 'ffmpeg_params_global_y ' "${ffmpeg_params_global_y}";

    display-vars 'ffmpeg_params_input    ' "${ffmpeg_params_input}";
    display-vars 'ffmpeg_params          ' "${ffmpeg_params}";
    display-vars 'ffmpeg_params_suf      ' "${ffmpeg_params_suf}";

    display-vars 'ffmpeg_params_crop_WHXY' "$ffmpeg_params_crop_WHXY";

    display-vars 'ffmpeg_params_crf      ' "${ffmpeg_params_crf}";
    display-vars 'ffmpeg_params_preset   ' "${ffmpeg_params_preset}";
    display-vars 'ffmpeg_params_tune     ' "${ffmpeg_params_tune}";

    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}

# - Global - # 

function ffmpeg-params-suf-add(){
    if [ $# -eq 0 ];then return $E_ARG_REQUIRE;fi
    if [ -z "$1"  ];then return $E_ARG_BAD;fi
    ffmpeg_params_suf+="$1";
    return 0;
}


function ffmpeg-params-all-add(){
    local _texte="${1:-""}";
    if [ -n  "$_texte" ];then  ffmpeg_params_all+=" $_texte"; fi
}

# Ajout des espace à la fin de APPEND pour que la chaine atteigne le bord droit du terminal
function ffmpeg-params-all-add-line(){
    local _APPEND_TXT="${1:-""}";
    if [ -z "$_APPEND_TXT" ]; then return $E_FALSE; fi
    texte-justify-left "$_APPEND_TXT" ' ';
    ffmpeg_params_all+="$texte_justify_left";
}


####################################
# les paramestres de ffmpeg global #
####################################
    function ffmpeg-params-global-y-disable(){ ffmpeg_params_global_y='';  }
    function ffmpeg-params-global-y-enable() { ffmpeg_params_global_y=' -y';}
    function ffmpeg-params-global-n-enable() { ffmpeg_params_global_y=' -n';}

    function ffmpeg-params-global-add-before(){
        local -i _fct_pile_app_level=3;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _param_txt="${1:-""}";
        if [ -n "$_param_txt" ]; then ffmpeg_params_global="$_param_txt $ffmpeg_params_global"; fi

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }

    function ffmpeg-params-global-add(){
        local -i _fct_pile_app_level=3;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _param_txt="${1:-""}";
        if [ -n "$_param_txt" ]; then ffmpeg_params_global+=" $_param_txt"; fi
        #display-vars 'ffmpeg_params_global' "$ffmpeg_params_global";
        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }


    function ffmpeg-params-global-ssto(){
        local -i _fct_pile_app_level=3;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _param_txt="${1:-""}";
        if [ -n "$_param_txt" ]; then ffmpeg-params-global-ss "$_param_txt"; fi

        local _param_txt="${2:-""}";
        if [ -n "$_param_txt" ]; then ffmpeg-params-global-to "$_param_txt"; fi


        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }

    function ffmpeg-params-global-ss(){
        local -i _fct_pile_app_level=3;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _param_txt="${1:-""}";
        if [ -z "$_param_txt" ]
        then ffmpeg_params_global_ss="";
        else ffmpeg_params_global_ss=" -ss $_param_txt";
        fi
        display-vars 'ffmpeg_params_global_ss' "$ffmpeg_params_global_ss";
        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }

    function ffmpeg-params-global-to(){
        local -i _fct_pile_app_level=3;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _param_txt="${1:-""}";
        if [ -z "$_param_txt" ]
        then ffmpeg_params_global_to="";
        else ffmpeg_params_global_to=" -to $_param_txt";
        fi
        display-vars 'ffmpeg_params_global_to' "$ffmpeg_params_global_to";
        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }

    # - calc - # 
    function ffmpeg-params-global-calc(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        ffmpeg_params_global=' -hide_banner';

        if [ -n "$ffmpeg_params_global_y" ];  then ffmpeg_params_global+="$ffmpeg_params_global_y"; fi
        if [ -n "$ffmpeg_params_global_ss" ]; then ffmpeg_params_global+="$ffmpeg_params_global_ss"; fi
        if [ -n "$ffmpeg_params_global_to" ]; then ffmpeg_params_global+="$ffmpeg_params_global_to"; fi

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }





#


###################################
# les paramestres de ffmpeg input #
###################################

    # Ajoute un input '-i $_pathname ' en debut de ffmpeg_params_input
    # ffmpeg-params-input-add-before  _pathname 
    function ffmpeg-params-input-add-before(){
        local -i _fct_pile_app_level=4;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _pathname="${1:-""}";

        if [ -n "$_pathname" ]
        then
            ffmpeg_params_input="-i \"$_pathname\" $ffmpeg_params_input ";
        fi

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }


    # Ajoute un input '-i $_pathname ' en fin de ffmpeg_params_input
    # ffmpeg-params-input-add  _pathname 
    function ffmpeg-params-input-add(){
        local -i _fct_pile_app_level=4;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _pathname="${1:-""}";

        if [ ! -z "$_pathname" ]
        then
            ffmpeg_params_input="${ffmpeg_params_input} -i \"$_pathname\"";
        fi

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }
#


####################################
# les paramestres de ffmpeg params #
####################################
    function ffmpeg-params-add-before(){
        local -i _fct_pile_app_level=4;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _val="${1:-""}";
        if [ -z "$_val" ]; then return $E_ARG_BAD; fi
        if [ -z "$ffmpeg_params" ]
        then ffmpeg_params="$_val";
        else ffmpeg_params="$_val $ffmpeg_params";
        fi

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }

    # Ajoute dans $ffmpeg_params les parametres.
    # convertit en code si cela correspond à un parametre ffmpeg sinon ajoute en raw
    # ex: ffmpeg_params+='param1' 'param2' ' parametre libre'
    function ffmpeg-params-add(){
        local -i _fct_pile_app_level=4;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        if [ $# -eq 0 ]\
        || [ -z "$1" ]
        then
            fct-pile-app-out 0 $_fct_pile_app_level; return 0;
        fi

        for _params_add in "$@"
        do
                #display-vars $LINENO'_params_add' "$_params_add";

            case "$_params_add" in

                # ========== #
                # = divers = #
                # ========== #
                'extrait-05-06mn')          ffmpeg_params+=' -ss 00:05:00 -to 00:06:00';   ;;
                'extrait-00-06mn')          ffmpeg_params+=' -ss 00:00:00 -to 00:06:00';   ;;
                'extrait-00-10mn')          ffmpeg_params+=' -ss 00:00:00 -to 00:10:00';   ;;

                # =========== #
                # = threads = #
                # =========== #
                'threads4' )                ffmpeg_params+=' -threads 4';      ;;
                'threads8' )                ffmpeg_params+=' -threads 8';      ;;
                'threads16')                ffmpeg_params+=' -threads 16';     ;;

                'filter_threads4' )         ffmpeg_params+=' -filter_threads 4';      ;;
                'filter_threads8' )         ffmpeg_params+=' -filter_threads 8';      ;;
                'filter_threads10')         ffmpeg_params+=' -filter_threads 10';     ;;


                'filter_complex_threads4' ) ffmpeg_params+=' -filter_complex_threads 4';      ;;
                'filter_complex_threads8' ) ffmpeg_params+=' -filter_complex_threads 8';      ;;
                'filter_complex_threads10') ffmpeg_params+=' -filter_complex_threads 10';     ;;


                # ============= #
                # = container = #
                # ============= #
                # https://www.ionos.fr/digitalguide/sites-internet/web-design/les-formats-de-fichiers-video/

                # container mov
                'faststart')
                    ffmpeg_params+=' -movflags +faststart';     ffmpeg-params-suf-add '-faststart';
                ;;

                'map_00_01')
                    ffmpeg_params+=' -map 0:0';                 ffmpeg-params-suf-add '-map00';
                    ffmpeg_params+=' -map 0:1';                 ffmpeg-params-suf-add '-map01';
                    ;;


                # ============= #
                # = metadatas = #
                # ============= #
                # https://medium.com/av-transcode/how-to-add-multiple-audio-tracks-to-a-single-video-using-ffmpeg-open-source-tool-27bff8cca30
                # language:
                    # FFmpeg expects the language can be specified as an ISO 639–2/T or ISO 639–2/B (3 letters) code. ISO 639 is a set of international standards that lists shortcodes for language names.
                    # https://www.iso.org/fr/iso-639-language-codes.html
                    # https://www.iso.org/fr/standard/4767.html
                    # https://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-2


                # copier les metadatas sources
                # -movflags use_metadata_tags
                
                # mp4
                # https://superuser.com/questions/1208273/add-new-and-non-defined-metadata-to-a-mp4-file#1208277
                # 

                'metadatas-del')
                    ffmpeg_params+=' -map_metadata -1';  # supprimer tous les metadatas (sauf -encoder)
                    ;;


                #'-vn')  # désactive les sources vidéos          (Video None) ;;
                #'-an')  # désactive les sources audios          (Audio None) ;;
                #'-sn')  # désactive les sources de sous titre   (Subtitle None) ;;


                # ================= #
                # = filtres video = #
                # ================= #
                #'filter_v')
                #    ffmpeg_params+=" -filter:v \"$ffmpeg_profil_v\"";
                #    ;;

                'cropdetect')
                    # https://www.linuxuprising.com/2020/01/ffmpeg-how-to-crop-videos-with-examples.html
                    # https://video.stackexchange.com/questions/4563/how-can-i-crop-a-video-with-ffmpeg
                    # ffmpeg -i "$MEDIAW_ROOT/Films/Science_fiction/${FUNCNAME[0]}" -t 1 -vf cropdetect -f null - 2>&1 | awk '/crop/ { print $NF }' | tail -1
                    #-vf cropdetect
                    ffmpeg_params+=' -filter:v "cropdetect"';
                    ffmpeg-params-suf-add '-cropdetect';
                ;;

                'crop')
                    ffmpeg_params+=" -filter:v \"crop=$ffmpeg_params_crop_WHXY\"";
                    ffmpeg-params-suf-add '-crop';  # w:h:x:y
                    ;;

                #'scale')    #ratoi
                    # https://commons.wikimedia.org/wiki/File:Vector_Video_Standards.png
                    #https://trac.ffmpeg.org/wiki/Scaling
                    # table de concordance par ratio
                    # 16/9 :    :854x480,1280x720,1920x1080
                    # 19/10:    :320x200,1680x1050,1920x1200,2560x1600
                    #  3/2 :    :720x480,1152,768,1280x854,1440x960
                    #  4/3 :    :320x240,640x480,768x576,800x600,1024x768,1280x960
                    #  5/4 :    :1280x1024
                    
                    # - algorithm - #
                    # -sws_flags bicubic  ¶
                    #https://ffmpeg.org/ffmpeg-scaler.html


                    # '-vf scale=1024:-1';
                    # '-vf scale=1024:-2'; # Certains codecs ne supportent pas les images impaires
                #;;

                # =============== #
                # = codec video = #
                # =============== #
                
                # Pour corriger Timestamps are unset in a packet for stream 0.
                #-codec:v copy -codec:a copy# -fflags +genpts
                'vcopy')
                    ffmpeg_params+=' -codec:v copy -fflags +genpts';        ffmpeg-params-suf-add '-vcopy';
                    
                    ;;

                'r25')
                    ffmpeg_params+=' -r 25';                                ffmpeg-params-suf-add '-r25';
                    ;;


                # https://trac.ffmpeg.org/wiki/Encode/H.264
                'x264' | 'h264')
                    #local _crf=18;             # doit etre définit avant
                    #local _preset='veryslow';  # doit etre définit avant

                    # video
                    ffmpeg_params+=' -codec:v libx264'; 
                    # Ces parametres sont intégrés par ffmpeg-exec-code()
                    #ffmpeg_params+=" $ffmpeg_params_crf";
                    #ffmpeg_params+=" $ffmpeg_params_preset";
                    #ffmpeg_params+=" $ffmpeg_params_tune";
                    ffmpeg-params-suf-add "-libx264-${ffmpeg_params_crf}-${ffmpeg_params_preset}-${ffmpeg_params_tune}";
                    ;;

                'mpeg4_unpack_bframes')
                    ffmpeg_params+=" -bsf:v mpeg4_unpack_bframes";
                    ffmpeg-params-suf-add '-mpeg4_unpack_bframes';
                    ;;


                # =================== #
                # = generique audio = #
                # =================== #
                'ar44100') ffmpeg_params+=' -ar 44100';    ffmpeg-params-suf-add '-ar44100';      ;;
                'stereo')  ffmpeg_params+=' -ac 2';        ffmpeg-params-suf-add '-ac2';          ;;
                'mono')    ffmpeg_params+=' -ac 1';        ffmpeg-params-suf-add '-ac1';          ;;


                # ======================= #
                # = audio: channelsplit = #
                # ======================= #
                # https://trac.ffmpeg.org/wiki/AudioChannelManipulation
                # audio 5.1 → stereo: use -ac2 


                # =============== #
                # = codec audio = #
                # =============== #
                'acopy')   ffmpeg_params+=' -codec:a copy';ffmpeg-params-suf-add '-acopy';        ;;


                # aac
                # https://trac.ffmpeg.org/wiki/Encode/AAC
                # 1- utiliser la librairie tier Fraunhofer FDK AAC (meilleur qualité) : -codec:a libfdk_aac
                # neccessite les directives de compilation: --enable-libfdk-aac (and additionally --enable-nonfree if you're also using --enable-gpl
                # 2- utiliser la librairie native (de ffmpeg) aac (meilleur moindre que la précédente) :  -codec:a aac
                # -b:a 128k     Constant Bit Rate (CBR) mode:    64k par canal, donc 128k/s pour la stréréo
                # -vbr 1..5     Variable Bit Rate (VBR) mode:   1: mauvaise qualité, 5:meilleur qualité
                # 2.1- High-Efficiency AACHigh-Efficiency AAC (experimental)
                    #  HE-AAC version 1:
                    # ffmpeg -i input.wav -c:a libfdk_aac -profile:a aac_he -b:a 64k output.m4a
                    # HE-AAC version 2 (stéréo only): 
                    # ffmpeg -i input.wav -c:a libfdk_aac -profile:a aac_he_v2 -b:a 32k output.m4a
                # Progressive Download :-movflags +faststart   (positionne les informations de localisation au début du fichier)
                # -a:r 44100    Audio Rate in Hz
                'aac')     ffmpeg_params+=' -codec:a aac'; ffmpeg-params-suf-add '-aac';          ;;

                'libopus')
                    # http://ffmpeg.org/ffmpeg-codecs.html#libopus-1
                    ffmpeg_params+=' -codec:a libopus';
                    ffmpeg_params+=' -mapping_family 0';
                    #ffmpeg_params+=' -b 64k';
                    #ffmpeg_params+=' -vbr [off|on|constrained';
                    ;;


                # ============== #
                # = covert art = #
                # ============== #
                # http://ffmpeg.org/ffmpeg.html#Main-options '-disposition'
                # utiliser -disposition
                # ffmpeg -i in.mp4 -i IMAGE -map 0 -map 1 -c copy -c:v:1 png -disposition:v:1 attached_pic out.mp4
                covert)    ffmpeg_params+=" -disposition:v:1 attached_pic -codec:v:1 png"; ffmpeg-params-suf-add '-covert';     ;;


                # ============= #
                # = subtitles = #
                # ============= #
                # https://en.wikibooks.org/wiki/FFMPEG_An_Intermediate_Guide/subtitle_options
                # https://trac.ffmpeg.org/wiki/ExtractSubtitles
                # https://trac.ffmpeg.org/wiki/HowToBurnSubtitlesIntoVideo
                # https://doc.ubuntu-fr.org/sous-titrage
                # https://www.matroska.org/technical/subtitles.html

                sn)
                    ffmpeg_params+=" -sn";
                    ffmpeg-params-suf-add '-sn';
                    ;;
                scopy)
                    ffmpeg_params+=" -codec:s copy";
                    ffmpeg-params-suf-add '-scopy';
                    ;;


                #which is image based
                ## hdmv_pgs_subtitle: ffmpeg -i film.mkv -c copy -map 0:4 film.sup
                ## https://unix.stackexchange.com/questions/284005/convert-image-based-subtitle-to-text-based-subtitle-inside-mkv-file

                ## https://superuser.com/questions/1748962/how-can-i-extract-a-subtitle-in-hdmv-pgs-format-from-a-video-with-ffmpeg

            'RC')
                    ffmpeg_params+="\
        ";          # Retour chariot. Cette ligne est normale.
                    ;;
            #
            # Supprime les metadonnées et les sous titres
            'nettoyage')
                    ffmpeg-params-add vcopy acopy '-sn'  metadatas-del;
                    ffmpeg-params-suf-add '-nettoyage';
                    ;;

            *)  
                #display-vars $LINENO'_params_add' "$_params_add";
                ffmpeg_params+=" $_params_add";
                ;;
            esac
        done

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }
#


########################################
# les paramestres de ffmpeg params-PTC #
########################################

    function ffmpeg-params-preset-tune-crf-clear (){
        ffmpeg-params-preset '';
        ffmpeg-params-tune   '';
        ffmpeg-params-crf;
    }

    #function ffmpeg-params-tune ( valeur  )
    function ffmpeg-params-tune (){
        local  _val="${1:-""}";
        if [ -z "$_val" ]
        then ffmpeg_params_tune="";
        else ffmpeg_params_tune="-tune $_val";
        fi
        return 0;
    }

    #function ffmpeg-params-preset ( valeur )
    function ffmpeg-params-preset (){
        local  _val="${1:-""}";
        if [ -z "$_val" ]
        then ffmpeg_params_preset="";
        else ffmpeg_params_preset="-preset $_val";
        fi
        return 0;
    }

    #function ffmpeg-params-crf ( valeur )
    function ffmpeg-params-crf (){
        local _val="${1:-""}";
        if [ -z "$_val" ]
        then ffmpeg_params_crf="";
        else ffmpeg_params_crf="-crf $_val";
        fi
        return 0;
    }

# - objet: ffmpeg_*_pathname* - #
    # apps/ffmpeg/ffmpeg-core-intOut.sh

# - objet: ffmpeg_out_* - #
    # apps/ffmpeg/ffmpeg-core-intOut.sh


################
# ffmpeg-map-* #
################
declare -g ffmpeg_map_src='';
declare -g ffmpeg_map_dest='';

#function ffmpeg-map-dest-select( map_dest map_langue [map_title] )
function ffmpeg-map-dest-select() {
    if [ $# -lt 2 ]; then return $E_ARG_REQUIRE; fi

    ffmpeg_map_dest="$1";
    local _map_langue="$2";

    local _map_title="${3:-""}";
    local _map_title_str="";
    if [ -n "$_map_title" ]; then _map_title_str=" -metadata:s:$ffmpeg_map_dest title=\"$_map_title\""; fi

    ffmpeg-params-add "-metadata:s:$ffmpeg_map_dest language=$_map_langue" "$_map_title_str";
    return 0;
}

#function ffmpeg-map-add( map_src map_dest [map_langue] [map_title] [map_comment] )
function ffmpeg-map-add() {
    if [ $# -lt 3 ]; then return $E_ARG_REQUIRE; fi

    ffmpeg_map_src="$1";
    ffmpeg_map_dest="$2";

    local _map_langue="${3:-""}";
    local _map_langue_str="";
    if [ -n "$_map_langue" ]; then _map_langue_str="-metadata:s:$ffmpeg_map_dest language=\"$_map_langue\""; fi

    local _map_title="${4:-""}";
    local _map_title_str="";
    if [ -n "$_map_title" ]; then _map_title_str="-metadata:s:$ffmpeg_map_dest title=\"$_map_title\""; fi

    local _map_comment="${5:-""}";
    local _map_comment_str="";
    if [ -n "$_map_comment" ]; then _map_comment_str="-metadata:s:$ffmpeg_map_dest comment=\"$_map_comment\""; fi

    ffmpeg-params-add  "-map $ffmpeg_map_src" "$_map_langue_str" "$_map_title_str" "$_map_comment_str";
    return 0;
}


# function ffmpeg-map-add-codec( argument  )
function ffmpeg-map-add-codec() {
    local _val="${1:""}";
    if [ -z "$_val" ]; then return $E_ARG_REQUIRE; fi
    if [ -z "$ffmpeg_map_dest" ]; then return $E_ARG_BAD; fi

    ffmpeg-params-add  "-codec:$ffmpeg_map_dest $_val";
    return 0;
}

# Définit la fréquence d'échantillonnage de l'audio, c'est-à-dire le nombre d'échantillons par seconde.
# function ffmpeg-map-add-ar( argument  )
function ffmpeg-map-add-ar() {
    local _val="${1:""}";
    if [ -z "$_val" ]; then return $E_ARG_REQUIRE; fi
    if [ -z "$ffmpeg_map_dest" ]; then return $E_ARG_BAD; fi

    ffmpeg-params-add  "-ar:$ffmpeg_map_dest $_val";
    return 0;
}
