echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh --show-libsLevel2 ffmpeg lib
# date de création    : 2024.10.07
# date de modification: 2024.10.16


##################
# ffmpeg-mappage #
##################
declare -g ffmpeg_mappage='';
declare -g ffmpeg_mappage_v0_lang='eng';    declare -g ffmpeg_mappage_v0_codec='copy';
declare -g ffmpeg_mappage_a0_lang='fra';    declare -g ffmpeg_mappage_a0_codec='copy';
declare -g ffmpeg_mappage_a1_lang='eng';    declare -g ffmpeg_mappage_a1_codec='copy';
declare -g ffmpeg_mappage_a2_lang='eng';    declare -g ffmpeg_mappage_a2_codec='copy';
declare -g ffmpeg_mappage_s0_lang='fra';    declare -g ffmpeg_mappage_s0_codec='copy';
declare -g ffmpeg_mappage_s1_lang='eng';    declare -g ffmpeg_mappage_s1_codec='copy';
declare -g ffmpeg_mappage_s2_lang='eng';    declare -g ffmpeg_mappage_s2_codec='copy';

function ffmpeg_mappage-init(){
    ffmpeg-mappage-v0-lang;
    ffmpeg-mappage-a0-lang;
}

function ffmpeg-mappage-v0-lang(){ ffmpeg_mappage_v0_lang="${1:-"eng"}";};   function ffmpeg-mappage-v0-codec(){ ffmpeg_mappage_v0_codec="${1:-"copy"}";}
function ffmpeg-mappage-a0-lang(){ ffmpeg_mappage_a0_lang="${1:-"fra"}";};   function ffmpeg-mappage-a0-codec(){ ffmpeg_mappage_a0_codec="${1:-"copy"}";}
function ffmpeg-mappage-a1-lang(){ ffmpeg_mappage_a1_lang="${1:-"eng"}";};   function ffmpeg-mappage-a1-codec(){ ffmpeg_mappage_a1_codec="${1:-"copy"}";}
function ffmpeg-mappage-s0-lang(){ ffmpeg_mappage_s0_lang="${1:-"fra"}";};   function ffmpeg-mappage-s0-codec(){ ffmpeg_mappage_s0_codec="${1:-"copy"}";}
function ffmpeg-mappage-s1-lang(){ ffmpeg_mappage_s1_lang="${1:-"eng"}";};   function ffmpeg-mappage-s1-codec(){ ffmpeg_mappage_s1_codec="${1:-"copy"}";}
function ffmpeg-mappage-s2-lang(){ ffmpeg_mappage_s2_lang="${1:-"eng"}";};   function ffmpeg-mappage-s2-codec(){ ffmpeg_mappage_s2_codec="${1:-"copy"}";}

function ffmpeg-mappage(){
    local -i _fct_pile_app_level=5;
    fct-pile-app-in "$*" $_fct_pile_app_level;
    ffmpeg_mappage="${1:-""}";  # Si vide : manuel

    titre4 "Mappage";
    if [ ! -f "$ffmpeg_in_pathname" ]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]}: Fichier '$ffmpeg_in_pathname' introuvable.";
        fct-pile-app-out $E_INODE_NOT_EXIST $_fct_pile_app_level; return $E_INODE_NOT_EXIST;
    fi
    echo "tail -f --lines=200 $ffmpeg_log_pathname";


    echo '';
    display-vars 'ffmpeg_in_pathname ' "$ffmpeg_in_pathname";
    ffmpeg-metadata-show  "$ffmpeg_in_pathname";
    ffmpeg-streams-show   "$ffmpeg_in_pathname";
    display-vars 'ffmpeg_out_pathname' "$ffmpeg_out_pathname";
    display-vars 'ffmpeg_metadata_del' "$ffmpeg_metadata_del" 'ffmpeg-params-global-y' "$ffmpeg_params_global_y";

    #f: forced;   F: forced+default

    if [ -z "$ffmpeg_mappage" ]
    then
        echo  '# - va - #';
        titreInfo "va     - av:va - v0|a0-aac_ac2 - v0|a0-51 - v0-h264|a0  - v0-h264|a0-aac_ac2";
        echo  '# - vas - #';
        titreInfo "vas    - v0|a0-aac_ac2|s0-mov_text - v0|a0-51|s0-mov_text  - v0|a0|s0-F - v0|a0|s0-mov_text - v0|a0|s0-mov_text-F";
        echo '# - vaa - #';
        titreInfo "vaa    - v0|a0|a1-aac_ac2 - v0|a0-aac_ac2|a1-aac_ac2 - v0|a0-aac_ac2|a1 - v0|a0-ac2-D|a0:a1-51"
        echo '# - vaas - #';
        titreInfo "vaas   - v0|a0-aac-ac2|a1-aac-ac2|s0 - v0|a0-aac-ac2|a1-aac-ac2|s0-mov_tex - tv0|a0-aac-ac2|a1-aac-ac2|s0"
        echo '# - vaass   - #';
        titreInfo "vaass  - v0|a0|a1|s0-F|s1 - v0|a0-aac_ac2|a1-aac_ac2|s0-F-mov_text|s1-mov_text - v0|a0|a1|s0-fra-mov_text-F|s1-fra-mov_text - v0|a0|a1|s0-mov_text-D|s1-mov_text - v0|a0-aac-ac2|a1-aac-ac2|s0-mov_text|s1-mov_text - v0|a1:a0_aac_ac2|a0:a1-aac_ac2|s0-mov_text-D|s1-mov_text - v0|a0-aac_ac2|a1-aac_ac2|s0-fra-mov_text-F|s1-fra-mov_text";
        echo '# - vaasss   - #';
        titreInfo "vaas-Fss - v|a0-acc_ac2|a1-acc_ac2|s0-F|s1|s2";
        echo '# - vass - #';
        titreInfo "vass - v|a|s0-F|s1 - v0|a0|s0-mov_text-F|s1-mov_text - v0|a0|s1:s0-mov_text-F|s0:s1-mov_text - v0|a0|s0-F|s1-mov_text - v0|a0-aac_ac2|s0|s1 - v0|a0-aac_ac2|s0-F|s1 - v0|a0-aac_ac2|s0-mov_text-f|s1-mov_text - v0|a0-aac|s0-mov_text-F|s1-mov_text -  ";
        echo '# - vaaass - #';
        titreInfo "       - v0|a01-aac_ac2|a1:a1-fra-51|s1-eng-mov_text - v0|a0_ac2|a0:a1-fra-51|a1:a2-eng-51|s1:s0-fra-mov_text-F|s0:s1-fra-mov_text";
        echo '# - vaaasss - #';
        titreInfo '       - v0|a0:a0_ac2|a0:a1_fra_51|a1:a2_eng_51|s0:s0_fra_mov_text-F|s1:s1_fra_mov_text|s2:s2_eng_mov_text - v0_a0_51_a1_51_eng_a2_s0_s1_s2'
        echo "# - VFQ - #";
        titreInfo 'v0|a0|a2:a1|s0-VFQ-F|s1-VFF|s2-VFF-SDH|s3-VFQ-SDH|s4-SDH-eng';
        read -p "Lancer la méthode n° ?[0/numéro]: " ffmpeg_mappage;
    fi
    titreInfo "Mappage: $ffmpeg_mappage";
    case "$ffmpeg_mappage" in
        # - copy - #
            "copy" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-params-add "-codec: copy -codec:a copy -codec:s copy";
                ffmpeg-methode-exec;ffmpeg-methode-run;
                #ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

        # - v0 - #
            'v0' )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
            ;;
        # - va - #
            "va" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0-aac_ac2" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2";
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0-51" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0-h264|a0" )
                #ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'h264'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0-h264|a0-aac_ac2" )
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'h264'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2";
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;
            
        # - vas - #
            "vas" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'complet';                                       ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0-aac_ac2|s0-mov_text" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2 -disposition:${ffmpeg_map_dest} forced+default";
                ffmpeg-map-add '0:s:0' 's:0' "$ffmpeg_mappage_s0_lang" 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;
            "v0|a0|s0-F" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' "$ffmpeg_mappage_s0_lang" 'forcé' ;                                        ffmpeg-map-add-codec "copy -disposition:${ffmpeg_map_dest} forced+default";
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0-51|s0-mov_text"  )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' "$ffmpeg_mappage_s0_lang" 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;
            "v0|a0|s0-mov_text" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' "$ffmpeg_mappage_s0_lang" 'complet';                   ffmpeg-map-add-codec "mov_text";
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0|s0-mov_text-F" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'forcé';                                         ffmpeg-map-add-codec "mov_text -disposition:${ffmpeg_map_dest} forced+default";
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

        
        # - vaa - #
            "vaa")
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo';                    ffmpeg-map-add-codec 'copy';   ffmpeg-params-add "-disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:1' 'a:1' 'eng' 'Stéréo';                                        ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0|a1-aac_ac2")
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo';                    ffmpeg-map-add-codec "copy -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:1' 'a:1' 'eng' 'Stéréo';                                        ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2";
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;


            "v0|a0-aac_ac2|a1-aac_ac2" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2 -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:1' 'a:1' 'eng' 'Stéréo';                                        ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2";
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0-aac_ac2|a1" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2 -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:1' 'a:1' 'eng' 'Stéréo';                                        ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0-ac2-D|a0:a1-51" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2 -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:0' 'a:1' 'fra' '5.1';                                           ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;
        # - vaas - #
            "vaas" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo';                    ffmpeg-map-add-codec "copy -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:1' 'a:1' 'eng' 'Stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'complet';                   ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0-aac-ac2|a1-aac-ac2|s0" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2 -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:1' 'a:1' 'eng' 'Stéréo';                                        ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2";
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'complet';                                       ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0-aac-ac2|a1-aac-ac2|s0-mov_text" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2 -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:1' 'a:1' 'eng' 'Stéréo';                                        ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2";
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'complet';                                       ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0-51|a0-aac_ac2-F|a0:a1-51|s0-mov_text" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo (forced)';           ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2";   ffmpeg-params-add "-disposition:${ffmpeg_map_dest} default -disposition:${ffmpeg_map_dest} forced";
                ffmpeg-map-add '0:a:0' 'a:1' 'fra' '5.1';                                           ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'complet';                                       ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;
            "v0|a1_fra:a0|a0_eng:a1|s0-mov_text" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:1' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo (forced)';           ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2";   ffmpeg-params-add "-disposition:${ffmpeg_map_dest} default -disposition:${ffmpeg_map_dest} forced";
                ffmpeg-map-add '0:a:0' 'a:1' 'eng' 'Stéréo';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

        # - vass - #
            "vass" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' "$ffmpeg_mappage_s0_lang" 'complet' ;                  ffmpeg-map-add-codec "copy -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:s:1' 's:1' "$ffmpeg_mappage_s1_lang" 'complet';                   ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v|a|s0-F|s1" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' "$ffmpeg_mappage_s0_lang" 'forcé' ;                    ffmpeg-map-add-codec "copy -disposition:${ffmpeg_map_dest} default+forced";
                ffmpeg-map-add '0:s:1' 's:1' "$ffmpeg_mappage_s1_lang" 'complet';                   ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v|a0-aac_ac2|s0-F|s1" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2" ;
                ffmpeg-map-add '0:s:0' 's:0' "$ffmpeg_mappage_s0_lang" 'forcé' ;                    ffmpeg-map-add-codec "copy -disposition:${ffmpeg_map_dest} default+forced";
                ffmpeg-map-add '0:s:1' 's:1' "$ffmpeg_mappage_s1_lang" 'complet';                   ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0|s0-mov_text-F|s1-mov_text" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' "$ffmpeg_mappage_s0_lang" 'forcé' ;                    ffmpeg-map-add-codec "mov_text -disposition:${ffmpeg_map_dest} default+forced";
                ffmpeg-map-add '0:s:1' 's:1' "$ffmpeg_mappage_s1_lang" 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;
            "v0|a0|s1:s0-mov_text|s0:s1-mov_text" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:1' 's:0' "$ffmpeg_mappage_s0_lang" 'complet' ;                  ffmpeg-map-add-codec "mov_text -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:s:0' 's:1' "$ffmpeg_mappage_s1_lang" 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0|s0-F|s1-mov_text" )
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.srt\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.srt\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' "$ffmpeg_mappage_s0_lang" 'forcé' ;                    ffmpeg-map-add-codec 'copy'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced+default";
                ffmpeg-map-add '0:s:1' 's:1' "$ffmpeg_mappage_s1_lang" 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0-aac_ac2|s0-F|s1" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2";
                ffmpeg-map-add '0:s:0' 's:0' "$ffmpeg_mappage_s0_lang" 'forcé' ;                    ffmpeg-map-add-codec "$ffmpeg_mappage_s0_codec"; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced+default";
                ffmpeg-map-add '0:s:1' 's:1' "$ffmpeg_mappage_s1_lang" 'complet';                   ffmpeg-map-add-codec "$ffmpeg_mappage_s1_codec";
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0-aac_ac2|s0|s1" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2";
                ffmpeg-map-add '0:s:0' 's:0' "$ffmpeg_mappage_s0_lang" 'complet' ;                  ffmpeg-map-add-codec "$ffmpeg_mappage_s0_codec"; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:s:1' 's:1' "$ffmpeg_mappage_s1_lang" 'complet';                   ffmpeg-map-add-codec "$ffmpeg_mappage_s1_codec";
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;
            "v0|a0-aac|s0-mov_text-F|s1-mov_text" )
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.srt\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.srt\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec 'aac';
                ffmpeg-map-add '0:s:0' 's:0' "$ffmpeg_mappage_s0_lang" 'forcé' ;                    ffmpeg-map-add-codec 'mov_text'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced+default";
                ffmpeg-map-add '0:s:1' 's:1' "$ffmpeg_mappage_s1_lang" 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0-51|s0-F|s1" )
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.srt\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.srt\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' "$ffmpeg_mappage_s0_lang" 'forcé' ;                    ffmpeg-map-add-codec 'copy'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced+default";
                ffmpeg-map-add '0:s:1' 's:1' "$ffmpeg_mappage_s1_lang" 'complet';                   ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

        # - vaasss - #
            "vaas-Fss" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"

                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo';   ffmpeg-map-add-codec "$ffmpeg_mappage_a0_codec -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:1' 'a:1' "$ffmpeg_mappage_a1_lang" 'Stéréo';   ffmpeg-map-add-codec "$ffmpeg_mappage_a1_codec";

                ffmpeg-map-add '0:s:0' 's:0' "$ffmpeg_mappage_a0_lang" 'forcé'  ; ffmpeg-map-add-codec "$ffmpeg_mappage_s0_codec  -disposition:${ffmpeg_map_dest} default+forced";
                ffmpeg-map-add '0:s:1' 's:1' "$ffmpeg_mappage_s1_lang" 'complet'; ffmpeg-map-add-codec "$ffmpeg_mappage_s1_codec";
                ffmpeg-map-add '0:s:2' 's:2' "$ffmpeg_mappage_s2_lang" 'complet'; ffmpeg-map-add-codec "$ffmpeg_mappage_s2_codec";
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v|a0-acc_ac2|a1-acc_ac2|s0-F|s1|s2" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"

                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo';   ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2 -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:1' 'a:1' "$ffmpeg_mappage_a1_lang" 'Stéréo';   ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2";

                ffmpeg-map-add '0:s:0' 's:0' "$ffmpeg_mappage_a0_lang" 'forcé'  ; ffmpeg-map-add-codec "$ffmpeg_mappage_s0_codec  -disposition:${ffmpeg_map_dest} default+forced";
                ffmpeg-map-add '0:s:1' 's:1' "$ffmpeg_mappage_s1_lang" 'complet'; ffmpeg-map-add-codec "$ffmpeg_mappage_s1_codec";
                ffmpeg-map-add '0:s:2' 's:2' "$ffmpeg_mappage_s2_lang" 'complet'; ffmpeg-map-add-codec "$ffmpeg_mappage_s2_codec";
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;


        # - vaass - #
            "vaass" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo';                    ffmpeg-map-add-codec 'copy';   ffmpeg-params-add "-disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:1' 'a:1' 'eng' 'Stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'complet';                   ffmpeg-map-add-codec 'copy'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:s:1' 's:1' 'eng' 'complet';                   ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0|a1|s0-F|s1" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"

                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo';                    ffmpeg-map-add-codec "copy -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:1' 'a:1' "$ffmpeg_mappage_a1_lang" 'Stéréo';                    ffmpeg-map-add-codec "copy";

                ffmpeg-map-add '0:s:0' 's:0' "$ffmpeg_mappage_s0_lang" 'forcé';                     ffmpeg-map-add-codec "$ffmpeg_mappage_s0_codec"; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} default+forced";
                ffmpeg-map-add '0:s:1' 's:1' "$ffmpeg_mappage_s1_lang" 'complet';                   ffmpeg-map-add-codec "$ffmpeg_mappage_s1_codec";
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;


            "v0|a0-aac_ac2|a1-aac_ac2|s0-F|s1" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"

                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2 -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:1' 'a:1' "$ffmpeg_mappage_a1_lang" 'Stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2";

                ffmpeg-map-add '0:s:0' 's:0' "$ffmpeg_mappage_s0_lang" 'forcé';                     ffmpeg-map-add-codec "$ffmpeg_mappage_s0_codec"; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} default+forced";
                ffmpeg-map-add '0:s:1' 's:1' "$ffmpeg_mappage_s1_lang" 'complet';                   ffmpeg-map-add-codec "$ffmpeg_mappage_s1_codec";
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;


            "v0|a0-aac_ac2|a1-aac_ac2|s0-F-mov_text|s1-mov_text" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"

                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2 -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:1' 'a:1' "$ffmpeg_mappage_a1_lang" 'Stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2";

                ffmpeg-map-add '0:s:0' 's:0' "$ffmpeg_mappage_s0_lang" 'forcé';                     ffmpeg-map-add-codec "mov_text"; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} default+forced";
                ffmpeg-map-add '0:s:1' 's:1' "$ffmpeg_mappage_s1_lang" 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0|a1|s0-mov_text-D|s1-mov_text" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo';                    ffmpeg-map-add-codec 'copy';   ffmpeg-params-add "-disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:1' 'a:1' 'eng' 'Stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-map-add '0:s:1' 's:1' 'eng' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0-aac-ac2|a1-aac-ac2|s0-mov_text|s1-mov_text" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2 -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:1' 'a:1' 'eng' 'Stéréo';                                        ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2";
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'complet';                                       ffmpeg-map-add-codec 'mov_text';
                ffmpeg-map-add '0:s:1' 's:1' 'eng' 'complet';                                       ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a1:a0_aac_ac2|a0:a1-aac_ac2|s0-mov_text-D|s1-mov_text" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:1' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2";   ffmpeg-params-add "-disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:0' 'a:1' 'eng' 'Stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2";
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-map-add '0:s:1' 's:1' 'eng' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0-aac_ac2|a1-aac_ac2|s0-fra-mov_text-F|s1-fra-mov_text" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'Stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2";   ffmpeg-params-add "-disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:1' 'a:1' 'eng' 'Stéréo';                                        ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2";
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'forcé';                                         ffmpeg-map-add-codec 'mov_text';    ffmpeg-params-add "-disposition:${ffmpeg_map_dest} default+forced";
                ffmpeg-map-add '0:s:1' 's:1' 'fra' 'complet';                                       ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;
            "v0|a01:a0_ac2|a1:a1_51_fra|a0:a2_51_eng|s0_fra_mov_text|s1-eng-mov_text" )
                #eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                #eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                #eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:2 \"${tmp_rep}/${ffmpeg_in_name}-map2.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:1' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2"; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced+default";
                ffmpeg-map-add '0:a:1' 'a:1' 'fra' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:a:0' 'a:2' 'eng' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'complet';                   ffmpeg-map-add-codec 'mov_text'; 
                ffmpeg-map-add '0:s:1' 's:1' 'eng' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;
            "v0|a0:a0_ac2|a0:a1_fra_51|a1:a2_eng_51|s1:s0_fra_mov_text-F|s0:s1_fra_mov_text" )
                #eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                #eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                #eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:2 \"${tmp_rep}/${ffmpeg_in_name}-map2.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2"; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced+default";
                ffmpeg-map-add '0:a:0' 'a:1' 'fra' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:a:1' 'a:2' 'eng' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:1' 's:0' 'fra' 'forcé' ;                    ffmpeg-map-add-codec 'mov_text'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced+default";
                ffmpeg-map-add '0:s:0' 's:1' 'fra' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

        # - vaaaasss - #';
            "v0|a0:a0_ac2|a0:a1_fra_51|a1:a2_eng_51|s0:s0_fra_mov_text-F|s1:s1_fra_mov_text|s2:s2_eng_mov_text" )
                #eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                #eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                #eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:2 \"${tmp_rep}/${ffmpeg_in_name}-map2.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" 'stéréo';                    ffmpeg-map-add-codec "aac -ac:${ffmpeg_map_dest} 2"; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced+default";
                ffmpeg-map-add '0:a:0' 'a:1' 'fra' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:a:1' 'a:2' 'eng' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'forcé' ;                    ffmpeg-map-add-codec 'mov_text'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced+default";
                ffmpeg-map-add '0:s:1' 's:1' 'fra' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-map-add '0:s:2' 's:2' 'eng' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;
            "v0_a0_51_a1_51_eng_a2_s0_s1_s2" )
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:2 \"${tmp_rep}/${ffmpeg_in_name}-map2.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' "$ffmpeg_mappage_a0_lang" '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:a:1' 'a:1' 'eng' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:a:2' 'a:2' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'copy'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced+default";
                ffmpeg-map-add '0:s:1' 's:0' 'fra' 'forcé' ;                    ffmpeg-map-add-codec 'copy'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced+default";
                ffmpeg-map-add '0:s:0' 's:1' 'fra' 'complet';                   ffmpeg-map-add-codec 'copy'; 
                ffmpeg-map-add '0:s:2' 's:2' 'eng' 'complet';                   ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;
        
            "v0|a0|a2:a1|s0-VFQ-F|s1-VFF|s2-VFF-SDH|s3-VFQ-SDH|s4-SDH-eng")
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' "$ffmpeg_mappage_v0_lang" "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                
                ffmpeg-map-add '0:a:0' 'a:0' "fra" 'stéréo';           ffmpeg-map-add-codec 'copy';
                #ffmpeg-map-add '0:a:1' 'a:1' 'fra' 'VFQ';          ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:a:2' 'a:1' 'eng' 'stéréo';        ffmpeg-map-add-codec 'copy';
                
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'VFQ-forcé' ;    ffmpeg-map-add-codec 'copy'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced+default";
                ffmpeg-map-add '0:s:1' 's:1' 'fra' 'VFFF';          ffmpeg-map-add-codec 'copy'; 
                ffmpeg-map-add '0:s:2' 's:2' 'fra' 'VFF SDH';       ffmpeg-map-add-codec 'copy'; 
                ffmpeg-map-add '0:s:3' 's:3' 'fra' 'VFQ SDH';       ffmpeg-map-add-codec 'copy'; 
                ffmpeg-map-add '0:s:4' 's:4' 'eng' 'SDH';           ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ;;
        * )
            echo "Annulé";return $E_FALSE; ;;
    esac
    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}

return 0;


