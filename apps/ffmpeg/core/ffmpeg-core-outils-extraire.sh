echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh --show-libsLevel2 ffmpeg lib
# date de création    : 2024.08.04
# date de modification: 2024.08.08

# function ffmpeg-extraire-image(){
# Extrait une image d'une video dans le repertoire courant
# Dépendant de: rien
# clear;gtt.sh -d --libExecAuto apps/ffmpeg/tests/test-fct-ffmpeg-extraire-image test-fct-ffmpeg-extraire-image # - src=pathname ss='HH:MM:SS.sss'

declare -g ffmpeg_extraire_image_src_pathname="";  # chemin complet  de la destination
declare -g ffmpeg_extraire_image_src_rep="";       # cheminc complet de la destination ($PWD)
declare -g ffmpeg_extraire_image_src_name="";      # name  (nom.ext) de la vidéo créée
declare -g ffmpeg_extraire_image_src_nom="";       # nom   (nom)     de la vidéo créée
declare -g ffmpeg_extraire_image_src_ext="";       # ext de la vidéo créée

declare -g ffmpeg_extraire_image_dest_pathname="";  # chemin complet de la destination
declare -g ffmpeg_extraire_image_dest_rep="";       # cheminc complet de la destination ($PWD)
declare -g ffmpeg_extraire_image_dest_name="";      # name de l'image créée
declare -g ffmpeg_extraire_image_dest_nom="";       # nom  de l'image créée
declare -g ffmpeg_extraire_image_dest_ext="";       # ext  de l'image créée

declare -g ffmpeg_extraire_image_ss="";

addLibrairie 'ffmpeg-extraire-image' "- src=pathname ss='HH:MM:SS.sss'";
function      ffmpeg-extraire-image(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;


    # - Initialisation des variables - #
        ffmpeg_extraire_image_src_pathname="";  # chemin complet  de la destination
        ffmpeg_extraire_image_src_rep="";       # cheminc complet de la destination ($PWD)
        ffmpeg_extraire_image_src_name="";      # name  (nom.ext) de la vidéo créée
        ffmpeg_extraire_image_src_nom="";       # nom   (nom)     de la vidéo créée
        ffmpeg_extraire_image_src_ext="";       # ext de la vidéo créée

        ffmpeg_extraire_image_dest_pathname="";  # chemin complet  de la destination
        ffmpeg_extraire_image_dest_rep="";       # cheminc complet de la destination ($PWD)
        ffmpeg_extraire_image_dest_name="";      # name  (nom.ext) de la vidéo créée
        ffmpeg_extraire_image_dest_nom="";       # nom   (nom)     de la vidéo créée
        ffmpeg_extraire_image_dest_ext="";       # ext de la vidéo créée

        ffmpeg_extraire_image_ss="";             # ext de la vidéo créée


    # - Controle des parametres - #
        local _src="${1:-""}";  # parametre obligatoire
        if [ "$_src" = '' ]
        then
            _src="${PSP_params['src']:-""}";
            if [ "$_src" = '' ]
            then
                erreurs-pile-add-echo "${FUNCNAME[0]} - ss='HH:MM:SS.sss' src=_src"
                fct-pile-app-out $E_ARG_REQUIRE $_fct_pile_app_level; return $E_ARG_REQUIRE;
            fi
        fi
        #display-vars '_src' "$_src";

        ffmpeg_extraire_image_ss="${2:-""}";   # parametre obligatoire
        if [ "$ffmpeg_extraire_image_ss" = '' ]
        then
            ffmpeg_extraire_image_ss="${PSP_params['ss']:-""}";
            if [ "$ffmpeg_extraire_image_ss" = '' ]
            then
                erreurs-pile-add-echo "${FUNCNAME[0]} - ss='HH:MM:SS.sss' src=_src"
                fct-pile-app-out $E_ARG_REQUIRE $_fct_pile_app_level; return $E_ARG_REQUIRE;
            fi
        fi
    #


    # - src: split et calcul du pathname - #
        ffmpeg_extraire_image_src_rep="${_src%/*}";
        ffmpeg_extraire_image_src_name="${_src##*/}";
        ffmpeg_extraire_image_src_nom="${ffmpeg_extraire_image_src_name%.*}";
        ffmpeg_extraire_image_src_ext="${ffmpeg_extraire_image_src_name##*.}";
        ffmpeg_extraire_image_src_pathname="${ffmpeg_extraire_image_src_rep}/${ffmpeg_extraire_image_src_name}";


        # - Calcul de l'inode de destination - #
        ffmpeg_extraire_image_dest_rep="$PWD";
        ffmpeg_extraire_image_dest_nom="${ffmpeg_extraire_image_src_nom}-extrait-${ffmpeg_extraire_image_ss/\:/-}";
        ffmpeg_extraire_image_dest_ext="png";
        ffmpeg_extraire_image_dest_name="${ffmpeg_extraire_image_dest_nom}.${ffmpeg_extraire_image_dest_ext}";
        ffmpeg_extraire_image_dest_pathname="${ffmpeg_extraire_image_dest_rep}/${ffmpeg_extraire_image_dest_name}";
        titreInfo "Vidéo générée: $ffmpeg_extraire_image_dest_pathname";
    #


    # - Vérification de la préexisance de la  destination - #
    if [ -f "$ffmpeg_extraire_image_dest_pathname" ]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]} : La destination existe déjà ($ffmpeg_extraire_image_dest_pathname)";
        fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
    fi


    # - Execution - #
    # vsync is deprecated use -fps_mode
    #-fps_mode[:stream_specifier] parameter (output,per-stream)
    # Set video sync method / framerate mode. vsync is applied to all output video streams but can be overridden for a stream by setting fps_mode.
    # vsync is deprecated and will be removed in the future.
    # For compatibility reasons some of the values for vsync can be specified as numbers (shown in parentheses in the following table).
    # passthrough (0): Each frame is passed with its timestamp from the demuxer to the muxer. 
    # cfr (1)        : Frames will be duplicated and dropped to achieve exactly the requested constant frame rate. 
    # vfr (2)        : Frames are passed through with their timestamp or dropped so as to prevent 2 frames from having the same timestamp. 
    # auto (-1)      : Chooses between cfr and vfr depending on muxer capabilities. This is the default method. 

    #ffmpeg -hide_banner  -ss $_ss -i "$_src" -frames:v 1 -vsync 0 -f image2 "$ffmpeg_extraire_image_dest_name";
    #eval-echo "ffmpeg -hide_banner -update  -ss \"$_ss\" -i \"$_src\" -frames:v 1 -fps_mode:v passthrough -f image2 \"$ffmpeg_extraire_image_dest_name\";";
    eval-echo "ffmpeg -hide_banner   -ss \"$ffmpeg_extraire_image_ss\" -i \"$ffmpeg_extraire_image_src_pathname\" -frames:v 1  \"$ffmpeg_extraire_image_dest_pathname\"";
    if [ -f "$ffmpeg_extraire_image_dest_pathname" ]
    then
        display "$ffmpeg_extraire_image_dest_pathname";   #lance imagemagic
    fi

    fct-pile-app-out $erreur_no $_fct_pile_app_level; return $erreur_no;
}


function      ffmpeg-extraire-image-display-vars(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    display-vars 'ffmpeg_extraire_image_src_pathname' "$ffmpeg_extraire_image_src_pathname";
    display-vars 'ffmpeg_extraire_image_src_rep     ' "$ffmpeg_extraire_image_src_rep";
    display-vars 'ffmpeg_extraire_image_src_name    ' "$ffmpeg_extraire_image_src_name";
    display-vars 'ffmpeg_extraire_image_src_nom     ' "$ffmpeg_extraire_image_src_nom";
    display-vars 'ffmpeg_extraire_image_src_ext     ' "$ffmpeg_extraire_image_src_ext";

    display-vars 'ffmpeg_extraire_image_dest_pathname' "$ffmpeg_extraire_image_dest_pathname";
    display-vars 'ffmpeg_extraire_image_dest_rep     ' "$ffmpeg_extraire_image_dest_rep";
    display-vars 'ffmpeg_extraire_image_dest_name    ' "$ffmpeg_extraire_image_dest_name";
    display-vars 'ffmpeg_extraire_image_dest_nom     ' "$ffmpeg_extraire_image_dest_nom";
    display-vars 'ffmpeg_extraire_image_dest_ext     ' "$ffmpeg_extraire_image_dest_ext";

    display-vars 'ffmpeg_extraire_image_ss           ' "$ffmpeg_extraire_image_ss";
    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}


# function ffmpeg-extraire-video(){
# Extrait une section video d'une video dans le repertoire courant
# Dépendant de: rien
# clear;gtt.sh -d --libExecAuto ffmpeg/tests/test-fct-ffmpeg-extraire-video test-fct-ffmpeg-extraire-video # - src=pathname  [ss='HH:MM:SS.sss']  [to='HH:MM:SS.sss']
# Note: travail dans le répertoire courant
declare -g ffmpeg_extraire_video_src_pathname="";  # chemin complet de la destination
declare -g ffmpeg_extraire_video_src_rep="";       # cheminc complet de la destination ($PWD)
declare -g ffmpeg_extraire_video_src_name="";      # name  (nom.ext) de la vidéo créée
declare -g ffmpeg_extraire_video_src_nom="";       # name  (nom.ext) de la vidéo créée
declare -g ffmpeg_extraire_video_src_ext="";       # ext de la vidéo créée

declare -g ffmpeg_extraire_video_dest_pathname="";  # chemin complet de la destination
declare -g ffmpeg_extraire_video_dest_rep="";       # cheminc complet de la destination ($PWD)
declare -g ffmpeg_extraire_video_dest_name="";      # name  (nom.ext) de la vidéo créée
declare -g ffmpeg_extraire_video_dest_nom="";       # name  (nom.ext) de la vidéo créée
declare -g ffmpeg_extraire_video_dest_ext="";       # ext de la vidéo créée

declare -g ffmpeg_extraire_video_ss="";
declare -g ffmpeg_extraire_video_to="";


addLibrairie 'ffmpeg-extraire-video' "ffmpeg-extraire-video - src=pathname [ss='HH:MM:SS.sss'] [to='HH:MM:SS.sss'] avec tous les flux vidéo";
function      ffmpeg-extraire-video(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";

    # - Initialisation des variables - #
    ffmpeg_extraire_video_src_pathname="";  # chemin complet  de la destination
    ffmpeg_extraire_video_src_rep="";       # cheminc complet de la destination ($PWD)
    ffmpeg_extraire_video_src_name="";      # name  (nom.ext) de la vidéo créée
    ffmpeg_extraire_video_src_nom="";       # name  (nom)     de la vidéo créée
    ffmpeg_extraire_video_src_ext="";       # ext             de la vidéo créée

    ffmpeg_extraire_video_dest_pathname="";  # chemin complet  de la destination
    ffmpeg_extraire_video_dest_rep="";       # cheminc complet de la destination ($PWD)
    ffmpeg_extraire_video_dest_name="";      # name  (nom.ext) de la vidéo créée
    ffmpeg_extraire_video_dest_nom="";       # name  (nom)     de la vidéo créée
    ffmpeg_extraire_video_dest_ext="";       # ext             de la vidéo créée

    ffmpeg_extraire_video_ss="";
    ffmpeg_extraire_video_to="";

    # - Controle des parametres - #
        local _src="${1:-""}";  # parametre obligatoire
        if [ "$_src" = '' ]
        then
            _src="${PSP_params['src']:-""}";
            if [ "$_src" = '' ]
            then
                erreurs-pile-add-echo "${FUNCNAME[0]} - src='' [ss='HH:MM:SS.sss'] [to='HH:MM:SS.sss']  # parametre 'src' manquant";
                fct-pile-app-out $E_ARG_REQUIRE $_fct_pile_app_level; return $E_ARG_REQUIRE;
            fi
        fi
        #display-vars '_src' "$_src";

        local ffmpeg_extraire_video_ss="${2:-""}";   # parametre optionel
        if [ "$ffmpeg_extraire_video_ss" = '' ]
        then
            ffmpeg_extraire_video_ss="${PSP_params['ss']:-""}";
            #if [ "$ffmpeg_extraire_video_ss" = '' ]
            #then
            #    erreurs-pile-add-echo "${FUNCNAME[0]} - ss='HH:MM:SS.sss' to='HH:MM:SS.sss' src=_src; # parametre 'ss' manquant";
            #    fct-pile-app-out $E_ARG_REQUIRE $_fct_pile_app_level; return $E_ARG_REQUIRE;
            #fi
        fi

        local ffmpeg_extraire_video_to="${3:-""}";   # parametre optionel
        if [ "$ffmpeg_extraire_video_to" = '' ]
        then
            ffmpeg_extraire_video_to="${PSP_params['to']:-""}";
            #if [ "$ffmpeg_extraire_video_to" = '' ]
            #then
            #    erreurs-pile-add-echo "${FUNCNAME[0]} - ss='HH:MM:SS.sss' to='HH:MM:SS.sss' src=_src; # parametre 'to' manquant";
            #    fct-pile-app-out $E_ARG_REQUIRE $_fct_pile_app_level; return $E_ARG_REQUIRE;
            #fi
        fi
    #

    # - src: split et calcul du pathname - #
        ffmpeg_extraire_video_src_rep="${_src%/*}";
        ffmpeg_extraire_video_src_name="${_src##*/}";
        ffmpeg_extraire_video_src_nom="${ffmpeg_extraire_video_src_name%.*}";
        ffmpeg_extraire_video_src_ext="${ffmpeg_extraire_video_src_name##*.}";
        ffmpeg_extraire_video_src_pathname="${ffmpeg_extraire_video_src_rep}/${ffmpeg_extraire_video_src_name}";


        # - Calcul de l'inode de destination - #
        ffmpeg_extraire_video_dest_rep="$PWD";
        # on laisse les tirets de séparation entre nom-ss-to, même si vide
        ffmpeg_extraire_video_dest_nom="${ffmpeg_extraire_video_src_nom}-extrait-${ffmpeg_extraire_video_ss/\:/-}-${ffmpeg_extraire_video_to/\:/-}";
        ffmpeg_extraire_video_dest_ext="$ffmpeg_extraire_video_src_ext";
        ffmpeg_extraire_video_dest_name="${ffmpeg_extraire_video_dest_nom}.${ffmpeg_extraire_video_dest_ext}";
        ffmpeg_extraire_video_dest_pathname="${ffmpeg_extraire_video_dest_rep}/${ffmpeg_extraire_video_dest_name}";
        titreInfo "Vidéo générée: $ffmpeg_extraire_video_dest_pathname";
    #

    # - Vérification de la préexisance de la destination - #
    if [ -f "$ffmpeg_extraire_video_dest_pathname" ]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]} : La destination existe déjà ($ffmpeg_extraire_video_dest_pathname)";
        fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
    fi


    # - Configuration des parametres ffmpeg optionels - #
    local _ffmpeg_ss="";
    if [ "$ffmpeg_extraire_video_ss" != "" ]; then  _ffmpeg_ss=" -ss \"$ffmpeg_extraire_video_ss\"";fi

    local _ffmpeg_to="";
    if [ "$ffmpeg_extraire_video_to" != "" ]; then  _ffmpeg_to=" -to \"$ffmpeg_extraire_video_to\"";fi


    # - Execution - #
    titreInfo "tail -f $ffmpeg_log_pathname";
    eval-echo "ffmpeg -hide_banner${_ffmpeg_ss}${_ffmpeg_to} -i \"$ffmpeg_extraire_video_src_pathname\" -c:v copy -an -sn \"$ffmpeg_extraire_video_dest_pathname\" 2>> \"$ffmpeg_log_pathname\""
    if [ $erreur_no -ne 0 ]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]} : $ffmpeg_extraire_video_dest_pathname: ffmpeg erreur: $erreur_no";
        fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
    fi

    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}


function      ffmpeg-extraire-video-display-vars(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    display-vars 'ffmpeg_extraire_video_src_pathname' "$ffmpeg_extraire_video_src_pathname";
    display-vars 'ffmpeg_extraire_video_src_rep     ' "$ffmpeg_extraire_video_src_rep";
    display-vars 'ffmpeg_extraire_video_src_name    ' "$ffmpeg_extraire_video_src_name";
    display-vars 'ffmpeg_extraire_video_src_nom     ' "$ffmpeg_extraire_video_src_nom";
    display-vars 'ffmpeg_extraire_video_src_ext     ' "$ffmpeg_extraire_video_src_ext";

    display-vars 'ffmpeg_extraire_video_dest_pathname' "$ffmpeg_extraire_video_dest_pathname";
    display-vars 'ffmpeg_extraire_video_dest_rep     ' "$ffmpeg_extraire_video_dest_rep";
    display-vars 'ffmpeg_extraire_video_dest_name    ' "$ffmpeg_extraire_video_dest_name";
    display-vars 'ffmpeg_extraire_video_dest_nom     ' "$ffmpeg_extraire_video_dest_nom";
    display-vars 'ffmpeg_extraire_video_dest_ext     ' "$ffmpeg_extraire_video_dest_ext";

    display-vars 'ffmpeg_extraire_video_ss           ' "$ffmpeg_extraire_video_ss";

    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}


#function ffmpeg-extraire-audio(){
# Extrait une  d'une video
# Dépend de : ffmpeg-infos pour connaitre le codec audio
# Utilise   : ffmpeg_in_pathname
# Non opérationel
addLibrairie 'ffmpeg-extraire-audio' '- src="" [_map_audio=""]  [dest_rep='./'] [out_ext=''] ):Extrait la piste audio du fichier donné en src'
function      ffmpeg-extraire-audio(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";

    
    # - Controle des parametres - #
    local _src="${1:-""}";
    if [ "$_src" = '' ]
    then
        _src="${PSP_params['src']:-""}";
        if [ "$_src" = '' ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]} - src=_src [map_audio='-map 0:1'] [dest_rep='_src_rep'] [dest_ext='']";
            fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
        fi
    fi
    #display-vars '_src' "$_src";

    local _map_audio="${2:-""}";
    if [ "$_map_audio" = '' ]
    then
        _map_audio="${PSP_params['map_audio']:-""}";
        if [ "$_map_audio" = '' ]
        then
            _map_audio='-map 0:1';
        fi
    fi
    #display-vars '_map_audio' "$_map_audio";

    local _dest_rep="${3:-""}";
    if [ "$_dest_rep" = '' ]
    then
        _dest_rep="${PSP_params['destRep']:-""}";
        if [ "$_dest_rep" = '' ]
        then
            _dest_rep='';    # laisser vide car , par défaut, il est dans le repertoire du fichier source (calculé plus loin)
        fi
    fi
    #display-vars '_dest_rep' "$_dest_rep";

    local _dest_ext="${4:-""}";
    if [ "$_dest_ext" = '' ]
    then
        _dest_rep="${PSP_params['dest_ext']:-""}";
        if [ "$_dest_rep" = '' ]
        then
            _dest_rep='';    # laisser vide car , par défaut, le mode automatique donne l'extention de la contenu dans source
        fi
    fi
    #display-vars '_dest_rep' "$_dest_rep";


    # - création de l'objet ffmpeg-in - #
    ffmpeg-in-pathname "$_src";


    # - Recherche du repertoire de destination - #
    if [ "$_dest_rep" == '' ]
    then
        _dest_rep="$ffmpeg_in_rep";
    fi

    if [ ! -d "$_dest_rep" ]\
    && [ ! -w "$_dest_rep" ]
    then
        erreurs-pile-add-echo "'$_dest_rep' n'est pas un répertoire ou n'est pas writable.";
        fct-pile-app-out $E_INODE_NOT_EXIST $_fct_pile_app_level; return $E_INODE_NOT_EXIST;
    fi


    # - Recherche du codec audio et définir son extention - #
    if [  "$_dest_ext" = "" ]
    then
        _dest_ext="$ffmpeg_infos_ca";         # par defaut ext selon ffmpeg -i
        case "$_dest_ext" in
            'mpeg4') _dest_ext='m4a'; ;;
        esac
    fi
    display-vars '_dest_ext' "$_dest_ext";


    local _out_pathname="$_dest_rep/${ffmpeg_in_nom}.${_dest_ext}";
    display-vars '_out_pathname' "$_out_pathname";
    if [ "$_out_pathname" = "$ffmpeg_in_pathname" ]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]} : La destination est la même que la source ($ffmpeg_in_pathname)";
        fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
    fi

    if [ -f "$_out_pathname" ]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]} : La destination existe déjà ($_out_pathname)";
        fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
    fi


    # - Execution - #
    titreInfo "tail -f $ffmpeg_log_pathname";
    eval-echo "ffmpeg -hide_banner -i \"$ffmpeg_in_pathname\" -vn -sn $_map_audio \"$_out_pathname\" 2>> \"$ffmpeg_log_pathname\""
    lastErrNu=$?;
    if [ $lastErrNu -ne 0 ]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]} : $ffmpeg_in_pathname: ffmpeg erreur: $lastErrNu";
        fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
    fi

    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}

#function      ffmpeg-extraire-srt(){
addLibrairie 'ffmpeg-extraire-srt' '- src="" [_map_srt="0:s:0"]  [dest_rep='./'] [dest_ext=''] ):Extrait la piste de sous titre du fichier donné en src'
function      ffmpeg-extraire-srt(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";

    
    # - Controle des parametres - #
    local _src="${1:-""}";
    if [ -z "$_src" ]
    then
        _src="${PSP_params['src']:-"$ffmpeg_in_pathname"}";
        if [ -z "$_src" ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]} - src=_src [map_srt='0:s:0'] [dest_rep='_src_rep'] [dest_ext='']";
            fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
        fi
    fi
    display-vars '_src' "$_src";

    local _map_srt="${2:-""}";
    if [ -z "$_map_srt" ]
    then
        _map_srt="${PSP_params['map_srt']:-"0:s:0"}";
    fi
    display-vars '_map_srt' "$_map_srt";

    local _dest_rep="${3:-""}";
    if [ -z "$_dest_rep" ]
    then
        _dest_rep="${PSP_params['dest_rep']:-""}";
        if [ -z "$_dest_rep" ]
        then
            _dest_rep='';    # laisser vide car , par défaut, il est dans le repertoire du fichier source (calculé plus loin)
        fi
    fi
    display-vars '_dest_rep' "$_dest_rep";

    local _dest_ext="${4:-""}";
    if [ -z "$_dest_ext" ]
    then
        _dest_ext="${PSP_params['dest_ext']:-"srt"}";
    fi
    display-vars '_dest_ext' "$_dest_ext";


    # - création de l'objet ffmpeg-in - #
    ffmpeg-in-pathname "$_src";


    # - Recherche du repertoire de destination - #
    if [ -z "$_dest_rep" ]
    then
        _dest_rep="$ffmpeg_in_rep";
    fi

    if [ ! -d "$_dest_rep" ]\
    && [ ! -w "$_dest_rep" ]
    then
        erreurs-pile-add-echo "'$_dest_rep' n'est pas un répertoire ou n'est pas writable.";
        fct-pile-app-out $E_INODE_NOT_EXIST $_fct_pile_app_level; return $E_INODE_NOT_EXIST;
    fi


    local _dest_pathname="$_dest_rep/${ffmpeg_in_nom}.${_dest_ext}";
    display-vars '_dest_pathname' "$_dest_pathname";
    if [ "$_dest_pathname" = "$ffmpeg_in_pathname" ]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]} : La destination est la même que la source ($ffmpeg_in_pathname)";
        fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
    fi

    if [ -f "$_out_pathname" ]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]} : La destination existe déjà ($_out_pathname)";
        fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
    fi


    # - Execution - #
    titreInfo "tail -f $ffmpeg_log_pathname";
    eval-echo "ffmpeg -hide_banner -i \"$ffmpeg_in_pathname\" -map $_map_srt -codec:s mov_text \"$_out_pathname\" 2>> \"$ffmpeg_log_pathname\""
    lastErrNu=$?;
    if [ $lastErrNu -ne 0 ]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]} : $ffmpeg_in_pathname: ffmpeg erreur: $lastErrNu";
        fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
    fi

    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}





#    # a:eng s:srt
#    ffmpeg -n -i "${_name}.avi" -i "${_name}.srt" \
#    -c:v copy -c:a copy -c:s mov_text \
#    -map 0:0 \
#    -map 0:2 \
#    -map 1  -metadata:s:a:0 title='Sous titre eng' -metadata:s:a:0 language='eng'  \
#    "/$tmp_rep/${_name}-eng-srtFRA.avi"
#}

return 0;
