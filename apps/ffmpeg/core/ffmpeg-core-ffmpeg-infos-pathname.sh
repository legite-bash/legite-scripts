echo-d "${BASH_SOURCE[0]}" 1;
# gtt.sh --show-libsLevel2 apps/ffmpeg/ffmpeg.sh lib
# date de création    : 202?
# date de modification: 2024.08.24

################################
# objet: ffmpeg-infos-pathname #
################################
# Dépend de:
# * ffmpeg-container-is_valide 
# * ffmpeg-setInfos-definitionX 
# * ffmpeg-setInfos-definitionY


declare -g  ffmpeg_infos_rep='';
declare -g  ffmpeg_infos_pathname='';
declare -g  ffmpeg_infos_name='';
declare -g  ffmpeg_infos_nom='';
declare -g  ffmpeg_infos_ext='';
declare -gi ffmpeg_infos_sizeWC=0;
declare -g  ffmpeg_infos_duration='';
declare -g  ffmpeg_infos_container=0;           # mkv, avi, mp4
declare -g  ffmpeg_infos_fps=0;                 # ne pas mettre en integer
declare -g  ffmpeg_infos_rtXY=0;                # 
declare -g  ffmpeg_infos_rtYX=0;                # 
declare -g  ffmpeg_infos_cv='';                 # codec video
declare -g  ffmpeg_infos_ca='';                 # codec audio
declare -g  ffmpeg_infos_definition='';         # XXXxYYY
declare -g  ffmpeg_infos_definitionX='';        # largeur
declare -g  ffmpeg_infos_definitionY='';        # hauteur
declare -g  ffmpeg_infos_tmp_name="ffmpeg-infos";    
declare -g  ffmpeg_infos_tmp_pathname="$ffmpeg_infos_tmp_name";    # fichier temporaire pour le traitement de getInfo()
declare -g  ffmpeg_infos_refresh=true;          # ne pas réappeller getInfos()
declare -gi ffmpeg_infos_show_fichierNb=0;      # nb total de fichier lu par ()

#declare -g  ffmpeg_infos_show_type='';


# - setPahthname- #    
function ffmpeg-infos-pathname(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    # - Controle des entrées - #
    ffmpeg_infos_pathname="${1:-""}";
    if [ "$ffmpeg_infos_pathname" = "" ]
    then
        ffmpeg-infos-init;
        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    fi

    # - Execution - #
    ffmpeg_infos_rep="${ffmpeg_infos_pathname%/*}";
    ffmpeg_infos_name="${ffmpeg_infos_pathname##*/}";
    ffmpeg_infos_nom="${ffmpeg_infos_name%.*}";
    ffmpeg_infos_ext="${ffmpeg_infos_name##*.}";
    
    ffmpeg_infos_tmp_name="ffmpeg-info-$ffmpeg_uuid";
    ffmpeg_infos_tmp_Pathname="$tmp_rep/$ffmpeg_infos_tmp_name";

    #ffmpeg_in_display-var;
    ffmpeg-infos-get "$ffmpeg_infos_pathname";
    ffmpeg_infos_refresh=false;
    #ffmpeg-infos-show false;

    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}


function ffmpeg-infos-init(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    ffmpeg_infos_sizeWC=0;
    ffmpeg_infos_container=0;           # mkv, avi, mp4
    ffmpeg_infos_fps=0;                 # ne pas mettre en integer
    ffmpeg_infos_rtXY=0;             # 
    ffmpeg_infos_rtYX=0;             # 
    ffmpeg_infos_cv='';                 # codec video
    ffmpeg_infos_ca='';                 # codec audio
    ffmpeg_infos_definition='';         # XXXxYYY
    ffmpeg_infos_definitionX=1;         # largeur
    ffmpeg_infos_definitionY=1;         # hauteur
    ffmpeg_infos_tmp_name="ffmpeg-infos";    
    ffmpeg_infos_tmp_pathname="$ffmpeg_infos_tmp_name";    # fichier temporaire pour le traitement de getInfo()
    ffmpeg_infos_refresh=true;          # ne pas réappeller getInfos()

    isffmpeg_infos_show_recursif_fctBefore=true;
        ffmpeg_infos_show_recursif_fctBefore='ffmpeg-infos-show-header;';
        ffmpeg_infos_show_recursif_fct="ffmpeg-infos-show-body";
    isffmpeg_infos_show_recursif_fctAfter=true;
        ffmpeg_infos_show_recursif_fctAfter='ffmpeg-infos-show-footer';

    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}



# Affiche sous forme de display-vars les variables d'informations
function ffmpeg-infos-display-vars(){
    local -i _fct_pile_app_level=5;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    display-vars 'ffmpeg_infos_rep          ' "$ffmpeg_infos_rep";
    display-vars 'ffmpeg_infos_pathname     ' "$ffmpeg_infos_pathname";
    display-vars 'ffmpeg_infos_name         ' "$ffmpeg_infos_name";
    display-vars 'ffmpeg_infos_nom          ' "$ffmpeg_infos_nom";
    display-vars 'ffmpeg_infos_ext          ' "$ffmpeg_infos_ext";
    display-vars 'ffmpeg_infos_tmp_pathname ' "$ffmpeg_infos_tmp_pathname";
    display-vars 'ffmpeg_infos_container    ' "$ffmpeg_infos_container";
    display-vars 'ffmpeg_infos_cv           ' "$ffmpeg_infos_cv"         'ffmpeg_infos_ca'    "$ffmpeg_infos_ca";
    display-vars 'ffmpeg_infos_rtXY         ' "$ffmpeg_infos_rtXY"       'ffmpeg_infos_rtYX'  "$ffmpeg_infos_rtYX";
    display-vars 'ffmpeg_infos_definition   ' "$ffmpeg_infos_definition" 'ffmpeg_infos_definitionX' "$ffmpeg_infos_definitionX" 'ffmpeg_infos_definitionY' "$ffmpeg_infos_definitionY";

    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}


#function ffmpeg-infos-get( ffmpeg_in_pathname )
# Utilise et met à jour: ffmpeg-infos-*
# : ffmpeg-infos-*;
# ne doit pas être appellé directement. C'est ffmpeg-in-pathname() qui l'appelle
# Recherche les informations du media donné en parametre
function ffmpeg-infos-get(){
    #
    # exmple: ffmpeg-infos-get "film.avi";
    #
    local -i _fct_pile_app_level=5;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    #titre2 "${FUNCNAME[0]}()";
    # _src, priorité
    # 1- $1
    # 2 - ${PSP_params['src'] [ -n "${ffmpeg_cover_pathnames['front']}" ]
    # 3-  valeur de $ffmpeg_in_pathname
    local _src="${1:-""}";

    ffmpeg-infos-init;
    if [ "$_src" = '' ]
    then
        _src="${PSP_params['src']:-""}";
        if [ "$_src" = '' ]
        then
            _src="$ffmpeg_in_pathname";
        fi
    fi
    #display-vars '_src' "$_src";


    if [   -d "$_src" ]
    then
        fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;    # retour normal. Pas d'erreur
    fi
    if [ ! -f "$_src" ]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]}($@): fichier introuvable. Appellé par ${FUNCNAME[1]}";
        fct-pile-app-out $E_INODE_NOT_EXIST $_fct_pile_app_level; return $E_INODE_NOT_EXIST;
    fi

    # - Verifier la validité du container - #
    if ! ffmpeg-container-is_valide "${_src##*.}"; #extention
    then
        #notifs-pile-add-echo "${FUNCNAME[0]}($@): Container non valide";
        fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
    fi

    #notifs-pile-add-echo "${FUNCNAME[0]}($@): Container valide";
    local _tmp="$(wc -c < "${_src}")";      # display-vars "${FUNCNAME[0]}():$LINENO:_tmp" "$_tmp";
    ffmpeg_infos_sizeWC=${_tmp%% *};        # display-vars "${FUNCNAME[0]}():$LINENO:ffmpeg_infos_sizeWC" "$ffmpeg_infos_sizeWC";

    # https://www.baeldung.com/linux/bash-generate-uuid

    # - Affihcer la sortie brut - #
    #ffmpeg -i "$_src";

    # - Mise en cache de la sortie - #
    #titreCmd "ffmpeg -hide_banner  -i \"$_src\" 2>> \"$ffmpeg_infos_tmp_Pathname\"";
    ffmpeg -hide_banner  -i "$_src" 2>> "$ffmpeg_infos_tmp_Pathname";
    erreur-set;
    #erreur-no-description-echo;
    #cat "$ffmpeg_infos_tmp_Pathname";

    local _tmp='';

    # - durée - #
    _tmp="$(grep 'Duration:' < "$ffmpeg_infos_tmp_Pathname")";
    _tmp="${_tmp#*: }";                                 #display-vars '_tmp1'        "$_tmp";
    _tmp="${_tmp%%,*}";                                 #display-vars '_tmp2'        "$_tmp";
    ffmpeg_infos_duration="$_tmp";

    # - extraction du contrainer - #
    _tmp="$(grep "Input #0," < "$ffmpeg_infos_tmp_Pathname")";
    _tmp="${_tmp#*, }";                                 #display-vars '_tmp1'        "$_tmp";
    _tmp="${_tmp%%,*}";                                 #display-vars '_tmp2'        "$_tmp";
    ffmpeg_infos_container="$_tmp";

    # - extraction de de la definition - #
    _tmp="$(grep Video: < "$ffmpeg_infos_tmp_Pathname")";  #display-vars '_tmp'        "$_tmp";
    _tmp="${_tmp%b/s*}";                                #display-vars '_tmp'        "$_tmp";
    _tmp="${_tmp%,*}";                                  #display-vars '_tmp'        "$_tmp";
    _tmp="${_tmp% [*}";                                 #display-vars '_tmp'        "$_tmp";
    _tmp="${_tmp%, SAR*}";                              #display-vars '_tmp'        "$_tmp";  #SAR
    _tmp="${_tmp%, [SAR*}";                             #display-vars '_tmp'        "$_tmp";  #SAR

    _tmp="${_tmp##*, }";                                #display-vars '_tmp'        "$_tmp";
    ffmpeg_infos_definition="$_tmp";
    if [[ "$ffmpeg_infos_definition" =~ ' ' ]];then ffmpeg_infos_definition=''; fi;    # ffmpeg_infos_definition '_xffmpeg_infos_rtXYy' "$ffmpeg_infos_definition";


    #echo $LINENO":ffmpeg-setInfos-definitionX(${_tmp%x*})";
    ffmpeg-setInfos-definitionX "${_tmp%x*}";              #display-vars 'ffmpeg_infos_definitionX'        "$ffmpeg_infos_definitionX"

    #echo $LINENO":ffmpeg-setInfos-definitionY(${_tmp#*x})";
    ffmpeg-setInfos-definitionY "${_tmp#*x}";              #display-vars 'ffmpeg_infos_definitionY'        "$ffmpeg_infos_definitionY"

    # - calcul du ratio - #
    ffmpeg_infos_rtXY="$(echo "$ffmpeg_infos_definitionX / $ffmpeg_infos_definitionY" | bc -l)"
    if [[ "$ffmpeg_infos_rtXY" =~ ' ' ]];then ffmpeg_infos_rtXY=''; fi;    # display-vars '_xffmpeg_infos_rtXYy' "$ffmpeg_infos_rtXY";

    ffmpeg_infos_rtYX="$(echo "$ffmpeg_infos_definitionY / $ffmpeg_infos_definitionX" | bc -l)"
    if [[ "$ffmpeg_infos_rtYX" =~ ' ' ]];then ffmpeg_infos_rtYX=''; fi;    # display-vars 'ffmpeg_infos_rtYX' "$ffmpeg_infos_rtYX";

    # - extraction de fps - #
    _tmp="$(grep fps < "$ffmpeg_infos_tmp_Pathname")";  #display-vars '_tmp'        "$_tmp";
    _tmp="${_tmp% fps*}";                               #display-vars '_tmp'        "$_tmp";
    _tmp="${_tmp##*, }";                                #display-vars '_tmp'        "$_tmp";
    ffmpeg_infos_fps=$_tmp;

    # - extraction du codec video - #
    _tmp="$(grep Video < "$ffmpeg_infos_tmp_Pathname")";#display-vars '_tmp'        "$_tmp";
    _tmp="${_tmp##*Video: }";                           #display-vars '_tmp'        "$_tmp";
    _tmp="${_tmp%% (*}";                                #display-vars '_tmp'        "$_tmp";
    _tmp="${_tmp%%,*}";                                 #display-vars '_tmp'        "$_tmp";
    ffmpeg_infos_cv="${_tmp}";

    _tmp="$(grep Audio < "$ffmpeg_infos_tmp_Pathname")";#display-vars '_tmp'        "$_tmp";
    _tmp="${_tmp##*Audio: }";                           #display-vars '_tmp'        "$_tmp";
    _tmp="${_tmp%% (*}";                                #display-vars '_tmp'        "$_tmp";
    _tmp="${_tmp%%,*}";                                 #display-vars '_tmp'        "$_tmp";
    ffmpeg_infos_ca="${_tmp}";

    #ffmpeg_infos_display-var;
    #cat "$ffmpeg_infos_tmp_Pathname";

    # - Suppression du cache - #
    rm "$ffmpeg_infos_tmp_Pathname";

    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}


return 0;
