echo-d "${BASH_SOURCE[0]}" 1;
# gtt.sh --show-libsLevel2 apps/ffmpeg/ffmpeg.sh lib
# date de création    : 202?
# date de modification: 2024.08.24

############################
# objet: ffmpeg-infos-show #
############################
# Dépend de:
# *ffmpeg-infos-pathname 


# Affiche  les informations DU FICHIER donnée en parametre
# function      ffmpeg-infos-show(  _src | refresh:false [-type="colonne"]
# @_src= $1 | false | ${PSP_params['src']}
# @refresh: appel ffmpeg_pathname()
# test: clear; gtt.sh -d --libExecAuto apps/ffmpeg/tests/obj-ffmpeg_infos/test-fct-ffmpeg-infos-show 'test-fct-ffmpeg-infos-show' # - src=pathname
addLibrairie 'ffmpeg-infos-show' "- src | refresh:false [-type=\"colonne\"]";      # -R
function      ffmpeg-infos-show(){
    if $isTrapCAsk;then return $E_CTRL_C;fi

    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;


    # ffmpeg_infos_display-var;   # debug

    # - gestion des paramètres - #
        #local _inode="${1:-"$PWD"}";
        # _src, priorité
        # 1- $1
        # 2 - ${PSP_params['src'] 
        # 3-  valeur de $ffmpeg_in_pathname
        local _src="${1:-""}";
        local _type="${2:-""}";

        if [ "$_src" = false ]
        then
            ffmpeg_infos_refresh=false;     # ne pas reapeller getInfos()
            _src="$ffmpeg_infos_pathname";
        else
            ffmpeg_infos_refresh=true;
            if [ "$_src" = '' ]
            then
                _src="${PSP_params['src']:-""}";
                if [ "$_src" = '' ]
                then
                    _src="$ffmpeg_infos_pathname";
                fi
            fi
        fi
        if [ "$_src" = '' ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]} - _src | refresh:false [ -type='colonne']";
            fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
        fi

        #display-vars '_src' "$_src";

        # - type -#
        if [ "$_type" = ""  ]
        then
            _type="${PSP_params['type']:-""}";
            if [ "$_type" = '' ];then _type="colonne";fi
        fi
        #display-vars '_type' "$_type";
    #

    ffmpeg-infos-init;
    #ffmpeg-infos-show-recursif;
    ffmpeg-infos-show-code "$_src";
    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}



# Donne les infos pour un fichier sur une ligne
# si -v > 0 affiche également ffmpeg-streams-show
# dependance: ffmpeg-infos-pathname
function ffmpeg-infos-show-code(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    local _src="${1:-""}";
    ffmpeg_infos_show_type="${2:-"colonne"}";

    if [ "$_src" = "" ]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]}: _src='';"
        fct-pile-app-out $$E_ARG_BAD; return $E_ARG_BAD;
    fi

    if [ $_src != false ]         # si refresh  alors on recherche
    then
        #titre4 "${FUNCNAME[0]}($@):$LINENO: REFRESH";
        ffmpeg-infos-pathname "$_src";
    #else
    #   echo "${FUNCNAME[0]}:$LINENO: PAS de REFRESH.";
    fi

    ((ffmpeg_infos_show_fichierNb++))
    case "$ffmpeg_infos_show_type" in
        'colonne')  ffmpeg-infos-colonnes-init;     ;;
        #'html')o "<tr><td></td></tr>";
        #;;
    esac

    local _xy="";
    local _x_y="";
    local _format="";
    #ffmpeg_infos_display-var
    #if [ "$ffmpeg_infos_definitionX" = '' ]\
    #|| [ "$ffmpeg_infos_definitionY" = '' ]
    #then
        #titre4 "${FUNCNAME[0]}($@):$LINENO";
        _xy="${ffmpeg_infos_definitionX}/${ffmpeg_infos_definitionY}";
        _x_y="${ffmpeg_infos_definitionX}x${ffmpeg_infos_definitionY}";
        if [ "$_x_y" = 'x' ];then _x_y='';fi

        #_format="${ffmpeg_formatVideos["$_xy"]:-"$_x_y"}";
        _format="$_x_y";
    #fi
    if [ $debug_verbose_level -ge 1 ];then  echo "";fi
    case "$ffmpeg_infos_show_type" in
        'colonne')
            colonnes-datas-values "$ffmpeg_infos_name" "$ffmpeg_infos_container" "$ffmpeg_infos_sizeWC" "$ffmpeg_infos_duration" "$ffmpeg_infos_cv" "$_format" "$ffmpeg_infos_fps"  "$ffmpeg_infos_ca" "${ffmpeg_infos_rtXY:0:5}";
            colonnes-datas-show;
            ;;
        'html')
            echo "<tr><td>$ffmpeg_infos_name</td><td>$ffmpeg_infos_container</td><td>$ffmpeg_infos_sizeWC</td><td>$ffmpeg_infos_duration</td><td>$ffmpeg_infos_cv" "$_format" "$ffmpeg_infos_fps</td><td>$ffmpeg_infos_ca</td><td>${ffmpeg_infos_rtXY:0:5}</td></tr>";
            ;;
    esac

    if [ $debug_verbose_level -ge 1 ];then  ffmpeg-streams-show "$_src";fi
    #fi
    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}


function ffmpeg-infos-colonnes-init(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    colonnes-init;
    colonnes-lg               30         14        12        11        6     9       6    6     6;
    colonnes-dir              C          C          R        C         R     L       C    C     R;
    colonnes-sep  "|";
    colonnes-entetes-values 'Name'  'container' 'taille' 'duration'  'cv' 'Format' 'fps' 'ca' 'ratio';
    #colonnes-entetes-show; # L'afficvhage se fait dans le header

    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}

function ffmpeg-infos-show-header(){
    local -i _fct_pile_app_level=5;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    #titre4 "$_src";
    case "$ffmpeg_infos_show_type" in
        'colonne')
            #ffmpeg-infos-colonnes-init;    # fait par ffmpeg-infos-pathname
            echo "" >> "$colonnes_out";
            echo "$_src" >> "$colonnes_out";
            colonnes-entetes-show;
            ;;
        'html')
            echo '<table class="ffmpeg-infos-show">';
            echo '<thead><tr><th>nom</th><th>container</th><th>size</th><th>duration</th><th>cv" "format" "fps</th><th>ca</th><th>ratioXY</th></tr></thead>';
            echo '<tbody>';
        ;;
    esac
    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}


function ffmpeg-infos-show-body(){
    local -i _fct_pile_app_level=5;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    #display-vars ${FUNCNAME[0]}:$LINENO:'ffmpeg_infos_show_recursif_pathname' "$ffmpeg_infos_show_recursif_pathname";
    ffmpeg-infos-show-code "$ffmpeg_infos_show_recursif_pathname";
    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}

function      ffmpeg-infos-show-footer(){
    local -i _fct_pile_app_level=5;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    case "$ffmpeg_infos_show_type" in
        'colonne')
            #display-vars 'ffmpeg_infos_show_fichierNb' "$ffmpeg_infos_show_fichierNb";
            ;;
        'html')
            echo "<tr>${ffmpeg_infos_show_fichierNb} fichiers</tr>" >> "/dev/shm/medialistes.html";
            echo '</tbody></table>' >> "/dev/shm/medialistes.html";
        ;;
    esac
    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}

return 0;
