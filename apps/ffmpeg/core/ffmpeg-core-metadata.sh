echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh --show-libsLevel2 ffmpeg lib
# date de création    : 2024.09.07
# date de modification: 2024.09.29
# clear; gtt.sh -d --show-libsLevel2 apps/ffmpeg/tests/test-ffmpeg-core-metadata

# https://wiki.multimedia.cx/index.php/FFmpeg_Metadata
# Format Matroska
# https://www.matroska.org/index.html
# https://www.matroska.org/technical/tagging.html
# .mkv files (Matroska video)
# .mka files (Matroska audio)
# .mks files (subtitles)
# .mk3d files (stereoscopic/3D video)

# Metadata Fields        | MKV   | MP4   | MP3   | FLAC  | AVI   | MOV   | OGG  
    # -----------------------|-------|-------|-------|-------|-------|-------|-------
    # Album                  | No    | Yes   | Yes   | Yes   | No    | Yes   | Yes  
    # Album Artist           | No    | Yes   | Yes   | Yes   | No    | Yes   | Yes  
    # Album Art URL          | No    | Yes   | Yes   | Yes   | No    | Yes   | Yes  
    # Artist                 | Yes   | Yes   | Yes   | Yes   | Yes   | Yes   | Yes  
    # Aspect Ratio           | Yes   | Yes   | No    | No    | Yes   | Yes   | Yes  
    # Bitrate                | Yes   | Yes   | Yes   | Yes   | Yes   | Yes   | Yes  
    # Channels               | Yes   | Yes   | Yes   | Yes   | Yes   | Yes   | Yes  
    # Chapter                | Yes   | Yes   | No    | No    | No    | Yes   | No   
    # Comment                | Yes   | Yes   | Yes   | Yes   | Yes   | Yes   | Yes  
    # composer               | Yes   | Yes   | Yes   | Yes   | No    | Yes   | Yes  
    # conductor              | Yes   | Yes   | Yes   | Yes   | No    | Yes   | Yes  
    # Copyright              | Yes   | Yes   | Yes   | Yes   | Yes   | Yes   | Yes  
    # Date                   | Yes   | Yes   | Yes   | Yes   | Yes   | Yes   | Yes  
    # Director               | Yes   | Yes   | No    | No    | Yes   | Yes   | No   
    # Disc Number            | No    | Yes   | Yes   | Yes   | No    | Yes   | Yes  
    # Duration               | Yes   | Yes   | Yes   | Yes   | Yes   | Yes   | Yes  
    # Encoded By             | Yes   | Yes   | Yes   | Yes   | Yes   | Yes   | Yes  
    # Encoder                | Yes   | Yes   | Yes   | Yes   | Yes   | Yes   | Yes  
    # Frame Rate             | Yes   | Yes   | No    | No    | Yes   | Yes   | Yes  
    # Genre                  | Yes   | Yes   | Yes   | Yes   | Yes   | Yes   | Yes  
    # ISRC                   | No    | Yes   | Yes   | Yes   | No    | Yes   | Yes  
    # Language               | Yes   | Yes   | No    | No    | Yes   | Yes   | Yes  
    # Lyrics                 | No    | Yes   | Yes   | Yes   | No    | Yes   | Yes  
    # Performer              | Yes   | Yes   | Yes   | Yes   | No    | Yes   | Yes  
    # publisher              | Yes   | Yes   | Yes   | Yes   | Yes   | Yes   | Yes  
    # Sample Rate            | Yes   | Yes   | Yes   | Yes   | Yes   | Yes   | Yes  
    # Subtitle Codec         | Yes   | Yes   | No    | No    | Yes   | Yes   | Yes  
    # Subtitle Language      | Yes   | Yes   | No    | No    | Yes   | Yes   | Yes  
    # Subtitle Title         | Yes   | Yes   | No    | No    | Yes   | Yes   | Yes  
    # Title                  | Yes   | Yes   | Yes   | Yes   | Yes   | Yes   | Yes  
    # Track Gain             | Yes   | Yes   | Yes   | Yes   | No    | No    | Yes  
    # Track Number           | No    | Yes   | Yes   | Yes   | No    | Yes   | Yes  
    # Track Peak             | Yes   | Yes   | Yes   | Yes   | No    | No    | Yes  
    # Track Title            | Yes   | Yes   | Yes   | Yes   | Yes   | Yes   | Yes  
    # Video Resolution       | Yes   | Yes   | No    | No    | Yes   | Yes   | Yes  
    # Writer                 | Yes   | Yes   | No    | No    | Yes   | Yes   | No   
#

###############################
# - objet:ffmpeg_metadata-* - #
###############################

# - déclaration - #
    # - Fonctionnement - #
    declare -g  ffmpeg_metadata_del='';                 # ffmpeg-metadata-del-enable/ffmpeg-metadata-del-disable
 

    # -- langue -- #
    declare -g  ffmpeg_metadata_language='';

    # - Organization Information - #
    declare -gi ffmpeg_metadata_track_no=0;             # Entrée commune
    declare -gi ffmpeg_metadata_track_nb=0;             # Entrée commune
    
    declare -gi ffmpeg_metadata_part_number=0;          # mkv 
    declare -gi ffmpeg_metadata_part_offset=0;          # mkv 
    declare -gi ffmpeg_metadata_total_parts=0;          # mkv 

    declare -gi ffmpeg_metadata_chapters=0;

    # - Titles - #
    declare -ga ffmpeg_metadata_title=();               declare -g ffmpeg_metadata_title_str='';
    declare -g  ffmpeg_metadata_subtitle='';

    # - Entities - #
    declare -ga  ffmpeg_metadata_director=();           declare -g ffmpeg_metadata_director_str='';
    declare -ga  ffmpeg_metadata_composer=();           declare -g ffmpeg_metadata_composer_str='';
    declare -g   ffmpeg_metadata_composer_nationality=''; # 	UTF-8 	Nationality of the main composer of the item, mostly for classical music, in the Matroska countries form, i.e. [@!BCP47] two-letter region subtag, without the UK exception.

    declare -ga  ffmpeg_metadata_conductor=();          declare -g ffmpeg_metadata_conductor_str='';

    declare -ga  ffmpeg_metadata_written_by=();         declare -g ffmpeg_metadata_written_by='';
    declare -ga  ffmpeg_metadata_producer=();           declare -g ffmpeg_metadata_producer_str='';
    declare -ga  ffmpeg_metadata_distributed_by=();     declare -g ffmpeg_metadata_distributed_by_str='';
    declare -ga  ffmpeg_metadata_publisher=();          declare -g ffmpeg_metadata_publisher_str='';
    declare -ga  ffmpeg_metadata_label=();              declare -g ffmpeg_metadata_label_str='';
    declare -ga  ffmpeg_metadata_writer=();             declare -g ffmpeg_metadata_writer_str='';
    declare -ga  ffmpeg_metadata_artist=();             declare -g ffmpeg_metadata_artist_str='';
    declare -ga  ffmpeg_metadata_actor=();              declare -g ffmpeg_metadata_actor_str='';
    declare -ga  ffmpeg_metadata_character=();          declare -g ffmpeg_metadata_character_str='';
    declare -ga  ffmpeg_metadata_performer=();          declare -g ffmpeg_metadata_performer_str='';

    # Search and Classification - #
    declare -ga ffmpeg_metadata_genre=();               declare -g ffmpeg_metadata_genre_str='';
    declare -g  ffmpeg_metadata_album='';
    declare -g  ffmpeg_metadata_support='';	            # entrée commun pour original_media_type.
    declare -g  ffmpeg_metadata_original_media_type=''; # DVD, BluDisc, TV, Web
    declare -ga ffmpeg_metadata_content_type=();    # UTF-8 	The type of the item. e.g., Documentary, Feature Film, Cartoon, Music Video, Music, Sound FX

    declare -g  ffmpeg_metadata_subject='';
    declare -ga ffmpeg_metadata_keywords=();           declare -g ffmpeg_metadata_keywords_str='';
    declare -g  ffmpeg_metadata_summary='';
    declare -g  ffmpeg_metadata_synopsis='';
    declare -g  ffmpeg_metadata_initial_key='';     # UTF-8 	The initial key that a musical track starts in. The format is identical to “TKEY” tag in [@!ID3v2].

    # - Temporal Information - #
    declare -g  ffmpeg_metadata_period='';          # UTF-8 	Describes the period that the piece is from or about. For example, “Renaissance”.

    declare -gi ffmpeg_metadata_year='';            # YYYY
    declare -g  ffmpeg_metadata_date='';            # YYYY-MM-DD-hh-mm-ss

    declare -g  ffmpeg_metadata_date_released='';   # 	UTF-8 	The time that the item was originally released. This is akin to the “TDRL” tag in [@!ID3v2].
    declare -g  ffmpeg_metadata_DATE_RECORDED='';   # 	UTF-8 	The time that the recording began. This is akin to the “TDRC” tag in [@!ID3v2].
    declare -g  ffmpeg_metadata_DATE_ENCODED='';    # 	UTF-8 	The time that the encoding of this item was completed began. This is akin to the “TDEN” tag in [@!ID3v2].
    declare -g  ffmpeg_metadata_date_tagged='';     # 	UTF-8 	The time that the tags were done for this item. This is akin to the “TDTG” tag in [@!ID3v2].
    declare -g  ffmpeg_metadata_DATE_DIGITIZED='';  # 	UTF-8 	The time that the item was transferred to a digital medium. This is akin to the “IDIT” tag in [@?RIFF.tags].
    declare -g  ffmpeg_metadata_date_written='';    # 	UTF-8 	The time that the writing of the music/script began.
    declare -g  ffmpeg_metadata_DATE_PURCHASED='';  # 	UTF-8 	Information on when the file was purchased; see also (#commercial) on purchase tags.

    # - Spatial Information - #
    declare -g  ffmpeg_metadata_recording_location='';   # 	UTF-8 	The location where the item was recorded, in the Matroska countries form, i.e. [@!BCP47] two-letter region subtag, without the UK exception. This code is followed by a comma, then more detailed information such as state/province, another comma, and then city. For example, “US, Texas, Austin”. This will allow for easy sorting. It is okay to only store the country, or the country and the state/province. More detailed information can be added after the city through the use of additional commas. In cases where the province/state is unknown, but you want to store the city, simply leave a space between the two commas. For example, “US, , Austin”.
    declare -ga ffmpeg_metadata_composition_location=(); # 	UTF-8 	Location that the item was originally designed/written, in the Matroska countries form, i.e. [@!BCP47] two-letter region subtag, without the UK exception. This code is followed by a comma, then more detailed information such as state/province, another comma, and then city. For example, “US, Texas, Austin”. This will allow for easy sorting. It is okay to only store the country, or the country and the state/province. More detailed information can be added after the city through the use of additional commas. In cases where the province/state is unknown, but you want to store the city, simply leave a space between the two commas. For example, “US, , Austin”.

    declare -g  ffmpeg_metadata_ISRC='';            # 	UTF-8 	The International Standard Recording Code [@!ISRC], excluding the “ISRC” prefix and including hyphens.
    declare -g  ffmpeg_metadata_MCDI='';            # 	binary 	This is a binary dump of the TOC of the CDROM that this item was taken from. This hreads the same information as the “MCDI” in [@!ID3v2].
    declare -g  ffmpeg_metadata_ISBN='';            # 	UTF-8 	The International Standard Recording Code [@!ISRC], excluding the “ISRC” prefix and including hyphens.
    declare -g  ffmpeg_metadata_MCDI='';            # 	UTF-8 	International Standard Book Number [@!ISBN].
    declare -g  ffmpeg_metadata_BARCODE='';         # 	UTF-8 	European Article Numbering EAN-13 barcode defined in [@!GS1] General Specifications.
    declare -g  ffmpeg_metadata_CATALOG_NUMBER='';  # 	UTF-8 	A label-specific string used to identify the release – for example, TIC 01.
    declare -g  ffmpeg_metadata_LABEL_CODE='';      # 	UTF-8 	A 4-digit or 5-digit number to identify the record label, typically printed as (LC) xxxx or (LC) 0xxxx on CDs medias or covers (only the number is stored).
    declare -g  ffmpeg_metadata_LCCN='';            # 	UTF-8 	Library of Congress Control Number [@!LCCN].
    declare -g  ffmpeg_metadata_IMDB='';            # 	UTF-8 	Internet Movie Database [@!IMDb] identifier. “tt” followed by at least 7 digits for Movies, TV Shows, and Episodes.
    declare -g  ffmpeg_metadata_TMDB='';            # 	UTF-8 	The Movie DB “movie_id” or “tv_id” identifier for movies/TV shows [@!MovieDB]. The variable length digits string MUST be prefixed with either “movie/” or “tv/”.
    declare -g  ffmpeg_metadata_TVDB='';            # 	UTF-8 	The TV Database “Series ID” or “Episode ID” identifier for TV shows [@!TheTVDB]. Variable length all-digits string identifying a TV Show.
    declare -g  ffmpeg_metadata_TVDB2='';           # 	UTF-8 	The TV Database [@!TheTVDB] tag which can include movies. The variable length digits string representing a “Series ID”, “Episode ID” or “Movie ID” identifier MUST be prefixed with “series/”, “episodes/” or “movies/” respectively.


    # - comment/description - #
    #declare -gr  FFMPEG_METADATA_COMMENT_SEPARATOR=$'\n';
    declare -gr  FFMPEG_METADATA_COMMENT_SEPARATOR='¤';
    declare -gr  FFMPEG_METADATA_COMMENT_SEPARATOR_2='#';

    declare -gA  ffmpeg_metadata_url=();
    #declare -g  ffmpeg_metadata_description='';

    declare -g  ffmpeg_metadata_comment_user='';        # Commentaire ajouté par l'utilisateur

    declare -g  ffmpeg_metadata_comment_read='';
    declare -g  ffmpeg_metadata_comment='';
    declare -g  ffmpeg_metadata_comment_support='';     # DVD/BlueRay/TV/WEB
    declare -g  ffmpeg_metadata_comment_language='';    # 'truefrench'

    declare -g ffmpeg_metadata_comment_convert_video_in_codec='';
    declare -g ffmpeg_metadata_comment_convert_video_out_map='';
    declare -g ffmpeg_metadata_comment_convert_video_out_codec='';

    declare -g ffmpeg_metadata_comment_convert_audio_in_codec='';
    declare -g ffmpeg_metadata_comment_convert_audio_out_map='';
    declare -g ffmpeg_metadata_comment_convert_audio_out_codec='';

    declare -g ffmpeg_metadata_original_content_type="";    # documentary
    declare -g ffmpeg_metadata_str='';
#


function ffmpeg-metadata-init(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    ffmpeg_metadata_str='';               # Commande ffmpeg complète concerant les métadatas

    # -- UUID -- #
    ffmpeg-metadata-UUID;

    # -- langue -- #
    ffmpeg-metadata-language;

    # - Organization Information - #
    ffmpeg-metadata-track-no;
    ffmpeg-metadata-track-nb;
    ffmpeg-metadata-part_number;          # mkv 
    ffmpeg-metadata-part_offset;          # mkv 
    ffmpeg-metadata-total_parts;          # mkv 
    ffmpeg-metadata-chapters;             # mkv

    # - Titles - #
    ffmpeg-metadata-title;
    ffmpeg-metadata-subtitle;
    
    # - Entities - #
    ffmpeg-metadata-director;
    ffmpeg-metadata-composer;
    ffmpeg-metadata-composer_nationality;
    ffmpeg-metadata-conductor;
    ffmpeg-metadata-actor;
    ffmpeg-metadata-character;
    ffmpeg-metadata-written_by;
    ffmpeg-metadata-producer;
    ffmpeg-metadata-distributed_by;
    ffmpeg-metadata-publisher;
    ffmpeg-metadata-label;
    ffmpeg-metadata-writer;
    ffmpeg-metadata-artist;                 # display-vars 'ffmpeg_metadata_artist:nb' "${#ffmpeg_metadata_artist[@]}";
    ffmpeg-metadata-performer;

    # Search and Classification - #
    ffmpeg-metadata-genre;
    ffmpeg-metadata-album;
    ffmpeg-metadata-support;	            # UTF-8 	DVD, Blu-ray, BluDisc
    ffmpeg-metadata-original_media_type;
    ffmpeg-metadata-content_type;           # UTF-8 	The type of the item. e.g., Documentary, Feature Film, Cartoon, Music Video, Music, Sound FX
    ffmpeg-metadata-subject;
    ffmpeg-metadata-keywords;
    ffmpeg-metadata-summary;
    ffmpeg-metadata-synopsis;
    ffmpeg-metadata-initial_key;

    # - Temporal Information - #
    ffmpeg-metadata-period;                 # UTF-8 	Describes the period that the piece is from or about. For example, “Renaissance”.

    ffmpeg-metadata-year;                   # YYYY
    ffmpeg-metadata-date;                   # YYYY-MM-DD-hh-mm-ss

    #ffmpeg-metadata-date_released;
    #ffmpeg-metadata-DATE_RECORDED;
    #ffmpeg-metadata-DATE_ENCODED;
    ffmpeg-metadata-date_tagged;
    #ffmpeg-metadata-DATE_DIGITIZED;
    ffmpeg-metadata-date_written;
    #ffmpeg-metadata-DATE_PURCHASED;

    # - Spatial Information - #
    #ffmpeg-metadata-recording_location;
    ffmpeg-metadata-composition_location;

    # - Identifiers - #
    ffmpeg-metadata-ISRC;
    ffmpeg-metadata-MCDI;
    ffmpeg-metadata-ISBN;
    ffmpeg-metadata-BARCODE;
    ffmpeg-metadata-CATALOG_NUMBER;
    ffmpeg-metadata-LABEL_CODE;
    ffmpeg-metadata-LCCN;
    ffmpeg-metadata-IMDB;
    ffmpeg-metadata-TMDB;
    ffmpeg-metadata-TVDB;
    ffmpeg-metadata-TVDB2;

    # - Comment/description - #
    ffmpeg-metadata-url;

    ffmpeg-metadata-comment-user;
    #ffmpeg-metadata-description;
    #ffmpeg-metadata-comment-read;          # lit dans un fichier, ne pas utiliser ici. La fonction est automatiquement appellé par -calc()
    ffmpeg-metadata-comment;                # Commentaire/description
    ffmpeg-metadata-comment-language;
    #ffmpeg-metadata-comment-support;       # Support d'origine: DVD,BlueRay,etc

    ffmpeg_metadata_comment_convert_video_in_codec='';
    ffmpeg_metadata_comment_convert_video_out_map='';
    ffmpeg_metadata_comment_convert_video_out_codec='';

    ffmpeg_metadata_comment_convert_audio_in_codec='';
    ffmpeg_metadata_comment_convert_audio_out_map='';
    ffmpeg_metadata_comment_convert_audio_out_codec='';


    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

function ffmpeg-metadata-display-vars(){
    
    display-vars 'ffmpeg_metadata_del                ' "$ffmpeg_metadata_del";
    display-vars 'ffmpeg_metadata_UUID               ' "$ffmpeg_metadata_UUID";

    titre4 '# - Language - #';
    display-vars 'ffmpeg_metadata_language           ' "$ffmpeg_metadata_language";

    titre4 '# - Organization Information - #';
    display-vars 'ffmpeg_metadata_track_no           ' "$ffmpeg_metadata_track_no"     'ffmpeg_metadata_track_nb        ' "$ffmpeg_metadata_track_nb";
    display-vars 'ffmpeg_metadata_total_parts        ' "$ffmpeg_metadata_total_parts"  'ffmpeg_metadata_part_number     ' "$ffmpeg_metadata_part_number"  'ffmpeg_metadata_part_offset        ' "$ffmpeg_metadata_part_offset";
    display-vars 'ffmpeg_metadata_chapters           ' "$ffmpeg_metadata_chapters";

    titre4 '# - Titles - #';
    display-tableau-a 'ffmpeg_metadata_title         ' "${ffmpeg_metadata_title[@]}";           display-vars 'ffmpeg_metadata_title_str         ' "$ffmpeg_metadata_title_str";
    display-vars      'ffmpeg_metadata_subtitle      ' "$ffmpeg_metadata_subtitle";

    titre4 '# - Entities - #';
    display-tableau-a 'ffmpeg_metadata_director      ' "${ffmpeg_metadata_director[@]}";        display-vars 'ffmpeg_metadata_director_str      ' "$ffmpeg_metadata_director_str";
    display-tableau-a 'ffmpeg_metadata_composer      ' "${ffmpeg_metadata_composer[@]}";        display-vars 'ffmpeg_metadata_composer_str      ' "$ffmpeg_metadata_composer_str";
    display-vars      'ffmpeg-metadata-composer-nationality' "${ffmpeg_metadata_composer_nationality}";

    display-tableau-a 'ffmpeg_metadata_conductor     ' "${ffmpeg_metadata_conductor[@]}";       display-vars 'ffmpeg_metadata_conductor_str     ' "$ffmpeg_metadata_conductor_str";
    display-tableau-a 'ffmpeg_metadata_writer        ' "${ffmpeg_metadata_writer[@]}";          display-vars 'ffmpeg_metadata_writer_str        ' "$ffmpeg_metadata_writer_str";
    display-tableau-a 'ffmpeg_metadata_actor         ' "${ffmpeg_metadata_actor[@]}";           display-vars 'ffmpeg_metadata_actor_str         ' "$ffmpeg_metadata_actor_str";
    display-tableau-a 'ffmpeg_metadata_character     ' "${ffmpeg_metadata_character[@]}";       display-vars 'ffmpeg_metadata_character_str     ' "$ffmpeg_metadata_character_str";
    display-tableau-a 'ffmpeg_metadata_written_by    ' "${ffmpeg_metadata_written_by[@]}";      display-vars 'ffmpeg_metadata_written_by_str    ' "$ffmpeg_metadata_written_by_str";
    display-tableau-a 'ffmpeg_metadata_performer     ' "${ffmpeg_metadata_performer[@]}";       display-vars 'ffmpeg_metadata_performer_str     ' "$ffmpeg_metadata_performer_str";
    display-tableau-a 'ffmpeg_metadata_producer      ' "${ffmpeg_metadata_producer[@]}";        display-vars 'ffmpeg_metadata_producer          ' "$ffmpeg_metadata_producer_str";
    display-tableau-a 'ffmpeg_metadata_distributed_by' "${ffmpeg_metadata_distributed_by[@]}";  display-vars 'ffmpeg_metadata_distributed_by_str' "$ffmpeg_metadata_distributed_by_str";
    display-tableau-a 'ffmpeg_metadata_publisher     ' "${ffmpeg_metadata_publisher[@]}";       display-vars 'ffmpeg_metadata_publisher_str     ' "$ffmpeg_metadata_publisher_str";
    display-tableau-a 'ffmpeg_metadata_label         ' "${ffmpeg_metadata_label[@]}";           display-vars 'ffmpeg_metadata_label_str         ' "$ffmpeg_metadata_label_str";
    display-tableau-a 'ffmpeg_metadata_artist        ' "${ffmpeg_metadata_artist[@]}";          display-vars 'ffmpeg_metadata_artist_str        ' "$ffmpeg_metadata_artist_str";
    display-tableau-a 'ffmpeg_metadata_performer     ' "${ffmpeg_metadata_performer[@]}";       display-vars 'ffmpeg_metadata_performer_str     ' "$ffmpeg_metadata_performer_str";

    titre4 '# - Search and Classification - #';
    display-vars 'ffmpeg_metadata_genre              ' "${ffmpeg_metadata_genre[@]}";           display-vars 'ffmpeg_metadata_genre_str         ' "$ffmpeg_metadata_genre_str";
    display-vars 'ffmpeg_metadata_album              ' "$ffmpeg_metadata_album";                display-vars 'ffmpeg_metadata_support_str       ' "$ffmpeg_metadata_support";
    display-vars 'ffmpeg_metadata_original_media_type' "$ffmpeg_metadata_original_media_type";
    display-vars  'ffmpeg_metadata_subject           ' "$ffmpeg_metadata_subject";
    display-vars 'ffmpeg_metadata_keywords           ' "${ffmpeg_metadata_keywords[@]}";        display-vars 'ffmpeg_metadata_keywords_str      ' "$ffmpeg_metadata_keywords_str";
    display-vars 'ffmpeg_metadata_summary            ' "$ffmpeg_metadata_summary";
    display-vars 'ffmpeg_metadata_synopsis           ' "$ffmpeg_metadata_synopsis";
    display-vars 'ffmpeg_metadata_initial_key        ' "$ffmpeg_metadata_initial_key";
    display-tableau-a 'ffmpeg_metadata_url           ' "${ffmpeg_metadata_url[@]}";             display-vars 'ffmpeg_metadata_url_str           ' "$ffmpeg_metadata_url_str";

    titre4 '# - Temporal Information - #';
    display-vars 'ffmpeg_metadata_period             ' "$ffmpeg_metadata_period";
    display-vars 'ffmpeg_metadata_year               ' "$ffmpeg_metadata_year"     'ffmpeg_metadata_date            ' "$ffmpeg_metadata_date";
    display-vars 'ffmpeg_metadata_date_tagged        ' "$ffmpeg_metadata_date_tagged";
    display-vars 'ffmpeg_metadata_date_written       ' "$ffmpeg_metadata_date_written";

    titre4 '# - Spatial Information - #';
    display-vars 'ffmpeg_metadata_composition_location' "${ffmpeg_metadata_composition_location[@]}";

    titre4 '# - Identifiers - #';
    display-vars 'ffmpeg_metadata_ISRC   ' "$ffmpeg_metadata_ISRC";
    display-vars 'ffmpeg_metadata_MCDI   ' "$ffmpeg_metadata_MCDI";
    display-vars 'ffmpeg_metadata_ISBN   ' "$ffmpeg_metadata_ISBN";
    display-vars 'ffmpeg_metadata_BARCODE' "$ffmpeg_metadata_BARCODE";
    display-vars 'ffmpeg_metadata_CATALOG_NUMBER' "$ffmpeg_metadata_CATALOG_NUMBER";
    display-vars 'ffmpeg_metadata_LABEL_CODE' "$ffmpeg_metadata_LABEL_CODE";
    display-vars 'ffmpeg_metadata_LCCN   ' "$ffmpeg_metadata_LCCN";
    display-vars 'ffmpeg_metadata_IMDB   ' "$ffmpeg_metadata_IMDB";
    display-vars 'ffmpeg_metadata_TMDB   ' "$ffmpeg_metadata_TMDB";
    display-vars 'ffmpeg_metadata_TVDB   ' "$ffmpeg_metadata_TVDB";
    display-vars 'ffmpeg_metadata_TVDB2  ' "$ffmpeg_metadata_TVDB2";

    titre4 '# - Comment/description - #';
    #display-vars 'ffmpeg_metadata_description        ' "$ffmpeg_metadata_description";

    display-vars 'ffmpeg_metadata_comment_read       ' "$ffmpeg_metadata_comment_read";
    display-vars 'ffmpeg_metadata_comment_user       ' "$ffmpeg_metadata_comment_user";
    #display-vars 'ffmpeg_metadata_comment            ' "$ffmpeg_metadata_comment";
    ffmpeg-metadata-comment-show;
    display-vars 'ffmpeg_metadata_comment_support    ' "$ffmpeg_metadata_comment_support";
    display-vars 'ffmpeg_metadata_comment_language   ' "$ffmpeg_metadata_comment_language";

    display-vars 'ffmpeg_metadata_comment_convert_video_in_codec ' "$ffmpeg_metadata_comment_convert_video_in_codec"\
                 'ffmpeg_metadata_comment_convert_video_out_map  ' "$ffmpeg_metadata_comment_convert_video_out_map"\
                 'ffmpeg_metadata_comment_convert_video_out_codec' "$ffmpeg_metadata_comment_convert_video_out_codec";

    display-vars 'ffmpeg_metadata_comment_convert_audio_in_codec ' "$ffmpeg_metadata_comment_convert_audio_in_codec"\
                 'ffmpeg_metadata_comment_convert_audio_out_map  ' "$ffmpeg_metadata_comment_convert_audio_out_map"\
                 'ffmpeg_metadata_comment_convert_audio_out_codec' "$ffmpeg_metadata_comment_convert_audio_out_codec";

    if [ $debug_verbose_level -gt 0 ]; then display-vars 'ffmpeg_metadata_str             ' "$ffmpeg_metadata_str"; fi
    return 0;
}


########################
# ffmpeg-metadata-UUID #
########################
    declare -g  ffmpeg_metadata_UUID='';                # 1 par fichier; Ecrit dans le metadata commentaire
    declare -g  ffmpeg_metadata_UUID_read='';

    function ffmpeg-metadata-UUID()     { ffmpeg_metadata_UUID="${1:-""}"; }
    function ffmpeg-metadata-generate() { ffmpeg_metadata_UUID="$(cat /proc/sys/kernel/random/uuid)"; }

    # Charge l'UUID, garantie que ffmpeg_metadata_UUID existe avec une valeur hérité ou une nouvelle valeur
    function      ffmpeg-metadata-UUID-load(){

        ffmpeg_metadata_UUID=''; 
        ffmpeg_metadata_UUID_read=''; 

        # - Rechercher dans une conversion déjà généré ou dans le cas d'une -apply (in==out) - #
        ffmpeg-metadata-UUID-read "$ffmpeg_out_pathname";
        if [ -n "$ffmpeg_metadata_UUID_read" ]
        then
            ffmpeg_metadata_UUID="$ffmpeg_metadata_UUID_read";
        else
            # - Rechercher dans le fichier source - #
            ffmpeg-metadata-UUID-read "$ffmpeg_in_pathname";
            if [ -n "$ffmpeg_metadata_UUID_read" ]
            then
                ffmpeg_metadata_UUID="$ffmpeg_metadata_UUID_read";
            else
                ffmpeg-metadata-generate;
                display-vars 'ffmpeg_metadata_UUID(generate)' "$ffmpeg_metadata_UUID";
            fi
        fi
        display-vars 'ffmpeg_metadata_UUID_read(load)' "$ffmpeg_metadata_UUID_read";
        display-vars 'ffmpeg_metadata_UUID(load)' "$ffmpeg_metadata_UUID";
    }

    #Lire le tag UUID dans le fichier donné en argument
    addLibrairie 'ffmpeg-metadata-UUID-read' "- src. Recherche l'UUID dans le tag UUID ou dans le bloc comment" ;
    function      ffmpeg-metadata-UUID-read(){
        # _src, priorité
        # 1- $1
        # 2 - ${PSP_params['src'] 
        # 3-  valeur de $ffmpeg_in_pathname
        local _src="${1:-""}";
        if [ -z "$_src" ]
        then
            _src="${PSP_params['src']:-""}";
            if [ -z "$_src" ]
            then
                _src="$ffmpeg_in_pathname";
                if [ -z "$_src" ]; then return $E_ARG_REQUIRE; fi
            fi
        fi
        #display-vars '_src' "$_src";
        if [ ! -f "$_src" ];    then return $E_INODE_NOT_EXIST;  fi 
        
        #local _src="$ffmpeg_in_pathname";   # Par défaut: methode: exec
        #if [ "$ffmpeg_methode" = 'metadata_apply' ]; then _src="$ffmpeg_out_pathname"; fi

        ffmpeg_metadata_UUID_read='';

        # - Vérifier la présence du TAG UUID - # 
        ffmpeg_metadata_UUID_read="$(ffprobe -v quiet -show_format "$_src" | grep -i "TAG:UUID")";
        #display-vars 'ffmpeg_metadata_UUID_read(read:TAG UUID)' "$ffmpeg_metadata_UUID_read";
        if [ -n "$ffmpeg_metadata_UUID_read" ]
        then
            ffmpeg_metadata_UUID_read="${ffmpeg_metadata_UUID_read#*UUID=}";
            display-vars "ffmpeg_metadata_UUID_read($_src:TAG UUID)" "$ffmpeg_metadata_UUID_read";
            return $E_TRUE;
        fi


        # - Rechercher dans le bloc comment  - # 
        ffmpeg_metadata_UUID_read="$(ffprobe -v quiet -show_format "$_src" | grep -i "TAG:COMMENT")";
        if [ -n "$ffmpeg_metadata_UUID_read" ]
        then
            if [[  "$ffmpeg_metadata_UUID_read" == *"UUID"* ]]
            then
                ffmpeg_metadata_UUID_read="${ffmpeg_metadata_UUID_read#*UUID=}";
                ffmpeg_metadata_UUID_read="${ffmpeg_metadata_UUID_read:0:36}";
                display-vars "ffmpeg_metadata_UUID_read($_src:comment)" "$ffmpeg_metadata_UUID_read";
                return $E_TRUE;
            fi

        fi
        # - Rechercher dans le bloc DESCRIPTION  - # 
        ffmpeg_metadata_UUID_read="$(ffprobe -v quiet -show_format "$_src" | grep -i "TAG:DESCRIPTION")";
        if [ -n "$ffmpeg_metadata_UUID_read" ]
        then
            if [[  "$ffmpeg_metadata_UUID_read" == *"UUID"* ]]
            then
                ffmpeg_metadata_UUID_read="${ffmpeg_metadata_UUID_read#*UUID=}";
                ffmpeg_metadata_UUID_read="${ffmpeg_metadata_UUID_read:0:36}";
                display-vars "ffmpeg_metadata_UUID_read($_src:DESCRIPTION)" "$ffmpeg_metadata_UUID_read";
                return $E_TRUE;
            fi
        fi
        return $E_FALSE;
    }

    # Inserre ;le tag dans le fichier (fonction autonome)
    # fonction NON terminée
    # clear; gtt.sh ffmpeg ffmpeg-metadata-UUID-insert - src=... 
    addLibrairie 'ffmpeg-metadata-UUID-insert' "- src=ffmpeg_in_pathname"
    function      ffmpeg-metadata-UUID-insert(){

        titre3 "${FUNCNAME[0]}";
        # _src, priorité
        # 1- $1
        # 2 - ${PSP_params['src'] 
        # 3-  valeur de $ffmpeg_in_pathname
        local _src="${1:-""}";
        if [ -z "$_src" ]
        then
            _src="${PSP_params['src']:-""}";
            if [ -z "$_src" ]
            then
                _src="$ffmpeg_in_pathname";
                if [ -z "$_src" ]; then return $E_ARG_REQUIRE; fi
            fi
        fi
        #display-vars '_src' "$_src";
        if [ ! -f "$_src" ];    then return $E_INODE_NOT_EXIST;  fi 
        src-pathname "$_src";

        ffmpeg-metadata-UUID-read "$src_pathname";
        if [ -n "$ffmpeg_metadata_UUID_read" ]
        then
            echo "$src_pathname: UUID détécté: $ffmpeg_metadata_UUID_read";
            return $E_TRUE;
        fi  

        local _UUID="$(cat /proc/sys/kernel/random/uuid)";
        display-vars '$_UUID' "$_UUID";

        # - spliter la source et créer une dest temporaire - #
        dest-pathname "$src_rep/${src_nom}-UUID.$src_ext";
        # -  - #
        ffmpeg -y -hide_banner -i "$_src" -codec: copy -metadata UUID="$_UUID" -metadata comment="UUID=$_UUID"  "$dest_pathname" 1>/dev/null;
        erreur_no=$?;
        if [ $erreur_no -ne 0 ]; then return $E_FALSE; fi
        rm "$src_pathname";
        mv "$dest_pathname" "$src_pathname";
        return $E_TRUE;
    }
#

#######################
# ffmpeg: hydratation #
#######################

    function ffmpeg-metadata-del-enable()  { ffmpeg_metadata_del=' -map_metadata -1'; }
    function ffmpeg-metadata-del-disable() { ffmpeg_metadata_del=''; }
    
    # -- langue -- #
    function ffmpeg-metadata-language()     { ffmpeg_metadata_language="${1:-""}";     }

    # - Organization Information - #
    function ffmpeg-metadata-track-no()     { ffmpeg_metadata_track_no=${1:-0}; ffmpeg-metadata-total_parts ${1:-0}; }
    function ffmpeg-metadata-track-nb()     { ffmpeg_metadata_track_nb=${1:-0}; ffmpeg-metadata-part_number ${1:-0}; }
    function ffmpeg-metadata-track-no-nb()  {
        ffmpeg_metadata_track_no=${1:-0}; ffmpeg-metadata-total_parts ${1:-0};
        ffmpeg_metadata_track_nb=${2:-0}; ffmpeg-metadata-part_number ${2:-0};
        }

    function ffmpeg-metadata-total_parts()  { ffmpeg_metadata_total_parts=${1:-0}; }
    function ffmpeg-metadata-part_number()  { ffmpeg_metadata_part_number=${1:-0}; }
    function ffmpeg-metadata-part_offset()  { ffmpeg_metadata_part_offset=${1:-0}; }
    function ffmpeg-metadata-chapters()     { ffmpeg_metadata_chapters=${1:-0}; }

    # - Titles - #
    function ffmpeg-metadata-title()        {
        ffmpeg_metadata_title=(); ffmpeg_metadata_title_str='';
        while [ $# -ne 0 ]; do
            if [ -z "${1:-""}" ];then return 0;fi
            ffmpeg_metadata_title[${#ffmpeg_metadata_title[@]}]="${1}";     shift;
        done;
    }
    function ffmpeg-metadata-title-add()    {
        if [ -z "${1:-""}" ];then return 0;fi
        while [ $# -ne 0 ]; do ffmpeg_metadata_title[${#ffmpeg_metadata_title[@]}]="${1}";  shift;  done;
    }

    function ffmpeg-metadata-subtitle()     { ffmpeg_metadata_subtitle="${1:-""}";}

    # - Entities - #
    function ffmpeg-metadata-director()        {
        ffmpeg_metadata_director=(); ffmpeg_metadata_director_str='';
        while [ $# -ne 0 ]; do
            if [ -z "${1:-""}" ];then return 0;fi
            ffmpeg_metadata_director[${#ffmpeg_metadata_director[@]}]="${1}";     shift;
        done;
    }
    function ffmpeg-metadata-director-add()    {
        if [ -z "${1:-""}" ];then return 0;fi
        while [ $# -ne 0 ]; do ffmpeg_metadata_director[${#ffmpeg_metadata_director[@]}]="${1}";  shift;  done;
    }

    function ffmpeg-metadata-composer()        {
        ffmpeg_metadata_composer=(); ffmpeg_metadata_composer_str='';
        while [ $# -ne 0 ]; do
            if [ -z "${1:-""}" ];then return 0; fi
            ffmpeg_metadata_composer[${#ffmpeg_metadata_composer[@]}]="${1}";     shift;
        done;
    }
    function ffmpeg-metadata-composer-add()    {
        if [ -z "${1:-""}" ];then return 0;fi
        while [ $# -ne 0 ]; do ffmpeg_metadata_composer[${#ffmpeg_metadata_composer[@]}]="${1}";  shift;  done;
    }

    function ffmpeg-metadata-composer_nationality()        {
        composer_nationality=(); composer_nationality_str='';
        while [ $# -ne 0 ]; do
            if [ -z "${1:-""}" ];then return 0; fi
            composer_nationality[${#composer_nationality[@]}]="${1}";     shift;
        done;
    }
    function ffmpeg-metadata-composer_nationality-add()    {
        if [ -z "${1:-""}" ];then return 0;fi
        while [ $# -ne 0 ]; do composer_nationality[${#composer_nationality[@]}]="${1}";  shift;  done;
    }

    function ffmpeg-metadata-conductor() {
        ffmpeg_metadata_conductor=(); ffmpeg_metadata_conductor_str='';
        while [ $# -ne 0 ]; do
            if [ -z "${1:-""}" ];then return 0; fi
            ffmpeg_metadata_conductor[${#ffmpeg_metadata_conductor[@]}]="${1}";     shift;
        done;
    }
    function ffmpeg-metadata-conductor-add()    {
        if [ -z "${1:-""}" ];then return 0;fi
        while [ $# -ne 0 ]; do ffmpeg_metadata_conductor[${#ffmpeg_metadata_conductor[@]}]="${1}";  shift;  done;
    }

    function ffmpeg-metadata-actor() {
        ffmpeg_metadata_actor=(); ffmpeg_metadata_actor_str='';
        while [ $# -ne 0 ]; do
            if [ -z "${1:-""}" ];then return 0; fi
            ffmpeg_metadata_actor[${#ffmpeg_metadata_actor[@]}]="${1}";     shift;
        done;
    }
    function ffmpeg-metadata-actor-add()    {
        if [ -z "${1:-""}" ];then return 0;fi
        while [ $# -ne 0 ]; do ffmpeg_metadata_actor[${#ffmpeg_metadata_actor[@]}]="${1}";  shift;  done;
    }

    function ffmpeg-metadata-character() {
        ffmpeg_metadata_character=(); ffmpeg_metadata_character_str='';
        while [ $# -ne 0 ]; do
            if [ -z "${1:-""}" ];then return 0; fi
            ffmpeg_metadata_character[${#ffmpeg_metadata_character[@]}]="${1}";     shift;
        done;
    }
    function ffmpeg-metadata-character-add()    {
        if [ -z "${1:-""}" ];then return 0;fi
        while [ $# -ne 0 ]; do ffmpeg_metadata_character[${#ffmpeg_metadata_character[@]}]="${1}";  shift;  done;
    }

    function ffmpeg-metadata-written_by() {
        ffmpeg_metadata_written_by=(); ffmpeg_metadata_written_by_str='';
        while [ $# -ne 0 ]; do
            if [ -z "${1:-""}" ];then return 0; fi
            ffmpeg_metadata_written_by[${#ffmpeg_metadata_written_by[@]}]="${1}";     shift;
        done;
    }
    function ffmpeg-metadata-written_by-add()    {
        if [ -z "${1:-""}" ];then return 0;fi
        while [ $# -ne 0 ]; do ffmpeg_metadata_written_by[${#ffmpeg_metadata_written_by[@]}]="${1}";  shift;  done;
    }

    function ffmpeg-metadata-producer() {
        ffmpeg_metadata_producer=(); ffmpeg_metadata_producer_str='';
        while [ $# -ne 0 ]; do
            if [ -z "${1:-""}" ];then return 0; fi
            ffmpeg_metadata_producer[${#ffmpeg_metadata_producer[@]}]="${1}";     shift;
        done;
    }
    function ffmpeg-metadata-producer-add()    {
        if [ -z "${1:-""}" ];then return 0;fi
        while [ $# -ne 0 ]; do ffmpeg_metadata_producer[${#ffmpeg_metadata_producer[@]}]="${1}";  shift;  done;
    }

    function ffmpeg-metadata-distributed_by() {
        ffmpeg_metadata_distributed_by=(); ffmpeg_metadata_distributed_by_str='';
        while [ $# -ne 0 ]; do
            if [ -z "${1:-""}" ];then return 0; fi
            ffmpeg_metadata_distributed_by[${#ffmpeg_metadata_distributed_by[@]}]="${1}";     shift;
        done;
    }
    function ffmpeg-metadata-distributed_by-add()    {
        if [ -z "${1:-""}" ];then return 0;fi
        while [ $# -ne 0 ]; do ffmpeg_metadata_distributed_by[${#ffmpeg_metadata_distributed_by[@]}]="${1}";  shift;  done;
    }

    function ffmpeg-metadata-publisher() {
        ffmpeg_metadata_publisher=(); ffmpeg_metadata_publisher_str='';
        while [ $# -ne 0 ]; do
            if [ -z "${1:-""}" ];then return 0; fi
            ffmpeg_metadata_publisher[${#ffmpeg_metadata_publisher[@]}]="${1}";     shift;
        done;
    }
    function ffmpeg-metadata-publisher-add()    {
        if [ -z "${1:-""}" ];then return 0;fi
        while [ $# -ne 0 ]; do ffmpeg_metadata_publisher[${#ffmpeg_metadata_publisher[@]}]="${1}";  shift;  done;
    }

    function ffmpeg-metadata-label() {
        ffmpeg_metadata_label=(); ffmpeg_metadata_label_str='';
        while [ $# -ne 0 ]; do
            if [ -z "${1:-""}" ];then return 0; fi
            ffmpeg_metadata_label[${#ffmpeg_metadata_label[@]}]="${1}";     shift;
        done;
    }
    function ffmpeg-metadata-label-add()    {
        if [ -z "${1:-""}" ];then return 0;fi
        while [ $# -ne 0 ]; do ffmpeg_metadata_label[${#ffmpeg_metadata_label[@]}]="${1}";  shift;  done;
    }

    function ffmpeg-metadata-writer() {
        ffmpeg_metadata_writer=(); ffmpeg_metadata_writer_str='';
        while [ $# -ne 0 ]; do
            if [ -z "${1:-""}" ];then return 0; fi
            ffmpeg_metadata_writer[${#ffmpeg_metadata_writer[@]}]="${1}";     shift;
        done;
    }
    function ffmpeg-metadata-writer-add()    {
        if [ -z "${1:-""}" ];then return 0;fi
        while [ $# -ne 0 ]; do ffmpeg_metadata_writer[${#ffmpeg_metadata_writer[@]}]="${1}";  shift;  done;
    }

    function ffmpeg-metadata-artist() {
        ffmpeg_metadata_artist=(); ffmpeg_metadata_artist_str='';
        while [ $# -ne 0 ]; do
            if [ -z "${1:-""}" ];then return 0; fi
            ffmpeg_metadata_artist[${#ffmpeg_metadata_artist[@]}]="${1}";     shift;
        done;
    }
    function ffmpeg-metadata-artist-add()    {
        if [ -z "${1:-""}" ];then return 0;fi
        while [ $# -ne 0 ]; do ffmpeg_metadata_artist[${#ffmpeg_metadata_artist[@]}]="${1}";  shift;  done;
    }

    function ffmpeg-metadata-performer() {
        ffmpeg_metadata_performer=(); ffmpeg_metadata_performer_str='';
        while [ $# -ne 0 ]; do
            if [ -z "${1:-""}" ];then return 0; fi
            ffmpeg_metadata_performer[${#ffmpeg_metadata_performer[@]}]="${1}";     shift;
        done;
    }
    function ffmpeg-metadata-performer-add()    {
        if [ -z "${1:-""}" ];then return 0;fi
        while [ $# -ne 0 ]; do ffmpeg_metadata_performer[${#ffmpeg_metadata_performer[@]}]="${1}";  shift;  done;
    }                                                                   

    # Search and Classification - #
    function ffmpeg-metadata-genre() {
        ffmpeg_metadata_genre=(); ffmpeg_metadata_genre_str='';
        while [ $# -ne 0 ]; do
            if [ -z "${1:-""}" ];then return 0; fi
            ffmpeg_metadata_genre[${#ffmpeg_metadata_genre[@]}]="${1}";     shift;
        done;
    }
    function ffmpeg-metadata-genre-add()    {
        if [ -z "${1:-""}" ];then return 0;fi
        while [ $# -ne 0 ]; do ffmpeg_metadata_genre[${#ffmpeg_metadata_genre[@]}]="${1}";  shift;  done;
    }                                                                   

    function ffmpeg-metadata-album()        { ffmpeg_metadata_album="${1:-""}";         }
    function ffmpeg-metadata-support()      { ffmpeg_metadata_support="${1:-""}";
                                              ffmpeg-metadata-original_media_type "${1:-""}";
    }
    function ffmpeg-metadata-original_media_type() { ffmpeg_metadata_original_media_type="${1:-""}";     }  # Utiliser support
    function ffmpeg-metadata-content_type() { ffmpeg_metadata_original_content_type="${1:-""}";     }

    function ffmpeg-metadata-subject()      { ffmpeg_metadata_subject="${1:-""}";       }

    function ffmpeg-metadata-keywords() {
        ffmpeg_metadata_keywords=(); ffmpeg_metadata_keywords_str='';
        while [ $# -ne 0 ]; do
            if [ -z "${1:-""}" ];then return 0; fi
            ffmpeg_metadata_keywords[${#ffmpeg_metadata_keywords[@]}]="${1}";     shift;
        done;
    }
    function ffmpeg-metadata-keywords-add()    {
        if [ -z "${1:-""}" ];then return 0;fi
        while [ $# -ne 0 ]; do ffmpeg_metadata_keywords[${#ffmpeg_metadata_keywords[@]}]="${1}";  shift;  done;
    }                                                                   

    function ffmpeg-metadata-summary()      { ffmpeg_metadata_summary="${1:-""}";      }
    function ffmpeg-metadata-synopsis()     { ffmpeg_metadata_synopsis="${1:-""}";      }
    function ffmpeg-metadata-initial_key()  { ffmpeg_metadata_initial_key="${1:-""}";      }


    # - Temporal Information - #
    function ffmpeg-metadata-period()       { ffmpeg_metadata_period="${1:-""}";        }
    function ffmpeg-metadata-year()         { ffmpeg_metadata_year=${1:-0} ;            }
    # function ffmpeg-metadata-date YYYY [MM-dd]
    # Fonction pour indiquer l'année et le mois-jj
    function ffmpeg-metadata-date()         {
        ffmpeg_metadata_date='';
        if [ $# -eq 0 ]; then return $E_ARG_REQUIRE; fi # Ne pas afficher d'erreur à cause du cas d'utilisation de éinit
        local _mm_jj="$1";
        ffmpeg_metadata_year=$1;
        ffmpeg_metadata_date=$ffmpeg_metadata_year;
        local _mm_jj="${2:-""}";
        if [ -n "$_mm_jj" ];then  ffmpeg_metadata_date+="-${_mm_jj}";fi
        return 0;
    }
    function ffmpeg-metadata-date_tagged()  { ffmpeg_metadata_date_tagged="${1:-""}";   }
    function ffmpeg-metadata-date_written() { ffmpeg_metadata_date_written="${1:-""}";  }

   
    # - Spatial Information - #

    function ffmpeg-metadata-composition_location() {
        ffmpeg_metadata_composition_location=(); ffmpeg_metadata_composition_location_str='';
        while [ $# -ne 0 ]; do
            if [ -z "${1:-""}" ];then return 0; fi
            ffmpeg_metadata_composition_location[${#ffmpeg_metadata_composition_location[@]}]="${1}";     shift;
        done;
    }
    function ffmpeg-metadata-composition_location-add()    {
        if [ -z "${1:-""}" ];then return 0;fi
        while [ $# -ne 0 ]; do ffmpeg_metadata_composition_location[${#ffmpeg_metadata_composition_location[@]}]="${1}";  shift;  done;
    }    
   # - Identifiers - #
    function ffmpeg-metadata-ISRC()         { ffmpeg_metadata_ISRC="${1:-""}";     }
    function ffmpeg-metadata-MCDI()         { ffmpeg_metadata_MCDI="${1:-""}";     }
    function ffmpeg-metadata-ISBN()         { ffmpeg_metadata_ISBN="${1:-""}";     }
    function ffmpeg-metadata-BARCODE()      { ffmpeg_metadata_BARCODE="${1:-""}";  }
    function ffmpeg-metadata-CATALOG_NUMBER() { ffmpeg_metadata_CATALOG_NUMBER="${1:-""}";     }
    function ffmpeg-metadata-LABEL_CODE()   { ffmpeg_metadata_LABEL_CODE="${1:-""}";     }

    function ffmpeg-metadata-LCCN()         { ffmpeg_metadata_LCCN="${1:-""}";     }
    function ffmpeg-metadata-IMDB()         { ffmpeg_metadata_IMDB="${1:-""}";     }
    function ffmpeg-metadata-TMDB()         { ffmpeg_metadata_TMDB="${1:-""}";     }
    function ffmpeg-metadata-TVDB()         { ffmpeg_metadata_TVDB="${1:-""}";     }
    function ffmpeg-metadata-TVDB2()        { ffmpeg_metadata_ISRC="${1:-""}";     }


    # - comment/description - #

    function ffmpeg-metadata-url() {
        ffmpeg_metadata_url=(); ffmpeg_metadata_url_str='';
        while [ $# -ne 0 ]; do
            if [ -z "${1:-""}" ];then return 0; fi
            ffmpeg_metadata_url[${#ffmpeg_metadata_url[@]}]="${1}";     shift;
        done;
    }
    function ffmpeg-metadata-url-add()    {
        if [ -z "${1:-""}" ];then return 0;fi
        while [ $# -ne 0 ]; do ffmpeg_metadata_url[${#ffmpeg_metadata_url[@]}]="${1}";  shift;  done;
    }  
#

###############
# - comment - #
###############


    ############################
    # - comment: UTILISATEUR - #
    ############################
        function ffmpeg-metadata-comment-user(){ ffmpeg_metadata_comment_user="${1:-""}";}
        function ffmpeg-metadata-comment-user-add() {
            local _comment="${1:-""}";
            #display-vars '_comment' "$_comment";
            if [ -n "$_comment" ]
            then
                if [ -z "$ffmpeg_metadata_comment_user" ]
                then ffmpeg_metadata_comment_user="$_comment";
                else ffmpeg_metadata_comment_user+="$_comment";
                fi
            fi
            return 0;
        }

    ########################
    # - comment: calculé - #
    ########################
        # Affiche une vue en ligne de chaque commentaire
        function ffmpeg-metadata-comment-show(){
            local -i _fct_pile_app_level=2;
            fct-pile-app-in "$*" $_fct_pile_app_level;

            #display-vars 'ffmpeg_metadata_comment' "${ffmpeg_metadata_comment}";
            IFS=$FFMPEG_METADATA_COMMENT_SEPARATOR read -r -a _tableau <<< "$ffmpeg_metadata_comment";
            #display-vars 'com_nb' "${#_tableau[@]}";
            # Afficher les éléments du tableau
            for element in "${_tableau[@]}"
            do
                if [ -n "${element}" ]; then display-vars "${element%%:*}" "${element#*:}"; fi
            done

            fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
        }

        function ffmpeg-metadata-comment-read(){
            local -i _fct_pile_app_level=2;
            fct-pile-app-in "$*" $_fct_pile_app_level;

            if [ -n "$ffmpeg_metadata_del"  ]
            then 
                echo "La suppression des metadatas est demandé, ne pas charger les anciens TAGS & commentaires";
                return 0;
            fi

            local _src="$ffmpeg_in_pathname";   # Par défaut: methode: exec
            if [ "$ffmpeg_methode" = 'metadata_apply' ]; then _src="$ffmpeg_out_pathname"; fi

            titre4 "Lecture du TAG:COMMENT/DESCRIPTION dans '$_src'";
            ffmpeg_metadata_comment_read=$(ffprobe -v quiet -show_format "$_src" | grep -i "TAG:COMMENT");
            #if [ $debug_verbose_level -gt 0 ]; then display-vars 'ffmpeg_metadata_comment_read' "$ffmpeg_metadata_comment_read"; fi

            if [ -z "$ffmpeg_metadata_comment_read" ]
            then
                ffmpeg_metadata_comment_read=$(ffprobe -v quiet -show_format "$_src" | grep -i "TAG:DESCRIPTION");
            fi

            ffmpeg_metadata_comment_read="${ffmpeg_metadata_comment_read#*=}";
            #display-vars 'ffmpeg_metadata_comment_read' "$ffmpeg_metadata_comment_read";

            fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
        }

        function ffmpeg-metadata-comment-from-read() {
            local -i _fct_pile_app_level=2;
            fct-pile-app-in "$*" $_fct_pile_app_level;

            ffmpeg-metadata-comment-read;
            ffmpeg-metadata-comment "$ffmpeg_metadata_comment_read";

            fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
        }
        function ffmpeg-metadata-comment()        { ffmpeg_metadata_comment="${1:-""}";     }
        function ffmpeg-metadata-comment-add() {
            local _comment="${1:-""}";
            #display-vars '_comment' "$_comment";
            if [ -n "$_comment" ]
            then
                if [ -z "$ffmpeg_metadata_comment" ]
                then ffmpeg_metadata_comment="${FFMPEG_METADATA_COMMENT_SEPARATOR_2}$_comment";    #display-vars 'LINENO' "$LINENO";
                else ffmpeg_metadata_comment+="${FFMPEG_METADATA_COMMENT_SEPARATOR_2}$_comment";  #display-vars 'LINENO' "$LINENO";
                fi
            fi
            return 0;
        }
        function ffmpeg-metadata-comment-add-before() {
            local _comment="${1:-""}";
            #display-vars '_comment' "$_comment";
            if [ -n "$_comment" ]
            then
                if [ -z "$ffmpeg_metadata_comment" ]
                then ffmpeg_metadata_comment="${FFMPEG_METADATA_COMMENT_SEPARATOR_2}$_comment";
                else ffmpeg_metadata_comment="$_comment${FFMPEG_METADATA_COMMENT_SEPARATOR_2}${ffmpeg_metadata_comment}";
                fi
            fi
        }

        function ffmpeg-metadata-comment-language() { ffmpeg_metadata_comment_language="${1:-""}";     }
        #function ffmpeg-metadata-comment-support()  { ffmpeg_metadata_support="${1:-""}";     }

        #function ffmpeg-metadata-description()      { ffmpeg_metadata_description="${1:-""}";     }
    #


    ########################
    # - comment: convert - #
    ########################
        # ffmpeg-metadata-comment-convert-video( [out_map]=0:0 [out_codec=h264] ) 
        function ffmpeg-metadata-comment-convert-video(){
            ffmpeg_metadata_comment_convert_video_in_codec="$ffmpeg_infos_cv";
            ffmpeg_metadata_comment_convert_video_out_map="${1:-"0:0"}";
            ffmpeg_metadata_comment_convert_video_out_map="-map $ffmpeg_metadata_comment_convert_video_out_map";
            ffmpeg_metadata_comment_convert_video_out_codec="${2:-"h264"}";
            #ffmpeg-params-add "$ffmpeg_metadata_comment_convert_video_out_codec";   # Ne pas l'ajouter aux parametres
            ffmpeg-metadata-comment-add-convert-video;
            return 0;
        }

        function ffmpeg-metadata-comment-add-convert-video(){
            if [ -n "$ffmpeg_metadata_comment_convert_video_in_codec" ] && [ -n "$ffmpeg_metadata_comment_convert_video_out_codec" ]
            then
                ffmpeg-metadata-comment-add "$ffmpeg_metadata_comment_convert_video_out_map: $ffmpeg_metadata_comment_convert_video_in_codec->$ffmpeg_metadata_comment_convert_video_out_codec";
            fi
            return 0;
        }
        


        # ffmpeg-metadata-comment-convert-audio( in_codec [out_map]=0:0 [out_codec=aac] ) 
        function ffmpeg-metadata-comment-convert-audio(){
            if [ $# -eq 0 ]
            then 
                erreurs-pile-add-echo "${FUNCNAME[0]}(): Argument requis.";
                return $E_ARG_REQUIRE;
            fi
            ffmpeg_metadata_comment_convert_audio_in_codec="$ffmpeg_infos_ca";
            ffmpeg_metadata_comment_convert_audio_out_map="${1:-"0:0"}";
            ffmpeg_metadata_comment_convert_audio_out_map="-map $ffmpeg_metadata_comment_convert_audio_out_map";
            ffmpeg_metadata_comment_convert_audio_out_codec="${2:-"aac"}";
            #ffmpeg-params-add "$ffmpeg_metadata_comment_convert_audio_out_codec";  # Ne pas l'ajouter aux parametres
            ffmpeg-metadata-comment-add-convert-audio;
            return 0;
        }

        function ffmpeg-metadata-comment-add-convert-audio(){
            if [ -n "$ffmpeg_metadata_comment_convert_audio_in_codec" ] && [ -n "$ffmpeg_metadata_comment_convert_audio_out_codec" ]
            then
                ffmpeg-metadata-comment-add "$ffmpeg_metadata_comment_convert_audio_out_map: $ffmpeg_metadata_comment_convert_audio_in_codec->$ffmpeg_metadata_comment_convert_audio_out_codec";
            fi
            return 0;
        }
    #
#


###########################
# - ffmpeg-metadata-str - #
###########################
    function ffmpeg-metadata-str() {
        local _str="${1:-""}";
        if [ -n "$_str" ];then ffmpeg_metadata_str="$_str"; fi
    }

    function ffmpeg-metadata-str-add() {
        #display-vars 'ffmpeg_metadata_date' "$ffmpeg_metadata_date";
        #display-vars 'ffmpeg_metadata_str ' "$ffmpeg_metadata_str";

        local _tag="${1:-""}";
        #display-vars '_tag' "$_tag";
        if [ -n "$_tag" ];then ffmpeg_metadata_str="$ffmpeg_metadata_str -metadata $_tag"; fi
        #display-vars 'ffmpeg_metadata_date' "$ffmpeg_metadata_date";
        #display-vars 'ffmpeg_metadata_str ' "$ffmpeg_metadata_str";
    }
#

####################
# ffmpeg-tags-show #
####################
    # 
    # clear; gtt.sh -d --libExecAuto apps/ffmpeg/tests/ffmpeg-core-outils/ test-ffmpeg-fct-ffmpeg-streams-show
    # Utilise ffmprobe pour lire les metadatas et les affiche
    #Usage: gtt.sh ffmpeg ffmpeg-metadata-show - src=$1
    addLibrairie 'ffmpeg-metadata-show' "gtt.sh ffmpeg ffmpeg-metadata-show - src= : Affiche les TAGS du fichier donné en src";
    function      ffmpeg-metadata-show(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        # - Controle des entrées - #
            local _src="${1:-""}";

            # _src, priorité
            # 1- $1
            # 2 - ${PSP_params['src'] 
            # 3-  valeur de $ffmpeg_in_pathname
            local _src="${1:-""}";
            if [ "$_src" = '' ]
            then
                _src="${PSP_params['src']:-""}";
                if [ "$_src" = '' ]
                then
                    _src="$ffmpeg_in_pathname";
                    if [ "$_src" = '' ]
                    then
                        erreurs-pile-add-echo "${FUNCNAME[0]} - src=";
                        fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
                    fi
                fi
            fi
            #display-vars '_src' "$_src";

            if [ ! -f "$_src" ]
            then
                fct-pile-gtt-out $E_INODE_NOT_EXIST $_pile_fct_gtt_level; return $E_INODE_NOT_EXIST;
            fi
        #

        ffprobe -hide_banner -v quiet -show_format -i "$_src" | grep "TAG:";
        #ffmpeg-metadata-comment-show;  # Ces données ne viennet pas du fichier, ils sotn donc désynchro

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }
#

############
# - calc - #
############
    # Calcul $ffmpeg_metadata_*
    # https://ffmpeg.org/doxygen/trunk/group__metadata__api.html
    # https://wiki.multimedia.cx/index.php?title=FFmpeg_Metadata (les metadatas acceptés par FFMPEG selon le format)
    # https://www.abonnel.fr/informatique/cfp/ffmpeg_mp3_tag_metadata
    # https://write.corbpie.com/adding-metadata-to-a-video-or-audio-file-with-ffmpeg/
    function ffmpeg-metadata-calc(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _extension="${1:-""}";
        titre4 "${FUNCNAME[0]}";

        # - Récupération de l'extention de out - #
        local _extension="$ffmpeg_out_ext";


        ###########################################
        # - Suppression des metadats précédents - #
        ###########################################
        if [ -n "$ffmpeg_metadata_del" ];then ffmpeg-metadata-str "$ffmpeg_metadata_del"; fi


        ############
        # - UUID - #
        ############
        ffmpeg-metadata-UUID-load;


        ####################################
        # - Préparation des commentaires - #
        ####################################

            # - Lecture  du comment du fichier src - #
            ffmpeg-metadata-comment-from-read;

            # - ajout de la préparation de la nouvelle application- #
            # Ne pas utiliser ffmpeg-metadata-comment-add()
            ffmpeg_metadata_comment+="$FFMPEG_METADATA_COMMENT_SEPARATOR$(date +"%Y.%m.%d %H.%M"):"; # Double | pour un ajout de commentaire général
        #

        ################
        # - metadata - #
        ################

            # -- UUID -- #
                ffmpeg-metadata-str-add "UUID=\"${ffmpeg_metadata_UUID}\"";
            #

            # -- langue -- #
                if [ -n "$ffmpeg_metadata_language" ]
                then
                    ffmpeg-metadata-str-add "language=\"${ffmpeg_metadata_language}\"";
                    ffmpeg-metadata-str-add "lang=\"${ffmpeg_metadata_language}\"";
                fi
            #

            # - Organization Information - #
                if [ $ffmpeg_metadata_track_no -ne 0 ]
                then
                    ffmpeg-metadata-str-add "track_no=${ffmpeg_metadata_track_no}";
                    ffmpeg-metadata-str-add "track_number=${ffmpeg_metadata_track_no}";
                    ffmpeg-metadata-str-add "episode_id=$ffmpeg_metadata_track_no";
                fi
                if [ $ffmpeg_metadata_track_nb -ne 0 ]
                then
                    ffmpeg-metadata-str-add "TOTAL_PARTS=$ffmpeg_metadata_track_nb";
                    ffmpeg-metadata-str-add "PART_NUMBER=$ffmpeg_metadata_track_nb";
                    ffmpeg-metadata-str-add "track_nb=$ffmpeg_metadata_track_nb";
                    ffmpeg-metadata-str-add "track_count=$ffmpeg_metadata_track_nb";       # m4a/?:Pas reconnu ffmpg
                fi

                ffmpeg-metadata-str-add "track=$ffmpeg_metadata_track_no/$ffmpeg_metadata_track_nb";
            #

            # - Titles - #
                if [ ${#ffmpeg_metadata_title[@]} -ne 0 ]
                then
                    ffmpeg_metadata_title_str='';
                    for _val in ${ffmpeg_metadata_title[@]};do ffmpeg_metadata_title_str+="$_val|"; done
                    if [ -n "$ffmpeg_metadata_title_str" ]
                    then
                        ffmpeg_metadata_title_str="${ffmpeg_metadata_title_str::-1}";
                        ffmpeg-metadata-str-add     "title=\"$ffmpeg_metadata_title_str\"";
                        ffmpeg-metadata-comment-add "title:${ffmpeg_metadata_title_str}";
                    fi
                fi

                if [ -n "${ffmpeg_metadata_subtitle}"  ]
                then
                    ffmpeg-metadata-str-add     "subtitle=\"$ffmpeg_metadata_subtitle\"";
                    ffmpeg-metadata-comment-add "subtitle:${ffmpeg_metadata_subtitle}";

                fi

            #

            # - Entities - #
                if [ ${#ffmpeg_metadata_director[@]} -ne 0 ]
                then
                    ffmpeg_metadata_director_str='';
                    for _val in ${ffmpeg_metadata_director[@]};do ffmpeg_metadata_director_str+="$_val|"; done
                    if [ -n "$ffmpeg_metadata_director_str" ]
                    then
                        ffmpeg_metadata_director_str="${ffmpeg_metadata_director_str::-1}";
                        ffmpeg-metadata-str-add     "DIRECTOR=\"$ffmpeg_metadata_director_str\"";
                        ffmpeg-metadata-comment-add "director:${ffmpeg_metadata_director_str}";
                    fi
                fi

                if [ ${#ffmpeg_metadata_composer[@]} -ne 0 ]
                then
                    ffmpeg_metadata_composer_str='';
                    for _val in ${ffmpeg_metadata_composer[@]};do ffmpeg_metadata_composer_str+="$_val|"; done
                    if [ -n "$ffmpeg_metadata_composer_str" ]
                    then
                        ffmpeg_metadata_composer_str=${ffmpeg_metadata_composer_str::-1};
                        ffmpeg-metadata-str-add     "COMPOSER=\"$ffmpeg_metadata_composer_str\"";
                        ffmpeg-metadata-comment-add "composer:${ffmpeg_metadata_composer_str}";
                    fi
                fi

                if [ -n "$ffmpeg_metadata_composer_nationality" ]; then ffmpeg-metadata-str-add "COMPOSER_NATIONALITY=\"$ffmpeg_metadata_composer_nationality\""; fi

                if [ ${#ffmpeg_metadata_conductor[@]} -ne 0 ]
                then
                    ffmpeg_metadata_conductor_str='';
                    for _val in ${ffmpeg_metadata_conductor[@]};do ffmpeg_metadata_conductor_str+="$_val|"; done
                    if [ -n "$ffmpeg_metadata_conductor_str" ]
                    then
                        ffmpeg_metadata_conductor_str="${ffmpeg_metadata_conductor_str::-1}";
                        ffmpeg-metadata-str-add     "CONDUCTOR=\"$ffmpeg_metadata_conductor_str\"";
                        ffmpeg-metadata-comment-add "conductor:${ffmpeg_metadata_conductor_str}";
                    fi
                fi

                if [ ${#ffmpeg_metadata_actor[@]} -ne 0 ]
                then
                    ffmpeg_metadata_actor_str='';
                    for _val in ${ffmpeg_metadata_actor[@]};do ffmpeg_metadata_actor_str+="$_val|"; done
                    if [ -n "$ffmpeg_metadata_actor" ]
                    then
                        ffmpeg_metadata_actor_str="${ffmpeg_metadata_actor_str::-1}";
                        ffmpeg-metadata-str-add     "ACTOR=\"$ffmpeg_metadata_actor_str\"";
                        ffmpeg-metadata-comment-add "actor:${ffmpeg_metadata_actor_str}";
                    fi
                fi

                if [ ${#ffmpeg_metadata_character[@]} -ne 0 ]
                then
                    ffmpeg_metadata_character_str='';
                    for _val in ${ffmpeg_metadata_character[@]};do ffmpeg_metadata_character_str+="$_val|"; done
                    if [ -n "$ffmpeg_metadata_character_str" ]
                    then
                        ffmpeg_metadata_character_str="${ffmpeg_metadata_character_str::-1}";
                        ffmpeg-metadata-str-add     "CHARACTER=\"$ffmpeg_metadata_character_str\"";
                        ffmpeg-metadata-comment-add "character:${ffmpeg_metadata_character_str}";
                    fi
                fi

                if [ ${#ffmpeg_metadata_written_by[@]} -ne 0 ]
                then
                    ffmpeg_metadata_written_by_str='';
                    for _val in ${ffmpeg_metadata_written_by[@]};do ffmpeg_metadata_written_by_str+="$_val|"; done
                    if [ -n "$ffmpeg_metadata_written_by_str" ]
                    then
                        ffmpeg_metadata_written_by_str="${ffmpeg_metadata_written_by_str::-1}";
                        ffmpeg-metadata-str-add     "WRITER_BY=\"$ffmpeg_metadata_written_by_str\"";
                        ffmpeg-metadata-comment-add "written_by:${ffmpeg_metadata_written_by_str}";
                    fi
                fi

                if [ ${#ffmpeg_metadata_producer[@]} -ne 0 ]
                then
                    ffmpeg_metadata_producer_str='';
                    for _val in ${ffmpeg_metadata_producer[@]};do ffmpeg_metadata_producer_str+="$_val|"; done
                    if [ -n "$ffmpeg_metadata_producer_str" ]
                    then
                        ffmpeg_metadata_producer_str="${ffmpeg_metadata_producer_str::-1}";
                        ffmpeg-metadata-str-add     "PRODUCER=\"$ffmpeg_metadata_producer_str\"";
                        ffmpeg-metadata-comment-add "producer:${ffmpeg_metadata_producer_str}";
                    fi
                fi

                if [ ${#ffmpeg_metadata_distributed_by[@]} -ne 0 ]
                then
                    ffmpeg_metadata_distributed_by_str='';
                    for _val in ${ffmpeg_metadata_distributed_by[@]};do ffmpeg_metadata_distributed_by_str+="$_val|"; done
                    if [ -n "$ffmpeg_metadata_distributed_by_str" ]
                    then
                        ffmpeg_metadata_distributed_by_str="${ffmpeg_metadata_distributed_by_str::-1}";
                        ffmpeg-metadata-str-add     "DISTRIBUTED_BY=\"$ffmpeg_metadata_distributed_by_str\"";
                        ffmpeg-metadata-comment-add "distributed_by:${ffmpeg_metadata_distributed_by_str}";
                    fi
                fi

                if [ ${#ffmpeg_metadata_publisher[@]} -ne 0 ]
                then
                    ffmpeg_metadata_publisher_str='';
                    for _val in ${ffmpeg_metadata_publisher[@]};do ffmpeg_metadata_publisher_str+="$_val|"; done
                    if [ -n "$ffmpeg_metadata_publisher_str" ]
                    then
                        ffmpeg_metadata_publisher_str="${ffmpeg_metadata_publisher_str::-1}";
                        ffmpeg-metadata-str-add     "PUBLISHER=\"$ffmpeg_metadata_publisher_str\"";
                        ffmpeg-metadata-comment-add "publisher:${ffmpeg_metadata_publisher_str}";
                    fi
                fi

                if [ ${#ffmpeg_metadata_label[@]} -ne 0 ]
                then
                    ffmpeg_metadata_label_str='';
                    for _val in ${ffmpeg_metadata_label[@]};do ffmpeg_metadata_label_str+="$_val|"; done
                    if [ -n "$ffmpeg_metadata_label_str" ]
                    then
                        ffmpeg_metadata_label_str="${ffmpeg_metadata_label_str::-1}";
                        ffmpeg-metadata-str-add     "LABEL=\"$ffmpeg_metadata_label_str\"";
                        ffmpeg-metadata-comment-add "label:${ffmpeg_metadata_label_str}";
                    fi
                fi

                if [ ${#ffmpeg_metadata_writer[@]} -ne 0 ]
                then
                    ffmpeg_metadata_writer_str='';
                    for _val in ${ffmpeg_metadata_writer[@]};do ffmpeg_metadata_writer_str+="$_val|"; done
                    if [ -n "$ffmpeg_metadata_writer_str" ]
                    then
                        ffmpeg_metadata_writer_str="${ffmpeg_metadata_writer_str::-1}";
                        ffmpeg-metadata-str-add     "WRITER=\"$ffmpeg_metadata_writer_str\"";
                        ffmpeg-metadata-comment-add "writer:${ffmpeg_metadata_writer_str}";
                    fi
                fi
                
                #display-tableau-a 'ffmpeg_metadata_artist     ' "${ffmpeg_metadata_artist[@]}";
                #display-vars '${#ffmpeg_metadata_artist[@]}' "${#ffmpeg_metadata_artist[@]}";
                #|| [ ${#ffmpeg_metadata_artist[@]} -eq 1 ] && [ -z "${ffmpeg_metadata_artist[0]}" ]
                if [ ${#ffmpeg_metadata_artist[@]} -eq 0 ]
                then
                    titreInfo "tag artist vide: on le remplit avec ffmpeg_metadata_actor_str.";
                    ffmpeg_metadata_artist_str="$ffmpeg_metadata_actor_str";
                else
                    titreInfo "tag artist non vide.";
                    ffmpeg_metadata_artist_str='';
                    for _val in ${ffmpeg_metadata_artist[@]};do ffmpeg_metadata_artist_str+="$_val|"; done
                fi
                #display-vars '${ffmpeg_metadata_artist_str' "${ffmpeg_metadata_artist_str}";
                if [ -n "$ffmpeg_metadata_artist_str" ]
                then
                    ffmpeg_metadata_artist_str="${ffmpeg_metadata_artist_str::-1}";
                    ffmpeg-metadata-str-add     "ARTIST=\"$ffmpeg_metadata_artist_str\"";
                    ffmpeg-metadata-comment-add "artist:${ffmpeg_metadata_artist_str}";
                fi
                #display-vars 'ffmpeg_metadata_artist_str' "$ffmpeg_metadata_artist_str";

                if [ ${#ffmpeg_metadata_performer[@]} -ne 0 ]
                then
                    ffmpeg_metadata_performer_str='';
                    for _val in ${ffmpeg_metadata_performer[@]};do ffmpeg_metadata_performer_str+="$_val|"; done
                    if [ -n "$ffmpeg_metadata_performer_str" ]
                    then
                        ffmpeg_metadata_performer_str="${ffmpeg_metadata_performer_str::-1}";
                        ffmpeg-metadata-str-add     "PERFORMER=\"$ffmpeg_metadata_performer_str\"";
                        ffmpeg-metadata-comment-add "performer:${ffmpeg_metadata_performer_str}";
                    fi
                fi
            #

            # Search and Classification - #

                if [ ${#ffmpeg_metadata_genre[@]} -ne 0 ]
                then
                    ffmpeg_metadata_genre_str='';
                    for _val in ${ffmpeg_metadata_genre[@]};do ffmpeg_metadata_genre_str+="$_val|"; done
                    if [ -n "$ffmpeg_metadata_genre_str" ]
                    then
                        ffmpeg_metadata_genre_str="${ffmpeg_metadata_genre_str::-1}";
                        ffmpeg-metadata-str-add     "GENRE=\"$ffmpeg_metadata_genre_str\"";
                        ffmpeg-metadata-comment-add "genre:${ffmpeg_metadata_genre_str}";
                    fi
                fi

                if [ -n "$ffmpeg_metadata_album" ]; then ffmpeg-metadata-str-add "album=\"$ffmpeg_metadata_album\""; fi
                if [ -n "$ffmpeg_metadata_support" ];
                then
                    ffmpeg-metadata-str-add "support=\"$ffmpeg_metadata_support\"";
                    ffmpeg-metadata-str-add "ORIGINAL_MEDIA_TYPE=\"$ffmpeg_metadata_original_media_type\"";
                    ffmpeg-metadata-comment-add "support=$ffmpeg_metadata_support";
                fi

                if [ -n "$ffmpeg_metadata_original_content_type" ]; then ffmpeg-metadata-str-add "ORIGINAL_CONTENT_TYPE=\"$ffmpeg_metadata_original_content_type\""; fi

                if [ -n "$ffmpeg_metadata_subject" ]; then ffmpeg-metadata-str-add "SUBJECT=\"$ffmpeg_metadata_subject\""; fi

                if [ ${#ffmpeg_metadata_keywords[@]} -ne 0 ]
                then
                    ffmpeg_metadata_keywords_str='';
                    for _val in ${ffmpeg_metadata_keywords[@]};do ffmpeg_metadata_keywords_str+="$_val|"; done
                    if [ -n "$ffmpeg_metadata_keywords_str" ]
                    then
                        ffmpeg_metadata_keywords_str="${ffmpeg_metadata_keywords_str::-1}";
                        ffmpeg-metadata-str-add     "KEYWORDS=\"$ffmpeg_metadata_keywords_str\"";
                        ffmpeg-metadata-comment-add "keywords:${ffmpeg_metadata_keywords_str}";
                    fi
                fi

                if [ -n "$ffmpeg_metadata_summary" ];       then ffmpeg-metadata-str-add "SUBKECT=\"$ffmpeg_metadata_summary\""; fi
                if [ -n "$ffmpeg_metadata_synopsis" ]
                then
                    ffmpeg-metadata-str-add "SYNOPSIS=\"$ffmpeg_metadata_synopsis\"";
                    ffmpeg-metadata-comment-add "Synopsis:${ffmpeg_metadata_synopsis}";
                fi
                if [ -n "$ffmpeg_metadata_initial_key" ];   then ffmpeg-metadata-str-add "INITIAL_KEY=\"$ffmpeg_metadata_initial_key\""; fi
            #

            # - Temporal Information - #

                # https://fr.wikipedia.org/wiki/ISO_8601 (AAAA-MM-JJTHH:MM:SS,ss-/+FF:ff)
                # api ffmpeg
                # 2024.01.22: Ne prend que l'année
            
                if  [ -n "$ffmpeg_metadata_period"  ]
                then
                    ffmpeg-metadata-str-add     "PERIOD=\"${ffmpeg_metadata_period}\"";
                    ffmpeg-metadata-comment-add "PERIOD:${ffmpeg_metadata_period}";
                fi
            
                if  [ $ffmpeg_metadata_year -gt 0 ]
                # || [ -n "$ffmpeg_metadata_date" ]
                then
                    case "$_extension" in
                        'mkv')
                            ffmpeg-metadata-str-add "year=\"${ffmpeg_metadata_year}\"";
                            ffmpeg-metadata-str-add "date=\"${ffmpeg_metadata_date}\"";
                        ;;
                        *)
                            ffmpeg-metadata-str-add "date=\"${ffmpeg_metadata_date}\"";        ;;
                    esac
                    ffmpeg-metadata-comment-add "Date de sortie: ${ffmpeg_metadata_date}.";

                fi

                if  [ -n "$ffmpeg_metadata_date_tagged"  ]
                then
                    ffmpeg-metadata-str-add     "DATE_TAGGED=\"${ffmpeg_metadata_date_tagged}\"";
                    ffmpeg-metadata-comment-add "DATE_TAGGED:${ffmpeg_metadata_date_tagged}";
                fi
                if  [ -n "$ffmpeg_metadata_date_written"  ]
                then
                    ffmpeg-metadata-str-add     "DATE_WRITTEN=\"${ffmpeg_metadata_date_written}\"";
                    ffmpeg-metadata-comment-add "date_written:${ffmpeg_metadata_date_written}";
                fi
            #

            # - Spatial Information - #
                if [ ${#ffmpeg_metadata_composition_location[@]} -ne 0 ]
                then
                    ffmpeg_metadata_composition_location_str='';
                    for _val in ${ffmpeg_metadata_composition_location[@]};do ffmpeg_metadata_composition_location_str+="$_val|"; done
                    if [ -n "$ffmpeg_metadata_composition_location_str" ]
                    then
                        ffmpeg_metadata_composition_location_str="${ffmpeg_metadata_composition_location_str::-1}";
                        ffmpeg-metadata-str-add     "COMPOSITION_LOCATION=\"$ffmpeg_metadata_composition_location_str\"";
                        ffmpeg-metadata-comment-add "composition_location:${ffmpeg_metadata_composition_location_str}";
                    fi
                fi
            #

            # - Identifiers - #
                if  [ -n "$ffmpeg_metadata_ISRC" ];             then ffmpeg-metadata-str-add "ISRC=\"${ffmpeg_metadata_ISRC}\"";   fi
                if  [ -n "$ffmpeg_metadata_MCDI" ];             then ffmpeg-metadata-str-add "MCDI=\"${ffmpeg_metadata_MCDI}\"";   fi
                if  [ -n "$ffmpeg_metadata_ISBN" ];             then ffmpeg-metadata-str-add "ISBN=\"${ffmpeg_metadata_ISBN}\"";   ffmpeg-metadata-comment-add "ISBN:${ffmpeg_metadata_ISBN}"; fi
                if  [ -n "$ffmpeg_metadata_BARCODE" ];          then ffmpeg-metadata-str-add "BARCODE=\"${ffmpeg_metadata_BARCODE}\""; fi
                if  [ -n "$ffmpeg_metadata_CATALOG_NUMBER" ];   then ffmpeg-metadata-str-add "CATALOG_NUMBER=\"${ffmpeg_metadata_CATALOG_NUMBER}\""; fi
                if  [ -n "$ffmpeg_metadata_LABEL_CODE" ];       then ffmpeg-metadata-str-add "LABEL_CODE=\"${ffmpeg_metadata_LABEL_CODE}\""; fi
                if  [ -n "$ffmpeg_metadata_LCCN" ];             then ffmpeg-metadata-str-add "LCCN=\"${ffmpeg_metadata_LCCN}\"";   fi
                if  [ -n "$ffmpeg_metadata_TMDB" ];             then ffmpeg-metadata-str-add "TMDB=\"${ffmpeg_metadata_TMDB}\"";   fi
                if  [ -n "$ffmpeg_metadata_IMDB" ];             then ffmpeg-metadata-str-add "IMDB=\"${ffmpeg_metadata_IMDB}\"";   ffmpeg-metadata-comment-add "IMDB:${ffmpeg_metadata_IMDB}"; fi
                if  [ -n "$ffmpeg_metadata_TVDB" ];             then ffmpeg-metadata-str-add "TVDB=\"${ffmpeg_metadata_TVDB}\"";   fi
                if  [ -n "$ffmpeg_metadata_TVDB2" ];            then ffmpeg-metadata-str-add "TVDB2=\"${ffmpeg_metadata_TVDB2}\"";  fi


                if [ ${#ffmpeg_metadata_url[@]} -ne 0 ]
                then
                    ffmpeg_metadata_url_str='';
                    for _val in ${ffmpeg_metadata_url[@]};do ffmpeg_metadata_url_str+="$_val|"; done
                    if [ -n "$ffmpeg_metadata_url_str" ]
                    then
                        ffmpeg_metadata_url_str="${ffmpeg_metadata_url_str::-1}";
                        ffmpeg-metadata-str-add     "URL=\"$ffmpeg_metadata_url_str\"";
                        ffmpeg-metadata-comment-add "url:${ffmpeg_metadata_url_str}";
                    fi
                fi
            #ffmpeg-metadata-display-vars;
            #
        #


        ###################
        # - Commentaire - #
        ###################

        # - Ajout des tags communs l'UUID dans le comment - #
        if [ -n "$ffmpeg_metadata_comment_support"  ];  then ffmpeg-metadata-comment-add "Support:$ffmpeg_metadata_comment_support.";  fi
        if [ -n "$ffmpeg_metadata_comment_language" ];  then ffmpeg-metadata-comment-add "$ffmpeg_metadata_comment_language.";         fi

        # - Ajout des commentaires de l'utilisateur - #
        #display-vars 'ffmpeg_metadata_comment_user' "$ffmpeg_metadata_comment_user";
        ffmpeg-metadata-comment-add "$ffmpeg_metadata_comment_user";

        # - Ajout de l'UUID dans le comment - #
        if [[ ! "$ffmpeg_metadata_comment" = *"UUID"* ]]
        then ffmpeg_metadata_comment="UUID=$ffmpeg_metadata_UUID$ffmpeg_metadata_comment";
        else ffmpeg_metadata_comment="$FFMPEG_METADATA_COMMENT_SEPARATOR$ffmpeg_metadata_comment";
        fi


        # - Intégration du commentaire au général - #
        if [ -n "$ffmpeg_metadata_comment" ]
        then
            # FFMPEG
            case "$_extension" in
                'm4a'|'mp4'|'mov'|'avi'|\
                'asf'|'wma'|'wmv'|\
                'rm' |'sox')
                    ffmpeg-metadata-str-add "comment=\"$ffmpeg_metadata_comment\"";     ;;
                'mkv')
                    ffmpeg-metadata-str-add "DESCRIPTION=\"$ffmpeg_metadata_comment\""; ;;
            esac
        fi


        ##############
        # - DEBUG  - #
        ##############
        if [ $debug_verbose_level -gt 0 ]; then  ffmpeg-metadata-display-vars; fi

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }
#


################################
# objet: ffmpeg-metadata-apply #
################################
    # Calcul les metadatas ffpmeg_metadata_* et les applique sur ffmpeg-out-*
    # function     ffmpeg_metadata_apply
    # redondant avec dl/save-metadata
    # Utilise en entrée uniquement $ffmpeg_out_pathname
    # test/exemples: clear; gtt.sh ffmpeg/tests/ffmpeg-core-metadata/ffmpeg-metadata-apply.sh
    # Utiliser ffmpeg-metadata-run pour calculer la src des metadatas de *-read
    #addLibrairie 'ffmpeg-metadata-apply';      # NE PAS Utiliser directement
    function      ffmpeg-metadata-apply(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        titre3 "${FUNCNAME[0]}":
        hr >> "$ffmpeg_log_pathname";

        local _erreurs_txt='';
        erreur-set 0;


        # - Controle des entrées - #
        # 1- ffmmpeg_out_*
        # ffmmpeg_out_* est utilisé plutot que ffmmpeg_in_* car c'est une fonction qui suit d'autres qui génère ffmmpeg_out_*

        #ffmpeg-out-display-vars;

        # - Création du fichier temporaire - #
        local _ffmpeg_out_tmp="$ffmpeg_out_rep/$ffmpeg_out_nom.md.$ffmpeg_out_ext"; # Fichier temporaire contenant les métadonnées


        # - Gestion du log - #
        echo "${FUNCNAME[0]}: $ffmpeg_out_pathname" >> $ffmpeg_log_pathname;
        #echo "$ffmpeg_in_name" >> $ffmpeg_log_pathname;


        # - Vérification de la source (out) - #
        if [ ! -f "${ffmpeg_out_pathname}" ]
        then
            _erreurs_txt="${FUNCNAME[0]}: Le fichier source '$ffmpeg_out_pathname' n'existe pas.";
            erreurs-pile-add-echo "$_erreurs_txt";
            echo "$_erreurs_txt" >> "$ffmpeg_log_pathname";
            fct-pile-app-out $E_INODE_NOT_EXIST $_fct_pile_app_level; return $E_INODE_NOT_EXIST;
        fi

        ffmpeg-params-global-calc;
        ffmpeg-metadata-calc;               # - Calcul de ffmpeg_metadata_str - #

        if [ -z "$ffmpeg_metadata_str" ]
        then
            _erreurs_txt="${FUNCNAME[0]}: La variable  ffmpeg_metadata_str est vide.";
            erreurs-pile-add-echo "$_erreurs_txt";
            echo "$_erreurs_txt" >> "$ffmpeg_log_pathname";
            fct-pile-app-out $E_INODE_NOT_EXIST $_fct_pile_app_level; return $E_INODE_NOT_EXIST;
        fi

        # - DEBUG: display-vars - #
        if [ $debug_verbose_level -gt 0 ]
        then
            display-vars 'ffmpeg_params_global' "$ffmpeg_params_global";
            display-vars 'ffmpeg_params       ' "$ffmpeg_params";
        fi


        # - Execution - #
        titre4 "# - ${FUNCNAME[0]}: Commande ffmpeg - #";
        if $isSimulation
        then titreCmd  "ffmpeg $ffmpeg_params_global -i \"$ffmpeg_out_pathname\" ${ffmpeg_params} ${ffmpeg_metadata_str} -codec:v copy -codec:a copy "$_ffmpeg_out_tmp" 2>> \"$ffmpeg_log_pathname\"";
        else eval-echo "ffmpeg $ffmpeg_params_global -i \"$ffmpeg_out_pathname\" ${ffmpeg_params} ${ffmpeg_metadata_str} -codec:v copy -codec:a copy "$_ffmpeg_out_tmp" 2>> \"$ffmpeg_log_pathname\"";
        fi
        #local _pid_last=${!:-"n/a"}
        titreInfo "Pour changer la priorité du proccessus: sudo nice -n -1 pid"; 


        # - Gestion des erreurs - #
        if [ $erreur_no -ne 0 ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}:'$ffmpeg_out_pathname': Erreur '$(erreur-no-description-echo)' lors de l'application des métadonnées";
            fct-pile-app-out $erreur_no $_fct_pile_app_level; return $erreur_no;
        fi

        if [ ! -f "$_ffmpeg_out_tmp" ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}: Le fichier temporaire '$_ffmpeg_out_tmp' n'existe pas.";
            fct-pile-app-out $E_INODE_NOT_EXIST $_fct_pile_app_level; return $E_INODE_NOT_EXIST;
        fi

        # si taille = 0 -> return

        titre4 "# - ${FUNCNAME[0]}: Remplacement du fichier originale par le fichier temporaire - #";
        eval-echo "mv \"$ffmpeg_out_pathname\" \"${ffmpeg_out_pathname}.read\""; 
        eval-echo "mv \"$_ffmpeg_out_tmp\"     \"${ffmpeg_out_pathname}\""; 
        if [ $erreur_no -eq 0 ]; then eval-echo "rm \"${ffmpeg_out_pathname}.read\""; fi

        titre4 "# - ${FUNCNAME[0]}: Comparaison des tailles - #";
        ffmpeg-in-sizeWC-compare;

        titre4 "# - ${FUNCNAME[0]}: Affichage du fichier final - #";
        ffmpeg-streams-show "$ffmpeg_out_pathname";
        #ffprobe -v quiet -print_format json -show_format "$ffmpeg_out_pathname";
        ffmpeg-metadata-show "$ffmpeg_out_pathname";

        fct-pile-app-out $erreur_no $_fct_pile_app_level; return $erreur_no;
    }
#


return 0;
