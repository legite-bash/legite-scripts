echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh --show-libsLevel2 ffmpeg
# date de création    : 202?
# date de modification: 2024.09.17


###########
# STREAMS #
###########
#https://superuser.com/questions/1273764/using-ffmpeg-on-how-do-i-copy-the-video-and-multiple-subtitle-streams-in-an-mkv
#-map 0 = -map 0:v? -map 0:a? -map 0:s? 
#Type 	Matches
#v 	Video
#V 	Video except attached pictures, video thumbnails, cover arts
#a 	Audio
#s 	Subtitles
#d 	Data
#t 	Attachments
#usage: codec:type. ex: codec:V


########################
# ffmpeg-exec-callback #
########################
declare -g ffmpeg_exec_callback_before='';
declare -g ffmpeg_exec_callback_after='';

# initialise la variable ffmpeg_exec_callback_before. CE n'est PAS le callback lui même!
function ffmpeg-exec-callback-before(){
    ffmpeg_exec_callback_before="${1:-""}":
}

function ffmpeg-exec-callback-after(){
    ffmpeg_exec_callback_after="${1:-""}":
}


####################
# ffmpeg-exec-code #
####################
    # function ffmpeg-exec-code( ffmpeg_in_pathname ffmpeg_out_pathname )
    # Description: Lance le programme ffmpeg en utilisant  ffmpeg_in,ffmpeg_out,ffmpeg_params_*
    # in doit être un fichier
    function ffmpeg-exec-code(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;


        # - Controle des entrées - #
        # Aucun, utilise $ffmpeg_in_pathname, $ffmpeg_out_pathname


        # -  - #
        titre3 "${FUNCNAME[0]}($@)";
        erreur-set 0;
        gtt-duree-debut;
        #ffmpeg_params_input=" -i \"$ffmpeg_in_pathname\" ${ffmpeg_params_input}";
        ffmpeg-params-input-add-before "$ffmpeg_in_pathname";


        # - Controle des in/out : Verifier que la destination est différente de la source- #
        titre4 "# - Protection contre l'auto-écrasement - #";
        local _in_realpath="$(realpath "$ffmpeg_in_pathname")";
        local _out_realpath="$(realpath "$ffmpeg_out_pathname")";
        if [ "$_in_realpath" = "$_out_realpath"  ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]} La source '$ffmpeg_in_pathname' est la même que la destination";
            fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
        fi


        titre4 "# - Afficher les infos du fichier original - #";
        ffmpeg-infos-show    "$ffmpeg_in_pathname";
        ffmpeg-streams-show  "$ffmpeg_in_pathname";
        ffmpeg-metadata-show "$ffmpeg_in_pathname";
        local -i _ffmpeg_in_sizeWC=$ffmpeg_infos_sizeWC;


        # - Préparation du log - #
        echo "$(hr)">> "$ffmpeg_log_pathname";

        # - Verfification de la sortie - #
        local _ffmpeg_out_isExist=false;
        if [ -f "${ffmpeg_out_name}" ]  # si le fichier créé existe deja
        then
            ffmpeg-out-sizeWC;

            if [ "$ffmpeg_out_sizeWC" = '' ]\
            || [ "$ffmpeg_out_sizeWC" = 0 ]
            then
                notifs-pile-add-echo "${ffmpeg_out_pathname} convertit pré-existant vide -> rm";
                rm "$ffmpeg_out_pathname";
            else
                _ffmpeg_out_isExist=true;
                notifs-pile-add-echo "Le fichier de destination '$ffmpeg_in_pathname' déjà existant.";
                echo      "Le fichier de destination '$ffmpeg_in_pathname' déjà existant." >> "$ffmpeg_log_pathname";
            fi
        fi

        #ffmpeg-in-display-vars;

        # - preset - #
        if [ -n "$ffmpeg_params_crf" ];     then ffmpeg-params-add-before " $ffmpeg_params_crf";   fi
        if [ -n "$ffmpeg_params_tune" ];    then ffmpeg-params-add-before " $ffmpeg_params_tune";   fi
        if [ -n "$ffmpeg_params_preset" ];  then ffmpeg-params-add-before " $ffmpeg_params_preset";   fi


        # - parametres spécifiques à un format - #
        hr;
        echo-v "Ajout des paramètres spécifique aux codecs" 1;

        case "$ffmpeg_infos_cv" in
            'h264')
                echo-v "Le codec vidéo est $ffmpeg_infos_cv: ajout de faststart" 1;
                ffmpeg-params-add "faststart";
                ;;
        esac

        # - calcul des sous éléments - #
        ffmpeg-params-global-calc;
        ffmpeg-cover-calc;


        # - Concat de tous les paramêtres - #
        ffmpeg_params_all="";
        ffmpeg-params-all-add-line "ffmpeg$ffmpeg_params_global";
        ffmpeg-params-all-add-line "$ffmpeg_params_input";
        ffmpeg-params-all-add-line "$ffmpeg_params";
        ffmpeg-params-all-add-line "$ffmpeg_metadata_str";
        ffmpeg-params-all-add-line " \"${ffmpeg_out_pathname}\"";


        # - Execution de ffmpeg- #
        titre4 'Execution';
        titreCmd  "Pour changer la priorité du proccessus: sudo nice -n -1 pid"; 
        titreInfo "tail -f '$ffmpeg_log_pathname'";

        local -i _erreur_no=0;
        if $isSimulation
        then echo   "[SIMULATION]"; titreCmd  "$ffmpeg_params_all 2>> \"$ffmpeg_log_pathname\"";
        else                        eval-echo "$ffmpeg_params_all 2>> \"$ffmpeg_log_pathname\"";
        fi
        erreur-no-description-echo;
        if [ $erreur_no -ne 0 ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}($ffmpeg_out_pathname): fail";
        fi


        #ffmpeg-out-display-vars;


        titre4 "# - Afficher les infos du fichier créé : '${ffmpeg_out_pathname}'- #";
        if [ -f "${ffmpeg_out_pathname}" ]
        then
            ffmpeg-infos-show    "$ffmpeg_out_pathname";
            ffmpeg-streams-show  "$ffmpeg_out_pathname";
            ffmpeg-metadata-show "$ffmpeg_out_pathname";
            ffmpeg-metadata-comment-show;
            ffmpeg-in-sizeWC-compare;

        fi
        titre4 "# - Rapport - #";
        gtt-duree-fin;
        gtt-duree-HHMMSS;
        echo "Durée: $gtt_duree_HHMMSS_str";
        echo "Heure de fin: $(date +%H:%M)";
        hr;
        fct-pile-app-out $_erreur_no $_fct_pile_app_level; return $_erreur_no;
    }


    #function     ffmpeg_exec_profil( [_src="$ffmpeg_in_pathname"] [[_ffmpeg_out_rep/]_ffmpeg_out_pathname|._ffmpeg_out_ext] )
    #                                                   pathname|.ext: Présence du point obligatoire si extention
    # si _ffmpeg_out_pathname commence par '.' alors c'est l'extention seule qui est changée
    # Utilise en entrée $ffmpeg_in_pathname, $ffmpeg_out_pathname
    # ffmpeg_in_pathname peut etre relatif au repertoire courant ou absolu

    # ffmpeg-in-pathname "$_src"
    # ffmpeg-out-pathname "$_dest"
    # Doit être initialisé avec ffmpeg-init()
    # exemple: clear; gtt.sh tests/test-ffmpeg-profils.sh 
    # Utiliser ffmpeg-metadata-run pour calculer la src des metadatas de *-read
    # addLibrairie 'ffmpeg-exec';    # NE PAS Utiliser directement
    function      ffmpeg-exec(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;


        # - Controle des entrées - #
            #ffmpeg_out_pathname "$1";  # pathname|.ext  # présence du point obligatoire si extention


            # _src, priorité
            # 1- $1
            # 2- ${PSP_params['src'] 
            # 3- valeur de $ffmpeg_in_pathname
            # 4- return
            
            local _src="${1-""}";                  # 
            if [ ! "$_src" = '' ]
            then
                    ffmpeg-in-pathname "$_src";
            else

                _src="${PSP_params['src']:-""}";
                #display-vars $LINENO'_src' "$_src";

                if [ ! "$_src" = '' ]
                then
                    #display-vars $LINENO'_src' "$_src";
                    ffmpeg-in-pathname "$_src";
                else
                    if [ "$ffmpeg_in_pathname" = '' ]
                    then
                        erreurs-pile-add-echo "${FUNCNAME[0]}(): Aucune source de définit.";
                        fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
                    fi
                fi
            fi
            unset _src;
            #display-vars $LINENO'ffmpeg_in_pathname' "$ffmpeg_in_pathname";


            # _dest, priorité
            # 1- $2
            # 2- ${PSP_params['dest'] 
            # 3- valeur de $ffmpeg_out_pathname
            # 4- return

            local _dest="${2:-""}";                  # 
            if [ ! "$_dest" = '' ]
            then
                ffmpeg-out-pathname "$_dest";
            else
                _dest="${PSP_params['dest']:-""}";
                if [ ! "$_dest" = '' ]
                then
                    ffmpeg-out-pathname "$_dest";
                else
                    if [ "$ffmpeg_out_pathname" = '' ]
                    then
                        erreurs-pile-add-echo "${FUNCNAME[0]}(): Aucune destination de définit.";
                        fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
                    fi
                fi
            fi
            unset _dest;
            #display-vars $LINENO'ffmpeg_out_pathname' "$ffmpeg_out_pathname";
        #

        titre4 "# - in - #";
        if [ ! -f "$ffmpeg_in_pathname" ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}('$ffmpeg_in_pathname'): fichier inexistant";
            fct-pile-app-out 0 $_fct_pile_app_level; return $E_INODE_NOT_EXIST;
        fi
        ffmpeg-in-display-vars;

        titre4 "# - out - #";
        ffmpeg-out-display-vars;


        titre1 "${FUNCNAME[0]}( '$ffmpeg_in_pathname' \n               '$ffmpeg_out_pathname')";
        titreInfo "tail --lines=200 -f \"$ffmpeg_log_pathname\"";


        ############################
        # - Calcul des metadatas - #
        ############################
        ffmpeg-metadata-calc;


        ############################
        # - input est un fichier - #
        ############################
        if [ -f "$ffmpeg_in_pathname" ]
        then
            echo-d "${FUNCNAME[0]}():'$ffmpeg_in_pathname' est un fichier" 3; 
            ffmpeg-exec-code "$ffmpeg_in_pathname" "$ffmpeg_out_pathname";
            fct-pile-app-out 0 $_fct_pile_app_level; return 0;
        fi

        ###############################
        # - input est un répertoire - #
        ###############################
        if   [ -d "$ffmpeg_in_pathname" ]
        then
            titreInfo  "${FUNCNAME[0]}():$LINENO:'$ffmpeg_in_pathname' est un repertoire" 1
            titre2  "$ffmpeg_in_pathname";
            if ! cd "$ffmpeg_in_pathname" 
            then
                erreurs-pile-add-echo "Impossible d'entrer dans le repertoire: '$ffmpeg_in_pathname'";
                fct-pile-app-out 0 $_fct_pile_app_level; return 0;
            fi

            #titre2 "$PWD";
            #ffmpeg-infos-show "$ffmpeg_in_pathname";  # repertoire  # DESACTIVER TEMPORAIREMENT POUR DEV


            ###############################
            # - Rechercher les fichiers - #
            ###############################
                #for _fichier_name in *"$ffmpeg_in_ext"     # converti tous les fichiers (concordant au masque) du repertoire
                #for _fichier_name in "$ffmpeg_in_pathname/"*

                titre4 "# - Recherche des fichiers dans le répertoire: '$ffmpeg_in_rep' - #";
                local  _fichiers_liste=$(find "$ffmpeg_in_rep" -maxdepth 1 -type f );  # find fichiers
                display-vars '_fichiers_liste' "$_fichiers_liste";

                if [ -z "$_fichiers_liste" ]
                then
                    echo "Pas de fichier.";
                else
                    local -i _fichierNb=0;
                    #local -i _fichierNb=${#_fichiers_liste[@]};
                    #display-vars '_fichierNb' "$_fichierNb";

                    #titre4 "# - Trie des fichiers - #";
                    #local  _fichiers_tries=$(echo $_fichiers_liste | sort);
                    #display-vars '_fichiers_tries' "${_fichiers_tries[@]}";


                    titre4 "# - Boucle sur les fichiers - #";
                    for _ffmpeg_in_pathname in $_fichiers_liste # NE PAS QUOTER sinon renvoi une seule chaine de caractere
                    #for _fichier_pathname in $_fichiers_tries  # NE PAS QUOTER si<non renvoi une seule chaine de caractere
                    do
                        HR;
                        display-vars '_ffmpeg_in_pathname    ' "${_ffmpeg_in_pathname}";
                        if $isTrapCAsk;then break;fi

                        echo "$_ffmpeg_in_pathname";

                        titre4 "# - Validation du container - #";
                        if ! ffmpeg-container-is_valide "$_ffmpeg_in_pathname"; then echo "format non valide"; continue; fi

                        titre4 "# - initialisation de ffmpeg_in_* - #";
                        ffmpeg-in-pathname "$_ffmpeg_in_pathname";

                        titre4 "# - callback ffmpeg-exec-callback-before: ${ffmpeg_exec_callback_before}() - #";
                        $ffmpeg_exec_callback_before;
                        ffmpeg-exec-callback-before-ext-to-mkv;

                        # - Execution de ffmpeg - #
                        eval-echo "ffmpeg-exec \"$ffmpeg_in_pathname\""; 

                        titre4 "# - callback ffmpeg-exec-callback-after: $ffmpeg_exec_callback_after - #";
                        $ffmpeg_exec_callback_after;        # ex: ffmpeg-infos-show "$_fichier_pathname";

                        
                        ((_fichierNb++))
                        break;
                    done
                    display-vars 'Nb de fichiers' "$_fichierNb";
                fi
            #


            #######################################
            # - Rechercher les sous repertoires - #
            #######################################
            if [ $recursifLevel -gt 0 ]
            then
                local -a _repListe=$(find "$_src" -type d );  # find sous repertoires
                for _repTrie in $(sort <<< "${_repListe[*]}") # NE PAS QUOTER sinon renvoi une seule chaine de caractere
                do
                    if $isTrapCAsk;then break;fi
                    ffmpeg-out-nom "$ffmpeg_in_nom";
                    ffmpeg_exec_profil "$_repTrie" "$_profil" "$ffmpeg_out_pathname"
                done
            fi
            #

        else # ce n'est ni un fichier ni repertoire
            erreurs-pile-add-echo "${FUNCNAME[0]}(). La source '$ffmpeg_in_pathname' est ni un repertoire, ni un fichier.";
            fct-pile-app-out 0 $_fct_pile_app_level; return $E_INODE_NOT_EXIST;
        fi

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }
#


return 0;