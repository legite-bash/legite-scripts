echo-d "${BASH_SOURCE[0]}" 1;

###########
## COVER ##
###########

###############
# DECLARATION #
###############

    # - la reference du cover dans le fichier - #
    declare -gr FFMPEG_COVER_LOCATION_DEFAULT='Cover (front)';
    declare -g  ffmpeg_cover_location="$FFMPEG_COVER_LOCATION_DEFAULT";           # le location en cours
    declare -ga ffmpeg_cover_location_liste=('Cover (front)' 'Cover (back)')
    declare -gA ffmpeg_cover_front_location_path; #contient les paths du fichier a intégrer
    ffmpeg_cover_front_location_path['Cover (front)']='';
    ffmpeg_cover_front_location_path['Cover (back)']='';

    # - la description du cover dans le fichier (delon le location)- #
    declare -gA ffmpeg_cover_location_descritions;                    # coverDescriptions["Cover (front)"]="Illustration de ..."
    ffmpeg_cover_location_descritions['Cover (front)']='';
    ffmpeg_cover_location_descritions['Cover (back)']='';

    # les levels de cover du + précis au + global
    declare -gr FFMPEG_COVER_LEVEL_DEFAULT='Cover (front)';
    declare -g  ffmpeg_cover_level="$FFMPEG_COVER_LEVEL_DEFAULT";
    declare -ga ffmpeg_cover_level_liste=('Cover (front)' 'yt_file' 'yt_file_WEBP' 'yt_file_JPG' 'yt_file_PNG' 'yt_page');
    declare -gA ffmpeg_cover_level_urls;             # ffmpeg_cover_level_urls["Cover (front)"]="url"
    ffmpeg_cover_level_urls['Cover (front)']='';
    ffmpeg_cover_level_urls['yt_file']='';
    ffmpeg_cover_level_urls['yt_file_WEBP']='';      # format yt natif pour les miniatures des videos
    ffmpeg_cover_level_urls['yt_file_JPG']='';       # dlCoverYT convertit yt_file_WEBP_path en yt_file_JPG_path
    ffmpeg_cover_level_urls['yt_file_PNG']='';
    ffmpeg_cover_level_urls['yt_page']='';
    
    declare -gA ffmpeg_cover_level_path;            # ffmpeg_cover_level_path["Cover (front)"]="fileName"
    ffmpeg_cover_level_path['Cover (front)']='';
    ffmpeg_cover_level_path['yt_file']='';
    ffmpeg_cover_level_path['yt_file_WEBP']='';
    ffmpeg_cover_level_path['yt_file_JPG']='';
    ffmpeg_cover_level_path['yt_file_PNG']='';
    ffmpeg_cover_level_path['yt_page']='';

    declare -gA ffmpeg_cover_level_descriptions;             # ffmpeg_cover_level_descriptions["Cover (front)"]="texte"
    ffmpeg_cover_level_descriptions['Cover (front)']='';
    ffmpeg_cover_level_descriptions['yt_file']='';
    ffmpeg_cover_level_descriptions['yt_file_WEBP']='';
    ffmpeg_cover_level_descriptions['yt_file_JPG']='';
    ffmpeg_cover_level_descriptions['yt_file_PNG']='';
    ffmpeg_cover_level_descriptions['yt_page']='';

    declare -g  ffmpeg_cover_front_tmp_pathname='';     # fichier temporaire pour ffmpeg pour cover Front
    declare -g  ffmpeg_cover_tag_str='';                # chemin du cover Front (calculer part set)
    declare -g  ffmpeg_cover_dl_use_dlyt=true;             # par defaut on telecharge le cover yt avec dlyt ()
#


########
# init #
########
    function ffmpeg-cover-init(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        ffmpeg-cover-level;
        ffmpeg_cover_level_urls['Cover (front)']='';
        ffmpeg_cover_level_urls['yt_file']='';
        ffmpeg_cover_level_urls['yt_file_JPG']='';
        ffmpeg_cover_level_urls['yt_file_PNG']='';
        ffmpeg_cover_level_urls['yt_page']='';

        ffmpeg_cover_level_path['Cover (front)']='';
        ffmpeg_cover_level_path['yt_file']='';
        ffmpeg_cover_level_path['yt_file_JPG']='';
        ffmpeg_cover_level_path['yt_file_PNG']='';
        ffmpeg_cover_level_path['yt_page']='';

        ffmpeg_cover_level_descriptions['Cover (front)']='';
        ffmpeg_cover_level_descriptions['yt_file']='';
        ffmpeg_cover_level_descriptions['yt_file_JPG']='';
        ffmpeg_cover_level_descriptions['yt_file_PNG']='';
        ffmpeg_cover_level_descriptions['yt_page']='';

        fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
    }


    function ffmpeg-cover-display-vars(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        titre2 'LES COVERS';

        titre3 'LEVELS';
        display-vars 'FFMPEG_COVER_LEVEL_DEFAULT'  "$FFMPEG_COVER_LEVEL_DEFAULT" 
            echo '  ffmpeg_cover_level_liste[@]  ' "${ffmpeg_cover_level_liste[@]}"
        display-vars 'ffmpeg_cover_level   '  "$ffmpeg_cover_level"

        for _coverName in "${ffmpeg_cover_level_liste[@]}"; 
        do
            echo "cover: $_coverName";
            display-vars "ffmpeg_cover_level_urls"  "${ffmpeg_cover_level_urls[$_coverName]}";

            local _path="${ffmpeg_cover_level_path[$_coverName]}";
            local _isPathOK="$_path";
            if [ ! -f "$_path" ];then _isPathOK="$WARN$_path$NORMAL"; fi
            display-vars "ffmpeg_cover_level_path" "$_isPathOK"
        done;

        titre3 'LOCATIONS';

        display-vars 'FFMPEG_COVER_LOCATION_DEFAULT'  "$FFMPEG_COVER_LOCATION_DEFAULT" 
        display-tableau-a 'ffmpeg_cover_location_liste' "${ffmpeg_cover_location_liste[@]}";

        display-vars 'ffmpeg_cover_location   '  "$ffmpeg_cover_location";

        for _coverName in "${ffmpeg_cover_location_liste[@]}"; 
        do
            _path=${ffmpeg_cover_front_location_path[$_coverName]:-''}
            display-vars "ffmpeg_cover_front_location_path[$_coverName]  " "$_path";
            display-vars "coverDescriptions[$_coverName]" "${ffmpeg_cover_location_descritions[$_coverName]:-""}";
        done;

        fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
    }
#


#############
# SELECTION #
#############
    # ffmpeg-cover-location ( [ffmpeg_cover_location] )
    function ffmpeg-cover-location(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _ffmpeg_cover_location="${1:-"$FFMPEG_COVER_LOCATION_DEFAULT"}";
        for   _elt in "${ffmpeg_cover_location_liste[@]}"; 
        do
            if [ "$_elt" = "$_ffmpeg_cover_location" ]; then ffmpeg_cover_location="$_ffmpeg_cover_location"; fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE; fi
        done;

        fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
    }

    # ffmpeg-cover-level ( [ffmpeg_cover_level] )
    function ffmpeg-cover-level(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _ffmpeg_cover_level="${1:-"$FFMPEG_COVER_LEVEL_DEFAULT"}";
        for   _elt in "${ffmpeg_cover_level_liste[@]}"; 
        do
            if [ "$_elt" = "$_ffmpeg_cover_level" ]; then ffmpeg_cover_location="$_ffmpeg_cover_level"; fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE; fi
        done;

        fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
    }


    ## -- COVER: GESTION DES LOCATIONS -- ##
    # setCoverLevelFront: ([coverFrontPath])
    # defini le chemin du coverFront manuellement ou automatiquement selon un algorythme
    # du + precis au + global
    #function setCoverFront(){
    function ffmpeg-cover-front-location-path(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        # set manuel
        ffmpeg_cover_front_location_path['Cover (front)']="${1:-""}";
        if [ -n "${ffmpeg_cover_front_location_path['Cover (front)']}" ]; then fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE; fi

        # set automatique
        #ffmpeg_cover_front_location_path['Cover (front)']='';

        local _cover="${ffmpeg_cover_level_path['yt_file_JPG']}";
        if [ -f "$_cover" ];then ffmpeg_cover_front_location_path['Cover (front)']="$_cover"; fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;  fi

        local _cover="${ffmpeg_cover_level_path['yt_file_PNG']}";
        if [ -f "$_cover" ];then ffmpeg_cover_front_location_path['Cover (front)']="$_cover"; fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;  fi

        local _cover="${ffmpeg_cover_level_path['yt_file']}";
        if [ -f "$_cover" ];then ffmpeg_cover_front_location_path['Cover (front)']="$_cover"; fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;  fi

        local _cover="${ffmpeg_cover_level_path['yt_page']}";
        if [ -f "$_cover" ];then ffmpeg_cover_front_location_path['Cover (front)']="$_cover"; fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;  fi

        fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
    }
#


##################
# TELECHARGEMENT #
##################
    ## -- Telechargement de cover -- ##

    # ffmpeg-cover-dl "lien" "nomFichier[.ext]" ["description"] ["coverLevel"]
    # ajoute un cover (copie l'extension du lien _source au fichier _destination_pathname)
    # et l'associe a un location de cover 
    # https://unix.stackexchange.com/questions/84915/add-album-art-cover-to-mp3-ogg-file-from-command-line-in-batch-mode
    # lame --ti logo.jpg file.mp3
    # https://id3.org/id3v2.3.0#Attached_picture
    function ffmpeg-cover-dl(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        if [ $# -eq 0 ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}[$?/1-4](lien nomFichier[.ext] [description] [coverType=front}]) ($*)";
            fct-pile-app-out $E_ARG_REQUIRE $_fct_pile_app_level; return $E_ARG_REQUIRE;
        fi

        local _source="${1:-""} ";  #url
        if [ -z "$_source" ];then  fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD; fi

        titre4 "Téléchargement du cover: $_source";
        local _source_ext=${_source##*.}

        local _destination_pathname=${2:-"$titreNormalize"} #avec sou sans ext
        local _destination_ext="${_destination_pathname##*.}";

        ffmpeg-cover-level ${4:-"$ffmpeg_cover_level"}

        ffmpeg_cover_level_urls["$ffmpeg_cover_level"]="$_source";

        ffmpeg_cover_level_descriptions["$ffmpeg_cover_level"]="${3:-""}";

        # gestion des extentions non valide
        
        if ! isExtensionValide "$_source_ext"
        then
            notifsAddEcho "Extension $_destination_pathname.$_source_ext non valide."
            fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
        fi

        # Si _destination_pathname n'a pas d'extension (nom==ext) 
        if [ "$_destination_pathname" = "$_destination_ext" ]
        then
            _destination_ext="${_source_ext,,}"; #lowercase
            _destination_pathname="${_destination_pathname}.${_destination_ext}";
        fi

        #normalize le chemin de la _destination_pathname
        text-normalize "$_destination_pathname";
        ffmpeg_cover_level_path["$ffmpeg_cover_level"]="$PWD/$text_normalize";
        local _path="${ffmpeg_cover_level_path["$ffmpeg_cover_level"]}"

        if [ ! -f "$_path" ]
        then
            #ffmpeg-cover-display-vars
            eval-echo "wget       --no-verbose --continue --show-progress --progress=bar --output-document=\"$_path\" \"$_source\"";
            if [ $erreur_no -ne 0 ]
            then
                erreurs-pile-add-echo "${FUNCNAME[0]}($_destination_pathname): Erreur lors du téléchargement"
            fi
        else
            echo "Cover '$_destination_pathname' déja téléchargé.";
        fi

        # mise a jours de location
        ffmpeg_cover_front_location_path["$ffmpeg_cover_location"]="${ffmpeg_cover_level_path["$ffmpeg_cover_level"]}";
        ffmpeg_cover_location_descritions["$ffmpeg_cover_location"]="${ffmpeg_cover_level_descriptions["$ffmpeg_cover_level"]}";

        fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
    }


    # ffmpeg-cover-dl-pageYT( URLLogo )
    # telecharge le logo (fournis en parametre) d'une page YT
    function ffmpeg-cover-dl-pageYT(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        if [ $# -ne 1 ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}() n'a pas le bon nombre de parametre(?#/1). ${FUNCNAME[0]} ( URLLogo )";
            fct-pile-app-out $E_ARG_REQUIRE $_fct_pile_app_level; return $E_ARG_REQUIRE;
        fi

        local url="$1";
        local destinationName="$auteurNormalize-logoYT"
        local destinationPath="$PWD/$destinationName.webp"; #VERIFIER SI format wep systematique!
        titre4 "Téléchargement du cover youtube: $auteurNormalize"

        ffmpeg_cover_level_urls['yt_page']='';
        ffmpeg_cover_level_path['yt_page']='';

        if [[ -f "$destinationPath" ]]
        then
            notifsAddEcho "${FUNCNAME[0]}($*): $destinationPath déjà téléchargé."
            ffmpeg_cover_level_urls['yt_page']="$url";
            ffmpeg_cover_level_path['yt_page']="$destinationPath";
        else
            if [[ "${url:0:4}" == 'http' ]]
            then
                ffmpeg_cover_level_urls['yt_page']="$url"
                echo wget --output-document="$destinationPath"  "$url";
                        wget --output-document="$destinationPath"  "$url";
                if [ $? -eq 0 ]
                then
                    ffmpeg_cover_level_path['yt_page']="$destinationPath";
                else
                erreurs-pile-add-echo "Erreur lors du téléchargement du cover yt: $url"
                fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
                fi
            else
                erreurs-pile-add-echo "Le lien du thumbnail du cover est NI un fichier NI un http: $url"
                fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
            fi
        fi

        #file "${coverLevel['yt_page_path']}"
        #display-vars 'url'                "$url"
        #display-vars 'destinationPath'    "$destinationPath"
        #ffmpeg-cover-display-vars


        fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
    }


    # ffmpeg-cover-dl-fileYT( 'youtube-code' 'destinationPath(sans extension)' )
    # telecharge la miniature d'une video youtube et remplit les variable:
    # ffmpeg_cover_level_urls['yt_file'];ffmpeg_cover_level_path['yt_file'];
    function ffmpeg-cover-dl-fileYT(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        if ! $ffmpeg_cover_dl_use_dlyt; then return; fi
        if [ $# -ne 2 ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}[$?/2]( youtube-code destinationPath )($*)";
            fct-pile-app-out $E_ARG_REQUIRE $_fct_pile_app_level; return $E_ARG_REQUIRE;
        fi

        # on recupere l'adresse http de la miniature
        local _codeYT="$1";
        titre4 "Téléchargement de la miniature (cover) de la video YT: $_codeYT";
        local url=$(yt-dlp --get-thumbnail https://www.youtube.com/watch?v=$_codeYT);
        titreInfo "url: '$url'";

        local URLextension=${url##*.};    #display-vars 'URLextension' "$URLextension"
        
        local destinationExtention="$URLextension";
        local _ext="${destinationExtention:0:3}";#    _ext=${_ext,,}; {lowercase}
        #display-vars  'URLextension' "$URLextension" '_ext' "$_ext"
        if [[ "$_ext" == 'jpg' ]];then destinationExtention='jpg';fi
        local destinationName="$2-$_codeYT";
        local destinationPath="$PWD/$destinationName.$destinationExtention";

        ffmpeg_cover_level_urls['yt_file']='';
        ffmpeg_cover_level_path['yt_file']='';


        if [[ -f "$destinationPath" ]]
        then
            notifsAddEcho "${FUNCNAME[0]}(): $destinationName déjà téléchargé.";
            ffmpeg_cover_level_urls['yt_file']="$url";
            ffmpeg_cover_level_path['yt_file']="$destinationPath";
        else
            if [[ "${url:0:4}" == 'http' ]]
            then
                ffmpeg_cover_level_path['yt_file']="$url";
                #echo wget --output-document="$destinationPath"  "$url";
                wget      --output-document="$destinationPath"  "$url";
                if [ $? -eq 0 ]
                then
                    ffmpeg_cover_level_path['yt_file']="$destinationPath";
                else
                erreurs-pile-add-echo "${FUNCNAME[0]}(): Erreur lors du téléchargement du cover yt_file: $url";
                fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
                fi
            else
                erreurs-pile-add-echo "${FUNCNAME[0]}():Le lien du thumbnail du cover est NI un fichier NI un http: $url";
                fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
            fi

            ffmpeg_cover_level_path['yt_file_PNG']='';
            ffmpeg_cover_level_path['yt_file_JPG']='';

            #si webp -> convertir en jpg (pour ogg)
            if [[ "$URLextension" == 'webp' ]]
            then
                local destinationJPGPath="$PWD/$destinationName.jpg";
                titre4 "Convertion du cover $destinationName.webp vers jpg";
                if [[ -f "$destinationJPGPath" ]]
                then
                    ffmpeg_cover_level_path['yt_file_JPG']="$destinationJPGPath";
                else
                    ffmpeg $ffmpeg_params_globaux -loglevel quiet -i \"$destinationPath\" \"$destinationJPGPath\";
                    if [ $? -eq 0 ]
                    then
                        ffmpeg_cover_level_path['yt_file_JPG']="$destinationJPGPath";
                    fi
                fi
            fi
        fi
        #ffmpeg-cover-display-vars


        fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
    }
#



# calcul du tag FFMpeg (ffmpeg_cover_tag_str) pour ajouter le cover 
function ffmpeg-cover-tags-calc(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre4 "${FUNCNAME[0]}($*)";
    ffmpeg_cover_tag_str='';
    if [[ ! -f "${ffmpeg_cover_level_path["Cover (front)"]}" ]]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]}: Chemin du cover invalide (${ffmpeg_cover_level_path["Cover (front)"]})";
        fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
    fi

    ffmpeg_cover_front_tmp_pathname="${fileName}.tags.${extension}"; # $fileName.tags.$extension
    if [ -f "$ffmpeg_cover_front_tmp_pathname" ]; then rm "$ffmpeg_cover_front_tmp_pathname";fi


    local _coverFrontTags='';
    #local _coverFrontPath="${ffmpeg_cover_level_path["Cover (front)"]}\";
    #_coverFrontTags="  -i \"${ffmpeg_cover_level_path["Cover (front)"]}\" -map 1:0 -metadata:s:1 title=\"${ffmpeg_cover_location_descritions["Cover (front)"]}\" -metadata:s:1 comment=\"Cover (front)\""

    # https://stackoverflow.com/questions/54717175/how-do-i-add-a-custom-thumbnail-to-a-mp4-file-using-ffmpeg
    #Using ffmpeg 4.0, released Apr 20 2018 or newer,
    #ffmpeg -i video.mp4 -i image.png -map 1 -map 0 -c copy -disposition:0 attached_pic out.mp4;
    #_coverFrontTags="ffmpeg $ffmpeg_params_globaux -i \"video.mp4\" -i \"image.png\" -map 1 -map 0 -c copy -disposition:0 attached_pic \"out.mp4\";";

    ffmpeg_cover_tag_str="$_coverFrontTags";
    display-vars 'ffmpeg_cover_front_tmp_pathname' "$ffmpeg_cover_front_tmp_pathname";
    display-vars 'ffmpeg_cover_tag_str           ' "$ffmpeg_cover_tag_str";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}



###############
# INTEGRATION #
###############
# Ajout du cover #
function ffmpeg-cover-add-local(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    ffmpeg-cover-tags-calc;

    local _coverFrontPath="${ffmpeg_cover_level_path["Cover (front)"]}";
    titre4 "Sauvegarde du cover comme metadatas(_coverFrontPath): $_coverFrontPath";
    eval-echo "ffmpeg $ffmpeg_params_globaux$ffmpeg_loglevelTags -i \"$fileName\"$_coverFrontPath -codec copy$tagVersion $ffmpeg_cover_tag_str \"$ffmpeg_cover_front_tmp_pathname\""
    if [ $erreur_no -ne 0 ]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]}:$WARN erreur avec $ffmpeg_cover_front_tmp_pathname ($_coverFrontPath)"
        #ls -s "$tmpFile"
    else #ajout du cover sans erreur
        echo "${FUNCNAME[0]}:Ajout du cover $ffmpeg_cover_front_tmp_pathname ($_coverFrontPath)"
        #ls -l "$tmpFile"
        eval_echo_D "mv \"$ffmpeg_cover_front_tmp_pathname\" \"$fileName\""
    fi

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}



# -- AJOUT DU COVER DANS LE FICHIER -- ##
# ffmpeg-cover-save( coverLevel [coverType] ){
# sauvegarde la cover dans le fichier en tant que metadata
function ffmpeg-cover-save(){
    if $isTrapCAsk ;then return $E_CTRL_C;fi
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;


    if [ $# -eq 0 ]
    then
        erreurs-pile-add-echo "$LINENO: ${FUNCNAME[0]}() n'a pas le bon nombre de parametre($#/1-2). ${FUNCNAME[0]} (filename [coverLevel])";
         fct-pile-app-out $E_ARG_REQUIRE $_fct_pile_app_level; return $E_ARG_REQUIRE;
    fi

    local _coverLevel="$1",
    local _coverType=${2:-"$FFMPEG_COVER_LOCATION_DEFAULT"}

    #getcoverLevel

    titre4 "Sauvegarde du cover comme metadatas: $_coverPath"
    if [[ "$_coverLevel" != '' ]]
    then
        local _coverPath="${ffmpeg_cover_front_location_path[$_coverType]}";
        if [[ ! -f "$_coverPath" ]]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}($*): Erreur lors de l'ajout du cover $_coverPath"
            ffmpeg-cover-add-ogg "$fileName" "$_coverPath";
        fi
    else
        erreurs-pile-add-echo "${FUNCNAME[0]}($*): '$_coverLevel non définit'"
    fi

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

#ffmpeg-cover-add-ogg ( fichier.ogg cover.png|cover.jpg)
function ffmpeg-cover-add-ogg(){
    if $isTrapCAsk ;then return $E_CTRL_C;fi
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;


    local oggPath="$1"
    local coverLevel="$2"

    titre4 "Sauvegarde du cover comme metadatas(_coverFrontPath): $oggPath"
    if [[ ! -f "${oggPath}" ]]
    then
        erreurs-pile-add-echo "$FUNCAME($*): fichier $oggPath introuvable."
        fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
    fi

    if [[ ! -f "${coverLevel}" ]]
    then
        erreurs-pile-add-echo "$FUNCAME($*): fichier $coverLevel introuvable."
        fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
    fi

    local imageMimeType=$(file -b --mime-location "${coverLevel}")

    if [ "${imageMimeType}" != "image/jpeg" ] && [ "${imageMimeType}" != "image/png" ]
    then
        erreurs-pile-add-echo "$FUNCAME($*): Le cover doit etre au format jpg ou png."
        fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
    fi

    local oggMimeType=$(file -b --mime-location "${oggPath}")

    if [ "${oggMimeType}" != "audio/x-vorbis+ogg" ] && [ "${oggMimeType}" != "audio/ogg" ]
    then
        erreurs-pile-add-echo "$FUNCAME($*): fichier $oggPath n'est pas au format ogg."
        fct-pile-app-out $E_ARG_BAD $_fct_pile_app_level; return $E_ARG_BAD;
    fi

    # Export existing comments to file
    local commentsPath="$(mktemp -t "tmp.XXXXXXXXXX")"
    vorbiscomment --list --raw "${oggPath}" > "${commentsPath}"

    # Remove existing images
    sed -i -e '/^metadata_block_picture/d' "${commentsPath}"

    # Insert cover image from file

    # metadata_block_picture format
    # See: https://xiph.org/flac/format.html# metadata_block_picture
    local imageWithHeader="$(mktemp -t "tmp.XXXXXXXXXX")"
    local description=''

    # Reset cache file
    echo -n '' > "${imageWithHeader}"

    # Picture location <32>
    printf "0: %.8x" 3 | xxd -r -g0 >> "${imageWithHeader}"

    # Mime location length <32>
    printf "0: %.8x" $(echo -n "${imageMimeType}" | wc -c) | xxd -r -g0 >> "${imageWithHeader}"

    # Mime location (n * 8)
    echo -n "${imageMimeType}" >> "${imageWithHeader}"

    # Description length <32>
    printf "0: %.8x" $(echo -n "${description}" | wc -c) | xxd -r -g0 >> "${imageWithHeader}"

    # Description (n * 8)
    echo -n "${description}" >> "${imageWithHeader}"

    # Picture with <32>
    printf "0: %.8x" 0 | xxd -r -g0  >> "${imageWithHeader}"

    # Picture height <32>
    printf "0: %.8x" 0 | xxd -r -g0 >> "${imageWithHeader}"

    # Picture color depth <32>
    printf "0: %.8x" 0 | xxd -r -g0 >> "${imageWithHeader}"

    # Picture color count <32>
    printf "0: %.8x" 0 | xxd -r -g0 >> "${imageWithHeader}"

    # Image file size <32>
    printf "0: %.8x" $(wc -c "${coverLevel}" | cut --delimiter=' ' --fields=1) | xxd -r -g0 >> "${imageWithHeader}"

    # Image file
    cat "${coverLevel}" >> "${imageWithHeader}"

    echo "metadata_block_picture=$(base64 --wrap=0 < "${imageWithHeader}")" >> "${commentsPath}"

    # Update vorbis file comments
    vorbiscomment --write --raw --commentfile "${commentsPath}" "${oggPath}"

    # Delete temp files
    rm "${imageWithHeader}"
    rm "${commentsPath}"

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}
