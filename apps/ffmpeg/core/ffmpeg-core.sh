echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh --show-libsLevel2 ffmpeg lib
# date de création    : 202?
# date de modification: 2024.09.04


# https://www.malekal.com/comment-utiliser-ffmpeg-exemples/
# recadrage: 
# https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Vector_Video_Standards.png/1024px-Vector_Video_Standards.png
# ratio
# 


##############################
# ffmpeg-container-is_valide #
##############################
    # ffmpeg-container-is_valide ( pathname )
    function ffmpeg-container-is_valide(){
        local -i _fct_pile_app_level=3;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _pathname="${1:-""}";
        ffmpeg_container_is_valide=false;
        if [ "$_pathname" = "" ];then  fct-pile-app-out $E_ARG_REQUIRE $_fct_pile_app_level; return $E_ARG_REQUIRE;fi

        local _container="${_pathname##*.}";

        if [[ "${ffmpeg_valideContainers[*]}" =~ $_container ]] # pas de quote
        then
            ffmpeg_container_is_valide=true;
            fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
        fi

        fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
    }
#


###############
# ffmpeg-uuid #
###############
    declare -g ffmpeg_uuid='';

    # clear; gtt.sh -d --libExecAuto apps/ffmpeg/tests/ffmpeg-core/ test-fct-ffmpeg-uuid
    function ffmpeg-uuid(){
        local -i _fct_pile_app_level=3;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        
        if [ -z "${1:-""}" ]
        then ffmpeg_uuid='';
        else ffmpeg_uuid="-$(cat /proc/sys/kernel/random/uuid)";
        fi
        #ffmpeg_log_pathname="$tmp_rep/${ffmpeg_log_name}${ffmpeg_uuid}.log";

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }
#


##################
# FORMATS VIDEOS #
##################

    # https://fr.wikipedia.org/wiki/Format_d%27affichage_vid%C3%A9o
    # https://stackovercoder.fr/superuser/810471/remove-mp4-video-top-and-bottom-black-bars-using-ffmpeg
    declare -A ffmpeg_formatVideos=();
        # 16/10: ratio:1.6
        ffmpeg_formatVideos["320/200"]='CGA';
        ffmpeg_formatVideos["1680/1050"]='WSXGA+';
        ffmpeg_formatVideos["1920/1200"]='WUXGA';
        ffmpeg_formatVideos["2560/1600"]='WQXGA';

        # 4/3: ratio:1.33
        ffmpeg_formatVideos["320/240"]='QVGA';
        ffmpeg_formatVideos["640/480"]='VGA';
        ffmpeg_formatVideos["800/600"]='SVGA';
        ffmpeg_formatVideos["1024/768"]='XGA';
        ffmpeg_formatVideos["1280/960"]='1280x960';
        ffmpeg_formatVideos["1400/1050"]='SXGA+';
        ffmpeg_formatVideos["1600/1200"]='UXGA';
        ffmpeg_formatVideos["2048/1536"]='QXGA';
        
        #3/2: ratio:1.5
        ffmpeg_formatVideos["720/480"]='NTSC';
        ffmpeg_formatVideos["1152/768"]='1152x768';
        ffmpeg_formatVideos["1280/854"]='1280x854';
        ffmpeg_formatVideos["1440/960"]='1440x960';

        # 16/9:1.77
        ffmpeg_formatVideos["854/480"]='WVGA';
        ffmpeg_formatVideos["1280/720"]='HD720';
        ffmpeg_formatVideos["1920/1080"]='HD1080';

        # 5/4: 1.25
        ffmpeg_formatVideos["1280/1050"]='SXGA';
    #


    # = ffmpeg_setInfos_* = #
    function ffmpeg-setInfos-definitionX(){
        local -i _fct_pile_app_level=3;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        ffmpeg_infos_definitionX=${1:-1};
        if [[ "$ffmpeg_infos_definitionX" =~ ' ' ]];then ffmpeg_infos_definitionX=''; fi;    # display-vars 'ffmpeg_infos_definitionX' "$ffmpeg_infos_definitionX";

        #if [ $((ffmpeg_infos_definitionX%2)) = 1 ]
        #then #echo "$LINENO:$ffmpeg_infos_definitionX:impaire";    # impaire
        #    ((ffmpeg_infos_definitionX-=1));
        #fi

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }


    function ffmpeg-setInfos-definitionY(){
        local -i _fct_pile_app_level=3;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        ffmpeg_infos_definitionY=${1:-1};
        if [[ "$ffmpeg_infos_definitionY" =~ ' ' ]];then ffmpeg_infos_definitionY=''; fi;    # display-vars 'ffmpeg_infos_definitionY' "$ffmpeg_infos_definitionY";

        #if [ $((ffmpeg_infos_definitionY%2)) = 1 ]
        #then #echo "$LINENO:$ffmpeg_infos_definitionY:impaire";    # impaire
        #    ((ffmpeg_infos_definitionY-=1));
        #fi

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }
#




return 0;
