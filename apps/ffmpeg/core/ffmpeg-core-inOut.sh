echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh --show-libsLevel2 apps/ffmpeg/ffmpeg.sh lib
# date de création    : 2024.08.04
# date de modification: 2024.08.24

# Description: objet pour manipumler les ffmpeg_outs (repertoires / fichiers)
# Decompose un chemin ex
# /root/rep1/rep1/fichier_nom.fichier.ext
#  ffmpeg_in_rep='/root/rep1/rep1';                                 # sans le slash de fin
#  ffmpeg_in_pathname='/root/rep1/rep1/fichier_nom.fichier.ext';    # chemin complet
#  ffmpeg_in_name='fichier_nom.fichier_ext';                        # nom + ext
#  ffmpeg_in_nom='fichier_nom';                                     # nom du fichier seul
#  ffmpeg_in_ext='fichier_ext';                                     # extention du fichier seul

# Dépendances: 
# * ffmpeg_in_cloneToOut;
# * ffmpeg_infos_pathname

# test
# clear; gtt.sh -d --libExecAuto apps/ffmpeg/tests/test-obj-ffmpeg-inOut-pathname test-obj-ffmpeg-inOut-pathname

#############################
# objet: ffmpeg-in-pathname #
#############################
    declare -g  ffmpeg_in_rep='';
    declare -g  ffmpeg_in_pathname='';
    declare -g  ffmpeg_in_name='';
    declare -g  ffmpeg_in_nom='';
    declare -g  ffmpeg_in_ext='';
    #declare -g  ffmpeg_uuid="-$(cat /proc/sys/kernel/random/uuid)";
    declare -g  ffmpeg_uuid='';
    declare -g  ffmpeg_log_name='ffmpeg_tail';                 # fichier de sortie de ffmpeg
    declare -g  ffmpeg_log_pathname="$tmp_rep/${ffmpeg_log_name}$ffmpeg_uuid.log";

    function ffmpeg-in-init(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        ffmpeg_in_rep='';
        ffmpeg_in_pathname='';
        ffmpeg_in_name='';
        ffmpeg_in_nom='';
        ffmpeg_in_ext='';
        #ffmpeg_infos_sizeWC;       # hérité de ffmpeg-infos
        #ffmpeg_log_pathname;       # hérité de ffmpeg-infos
        ffmpeg_infos_refresh=true;

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }

    #function ffmpeg-in-display-vars()
    # Affiche sous forme de display-vars les variables d'informations
    function ffmpeg-in-display-vars(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        display-vars 'ffmpeg_in_rep      ' "$ffmpeg_in_rep";
        display-vars 'ffmpeg_in_pathname ' "$ffmpeg_in_pathname";
        display-vars 'ffmpeg_in_name     ' "$ffmpeg_in_name";
        display-vars 'ffmpeg_in_nom      ' "$ffmpeg_in_nom";
        display-vars 'ffmpeg_in_ext      ' "$ffmpeg_in_ext";
        display-vars 'ffmpeg_infos_sizeWC' "$ffmpeg_infos_sizeWC";
        display-vars 'ffmpeg_log_pathname' "$ffmpeg_log_pathname";

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }


    # ffmpeg-in-pathname( pathname )
    # definit les variables d'information du média
    # clone in dans out
    # set ffmpeg-infos-pathname

    function ffmpeg-in-pathname(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        #titre1 "${FUNCNAME[0]}()"
        #display-vars 'ffmpeg_in_pathname' "$1";
        # Ne pas utiliser init() ici
        ffmpeg_in_pathname="${1-""}";
        ffmpeg_in_rep="${ffmpeg_in_pathname%/*}";
        ffmpeg_in_name="${ffmpeg_in_pathname##*/}";
        ffmpeg_in_nom="${ffmpeg_in_name%.*}";
        ffmpeg_in_ext="${ffmpeg_in_name##*.}";
        ffmpeg_infos_sizeWC=0;
        if [ "$ffmpeg_in_rep" = "$ffmpeg_in_name" ];then ffmpeg_in_rep='';fi # en cas d'absence de rep

        ffmpeg-in-clone-to-out;
        ffmpeg-infos-pathname "$ffmpeg_in_pathname";

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }


    function ffmpeg-in-rep(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        ffmpeg_in_rep="${1:-""}";
        if [ "$ffmpeg_in_rep" = '' ]
        then ffmpeg_in_pathname="$ffmpeg_in_name";
        else ffmpeg_in_pathname="$ffmpeg_in_rep/$ffmpeg_in_name";
        fi

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }


    # Initialise ffmpeg-out-* avec ffmpeg-in-*
    function ffmpeg-in-clone-to-out(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        ffmpeg_out_pathname="$ffmpeg_in_pathname";
        ffmpeg_out_rep="$ffmpeg_in_rep";
        ffmpeg_out_name="$ffmpeg_in_name";
        ffmpeg_out_nom="$ffmpeg_in_nom";
        ffmpeg_out_sizeWC=0;
        ffmpeg_out_ext="$ffmpeg_in_ext";   # pk il a été désactivé?

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }

    function ffmpeg-in-sizeWC(){
        local _tmp="$(wc -c < "${ffmpeg_in_pathname}")";   # display-vars "${FUNCNAME[0]}():$LINENO:_tmp" "$_tmp";
        ffmpeg_in_sizeWC=${_tmp%% *};
        #display-vars "${FUNCNAME[0]}():$LINENO:ffmpeg_out_sizeWC  " "$ffmpeg_out_sizeWC";
        
    }

    function ffmpeg-in-sizeWC-compare() {
        ffmpeg-out-sizeWC;
        if [ $ffmpeg_out_sizeWC -eq 0 ]
        then
            erreurs-pile-add-echo "${ffmpeg_out_pathname} converti vide -> rm";
            rm "$ffmpeg_out_pathname";
        else
            titre4 "# - Comparaison in<->out - #";
            
            # - Mise à jours de la taille des fichiers - #
            ffmpeg-in-sizeWC;ffmpeg-out-sizeWC;
            # - Calcul du ratio et du delta - #
            local  _ratio='';
            _ratio="$(echo "scale=3; $ffmpeg_out_sizeWC * 100 / $ffmpeg_in_sizeWC" | bc)";
            local -i _delta=$(( ffmpeg_out_sizeWC - ffmpeg_in_sizeWC ))
            echo "ratio: $ffmpeg_in_sizeWC/$ffmpeg_out_sizeWC : ${_ratio}%. Delta: $_delta octets";
            #notifs-pile-add-echo "${FUNCNAME[0]}():${ffmpeg_out_pathname}, taille: $ffmpeg_out_sizeWC";
            
            # -- Afficher les images comparatives -#
            #if ! $_ffmpeg_out_isExist; then ffmpeg-compare-images "$ffmpeg_in_name" "$ffmpeg_out_name";fi # ss
        fi
    }


#


##############################
# objet: ffmpeg-out-pathname #
##############################
    declare -g  ffmpeg_out_rep='';
    declare -g  ffmpeg_out_pathname='';
    declare -g  ffmpeg_out_name='';
    declare -g  ffmpeg_out_nom='';
    declare -g  ffmpeg_out_ext='';
    declare -gi ffmpeg_out_sizeWC=0;

    function ffmpeg-out-init(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        ffmpeg_out_rep='';
        ffmpeg_out_pathname='';
        ffmpeg_out_name='';
        ffmpeg_out_nom='';
        ffmpeg_out_ext='';
        ffmpeg_out_sizeWC=0;

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }


    #function ffmpeg_in_display-var()
    # Affiche sous forme de display-vars les variables d'informations
    function ffmpeg-out-display-vars(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        display-vars 'ffmpeg_out_rep     ' "$ffmpeg_out_rep";
        display-vars 'ffmpeg_out_pathname' "$ffmpeg_out_pathname";
        display-vars 'ffmpeg_out_name    ' "$ffmpeg_out_name";
        display-vars 'ffmpeg_out_nom     ' "$ffmpeg_out_nom";
        display-vars 'ffmpeg_out_ext     ' "$ffmpeg_out_ext";
        display-vars 'ffmpeg_out_sizeWC  ' "$ffmpeg_out_sizeWC";

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }


    #ffmpeg_out-pathname( pathname|._ext )
    # definit les variables d'information du média
    function ffmpeg-out-pathname(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        ffmpeg-out-init;
        if [ "${1:-""}" = ""  ];then ffmpeg_out-init;return $E_FALSE;fi
        ffmpeg_out_pathname="$1";
        #ffmpeg_out_pathname="$1";
        ffmpeg_out_rep="${ffmpeg_out_pathname%/*}";       # display-vars 'ffmpeg_out_rep' "$ffmpeg_out_rep";
        ffmpeg_out_name="${ffmpeg_out_pathname##*/}";     # display-vars 'ffmpeg_out_name' "$ffmpeg_out_name";
        ffmpeg_out_nom="${ffmpeg_out_name%.*}";           # display-vars 'ffmpeg_out_nom' "$ffmpeg_out_nom";
        ffmpeg_out_ext="${ffmpeg_out_name##*.}";


        # - filtre:nom - ##################
        #ffmpeg_out_nom+='.';                       # Le nom d'origine finit par un point: on rajoute ce point. PK???


        if [ "${ffmpeg_out_name:0:1}" = '.' ]        # est-ce un fichier caché ? (.htaccess[.extention])
        then
            if [ "$ffmpeg_out_nom" = '' ]            # est ce un fichier caché seul (.htaccess)?
            then ffmpeg_out_nom="${ffmpeg_out_name}";     # 
            #else  ffmpeg_out_nom="${ffmpeg_out_nom}";    # non (.fichierCaché.extention). On change pas le nom, il est dajà filtré
            fi
            if [ "$ffmpeg_out_ext" = "${ffmpeg_out_nom:1}" ]  # Si l'extention egal au nom alors il y a pas d'extention
            then
                ffmpeg_out_ext='';
            fi
        fi


        # - filtre:rep - #
        if [ "$ffmpeg_out_rep" = "$ffmpeg_out_nom" ];then ffmpeg_out_rep='';fi    # pathname ne contient aucun répertoire
        if [ "$ffmpeg_out_rep" = "" ]&&\
           [ "${ffmpeg_out_pathname:0:1}" = '/' ]
        then
            ffmpeg_out_rep='/';  # Le fichier est à la racine
        fi

        # - filtre:ext - #
        if [ "$ffmpeg_out_ext" = "$ffmpeg_out_nom" ];then ffmpeg_out_ext='';fi    # L'extention est vide
        return 0;

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }

    function ffmpeg-out-rep(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        ffmpeg_out_rep="${1:-""}";
        if [ "$ffmpeg_out_rep" = '' ]
        then ffmpeg_out_pathname="$ffmpeg_out_name";
        else ffmpeg_out_pathname="$ffmpeg_out_rep/$ffmpeg_out_name";
        fi

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }


    function ffmpeg-out-name(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        ffmpeg_out_name="${1:-""}";
        #ffmpeg_out_rep="${ffmpeg_out_pathname%/*}";
        ffmpeg_out_nom="${ffmpeg_out_name%.*}";
        ffmpeg_out_ext="${ffmpeg_out_name##*.}";
        #ffmpeg_out_name="${ffmpeg_out_nom}.${ffmpeg_out_ext}";
        if [ "$ffmpeg_out_rep" = '' ]
        then ffmpeg_out_pathname="$ffmpeg_out_name";
        else ffmpeg_out_pathname="$ffmpeg_out_rep/$ffmpeg_out_name";
        fi

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }

    function ffmpeg-out-nom(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        ffmpeg_out_nom="${1:-""}";
        ffmpeg_out_name="${ffmpeg_out_nom}.${ffmpeg_out_ext}";
        if [ "$ffmpeg_out_rep" = '' ]
        then ffmpeg_out_pathname="$ffmpeg_out_name";
        else ffmpeg_out_pathname="$ffmpeg_out_rep/$ffmpeg_out_name";
        fi

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }

    function ffmpeg-out-ext(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        ffmpeg_out_ext="${1:-""}";
        #ffmpeg_out_rep="${ffmpeg_out_pathname%/*}";
        #ffmpeg_out_nom="${ffmpeg_out_name%.*}";
        ffmpeg_out_name="${ffmpeg_out_nom}.${ffmpeg_out_ext}";
        if [ "$ffmpeg_out_rep" = '' ]
        then ffmpeg_out_pathname="$ffmpeg_out_name";
        else ffmpeg_out_pathname="$ffmpeg_out_rep/$ffmpeg_out_name";
        fi

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }

    function ffmpeg-out-sizeWC(){
        local _tmp="$(wc -c < "${ffmpeg_out_pathname}")";   # display-vars "${FUNCNAME[0]}():$LINENO:_tmp" "$_tmp";
        ffmpeg_out_sizeWC=${_tmp%% *};
        #display-vars "${FUNCNAME[0]}():$LINENO:ffmpeg_infos_sizeWC" "$ffmpeg_infos_sizeWC";
        #display-vars "${FUNCNAME[0]}():$LINENO:ffmpeg_out_sizeWC  " "$ffmpeg_out_sizeWC";
        
    }



#

return 0;
