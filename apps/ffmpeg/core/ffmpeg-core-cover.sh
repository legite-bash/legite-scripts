echo-d "${BASH_SOURCE[0]}" 1;

# date de création    : 2024.10
# date de modification: 2024.10.22
# clear; gtt.sh -d --show-libsLevel2 apps/ffmpeg/tests/test-ffmpeg-core-cover

###############
# DECLARATION #
###############

    # - la reference du cover dans le fichier - #
    declare -gr FFMPEG_COVER_LOCATION_DEFAULT='front';
    declare -g  ffmpeg_cover_emplacement="$FFMPEG_COVER_LOCATION_DEFAULT";  # l'emplacement en cours
    #declare -ga ffmpeg_cover_emplacement_liste=('Cover (front)' 'Cover (back)')
    declare -ga ffmpeg_cover_emplacement_liste=('front' 'back')
    declare -gA ffmpeg_cover_extention_liste=('ogg' 'mp3' 'mkv' 'png' 'jpg' 'jpeg' 'ogm');

    declare -gA ffmpeg_cover_urls=();           # Tableau: Contient les urls pour chaque emplacement des fichier à intégrer dans le fichier
    declare -gA ffmpeg_cover_pathnames=();      # Tableau: Contient les chemins pour chaque emplacement de cover à intégrer dans le fichier
    declare -gA ffmpeg_cover_descritions=();    # coverDescriptions["Cover (front)"]="Illustration de ..."

    declare -g  ffmpeg_cover_standalone_str='';   # Commande complete pour ffmpeg pour les fonction standalone
#


########
# init #
########
    function ffmpeg-cover-init(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        for _emplacement in "${ffmpeg_cover_emplacement_liste[@]}"
        do
            ffmpeg_cover_urls["$_emplacement"]='';
            ffmpeg_cover_pathnames["$_emplacement"]='';
            ffmpeg_cover_descritions["$_emplacement"]='';
        done

        fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
    }

    function ffmpeg-cover-display-vars(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        titre4 'covers';
        display-tableau-a 'ffmpeg_cover_emplacement_liste' "${ffmpeg_cover_emplacement_liste[@]}";
        display-vars 'FFMPEG_COVER_LOCATION_DEFAULT      '  "$FFMPEG_COVER_LOCATION_DEFAULT" 
        display-vars 'ffmpeg_cover_emplacement           '  "$ffmpeg_cover_emplacement";

        for _emplacement in "${ffmpeg_cover_emplacement_liste[@]}"
        do
            _path="${ffmpeg_cover_pathnames[$_emplacement]:-""}";
            display-vars "ffmpeg_cover_urls[$_emplacement]        " "${ffmpeg_cover_urls[$_emplacement]}";
            display-vars "ffmpeg_cover_pathnames[$_emplacement]   " "${ffmpeg_cover_pathnames[$_emplacement]}";
            display-vars "ffmpeg_cover_descriptions[$_emplacement]" "${ffmpeg_cover_descritions[$_emplacement]:-""}";
        done;

        fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
    }

    # ffmpeg-cover-extension-is_valide( 'element a tester' )
    function ffmpeg-cover-extension-is_valide(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _extention="${1,,}";
        if [ -z "$_extention" ];then fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_FALSE; fi

        for _element in "${ffmpeg_cover_extention_liste[@]}";
        do
            if [ "$_element" = "$_extention" ]; then fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;  fi
        done
        fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_FALSE;
    }

    # ffmpeg-cover-emplacement-is_valide( 'element a tester' )
    function ffmpeg-cover-emplacement-is_valide(){
        local _elt="${1}";
        if [ -z "$_elt" ];then return $E_FALSE;fi

        for _emplacement in "${ffmpeg_cover_emplacement_liste[@]}";
        do
            if [ "$_emplacement" = "$_elt" ]; then return $E_TRUE; fi
        done
        return $E_FALSE;
    }
#


###############
# HYDRATATION #
###############
    #        ffmpeg-cover-urls( 'emplacement' 'url' )
    function ffmpeg-cover-urls(){
        local _emplacement="${1:-""}";
        if [ -z "$_emplacement" ];then return $E_ARG_REQUIRE; fi
        if ! ffmpeg-cover-emplacement-is_valide  "$_emplacement"; then  return $E_ARG_BAD ; fi

        ffmpeg_cover_urls["$_emplacement"]="${2:-""}";
        return $E_TRUE;
    }

    #        ffmpeg-cover-pathnames( 'emplacement' 'pathanme' )
    function ffmpeg-cover-pathnames(){
        local _emplacement="${1:-""}";
        if [ -z "$_emplacement" ];then return $E_ARG_REQUIRE; fi
        if ! ffmpeg-cover-emplacement-is_valide  "$_emplacement"; then  return $E_ARG_BAD ; fi

        ffmpeg_cover_pathnames["$_emplacement"]="${2:-""}";
        return $E_TRUE;
    }

    #        ffmpeg-cover-descriptions( 'emplacement' 'description' )
    function ffmpeg-cover-descriptions(){
        local _emplacement="${1:-""}";
        if [ -z "$_emplacement" ];then return $E_ARG_REQUIRE; fi
        if ! ffmpeg-cover-emplacement-is_valide  "$_emplacement"; then  return $E_ARG_BAD ; fi

        ffmpeg_cover_descriptions["$_emplacement"]="${2:-""}";
        return $E_TRUE;
    }
#


#############
# SELECTION #
#############
    # ffmpeg-cover-emplacement ( [ffmpeg_cover_emplacement] )
    # définit l'empacement actuel
    # a quoi ca sert?:    
    function ffmpeg-cover-emplacement_obsolete(){
        local -i _fct_pile_app_level=1;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _ffmpeg_cover_emplacement="${1:-"$FFMPEG_COVER_LOCATION_DEFAULT"}";
        for   _emplacement in "${ffmpeg_cover_emplacement_liste[@]}"; 
        do
            if [ "$_emplacement" = "$_ffmpeg_cover_emplacement" ]; then ffmpeg_cover_emplacement="$_ffmpeg_cover_emplacement"; fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE; fi
        done;

        fct-pile-app-out $E_FALSE $_fct_pile_app_level; return $E_FALSE;
    }

#


##################
# TELECHARGEMENT #
##################
    # ffmpeg-cover-dl
    # 1- Telecharge les fichier contenu dans le tableau ffmpeg_cover_urls
    # 2- ffmpeg_cover_pathnames['$_emplacement']="$ffmpeg_in_rep/$ffmpeg_in_nom-$_emplacement.ffmpeg_in_ext";
    function ffmpeg-cover-dl(){
        if $isTrapCAsk ;then return $E_CTRL_C;fi
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        titre4 "${FUNCNAME[0]}()";
        local _url_ext='';
        local _pathname='';
        # Pour chaque emplacement
        for _emplacement in "${ffmpeg_cover_emplacement_liste[@]}"; 
        do
            #ffmpeg_cover_pathnames["$_emplacement"]='';        # Ne pas initialiser, l'utilisateur peut mettre le chemin directement
            if [ -n "${ffmpeg_cover_urls["$_emplacement"]}" ]
            then
                _url_ext="${ffmpeg_cover_urls["$_emplacement"]##*.}";    #display-vars '_url_ext' "$_url_ext";
                _pathname="$tmp_rep/$ffmpeg_in_nom-$_emplacement.$_url_ext";
                ffmpeg_cover_pathnames["$_emplacement"]="$_pathname";
                if [ ! -f "$_pathname" ]
                then
                    titre4 "${FUNCNAME[0]}(): Téléchargement";
                    eval-echo "wget -O \"$_pathname\" \"${ffmpeg_cover_urls["$_emplacement"]}\"";
                fi

                # - Convertion vers png - # 
                if [ "$_url_ext" != "png" ]
                then
                    if [ ! -f "$tmp_rep/$ffmpeg_in_nom-$_emplacement.png" ]
                    then
                        titre4 "${FUNCNAME[0]}(): Conversion vers png";
                        eval-echo "ffmpeg -hide_banner -i \"$_pathname\" -update 1 -map_metadata -1 \"$tmp_rep/$ffmpeg_in_nom-$_emplacement.png\" 2>> \"$ffmpeg_log_pathname\"";
                        ffmpeg_cover_pathnames["$_emplacement"]="$tmp_rep/$ffmpeg_in_nom-$_emplacement.png";
                    fi
                fi

                if [ ! -f "$_pathname" ]
                then
                    erreurs-pile-add-echo "cover['$_emplacement']: '$_pathname' erreur lors du téléchargement";
                fi

            fi
        done;

        fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
    }
#


########
# CALC #
########
    function ffmpeg-cover-calc() {
        local -i _fct_pile_app_level=5;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        # - telechargement - #
        ffmpeg-cover-dl;
        local _input_no=1;                  # 1: 2ème fichier d'entrée
        local _dest_no=1;                   # 2: 2ème piste vidéo

        if [ $debug_verbose_level -gt 0 ]; then ffmpeg-cover-display-vars; fi

        if [ -n "${ffmpeg_cover_pathnames['front']}" ]
        then
            if [ ! -f "${ffmpeg_cover_pathnames['front']}" ]
            then
                titreWarn "cover front: ${ffmpeg_cover_pathnames['front']}: introuvable!";
            else
                echo "Ajout du cover front: '${ffmpeg_cover_pathnames['front']}'";
                # code d'ajout d'un cover
                case "$ffmpeg_out_ext" in
                    "mp4" )
                        ffmpeg-params-input-add "${ffmpeg_cover_pathnames['front']}";
                        display-vars 'ffmpeg_params_input' "$ffmpeg_params_input";
                        ffmpeg-map-add "${_input_no}:v:0" "v:$_dest_no" '' 'Cover (front)' 'front cover';
                        ffmpeg-map-add-codec "copy";
                        ffmpeg-params-add "-disposition:$ffmpeg_map_dest attached_pic";
                        #ffmpeg-params-add "";
                        ((_input_no++)); ((_dest_no++));
                        ;;
                    
                    'mkv' )
                        ffmpeg-params-add "-attach ${ffmpeg_cover_pathnames['front']} -metadata:s:t mimetype=image/png";
                        ;;

                * ) erreurs-pile-add-echo "${FUNCNAME[0]}: Le format '$ffmpeg_out_ext' n'est pas prise en charge";
                esac
            fi
        fi

        fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
    }
#


###############
# INTEGRATION #
###############
# Integre les covers dans ffmpeg_in_pathname: fonction standalone #
function ffmpeg-cover-integrate-in-pathname(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre4 "# - in: $ffmpeg_in_pathname - #";
    #ffmpeg-infos-show    "$ffmpeg_in_pathname";
    #ffmpeg-streams-show  "$ffmpeg_in_pathname";
    #ffmpeg-metadata-show "$ffmpeg_in_pathname";

    ffmpeg-cover-dl;
    #ffmpeg-params-global-calc;
    #                                                                                                                                                                                       -map 1:0 -c copy         -id3v2_version 3 -metadata:s:v   title="Album cover"   -metadata:s:v   comment="Cover (front)"     out.mp3
    #eval-echo "ffmpeg $ffmpeg_params_global -i \"$ffmpeg_in_pathname\" -i \"${ffmpeg_cover_pathnames["$ffmpeg_cover_emplacement"]}\" -map 0:v:0 -codec:v:0 copy -map 0:a:0 -codec:a:0 copy  -map 1:0 -codec:v:1 copy -id3v2_version 3 -metadata:s:v:1 title=\"Album cover\" -metadata:s:v:1 comment=\"Cover (front)\" \"$ffmpeg_out_pathname\" 2>> \"$ffmpeg_log_pathname\";";
    ffmpeg-mappage # v0_c0
    #ffmpeg-methode-run;

    #titre4 "# - out: $ffmpeg_out_pathname - #";
    #ffmpeg-infos-show    "$ffmpeg_out_pathname";
    #ffmpeg-streams-show  "$ffmpeg_out_pathname";
    #ffmpeg-metadata-show "$ffmpeg_out_pathname";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


return 0;