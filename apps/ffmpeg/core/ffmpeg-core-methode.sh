echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh --show-libsLevel2 ffmpeg ffmpeg.sh lib
# date de création    : 2024.09.04
# date de modification: 2024.10.05


#################################
# - methode de fonctionnement - #
#################################
    declare -g  ffmpeg_methode='exec';              # exec|apply

    # - Fonctionnement - #
    function ffmpeg-methode()                   { ffmpeg_methode="${1:-"exec"}"; }
    function ffmpeg-methode-exec()              { ffmpeg_methode="exec"; }
    function ffmpeg-methode-metadata_apply()    { ffmpeg_methode="metadata_apply"; }

    function ffmpeg-methode-run(){
        case "$ffmpeg_methode" in
            'exec'          ) ffmpeg-exec;  ;;
            'metadata_apply') ffmpeg-metadata-apply; ;;
        esac
        return 0;
    }


###################
# ffmpeg-mappage #
##################
    declare -g ffmpeg_mappage='';
    function ffmpeg-mappage(){

        titre4 "Mappage";
        echo '# - va - #';
        titreInfo "v0_a0    - v0_fra_a0         -  v0_a0_51 - v0_a0_aac_ac2 - v0_fra_a0_aac_ac2 - v0_h264_a0";
        titreInfo "v0_a0_v1 - v0_fra|a0_aac_ac2";

        echo '# - vas - #';
        titreInfo "v0_a0_s0 - v0_a0_s0_F - v0_a0_51_s0_mov_text - v0_a0_s0_mov_text - v0_a0_s0_mov_text_F - v0_a0_s0_subrip - v0_a0_s0_subrip_F";

        echo '# - vaa - #';
        titreInfo "v0|a0|a1 - v0|a0:a0_ac2_D|a0:a1_51"

        echo '# - vaas - #';
        titreInfo "v0_a0_51_a0_F_aac_ac2_a0:1_51_s0_mov_text - v0|a1_fra:a0|a0_eng:a1|s0_mov_text"

        echo '# - vaass - #';
        titreInfo "v0|a0|a1|s0_mov_text_D|s1_mov_text";

        echo '# - vass - #';
        titreInfo "v0_a0_s0_f_s1 -  v0_a0_aac_ac2_s0_f_s1 - v0|a0_aac_ac2|s0_mov_text_f-|s1_mov_text - v0_a0_aac_s0_f_mov_text_s1_mov_text - v0_a0_s0_f_s1_mov_text - v0_a0_51_s0_f_s1";

        echo '# - vaaass - #';
        titreInfo " v0|a01:a0_ac2|a1:a1_51_fra|a0:a2_51_eng|s0_fra_mov_text|s1_eng_mov_text - v0|a0:a0_ac2|a0:a1_fra_51|a1:a2_eng_51|s1:s0_fra_mov_text_F|s0:s1_fra_mov_text";

        echo '# - vaaasss - #';
        titreInfo 'v0|a0:a0_ac2|a0:a1_fra_51|a1:a2_eng_51|s0:s0_fra_mov_text_F|s1:s1_fra_mov_text|s2:s2_eng_mov_text - v0_a0_51_a1_51_eng_a2_s0_s1_s2'

        echo '';
        display-vars 'ffmpeg_in_pathname ' "$ffmpeg_in_pathname";
        ffmpeg-metadata-show  "$ffmpeg_in_pathname";
        ffmpeg-streams-show   "$ffmpeg_in_pathname";
        display-vars 'ffmpeg_out_pathname' "$ffmpeg_out_pathname";
        display-vars 'ffmpeg_metadata_del' "$ffmpeg_metadata_del" 'ffmpeg-params-global-y' "$ffmpeg_params_global_y";
        read -p "Lancer la méthode n° ?[0/numéro]: " ffmpeg_mappage;

        titreInfo "Mappage: $ffmpeg_mappage";
        # scodec: mov_text | subrip
        case "$ffmpeg_mappage" in

            # - va - #
            "v0_a0" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0_fra_a0" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'fra' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;
            "v0_fra|a0_aac_ac2" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'fra' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'aac -ac 2';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;


            "v0_a0_51" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;
            "v0_a0_aac_ac2" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'aac -ac 2';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;
            "v0_fra_a0_aac_ac2" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'fra' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'aac -ac 2';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0_h264_a0" )
                #ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'h264'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0:a0_ac2|v1" )
                #ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'aac -ac 2';
                ffmpeg-map-add '0:v:1' 'v:1' 'eng' "";                          ffmpeg-map-add-codec 'copy'; # ffmpeg-params-add "";"
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;
            
            # - vas - #
            "v0_a0_s0" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'complet';                   ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0_a0_s0_F" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'forcé' ;                    ffmpeg-map-add-codec "copy -disposition:${ffmpeg_map_dest} forced";
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0_a0_51_s0_mov_text"  )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;
            "v0_a0_s0_mov_text" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'complet';                   ffmpeg-map-add-codec "mov_text"
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0_a0_s0_mov_text_F" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'forcé';                     ffmpeg-map-add-codec "mov_text -disposition:${ffmpeg_map_dest} default";
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0_a0_s0_subrip" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'complet';                   ffmpeg-map-add-codec 'subrip';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0_a0_s0_subrip_F" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' '(forcé)';                          ffmpeg-map-add-codec 'subrip';ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced -disposition:${ffmpeg_map_dest} default";
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            # - vaa - #
            "v0|a0|a1")
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'Stéréo';                    ffmpeg-map-add-codec 'copy';   ffmpeg-params-add "-disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:1' 'a:1' 'eng' 'Stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;
            
            "v0|a0:a0_ac2_D|a0:a1_51" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'Stéréo';                    ffmpeg-map-add-codec 'aac -ac 2';   ffmpeg-params-add "-disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:0' 'a:1' 'fra' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            # - vaas - #
            "v0_a0_51_a0_F_aac_ac2_a0:1_51_s0_mov_text" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'Stéréo (forced)';           ffmpeg-map-add-codec 'aac -ac 2';   ffmpeg-params-add "-disposition:${ffmpeg_map_dest} default -disposition:${ffmpeg_map_dest} forced";
                ffmpeg-map-add '0:a:0' 'a:1' 'fra' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;
            "v0|a1_fra:a0|a0_eng:a1|s0_mov_text" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:1' 'a:0' 'fra' 'Stéréo (forced)';           ffmpeg-map-add-codec 'aac -ac 2';   ffmpeg-params-add "-disposition:${ffmpeg_map_dest} default -disposition:${ffmpeg_map_dest} forced";
                ffmpeg-map-add '0:a:0' 'a:1' 'eng' 'Stéréo';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            # - vaass - #
            "v0|a0|a1|s0_mov_text_D|s1_mov_text" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'Stéréo';                    ffmpeg-map-add-codec 'copy';   ffmpeg-params-add "-disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:1' 'a:1' 'eng' 'Stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-map-add '0:s:1' 's:1' 'eng' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            # - vass - #
            "v0_a0_s0_f_s1" )
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.srt\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.srt\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'forcé' ;                    ffmpeg-map-add-codec 'copy'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:s:1' 's:1' 'fra' 'complet';                   ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0_a0_s0_f_s1_mov_text" )
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.srt\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.srt\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'forcé' ;                    ffmpeg-map-add-codec 'copy'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:s:1' 's:1' 'fra' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0_a0_aac_ac2_s0_f_s1" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'aac -ac 2';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'forcé' ;                    ffmpeg-map-add-codec 'copy'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:s:1' 's:1' 'fra' 'complet';                   ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0|a0_aac_ac2|s0_mov_text_f-|s1_mov_text" )
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'aac -ac 2';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'forcé' ;                    ffmpeg-map-add-codec 'copy'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:s:1' 's:1' 'fra' 'complet';                   ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            "v0_a0_aac_s0_f_mov_text_s1_mov_text" )
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.srt\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.srt\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'aac';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'forcé' ;                    ffmpeg-map-add-codec 'mov_text'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:s:1' 's:1' 'fra' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;


            "v0_a0_51_s0_f_s1" )
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.srt\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.srt\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'forcé' ;                    ffmpeg-map-add-codec 'copy'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:s:1' 's:1' 'fra' 'complet';                   ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            # - vaass - #';
            "v0|a01:a0_ac2|a1:a1_51_fra|a0:a2_51_eng|s0_fra_mov_text|s1_eng_mov_text" )
                #eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                #eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                #eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:2 \"${tmp_rep}/${ffmpeg_in_name}-map2.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:1' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'aac -ac 2'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:1' 'a:1' 'fra' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:a:0' 'a:2' 'eng' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'complet';                   ffmpeg-map-add-codec 'mov_text'; 
                ffmpeg-map-add '0:s:1' 's:1' 'eng' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;
            "v0|a0:a0_ac2|a0:a1_fra_51|a1:a2_eng_51|s1:s0_fra_mov_text_F|s0:s1_fra_mov_text" )
                #eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                #eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                #eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:2 \"${tmp_rep}/${ffmpeg_in_name}-map2.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'aac -ac 2'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:0' 'a:1' 'fra' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:a:1' 'a:2' 'eng' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:1' 's:0' 'fra' 'forcé' ;                    ffmpeg-map-add-codec 'mov_text'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:s:0' 's:1' 'fra' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;

            # - vaaaasss - #';
            "v0|a0:a0_ac2|a0:a1_fra_51|a1:a2_eng_51|s0:s0_fra_mov_text_F|s1:s1_fra_mov_text|s2:s2_eng_mov_text" )
                #eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                #eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                #eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:2 \"${tmp_rep}/${ffmpeg_in_name}-map2.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'aac -ac 2'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:a:0' 'a:1' 'fra' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:a:1' 'a:2' 'eng' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:s:0' 's:0' 'fra' 'forcé' ;                    ffmpeg-map-add-codec 'mov_text'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:s:1' 's:1' 'fra' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-map-add '0:s:2' 's:2' 'eng' 'complet';                   ffmpeg-map-add-codec 'mov_text';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;



            "v0_a0_51_a1_51_eng_a2_s0_s1_s2" )
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:0 \"${tmp_rep}/${ffmpeg_in_name}-map0.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:1 \"${tmp_rep}/${ffmpeg_in_name}-map1.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                eval-echo "ffmpeg -y -i \"$ffmpeg_in_pathname\" -map 0:s:2 \"${tmp_rep}/${ffmpeg_in_name}-map2.ssa\" 2>> \"$ffmpeg_log_pathname\"";
                ffmpeg-params-preset-tune-crf-clear;
                ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
                ffmpeg-map-add '0:a:0' 'a:0' 'fra' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:a:1' 'a:1' 'eng' '5.1';                       ffmpeg-map-add-codec 'copy';
                ffmpeg-map-add '0:a:2' 'a:2' 'fra' 'stéréo';                    ffmpeg-map-add-codec 'copy'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:s:1' 's:0' 'fra' 'forcé' ;                    ffmpeg-map-add-codec 'copy'; ffmpeg-params-add "-disposition:${ffmpeg_map_dest} forced -disposition:${ffmpeg_map_dest} default";
                ffmpeg-map-add '0:s:0' 's:1' 'fra' 'complet';                   ffmpeg-map-add-codec 'copy'; 
                ffmpeg-map-add '0:s:2' 's:2' 'eng' 'complet';                   ffmpeg-map-add-codec 'copy';
                ffmpeg-methode-exec;ffmpeg-methode-run;
                ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";
                ;;


            * )
                echo "Annulé";return $E_FALSE; ;;
        esac
        return 0;
    }
#


return 0;


