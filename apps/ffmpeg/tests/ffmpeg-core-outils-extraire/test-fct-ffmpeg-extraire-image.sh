echo "${BASH_SOURCE[0]}";

# gtt.sh --show-libsLevel2 apps/ffmpeg/tests/test-fct-ffmpeg.sh lib
# date de création    : 2024.04.23
# date de modification: 2024.08.03



# clear;gtt.sh -d --libExecAuto apps/ffmpeg/tests/test-fct-ffmpeg-extraire-image test-fct-ffmpeg-extraire-image # - ss='HH:MM:SS.sss' src=pathname
addLibrairie 'test-fct-ffmpeg-extraire-image';
function      test-fct-ffmpeg-extraire-image(){
    
    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-outils-extraire.sh";
    # VIDEOTEST_PATHNAME declaré dans apps/ffmpeg/tests/_ini.cfg.sh

    tmp-cd;
    display-vars '$PWD' "$PWD";

    titre2   "ffmpeg-extraire-image; #sans parametre";
    eval-echo "ffmpeg-extraire-image";
    erreur-no-description-echo $?;

    titre2   "ffmpeg-extraire-image ''; #parametre vide";
    eval-echo "ffmpeg-extraire-image ''";
    erreur-no-description-echo $?;

    titre2   "ffmpeg-extraire-image '00:00:1.18';";
    eval-echo "ffmpeg-extraire-image '00:00:1.18';"
    erreur-no-description-echo $?;

    # test valide
    titre2   "ffmpeg-extraire-image \"$VIDEOTEST_PATHNAME\";";
    eval-echo "ffmpeg-extraire-image \"$VIDEOTEST_PATHNAME\"";
    erreur-no-description-echo $?;
    ffmpeg-extraire-image-displayVars;

    titre2   "ffmpeg-extraire-image \"$VIDEOTEST_PATHNAME\" '00:00:01.17';";
    eval-echo "ffmpeg-extraire-image \"$VIDEOTEST_PATHNAME\" '00:00:01.17'";
    erreur-no-description-echo $?;
    ffmpeg-extraire-image-displayVars;

    titre1 "${FUNCNAME[0]}:END";
    return 0;
}


#libExecAuto test-fct-ffmpeg-extraire-image

return 0;

