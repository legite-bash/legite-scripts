echo "${BASH_SOURCE[0]}";

# gtt.sh --show-libsLevel2 apps/ffmpeg/tests/test-fct-ffmpeg.sh lib
# date de création    : 2024.04.23
# date de modification: 2024.08.03


# clear;gtt.sh -d --libExecAuto apps/ffmpeg/tests/test-fct-ffmpeg-extraire-video test-fct-ffmpeg-extraire-video # - src=pathname [ss='HH:MM:SS.sss'] [to='HH:MM:SS.sss'] 
addLibrairie 'test-fct-ffmpeg-extraire-video';
function      test-fct-ffmpeg-extraire-video(){
    
    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-outils-extraire.sh";
    # VIDEOTEST_PATHNAME declaré dans apps/ffmpeg/tests/_ini.cfg.sh

    tmp-cd;
    display-vars '$PWD' "$PWD";

    #titre2   "ffmpeg-extraire-video; #sans parametre";
    #eval-echo "ffmpeg-extraire-video";
    #erreur-no-description-echo $?;

    #titre2   "ffmpeg-extraire-video ''; #parametre vide";
    #eval-echo "ffmpeg-extraire-video ''";
    #erreur-no-description-echo $?;

    titre2   "ffmpeg-extraire-video \"$VIDEOTEST_PATHNAME\";";
    eval-echo "ffmpeg-extraire-video \"$VIDEOTEST_PATHNAME\"";
    erreur-no-description-echo $?;
    ffmpeg-extraire-video-displayVars;
    eval-echo "ls -l '$ffmpeg_extraire_video_dest_pathname'";

    titre2   "ffmpeg-extraire-video \"$VIDEOTEST_PATHNAME\" '00:00:01.17' # -ss";
    eval-echo "ffmpeg-extraire-video \"$VIDEOTEST_PATHNAME\" '00:00:01.17'";
    erreur-no-description-echo $?;
    ffmpeg-extraire-video-displayVars;
    eval-echo "ls -l '$ffmpeg_extraire_video_dest_pathname'";

    titre2   "ffmpeg-extraire-video \"$VIDEOTEST_PATHNAME\" '' '00:00:06.10' # -to";
    eval-echo "ffmpeg-extraire-video \"$VIDEOTEST_PATHNAME\" '' '00:00:06.10'";
    erreur-no-description-echo $?;
    ffmpeg-extraire-video-displayVars;
    eval-echo "ls -l '$ffmpeg_extraire_video_dest_pathname'";

    titre2   "ffmpeg-extraire-video \"$VIDEOTEST_PATHNAME\" '00:00:05.50' '00:00:10.10' # -ss -to";
    eval-echo "ffmpeg-extraire-video \"$VIDEOTEST_PATHNAME\" '00:00:05.50' '00:00:10.10'";
    erreur-no-description-echo $?;
    ffmpeg-extraire-video-displayVars;
    eval-echo "ls -l '$ffmpeg_extraire_video_dest_pathname'";


    titre1 "${FUNCNAME[0]}:END";
    return 0;
}


#libExecAuto test-fct-ffmpeg-extraire-video

return 0;

