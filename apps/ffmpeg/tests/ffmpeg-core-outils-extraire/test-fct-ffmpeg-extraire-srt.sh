echo "${BASH_SOURCE[0]}";

# gtt.sh --show-libsLevel2 apps/ffmpeg/tests/ffmpeg-core-outils-extraire/test-fct-ffmpeg-extraire-srt.sh
# date de création    : 2024.09.19
# date de modification: 2024.09.19



# clear;gtt.sh -d --libExecAuto apps/ffmpeg/tests/ffmpeg-core-outils-extraire/test-fct-ffmpeg-extraire-srt.sh test-fct-ffmpeg-extraire-srt # - ss='HH:MM:SS.sss' src=pathname
addLibrairie 'test-fct-ffmpeg-extraire-srt';
function      test-fct-ffmpeg-extraire-srt(){
    
    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-outils-extraire.sh";
    # VIDEOTEST_PATHNAME declaré dans apps/ffmpeg/tests/_ini.cfg.sh

    tmp-cd;
    display-vars '$PWD' "$PWD";

    local _programme='ffmpeg-extraire-srt';

    titre3    "${_programme}; #sans parametre";
    eval-echo "${_programme}";
    erreur-no-description-echo $?;

    titre3    "${_programme} ''; #parametre vide";
    eval-echo "${_programme} ''";
    erreur-no-description-echo $?;

    # test valide
    titre3    "${_programme} \"$VIDEOTEST_PATHNAME\";";
    eval-echo "${_programme} \"$VIDEOTEST_PATHNAME\"";
    erreur-no-description-echo $?;


    titre1 "${FUNCNAME[0]}:END";
    return 0;
}


#libExecAuto test-fct-ffmpeg-extraire-srt

return 0;

