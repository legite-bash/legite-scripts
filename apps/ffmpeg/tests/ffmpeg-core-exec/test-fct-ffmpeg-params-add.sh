echo "${BASH_SOURCE[0]}";

# date de création    : 2024.08.29
# date de modification: 2024.09.05
# clear; gtt.sh --show-libsLevel2 apps/ffmpeg/tests/ffmpeg-core-exec

# clear; gtt.sh -d --libExecAuto apps/ffmpeg/tests/ffmpeg-core-exec test-fct-ffmpeg-params-add
addLibrairie 'test-fct-ffmpeg-params-add';
function      test-fct-ffmpeg-params-add(){
    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg.sh";

    libExecAuto "test-fct-ffmpeg-params-add--vide";
    libExecAuto "test-fct-ffmpeg-params-add--multiple";

    return 0;
}


# clear; gtt.sh -d  apps/ffmpeg/tests/ffmpeg-core-exec test-fct-ffmpeg-params-add--vide
addLibrairie 'test-fct-ffmpeg-params-add--vide';
function      test-fct-ffmpeg-params-add--vide(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";
    titreInfo "";

    local _programme="ffmpeg";


    titre4 'sans argument':
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-params-add";
    erreur-no-description-echo;
    ${_programme}-display-vars;


    titre4 'argument vide';
    #eval-echo "${_programme}-init";
    eval-echo "${_programme}-params-add ''";
    erreur-no-description-echo;
    ${_programme}-display-vars;
    
    titre4 'valeur: 0';
    #eval-echo "${_programme}-init";
    eval-echo "${_programme}-params-add 0";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    titre4 'valeur: -1';
    #eval-echo "${_programme}-init";
    eval-echo "${_programme}-params-add -1";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    titre4 'valeur: --1';
    #eval-echo "${_programme}-init";
    eval-echo "${_programme}-params-add --1";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    titre4 'valeur: $E_FALSE';
    #eval-echo "${_programme}-init";
    eval-echo "${_programme}-params-add $E_FALSE";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh -d  apps/ffmpeg/tests/ffmpeg-core-exec test-fct-ffmpeg-params-add--multiple
addLibrairie 'test-fct-ffmpeg-params-add--multiple';
function      test-fct-ffmpeg-params-add--multiple(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";
    titreInfo "";

    local _programme="ffmpeg";


    titre4 'multiple arguments arbitraire':
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-params-add '-v' '-arg2' arg3_sans_apostrophe";
    eval-echo "${_programme}-params-add '2ème passe'";
    erreur-no-description-echo;
    ${_programme}-display-vars;


    titre4 'multiple arguments correspondant à une commande prédéfinie':
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-params-add 'extrait-05-06mn' 'faststart' cropdetect";
    eval-echo "${_programme}-params-add vcopy";

    erreur-no-description-echo;
    ${_programme}-display-vars;


    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

return 0;
