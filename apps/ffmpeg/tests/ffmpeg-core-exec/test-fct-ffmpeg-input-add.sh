echo "${BASH_SOURCE[0]}";

# date de création    : 2024.08.29
# date de modification: 2024.09.05
# clear; gtt.sh --show-libsLevel2 apps/ffmpeg/tests/ffmpeg-core-exec

# clear; gtt.sh -d --libExecAuto apps/ffmpeg/tests/ffmpeg-core-exec test-fct-ffmpeg-input-add
addLibrairie 'test-fct-ffmpeg-input-add';
function      test-fct-ffmpeg-input-add(){
    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg.sh";

    libExecAuto "test-fct-ffmpeg-input-add--vide";

    return 0;
}


# clear; gtt.sh -d  apps/ffmpeg/tests/ffmpeg-core-exec test-fct-ffmpeg-input-add--vide
addLibrairie 'test-fct-ffmpeg-input-add--vide';
function      test-fct-ffmpeg-input-add--vide(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";
    titreInfo "";

    local _programme="ffmpeg";


    titre4 'sans argument':
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-input-add";
    erreur-no-description-echo;
    ${_programme}-display-vars;


    titre4 'argument vide';
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-input-add ''";
    erreur-no-description-echo;
    ${_programme}-display-vars;
    
    titre4 'valeur: 0';
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-input-add 0";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    titre4 'valeur: -1';
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-input-add -1";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    titre4 'valeur: --1';
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-input-add --1";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    titre4 'valeur: $E_FALSE';
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-input-add $E_FALSE";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

return 0;
