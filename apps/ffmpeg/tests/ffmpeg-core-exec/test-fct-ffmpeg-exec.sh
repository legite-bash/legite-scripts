echo "${BASH_SOURCE[0]}";

# date de création    : 2024.08.29
# date de modification: 2024.09.18
# clear; gtt.sh --show-libsLevel2 apps/ffmpeg/tests/ffmpeg-core-exec



# clear; gtt.sh -d --libExecAuto apps/ffmpeg/tests/ffmpeg-core-exec test-fct-ffmpeg-exec
addLibrairie 'test-fct-ffmpeg-exec';
function      test-fct-ffmpeg-exec(){
    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg.sh";

    libExecAuto "test-fct-ffmpeg-exec--vide";
    libExecAuto "test-fct-ffmpeg-exec--src-1_fichier";
    libExecAuto "test-fct-ffmpeg-exec--src-ffmpeg_in_pathname";
    return 0;
}


# clear; gtt.sh -d  apps/ffmpeg/tests/ffmpeg-core-exec test-fct-ffmpeg-exec--vide
addLibrairie 'test-fct-ffmpeg-exec--vide';
function      test-fct-ffmpeg-exec--vide(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";
    titreInfo "";

    local _programme="ffmpeg";


    titre4 'sans argument':
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-exec";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    titre4 'argument vide';
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-exec ''";
    erreur-no-description-echo;
    ${_programme}-display-vars;
    

    titre4 'valeur: 0';
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-exec 0";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    titre4 'valeur: -1';
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-exec -1";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    titre4 'valeur: --1';
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-exec --1";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    titre4 'valeur: $E_FALSE';
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-exec $E_FALSE";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh -d  apps/ffmpeg/tests/ffmpeg-core-exec test-fct-ffmpeg-exec--src-egal-dest
addLibrairie 'test-fct-ffmpeg-exec--src-egal-dest';
function      test-fct-ffmpeg-exec--src-egal-dest(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";
    titreInfo "ffmpeg-in-pathname ffmpeg-out-rep";

    local _programme="ffmpeg";

    titre4 'Définition de ffmpeg-in-pathname':
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-params-global-y-enable";

    eval-echo "ffmpeg-in-pathname  '$VIDEOTEST_PATHNAME'";
    
    eval-echo "${_programme}-exec '$ffmpeg_in_pathname' '$ffmpeg_out_pathname'";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh -d  apps/ffmpeg/tests/ffmpeg-core-exec test-fct-ffmpeg-exec--src-1_fichier
addLibrairie 'test-fct-ffmpeg-exec--src-1_fichier';
function      test-fct-ffmpeg-exec--src-1_fichier(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";
    titreInfo "Essais de destinations différentes";

    local _programme="ffmpeg";


    titre4 '1 fichier':
    eval-echo "${_programme}-init";
    #eval-echo "${_programme}-params-global-y-enable";

    eval-echo "${_programme}-exec '$VIDEOTEST_PATHNAME'";
    erreur-no-description-echo;
    ${_programme}-display-vars;


    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh -d  apps/ffmpeg/tests/ffmpeg-core-exec test-fct-ffmpeg-exec--src-ffmpeg_in_pathname
addLibrairie 'test-fct-ffmpeg-exec--src-ffmpeg_in_pathname';
function      test-fct-ffmpeg-exec--src-ffmpeg_in_pathname(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";
    titreInfo "ffmpeg-in-pathname ffmpeg-out-ext";

    local _programme="ffmpeg";

    titre4 'Définition de ffmpeg-in-pathname':
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-params-global-y-enable";

    eval-echo "ffmpeg-in-pathname  '$VIDEOTEST_PATHNAME'";
    eval-echo "ffmpeg-out-pathname '$VIDEOTEST_PATHNAME'";
    eval-echo "ffmpeg-out-ext      'mkv'";
    
    eval-echo "${_programme}-exec '$ffmpeg_in_pathname' '$ffmpeg_out_pathname'";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh -d  apps/ffmpeg/tests/ffmpeg-core-exec test-fct-ffmpeg-exec--src-ffmpeg_in_pathname-dest-rep
addLibrairie 'test-fct-ffmpeg-exec--src-ffmpeg_in_pathname-dest-rep';
function      test-fct-ffmpeg-exec--src-ffmpeg_in_pathname-dest-rep(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";
    titreInfo "ffmpeg-in-pathname ffmpeg-out-rep";

    local _programme="ffmpeg";

    titre4 'Définition de ffmpeg-in-pathname':
    eval-echo "${_programme}-init";
    #eval-echo "${_programme}-params-global-y-enable";

    eval-echo "ffmpeg-in-pathname  '$VIDEOTEST_PATHNAME'";
    eval-echo "ffmpeg-out-rep      '$tmp_rep'";
    
    eval-echo "${_programme}-exec '$ffmpeg_in_pathname' '$ffmpeg_out_pathname'";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


function ffmpeg-exec-callback-before-ext-to-mkv(){
    local -i _fct_pile_app_level=2;

    fct-pile-app-in "$*" $_fct_pile_app_level;
    ffmpeg-out-rep "$tmp_rep/../destination/";
    ffmpeg-out-ext 'mkv';

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}



# clear; gtt.sh -d  apps/ffmpeg/tests/ffmpeg-core-exec test-fct-ffmpeg-exec--src-ffmpeg_in_rep
addLibrairie 'test-fct-ffmpeg-exec--src-ffmpeg_in_rep';
function      test-fct-ffmpeg-exec--src-ffmpeg_in_rep(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;


    titre2 "${FUNCNAME[0]}()";
    titreInfo "ffmpeg-in-pathname ffmpeg-out-rep";


    
    titre4 'Définition de ffmpeg-in-pathname':
    eval-echo "ffmpeg-init";
    #eval-echo "${_programme}-params-global-y-enable";

    eval-echo "ffmpeg-in-init";
    eval-echo "ffmpeg-in-pathname  '${tmp_rep}/'";
    eval-echo "ffmpeg-exec-callback-before 'ffmpeg-exec-callback-before-ext-to-mkv'";

    titre4 'listing de ffmpeg-in-pathname':
    ls "${ffmpeg_in_pathname}";


    #titre4 'in/out':
    #ffmpeg-in-display-vars;
    #ffmpeg-out-display-vars;

    # - Execution - #
    hr;
    eval-echo "ffmpeg-exec '$ffmpeg_in_pathname'  '$ffmpeg_out_pathname' ";
    erreur-no-description-echo;
    hr;
    ffmpeg-display-vars;

    titre0 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

return 0;
