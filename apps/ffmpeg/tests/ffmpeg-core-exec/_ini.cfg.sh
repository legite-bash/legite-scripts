echo "${BASH_SOURCE[0]}";

# gtt.sh 
# date de création    : 2024.08.24
# date de modification: 2024.08.24
# Description: 
# clear;gtt.sh --show-libsLevel2 apps/ffmpeg/tests/

local _source_rep="${BASH_SOURCE%/*.*}";                        # display-vars '_source_rep' "$_source_rep";
local _source_rep_last="${_source_rep##*/}";                    # display-vars '_source_rep_last' "$_source_rep_last";
local _source_name="${BASH_SOURCE##*/}";                        # display-vars '_source_name' "$_source_name";
local _source_nom="${_source_name%.*}";                         # display-vars '_source_nom' "$_source_nom";
local _source_ext="${_source_name##*.}";                        # display-vars '_source_ext' "$_source_ext";

local _test_rep_rel="${_source_rep##$GTT_VERSION_SELECT_REP}";   display-vars '_test_rep_rel' "$_test_rep_rel";  # /tests/./


#################################################
titre1 "Préparations de tests: ${_test_rep_rel}";
#################################################


#############################################################
titre2 "Création du repertoire temporaire: ${_test_rep_rel}";
#############################################################
tmp-rep "$TMP_ROOT${_test_rep_rel}";
tmp-display-vars;


############################################################
titre2 "CHARGEMENT DES FICHIERS DE TESTS: ${_test_rep_rel}";
############################################################
for _fichier_pathname in $_source_rep/*.sh
do
    if [ "${_fichier_pathname##*/}" = '_ini.cfg.sh' ];then continue;fi  
    require-once "$_fichier_pathname";
done

############################################################
#titre2 "Telechargement du VIDEOTEST";
############################################################

VIDEOTEST-dw;


titreInfo "Pour connaitre les librairies tests disponibles: gtt.sh --show-libsLevel2 apps/gtt/sid/${_test_rep_rel}";



########
# MAIN #
########
return 0;
