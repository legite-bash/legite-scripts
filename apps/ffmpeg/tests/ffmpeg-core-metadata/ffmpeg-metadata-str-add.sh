echo "${BASH_SOURCE[0]}";

# date de création    : 2024.09.15
# date de modification: 2024.09.15
# clear; gtt.sh --show-libsLevel2 apps/ffmpeg/tests/ffmpeg-core-metadata/


# clear; gtt.sh --libExecAuto apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-str-add
addLibrairie 'test-ffmpeg-metadata-str-add';
function      test-ffmpeg-metadata-str-add(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-metadata.sh";

    local _programme="ffmpeg-metadata-str-add";

    libExecAuto "${_programme}--vide";
    libExecAuto "${_programme}--exemple-simple";

    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-str-add--vide
addLibrairie 'test-ffmpeg-metadata-str-add--vide';
function      test-ffmpeg-metadata-str-add--vide(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-metadata.sh";

    local _programme="ffmpeg-metadata-str-add";

    titre4 'sans argument':
    ffmpeg-metadata-init;
    eval-echo "$_programme";
    erreur-no-description-echo;
    ffmpeg-metadata-display-vars;

    titre4 'argument vide':
    ffmpeg-metadata-init;
    eval-echo "$_programme ''";
    erreur-no-description-echo;
    ffmpeg-metadata-display-vars;

    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-str-add--exemple-all
addLibrairie 'test-ffmpeg-metadata-str-add--exemple-all';
function      test-ffmpeg-metadata-str-add--exemple-all(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-metadata.sh";

    local _programme="ffmpeg-metadata-str-add";

    titre4 'tag: all':
    ffmpeg-metadata-init;
    eval-echo "ffmpeg-metadata-year 2024";
    erreur-no-description-echo;

    ffmpeg-metadata-calc;
    erreur-no-description-echo;

    ffmpeg-metadata-display-vars;


    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

return 0;
