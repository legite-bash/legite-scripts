echo "${BASH_SOURCE[0]}";

# date de création    : 2024.09.28
# date de modification: 2024.09.28
# clear; gtt.sh --show-libsLevel2 apps/ffmpeg/tests/ffmpeg-core-metadata/


# clear; gtt.sh --libExecAuto apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-actor
addLibrairie 'test-ffmpeg-metadata-actor';
function      test-ffmpeg-metadata-actor(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-metadata.sh";

    local _programme="ffmpeg-metadata-actor";

    libExecAuto "${_programme}--vide";
    libExecAuto "${_programme}--exemple-simple";

    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-actor--vide
addLibrairie 'test-ffmpeg-metadata-actor--vide';
function      test-ffmpeg-metadata-actor--vide(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-metadata.sh";

    local _programme="ffmpeg-metadata-actor";

    hr;
    echo "${ffmpeg_metadata_actor[@]}";

    display-vars 'nb'  "${#ffmpeg_metadata_actor[@]}";
    display-tableau-a 'ffmpeg_metadata_actor         ' "${ffmpeg_metadata_actor[@]}";

    titre4 'sans argument':
    ffmpeg-metadata-init;
    eval-echo "$_programme";
    erreur-no-description-echo;
    display-vars 'nb'  "${#ffmpeg_metadata_actor[@]}";
    display-tableau-a 'ffmpeg_metadata_actor         ' "${ffmpeg_metadata_actor[@]}";
    display-vars 'ffmpeg_metadata_str' "$ffmpeg_metadata_str";


    titre4 'argument vide':
    ffmpeg-metadata-init;
    eval-echo "$_programme ''";
    erreur-no-description-echo;
    display-vars 'nb'  "${#ffmpeg_metadata_actor[@]}";
    display-tableau-a 'ffmpeg_metadata_actor         ' "${ffmpeg_metadata_actor[@]:-}";
    display-vars 'ffmpeg_metadata_str' "$ffmpeg_metadata_str";


    eval-echo "$_programme 'acteur-1' 'acteur-2'";
    display-vars 'nb'  "${#ffmpeg_metadata_actor[@]}";
    display-tableau-a 'ffmpeg_metadata_actor         ' "${ffmpeg_metadata_actor[@]}";
    display-vars 'ffmpeg_metadata_str' "$ffmpeg_metadata_str";

    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


return 0;
