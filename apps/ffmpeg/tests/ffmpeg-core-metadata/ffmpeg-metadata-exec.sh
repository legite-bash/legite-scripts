echo "${BASH_SOURCE[0]}";

# date de création    : 2024.10.01
# date de modification: 2024.10.01
# clear; gtt.sh --show-libsLevel2 apps/ffmpeg/tests/ffmpeg-core-metadata/


# clear; gtt.sh --libExecAuto apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-exec
addLibrairie 'test-ffmpeg-metadata-exec';
function      test-ffmpeg-metadata-exec(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-metadata.sh";

    local _programme="ffmpeg-metadata-exec";

    libExecAuto "${_programme}--vide";
    libExecAuto "${_programme}---out-pathname";
    libExecAuto "${_programme}--exemple-simple";

    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-exec--vide
addLibrairie 'test-ffmpeg-metadata-exec--vide';
function      test-ffmpeg-metadata-exec--vide(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-metadata.sh";

    local _programme="ffmpeg-metadata-exec";

    titre4 'sans argument':
    ffmpeg-metadata-init;
    eval-echo "$_programme";
    erreur-no-description-echo;
    ffmpeg-metadata-display-vars;

    titre4 'argument vide':
    ffmpeg-metadata-init;
    eval-echo "$_programme ''";
    erreur-no-description-echo;
    ffmpeg-metadata-display-vars;

    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


function      test-ffmpeg-metadata-exec--metadata-init(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;


    ffmpeg-metadata-init;

    # -- langue -- #
    ffmpeg-metadata-language 'fra';
    # - Organization Information - #
    ffmpeg-metadata-track-no  10;
    ffmpeg-metadata-track-nb  20;


    # - Titles - #
    ffmpeg-metadata-title    'Titre de la vidéo';

    # - Entities - #

    #ffmpeg-metadata-director            'directeur-1' 'directeur-2';
    #ffmpeg-metadata-director-add        'directeur-add-1'  'directeur-add-2';

    #ffmpeg-metadata-composer            'composer-1';
    #ffmpeg-metadata-composer-add        'composer-2' 'composer-3';
    #ffmpeg-metadata-composer_nationality 'fra';

    #ffmpeg-metadata-conductor           'conductor-1';
    #ffmpeg-metadata-conductor-add       'conductor-2'  'conductor-3';

    ffmpeg-metadata-actor               'actor-1';
    ffmpeg-metadata-actor-add           'actor-2'  'actor-3';

    ffmpeg-metadata-character           'character-1';
    ffmpeg-metadata-character-add       'character-2'  'character-3';

    ffmpeg-metadata-written_by           'written_by-1';
    ffmpeg-metadata-written_by-add      'written_by-2'  'written_by-3';

    ffmpeg-metadata-producer            'producer-1';
    ffmpeg-metadata-producer-add        'producer-2'  'producer-3';

    ffmpeg-metadata-distributed_by      'distributed_by-1';
    ffmpeg-metadata-distributed_by-add  'distributed_by-2' 'distributed_by-3';

    ffmpeg-metadata-publisher           'publisher-1';
    ffmpeg-metadata-publisher-add       'publisher-2' 'publisher-3';

    ffmpeg-metadata-label               'label-1';
    ffmpeg-metadata-label-add           'label-2' 'label-3';

    ffmpeg-metadata-writer              'writer-1';
    ffmpeg-metadata-writer-add          'writer-2' 'writer-2';

    ffmpeg-metadata-artist              'artist-1';
    ffmpeg-metadata-artist-add          'artist-2' 'artist-3';

    ffmpeg-metadata-performer           'performer-1';
    ffmpeg-metadata-performer-add       'performer-2' 'artist-3';


    # Search and Classification - #
    ffmpeg-metadata-genre               'genre-1';
    ffmpeg-metadata-genre-add           'genre-2' 'genre-3';

    ffmpeg-metadata-album               "L'album à Moi";

    ffmpeg-metadata-support             "DVD";
    #ffmpeg-metadata-original_media_type ''
    ffmpeg-metadata-content_type        'Documentary';
    ffmpeg-metadata-subject             "";
    ffmpeg-metadata-keywords            "";
    ffmpeg-metadata-summary             "";
    ffmpeg-metadata-synopsis            "";
    ffmpeg-metadata-initial_key         "";


    # - Temporal Information - #
    ffmpeg-metadata-period              'medieval';
    ffmpeg-metadata-date                2024 '10-30';
    ffmpeg-metadata-date_tagged         '2024-12-01';
    ffmpeg-metadata-date_written        '1806-06-31';

    # - Spatial Information - #
    ffmpeg-metadata-composition_location     'composition_location-1';
    ffmpeg-metadata-composition_location-add 'composition_location-2' 'composition_location-3';

    # - Identifiers - #
    ffmpeg-metadata-ISRC "ISRC";
    ffmpeg-metadata-MCDI "MCDI";
    ffmpeg-metadata-ISBN "ISBN";
    ffmpeg-metadata-BARCODE "BARCODE";
    ffmpeg-metadata-CATALOG_NUMBER "CATALOG_NUMBER";
    ffmpeg-metadata-LABEL_CODE "LABEL_CODE";
    ffmpeg-metadata-LCCN "LCCN";
    ffmpeg-metadata-IMDB "IMDB";
    ffmpeg-metadata-TMDB "TMDB";
    ffmpeg-metadata-TVDB "TVDB";
    ffmpeg-metadata-TVDB2 "TVDB2";

    # - comment/description - #
    ffmpeg-metadata-url      'https://perdu.com';
    ffmpeg-metadata-url-add  'https://lost.fm' 'https://url-3';

    titre3 "Personalisation des pistes";
    ffmpeg-map-dest-select  'v:0' 'bra' "support";  
    ffmpeg-map-dest-select  'a:0' 'chi' 'stereo';         
    ffmpeg-map-dest-select  's:0' 'fra' 'ocean';         
    
    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-exec--exemple-VIDEOTEST_ORI
addLibrairie 'test-ffmpeg-metadata-exec--exemple-VIDEOTEST_ORI';
function      test-ffmpeg-metadata-exec--exemple-VIDEOTEST_ORI(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;
    titre1 "${FUNCNAME[0]}:BEGIN";

    ffmpeg-metadata-init;

    #titre2 "# - in - #";
    ffmpeg-in-pathname    "$VIDEOTEST_ORI_PATHNAME";
    ffmpeg-metadata-show  "$VIDEOTEST_ORI_PATHNAME";
    ffmpeg-streams-show   "$VIDEOTEST_ORI_PATHNAME";

    #titre2 "# - out - #";
    ffmpeg-metadata-del-disable;
    ffmpeg-out-rep "$tmp_rep";    ffmpeg-out-ext "mp4";
    ffmpeg-params-global-y-enable;
    #ffmpeg-params-add extrait-05-06mn;

    #titre2 "metadata";
    #

    ffmpeg-mappage;

    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-exec--exemple-VIDEOTEST_MP4
addLibrairie 'test-ffmpeg-metadata-exec--exemple-VIDEOTEST_MP4';
function      test-ffmpeg-metadata-exec--exemple-VIDEOTEST_MP4(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;
    titre1 "${FUNCNAME[0]}:BEGIN";

    ffmpeg-metadata-init;

    #titre2 "# - in - #";
    ffmpeg-in-pathname    "$VIDEOTEST_PATHNAME";
    ffmpeg-metadata-show  "$VIDEOTEST_PATHNAME";
    ffmpeg-streams-show   "$VIDEOTEST_PATHNAME";

    #titre2 "# - out - #";
    ffmpeg-metadata-del-disable;
    ffmpeg-out-rep "$tmp_rep";    ffmpeg-out-ext "mkv";
    ffmpeg-params-global-y-enable;
    #ffmpeg-params-add extrait-05-06mn;

    #titre2 "metadata";
        ffmpeg-metadata-comment-user-add "Nouveeau commentaire.";
    #

    ffmpeg-mappage;


    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-exec--exemple-VIDEOTEST_MKV
addLibrairie 'test-ffmpeg-metadata-exec--exemple-VIDEOTEST_MKV';
function      test-ffmpeg-metadata-exec--exemple-VIDEOTEST_MKV(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";

    ffmpeg-metadata-init;

    #titre2 "# - in - #";
    ffmpeg-in-pathname    "$VIDEOTEST_MKV_PATHNAME";
    ffmpeg-metadata-show  "$VIDEOTEST_MKV_PATHNAME";
    ffmpeg-streams-show   "$VIDEOTEST_MKV_PATHNAME";

    #titre2 "# - out - #";
    ffmpeg-metadata-del-disable;
    ffmpeg-out-rep "$tmp_rep";    ffmpeg-out-ext "mp4";
    ffmpeg-params-global-y-enable;
    #ffmpeg-params-add extrait-05-06mn;

    #titre2 "metadata";
        ffmpeg-metadata-comment-user-add "Nouveeau commentaire.";
    #

    ffmpeg-mappage;

    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


return 0;
