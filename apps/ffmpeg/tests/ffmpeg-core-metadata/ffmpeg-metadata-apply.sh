echo "${BASH_SOURCE[0]}";

# date de création    : 2024.09.15
# date de modification: 2024.09.23
# clear; gtt.sh --show-libsLevel2 apps/ffmpeg/tests/ffmpeg-core-metadata/


# clear; gtt.sh --libExecAuto apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-apply
addLibrairie 'test-ffmpeg-metadata-apply';
function      test-ffmpeg-metadata-apply(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-metadata.sh";

    local _programme="ffmpeg-metadata-apply";

    libExecAuto "${_programme}--vide";
    libExecAuto "${_programme}---out-pathname";
    libExecAuto "${_programme}--exemple-simple";

    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-apply--vide
addLibrairie 'test-ffmpeg-metadata-apply--vide';
function      test-ffmpeg-metadata-apply--vide(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-metadata.sh";

    local _programme="ffmpeg-metadata-apply";

    titre4 'sans argument':
    ffmpeg-metadata-init;
    eval-echo "$_programme";
    erreur-no-description-echo;
    ffmpeg-metadata-display-vars;

    titre4 'argument vide':
    ffmpeg-metadata-init;
    eval-echo "$_programme ''";
    erreur-no-description-echo;
    ffmpeg-metadata-display-vars;

    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-apply--syntaxe
addLibrairie 'test-ffmpeg-metadata-apply--syntaxe';
function      test-ffmpeg-metadata-apply--syntaxe(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-metadata.sh";


    local _programme="ffmpeg-metadata-apply";

    titre4 'fichier local:';
    ffmpeg-metadata-init;
    eval-echo "$_programme 'nom.ext'";
    erreur-no-description-echo;
    ffmpeg-out-display-vars;
    #ffmpeg-metadata-display-vars;

    titre4 'fichier local:';
    ffmpeg-metadata-init;
    eval-echo "$_programme 'nom'";
    erreur-no-description-echo;
    ffmpeg-out-display-vars;
    #ffmpeg-metadata-display-vars;

    titre4 'fichier pathname:';
    ffmpeg-metadata-init;
    eval-echo "$_programme '$tmp_rep/nom.ext'";
    erreur-no-description-echo;
    ffmpeg-out-display-vars;
    ffmpeg-metadata-display-vars;

    titre4 'fichier pathname:';
    ffmpeg-metadata-init;
    eval-echo "$_programme '$tmp_rep/nom'";
    erreur-no-description-echo;
    ffmpeg-out-display-vars;
    ffmpeg-metadata-display-vars;


    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}



function      test-ffmpeg-metadata-apply--metadata-init(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;


    ffmpeg-metadata-init;

    # -- langue -- #
    ffmpeg-metadata-language 'fra';
    # - Organization Information - #
    ffmpeg-metadata-track-no  10;
    ffmpeg-metadata-track-nb  20;


    # - Titles - #
    ffmpeg-metadata-title    'Titre de la vidéo';

    # - Entities - #

    #ffmpeg-metadata-director            'directeur-1' 'directeur-2';
    #ffmpeg-metadata-director-add        'directeur-add-1'  'directeur-add-2';

    #ffmpeg-metadata-composer            'composer-1';
    #ffmpeg-metadata-composer-add        'composer-2' 'composer-3';
    #ffmpeg-metadata-composer_nationality 'fra';

    #ffmpeg-metadata-conductor           'conductor-1';
    #ffmpeg-metadata-conductor-add       'conductor-2'  'conductor-3';

    ffmpeg-metadata-actor               'actor-1';
    ffmpeg-metadata-actor-add           'actor-2'  'actor-3';

    ffmpeg-metadata-character           'character-1';
    ffmpeg-metadata-character-add       'character-2'  'character-3';

    ffmpeg-metadata-written_by           'written_by-1';
    ffmpeg-metadata-written_by-add      'written_by-2'  'written_by-3';

    ffmpeg-metadata-producer            'producer-1';
    ffmpeg-metadata-producer-add        'producer-2'  'producer-3';

    ffmpeg-metadata-distributed_by      'distributed_by-1';
    ffmpeg-metadata-distributed_by-add  'distributed_by-2' 'distributed_by-3';

    ffmpeg-metadata-publisher           'publisher-1';
    ffmpeg-metadata-publisher-add       'publisher-2' 'publisher-3';

    ffmpeg-metadata-label               'label-1';
    ffmpeg-metadata-label-add           'label-2' 'label-3';

    ffmpeg-metadata-writer              'writer-1';
    ffmpeg-metadata-writer-add          'writer-2' 'writer-2';

    ffmpeg-metadata-artist              'artist-1';
    ffmpeg-metadata-artist-add          'artist-2' 'artist-3';

    ffmpeg-metadata-performer           'performer-1';
    ffmpeg-metadata-performer-add       'performer-2' 'artist-3';


    # Search and Classification - #
    ffmpeg-metadata-genre               'genre-1';
    ffmpeg-metadata-genre-add           'genre-2' 'genre-3';

    ffmpeg-metadata-album               "L'album à Moi";

    ffmpeg-metadata-support             "DVD";
    #ffmpeg-metadata-original_media_type ''
    ffmpeg-metadata-content_type        'Documentary';
    ffmpeg-metadata-subject             "";
    ffmpeg-metadata-keywords            "";
    ffmpeg-metadata-summary             "";
    ffmpeg-metadata-synopsis            "";
    ffmpeg-metadata-initial_key         "";


    # - Temporal Information - #
    ffmpeg-metadata-period              'medieval';
    ffmpeg-metadata-date                2024 '10-30';
    ffmpeg-metadata-date_tagged         '2024-12-01';
    ffmpeg-metadata-date_written        '1806-06-31';

    # - Spatial Information - #
    ffmpeg-metadata-composition_location     'composition_location-1';
    ffmpeg-metadata-composition_location-add 'composition_location-2' 'composition_location-3';

    # - Identifiers - #
    ffmpeg-metadata-ISRC "ISRC";
    ffmpeg-metadata-MCDI "MCDI";
    ffmpeg-metadata-ISBN "ISBN";
    ffmpeg-metadata-BARCODE "BARCODE";
    ffmpeg-metadata-CATALOG_NUMBER "CATALOG_NUMBER";
    ffmpeg-metadata-LABEL_CODE "LABEL_CODE";
    ffmpeg-metadata-LCCN "LCCN";
    ffmpeg-metadata-IMDB "IMDB";
    ffmpeg-metadata-TMDB "TMDB";
    ffmpeg-metadata-TVDB "TVDB";
    ffmpeg-metadata-TVDB2 "TVDB2";

    # - comment/description - #
    ffmpeg-metadata-url      'https://perdu.com';
    ffmpeg-metadata-url-add  'https://lost.fm' 'https://url-3';

    titre3 "Personalisation des pistes";
    ffmpeg-map-dest-select  'v:0' 'bra' "support";  
    ffmpeg-map-dest-select  'a:0' 'chi' 'stereo';         
    ffmpeg-map-dest-select  's:0' 'fra' 'ocean';         
    
    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# Code commun; ne pas appeller directement
function      test-ffmpeg-metadata-apply--exemple-VIDEOTEST(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

   # titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-metadata.sh";

    local _fichier_test_pathname="${1:-""}";
    titre2 "Fichier test: '$_fichier_test_pathname'";
        if [ -z "$_fichier_test_pathname" ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}: Nom de fichier requis!";
            return $E_INODE_NOT_EXIST;
        fi

        if [ ! -f "$_fichier_test_pathname" ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}: '$_fichier_test_pathname' introuvable";
            return $E_INODE_NOT_EXIST;
        fi
    #

    # - Faire une copie nommé ori - #
    #local _fichier_test_ori_rep="${_fichier_test_ori_pathname%%/*}";
        #local _fichier_test_ori_name="${_fichier_test_ori_pathname##*/}";
        #local _fichier_test_ori_ext="${_fichier_test_ori_name%*.}";

        #local _fichier_test_rep="$tmp_rep";
        #local _fichier_test_pathname="$_fichier_test_rep/$_fichier_test_ori_name";
        #titre2 "Fichier de test: '$_fichier_test_pathname'";
        #if [ ! -f "$_fichier_test_pathname" ]; then cp "$_fichier_test_ori_pathname" "$_fichier_test_pathname"; fi
    ffmpeg-streams-show  "$_fichier_test_pathname";
    ffmpeg-metadata-show "$_fichier_test_pathname";

    ################
    # test d'ajout #
    ################
        titre2 "Test d'ajout";
        
        #titre3 "# - Initialisation des metadatas - #";
        ffmpeg-metadata-del-enable;
        ffmpeg-methode-metadata_apply;
        ffmpeg-params-global-y-enable;
        ffmpeg-out-pathname "$_fichier_test_pathname";
        test-ffmpeg-metadata-apply--metadata-init;

        hr;
        eval-echo "ffmpeg-methode-run";
        hr;
        erreur-no-description-echo;

        ffmpeg-metadata-show "$_fichier_test_pathname";

        #return 0;   # NE PAS FAIRE DE TEST D'AJOUT
    #

    ######################################################
    # test d'ajout' de metadats sans effacer les anciens #
    ######################################################
        titre2 "test d'ajout' de metadatas sans effacer les anciens":

        ffmpeg-out-pathname "$_fichier_test_pathname";
        ffmpeg-metadata-del-disable;
        ffmpeg-methode-metadata_apply;
        ffmpeg-metadata-init;           # Supprimer les anciennes valeurs du cache
        #ffmpeg-metadata-display-vars;
        ffmpeg-metadata-date            2024 '10.30'; 

        ffmpeg-metadata-artist   'Toi';

        ffmpeg-metadata-comment-user-add  "Commentaire 2";


        #titre3 "Execution du test";
        hr;
        eval-echo "ffmpeg-methode-run";
        hr;
        erreur-no-description-echo;

        ffmpeg-metadata-show "$_fichier_test_pathname";
    #

    #titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-apply--exemple-VIDEOTEST_ORI
addLibrairie 'test-ffmpeg-metadata-apply--exemple-VIDEOTEST_ORI';
function      test-ffmpeg-metadata-apply--exemple-VIDEOTEST_ORI(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;
    titre1 "${FUNCNAME[0]}:BEGIN";
    test-ffmpeg-metadata-apply--exemple-VIDEOTEST "$VIDEOTEST_ORI_PATHNAME";
    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-apply--exemple-VIDEOTEST_MP4
addLibrairie 'test-ffmpeg-metadata-apply--exemple-VIDEOTEST_MP4';
function      test-ffmpeg-metadata-apply--exemple-VIDEOTEST_MP4(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;
    titre1 "${FUNCNAME[0]}:BEGIN";
    test-ffmpeg-metadata-apply--exemple-VIDEOTEST "$VIDEOTEST_PATHNAME";
    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-apply--exemple-VIDEOTEST_MKV
addLibrairie 'test-ffmpeg-metadata-apply--exemple-VIDEOTEST_MKV';
function      test-ffmpeg-metadata-apply--exemple-VIDEOTEST_MKV(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    test-ffmpeg-metadata-apply--exemple-VIDEOTEST "$VIDEOTEST_MKV_PATHNAME";
    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


return 0;
