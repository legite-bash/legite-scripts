echo "${BASH_SOURCE[0]}";

# date de création    : 2024.09.29
# date de modification: 2024.09.29
# clear; gtt.sh --UUID-libsLevel2 apps/ffmpeg/tests/ffmpeg-core-metadata/


# clear; gtt.sh --libExecAuto apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-UUID
addLibrairie 'test-ffmpeg-metadata-UUID';
function      test-ffmpeg-metadata-UUID(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-metadata.sh";

    local _programme="ffmpeg-metadata-UUID";

    libExecAuto "${_programme}--vide";
    libExecAuto "${_programme}---out-pathname";
    libExecAuto "${_programme}--exemple-simple";

    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}




# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-UUID-read--vide
addLibrairie 'test-ffmpeg-metadata-UUID-read--vide';
function      test-ffmpeg-metadata-UUID-read--vide(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-metadata.sh";

    local _programme="ffmpeg-metadata-UUID-read";

    titre4 'sans argument':
    ffmpeg-metadata-init;
    eval-echo "$_programme";
    erreur-no-description-echo;
    display-vars 'ffmpeg_metadata_UUID' "$ffmpeg_metadata_UUID";

    titre4 'argument vide':
    ffmpeg-metadata-init;
    eval-echo "$_programme ''";
    erreur-no-description-echo;
    display-vars 'ffmpeg_metadata_UUID' "$ffmpeg_metadata_UUID";

    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-UUID-read-VIDEOTEST
addLibrairie 'test-ffmpeg-metadata-UUID-read-VIDEOTEST';
function      test-ffmpeg-metadata-UUID-read-VIDEOTEST(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-metadata.sh";

    local _programme="ffmpeg-metadata-UUID-read";

    titre4 '':


    ffmpeg-metadata-init;
    eval-echo "$_programme '$VIDEOTEST_PATHNAME' ";
    erreur-no-description-echo;
    display-vars 'ffmpeg_metadata_UUID' "$ffmpeg_metadata_UUID";



    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-UUID-read-VIDEOTEST_ORI
addLibrairie 'test-ffmpeg-metadata-UUID-read-VIDEOTEST_ORI';
function      test-ffmpeg-metadata-UUID-read-VIDEOTEST_ORI(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-metadata.sh";

    local _programme="ffmpeg-metadata-UUID-read";

    titre4 '':
    ffmpeg-metadata-init;
    eval-echo "$_programme '$VIDEOTEST_ORI_PATHNAME' ";
    erreur-no-description-echo;
    display-vars 'ffmpeg_metadata_UUID' "$ffmpeg_metadata_UUID";


    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-metadata/ test-ffmpeg-metadata-UUID-insert-VIDEOTEST_ORI
addLibrairie 'test-ffmpeg-metadata-UUID-insert-VIDEOTEST_ORI';
function      test-ffmpeg-metadata-UUID-insert-VIDEOTEST_ORI(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-metadata.sh";

    local _programme="ffmpeg-metadata-UUID-insert";

    titre4 '':
    ffmpeg-metadata-init;
    eval-echo "$_programme '$VIDEOTEST_ORI_PATHNAME'";
    erreur-no-description-echo;
    display-vars 'ffmpeg_metadata_UUID' "$ffmpeg_metadata_UUID";


    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

return 0;
