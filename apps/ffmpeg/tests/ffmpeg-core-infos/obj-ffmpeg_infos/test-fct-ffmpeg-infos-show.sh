echo "${BASH_SOURCE[0]}";

# gtt.sh --show-libsLevel2 apps/ffmpeg/tests/test-obj-fct-ffmpeg-infos.sh   # lib
# date de création    : 2024.04.23
# date de modification: 2024.08.03


# clear; gtt.sh -d --libExecAuto apps/ffmpeg/tests/obj-ffmpeg_infos/test-fct-ffmpeg-infos-show 'test-fct-ffmpeg-infos-show'; # - src=pathname
addLibrairie 'test-fct-ffmpeg-infos-show' "Affiche (uniquement) les streams d'un fichier";
function      test-fct-ffmpeg-infos-show(){

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-ffmpeg-infos.sh";

    titre3 "Sans parametre";
    eval-echo "ffmpeg-infos-init";        # Obligatoire
    eval-echo "ffmpeg-infos-show";
    erreur-no-description-echo $?;

    titre3 "Parametre vide";
    eval-echo "ffmpeg-infos-init";        # Obligatoire
    eval-echo "ffmpeg-infos-show ''";
    erreur-no-description-echo $?;

    titre3 "1 fichier";
    eval-echo "ffmpeg-infos-init";        # Obligatoire
    eval-echo "ffmpeg-infos-show '$VIDEOTEST_PATHNAME'";
    erreur-no-description-echo $?;
    # echo 'Rendu attendu:';
    # echo "charge_teaser06wide-720p.mp4        |   mov   |   5274373|00:00:17.09|   h264   | 1280x720|24   | aac |1.777|";
    




    titre1 "${FUNCNAME[0]}:END";

    return 0;
}

#libExecAuto test-fct-ffmpeg-infos-show;
return 0;

