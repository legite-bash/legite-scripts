echo "${BASH_SOURCE[0]}";

# date de création    : 2024.10.01
# date de modification: 2024.10.01
# clear; gtt.sh --show-libsLevel2 apps/ffmpeg/tests/ffmpeg-core-cover/


# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-cover/ test-ffmpeg-cover-display-vars
addLibrairie 'test-ffmpeg-cover-display-vars';
function      test-ffmpeg-cover-display-vars(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-cover.sh";

    ffmpeg-cover-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


function      test-ffmpeg-cover-cover-init(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    ffmpeg-cover-init;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

function      test-ffmpeg-cover-cover-init(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    ffmpeg-cover-init;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-cover/ test-ffmpeg-cover--MKV_to_MKV-add
addLibrairie 'test-ffmpeg-cover--MKV_to_MKV-add'
function     test-ffmpeg-cover--MKV_to_MKV-add(){
    ffmpeg-in-pathname    "$VIDEOTEST_MKV_PATHNAME";
    ffmpeg-out-rep "$tmp_rep";    ffmpeg-out-ext "mkv";
    ffmpeg-params-global-y-enable;            ffmpeg-metadata-del-enable;
    ffmpeg-cover-init;

    ffmpeg-params-preset-tune-crf-clear;
    ffmpeg-map-add '0:v:0' 'v:0' "eng" "3D";        ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
    ffmpeg-map-add '0:a:0' 'a:0' "eng" 'Stéréo';    ffmpeg-map-add-codec "copy -disposition:${ffmpeg_map_dest} default";
    ffmpeg-cover-urls 'front' 'https://www.blender.org/wp-content/uploads/2015/03/blender_logo_socket-1-1280x391.png';
    ffmpeg-methode-exec;ffmpeg-methode-run;
}
