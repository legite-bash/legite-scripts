echo "${BASH_SOURCE[0]}";

# date de création    : 2024.10.02
# date de modification: 2024.10.02
# clear; gtt.sh --show-libsLevel2 apps/ffmpeg/tests/ffmpeg-core-cover/


# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-cover/ ffmpeg-cover-integrate-in-pathname
addLibrairie 'test-ffmpeg-cover-integrate-in-pathname';
function      test-ffmpeg-cover-integrate-in-pathname(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-cover.sh";

    ffmpeg-cover-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-cover/ ffmpeg-cover-integrate-in-pathname--vide
function      ffmpeg-cover-integrate-in-pathname--vide(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-cover.sh";

    local _programme="ffmpeg-cover-integrate-in-pathname";

    titre4 'sans argument':
    ffmpeg-metadata-init;
    eval-echo "$_programme";
    erreur-no-description-echo;
    display-vars 'ffmpeg_metadata_UUID' "$ffmpeg_metadata_UUID";

    titre4 'argument vide':
    ffmpeg-metadata-init;
    eval-echo "$_programme ''";
    erreur-no-description-echo;
    display-vars 'ffmpeg_metadata_UUID' "$ffmpeg_metadata_UUID";

    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}



# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-cover/ test-ffmpeg-cover-integrate-in-pathname
addLibrairie 'test-ffmpeg-cover-integrate-in-pathname';
function      test-ffmpeg-cover-integrate-in-pathname(){

    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";

    local _videotest_pathname="$VIDEOTEST_MP4_PATHNAME";
    #local _videotest_pathname="$VIDEOTEST_MKV_PATHNAME";
    #titre2 "# - in - #";
    ffmpeg-in-pathname    "$_videotest_pathname";
    display-vars          'ffmpeg_in_pathname' "$ffmpeg_in_pathname";
    ffmpeg-metadata-show  "$_videotest_pathname";
    ffmpeg-streams-show   "$_videotest_pathname";

    #titre2 "# - out - #";
    ffmpeg-out-rep "$tmp_rep";    ffmpeg-out-ext "mkv";
    ffmpeg-params-global-y-enable;
    ffmpeg-metadata-del-disable;

    titre2 "Initilisation";
    eval-echo "ffmpeg-cover-init";

    titre2 "Selection";
    #ffmpeg-cover-emplacement;
    ffmpeg-cover-pathnames 'Cover (front)' "$VIDEOTEST_PNG";
    ffmpeg-cover-pathnames 'Cover (back)'  "$VIDEOTEST_PNG";
    eval-echo "ffmpeg-cover-display-vars";


# code fonctionnel:
#ffmpeg  -hide_banner -y
#-i "/media/mediatheques/CHARGE-Blender_Open_Movie.mp4" 
#-i "/media/mediatheques/CHARGE-Blender_Open_Movie.png"
#-map 0:v:0 -codec:v:0 copy
#-map 1:v:0 -codec:v:1 copy  -metadata:s:v:1 title="covert (front)" -disposition:v:1 attached_pic
#"/dev/shm/home/datas/www/bash/gtt/apps/ffmpeg/tests/ffmpeg-core-cover/CHARGE-Blender_Open_Movie.mp4"


    ffmpeg-cover-integrate-in-pathname;

    #ffmpeg-mappage;

    #titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

