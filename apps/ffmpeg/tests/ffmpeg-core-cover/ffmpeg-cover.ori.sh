echo "${BASH_SOURCE[0]}";

# date de création    : 2024.10.01
# date de modification: 2024.10.01
# clear; gtt.sh --show-libsLevel2 apps/ffmpeg/tests/ffmpeg-core-cover/


# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-cover/ test-ffmpeg-cover-display-vars
addLibrairie 'test-ffmpeg-cover-display-vars';
function      test-ffmpeg-cover-display-vars(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-cover.sh";

    ffmpeg-cover-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


function      test-ffmpeg-cover--cover-init(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    ffmpeg-cover-init;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-cover/ test-ffmpeg-cover--exemple-VIDEOTEST_MP4
addLibrairie 'test-ffmpeg-cover--exemple-VIDEOTEST_MP4';
function      test-ffmpeg-cover--exemple-VIDEOTEST_MP4(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";

    #titre2 "# - in - #";
    ffmpeg-in-pathname    "$VIDEOTEST_PATHNAME";
    #ffmpeg-metadata-show     "$VIDEOTEST_PATHNAME";
    #ffmpeg-streams-show   "$VIDEOTEST_PATHNAME";

    #titre2 "# - out - #";
    ffmpeg-out-rep "$tmp_rep";    ffmpeg-out-ext "mkv";
    ffmpeg-params-global-y-enable;
    ffmpeg-metadata-del-disable;
    #ffmpeg-params-add extrait-05-06mn;

    titre2 "Initilisation";
    eval-echo "ffmpeg-cover-init";

    titre2 "Selection";
    eval-echo "ffmpeg-cover-type";
    eval-echo "ffmpeg-cover-level";

    eval-echo "ffmpeg-cover-display-vars";

    #




    #ffmpeg-mappage;

    #titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

