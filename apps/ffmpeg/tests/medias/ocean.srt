1
00:00:05.000 --> 00:00:15.000
L'océan, vaste et mystérieux, couvre 71% de la surface de la Terre.

2
00:00:20.000 --> 00:00:30.000
Sous ses vagues se cachent des écosystèmes foisonnants de vie, des plus petits planctons aux plus grandes baleines.

3
00:00:35.000 --> 00:00:45.000
L'humanité n'a exploré qu'une fraction de ces profondeurs, laissant une grande partie de l'océan encore inconnue.

4
00:00:50.000 --> 00:01:00.000
Les courants océaniques régulent le climat et influencent les conditions météorologiques du globe entier.

5
00:01:05.000 --> 00:01:15.000
Des récifs coralliens aux tranchées abyssales, chaque environnement est unique et plein de surprises.

6
00:01:20.000 --> 00:01:30.000
Cependant, l'océan est menacé par la pollution, la surpêche et le changement climatique.

7
00:01:35.000 --> 00:01:45.000
C'est à nous de protéger ces eaux et de veiller à ce qu'elles restent vibrantes pour les générations futures.

8
00:01:50.000 --> 00:02:00.000
Alors que nous regardons l'horizon, l'océan continue d'inspirer émerveillement et respect.

9
00:02:05.000 --> 00:02:15.000
Plongeons plus profondément dans ses mystères et assurons sa protection pour l'avenir.

10
00:02:20.000 --> 00:02:30.000
L'océan joue également un rôle crucial en absorbant le dioxyde de carbone, aidant ainsi à réguler la température de la planète.

11
00:02:35.000 --> 00:02:45.000
Les récifs coralliens, souvent qualifiés de "forêts tropicales de la mer", abritent un quart de toutes les espèces marines.

12
00:02:50.000 --> 00:03:00.000
Dans les profondeurs de l'océan, des sources volcaniques libèrent des minéraux qui soutiennent des écosystèmes uniques.

13
00:03:05.000 --> 00:03:15.000
Les océans sont essentiels à la survie humaine, fournissant nourriture, emplois et loisirs à des milliards de personnes.

14
00:03:20.000 --> 00:03:30.000
Cependant, les déchets plastiques sont l'une des menaces les plus urgentes, avec des millions de tonnes entrant dans l'océan chaque année.

15
00:03:35.000 --> 00:03:45.000
Des initiatives mondiales visent à protéger l'océan, mais il faut davantage d'actions aux niveaux local et individuel.

16
00:03:50.000 --> 00:04:00.000
En tant que gardiens de la planète, nous devons agir pour réduire notre impact sur ces eaux et garantir un avenir sain pour la vie marine.

17
00:04:05.000 --> 00:04:15.000
En protégeant l'océan, nous protégeons notre climat, notre biodiversité, et finalement, nous-mêmes.

18
00:04:55.000 --> 00:05:00.000
Fin.
