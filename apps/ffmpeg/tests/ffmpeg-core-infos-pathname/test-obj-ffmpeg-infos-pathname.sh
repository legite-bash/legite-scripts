echo "${BASH_SOURCE[0]}";

# gtt.sh --show-libsLevel2 apps/ffmpeg/tests/test-ffmpeg.sh lib
# date de création    : 2024.04.23
# date de modification: 2024.09.04


# clear; gtt.sh -d --libExecAuto ffmpeg/tests/ffmpeg-core-infos-pathname/ test-obj-ffmpeg-infos-pathname
addLibrairie 'test-obj-ffmpeg-infos-pathname';
function      test-obj-ffmpeg-infos-pathname(){
    
    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code:apps/ffmpeg/ffmpeg-core-ffmpeg-infos.sh";

    titre2   "initialisation:  ffmpeg-infos-pathname # Sans parametre";
    eval-echo "ffmpeg-infos-pathname";
    erreur-no-description-echo $?;
    ffmpeg-infos-display-vars;


    #titre2   "ffmpeg-infos-pathname 'VIDEOTEST_REP'; # Avec un repertoire";
    #eval-echo "ffmpeg_in_init";
    #eval-echo "ffmpeg-infos-pathname \"$VIDEOTEST_REP\"";
    #erreur-no-description-echo $?;
    #ffmpeg-infos-display-vars;

    #titre2   "ffmpeg-infos-pathname '/dev/null/fichier_inexistant.ext'; # Avec un fichier inexistant";
    #eval-echo "ffmpeg_in_init";
    #eval-echo "ffmpeg-infos-pathname \"/dev/null/fichier_inexistant.ext\"";
    #erreur-no-description-echo $?;
    #ffmpeg-infos-display-vars;


    titre2   "ffmpeg-infos-pathname 'VIDEOTEST_PATHNAME'; # Avec un pathname de fichier";
    eval-echo "ffmpeg_in_init";
    eval-echo "ffmpeg-infos-pathname \"$VIDEOTEST_PATHNAME\"";
    erreur-no-description-echo $?;
    ffmpeg-infos-display-vars;


    titre1 "${FUNCNAME[0]}:END";
    return 0;
}


return 0;

