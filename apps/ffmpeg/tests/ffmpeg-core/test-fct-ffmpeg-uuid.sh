echo "${BASH_SOURCE[0]}";


# date de création    : 2024.04.23
# date de modification: 2024.08.03
# clear; gtt.sh --show-libsLevel2 apps/ffmpeg/tests/ffmpeg-core/

# clear; gtt.sh -d --libExecAuto apps/ffmpeg/tests/ffmpeg-core/ test-ffmpeg-fct-ffmpeg-uuid
addLibrairie 'test-ffmpeg-fct-ffmpeg-uuid';
function      test-ffmpeg-fct-ffmpeg-uuid(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";
    titreInfo "";

    local _programme="ffmpeg-uuid";


    titre4 'sans argument':
    eval-echo "$_programme";
    erreur-no-description-echo;
    display-vars 'ffmpeg_uuid' "$ffmpeg_uuid";

    titre4 'argument vide':
    eval-echo "$_programme ''";
    erreur-no-description-echo;
    display-vars 'ffmpeg_uuid' "$ffmpeg_uuid";
    
    titre4 'valeur: 0':
    eval-echo "$_programme 0";
    erreur-no-description-echo;
    display-vars 'ffmpeg_uuid' "$ffmpeg_uuid";

    titre4 'valeur: -1':
    eval-echo "$_programme -1";
    erreur-no-description-echo;
    display-vars 'ffmpeg_uuid' "$ffmpeg_uuid";

    titre4 'valeur: --1':
    eval-echo "$_programme --1";
    erreur-no-description-echo;
    display-vars 'ffmpeg_uuid' "$ffmpeg_uuid";

    titre4 'valeur: $E_FALSE':
    eval-echo "$_programme $E_FALSE";
    erreur-no-description-echo;
    display-vars 'ffmpeg_uuid' "$ffmpeg_uuid";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

#libExecAuto test-fct-ffmpeg-streams-show;

return 0;

