echo "${BASH_SOURCE[0]}";

# date de création    : 2024.04.23
# date de modification: 2024.08.29
# gtt.sh --show-libsLevel2 apps/ffmpeg/tests/ffmpeg-core/


# clear; gtt.sh -d --libExecAuto   apps/ffmpeg/tests/ffmpeg-core/ test-ffmpeg-fct-ffmpeg-container-is_valide
addLibrairie 'test-ffmpeg-fct-ffmpeg-container-is_valide';
function      test-ffmpeg-fct-ffmpeg-container-is_valide(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";
    titreInfo "";

    local _programme="ffmpeg-container-is_valide";

    titre4 'sans argument':
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide':
    eval-echo "$_programme ''";
    erreur-no-description-echo;
    
    titre4 'valeur: 0':
    eval-echo "$_programme 0";
    erreur-no-description-echo;

    titre4 'valeur: -1':
    eval-echo "$_programme -1";
    erreur-no-description-echo;

    titre4 'valeur: --1':
    eval-echo "$_programme --1";
    erreur-no-description-echo;

    titre4 'valeur: $E_FALSE':
    eval-echo "$_programme $E_FALSE";
    erreur-no-description-echo;


    # - Container valide - #
    titre4 'valeur: mkv':
    eval-echo "$_programme 'mkv'";
    erreur-no-description-echo;

    titre4 'valeur: avi':
    eval-echo "$_programme 'avi'";
    erreur-no-description-echo;

    # - Container invalide - #
    titre4 'valeur: txt':
    eval-echo "$_programme 'txt'";
    erreur-no-description-echo;

    # - pathname - #
    titre4 'valeur: pathname':
    eval-echo "$_programme '$VIDEOTEST_PATHNAME'";
    erreur-no-description-echo;

    # - pathname - #
    titre4 'valeur: rep':
    eval-echo "$_programme '$VIDEOTEST_REP'";
    erreur-no-description-echo;

    # - pathname - #
    titre4 'valeur: nom':
    eval-echo "$_programme '$VIDEOTEST_NOM'";
    erreur-no-description-echo;




    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

#libExecAuto test-fct-ffmpeg-isValideContainer;

return 0;

