echo "${BASH_SOURCE[0]}";

# clear; gtt.sh --show-libsLevel2 apps/ffmpeg/tests/ffmpeg-core/.sh
# date de création    : 2024.04.23
# date de modification: 2024.08.29


addLibrairie 'fct-ffmpeg';
function fct-ffmpeg(){
    #ffmpeg_help;
    #fct-ffmpeg-setUuid;
    #fct-ffmpeg-isValideContainer;
    #fct-ffmpeg-profil_addParams;
    return 0;
}

# clear; gtt.sh -d --libExecAuto apps/ffmpeg/tests/ffmpeg-core/ fct-ffmpeg-help
addLibrairie 'fct-ffmpeg-help';
function      fct-ffmpeg-help(){
    fctIn "$*";

    eval-echo 'ffmpeg --help'
    eval-echo 'ffmpeg -h long'
    eval-echo 'ffmpeg -h full'

    topic='';
    eval-echo "ffmpeg -h $topic"

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

return 0;

