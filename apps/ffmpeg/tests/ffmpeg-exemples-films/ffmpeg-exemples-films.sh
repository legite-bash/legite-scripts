echo-d "${BASH_SOURCE[0]}" ;

# date de création    : 2024.09.17
# date de modification: 2024.09.18
# clear; gtt.sh --show-libsLevel2 apps/ffmpeg/tests/ffmpeg-exemples-films/

# - Téléchargement du fichier VIDEOTEST - #
#echo $LINENO
require-once "$APPS_REP/ffmpeg/tests/VIDEOTEST.sh";
#echo $LINENO
VIDEOTEST-dw;
#echo $LINENO

#
    #Le format HDMV PGS (Presentation Graphic Stream) subtitle est un format de sous-titres utilisé principalement dans les disques Blu-ray. Voici quelques détails sur ce format :
    #    HDMV signifie High Definition Movie Video, qui est le standard utilisé pour les menus et les sous-titres dans les Blu-ray.
    #    PGS désigne un format de sous-titres bitmap, ce qui signifie que les sous-titres sont stockés sous forme d'images, et non de texte. Cela permet des sous-titres graphiques plus riches, avec des couleurs et des effets, mais limite leur éditabilité.
    #    Il est couramment utilisé pour les sous-titres préintégrés dans les films Blu-ray.

    #    SRT (SubRip Subtitle)
    #        Support : Fichiers vidéo courants (MP4, MKV, AVI, etc.), plateformes de streaming, lecteurs multimédias
    #        Description : Format de texte simple, facilement modifiable.

    #    ASS/SSA (Advanced SubStation Alpha/SubStation Alpha)
    #        Support : Fichiers vidéo courants, plateformes de streaming, lecteurs multimédias
    #        Description : Format de texte avec des options de formatage avancées, y compris les styles et les effets.

    #    SUB (MicroDVD)
    #        Support : Fichiers vidéo courants, lecteurs multimédias
    #        Description : Format de sous-titres textuels avec un synchronisation temporelle.

    #    IDX/SUB (VobSub)
    #        Support : DVD, fichiers vidéo courants, lecteurs multimédias
    #        Description : Format de sous-titres bitmap utilisé sur les DVDs.

    #    PGS (Presentation Graphic Stream)
    #        Support : Blu-ray Discs
    #        Description : Format de sous-titres bitmap pour les Blu-ray, permettant des sous-titres graphiques.

    #    DVD (Subpicture)
    #        Support : DVDs
    #        Description : Format de sous-titres bitmap intégré aux disques DVD.

    #    XSUB (Subtitles for DivX)
    #        Support : DivX fichiers vidéo
    #        Description : Format de sous-titres bitmap utilisé avec les fichiers DivX.

    #    SCC (Scenarist Closed Caption)
    #        Support : DVD, Blu-ray
    #        Description : Format de sous-titres pour le sous-titrage en anglais, souvent utilisé pour les DVD et Blu-ray.

    #    SMI (SAMI - Synchronized Accessible Media Interchange)
    #        Support : Fichiers vidéo courants, lecteurs multimédias
    #        Description : Format de sous-titres textuels avec options de synchronisation.

    #    TTML (Timed Text Markup Language)
    #        Support : IPTV, services de streaming, fichiers vidéo
    #        Description : Format de sous-titres XML utilisé pour la télévision numérique et le streaming.

    #    DVB Subtitle
    #        Support : Diffusion numérique, télévision par satellite/câble
    #        Description : Format de sous-titres utilisé pour la diffusion numérique terrestre.

    #    WebVTT (Web Video Text Tracks)
    #        Support : HTML5 vidéo, plateformes de streaming
    #        Description : Format de sous-titres textuels utilisé principalement pour les vidéos en ligne.
#

function ffmpeg-exemples-films-init(){

    # collections/_ini.cfg.sh
    FILMS_REP="$VIDEOTEST_REP";
    
    if ! cd "$FILMS_REP"
    then
        erreurs-pile-add-echo "Impossible d'entrer dans le repertoire '$FILMS_REP'";
        return $E_INODE_NOT_EXIST;
    fi
    titreInfo "PWD: $PWD";

    ffmpeg-params-crf     18;
    #ffmpeg-params-preset 'veryslow';
    ffmpeg-params-tune   'film';

    ffmpeg-metadata-init;

    ffmpeg-metadata-date             '';
    ffmpeg-metadata-artist           '';
    ffmpeg-metadata-performer        '';
    ffmpeg-metadata-genre            '';
    ffmpeg-metadata-album            '';
    ffmpeg-metadata-track-no          0;
    ffmpeg-metadata-track-nb          0;
    ffmpeg-metadata-title            '';
    ffmpeg-metadata-url              '';
    ffmpeg-metadata-comment-add      '';
    ffmpeg-metadata-comment-support  '';
    ffmpeg-metadata-comment-language '';
    return 0;
}



# clear; gtt.sh  apps/ffmpeg/tests/ffmpeg-exemples-films/ffmpeg-exemples-films.sh ffmpeg-exemples-films-VIDEOTEST
addLibrairie 'ffmpeg-exemples-films-VIDEOTEST';
function      ffmpeg-exemples-films-VIDEOTEST(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";


    #titre2 "# - Initialisation du contexte - #";
    ffmpeg-exemples-films-init;

    titre2 "# - in - #";
    ffmpeg-in-pathname    "$VIDEOTEST_PATHNAME";
    ffmpeg-metadata-show  "$ffmpeg_in_pathname";
    #ffmpeg-streams-show   "$ffmpeg_in_pathname"; return 0;


    #titre2 "# - out - #";
    ffmpeg-out-rep "$tmp_rep";    ffmpeg-out-ext "mp4";

    #titre2 "metadata";
    ffmpeg-metadata-init;

    ffmpeg-metadata-date             1625;
    ffmpeg-metadata-artist           'Blender';
    #ffmpeg-metadata-performer        '';
    #ffmpeg-metadata-performer-add    'toi | lui';
    ffmpeg-metadata-genre            'Science-fiction';
    ffmpeg-metadata-album            'Animation';
    ffmpeg-metadata-track-no          1;
    ffmpeg-metadata-track-nb          1;
    ffmpeg-metadata-title            'Truc man';
    ffmpeg-metadata-url              'http://perdu.com';
    ffmpeg-metadata-comment-support  '3D';
    ffmpeg-metadata-comment-language 'TrueFrench';


    #titre2 "params-global";
    #ffmpeg-params-global-add '-analyzeduration 100M';
    ffmpeg-params-global-n-enable;
    ffmpeg-params-add extrait-00-10mn;
    ffmpeg-params-add metadatas-del;

    ffmpeg-params-preset-tune-crf-clear;
    ffmpeg-map-add '0:v:0' 'v:0' 'eng' "$ffmpeg_metadata_comment_support";  ffmpeg-map-add-codec 'copy -fflags +genpts -max_muxing_queue_size 1024'; # ffmpeg-params-add "";"
    ffmpeg-map-add '0:a:0' 'a:0' 'fra' 'stereo';                            ffmpeg-map-add-codec 'aac -ac 2';   ffmpeg-map-add-ar '441400'
    ffmpeg-map-add '0:a:0' 'a:1' 'fra' '5.1';                               ffmpeg-map-add-codec 'copy';
    ffmpeg-map-add '0:s:0' 's:0' 'fra' 'dvd_subtitle';                      ffmpeg-map-add-codec 'mov_text';


    ffmpeg-params-add '-vf "setsar=1"';
    ffmpeg-params-add '-vf "setsar=1"';
    ffmpeg-params-add '-aspect 20:11';
    ffmpeg-params-add '-vf "scale=720x396:force_original_aspect_ratio=decrease"';


    # - map 0:0:video:mp4 - #
    #ffmpeg-metadata-comment-convert-video   # '0:0' 'h264';
    #ffmpeg-params-add  '-map 0:v:0';
    #ffmpeg-params-add  '-metadata:s:v:0 language=deu';
    #ffmpeg-params-add  r25;
    #ffmpeg-params-crf  '50';

    # - audios - #
    #ffmpeg-params-add  '-map 0:a:0';
    #ffmpeg-params-add  '-metadata:s:a:0 language=fra';
    #ffmpeg-params-add  'acopy';


    # - map a:0:audio - #
    #ffmpeg-metadata-comment-convert-audio '0:a:0' 'mp3';
    #ffmpeg-params-add  '-map 0:a:0';
    #ffmpeg-params-add  '-metadata:s:a:1 language=spa';
    #ffmpeg-params-add  '-codec:a:1 libmp3lame -b:a:1 32k';

    # - map a:0:audio - #
    #ffmpeg-metadata-comment-convert-audio '0:a:0' 'aac';
    #ffmpeg-params-add  '-map 0:a:0';
    #ffmpeg-params-add  '-metadata:s:a:2 language=fra';
    #ffmpeg-params-add  '-codec:a:2 libmp3lame -b:a:2 64k';

    # - map a:0:audio - #
    #ffmpeg-metadata-comment-convert-audio '0:a:0' 'aac';
    #ffmpeg-params-add  '-map 0:a:0';
    #ffmpeg-params-add  '-metadata:s:a:3 language=fra';
    #ffmpeg-params-add  '-codec:a:3 aac';

    #ffmpeg-params-add   ar44100;


    #titre2 "# - Execution de ffmpeg - #";
    ffmpeg-exec;

    ffmpeg-images-compare "$ffmpeg_in_pathname" "$ffmpeg_out_pathname" "00:05:30";

    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


return 0;
