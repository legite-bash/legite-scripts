echo "${BASH_SOURCE[0]}";

# date de création    : 2024.04.23
# date de modification: 2024.08.29
# clear; gtt.sh --show-libsLevel2 apps/ffmpeg/tests/ffmpeg-core-outils/


# clear; gtt.sh -d --libExecAuto   apps/ffmpeg/tests/ffmpeg-core-outils/ test-ffmpeg-fct-ffmpeg-scale-show
addLibrairie 'test-ffmpeg-fct-ffmpeg-scale-show';
function      test-ffmpeg-fct-ffmpeg-scale-show(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-outils.sh";

    local _programme="ffmpeg-scale-show";

    titre4 'sans argument':
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide':
    eval-echo "$_programme ''";
    erreur-no-description-echo;


    titre4 "valeur: 1000;";
    eval-echo "${_programme} '1000'";
    erreur-no-description-echo;

    #                            X     Y
    local _X=1000;
    local _Y=800
    titre4 "valeur: $_X $_Y;";
    eval-echo "${_programme} $_X $_Y";
    erreur-no-description-echo;

    titre1 "${FUNCNAME[0]}:END";
    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


#libExecAuto test-fct-ffmpeg-scale-show;

return 0;

