echo "${BASH_SOURCE[0]}";

# date de création    : 2024.04.23
# date de modification: 2024.08.29
# clear; gtt.sh --show-libsLevel2 apps/ffmpeg/tests/ffmpeg-core-outils/


# clear; gtt.sh --libExecAuto apps/ffmpeg/tests/ffmpeg-core-outils/ test-ffmpeg-fct-ffmpeg-crop-add
addLibrairie 'test-ffmpeg-fct-ffmpeg-crop-add';
function      test-ffmpeg-fct-ffmpeg-crop-add(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-outils.sh";

    local _programme="ffmpeg-crop-add";

    libExecAuto "${_programme}--vide";
    libExecAuto "${_programme}--exemple-simple";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-outils/ test-ffmpeg-fct-ffmpeg-crop-add--vide
addLibrairie 'test-ffmpeg-fct-ffmpeg-crop-add--vide';
function      test-ffmpeg-fct-ffmpeg-crop-add--vide(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-outils.sh";

    local _programme="ffmpeg-crop-add";

    titre4 'sans argument':
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide':
    eval-echo "$_programme ''";
    erreur-no-description-echo;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-outils/ test-ffmpeg-fct-ffmpeg-crop-add--detect
addLibrairie 'test-ffmpeg-fct-ffmpeg-crop-add--detect';
function      test-ffmpeg-fct-ffmpeg-crop-add--detect(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-outils.sh";

    local _programme="ffmpeg-crop-add";

    titre4 "ffmpeg-crop-add - src=\"$VIDEOTEST_PATHNAME\" time=\"HH:MM:SS.sss\" [crop=\"W:H:X:Y\"]";
    ffmpeg-profil-init;
    ffmpeg-profil-display-vars;
    eval-echo "ffmpeg-crop-add  \"$VIDEOTEST_PATHNAME\" \"00:00:04.050\" ";
    erreur-no-description-echo;
    ffmpeg-profil-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

# clear; gtt.sh apps/ffmpeg/tests/ffmpeg-core-outils/ test-ffmpeg-fct-ffmpeg-crop-add--exemple-simple
addLibrairie 'test-ffmpeg-fct-ffmpeg-crop-add--exemple-simple';
function      test-ffmpeg-fct-ffmpeg-crop-add--exemple-simple(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-outils.sh";

    local _programme="ffmpeg-crop-add";

    titre4 "ffmpeg-crop-add - src=\"$VIDEOTEST_PATHNAME\" time=\"HH:MM:SS.sss\" [crop=\"W:H:X:Y\"]";
    ffmpeg-profil-init;
    ffmpeg-profil-display-vars;
    eval-echo "ffmpeg-crop-add  \"$VIDEOTEST_PATHNAME\" \"00:00:04.050\" \"300:200:400:200\"";
    erreur-no-description-echo;
    ffmpeg-profil-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


return 0;
