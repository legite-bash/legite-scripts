echo "${BASH_SOURCE[0]}";

# date de création    : 2024.04.01
# date de modification: 2024.08.29
# clear; gtt.sh --show-libsLevel2 apps/ffmpeg/tests/ffmpeg-core-outils/

# clear; gtt.sh -d --libExecAuto   apps/ffmpeg/tests/ffmpeg-core-outils/ test-ffmpeg-fct-ffmpeg-streams-show
addLibrairie 'test-ffmpeg-fct-ffmpeg-streams-show' "Affiche (uniquement) les streams d'un fichier";
function      test-ffmpeg-fct-ffmpeg-streams-show(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg-core-outils.sh";

    local _programme="ffmpeg-streams-show";

    titre4 'sans argument':
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide':
    eval-echo "$_programme ''";
    erreur-no-description-echo;

    titre4    "$_programme \"$VIDEOTEST_PATHNAME\"";
    eval-echo "$_programme \"$VIDEOTEST_PATHNAME\"";
    erreur-no-description-echo ;

    titre4    "$_programme \"fichier inexistant\"";
    eval-echo "$_programme \"fichier inexistant\"";
    erreur-no-description-echo ;

    titre4    "$_programme \"repertoire\"";
    eval-echo "$_programme \"$VIDEOTEST_PATHNAME\"";
    erreur-no-description-echo ;


    titre1 "${FUNCNAME[0]}:END";
    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


return 0;

