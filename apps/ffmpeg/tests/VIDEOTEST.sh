echo "${BASH_SOURCE[0]}";

# gtt.sh 
# date de création    : 2024.09.18
# date de modification: 2024.10.04
# Description: 
# clear;gtt.sh --show-libsLevel2 apps/ffmpeg/tests/VIDEOTEST.sh
#~Utilisé par :
# /www/bash/gtt/apps/ffmpeg/tests/ffmpeg-core-metadata/ffmpeg-metadata-apply.sh

titreInfo "tail -f \"$ffmpeg_log_pathname\"";

# Liste de video tests possibles:
# UXqq0ZvbOnk "CHARGE - Blender Open Movie";
# jTVG5kdHRQQ "Blender-4.2LTS-Showcase_Reel";
# hzqD4xcbEuE "Blender Grease Pencil Reel";



# Télécharger le fichier VIDEOTEST
function VIDEOTEST_ORI-dw(){
    local -i _fct_pile_app_level=5;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    if [ ! -f "$VIDEOTEST_ORI_PATHNAME" ]
    then
        titre4 "# - VIDEOTEST_ORI: téléchargement - #";
        eval-echo "yt-dlp -o "$VIDEOTEST_ORI_PATHNAME" --format 18 $VIDEOTEST_MP4_URL";
        eval-echo "chmod 444 \"$VIDEOTEST_ORI_PATHNAME\"";   # Lecture seul: Ne dois jamais être modifié.
    fi

    if [ ! -f "$VIDEOTEST_ORI_PATHNAME" ]
    then
        erreurs-pile-add-echo "VIDEOTEST_ORI_PATHNAME: '$VIDEOTEST_ORI_PATHNAME' Non créé!"
    fi


    fct-pile-app-out 0 $_fct_pile_app_level;return 0;
}

function VIDEOTEST_MP4-create(){
    local -i _fct_pile_app_level=5;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    if [ ! -f "$VIDEOTEST_MP4_PATHNAME" ]
    then
        hr >> "$ffmpeg_log_pathname";
        titre4 "# - Créer VIDEOTEST à partir de VIDEOTEST_ORI - #";
        # - Générer le UUID - #
        local _UUID="$(cat /proc/sys/kernel/random/uuid)";
        display-vars '$_UUID' "$_UUID";

        titre4 "# - Ajouter le fichier de sous titre et l'UUID - #";
        local _file_srt_pathname="$APPS_REP/ffmpeg/tests/medias/ocean.srt";
        eval-echo "ffmpeg -y -hide_banner -i \"$VIDEOTEST_ORI_PATHNAME\" -i \"$_file_srt_pathname\" -codec:v copy -codec:a copy -codec:s mov_text -metadata UUID="$_UUID" -metadata comment="UUID=$_UUID"  \"$VIDEOTEST_MP4_PATHNAME\" 2>>$ffmpeg_log_pathname" ;
        erreur-no-description-echo;
    fi
    if [ ! -f "$VIDEOTEST_MP4_PATHNAME" ]
    then
        erreurs-pile-add-echo "VIDEOTEST_MP4_PATHNAME: '$VIDEOTEST_MP4_PATHNAME' Non créé!"
    fi

    # - Extraire une image - #
    if [ ! -f "VIDEOTEST_PNG" ]
    then
        eval-echo "ffmpeg -hide_banner -y -ss 00:00:08 -i \"$VIDEOTEST_MP4_PATHNAME\"  -vframes 1 -q:v 2 \"$VIDEOTEST_PNG\" 2>/dev/null";
    fi

    fct-pile-app-out 0 $_fct_pile_app_level;return 0;
}

function VIDEOTEST_MKV-create(){
    local -i _fct_pile_app_level=5;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    if [ ! -f "$VIDEOTEST_MKV_PATHNAME" ]
    then
        hr >> "$ffmpeg_log_pathname";
        titre4 "# - Créer VIDEOTEST_MKV à partir de VIDEOTEST_ORI - #";
        # - Générer le UUID - #
        local _UUID="$(cat /proc/sys/kernel/random/uuid)";
        display-vars '$_UUID' "$_UUID";

        titre4 "# - Ajouter le fichier de sous titre et l'UUID - #";
        local _file_srt_pathname="$APPS_REP/ffmpeg/tests/medias/ocean.srt";
        eval-echo "ffmpeg -y -hide_banner -i \"$VIDEOTEST_ORI_PATHNAME\" -i \"$_file_srt_pathname\" -codec:v copy -codec:a copy -codec:s copy -metadata UUID="$_UUID" -metadata comment="UUID=$_UUID"  \"$VIDEOTEST_MKV_PATHNAME\" 2>>$ffmpeg_log_pathname" ;
        erreur-no-description-echo;
    fi

    if [ ! -f "$VIDEOTEST_MKV_PATHNAME" ]
    then
        erreurs-pile-add-echo "VIDEOTEST_MKV_PATHNAME: '$VIDEOTEST_MKV_PATHNAME' Non créé!"
    fi

    fct-pile-app-out 0 $_fct_pile_app_level;return 0;
}


##############################################################
#titre2 "Initialisation de l'objet VIDEOTEST_* pour les tests";
##############################################################
hr >> "$ffmpeg_log_pathname";


#yt-dlp -o "/media/mediatheques/CHARGE-Blender_Open_Movie.mp4" --format 18 https://www.youtube.com/watch?v=UXqq0ZvbOnk
declare -g VIDEOTEST_MP4_REP="/media/mediatheques";
declare -g VIDEOTEST_MP4_NOM='CHARGE-Blender_Open_Movie';
declare -g VIDEOTEST_MP4_EXT='mp4';
declare -g VIDEOTEST_MP4_NAME="${VIDEOTEST_MP4_NOM}.${VIDEOTEST_MP4_EXT}";
declare -g VIDEOTEST_MP4_PATHNAME="$VIDEOTEST_MP4_REP/$VIDEOTEST_MP4_NAME";
declare -g VIDEOTEST_MP4_URL="https://www.youtube.com/watch?v=UXqq0ZvbOnk";
declare -g VIDEOTEST_PNG="$VIDEOTEST_MP4_REP/${VIDEOTEST_MP4_NOM}.png";


titre3 "Création de VIDEOTEST_ORI_PATHNAME";
declare -g VIDEOTEST_ORI_PATHNAME="$VIDEOTEST_MP4_REP/${VIDEOTEST_MP4_NOM}-ori.${VIDEOTEST_MP4_EXT}";
display-vars 'VIDEOTEST_ORI_PATHNAME' "$VIDEOTEST_ORI_PATHNAME";
VIDEOTEST_ORI-dw;


titre3 "Création de VIDEOTEST_MP4_PATHNAME";
display-vars 'VIDEOTEST_MP4_REP     ' "$VIDEOTEST_MP4_REP";
display-vars 'VIDEOTEST_MP4_NOM     ' "$VIDEOTEST_MP4_NOM";
display-vars 'VIDEOTEST_MP4_EXT     ' "$VIDEOTEST_MP4_EXT";
display-vars 'VIDEOTEST_MP4_NAME    ' "$VIDEOTEST_MP4_NAME";
display-vars 'VIDEOTEST_MP4_PATHNAME' "$VIDEOTEST_MP4_PATHNAME";
display-vars 'VIDEOTEST_MP4_URL     ' "$VIDEOTEST_MP4_URL";
VIDEOTEST_MP4-create;
display-vars          'VIDEOTEST_MP4_PATHNAME' "$VIDEOTEST_MP4_PATHNAME";
ffmpeg-streams-show  "$VIDEOTEST_MP4_PATHNAME";
ffmpeg-metadata-show "$VIDEOTEST_MP4_PATHNAME";


titre3 "Création de VIDEOTEST_MKV_PATHNAME";
declare -g VIDEOTEST_MKV_EXT='mkv';                                             display-vars 'VIDEOTEST_MKV_EXT     ' "$VIDEOTEST_MKV_EXT";
declare -g VIDEOTEST_MKV_NAME="${VIDEOTEST_MP4_NOM}.${VIDEOTEST_MKV_EXT}";      display-vars 'VIDEOTEST_MKV_NAME    ' "$VIDEOTEST_MKV_NAME";
declare -g VIDEOTEST_MKV_PATHNAME="$VIDEOTEST_MP4_REP/$VIDEOTEST_MKV_NAME";     display-vars 'VIDEOTEST_MKV_PATHNAME' "$VIDEOTEST_MKV_PATHNAME";
VIDEOTEST_MKV-create;
display-vars         'VIDEOTEST_MKV_PATHNAME' "$VIDEOTEST_MKV_PATHNAME";
ffmpeg-streams-show  "$VIDEOTEST_MKV_PATHNAME";
ffmpeg-metadata-show "$VIDEOTEST_MKV_PATHNAME";

echo '';
return 0;
