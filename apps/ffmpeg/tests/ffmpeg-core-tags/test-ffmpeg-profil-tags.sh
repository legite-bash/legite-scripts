echo "${BASH_SOURCE[0]}";

# clear; gtt.sh -d --libExecAuto apps/ffmpeg/tests/testffmpeg-profils lib
# date de création    : 2024.?
# date de modification: 2024.08.12
# fichier code: core/ffmpeg-core-tags.sh


# clear; gtt.sh -d --libExecAuto apps/ffmpeg/tests/testffmpeg-profils-tags test-ffmpeg-profils-tags-tags
addLibrairie 'test-ffmpeg-profils-tags';
function      test-ffmpeg-profils-tags(){
    local _SRC_REP="$VIDEOTEST_REP";
    local _DESTINATION_REP="$tmp_rep";
    
    if ! cd "$_SRC_REP"
    then
        erreurs-pile-add-echo "Impossible d'entrer dans le repertoire '$_SRC_REP'";
        return $E_INODE_NOT_EXIST;
    fi

    titre1 "${FUNCNAME[0]}: ($PWD)";

    ffmpeg_profils_params_crf=18;
    ffmpeg_profils_params_preset='veryslow';
    ffmpeg_profils_params_tune='film';

    local _fichier_suffixe="";

    ffmpeg_metadata_artist='';
    ffmpeg_metadata_album='Gimp';
    ffmpeg_metadata_language='eng';
    ffmpeg_metadata_genre='SF 3D';

    ffmpeg_metadata_date="2011";
    ffmpeg_metadata_title="Démonstration d'une vidéo 3D avec GIMP";

    test-ffmpeg-profils-tags-exemple-vcopy-acopy;
    ffmpeg-profil-show;

    #carillion 'Defend_Castle';
    return 0;
}


#########
# FILMS #
#########

# clear; gtt.sh -d --libExecAuto apps/ffmpeg/tests/testffmpeg-profils   test-ffmpeg-profils-tags-exemple-vcopy-acopy
addLibrairie 'test-ffmpeg-profils-tags-exemple-vcopy-acopy';
function      test-ffmpeg-profils-tags-exemple-vcopy-acopy(){

    local _profil_nom='vcopy-acopy';
    ffmpeg-profil-init "$_profil_nom";
    ffmpeg-profil-global-raw-add '-hide_banner -y';
    ffmpeg-profil-params-add    faststart metadatas-del metadatas;
    ffmpeg-profil-params-add    faststart ;
    ffmpeg-profil-params-add    vcopy acopy '-sn'  metadatas-del metadatas

    ffmpeg-profil-exec "$_profil_nom" "$VIDEOTEST_PATHNAME" "${tmp_rep}/${FUNCNAME[0]}.mp4" ;

    return 0;
}



addLibrairie 'test-ffmpeg-profils-tags-exemple-vcopy-an';
function      test-ffmpeg-profils-tags-exemple-vcopy-an(){

    ffmpeg-profil-init "exemple-1";
    ffmpeg-profil-global-raw-add '-hide_banner -y';
    ffmpeg-profil-params-add     faststart metadatas-del metadatas;
    ffmpeg-profil-params-add    '-map 0:0' vcopy  x264-crf-preset '-vf scale=1152:-2'      '-metadata:s:v:0 language=eng';
    ffmpeg-profil-params-add    '-map 0:1' ar44100 stereo        '-metadata:s:a:0 language=end';

    ffmpeg-profil-show;

    echo 'profil par défaut';
    eval-echo "ffmpeg_exec_profil \"exemple-1\" \"$VIDEOTEST_PATHNAME\" \"${tmp_rep}/exemple-1.mp4\"" ;

    #echo '';
    #local _suf="${ffmpeg_profils_suf["$ffmpeg-profil-nom"]:-""}";
    #ffmpeg_exec_profil "$_src_pathname" "${_DESTINATION_REP}/${_fichier_nom}-$_suf.mp4" ;
    return 0;
}


function test-ffmpeg-profils-tags-exemple-vcopy-an0(){

    ffmpeg-profil-init "exemple-1";
    ffmpeg-profil-global-raw-add '-hide_banner -y';        # un seul parametre!
    ffmpeg-profil-params-add     faststart metadatas-del metadatas;
    ffmpeg-profil-params-add    '-map 0:0' vcopy  x264-crf-preset '-vf scale=1152:-2'      '-metadata:s:v:0 language=eng';
    ffmpeg-profil-params-add    '-map 0:1' ar44100 stereo        '-metadata:s:a:0 language=end';

    ffmpeg-profil-show;

    echo 'profil par défaut';
    eval-echo "ffmpeg_exec_profil \"exemple-1\" \"$VIDEOTEST_PATHNAME\" \"${tmp_rep}/exemple-1.mp4\"" ;

    #echo '';
    #local _suf="${ffmpeg_profils_suf["$ffmpeg-profil-nom"]:-""}";
    #ffmpeg_exec_profil "$_src_pathname" "${_DESTINATION_REP}/${_fichier_nom}-$_suf.mp4" ;
    return 0;
}


function 2012.04.11-the_avengers(){
    local _date="2012"
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='The avengers 1';
    local ffmpeg_profils_params_crf=18;
    local ffmpeg_profils_params_preset='veryslow';
    local _profil_nom="$_fichier_nom";
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add "$_profil_nom" faststart map_00_01 x264-crf-preset aac ar44100 stereo metadatas-del metadatas
 

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}.mp4" ;
    return 0;
}

function 2012.07.04-The_Amazing_Spiderman-1(){
    local _date="2012"
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='The Amazing Spiderman 1';

    local _profil_nom="$_fichier_nom";
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01 x264-crf-preset aac ar44100 metadatas-del metadatas
    # natif stereo

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}.mp4" ;
    return 0;
}

function 2013.05.03-Ironman-3(){
    local _date="2013";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Ironman 3';

    local _profil_nom="$_fichier_nom";
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01 x264-crf-preset aac ar44100 metadatas-del metadatas
    # natif stereo

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}.mp4" ;
    return 0;
}

function 2013.10.30-Thor-2-the_dark_world(){
    local _date="2013";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Thor 2 - The dark world';

    local _profil_nom="$_fichier_nom";
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01;
    #ffmpeg-profil-params-add      vcopy acopy metadatas-del metadatas
    ffmpeg-profil-params-add      nettoyage;

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-copy.mp4" ;
    return 0;
}

function 2013-Sparks(){
    local _date="2013";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Sparks';

    local _profil_nom="$_fichier_nom";
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01 x264-crf-preset aac ar44100 metadatas-del metadatas
    # natif stereo

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}.mp4" ;
    return 0;
}

function 2014.04.30-The_Amazing_Spiderman-2(){
    local _date="2014";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='The Amazing Spiderman 2';

    local _profil_nom="$_fichier_nom";
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01 x264-crf-preset aac ar44100 metadatas-del metadatas
    # natif stereo

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}.mp4" ;
    return 0;
}

function 2014.05.10-Captain_America-2-The_winter_soldier(){
    local _date="2014";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title="Captain America 3 - Le soldat d'hivers";

    local _profil_nom="$_fichier_nom";
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01 x264-crf-preset aac '-sn' metadatas-del metadatas
    # natif stereo

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}.mp4" ;
    return 0;
}

function 2014.08.01-les-gardiens-de-la-galaxie-1(){
    local _date="2014";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Les gardiens de la galaxie 1';

    local _profil_nom="$_fichier_nom";
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01 x264-crf-preset aac '-sn' metadatas-del metadatas
    # AF: supprimer le sous titre

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}.mp4" ;
    return 0;
}

function 2015.05.01-Avengers-2-Age_Of_Ultron(){
    local _date="2015";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Les gardiens de la galaxie 1';
    
    local _fichier_suffixe='aac-stereo';
    local _profil_nom="$_fichier_nom";
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01
    ffmpeg-profil-params-add      vcopy aac


    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${_fichier_suffixe}.mp4" ;
    return 0;
}

function 2015.06.29-antman-1(){
    local _date="2015";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Antman 1';

    local _profil_nom="$_fichier_nom";
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01 x264-crf-preset aac  metadatas-del metadatas
    # natif stereo

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}.mp4" ;
    return 0;
}

function 2016.04.12-Captain_America-3-Civil_War(){
    local _date="2016";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Captain America 3 - Civil war';

    local _profil_nom="$_fichier_nom";
     local _fichier_suffixe='';
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01;
    #ffmpeg-profil-params-add      x264-crf-preset aac '-sn' metadatas-del ffmpeg_metadata_artist;  # _fichier_suffixe="${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}";
    ffmpeg-profil-params-add      nettoyage;             _fichier_suffixe="nettoyage";
    # natif stereo

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${_fichier_suffixe}.mp4" ;
    return 0;
}

function 2016.04.16-Black_Widow(){
    local _date="2016";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Black Widow';

    local _profil_nom="$_fichier_nom";
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01 x264-crf-preset aac '-sn' metadatas-del metadatas
    # natif stereo

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}.mp4" ;
    return 0;
}

function 2016-Docteur_Strange-1(){
    local _date="2016";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Docteur Strange 1';

    local _profil_nom="$_fichier_nom";
    local _fichier_suffixe='';
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01;
    #ffmpeg-profil-params-add      x264-crf-preset aac '-sn' metadatas-del ffmpeg_metadata_artist;  # _fichier_suffixe="${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}";
    ffmpeg-profil-params-add      nettoyage;             _fichier_suffixe="nettoyage";
    # natif stereo

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${_fichier_suffixe}.mp4" ;
    return 0;
}

function 2017.05.05-Les_gardiens_de_la_galaxie-2(){
    local _date="2017";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Les gardiens de la galaxie-2';

    local _profil_nom="$_fichier_nom";
    local _fichier_suffixe='';
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01;
    #ffmpeg-profil-params-add      x264-crf-preset aac '-sn' metadatas-del ffmpeg_metadata_artist;  # _fichier_suffixe="${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}";
    ffmpeg-profil-params-add      nettoyage;             _fichier_suffixe="nettoyage";
    # natif stereo

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${_fichier_suffixe}.mp4" ;    return 0;
}

function 2017.07.12-Spiderman-3-Homecoming(){
    local _date="2017";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Spiderman 3';

    local _profil_nom="$_fichier_nom";
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01 x264-crf-preset aac ar44100 '-sn' metadatas-del metadatas
    # natif stereo

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}.mp4" ;
    return 0;
}

function 2017.11.03-Thor-3-Ragnarok(){
    local _date="2017";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Thor 3 - Ragnarok';

    local _profil_nom="$_fichier_nom";
    local _fichier_suffixe='';
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01;
    #ffmpeg-profil-params-add      x264-crf-preset aac '-sn' metadatas-del ffmpeg_metadata_artist;  # _fichier_suffixe="${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}";
    ffmpeg-profil-params-add      nettoyage;             _fichier_suffixe="nettoyage";
    # natif stereo

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${_fichier_suffixe}.mp4" ;   return 0;
}

function 2018.02.16-Black_Panther(){
    local _date="2018";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Black Panther';

    local _profil_nom="$_fichier_nom";
    local _fichier_suffixe='';
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01;
    #ffmpeg-profil-params-add      x264-crf-preset aac '-sn' metadatas-del ffmpeg_metadata_artist;  # _fichier_suffixe="${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}";
    ffmpeg-profil-params-add      nettoyage;             _fichier_suffixe="nettoyage";
    # natif stereo

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${_fichier_suffixe}.mp4" ;
    return 0;
}

function 2018.04.27-The_Avengers-3-InfiniteWar(){
    local _date="2018";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='The avengers 3 - Infinite war';

    local _profil_nom="$_fichier_nom";
    local _fichier_suffixe='';
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01;
    #ffmpeg-profil-params-add      x264-crf-preset aac '-sn' metadatas-del ffmpeg_metadata_artist;  # _fichier_suffixe="${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}";
    ffmpeg-profil-params-add      nettoyage;             _fichier_suffixe="nettoyage";
    # natif stereo

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${_fichier_suffixe}.mp4" ;
    return 0;
}

function 2018.07.06-Antman-2-Antman_et_la_guepe(){
    local _date="2018";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Antman 2 - Antman et la guêpe';

    local _profil_nom="$_fichier_nom";
    local _fichier_suffixe='';
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01;
    #ffmpeg-profil-params-add      x264-crf-preset aac '-sn' metadatas-del ffmpeg_metadata_artist;  # _fichier_suffixe="${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}";
    ffmpeg-profil-params-add      nettoyage;             _fichier_suffixe="nettoyage";
    # natif stereo

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${_fichier_suffixe}.mp4" ;
    return 0;
}

function 2018.10.10-Venom-1(){
    local _date="2018";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Venon 1';

    local _profil_nom="$_fichier_nom";
    local _fichier_suffixe='aac';
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01;
    ffmpeg-profil-params-add      vcopy  aac stereo # _fichier_suffixe="aac";
    # natif stereo

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${_fichier_suffixe}.mp4" ;
    return 0;
}

function 2019.03.06-Captain_Marvel(){
    local _date="2019";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Captain Marvel';

    local _profil_nom="$_fichier_nom";
    local _fichier_suffixe='';
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01;
    #ffmpeg-profil-params-add      x264-crf-preset aac '-sn' metadatas-del ffmpeg_metadata_artist;  # _fichier_suffixe="${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}";
    ffmpeg-profil-params-add      nettoyage;             _fichier_suffixe="nettoyage";
    # natif stereo

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${_fichier_suffixe}.mp4" ;
    return 0;
}

function 2019.04.24-Avengers-4-Endgame(){
    local _date="2019";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Avengers 4 - Endgame';

    local _profil_nom="$_fichier_nom";
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01 x264-crf-preset aac ar44100 '-sn' metadatas-del metadatas

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}.mp4" ;
    return 0;
}

function 2019.08-Spiderman-4-Far_From_Home(){
    local _date="2019";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";
    
    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Spiderman 4 - Far from Home';

    local _profil_nom="$_fichier_nom";
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01;
    ffmpeg-profil-params-add      '-codec:v copy  -codec:a copy' '-sn' metadatas-del metadatas

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-copy.mp4" ;
    return 0;
}

function 2021.11.03-Eternals(){
    local _date="2021";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Les éternels';

    local _profil_nom="$_fichier_nom";
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01 x264-crf-preset aac  '-sn' metadatas-del metadatas

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}.mp4" ;
    return 0;
}

function 2021.12-Spiderman-5-No_Way_Home(){
    local _date="2021";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Spiderman 5- No way Home';

    local _profil_nom="$_fichier_nom";
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01 x264-crf-preset aac  '-sn' metadatas-del metadatas

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}.mp4" ;
    return 0;
}

function 2021-Venom-2-Let_There_Be_Carnage(){
    local _date="2021";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Venom 2 - Let There Be Carnage';

    local _profil_nom="$_fichier_nom";
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01 x264-crf-preset aac  '-sn' metadatas-del metadatas

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}.mp4" ;
    return 0;
}

function 2022.06-Docteur_Strange-2-In_the_multiverse_of_madness(){
    local _date="2022";
    local _fichier_nom="${FUNCNAME[0]}";
    local _src_pathname="${_fichier_nom}.mp4";

    ffmpeg_metadata_date="$_date";
    ffmpeg_metadata_title='Docteur Strange 2 - In the multiverse of madness';

    local _profil_nom="$_fichier_nom";
    local _fichier_suffixe='';
    ffmpeg-profil-global-raw-add "$_profil_nom" '-hide_banner -n';
    ffmpeg-profil-params-add      faststart map_00_01;
    #ffmpeg-profil-params-add      x264-crf-preset aac '-sn' metadatas-del ffmpeg_metadata_artist;  # _fichier_suffixe="${ffmpeg_profils_params_crf}-${ffmpeg_profils_params_preset}";
    ffmpeg-profil-params-add      nettoyage;             _fichier_suffixe="nettoyage";
    # natif stereo

    ffmpeg_exec_profil "$_src_pathname" "$_profil_nom" "${_DESTINATION_REP}/${_fichier_nom}-${_fichier_suffixe}.mp4" ;
    return 0;
}
