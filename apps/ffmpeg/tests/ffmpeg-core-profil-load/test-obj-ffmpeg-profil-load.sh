echo "${BASH_SOURCE[0]}";

# date de création    : 2024.08.29
# date de modification: 2024.09.05
# clear; gtt.sh --show-libsLevel2 ffmpeg/tests/ffmpeg-core-profil-load/

# clear; gtt.sh -d ffmpeg/tests/ffmpeg-core-profil-load/ test-obj-ffmpeg-profil-load
addLibrairie 'test-obj-ffmpeg-profil-load';
function      test-obj-ffmpeg-profil-load(){
    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code: apps/ffmpeg/ffmpeg.sh";

    # - test1 - #
    #test-fct-ffmpeg-profil--load 'defaut' "${VIDEOTEST_PATHNAME}";
    #ffmpeg-exec;

    # - test1 - #
    #test-fct-ffmpeg-profil--load 'container-to-mp4' "${VIDEOTEST_PATHNAME}";
    #ffmpeg-exec;

    # - test1 - #
    test-fct-ffmpeg-profil--load 'container-to-mkv' "${VIDEOTEST_PATHNAME}";
    ffmpeg-exec;

    return 0;
}



# test-fct-ffmpeg-profil--load( profil_nom pathname )
# Charge la fonction qui correspond au nom du profil donné en $1
# FONCTION INTERNE: N'est pas concut pour être appeller depuis l'exptérieur
function      test-fct-ffmpeg-profil-load(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}()";
    titreInfo "";


    local _profil_nom="${1:-""}";
    local _in_pathname="${2:-""}";
    local _programme="";


    titre2 "ffmpeg: profil: ffmpeg-profil-load-$_profil_nom":
    eval-echo "ffmpeg-profil-load-$_profil_nom '$_in_pathname'";
    erreur-no-description-echo;
    ${_programme}-display-vars;


    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

return 0;
