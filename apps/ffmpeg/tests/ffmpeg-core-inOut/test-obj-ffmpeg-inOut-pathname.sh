echo "${BASH_SOURCE[0]}";

# gtt.sh --show-libsLevel2 apps/ffmpeg/tests/test-ffmpeg.sh lib
# date de création    : 2024.04.23
# date de modification: 2024.09.20


# clear; gtt.sh -d --libExecAuto ffmpeg/tests/ffmpeg-core-inOut test-obj-ffmpeg-inOut-pathname
addLibrairie 'test-obj-ffmpeg-inOut-pathname';
function      test-obj-ffmpeg-inOut-pathname(){
    
    titre1 "${FUNCNAME[0]}:BEGIN";
    titreInfo "Fichier code:apps/ffmpeg/ffmpeg-core-intOut.sh";

    titre2   "initialisation:";
    eval-echo "ffmpeg-in-init";
    erreur-no-description-echo $?;
    ffmpeg-in-display-vars;


    titre2   "ffmpeg-in-:";

    #titre3   "ffmpeg-in-pathname; # Sans parametre";
    #eval-echo "ffmpeg-in-pathname";
    #erreur-no-description-echo $?;


    #titre3   "ffmpeg-in-pathname ''; # Parametre vide";
    #eval-echo "ffmpeg-in-pathname ''";
    #erreur-no-description-echo $?;
    #ffmpeg-in-display-vars;


    #titre3   "ffmpeg-in-pathname 'VIDEOTEST_REP'; # Avec un repertoire";
    #eval-echo "ffmpeg-in-init";
    #eval-echo "ffmpeg-in-pathname \"$VIDEOTEST_REP\"";
    #erreur-no-description-echo $?;
    #ffmpeg-in-display-vars;

    titre3   "ffmpeg-in-pathname '/dev/null/fichier_inexistant.ext'; # Avec un fichier inexistant";
    eval-echo "ffmpeg-in-init";
    eval-echo "ffmpeg-in-pathname \"/dev/null/fichier_inexistant.ext\"";
    erreur-no-description-echo $?;
    ffmpeg-in-display-vars;
    ffmpeg-out-display-vars;


    titre3   "ffmpeg-in-pathname 'VIDEOTEST_PATHNAME';              # Avec un pathname de fichier";
    eval-echo "ffmpeg-in-init";
    eval-echo "ffmpeg-in-pathname \"$VIDEOTEST_PATHNAME\"";
    erreur-no-description-echo $?;
    ffmpeg-in-display-vars;
    ffmpeg-out-display-vars;


    titre3   "ffmpeg-in-pathname '/dev/null/destination.extention'; # Avec un pathname de fichier";
    eval-echo "ffmpeg-in-init";
    eval-echo "ffmpeg-in-pathname \"/dev/null/destination.extention\"";
    erreur-no-description-echo $?;
    ffmpeg-out-display-vars;

    ########################
    titre2   "ffmpeg-out-:";


    titre3   "ffmpeg-out-name 'nouveau_nom.nouvelle_extention'; # Avec un name";
    eval-echo "ffmpeg-out-name \"nouveau_nom.nouvelle_extention\"";
    erreur-no-description-echo $?;
    ffmpeg-out-display-vars;

    
    titre3   "ffmpeg-out-pathname '/dev/null/destination.extention'; # Avec un pathname de fichier";
    eval-echo "ffmpeg-out-pathname \"/dev/null/destination.extention\"";
    erreur-no-description-echo $?;
    ffmpeg-out-display-vars;


    ## Cette forme n'est pas prise en charge
    #titre3   "ffmpeg-out-name 'nouveau_nomSansExtention'; # Avec un nom seul";
    #eval-echo "ffmpeg-out-name \"nouveau_nomSansExtention\"";
    #erreur-no-description-echo $?;
    #ffmpeg-out-display-vars;


    titre3   "ffmpeg-out-nom 'nouveau_nom3'; # Changement de nom seul";
    eval-echo "ffmpeg-out-nom \"nouveau_nom3\"";
    erreur-no-description-echo $?;
    ffmpeg-out-display-vars;


    titre3   "ffmpeg-out-ext 'nouvelle_extention2'; # Avec une extention name";
    eval-echo "ffmpeg-out-ext \"nouvelle_extention2\"";
    erreur-no-description-echo $?;
    ffmpeg-out-display-vars;



    titre1 "${FUNCNAME[0]}:END";
    return 0;
}

#libExecAuto test-fct-ffmpeg-streams-show;

return 0;

