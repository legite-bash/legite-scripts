echo-d "${BASH_SOURCE[0]}" 1;
# https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu

export  ffmpeg_sources='/tmp/ffmpeg_sources'
export  ffmpeg_build='/tmp/ffmpeg_build'
export  ffmpeg_bin="$HOME/bin"
declare ffmpeg_lib_param='';  # parametre de compilation dédié aux librairies
mkdir -p $ffmpeg_sources ~/bin $ffmpeg_build


function compil_FFmpeg_old(){

    #if [ $IS_ROOT -eq 0 ];then return; fi
    echo $BOLD${INFO}Compilation et installation de FFmpeg$NORMAL$NOBOLD
    echo 'REFS'
    echo '1-https://trac.ffmpeg.org/wiki/CompilationGuide'
    echo '2-https://trac.ffmpeg.org/wiki/CompilationGuide/Generic'
    echo '3-https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu'
    echo 'https://www.xmodulo.com/compile-ffmpeg-ubuntu-debian.html'
    echo 'https://gist.github.com/dturner/11fe5c8e420c2a73e15537274aafbd3a'
    echo './configure --help'
    echo ''

    eval-echo 'sudo aptitude -y install git make nasm pkg-config libx264-dev libxext-dev libxfixes-dev zlib1g-dev'
    #local -i err=0;

    echo -e '\n# -- AJOUT DE LA CLEF GPG -- # '
    curl https://ffmpeg.org/ffmpeg-devel.asc | gpg --import


    echo -e '\n# -- CLONE -- # '
    cd /tmp
    eval-echo 'git clone https://github.com/FFmpeg/FFmpeg.git'
    #displayVar 'evalErr' "$evalErr"
    if [ $evalErr -ne 0 -a $evalErr -ne 128 ];then erreurs-pile-add-echo "${FUNCNAME[0]}: Erreur lors du téléchargement du dépot git"; 
        return $E_EXIT_BAD;
    fi

    cd 'FFmpeg'
    if [ $? -ne 0 ];then erreurs-pile-add-echo "${FUNCNAME[0]}: Impossible d'aller dans le repertoitoire /tmp/FFmpeg"; return $E_EXIT_BAD;fi


    echo -e '\n # -- CONFIGURE -- # '
    local FFmpeg_params=''
    FFmpeg_params="$FFmpeg_params --disable-doc --disable-htmlpages --disable-manpages --disable-podpages --disable-txtpages"
    #FFmpeg_params="$FFmpeg_params --enable-nonfree --enable-gpl --enable-libx264  --enable-zlib"
    echo 'https://gist.github.com/dturner/11fe5c8e420c2a73e15537274aafbd3a'
    eval-echo "./configure $FFmpeg_params"
    if [ $? -ne 0 ];then erreurs-pile-add-echo "${FUNCNAME[0]}: Erreur lors de ./configure"; return $E_EXIT_BAD;fi

    echo -e '\n# -- MAKE -- # '
    time eval-echo 'make'
    if [ $evalErr -ne 0 ];then erreurs-pile-add-echo "${FUNCNAME[0]}: Erreur lors de make"; return $E_EXIT_BAD;fi

    echo -e '\n# -- INSTALL -- # '
    #eval-echo 'sudo install'
    #if [ $? -ne 0 ];then erreurs-pile-add-echo "${FUNCNAME[0]}: Erreur lors de install"; return $E_EXIT_BAD;fi

    return 0;
}
#librairiesTbl['compil_FFmpeg']=compil_FFmpeg;


addLibrairie 'compil_FFmpeg' "Compile ffmpeg";
function compil_FFmpeg(){

    #if [ $IS_ROOT -eq 0 ];then return; fi
    echo $BOLD${INFO}Compilation et installation de FFmpeg$NORMAL$NOBOLD
    echo 'REFS'
    echo '1-https://trac.ffmpeg.org/wiki/CompilationGuide';
    echo '2-https://trac.ffmpeg.org/wiki/CompilationGuide/Generic';
    echo '3-https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu';
    echo 'https://www.xmodulo.com/compile-ffmpeg-ubuntu-debian.html';
    echo 'https://gist.github.com/dturner/11fe5c8e420c2a73e15537274aafbd3a';
    echo './configure --help';
    echo '';

    #ffmpeg_dependance

    notifsAdd "${FUNCNAME[0]}"
    return 0;
}

################################
# PREPARATION A LA COMPILATION #
###############################
    addLibrairie 'ffmpeg_dependanceConsole' 'Ajoute les dépendances pour la campilation de FFPMPEG sans serveur X'
    function ffmpeg_dependanceConsole(){

        sudo aptitude update  && sudo aptitude -y install \
            autoconf \
            automake \
            build-essential \
            cmake \
            git-core \
            libass-dev \
            libfreetype6-dev \
            libgnutls28-dev \
            libmp3lame-dev \
            libtool \
            libvorbis-dev \
            meson \
            ninja-build \
            pkg-config \
            texinfo \
            wget \
            yasm \
            zlib1g-dev \
            gnutls-bin
        # installation requis et qui n'est pas dans la doc: gnutls-bin
        if [[ $? -eq 0 ]]
        then notifs-pile-add-echo 'Dependance ajoutée ou déjà présente.'
        else erreurs-pile-add-echo 'Dependence non ajouté'
        fi

        return 0;
    }

    addLibrairie 'ffmpeg_dependanceX' 'Ajoute les dépendances pour la compilation de FFPMPEG avec serveur X'
    function ffmpeg_dependanceX(){

        ffmpeg_dependanceConsole

        sudo aptitude update && sudo aptitude -y install \
            libsdl2-dev \
            libva-dev \
            libvdpau-dev \
            libxcb1-dev \
            libxcb-shm0-dev \
            libxcb-xfixes0-dev

        if [[ $? -eq 0 ]]
        then notifs-pile-add-echo 'Dependance ajoutée ou déjà présente.'
        else erreurs-pile-add-echo 'Dependence non ajouté'
        fi
        
        return 0;
    }

    addLibrairie 'ffmpeg_dependanceX_remove' 'Supprime les dépendances pour la campilation de FFPMPEG avec serveur X'
    function ffmpeg_dependanceX_remove(){

        ffmpeg_dependanceConsole

        sudo aptitude purge \
            libsdl2-dev \
            libva-dev \
            libvdpau-dev \
            libxcb1-dev \
            libxcb-shm0-dev \
            libxcb-xfixes0-dev

        if [[ $? -eq 0 ]]
        then notifs-pile-add-echo 'Dependance supprimées.'
        else erreurs-pile-add-echo 'Dependence non supprimées'
        fi
        fctOut "${FUNCNAME[0]}(0)"; return 0;
    }
#


########
# Libs #
########
# - x264 - #
    addLibrairie 'ffmpeg_installLib_x264' 'Compilation FFMpeg: ajout de la librairie: x264'
    function ffmpeg_installLib_x264(){

        eval-echo 'sudo aptitude install libx264-dev'
        if [[ $? -eq 0 ]]
        then notifs-pile-add-echo 'Lib FFMpeg installé.';ffmpeg_lib_param='--enable-gpl --enable-libx264';
        else erreurs-pile-add-echo 'Lib FFMpeg non installé.'
        fi

        return 0;
    }

    addLibrairie 'ffmpeg_compilLib_x264' 'Compilation FFMpeg: compilation de la librairie: x264'
    function ffmpeg_compilLib_x264(){

        cd $ffmpeg_sources && \
        git -C x264 pull 2> /dev/null || git clone --depth 1 https://code.videolan.org/videolan/x264.git && \
        cd x264 && \
        PATH="$ffmpeg_bin:$PATH" PKG_CONFIG_PATH="$ffmpeg_build/lib/pkgconfig" ./configure --prefix="$ffmpeg_build" --bindir="$ffmpeg_bin" --enable-static --enable-pic && \
        PATH="$ffmpeg_bin:$PATH" make && \
        make install

        if [[ $? -eq 0 ]]
        then notifs-pile-add-echo 'Lib FFMpeg x264 installé.';ffmpeg_lib_param='--enable-gpl --enable-libx264';
        else erreurs-pile-add-echo 'Lib FFMpeg x264 non installé.'
        fi

        return 0;
    }
#

# - x265 - #
    addLibrairie 'ffmpeg_addLib_' 'Compilation FFMpeg: ajout de la librairie: x265'
    function ffmpeg_addLib_x265(){

        eval-echo 'sudo aptitude install libx265-dev libnuma-dev'
        if [[ $? -eq 0 ]]
        then
            notifs-pile-add-echo  'Lib FFMpeg ajoutée.';
            ffmpeg_lib_param='--enable-gpl --enable-libx265';
        else
            erreurs-pile-add-echo 'Lib FFMpeg non ajoutée.'
        fi

        return 0;
    }

    addLibrairie 'ffmpeg_compilLib_x265' 'Compilation FFMpeg: compilation de la librairie: x265'
    function ffmpeg_compilLib_x265(){

        sudo aptitude install libnuma-dev && \
        cd $ffmpeg_sources && \
        wget -O x265.tar.bz2 https://bitbucket.org/multicoreware/x265_git/get/master.tar.bz2 && \
        tar xjvf x265.tar.bz2 && \
        cd multicoreware*/build/linux && \
        PATH="$ffmpeg_bin:$PATH" cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX="$ffmpeg_build" -DENABLE_SHARED=off ../../source && \
        PATH="$ffmpeg_bin:$PATH" make && \
        make install

        if [[ $? -eq 0 ]]
        then notifs-pile-add-echo 'Lib FFMpeg x265 ajoutée.';ffmpeg_lib_param='--enable-gpl --enable-libx265';
        else erreurs-pile-add-echo 'Lib FFMpeg x265 non ajoutée.'
        fi

        return 0;
    }
#

##########
# FFMPEG #
##########
    addLibrairie 'ffmpeg_compil_ffmpeg_lib_all' 'Compilation de FFMpeg avec toutes les libs'
    function ffmpeg_compil_ffmpeg_lib_all(){

        cd $ffmpeg_sources && \
        wget -O ffmpeg-snapshot.tar.bz2 https://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2 && \
        tar xjvf ffmpeg-snapshot.tar.bz2 && \
        cd ffmpeg && \
        PATH="$ffmpeg_bin:$PATH" PKG_CONFIG_PATH="$ffmpeg_build/lib/pkgconfig" ./configure \ 
        --prefix="$ffmpeg_build" \
        --pkg-config-flags="--static" \
        --extra-cflags="-I$ffmpeg_build/include" \
        --extra-ldflags="-L$ffmpeg_build/lib" \
        --extra-libs="-lpthread -lm" \
        --ld="g++" \
        --bindir="$ffmpeg_bin" \
        --enable-gpl \
        --enable-gnutls \
        --enable-libaom \
        --enable-libass \
        --enable-libfdk-aac \
        --enable-libfreetype \
        --enable-libmp3lame \
        --enable-libopus \
        --enable-libsvtav1 \
        --enable-libdav1d \
        --enable-libvorbis \
        --enable-libvpx \
        --enable-libx264 \
        --enable-libx265 \
        --enable-nonfree && \
        PATH="$ffmpeg_bin:$PATH" make && \
        make install && \
        hash -r

        if [[ $? -eq 0 ]]
        then
            notifs-pile-add-echo 'Lib FFMpeg x265 ajoutée.';ffmpeg_lib_param='--enable-gpl --enable-libx265';
            echo 'Rechargement '
        else
            erreurs-pile-add-echo 'Lib FFMpeg x265 non ajoutée.'
        fi

        return 0;
    }

    addLibrairie 'ffmpeg_compil_ffmpeg_lib_libx264' 'Compilation de FFMpeg avec la lib libx264'
    function ffmpeg_compil_ffmpeg_lib_libx264(){

        displayVars-D 'ffmpeg_build' "$ffmpeg_build"

        eval-echo "cd $ffmpeg_sources"
        if [ ! -f ffmpeg-snapshot.tar.bz2 ];then wget -O ffmpeg-snapshot.tar.bz2 https://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2;fi
        if [ ! -d ffmpeg ];then tar xjvf ffmpeg-snapshot.tar.bz2;fi
        eval-echo 'cd ffmpeg'
        PATH="$ffmpeg_bin:$PATH"
        PKG_CONFIG_PATH="$ffmpeg_build/lib/pkgconfig"
        echo './configure'
        ./configure \
            --prefix="$ffmpeg_build" \
            --extra-cflags="-I$ffmpeg_build/include" \
            --extra-ldflags="-L$ffmpeg_build/lib" \
            --extra-libs="-lpthread -lm" \
            --ld="g++" \
            --bindir="$ffmpeg_bin" \
            --enable-gpl \
            --enable-gnutls \
            --enable-libx264 \
            --enable-nonfree
            # ligne posant pb:
            #--pkg-config-flags="--static" \

        PATH="$ffmpeg_bin:$PATH"
        eval-echo 'make'
        eval-echo 'make install'
        eval-echo 'hash -r'
        
        if [[ $? -eq 0 ]]
        then
            notifs-pile-add-echo 'Lib FFMpeg x264 ajoutée.';
            echo 'Rechargement de ~/.profile'
            . ~/.profile
        else
            erreurs-pile-add-echo 'Lib FFMpeg x264 non ajoutée.'
        fi

        return 0;
    }

#