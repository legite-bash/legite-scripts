echo-d "${BASH_SOURCE[0]}" 1;

# clear; gtt.sh --show-vars vipi
# date de création    : 2024.10.28
# date de modification: 2024.10.26

#######################
# vipi-video-pathname #
# function setVideoName "videoName"##################
# objet: vipi-video-* #
##################

# inode: repertoire ou fichier ou lien symbolique
# initialise les variables attachées
    declare -g vipi_video_pathname='';           # chemin complet /rep/er/toire/inodeNom.inodeExt
    declare -g vipi_video_rep='';                # /rep/er/toire sans slash de fin
    declare -g vipi_video_name='';               # inodeNom.inodeExt
    declare -g vipi_video_nom='';                # nom de l'inode sans l'extention
    declare -g vipi_video_ext='';                # extention de l'inode sans le le point
    #declare -g vipi_video_log_pathname='';      # chemin du fichier de log
    declare -g vipi_video_isShadowFile=false;    # le nom de fichier commence  il par un point '.' ?

    function vipi-video-init(){
        vipi_video_pathname='';
        vipi_video_rep='';
        vipi_video_name='';
        vipi_video_nom='';
        vipi_video_ext='';
        vipi_video_isShadowFile=false;
        return 0;
    }


    # vipi-video-display-vars()
    # Affiche sous forme de display-vars les variables d'informations
    function vipi-video-display-vars(){
        display-vars 'vipi_video_pathname   ' "$vipi_video_pathname";
        display-vars 'vipi_video_rep        ' "$vipi_video_rep";
        display-vars 'vipi_video_name       ' "$vipi_video_name";
        display-vars 'vipi_video_nom        ' "$vipi_video_nom";
        display-vars 'vipi_video_ext        ' "$vipi_video_ext";

        display-vars 'formatYT' "$formatYT";
        #display-vars 'youtubeRef' "$youtubeRef";
        display-vars 'videosNb' "$videosNb";

        #display-vars 'videoExtraitNom' "$videoExtraitNom";
        #display-vars 'videoExtraitPath' "$videoExtraitPath";

        display-vars 'fps' "$fps";

        #display-vars 'seanceVideoPath' "$seanceVideoPath";
        #display-vars 'seanceImageRepForVideo' "$seanceImageRepForVideo";

        return 0;
    }


    #vipi-video-pathname( pathname )
    # definit les variables de l'inode
    function vipi-video-pathname(){
        vipi-video-init;
        if [ "${1:-""}" = ""  ];then vipi-video-init;return $E_FALSE;fi
        vipi_video_pathname="$1";
        #vipi_video_pathname="$1";
        vipi_video_rep="${vipi_video_pathname%/*}";       # display-vars 'vipi_video_rep' "$vipi_video_rep";
        vipi_video_name="${vipi_video_pathname##*/}";     # display-vars 'vipi_video_name' "$vipi_video_name";
        vipi_video_nom="${vipi_video_name%.*}";           # display-vars 'vipi_video_nom' "$vipi_video_nom";
        vipi_video_ext="${vipi_video_name##*.}";


        # - filtre:nom - #
        if [ "${vipi_video_name:0:1}" = '.' ]        # est-ce un fichier caché ? (.htaccess[.extention])
        then
            if [ "$vipi_video_nom" = '' ]            # est ce un fichier caché seul (.htaccess)?
            then vipi_video_nom="${vipi_video_name}";     # 
            #else  vipi_video_nom="${vipi_video_nom}";    # non (.fichierCaché.extention). On change pas le nom, il est dajà filtré
            fi
            if [ "$vipi_video_ext" = "${vipi_video_nom:1}" ]  # Si l'extention egal au nom alors il y a pas d'extention
            then
                vipi_video_ext='';
            fi
        fi


        # - filtre:rep - #
        if [ "$vipi_video_rep" = "$vipi_video_nom" ]\
        || [ "$vipi_video_rep" = "${vipi_video_nom}." ]   # Fichier seul qui finit par un point
        then vipi_video_rep='';fi                    # pathname ne contient aucun répertoire

        if [ -z "$vipi_video_rep"  ]\
        && [ "${vipi_video_pathname:0:1}" = '/' ]
        then vipi_video_rep='/'; fi # Le fichier est à la racine


        # - filtre:ext - #
        if [ "$vipi_video_ext" = "$vipi_video_nom" ];then vipi_video_ext='';fi    # L'extention est vide
        return 0;
    }

    function vipi-video-rep(){
            vipi_video_rep="${1:-""}";
            if [ "$vipi_video_rep" != '/' ]                              # si pas root ('/')
            then vipi_video_rep="${vipi_video_rep%/}";                        # on supprime le dernier slash si indiqué
            fi
            if [ "$vipi_video_rep" = '' ]
            then  vipi_video_pathname="${vipi_video_name}"
            else  vipi_video_pathname="${vipi_video_rep}/${vipi_video_name}"
            fi
            return 0;
        }
    #

    function vipi-video-nom(){
        if [ "${1:-""}" = ""  ];then return $E_FALSE;fi # le nom ne peut etre vide
        vipi_video_nom="${1}";
        if [ "$vipi_video_ext" = '' ]
        then  vipi_video_name="${vipi_video_nom}"
        else  vipi_video_name="${vipi_video_nom}.${vipi_video_ext}"
        fi
        if [ "$vipi_video_rep" = '' ]
        then  vipi_video_pathname="${vipi_video_name}"
        else  vipi_video_pathname="${vipi_video_rep}/${vipi_video_name}"
        fi
        return 0;
    }

    function vipi-video-ext(){
        vipi_video_ext="${1:-""}";
        if [ "$vipi_video_ext" = '' ]
        then  vipi_video_name=".${vipi_video_nom}"
        else  vipi_video_name="${vipi_video_nom}.${vipi_video_ext}"
        fi
        if [ "$vipi_video_rep" = '' ]
        then  vipi_video_pathname="${vipi_video_name}"
        else  vipi_video_pathname="${vipi_video_rep}/${vipi_video_name}"
        fi
        return 0;
    }
#


function setVideoExtraitNom(){
    videoExtraitNom="$1";
    setVideoCalc;
    return 0;
}

function setFormatYTt(){
    formatYT=$1;
    setVideoCalc;
    return 0;
}

function setVideoCalc(){
    #echo "${FUNCNAME[0]}($@)";
    local _formatYT_txt='';if [ $formatYT -ne 0 ];then _formatYT_txt="-${formatYT}";fi

    videoNom="${videoNom}${_formatYT_txt}";
    videoName="${videoNom}.${videoExt}";
    videoPath="$SEANCE_VIDEOS_REP/${videoName}";

    seanceVideoPath="${videoPath##$SALLE_VIDEOS_REP/}";  # le chemin de la video a partir de $SALLE_VIDEOS_REP
    videoImagesRep="$SEANCE_IMAGES_REP/$videoName"; 

    videoCfgShPathname="$videoCfgShRep/${videoName}.cfg.sh";
    videoCfgJsPathname="$videoCfgJsRep/${videoName}.cfg.js";
    videoCfgJsAppendPathname="$videoCfgJsRep/${videoName}-append.cfg.js";

    # extrait
    local _videoExtraitNom='';if [ "$videoExtraitNom" != '' ];then _videoExtraitNom="-${videoExtraitNom}";fi
    videoExtraitNom="${videoNom}${_videoExtraitNom}${_formatYT_txt}";
    videoExtraitPath="$SEANCE_VIDEOS_REP/${videoExtraitNom}.$videoExt";
    #videosCfg_nom["$seanceVideoPath"]="$videoNom";
    #videosCfg_fps["$seanceVideoPath"]=$fps;
    #videosCfg_formatYT["$seanceVideoPath"]=$formatYT;
    #videosCfg_refYT["$seanceVideoPath"]="$youtubeRef";
    #display-vars $LINENO:'videoNom' "$videoNom";
    #display-vars $LINENO:'videoName' "$videoName";
    return 0;
}
