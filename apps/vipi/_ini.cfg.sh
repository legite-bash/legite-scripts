echo-d "${BASH_SOURCE[0]}" 1;

# clear; gtt.sh --show-vars vipi
# date de création    : 2023.05.08
# date de modification: 2024.10.26

declare -gr VIPI_ROOT="${BASH_SOURCE%/*}";

#############
## INCLUDE ##
#############
require-once "$VIPI_ROOT/vipi-vars.sh";
require-once "$VIPI_ROOT/vipi-video-pathname.sh";
require-once "$VIPI_ROOT/vipi-core.sh";
require-once "$VIPI_ROOT/vipi-extract.sh";
require-once "$VIPI_ROOT/vipi-createJS.sh";


###########
## USAGE ##
###########
    function vipi-usage(){
        local _usage="usage: gtt.sh vipi  [--extract] - salle='defaut' seance='$SEANCE_NOM' video='$VIDEO_NAME'";
        titreUsage "$_usage";
        echo --extract : extract les images
        echo "exemple: gtt.sh vipi --extract - salle='defaut' seance='demo' video='all' Extract les images de toutes les vidéos du repertoire 'defaut/demo'";
        #erreurs-pile-add "$_usage";
        return 0;
    }
#

##########
## MAIN ##
##########
    function vipi-main(){
        fctIn;
        titre1 'Variables globales';
        displayVar 'SALLE_NOM' "$SALLE_NOM" 'SEANCE_NOM' "$SEANCE_NOM" 'VIDEO_NAME' "$VIDEO_NAME";


        titre1 'showVarsLocal';
        #show-vars-local;

        titre1 "Connexion au repertoire à distance";
        # relatif à                                         $HOME/legralNet/legral-serveur/
        if ! connectSSHFS '/mnt/WD4ToBlue/spirit/'          'vipi-datas'      'rw'
        then
            erreurs-pile-add-echo "Connexion à '/mnt/WD4ToBlue/spirit/' -> 'vipi-datas' impossible";
            fctOut "${FUNCNAME[0]}($E_FALSE)"; return $E_FALSE;
        fi
        if ! connectSSHFS '/www/www-sites/spirits/'  'legral-serveur-site-spirits/'   'rw'
        then
            erreurs-pile-add-echo "Connexion à '/www/www-sites/spirits/' -> 'legral-serveur-site-spirits' impossible";
            fctOut "${FUNCNAME[0]}($E_FALSE)"; return $E_FALSE;
        fi

        # =========== #
        # == SALLE == #
        # =========== #
        titre1 "Vérification de la salle ($SALLE_NOM)";
            if [ "$SALLE_NOM" = '' ]
            then
                vipi-usage;
                titre2 "Les salles exitantes ($SALLE_VIDEOS_REP):";
                ls -A "$SALLE_VIDEOS_REP/";
                fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE; # E_USAGE
            fi
        #

        titre2 "Création de SALLE_VIDEOS_REP";
            if [ ! -d "$SEANCE_VIDEOS_REP" ];then mkdir -p "$SALLE_VIDEOS_REP";fi
            if ! cd "$SALLE_VIDEOS_REP" 
            then
                erreurs-pile-add-echo "Impossible d'entrée dans le repertoire: '$SALLE_VIDEOS_REP'";
                fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;

            fi
            displayVar 'repertoire' "$PWD";
        #

        # == SALLE_CFG_JS_REP == #
            #titre1 "Préparation de  cfg_js de la salle";

            titre2 "Création de SALLE_CFG_JS_REP";
                if [ ! -d "$SALLE_CFG_JS_REP" ];then eval-echo "mkdir -p \"$SALLE_CFG_JS_REP\"";fi
                if !   cd "$SALLE_CFG_JS_REP"
                then
                    erreurs-pile-add-echo "Impossible d'entrée dans le repertoire: '$SALLE_CFG_JS_REP'";
                    fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
                fi
            displayVar 'repertoire' "$PWD";

            SELECT_salle_seanceTbl_js_create;
        #


        # ============ #
        # == SEANCE == #
        # ============ #

        titre1 "Vérification de la seance ($SEANCE_NOM)";
            if [ "$SEANCE_NOM" = '' ]
            then
                vipi-usage;
                titre2 "Les seances exitantes ($SEANCE_VIDEOS_REP):";
                ls -A "$SEANCE_VIDEOS_REP/";
                fctOut "${FUNCNAME[0]}($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE; # E_USAGE
            fi
        #

        titre2 "Création de SEANCE_VIDEOS_REP";
            if [ ! -d "$SEANCE_VIDEOS_REP" ];then mkdir -p "$SEANCE_VIDEOS_REP";fi
            if ! cd "$SEANCE_VIDEOS_REP" 
            then
                erreurs-pile-add-echo "Impossible d'entrée dans le repertoire: '$SEANCE_VIDEOS_REP'";
                fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;

            fi
            displayVar 'repertoire' "$PWD";
        #

        # == SEANCE_CFG_SH_REP == #
            #titre1 "Préparation de  cfg_sh de la seance";

            titre2 "Création de SEANCE_CFG_SH_REP";
                if [ ! -d "$SEANCE_CFG_SH_REP" ];then eval-echo "mkdir -p \"$SEANCE_CFG_SH_REP\"";fi
                if !   cd "$SEANCE_CFG_SH_REP"
                then
                    erreurs-pile-add-echo "Impossible d'entrée dans le repertoire: '$SEANCE_CFG_SH_REP'";
                    fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
                fi
            displayVar 'repertoire' "$PWD";
            #

            titre2 'Création et chargement de SEANCE_CFG_SH_PATHNAME';
                if [ ! -f "$SEANCE_CFG_SH_PATHNAME" ]
                then
                    touch "$SEANCE_CFG_SH_PATHNAME";
                fi
                exec-file  "$SEANCE_CFG_SH_PATHNAME";
                displayVar 'fichier' "$SEANCE_CFG_SH_PATHNAME";
            #
        #

        # == SEANCE_CFG_JS_REP == #
            #titre1 "Préparation de  cfg_js de la seance";

            titre2 "Création de SEANCE_CFG_JS_REP";
                if [ ! -d "$SEANCE_CFG_JS_REP" ];then eval-echo "mkdir -p \"$SEANCE_CFG_JS_REP\"";fi
                if !   cd "$SEANCE_CFG_JS_REP"
                then
                    erreurs-pile-add-echo "Impossible d'entrée dans le repertoire: '$SEANCE_CFG_JS_REP'";
                    fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
                fi
            displayVar 'repertoire' "$PWD";
            #

            titre2 'Création de SEANCE_CFG_JS_PATHNAME';
                if [ ! -f "$SEANCE_CFG_JS_PATHNAME" ]
                then
                    touch "$SEANCE_CFG_JS_PATHNAME";
                fi
                display-vars 'fichier' "$SEANCE_CFG_JS_PATHNAME";
            #
            SELECT_seance_videos_create;
        #

        # ============ #
        # == AUTO == #
        # ============ #
        auto_create;

        fctOut "${FUNCNAME[0]}(0)"; return 0;
    }
#


###############
## videosCfg ##
###############
    #function videosCfg_init(){
    #    videosCfg_titre=();
    #    videosCfg_fps=();
    #    videosCfg_formatYT=();
    #    videosCfg_refYT=();
    #}


############
## DIVERS ##
############
    ##
    # Calcul le nombre de fihcier PNG dans le repertoire donné en parametre
    # et remplit la variable $PNGinDirNb
    local -i PNGinDirNb=0;
    function PNGinDir(){
        PNGinDirNb=0;
        titre3 "Comptage du nombre d'images";
        if [ ! -d "$videoImagesRep" ]
        then
            erreurs-pile-add-echo "Le repertoire des images '${videoImagesRep#"$SEANCE_IMAGES_REP"/}' n'existe pas."
            return $E_FALSE;
        fi
        for _v in $(ls -A -1 "$videoImagesRep")
        do
            if [ "${_v##*.}" != "png" ];then continue;fi
            (( PNGinDirNb++ ))
        done; 
        #displayVar 'PNGinDirNb' "$PNGinDirNb";
        return 0;
    }


    function vipi-video_rename(){
        local _description="$1";
        local _nu='';
        local _src="video${zoomVideoNo}${zoomSeanceNo}.mp4";
        cd "$SEANCE_VIDEOS_REP/";
        if [ -f "$_src" ]
        then
            if [ $zoomVideoNo -lt 10 ];then _nu="0$zoomVideoNo"; else _nu="$zoomVideoNo";fi
            echo mv "$_src" "${DATE_SEANCE}-${_nu}-${_description}.mp4";
                mv "$_src" "${DATE_SEANCE}-${_nu}-${_description}.mp4";
        fi
        cd "$OLDPWD";
        return 0;
    }
#


##########################
# - CONSTRUCTION AUTO  - #
##########################
    function auto_create(){
        fctIn "$@";
        titre1 "${FUNCNAME[0]}($@)";
        displayVar 'VIDEO_NAME' "$VIDEO_NAME";
        displayVar 'videoName' "$videoName";
        if [ "$VIDEO_NAME" =  '' ]
        then
            _isExitAsk=true;
            titre2 "Les videos exitantes de la seance ($SEANCE_NOM):";
            eval-echo "ls -A \"$SEANCE_VIDEOS_REP\""; 
            echo 'Pas de nom de video fournis -> pas de scan';
            fctOut "${FUNCNAME[0]}($E_FALSE)"; return $E_FALSE;
        fi

        case "$VIDEO_NAME" in

            'all') vipi-scanVideosInSceance;      ;;
            *)
                setVideoNom "$VIDEO_NAME";
                #seanceCfgJs_create;
                scanVideo "$videoNom";  ;;
        esac
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


#################
## SCAN VIDEOS ##
#################
    function vipi-scanVideosInSceance(){
        fctIn "$@";
        titre2 "${FUNCNAME[0]}():Scan du repertoire de la séance pour analyser les vidéos";

        for _videoName in $(ls -A -1 "$SEANCE_VIDEOS_REP/")
        do
            if  $isTrapCAsk; then continue;fi
            setVideoName "$_videoName";        # mise a jour de la globale
            
            #SELECT_seance_js_create "$videoName";
            #seanceCfgJs_create
            scanVideo "$videoName";
        done
        fctOut "${FUNCNAME[0]}(0)";return 0;
    }


    #scanVideo videoName=nom_de_la_video.ext
    # $1=videoName
    function scanVideo(){
        if  $isTrapCAsk; then return $E_CTRL_C;fi
        fctIn "$@";
        titre2 "${FUNCNAME[0]}($@)";
        if [ $# -ne 1 ]
        then
            erreurs-pile-add-echo "${FUNCNAME($@)} Nom de la video requis";
            fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)";return $E_INODE_NOT_EXIST;
        fi

           if ! setVideoName "$1"; then return $E_FALSE;fi
        titreInfo "$videoName";

        if [ "$videoExt" != 'mp4' ]\
        && [ "$videoExt" != 'ogv' ]\
        && [ "$videoExt" != 'mkv' ]\
        && [ "$videoExt" != 'webm' ];
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}($@): l'extention '$videoExt' n'est pas supportée."
            return $E_FALSE;
        fi
        #displayVarsVideo;

        if [ ! -f "$videoPath" ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}: La video '$videoPath' est introuvable. ";
            ls -A "$SEANCE_VIDEOS_REP";
            fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)";return $E_INODE_NOT_EXIST;
        fi

        titre2 "Création du repertoire des images (videoImagesRep)";
            videoImagesRep="$SEANCE_IMAGES_REP/$videoName";
            if [ ! -d "$videoImagesRep" ]
            then
                if eval-echo "mkdir -p '$videoImagesRep'"
                then
                    notifsAddEcho "videoImagesRep:Création";
                else
                    erreurs-pile-add-echo "Impossible de créerle repertoire: '$videoImagesRep'";
                    fctOut "${FUNCNAME[0]}($E_INODE_NOT_EXIST)";  return $E_INODE_NOT_EXIST;
                fi
            fi
        #

        #titre2 'Comptage du nombre de fichier png:';
        PNGinDir;
        images_extract;
        videoCfgSh_createAndExec;
        displayVar 'fps' "$fps";
        videoCfgJs_create;
        videoCfgJsAppendPathname_create;

        fctOut "${FUNCNAME[0]}(0)";return 0;
    }
#


function videoCfgSh_createAndExec(){
    fctIn;
    titre2 "${FUNCNAME[0]}()";
    if [ ! -f "$videoCfgShPathname" ]
    then
        touch "$videoCfgShPathname";
        notifsAddEcho "'$videoCfgShPathname' Crée.";
    fi
    execFile "$videoCfgShPathname";
    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

# ########## #
# PARAMETRES #
# ########## #
    #displayTableau  'ARG_TBL' "${ARG_TBL[@]}";
    local -i _argNu=-1;
    while true
    do
        if $isTrapCAsk;then break;fi
        ((_argNu++))
        if [ $_argNu -ge $ARG_NB ];then break;fi

        #displayVar '${ARG_TBL[$_argNu]}' "${ARG_TBL[$_argNu]}";
        case "${ARG_TBL[$_argNu]}" in
            -)  # indicateur des parametres pour les librairies/collections
                break;  # Les PSPParams sont déjà traités
                ;;
            
            --extract)     isExtract=true;    ;;
   
            #*)
                #
        esac

    done
    unset librairie param;
#

# ######### #
# vipi-main #
# ######### #
#vipi-main;
