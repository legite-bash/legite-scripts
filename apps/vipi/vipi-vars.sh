echo-d "${BASH_SOURCE[0]}" 1;

# clear; gtt.sh --show-vars vipi
# date de création    : 2023.05.08
# date de modification: 2024.10.26


###########
# DECLARE #
###########
    #declare _isExitAsk=false;
    declare -g  vipi_is_Run=false;         # Indique su vipi doit ou est executé

    declare -gr SALLE_NOM="${PSP_params['salle']:-"defaut"}";
    declare -gr SEANCE_NOM="${PSP_params['seance']:-""}";

    #declare -gr SITE_ROOT="/home/pascal/legralNet/legral-serveur/legral-serveur-site-spiritsW/IFRES/TCI";
    declare -gr SITE_ROOT="/home/pascal/legralNet/legral-serveur/legral-serveur-wwwW/www-sites/spirits"
    declare -gr WEB_VARS_ROOT="$SITE_ROOT/web/vars";                #


    # == SALLES == #
    declare -gr SALLES_VIDEOS_ROOT="$SITE_ROOT/seances-videos";
    declare -gr SALLES_IMAGES_ROOT="$SITE_ROOT/seances-images";
    declare -gr SALLES_CFG_SH_ROOT="$VIPI_ROOT/seances-cfg-sh";# .cfg.sh
    declare -gr SALLES_CFG_JS_ROOT="$WEB_VARS_ROOT";                   # .cfg.js

    # == SALLE == #
    declare -gr SALLE_VIDEOS_REP="$SALLES_VIDEOS_ROOT/$SALLE_NOM";
    declare -gr SALLE_IMAGES_REP="$SALLES_IMAGES_ROOT/$SALLE_NOM";
    declare -gr SALLE_CFG_SH_REP="$SALLES_CFG_SH_ROOT/$SALLE_NOM";
    declare -gr SALLE_CFG_JS_REP="$SALLES_CFG_JS_ROOT/$SALLE_NOM";
    declare -gr SALLE_CFG_JS_SELECT_PATHNAME="$SALLE_CFG_JS_REP/0salle-select.cfg.js";

    # == SEANCE == #
    declare -gr SEANCE_DATE="${SEANCE_NOM:0:10}";
    declare -gr SEANCE_VIDEOS_REP="$SALLE_VIDEOS_REP/$SEANCE_NOM";
    declare -gr SEANCE_IMAGES_REP="$SALLE_IMAGES_REP/$SEANCE_NOM"; 

    declare -gr SEANCE_CFG_SH_REP="$SALLE_CFG_SH_REP/$SEANCE_NOM";
    declare -gr SEANCE_CFG_SH_PATHNAME="$SEANCE_CFG_SH_REP/${SEANCE_NOM}.cfg.sh";

    declare -gr SEANCE_CFG_JS_REP="$SALLE_CFG_JS_REP/$SEANCE_NOM";
    declare -gr SEANCE_CFG_JS_PATHNAME="$SEANCE_CFG_JS_REP/0seance.cfg.js";
    declare -gr SEANCE_CFG_JS_SELECT_PATHNAME="$SEANCE_CFG_JS_REP/0seance-select.cfg.js";

    # == VIDEO == #
    #declare    videoDir='';
    #declare    vipi-video-pathname '';
    #declare    seancevipi-video-pathname '';                                    # videoPath a partir du $SEANCE_VIDEOS_REP
    #declare    vipi-video-name '';  # nom + ext
    #declare    vipi-video-nom '';   # nom
    #declare    videoExt='mp4';
    declare -gi videosNb=0;    # depend du tableau videosCfg_titre[]

    declare -gr VIDEO_NAME="${PSP_params['video']:-""}";
    declare    videoCfgShRep="$SEANCE_CFG_SH_REP";                    # le rep est le rep de la seance et non :"SEANCE_CFG_JS_REP/$videoName;
    declare    videoCfgShPathname="$videoCfgShRep/$videoName.cfg.sh"; # dynamique depend de videoName

    declare    videoCfgJsRep="$SEANCE_CFG_JS_REP";                    # le rep est le rep de la seance et non :"SEANCE_CFG_JS_REP/$videoName;

    declare    videoCfgJsPathname="$videoCfgJsRep/$videoName.cfg.js"; # vipi-video-calc()                  -> fichier générer automatiquement
    declare    videoCfgJsAppendPathname="$videoCfgJsRep/${videoName}-append.cfg.js";   # vipi-video-calc()   -> fichier vide personalisé

    declare    videoVideosRep="$SEANCE_VIDEOS_REP/$videoName";        # dynamique
    declare    videoImagesRep="$SEANCE_IMAGES_REP/$videoName";        # dynamique

    # - video source - #
    declare    videoNomSource='';

    # - video courante - #

    # - video extrait - #
    declare    videoExtraitNom='';   # nom de l'extrait
    declare    videoExtraitPath='';  # chemin de l'extrait
    declare -gi partNb=0;
    declare -ga partNom=();

    # les arguments
    #declare isExtract=false;     # demande la generation des images

    # == select == #
    declare SELECT_seances_html='';
    declare SELECT_seance_videos_html='';

    # == ffmpeg == #
    # 
    declare    ffmpeg_ss='';
    declare    ffmpeg_to='';

    # == pour le diaporama js == #
    declare -gi zoomSeanceNo=0;
    declare -gi zoomVideoNo=0;
    declare -gri FPS_DEFAUT=25;                # frame per seconds (video zoom)
    declare -gi fps=25;                        # frame per seconds (video zoom) # a rendre obsolete
    declare -gri FORMAT_YT_DEFAUT=0;           # desactivé
    declare -gi formatYT=$FORMAT_YT_DEFAUT;
    declare    youtubeRef='';
    #declare -gA videosCfg_titre=(['A']='a');  # tableau
    declare -gA videosCfg_titre=();            # tableau
    #displayVar '${#videosCfg_titre}' "${#videosCfg_titre[@]}";
    #displayTableauA 'videosCfg_titre' "${videosCfg_titre[@]:-"vide"}";

    declare -gA videosCfg_fps=();              # tableau
    declare -gA videosCfg_formatYT=();         # tableau
    declare -gA videosCfg_refYT=();            # tableau
#

###########
# DISPLAY #
###########
    function display-vars-local(){
        display-vars 'VIPI_ROOT               ' "$VIPI_ROOT";
        #display-vars '_isExitAsk              ' "$_isExitAsk";
        display-vars 'SALLES_CFG_SH_ROOT      ' "$SALLES_CFG_SH_ROOT";
        display-vars 'SITE_ROOT               ' "$SITE_ROOT";
        display-vars 'WEB_VARS_ROOT           ' "$WEB_VARS_ROOT";

        echo 'SALLES';
        display-vars 'SALLES_VIDEOS_ROOT      ' "$SALLES_VIDEOS_ROOT";
        display-vars 'SALLES_IMAGES_ROOT      ' "$SALLES_IMAGES_ROOT";
        display-vars 'SALLES_CFG_SH_ROOT      ' "$SALLES_CFG_SH_ROOT";
        display-vars 'SALLES_CFG_JS_ROOT      ' "$SALLES_CFG_JS_ROOT";

        echo 'SALLE';
        display-vars 'SALLE_NOM' "$SALLE_NOM";
        display-vars 'SALLE_VIDEOS_REP        ' "$SALLE_VIDEOS_REP";
        display-vars 'SALLE_IMAGES_REP        ' "$SALLE_IMAGES_REP";
        display-vars 'SALLE_CFG_SH_REP        ' "$SALLE_CFG_SH_REP";
        display-vars 'SALLE_CFG_JS_REP        ' "$SALLE_CFG_JS_REP";
        display-vars 'SALLE_CFG_JS_SELECT_PATHNAME'  "$SALLE_CFG_JS_SELECT_PATHNAME";

        echo 'SEANCE';
        display-vars 'SEANCE_NOM              ' "$SEANCE_NOM";
        display-vars 'SEANCE_DATE             ' "$SEANCE_DATE";
        display-vars 'SEANCE_VIDEOS_REP       ' "$SEANCE_VIDEOS_REP";
        display-vars 'SEANCE_IMAGES_REP       ' "$SEANCE_IMAGES_REP";
        #display-vars 'seanceImageRepForVideo' "$seanceImageRepForVideo";
        display-vars 'SEANCE_CFG_SH_REP       ' "$SEANCE_CFG_SH_REP";
        display-vars 'SEANCE_CFG_SH_PATHNAME  ' "$SEANCE_CFG_SH_PATHNAME";
        display-vars 'SEANCE_CFG_JS_SELECT_PATHNAME'  "$SEANCE_CFG_JS_SELECT_PATHNAME";

        echo 'VIDEO';
        display-vars 'VIDEO_NAME'             "$VIDEO_NAME";
    }


#

############
## SETTER ##
############

    function setVideoExtraitNom(){
        videoExtraitNom="$1";
        vipi-video-calc;
        return 0;
    }



    function vipi-video-calc(){
        #echo "${FUNCNAME[0]}($@)";
        local _formatYT_txt='';if [ $formatYT -ne 0 ];then _formatYT_txt="-${formatYT}";fi

        vipi-video-nom "${videoNom}${_formatYT_txt}";
        vipi-video-name "${videoNom}.${videoExt}";
        vipi-video-pathname "$SEANCE_VIDEOS_REP/${videoName}";

        seancevipi-video-pathname "${videoPath##$SALLE_VIDEOS_REP/}";  # le chemin de la video a partir de $SALLE_VIDEOS_REP
        videoImagesRep="$SEANCE_IMAGES_REP/$videoName"; 

        videoCfgShPathname="$videoCfgShRep/${videoName}.cfg.sh";
        videoCfgJsPathname="$videoCfgJsRep/${videoName}.cfg.js";
        videoCfgJsAppendPathname="$videoCfgJsRep/${videoName}-append.cfg.js";

        # extrait
        local _videoExtraitNom='';if [ "$videoExtraitNom" != '' ];then _videoExtraitNom="-${videoExtraitNom}";fi
        videoExtraitNom="${videoNom}${_videoExtraitNom}${_formatYT_txt}";
        videoExtraitPath="$SEANCE_VIDEOS_REP/${videoExtraitNom}.$videoExt";
        #videosCfg_nom["$seanceVideoPath"]="$videoNom";
        #videosCfg_fps["$seanceVideoPath"]=$fps;
        #videosCfg_formatYT["$seanceVideoPath"]=$formatYT;
        #videosCfg_refYT["$seanceVideoPath"]="$youtubeRef";
        #displayVar $LINENO:'videoNom' "$videoNom";
        #displayVar $LINENO:'videoName' "$videoName";
        return 0;
    }
#


return 0;