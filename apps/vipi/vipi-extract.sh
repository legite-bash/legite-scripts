echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh --show-vars vipi
# date de création    : 2023.05.08
# date de modification: 2024.10.26

declare -g isExtract=false;     # demande la generation des iamges

function partie_begin(){
    if [ $# -ne 1 ]
    then
        erreurs-pile-add-echo "$FUNCNAME($@) neccessite le nom de la partie comme argument.";
        return $E_ARG_BAD;
    fi
    ((partNb++))
    partNom[$partNb]="$1";
    setVideoNom "$videoNomSource";
    setVideoExtraitNom "$1";
}

#function partie_end(){
#    
#}

#####################################
# TELECHARGEMENT D'UNE VIDEO RACINE #
# variables utilisées:
# SEANCE_VIDEOS_REP
# formatYT
# fps
#####################################
function telechargeYT(){
    # Tester validité de $videoNom et $youtubeRef

    #formatYT="${formatYT:-$FORMAT_YT_DEFAUT}";
    #videoPathSansExt="$SEANCE_VIDEOS_REP/${videoTitre}-$formatYT";
    #videoPath="${videoPathSansExt}.mp4";

    #video-display-vars;

    titre2 "$FUNCNAME():Téléchargement de '$youtubeRef': $videoNom - $formatYT";
    local _youtubeURL="https://www.youtube.com/watch?v=$youtubeRef";

    display-vars 'videoPath' "$videoPath";
    if [  -f "${videoPath}" ]
    then
        echo "Le fichier '${videoPath}' existe déjà";
    else
        #eval-echo "youtube-dl --format $formatYT --output=\"${videoPath}\" $_youtubeURL";
        if [ $? -ne 0 ]
        then
            erreurs-pile-add-echo "Erreur avec youtube-dl";
            return $E_INODE_NOT_EXIST;
        fi
    fi
}


########################
# EXTRATION DES VIDEOS #
# variables utilisées:
# FORMAT_YT_DEFAUT (facultatif)
# fps
# extrait_titre
########################


#function video_extractPart '00.00.00,000' ['00.00.00,000']
## variables utilisées
# videoPath
# extrait_titre
# formatYT
function video_extractPart(){
    local -i _errNu=0;

    local _ffmpeg_ss="$1";
    local _ffmpeg_to="${2:-""}";
    if [ "$_ffmpeg_to" != '' ];then _ffmpeg_to="-to ${_ffmpeg_to}";fi

    titre1 "Extraction de la partie: '$videoExtraitNom'";
    #video-display-vars;

    if [ -f "${videoExtraitPath}" ]
    then
        echo "déjà faite";
    else
        eval-echo "ffmpeg -n  -i \"$videoPath\" -ss ${_ffmpeg_ss} ${_ffmpeg_to} -c:v copy -c:a copy \"${videoExtraitPath}\"";
        if [ $_errNu -eq 0 ]
        then notifsAddEcho "L'extraction de la partie '$videoExtraitNom' a été faite san erreur";
        else erreurs-pile-add-echo "Erreur ($_errNu) lors de l'extraction de la partie '$videoExtraitNom'";
        fi
    fi
    # le nom de l'extrait devient le nouveau nom de la video a traiter
    titreInfo "Changement de nom avec l'extrait"
    setVideoNom "$videoExtraitNom";
    #video-display-vars;
    return $_errNu;
}


####################
## EXTRACT IMAGES ##
####################
    # function images_extract : pas d'argument
    ## variables utilisées:
    # isExtract
    # SEANCE_NOM
    # videoName
    # seanceVideoPath
    # seanceImageRepForVideo
    function images_extract(){
        if  $isTrapCAsk; then return $E_CTRL_C;fi
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;
        local -i _errNu=0;
        titre2 "${FUNCNAME}():Extraction des images";
        #video-display-vars;

        if [ $isExtract = false ]
        then
            echo '--extract est pas indiqué.';
            if [ $PNGinDirNb -eq 0 ]
            then
                titreInfo "$videoName:Le repertoire images ne contient aucune image. Utiliser --extract";
            else
                titreInfo "$PNGinDirNb images sont déjà présentes."
            fi
            return 0;
        fi


        _ffmpeg_params_global=' -n ';
            # -n: ne pas écraser
        _ffmpeg_params='';
        # https://dev.exiv2.org/projects/exiv2/wiki/The_Metadata_in_PNG_files
        _ffmpeg_metadata="-metadata title=\"${videoName}\" -metadata year=\"2023\" -metadata Author=\"IFRES\" -metadata Description=\"Video source:'$videoName'\" ";

        #ffmpeg -i film.mp4 -ss 00:00:10.00 -to 00:00:46.00 -c:v copy -c:a copy  extraitdufilm.avi
        eval-echo  "ffmpeg $_ffmpeg_params_global -i '$videoPath' $_ffmpeg_metadata $_ffmpeg_params -vsync 0 -f image2 '$videoImagesRep/image%06d.png'";
        _errNu=$?;
        #eval-echo "ffmpeg $_ffmpeg_params_global -i '$seanceVideoPath'                     '$seanceImageRepForVideo/image%06d.jpg'";
        #eval-echo "ffmpeg $_ffmpeg_params_global -i '$seanceVideoPath' -qscale:v 2         '$seanceImageRepForVideo/image%06d-qs2.jpg'";

        if [ $_errNu -eq 0 ]
        then notifsAddEcho  "Extraction des images de '$videoName'";
        else erreurs-pile-add-echo "Erreur kirs de l'extraction des images de '$videoName'";
        fi
        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE; "${FUNCNAME}(0)";return 0;
    }
#
