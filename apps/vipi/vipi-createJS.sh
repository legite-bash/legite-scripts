echo-d "${BASH_SOURCE[0]}" 1;

# clear; gtt.sh --show-vars vipi
# date de création    : 2023.05.08
# date de modification: 2024.10.26


############
## SELECT ##
############
    # Remplit le fichier SALLE_CFG_JS_SELECT_PATHNAME pour le select SELECT_seances.
    function SELECT_salle_seanceTbl_js_create(){
        titre2 "$FUNCNAME()";
        local _seanceIndex=0;
        local SELECT_seances_js='/*var salle_seanceTbl=Array();*/';

        echo -e "// Fichier généré automatiquement\n" >  "$SALLE_CFG_JS_SELECT_PATHNAME";   #CREATION
        for _seanceNom in $(ls -A -1 "$SALLE_VIDEOS_REP/")
        do
            #_seance_nom_select[$_seanceIndex]="$_seanceNom";
            SELECT_seances_js+="salle_seanceTbl[${_seanceIndex}]=\"${_seanceNom}\";";
            ((_seanceIndex++))
        done

        echo "${SELECT_seances_js}" >> "$SALLE_CFG_JS_SELECT_PATHNAME";
        echo "mise à jour de '$SALLE_CFG_JS_SELECT_PATHNAME'."
    }


    # crée le select 'SELECT_seance_videos'
    ## variables_utilisées
    # SEANCE_VIDEOS_REP
    function SELECT_seance_videos_create(){
        titre2 "$FUNCNAME()";

        local -i _videoIndex=0;

        echo -e "// Fichier généré automatiquement\n" >  "$SEANCE_CFG_JS_SELECT_PATHNAME";   #CREATION

        for _videoName in $(ls -A -1 "$SEANCE_VIDEOS_REP/")
        do
            local _selected='';
            local _videoNom="${_videoName%.*}";
            local _videoExt="${_videoName##*.}";

            echo "seance_videosTbl[${_videoIndex}]=\"${_videoName}\";" >>  "$SEANCE_CFG_JS_SELECT_PATHNAME";
            ((_videoIndex++))
        done
        echo "mise à jour de '$SEANCE_CFG_JS_SELECT_PATHNAME'."
        return 0;
    }
#


###################
## SEANCE_CFG_JS ##
###################
    ## variables utilisées:
    # videoNom
    # videosCfg_fps["$videoNom"]
    # videosCfg_formatYT["$videoNom"]

    #function IFRES-creer_index-diaporaTimer(){
    #function seanceCfgJs_create(){
        # fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        #titre1 "$FUNCNAME():";

        #SELECT_seance_js_create;

        #fctOut "${FUNCNAME}(0)";return 0;
    #}
#


##################
## VIDEO_CFG_JS ##
##################
## variables utilisées:
# SEANCE_NOM
# videoName
#function videoCfgJs_create(){
# creation du fichier de cfg js d'une video
function videoCfgJs_create(){
    local -i _fct_pile_gtt_level=2;
    fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

    titre2 "$FUNCNAME($@)";


    if  [ $isExtract == true ]
    then
        PNGinDir;       # il y a eu extraction d'image -> on recalcul le nombre d'image
    fi

    if [ $PNGinDirNb -eq 0 ]
    then
        erreurs-pile-add-echo "Le repertoire images ne contient aucune images. Utiliser --extract";
    fi

    titre3 'Création (écrase) du fichier de cfgJS de la vidéo:';
        display-vars 'videoCfgJsPathname' "$videoCfgJsPathname";
        #if [  -f "$videoCfgJsPathname" ]
        #then
        #    notifsAddEcho "$videoName:Un fichier de configuration de la vidéo existe déjà";
        #else
            notifsAddEcho "$videoName: Fichier de configuration de la vidéo créé";
            touch "$videoCfgJsPathname";
            echo "//Fichier généré automatiquement"  > "$videoCfgJsPathname";
            echo "//$videoCfgJsPathname"        >> "$videoCfgJsPathname";
            echo "//SEANCE_NOM=${SEANCE_NOM};"  >> "$videoCfgJsPathname";
            echo "//videoName=$videoName;"      >> "$videoCfgJsPathname";
            echo "fps=$fps;"                    >> "$videoCfgJsPathname";
            echo "imagesNb=$PNGinDirNb;"        >> "$videoCfgJsPathname";
        #fi
    #
    fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
}



##################
## videoCfgJsAppendPathname ##
##################
## variables utilisées:
# SEANCE_NOM
# videoName
#function videoCfgJs_create(){
# creation du fichier de cfg js d'une video
function videoCfgJsAppendPathname_create(){
    local -i _fct_pile_gtt_level=2;
    fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

    titre2 "$FUNCNAME($@)";


    titre3 'Création d un fichier vide personalisé à la vidéo:';
        display-vars 'videoCfgJsAppendPathname' "$videoCfgJsAppendPathname";
        if [  -f "$videoCfgJsAppendPathname" ]
        then
            notifsAddEcho "$videoName: Fichier de configuration personalisé de la vidéo existe déjà";
        else
            notifsAddEcho "$videoName: Fichier de configuration personalisé de la vidéo créé";
            touch "$videoCfgJsAppendPathname";
            echo "// Mettez ici vos ajours personnelle pour la vidéo"  > "$videoCfgJsAppendPathname";
        fi
    #
    fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
}

return 0;