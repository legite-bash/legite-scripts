#!/bin/bash
#style
#https://abs.traduc.org/abs-5.0-fr/ch32.html#unofficialst

#http://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html#The-Set-Builtin
#http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -u

return;     # script a réécrire pour l'intégrer à gtt

#set -eE  # same as: `set -o errexit -o errtrace`

#declare -r SCRIPT_PATH=`realpath $0`;        # chemin  absolu complet du script rep + nom
#declare -r SCRIPT_REP=`dirname $SCRIPT_PATH`;# repertoire absolu du script (pas de slash de fin)
#declare -r VERSION="v0.0.1-2021.11.14";
declare    CONF_REP="$HOME/.legite/add_ln/";

#if [ ! -f "$SCRIPT_REP/legite-lib.sh" ]
#then
#    echo "$SCRIPT_REP/legite-lib.sh introuvable -> quit"
#    exit 1
#fi
#. $SCRIPT_REP/legite-lib.sh

######################
#   VARIABLES INIT   #
# (avant legite-lib) #
######################
# --- variables des includes --- #
declare -r UPDATE_HTTP="$UPDATE_HTTP_ROOT/add_ln";


#############
# VARIABLES #
# DU PROJET #
#############

    ## COMMUNS ##
        declare    collectionRoot='';
        #declare collectionRep="$CONF_REP";
        declare -a collectionsTbl;       # (array) liste des collections appellées
        declare -i collectionsTblIndex=0 # 
        #declare    collection='';        # collection en cours   # obsolete
        declare -i isListeCollections=0; # 
        declare -i isCollectionNeeded=0;

        declare    repSrc='';
        declare    repDest='';
#

# #################### #
# FONCTIONS GENERIQUES #
# #################### #
    usage(){
        if [ $argNb -eq 0 ]
        then
            echo "$SCRIPT_FILENAME [-d] [--showVars] [--showLibs] [--update] [--conf=fichier.sh] collection";
            echo "--update: update le programme legite-install.sh lui meme (via $UPDATE_HTTP)."
            echo "--conf: execute un fichier de configuration si existe."
            echo "Fichier de configurations:      (visible avec --showVars)."
            echo "programme: execute une fonction (visible avec --showLibs)."
            echo "      [-S] [--listeFormats] [--no-download] [--convert] [--saveTags] collection";
            echo "--no-download: Ne pas telecharger"
            exit $E_ARG_NONE;
        fi
    }


    showVarsLocal(){
        showDebug "$FUNCNAME($*)"
        displayVar "librairiesTbl[*]"   "${librairiesTbl[*]}"
        displayVar "isListeCollections" "$isListeCollections"
        displayVar "collectionRoot"     "$collectionRoot"
        displayVar "isListeCollections" "$isListeCollections"
        displayVar "isCollectionNeeded" "$isCollectionNeeded"
    }

    showVarsPost(){
        showDebug "$FUNCNAME($*)"
        if [ $isShowVars -eq 1 ]
        then
            showVarsLocal
        fi
        local -r -i SCRIPT_FIN=$(date +%s);
        local -r -i SCRIPT_DUREE=$(($SCRIPT_FIN-$SCRIPT_DEBUT));
        if [ $isShowDuree -eq 1 ];then echo "$SCRIPT_DUREE secondes";fi
    }
#


################
# MISE A JOURS #
################
    # - telecharge et installe la derniere version en remplacant le script qui a lancer l'update - #
    selfUpdate (){
        if [ $isUpdate -eq 1 ]
        then
            showDebug "\n$FUNCNAME($*)"
            echo "mise a jours du programme";

            cd "$SCRIPT_REP";
            updateProg "$UPDATE_HTTP/$SCRIPT_FILENAME" "./$SCRIPT_FILENAME"
            cd -        

            if [ ! -d "$CONF_USR_ROOT" ];then mkdir "$CONF_USR_ROOT";fi
            updateProg "$UPDATE_HTTP/$SCRIPT_FILENAME"   "/usr/local/bin/$SCRIPT_FILENAME"

            if [ ! -d "$" ];then mkdir -p $CONF_REP;fi

            updateProg "$UPDATE_HTTP/collections/Lovecraft.sh"    "$CONF_REP/Lovecraft.sh"
            updateProg "$UPDATE_HTTP/collections/Monsieur_Phi.sh" "$CONF_REP/Monsieur_Phi.sh"
            updateProg "$UPDATE_HTTP/collections/Verne.sh"        "$CONF_REP/Verne.sh"
        fi
    }
#

##########################
# GESTION DES LIBRAIRIES #
##########################
    # - Affiche les collections du repertoire collections - #
    showLibs(){
        if [ $isShowLibs -eq 1 ]
        then
            #isCollectionNeeded=0;
            #echo ${INFO}$collectionRep $NORMAL
            local liste=$(ls --almost-all --ignore-backups -1 $CONF_REP/*.sh)
            local collection;
            local collections=''; #(string) listes de collections dans le repertoire de collection
            local fileName;
            local sorted='';

            for collection in $liste
            do
                #echo $collecion
                fileName=${collection##*/};         # nom.ext
                collections="$collections ${fileName%.sh}" 
            done

            IFS=$'\n' sorted=($(sort <<<"${collections[*]}"))
            unset IFS
            echo "$sorted";
        fi
    }


    # - appel des collections appeller en argument - #
    execLibrairiesCallProjet(){
        showDebug "$FUNCNAME($*)";
        local libNom=''
        if [ $isTrapCAsk -eq 1 ];then break;fi

        # initialialisation de librairiesTbl avec librairiesCall
        for librairieNu in {0..10} #$librairiesCallIndex}
        do
            local libNom=${librairiesCall[$librairieNu]:-''}
            if [ "$libNom" == '' ];then break;fi
            
            displayVar "libNom" "$libNom";

            local collectionPath="$CONF__COLLECTIONS_REP/$libNom.sh"

            if [ ! -f "$collectionPath" ]
            then
                echo $WARN"La collection ${INFO}$collectionPath$WARN existe pas ou n'est pas un fichier!"$NORMAL;
                continue
            fi

            if [ ! -x "$collectionPath" ]
            then
                chmod +x "$collectionPath"
                if [ $? -eq 0 ]
                then
                    echo $INFO"La collection $collectionPath a été rendu executable."$NORMAL;
                else
                    echo $WARN"La collection $collectionPath NE peut etre rendu executable."$NORMAL;
                    continue
                fi
            fi

            # - charger la collection - #
            . $collectionPath
            if [ $? -eq 0 ]
            then
                echo "$collectionPath chargé"
            else
                echo $WARN"Erreur lors du chargement de $collectionPath"
                continue
            fi    

            librairiesTbl[$libNom]="$libNom"


        done

        # - listeFormat - #
        #listeFormats;
        execLibrairiesCall
    }
#

# ################### #
# FONCTIONS DU PROJET #
# ################### #
        # isExtentionValide( 'element a tester' )
        isExtentionValide(){
            if [ $# -eq 0 ];then return 0;fi

            local -a ExtentionTbl=('ogg' 'mp3' 'mkv' 'png' 'jpg' 'jpeg' 'ogm');
            for element in "${ExtentionTbl[@]}";
            do
                #echo "$element - $1"
                #https://linuxhint.com/bash_lowercase_uppercase_strings/ (bash 4)
                #${1^^} = UPERCASE $1
                #${1,,} = lower $1
                if [ "$element" == "${1,,}" ]; then return 1; fi
            done
            return 0;
        }
    #


    #createCollection ( 'repSrc' 'repDest' )
    createCollection(){
        showDebug "\n$FUNCNAME($*)"
        if [ $isTrapCAsk -eq 1 ];then return;fi

        echo ''
        if [ $# -ne 2 ]
        then
            erreursAddEcho "$FUNCNAME: Mauvais nombre de param $#/"
            return $E_ARG_REQUIRE;
        fi

        repSrc="$1";
        if [ ! -d "$repSrc" ]
        then
            erreursAddEcho "Le repertoire source: $repSrc n'existe pas."
            return $E_EXIT_BAD
        fi


        repDest="$2";
        if [ ! -d "$repDest" ];then   mkdir -p "$repDest";fi
        cd "$repDest"
        if [ ! -d "$repDest" ]
        then
            erreursAddEcho "Erreur lors de la création du repertoire de destination: $repDest."
            return $E_EXIT_BAD
        fi

        if [ ! -x "$repDest" ]
        then
            erreursAddEcho "Le repertoire de destination: $repDest n'est pas accessible en écriture."
            return $E_EXIT_BAD
        fi

        notifsAddEcho "Créaction de la collection: $repDest venant de $repSrc"
        displayVar 'src    ' "$repSrc"
        displayVar 'repDest' "$repDest";
        displayVar 'courant' "$PWD"

    }

    closeCollection(){
        cd ..
    }

    #addln( 'source' 'lien' )
    # utilise $repSrc et créer le lien dans le repertoire courant
    addln(){
        showDebug "\n$FUNCNAME($*)"
        if [ $isTrapCAsk -eq 1 ];then return;fi

        local source="$repSrc/$1"
        local sourceFilename="${source##*/}";
        local lien=${2:-"$sourceFilename"}

        if [ ! -e "$source" ]
        then
            notifsAddEcho "a source ($source) du lien existe pas."
            return $E_EXIT_BAD
        fi

        if [ -e "$lien" ]
        then
            notifsAddEcho "Le lien $lien existe déjà."
        else
            eval-echo "ln -s \"$source\" \"$lien\""
            if [ $? -eq 0 ]
            then
                notifsAddEcho "Création du lien $lien OK."
            else
                erreursAddEcho "Erreur lors de la création du lien: $lien"
            fi
        fi
    }
#


###########
# RAPPORT #
###########
    rapportShow(){
        if [ $librairiesCallIndex -eq 0 ];then return;fi
        showDebug "\n$FUNCNAME($*)"
        echo ''
        displayVar "collections" "$CONF_REP"
        echo ''
    }


    #affiche les differentes variables calculés
    showDatas(){
        showDebug "\n$FUNCNAME($*)"
        echo 'Les datas:';

        showTags


        ## COVER ##
        echo ''
        showCovers

        echo ''
        ## GENERIQUE ##
        displayVar 'generiqueNo' "$generiqueNo" '' "$generiqueNb" 'glz' "$glz";
        displayVar 'tag_track  ' "$tag_track" '' "$tag_trackNb" 'glz' "$glz" 'tracksTN' "$tracksTN"

        displayVar 'fichierNo  ' "$fichierNo" '' "$fichierNb" 'flz' "$flz";
        if [ "$fichierTitre" != '' ]
        then
            displayVar 'fichierTitre         ' "$fichierTitre"
            displayVar 'fichierTitreNormalize' "$fichierTitreNormalize";
            displayVar 'fichierTitreTN      '  "$fichierTitreTN";
        fi

        echo ''
        ## LIVRE ##
        displayVar 'tomeNo     ' "$tomeNo" '' "$tomeNb" 'tlz' "$tlz";
        if [ "$tomeTitre" != '' ]
        then
            displayVar 'tomeTitre           ' "$tomeTitre"
            displayVar 'tomeTitreNormalize  ' "$tomeTitreNormalize";
            displayVar 'tomeTitreTN        '  "$tomeTitreTN";
        fi
        displayVar 'partieNo   ' "$partieNo" '' "$partieNb" 'plz' "$plz";
        if [ "$partieTitre" != '' ]
        then
            displayVar 'partieTitre         ' "$partieTitre"
            displayVar 'partieTitreNormalize'  "$partieTitreNormalize" 
            displayVar 'partieTitreTN      '  "$partieTitreTN" 
        fi

        displayVar 'chapitreNo ' "$chapitreNo" '' "$chapitreNb" 'clz' "$clz";
        # local -a chapitreTitreTbl;                    # utilisé en local: contient les titres des chapitres
        if [ "$chapitreTitre" != '' ]
        then
            displayVar 'chapitreTitre         ' "$chapitreTitre"
            displayVar 'chapitreTitreNormalize' "$chapitreTitreNormalize";
            displayVar 'chapitreTitreTN      '  "$chapitreTitreTN";
        fi

        displayVarNotNull 'generique_Glz_TitreNormalize       ' "$generique_Glz_TitreNormalize"
        displayVarNotNull 'generique_date_Glz_TitreNormalize  ' "$generique_date_Glz_TitreNormalize"

        if [ "$titreNormalize" != '' ]
        then
            displayVar 'livreTitreNormalize_Auto           ' "$livreTitreNormalize_Auto"
        fi
        #displayVar 'livreTitreNormalize_Album_Fichier ' "$livreTitreNormalize_Album_Fichier"
        #displayVar 'livreTitreNormalize_Album_Chapitre' "$livreTitreNormalize_Album_Chapitre"

        echo ''
        ## MUSIQUE ##
        displayVar 'musiqueNo  ' "$musiqueNo" '' "$musiqueNb" 'mlz' "$mlz";
        if [ "$musiqueTitre" != '' ]
        then
            displayVar 'musiqueTitre               ' "$musiqueTitre";
            displayVar 'musiqueTitreNormalize      ' "$musiqueTitreNormalize";
            displayVar 'musiqueTitreTN             ' "$musiqueTitreTN";
            displayVar 'musique_Mlz_TitreNormalize'  "$musique_Mlz_TitreNormalize";
        fi
        displayVarNotNull 'tag_note' "$tag_note"

    }
#


################
## PARAMETRES ##
################
TEMP=$(getopt \
     --options v::dVhnS \
     --long help,version,verbose::,debug,showVars,update,conf::,showLibs,showDuree\
,listeFormats,listeCollections,no-download,convert::,saveTags,showTags:: \
     -- "$@")
eval set -- "$TEMP"

    while true
    do
        if [ $isTrapCAsk -eq 1 ];then break;fi
        argument=${1:-''}
        parametre=${2:-''}
        #echo "argument:'$argument', parametre='$parametre'"
        case "$argument" in

            # - fonctions generiques - #
            -h|--help) usage; exit 0; shift ;;
            -V|--version) echo "$VERSION"; exit 0; shift ;;
            -v|--verbose)
                case "$parametre" in
                    '') verbosity=1; shift 2 ;;
                    *)  #echo "Option c, argument \`$2'" ;
                    verbosity=$parametre; shift 2;;
                esac
                ;;
            -d|--debug) isDebug=1;isShowVars=1;shift;  ;;
            --showVars) isShowVars=1; shift  ;;
            --update)   isUpdate=1;   shift ;;
            --conf)     CONF_PSP_PATH="$parametre";  shift 2; ;;

            # - Fonctions specifiques au programme - #
            -S)
                isSimulation=1;
                YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --simulate"
                shift
                ;;

            #--listeFormats)     listeFormats=1;        shift   ;;
            --collection)       collection=$parametre; shift 2 ;;

            --listeCollections | --showLibs )
                isShowLibs=1;  # ne pas activer le mecanisme par defaut
                isListeCollections=1;
                shift
                ;;

            --showDuree ) isShowDuree=1;        shift   ;;


            --no-download)       isNoDownload=1;        shift   ;;
            --convert)          setConvert "$2";       shift 2 ;;
            --saveTags)         isSaveTags=1;          shift   ;;
            --showTags)      showTagsPath="$2";  shift 2 ;;

            #--) # parcours des arguments supplementaires
            --)
                if [ -z "$parametre" ]
                then
                    shift;
                    break;
                fi
                #echo "--)$argument,$parametre"
                librairie="$parametre";
                librairiesCall[$librairiesCallIndex]="$librairie"
                ((librairiesCallIndex++))
                shift 2;
                ;;

            *)
                if [ -z "$argument" ]
                then
                    shift 1;
                    break;
                fi
                #echo "*)$argument,$parametre"
                librairie="$argument";
                librairiesCall[$librairiesCallIndex]="$librairie"
                ((librairiesCallIndex++))
                shift 1;
                ;;

            esac
    done
#


##########
## MAIN ##
##########
#clear;
echo "${INFO}$SCRIPT_PATH $VERSION$NORMAL";

usage;
selfUpdate;
showVars;
loadConfs 
showLibs;
##execLibrairiesCall

#setFfmpeg_loglevelTags
execLibrairiesCallProjet

echo ''
notifsShow;
erreursShow;
rapportShow;
trapErrShow;

showVarsPost;

#afficher l'espace libre sur collectionRoot 'en dernier car bug random'
echo -e "\nEspace libre restant:"
df_result=$(df -h /media/docutheques | grep /)
dl_support=$(echo $df_result   | cut --delimiter=" " --fields=1)
dl_taille=$(echo $df_result    | cut --delimiter=" " --fields=2)
#dl_utiliser=$(echo $df_result | cut --delimiter=" " --fields=3)
dl_libre=$(echo $df_result     | cut --delimiter=" " --fields=4)
#dl_utiliserPct=$(echo $df_result | cut --delimiter=" " --fields=5)
dl_monterSur=$(echo $df_result | cut --delimiter=" " --fields=6)

echo  "${INFO}$dl_support${NORMAL}(${INFO}$dl_monterSur${NORMAL}) Libre: $INFO$dl_libre$NORMAL/$INFO$dl_taille$NORMAL"

if [ $isTrapCAsk -eq 1 ];then exit $E_CTRL_C;fi
exit 0