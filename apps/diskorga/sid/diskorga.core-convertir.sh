echo_D "$BASH_SOURCE" 1;

###########
# convert #
###########
    # Convertit tous les fichier $dko_repertoire_source/$dko_convertFrom en dko_repertoire_destination.$dko_convertTo
    function dko_convertir() {
        local -i _fctIn_lvl=$fctIn_lvl_DEFAUT;
        fct-in "$*" $_fctIn_lvl;

        if $isTrapCAsk; then fctOut "$FUNCNAME($E_CTRL_C)" $_fctIn_lvl;return $E_CTRL_C; fi

        local _repertoire="${:-"$dko_repertoire_source"}";

        if [ ! -d "$_repertoire" ]
        then
            erreursAddEcho "$_repertoire n'est pas un repertoire";
            fctOut "$FUNCNAME($E_INODE_NOT_EXIST)";return $E_INODE_NOT_EXIST;
        fi

        if [ ! -d "$dko_repertoire_destination" ]
        then
            evalEcho "mkdir -p $dko_repertoire_destination";
        fi

        # - Creation de la pile des fichiers (fullPath)- #
        local -A _fichiers;
        local -i _fichierNb=0;
        local _srcPath="$_repertoire*.$dko_convertFrom";
        displayVar "source" "$_repertoire ('$dko_convertFrom' -> '$dko_convertTo')";
        #displayVar "_destPath" "$_destPath"

        local _fichier;
        local fichier_source_relatif="";
        for _fichier in $_srcPath
        do
            # - calcul de la source relative - #
            fichier_source_relatif="${_fichier/$_repertoire}"
            _fichiers[$fichierNb]="$fichier_source_relatif" 
            ((dkg_fichierNb++))
        done
        #displayVar "fichiers" "${fichiers[*]}"

        # - Trie de la pile - #
        IFS=$'\n';
        local _sorted=($(sort <<<"${fichiers[@]}"))
        #local _sorted=($(sort <<<"${fichiers[*]}"))
        unset IFS _fichiers;
        #displayVar "_sorted" "[$_fichierNb]${_sorted[*]}";

        # - Convertion des fichiers - #
        local _filename=''
        local _srcPath='';
        local _destPath='';

        for _fichier in ${_sorted[@]}
        do
            if $isTrapCAsk;then erreursAddEcho "$FUNCNAME: Interuption par Ctrl C"; break; fi
            _filename="${_fichier%.*}";
            _srcPath="$_repertoire${_filename}.$dko_convertFrom";
            _destPath="$_repertoire${_filename}.$dko_convertTo";

            #echo ""
            #displayVar "_fichier" "$_fichier" "_filename" "$_filename"
            #displayVar "_srcPath" "$_srcPath" "_destPath" "$_destPath"
            titre4 "Convertion de '$_fichier': '$_filename'";
            
            if [ -f "$_destPath" ]
            then
                notifsAddEcho "Convertion de '$_fichier': '$_filename' déjà convertit.";
                ((dko_inodeCompteurs['convert_exist']++))
            else
                #warning error
                if ffmpeg -loglevel error -i "$_srcPath" "$_destPath"
                then
                    notifsAddEcho "Convertion de '$_fichier': '$_filename'";
                    ((dko_inodeCompteurs['convert_ok']++))
                else
                    erreursAddEcho "Convertion de '$_fichier': '$_filename'";
                    ((dko_inodeCompteurs['convert_fail']++))
                fi
            fi
        done
        ((dko_actionNb++))
        fctOut "$FUNCNAME(0)" $_fctIn_lvl;return 0;
    }
#
