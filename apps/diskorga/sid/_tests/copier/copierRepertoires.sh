if $isShowLibs;then isPSPNextAsk=true;return 0;fi

echo "$BASH_SOURCE:BEGIN";


#cl;gtt.sh diskorga/sid/_tests/copier/copierRepertoires fct_dko_copierRepertoires
# repertoire  src dest identique ()./)

#cl;gtt.sh diskorga/sid/_tests/copier/copierRepertoires fct_dko_copierRepertoires - src="/tmp" dest="$HOME/"
#cl;gtt.sh diskorga/sid/_tests/copier/copierRepertoires fct_dko_copierRepertoires - src="src"
#cl;gtt.sh diskorga/sid/_tests/copier/copierRepertoires fct_dko_copierRepertoires -           dest="$HOME/tmp"


addLibrairie 'fct_dko_copierRepertoires';
function      fct_dko_copierRepertoires(){
    evalEcho "dko_isCopier=true";
    evalEcho "dko_recursifLevel=9";
    evalEcho "dko_isFileOnly=false";
    displayVar 'dko_isCreateCache'  "$dko_isCreateCache"    'dko_isCopier'  "$dko_isCopier" 'dko_isFromCache'    "$dko_isFromCache";
    displayVar 'dko_cachePrefixe'   "$dko_cachePrefixe"     'dko_cacheName' "$dko_cacheName";
    displayVar 'dko_isFileonly'     "$dko_isFileonly"       'dko_recursifLevel' "$dko_recursifLevel";

    dko_repertoire_source=${1:-"$dko_repertoire_source"};
    dko_repertoire_destination=${2:-"$dko_repertoire_destination"};

    evalEcho "dko_copierRepertoires \"$dko_repertoire_source\" \"$dko_repertoire_destination\"";

    dko_resumer;
}

##############
# LIBRAIRIES #
##############
addLibrairie 'fct_dko_copierRepertoires_cacheDesactive';
function      fct_dko_copierRepertoires_cacheDesactive() {
    titre2 'Cache désactivé';
    dko_isCopier=true;
    #dko_isCreateCache=true;
    displayVar 'dko_isCreateCache'  "$dko_isCreateCache"    'dko_isCopier'  "$dko_isCopier" 'dko_isFromCache'    "$dko_isFromCache";
    #displayVar 'dko_cachePrefixe'   "$dko_cachePrefixe"     'dko_cacheName' "$dko_cacheName";
    #displayVar 'dko_isFileonly'     "dko_isFileonly"        'dko_recursifLevel' "$dko_recursifLevel";
    evalEcho "fct_dko_copierRepertoires \"$TEST_DISKORGA_REP_SRC\" \"$TEST_DISKORGA_REP_DEST\";";
}

addLibrairie 'fct_dko_copierRepertoires_fromCache';
function      fct_dko_copierRepertoires_fromCache() {
    titre2 "dko_isFromCache=$dko_isFromCache";
    dko_isCopier=true;
    dko_isFromCache=true;
    #dko_isCreateCache=true;
    displayVar 'dko_isCreateCache'  "$dko_isCreateCache"    'dko_isCopier'  "$dko_isCopier" 'dko_isFromCache'    "$dko_isFromCache";
    #displayVar 'dko_cachePrefixe'   "$dko_cachePrefixe"     'dko_cacheName' "$dko_cacheName";
    #displayVar 'dko_isFileonly'     "dko_isFileonly"        'dko_recursifLevel' "$dko_recursifLevel";
    evalEcho "fct_dko_copierRepertoires \"$TEST_DISKORGA_REP_SRC\" \"$TEST_DISKORGA_REP_DEST\";";
}


addLibrairie 'fct_dko_copierRepertoires_TEST_DISKORGA';
function      fct_dko_copierRepertoires_TEST_DISKORGA() {
    titre2 'fct_dko_copierRepertoires TEST_DISKORGA';
    evalEcho "dko_isCopier=true";
    displayVar 'dko_isCreateCache'  "$dko_isCreateCache"    'dko_isCopier'  "$dko_isCopier" 'dko_isFromCache'    "$dko_isFromCache";
    displayVar 'dko_cachePrefixe'   "$dko_cachePrefixe"     'dko_cacheName' "$dko_cacheName";
    displayVar 'dko_isFileonly'     "$dko_isFileonly"       'dko_recursifLevel' "$dko_recursifLevel";
    evalEcho "fct_dko_copierRepertoires \"$TEST_DISKORGA_REP_SRC\" \"$TEST_DISKORGA_REP_DEST\";";
}



#############
# -  MAIN - #
#############
saveDebugGTTLevel $DEBUG_GTT_LEVEL_DEFAUT;
saveDebugLevel $DEBUG_LEVEL_DEFAUT;

displayVar 'dko_cachePrefixe'  "$dko_cachePrefixe"  'dko_cacheName'     "$dko_cacheName";
displayVar 'dko_isFromCache'   "$dko_isFromCache"   'dko_isKeepCache'   "$dko_isKeepCache";
displayVar 'dko_isCreateCache' "$dko_isCreateCache";


#fct_dko_copierRepertoires_TEST_DISKORGA

restaureDebugGTTLevel;
restaureDebugLevel;
echo "$BASH_SOURCE:END";
