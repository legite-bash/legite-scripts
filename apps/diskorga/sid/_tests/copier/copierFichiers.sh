if $isShowLibs;then isPSPNextAsk=true;return 0;fi

echo "$BASH_SOURCE:BEGIN";
#saveDebugGTTLevel $DEBUG_GTT_LEVEL_DEFAUT;

saveDebugLevel $DEBUG_LEVEL_DEFAUT;


TITRE1 "FAIL CACHE"
evalEcho "dko_isCopier=false";
evalEcho "dko_recursifLevel=9";
evalEcho "dko_isFileOnly=false";
displayVar 'dko_isCreateCache'  "$dko_isCreateCache"    'dko_isCopier'  "$dko_isCopier" 'dko_isFromCache'    "$dko_isFromCache";
displayVar 'dko_cachePrefixe'   "$dko_cachePrefixe"     'dko_cacheName' "$dko_cacheName";
displayVar 'dko_isFileonly'     "$dko_isFileonly"       'dko_recursifLevel' "$dko_recursifLevel";

evalEcho "dko_copierFichiers \"$TEST_DISKORGA_REP_SRC\" \"$TEST_DISKORGA_REP_DEST\"";

#restaureDebugGTTLevel;
restaureDebugLevel;

echo "$BASH_SOURCE:END";
