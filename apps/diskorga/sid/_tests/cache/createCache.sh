echo "$BASH_SOURCE:BEGIN";

#saveDebugGTTLevel $DEBUG_GTT_LEVEL_DEFAUT;
saveDebugLevel $DEBUG_LEVEL_DEFAUT;

displayVar 'dko_cachePrefixe'  "$dko_cachePrefixe" 'dko_cacheName'  "$dko_cacheName";
displayVar 'dko_isFromCache'   "$dko_isFromCache" 'dko_isKeepCache' "$dko_isKeepCache";
displayVar 'dko_isCreateCache' "$dko_isCreateCache";

function fct_createCacheDesactive() {
    titre2 'Cache désactivé';
    displayVar 'dko_isCreateCache'  "$dko_isCreateCache"    'dko_isCopier'  "$dko_isCopier" 'dko_isFromCache'    "$dko_isFromCache";
    #displayVar 'dko_cachePrefixe'   "$dko_cachePrefixe"     'dko_cacheName' "$dko_cacheName";
    #displayVar 'dko_isFileonly'     "dko_isFileonly"        'dko_recursifLevel' "$dko_recursifLevel";


    titre2 'dko_createCache';
    evalEcho "dko_isCopier=true";
    displayVar 'dko_isCreateCache'  "$dko_isCreateCache"    'dko_isCopier'  "$dko_isCopier" 'dko_isFromCache'    "$dko_isFromCache";
    displayVar 'dko_cachePrefixe'   "$dko_cachePrefixe"     'dko_cacheName' "$dko_cacheName";
    displayVar 'dko_isFileonly'     "$dko_isFileonly"       'dko_recursifLevel' "$dko_recursifLevel";
    evalEcho "dko_createCache";
}

function fct_createCache() {
    titre2 'dko_createCache';
    evalEcho "dko_isCopier=true";
    evalEcho "dko_recursifLevel=9";
    displayVar 'dko_isCreateCache'  "$dko_isCreateCache"    'dko_isCopier'      "$dko_isCopier"     'dko_isFromCache'    "$dko_isFromCache";
    displayVar 'dko_cachePrefixe'   "$dko_cachePrefixe"     'dko_cacheName'     "$dko_cacheName";
    displayVar 'dko_isFileonly'     "$dko_isFileonly"       'dko_recursifLevel' "$dko_recursifLevel";
    evalEcho "dko_createCache";
}


function fct_createCache_d() {
    titre2 'dko_createCache';
    evalEcho "dko_isCopier=true";
    evalEcho "dko_recursifLevel=0";
    displayVar 'dko_isCreateCache'  "$dko_isCreateCache"    'dko_isCopier'      "$dko_isCopier"     'dko_isFromCache'    "$dko_isFromCache";
    displayVar 'dko_cachePrefixe'   "$dko_cachePrefixe"     'dko_cacheName'     "$dko_cacheName";
    displayVar 'dko_isFileonly'     "$dko_isFileonly"       'dko_recursifLevel' "$dko_recursifLevel";
    evalEcho "dko_createCache 'd' '$1'";
}

function fct_createCache_d_recursif() {
    titre2 'dko_createCache';
    evalEcho "dko_isCopier=true";
    evalEcho "dko_recursifLevel=9";
    displayVar 'dko_isCreateCache'  "$dko_isCreateCache"    'dko_isCopier'      "$dko_isCopier"     'dko_isFromCache'    "$dko_isFromCache";
    displayVar 'dko_cachePrefixe'   "$dko_cachePrefixe"     'dko_cacheName'     "$dko_cacheName";
    displayVar 'dko_isFileonly'     "$dko_isFileonly"       'dko_recursifLevel' "$dko_recursifLevel";
    evalEcho "dko_createCache 'd' '$1'";
}


function fct_createCache_f() {
    titre2 'dko_createCache';
    evalEcho "dko_isCopier=true";
    evalEcho "dko_recursifLevel=0";
    displayVar 'dko_isCreateCache'  "$dko_isCreateCache"    'dko_isCopier'      "$dko_isCopier"     'dko_isFromCache'    "$dko_isFromCache";
    displayVar 'dko_cachePrefixe'   "$dko_cachePrefixe"     'dko_cacheName'     "$dko_cacheName";
    displayVar 'dko_isFileonly'     "$dko_isFileonly"       'dko_recursifLevel' "$dko_recursifLevel";
    evalEcho "dko_createCache 'f' '$1'";
}

function fct_createCache_f_recursif() {
    titre2 'dko_createCache';
    evalEcho "dko_isCopier=true";
    evalEcho "dko_recursifLevel=9";
    displayVar 'dko_isCreateCache'  "$dko_isCreateCache"    'dko_isCopier'      "$dko_isCopier"     'dko_isFromCache'    "$dko_isFromCache";
    displayVar 'dko_cachePrefixe'   "$dko_cachePrefixe"     'dko_cacheName'     "$dko_cacheName";
    displayVar 'dko_isFileonly'     "$dko_isFileonly"       'dko_recursifLevel' "$dko_recursifLevel";
    evalEcho "dko_createCache 'f' '$1'";
}



#fct_createCache_d "$HOME/Musique"*
fct_createCache_d_recursif "$HOME/Musique"*
#fct_createCache_f "$HOME/Musique"



#restaureDebugGTTLevel;
restaureDebugLevel;
echo "$BASH_SOURCE:END";
