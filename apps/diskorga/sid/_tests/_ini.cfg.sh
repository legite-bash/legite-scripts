echo_d "$BASH_SOURCE" 1;

declare -g _appNom='diskorga';
declare -g TEST_DISKORGA_REP="$TMP_ROOT/$_appNom";
declare -g TEST_DISKORGA_REP_SRC="$TEST_DISKORGA_REP/${_appNom}-src";
declare -g TEST_DISKORGA_REP_DEST="$TEST_DISKORGA_REP/${_appNom}-dest";


# créer le repertoire de test
function test_diskorga_initRep() {
    fct-in;

    TITRE1 "INITIALISATION DES TESTS";

    displayVar 'TEST_DISKORGA_REP'      "$TEST_DISKORGA_REP";
    displayVar 'TEST_DISKORGA_REP_SRC'  "$TEST_DISKORGA_REP_SRC";
    displayVar 'TEST_DISKORGA_REP_DEST' "$TEST_DISKORGA_REP_DEST";
 

    titre2 "Création des repertoires sources";
        # Suppression du repertoire DEST
        if [ -d "$TEST_DISKORGA_REP_SRC" ]
        then
            rm -R "$TEST_DISKORGA_REP_SRC";
        fi        
        mkdir -p "$TEST_DISKORGA_REP_SRC" && cd "$TEST_DISKORGA_REP_SRC";
        if [ $? -ne 0 ]
        then
            erreursAddEcho "Erreur lors de la creation de '$TEST_DISKORGA_REP_SRC'";
            fctOut "$FUNCNAME($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
        fi


        titre3 "Création des templates des fichiers";

        test_diskorga_createFichiers;
        test_diskorga_createFichiersParticuliers
        test_diskorga_createDirD1D9;
        #test_diskorga_createDirRecursif9;
    #

    titre3 "Création des repertoires destinations";
        # Suppression du repertoire DEST
        if [ -d "$TEST_DISKORGA_REP_DEST" ]
        then
            rm -R "$TEST_DISKORGA_REP_DEST";
        fi

        mkdir -p "$TEST_DISKORGA_REP_DEST";
        if [ $? -ne 0 ]
        then
            erreursAddEcho "Erreur lors de la creation du répertoire '$TEST_DISKORGA_REP_DEST'";
            fctOut "$FUNCNAME($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
        fi
    #

    #mc "$TEST_DISKORGA_REP_SRC" "$TEST_DISKORGA_REP_DEST";

    fctOut "$FUNCNAME(0)"; return 0;
}


############################
# CREATION DES REPERTOIRES #
############################
function test_diskorga_createDirD1D9() {
    fct-in;

    local _dirPrefixe9="${1:-""}"
    for _dirIndex in {1..9}
    do
        mkdir "D$_dirIndex" && cd "D$_dirIndex";
        if [ $? -ne 0 ]; then fctOut "$FUNCNAME";return $E_INODE_NOT_EXIST;fi
        test_diskorga_createFichiers;
        test_diskorga_createFichiersParticuliers;
        cd ..
    done
    fctOut "$FUNCNAME(0)";return 0;
}

# création de 9 repertoires dans le repertoire courant
function test_diskorga_createRep_test() {
    fct-in "$*";
    local -i _dirIndex=${1:-1}
    local    _dirPrefixe="${2:-"D$_dirIndex"}"
    if [ $_dirIndex -ge 9 ]; then return 0;fi
    #displayVar '_dirIndex' "$_dirIndex" '_dirPrefixe' "$_dirPrefixe"

    mkdir "$_dirPrefixe" && cd "$_dirPrefixe";
    if [ $? -ne 0 ]; then fctOut "$FUNCNAME";return $E_INODE_NOT_EXIST;fi

    (( _dirIndex++ ))
    _dirPrefixe="${_dirPrefixe}D$_dirIndex";
    test_diskorga_createRep9 $_dirIndex "$_dirPrefixe";

    # - Ajouter les fichier fichiers - #
    #test_diskorga_createFichiers;
    #test_diskorga_createFichiersParticuliers

    cd ..;

    fctOut "$FUNCNAME";return 0;
}


#########################
# CREATION DES FICHIERS #
#########################

# - Creation des templates de tests - #
function test_diskorga_createFichiers() {
    fct-in;
    #displayVar 'PWD' "$PWD";

    for _fichierName in F{1..10}
    do
        touch "$_fichierName";
    done

    fctOut "$FUNCNAME(0)";return 0;
}


function test_diskorga_createFichiersParticuliers() {
    fct-in;

    touch "a+b.ext"
    touch "a b .ext"
    touch "a [b].ext"

    # - test_filtre_supprimerFichiersParasite - #
    touch "desktop.ini"
    touch "Thumbs.db"
    touch "AlbumArt-ex AlbumArt"
    touch "AlbumArtAlbumArt"
    touch -- "-AlbumArtAlbumArt"
    mkdir 'rep avec espace'
    touch 'rep avec espace/fichier avec espace'
    touch 'Fichier à renommer.txt'

    fctOut "$FUNCNAME(0)";return 0;
}



echo '# - MAIN - #'
# - Controle des accèes - #
if $isShowLibs;
then
echo "$LINENO: isShowLibs == $isShowLibs -> return 0";
    isPSPNextAsk=true;
    return 0;
fi
test_diskorga_initRep;



