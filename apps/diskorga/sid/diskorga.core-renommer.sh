echo_D "$BASH_SOURCE" 1;

###############################
# renommerInode("repertoire") #
###############################
    function dko_renommerInode() {
        local -i _fctIn_lvl=3;
        fctIn "$*" $_fctIn_lvl;

        #http://www.tux-planet.fr/renommer-le-nom-des-fichiers-en-minuscule-sous-linux/

        if [ $# -eq 0 ]
        then
            erreursAddEcho "$FUNCNAME: Neccessite un inode (répertoire|fichier) en parametre.";
            fctOut "$FUNCNAME($E_ARG_REQUIRE)" $_fctIn_lvl;return $E_ARG_REQUIRE;
        fi

        if $isTrapCAsk; then fctOut "$FUNCNAME($E_CTRL_C)" $_fctIn_lvl;return $E_CTRL_C; fi

        ((dko_actionNb++))

        local    _oriName="$1";
        local    _oldName="$_oriName";

        local -i _filtreNo=0;   # global?

        getNormalizeText "$_oriName";

        #echo 'Affiche des filtres';
        #displayTableauA 'dko_filtres' "${dko_filtres[*]}";

        # executer tous les filtres
        #for _filtreNom in ${dko_filtres[*]}
        #do
        #    ${dko_filtres[$_filtreNom]} #execute la fonction filtre
        #done

        dko_renommerInodeLocal;         # diskorga.ori|cfg.sh

        local   _newName="$normalizeText";
        if [ "$_oriName" = "$_newName" ]
        then
            if [ -f "$_newName" ]
            then
                echo "${dko_tabulation}\"$_oriName\"";
            fi
        else

            # rename
            #echo "mv --no-clobber \"$oldName\" \"$_newName\" 2>/dev/null"
            if mv --no-clobber "$_oldName" "$_newName" 2>/dev/null
            then
                if [ -d "$_newName" ]
                then
                    echo "${dko_tabulation}$CYAN\"$_oriName\"";
                    echo "${dko_tabulation}\"$_newName\"$NORMAL";
                    #echo ${dko_tabulation}$YELLOW"filtres: $filtresActifs"$NORMAL;
                    ((dko_inodeCompteurs['repertoires_modifies']++));
                fi

                if [ -f "$_newName" ]
                then
                    echo "${dko_tabulation}$CYAN\"$_oriName\"";
                    echo "${dko_tabulation}\"$_newName\"$NORMAL";
                    #echo ${dko_tabulation}$YELLOW"filtres: $filtresActifs"$NORMAL;
                    ((dko_inodeCompteurs['fichiers_modifies']++));
                fi
            else
                erreursAddEcho "Erreur lors du renommage de l'inode: '$_oriName'";
            fi
        fi
        fctOut "$FUNCNAME(0)" $_fctIn_lvl;return 0;
    }

    # Fonction destinée à être surchargé par le client
    function dko_renommerInodeLocal() {
        fctIn "$*";
        echo_d "$FUNCNAME() du fichier '$BASH_SOURCE'";

        fctOut "$FUNCNAME(0)";return 0;
    }

    # Utilisé par renameRep()
    function dko_setTabulationDl() {
        dko_tabulation='';
        for (( _t=1; _t<=dko_profondeurNb; _t++ ))
        do
            dko_tabulation="$dko_tabulation$dko_tabulationMotif"
        done
        dko_tabulation=$INFO"$dko_tabulation"$NORMAL
        return 0;
    }


    #dko_moveFile(oldName, _newName)
    function dko_moveFileOld() {
        local -i _fctIn_lvl=$fctIn_lvl_DEFAUT;
        fctIn "$*" $_fctIn_lvl;
        if [ $# -ne 2 ]
        then
            erreursAddEcho "$FUNCNAME: Neccessite deux répertoires en parametre.";
            fctOut "$FUNCNAME($E_ARG_REQUIRE)" $_fctIn_lvl;return $E_ARG_REQUIRE;
        fi

        if $isTrapCAsk; then fctOut "$FUNCNAME($E_CTRL_C)" $_fctIn_lvl;return $E_CTRL_C; fi

        local   _oldName="$1"; # global
        local   _newName="$2"; # global
        #echo_d "filtre ${_filtreNo}:_oldName=$_oldName -> _newName=$_newName" $LINENO;

        if [ "$_oldName" != "$_newName" ]
        then
            #filtresActifs="$filtresActifs '$filtreNo'"
            mv --no-clobber "$_oldName" "$_newName" 2>/dev/null
            local _result=$?;
            echo "result=$_result";
            #if [ $_result -eq 0 ]
            #then
            #    echo "OK"
            #else
            #    echo "NOK"    
            #fi

            _oldName="$_newName";
        fi
        fctOut "$FUNCNAME(0)" $_fctIn_lvl;return 0;
    }
#


#############
# renameRep #
#############
    function dko_renommerRepertoire() {
        local -i _fctIn_lvl=$fctIn_lvl_DEFAUT;
        fctIn "$*";
        if [ $# -ne 1 ]
        then
            erreursAddEcho "$FUNCNAME: Neccessite un répertoires en parametre.";
            fctOut "$FUNCNAME($E_ARG_REQUIRE)" $_fctIn_lvl;return $E_ARG_REQUIRE;
        fi

        if $isTrapCAsk; then fctOut "$FUNCNAME($E_CTRL_C)" $_fctIn_lvl;return $E_CTRL_C; fi

        local _repertoireLocal="$1";
        if [ ! -d "$_repertoireLocal" ]
        then
            erreursAddEcho "$repertoireLocal n'est pas un repertoire.";
            fctOut "$FUNCNAME($E_INODE_NOT_EXIST)" $_fctIn_lvl;return $E_INODE_NOT_EXIST;
        fi
    
        if [ ! -w "$_repertoireLocal" ]
        then
            erreursAddEcho "$_repertoireLocal n'est pas en mode écriture";
            fctOut "$FUNCNAME($E_INODE_NOT_EXIST)" $_fctIn_lvl;return $E_INODE_NOT_EXIST;
        fi

        echo "${dko_tabulation}${INFO}$dko_repertoireTotal[$dko_profondeurNb/$dko_PROFONDEUR_MAX]$_repertoireLocal)$NORMAL";

        ###################################################
        # - on se positionne dans le repertoire indiqué - #
        ###################################################
        cd "$_repertoireLocal";
        if [ $? -ne 0 ]
        then
            # si le repertoire a pas été changé on quitte la fonction
            echo "${dko_tabulation}${WARN}Problème lors de l'entrée dans ce repertoire"
            fctOut "$FUNCNAME($E_INODE_NOT_EXIST)" _fctIn_lvl;return $E_INODE_NOT_EXIST;
        fi

        ((dko_profondeurNb++));
        dko_setTabulationDl;

        # - gestion du niveau de recursivité - #
        if [ $dko_profondeurNb -gt $dko_PROFONDEUR_MAX ]
        then
            erreursAddEcho "${dko_tabulation}${WARN}Niveau max ($dko_profondeurNb/$dko_PROFONDEUR_MAX) de sous répertoire atteint $NORMAL$NOBOLD";
            return # on sort de la fonction
        fi

        ((dko_repertoireTotal++));
        ((dko_inodeCompteurs['repertoires_lus']++));

        echo_d "rep courant($dko_profondeurNb):$PWD" $LINENO;
        # - analyse du contenu du repertoire - #
        local _inode;
        for _inode in *
        do
            if $isTrapCAsk
            then
                echo_d "$FUNCNAME: Interuption par Ctrl C";
                break;
            fi

            #displayVarDebug "$LINENO: PWD" "$PWD"  "_inode" "$_inode" ;
            if [ -d "$_inode" ]
            then
                if [ $dko_recursifLevel -gt 0 ]
                then
                    #_repertoireLocal="$_repertoireLocal/$_inode/";
                    _repertoireLocal="$PWD/$_inode/";
                    dko_renommerRepertoire "$_repertoireLocal";
                fi

            elif [ -f "$_inode" ]
            then
                echo_d "Traitement de l'_inode.";
                ((dko_inodeCompteurs['fichiers_lus']++));

                # on supprime les fichiers parasites
                if [ $dko_isDironly = false ]
                then
                    filtre_supprimerFichiersParasite "$_inode";
                fi

                if $dko_isDeleted
                then
                    continue;
                fi

                if [ $dko_isDironly = false ]
                then
                    dko_renommerInode "$_inode";
                fi

            elif [ -L "$_inode" ]
            then
                echo "${INFO} Lien symbolique. On ne modifie pas.$NORMAL";
                ((dko_inodeCompteurs['liens']++));
            fi

        done

        # - on change le nom du repertoire lui meme - #
        #echo "$LINENO:ON RENOMME LE REPERTOIRE:$repertoireLocal";
        if [ $dko_isFileonly = false ]
        then
            dko_renommerInode "$_repertoireLocal";
        fi

        # - retour au repertoire précédent - #
        #echo_d "cd ..";
        cd ..

        ((dko_profondeurNb--));
        _repertoireLocal="$PWD";    # a supprimer ? 

        dko_setTabulationDl;
        fctOut "$FUNCNAME(0)" $_fctIn_lvl; return 0;
    }
#
