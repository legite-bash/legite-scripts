echo_D "$BASH_SOURCE" 1;


# #################### #
# FONCTIONS GENERIQUES #
# #################### #
    function diskorga-usage() {
        if [ $aideLevel -gt 0 ]
        then
            echo '';
            titreInfo "$SCRIPT_FILENAME  diskorga  [-R] [--dironly] [--fileonly] [--profondeur_max=n] [- [src='repertoire_source'] [dest='repertoire_destination']]\
            \n   --cachePrefixe --createCache repertoire_source\
            \n   [--fichierMotif=motif] --renommer repertoire_source\
            \n   [--keepCache] [--cachePrefixe] [--fromCache] [--fichierMotif=motif] --copier repertoire_source repertoire_destination\
            \n   --comparerStats repertoire_source repertoire_destination\
            \n   --convertir [--convertirFrom=m4a] [--convertirTo=ogg] [repertoire='./']";
            echo -e "\nCommandes multiples:"
            echo "Si plusieurs commandes sont indiquées simultanément elles sont faites dans l'ordre suivant: renommer, copier, comparer"
            echo " --test filtre";
            #echo " --profondeur_max 0:desactiver; 1: repertoire_source; n: root+n-1. defaut: $PROFONDEUR_MAX";
            echo 'fichierMotif=".m4a$\\.ogg$" # ne recherche que les extentions m4a et ogg';
            echo 'repSource et repDestination ont, par défaut la valeur du chemin courant "./"';
            return 0;
        fi
    }

    function diskorga-showVars() {
        fctIn;
        if [ $isShowVars -eq 0 ];then fctOut "$FUNCNAME(0)";return 0;fi
        titre1 "$FUNCNAME";
        displayVar "dko_repertoire_source"      "$dko_repertoire_source"
        displayVar "dko_repertoire_destination" "$dko_repertoire_destination"
        displayVar "dko_fichierMotif"           "$dko_fichierMotif"
        displayVar "DISKORGA_TMP_REP"           "$DISKORGA_TMP_REP"
        displayVar "dko_cachePrefixe"           "$dko_cachePrefixe" "dko_isCreateCache" "$dko_isCreateCache" "dko_isKeepCache" "$dko_isKeepCache" "dko_isFromCache" "$dko_isFromCache"
        displayVar "dko_isDeleted"              "$dko_isDeleted"
        displayVar "dko_isDironly"              "$dko_isDironly"    "dko_isFileonly"    "$dko_isFileonly"
        displayVar "dko_isRenommer"             "$dko_isRenommer"   "dko_isCopier"      "$dko_isCopier" "dko_isComparer" "$dko_isComparer" 'dko_isComparerStats' "$dko_isComparerStats"
        displayVar "dko_isConvertir"            "$dko_isConvertir"  "dko_convertirFrom" #"$dko_convertirFrom" "dko_convertirTo" "$dko_convertirTo"
        displayVar "dko_isStats"                "$dko_isStats"
        displayVar "dko_tabulationMotif"        "$dko_tabulationMotif" "dko_tabulation" "$dko_tabulation"
        displayVar "dko_repertoireTotal"        "$dko_repertoireTotal"
        displayVar "dko_profondeurNb" "$dko_profondeurNb" "dko_PROFONDEUR_MAX" "$dko_PROFONDEUR_MAX"
        fctOut "$FUNCNAME(0)";return 0;
    }

    function diskorga-showVarsPost() {
        fctIn;
        if [ $isShowVars -eq 0 ];then fctOut "$FUNCNAME(0)";return 0;fi
        titre1 "$FUNCNAME";
    
        displayVar  'dko_actionNb'          "$dko_actionNb";
        displayVar  'dko_cachePrefixe'      "$dko_cachePrefixe";
        displayVar  'dko_isKeepCache'       "$dko_isKeepCache";
        displayVar  'dko_isCreateCache'     "$dko_isCreateCache";
        displayVar  'dko_isFromCache'       "$dko_isFromCache";
        displayVar  'dko_cacheName'         "$dko_cacheName",

        ##displayVar  -gr testDir="$DISKORGA_TMP_REP/test";

        #displayVar  -gA dko_filtres;                    # tableau contenant les fonctions de filtre
        ##displayVar  -g  filtreNo="";                   # indique le nom du filtre actif
        ##displayVar  filtresActifs="";                  # dko_filtres qui ont été activés 
        #displayVar  -g  dko_repertoire_source='';       # repertoire INITIAL contenant les fichiers a renommer defini en argument ou a copier
        #displayVar  -g  dko_repertoire_destination='';  # repertoire ou sera copié le repertoire source. uitilisé uniquement si -c présent
        #displayVar  -g  dko_tabulationMotif='-';        # motif de decalage pour chaque sous rep (modifié aussi par setTabulationDl)
        #displayVar  -g  dko_tabulation='';              # cumul du tabMotifFct# conflit avec gtt
        #displayVar  -gi dko_repertoireTotal=0;          # nombre total de repertoire scannés

        #displayVar  -g  dko_fichierMotif


        fctOut "$FUNCNAME(0)";return 0;
    }
#


# ######
# MAIN #
# ######
#saveDebugGTTLevel $DEBUG_GTT_LEVEL_DEFAUT;
#saveDebugLevel $DEBUG_LEVEL_DEFAUT;


#debugGTTLevel=5;
execOnce "$DISKORGA_REP/diskorga.vars.sh";
execOnce "$DISKORGA_REP/diskorga.core.sh";



# - Chargement des filtres - #
loadRepertoiresCfg 'filtres' "$DISKORGA_REP"; # charge les sur-repertoires mais pas les sous repertoires
#diskorga-usage;
#diskorga-showVars; # appeller automatiquement par GTT ?

# - Backup de l'app - #
if [ $backupLevel -ne 0 ]
then
    loadRepertoiresCfg '_backup' "$GTT_REP/";
    backup-sidToToday 'diskorga';  # backup l'app diskorga
fi



# ########## #
# PARAMETRES #
# ########## #
#displayTableau  'ARG_TBL' "${ARG_TBL[@]}";

    local -i _argNu=-1;
    while true
    do
        if $isTrapCAsk;then break;fi
        ((_argNu++))
        if [ $_argNu -ge $ARG_NB ];then break;fi

        #displayVar '${ARG_TBL[$_argNu]}' "${ARG_TBL[$_argNu]}";
        case "${ARG_TBL[$_argNu]}" in
            -)  # indicateur des parametres pour les librairies/collections
                break;  # Les PSPParams sont déjà traités
                ;;
            #-h)
            #    diskorga-usage;
            #;;

            --renommer)     dko_isRenommer=true;    ;;
            --keepCache)    dko_isKeepCache=true;   ;;

            --createCache)
                dko_isCreateCache=true;
                dko_isKeepCache=true; # force la preservation du cache
                ;;

            #--cachePrefixe) dko_cachePrefixe="$2";  shift 1; ;;
            --fromCache)    dko_isFromCache=true;   ;;

            -c|--copier)    dko_isCopier=true;
                            dko_isComparerStats=true;
                            dko_isCreateCache=true;  ;;

            --comparer)     dko_isComparer=true;    ;;

            --convertir)    dko_isConvertir=true;   ;;
            #--convertFrom) dko_convertFrom="$2";   ;;
            #--convertTo)   dko_convertTo="$2";     ;;

            --dironly)      dko_isDironly=true;        ((recursifLvl++)); ;;
            --fileonly)     dko_isFileonly=true;    ;;
            --stats)        dko_isStats=true;       ;;
            --comparerStats) dko_isComparerStats=true;          ;;

            #-p|--dko_PROFONDEUR_MAX )
                #    # on accepte la valeur demandé
                #    dko_PROFONDEUR_MAX=$2;

                #    # on limite les valeurs si elle depasse
                #    if [ $2 -lt 0 ]
                #    then
                #        dko_PROFONDEUR_MAX=0;
                #        titreWarn "dko_PROFONDEUR_MAX=$dko_PROFONDEUR_MAX"
                #    fi
                #    if [ $2 -gt 9 ]
                #    then
                #        dko_PROFONDEUR_MAX=9;
                #        titreWarn "dko_PROFONDEUR_MAX=$dko_PROFONDEUR_MAX"
                #    fi
                #    ;;
    
            #--dko_fichierMotif) dko_fichierMotif="$2"           ;;

            #*)
                #
        esac

    done
    unset librairie param;
#

########
# main #
########
#echo $BASH_SOURCE:$LINENO;
#beginStandard

# Controle d'accè: Si $isShowLibs -> on quitte après chargement des libs 
if $isShowLibs; then return;fi

diskorga-usage;
dko_repertoire_source="${PSPParams['src']:-"./"}";
dko_repertoire_source=${dko_repertoire_source%/};       dko_repertoire_source="$dko_repertoire_source/";

dko_repertoire_destination="${PSPParams['dest']:-"./"}";
dko_repertoire_destination=${dko_repertoire_destination%/};     dko_repertoire_destination="$dko_repertoire_destination/";

#displayVar "dko_repertoire_source"      "$dko_repertoire_source";
#displayVar "dko_repertoire_destination" "$dko_repertoire_destination";


if [ ! -d "$dko_repertoire_source" ]
then
    titreWarn "diskorga:Le repertoire source '$dko_repertoire_source' n'est pas un repertoire!"
    #endStandard
    diskorga-showVarsPost;

    return $E_ARG_BAD;
else
    echo_d "'$dko_repertoire_source' est bien un repertoire.";
fi

diskorga-showVars;


if $dko_isCreateCache
then
    dko_createCache 'd';
    dko_createCache 'f';
fi

if $dko_isRenommer
then
    titre1 'RENOMMER';
    dko_renommerRepertoire "$dko_repertoire_source";
fi

if $dko_isCopier
then
    dko_copier;
fi

if $dko_isComparer
then
    dko_comparer;
fi

if $dko_isConvertir
then
    dko_convertir;
fi

if [ $dko_isKeepCache = true ] && [ -f "$dko_cachePrefixe-dir.txt" ]
then
    echo_d "Suppression du cache '$dko_cachePrefixe-dir.txt'";
    rm "$dko_cachePrefixe-dir.txt";
fi

if $dko_isStats
then
    dko_stats_analyse "$dko_repertoire_source";
fi

dko_resumer; # apres la sortie standard


if [ $testLevel -ne 0 ]
then
    displayVar 'DISKORGA_TMP_REP' "$DISKORGA_TMP_REP";
    loadRepertoiresCfg '_tests' "$DISKORGA_REP";
fi


diskorga-showVarsPost;

restaureDebugGTTLevel;
restaureDebugLevel;