echo_D "$BASH_SOURCE" 1;
#renommerInodeLocal() {
#    showDebug "diskorga.ori|cfg.sh $FUNCNAME($@)";
#    echo "diskorga.ori|cfg.sh $FUNCNAME($@)";
#    #normalizeText=$(echo $normalizeText | sed -e 's/Text a changé/Test de rempacement/g');
#}

###############
# les filtres #
###############
##########################################
# filtre_supprimerFichierParasite(@)     #
# $@: tous les arg                       #
# utilise la variable globale $dko_inodeCompteurs #
##########################################
addLibrairie 'filtre_supprimerFichiersParasite';
function      filtre_supprimerFichiersParasite() {
    local -i _fctIn_level=$fctIn_lvl_DEFAUT;
    fctIn "$*" $_fctIn_level;
    if [ $# -ne 1 ]
    then
        erreursAddEcho "$FUNCNAME: Neccessite un nom de fichier en parametre.";
        fctOut "$FUNCNAME($E_ARG_REQUIRE)" $_fctIn_level;return $E_ARG_REQUIRE;
    fi

    if $isTrapCAsk ; then fctOut "$FUNCNAME($E_CTRL_C)" $_fctIn_level;return $E_CTRL_C; fi

    _fichierNom="$1";

    dko_isDeleted=false;

    # indoe: global ?
    case "${_fichierNom,,}" in 
        "desktop.ini" | "thumbs.db" )
            dko_isDeleted=true;
            ;;
    esac

    # suppression des AlbumArt*
    local   _subAlbumArt="${_fichierNom:0:9}";
    displayVarDebug '_fichierNom' "$_fichierNom" '_subAlbumArt' "$_subAlbumArt"
    if [ "$_subAlbumArt" = "AlbumArt" ]
    then
        dko_isDeleted=true;
    fi

    if $dko_isDeleted
    then
        echo "${dko_tabulation}${WARN}Suppression du fichier '$_fichierNom'.$NORMAL";
        rm "./$_fichierNom";
        ((dko_inodeCompteurs['fichiers_supprimes']++));
    fi
    fctOut "$FUNCNAME(0)" $_fctIn_level; return 0;
}


addLibrairie 'filtre_supprimerSignatures';
function      filtre_supprimerSignatures() {
    local -i _fctIn_level=$fctIn_lvl_DEFAUT;
    fctIn "$*" $_fctIn_level;
    
    if $isTrapCAsk; then fctOut "$FUNCNAME($E_CTRL_C)" $_fctIn_level;return $E_CTRL_C; fi
    #normalizeText="$1";

     # - SUPPRIMER LES SEQUENCES SUIVANTES - #
    normalizeText=$(echo $normalizeText | sed -e 's/emule-island.com//I');
    normalizeText=$(echo $normalizeText | sed -e 's/emule-island.ru//I');
    normalizeText=$(echo $normalizeText | sed -e 's/zone-telechargement.ws//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.Annuaire-Telechargement.com//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.Zone-Telechargement.com//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.Zone-Telechargement.net//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.Zone-Telechargement.Ws//I');
    normalizeText=$(echo $normalizeText | sed -e 's/zone-telechargement.com//I');
    normalizeText=$(echo $normalizeText | sed -e 's/Annuaire-Telechargement.com//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.Annuaire-Telechargement.com//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.Zone-Telechargement1.org//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.Zone-Annuaire.com//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.Zone-Telechargement1.com//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.Cpasbien.me//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.CpasBien.io//I');
    normalizeText=$(echo $normalizeText | sed -e 's/found_via_ed2k-series.new//I');
    normalizeText=$(echo $normalizeText | sed -e 's/.WwW.ZT-ZA.COM//I')
    normalizeText=$(echo $normalizeText | sed -e 's/.WwW.ZoNe-TelecharGement.CaM//gi')
    #displayVar "diskorga.ori: $LINENO: normalizeText" "$normalizeText"
    fctOut "$FUNCNAME(0)" $_fctIn_level;return 0;
}
