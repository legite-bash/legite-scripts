echo_D "$BASH_SOURCE" 1;

################
# TEMP ET LOGS #
################
declare -gr DISKORGA_TMP_REP="$TMP_ROOT/diskorga"

if [ ! -d "$DISKORGA_TMP_REP" ]
then
    mkdir -p "$DISKORGA_TMP_REP";
fi

declare -gi dko_actionNb=0;                 # nb d'action demandé
declare -g  dko_cachePrefixe="$DISKORGA_TMP_REP/$SCRIPT_NAME";    # $dko_cachePrefixe-dir.txt $dko_cachePrefixe-file.txt (Attention n'est pas un repertoire)
declare -g  dko_isKeepCache=true;           # garder le fichier de cache (ne pas le supprimer)
declare -g  dko_isCreateCache=false;        # crée seulement le cache
                                            # (ne fait aucune autre operation, met dko_isKeepCache a true)
                                            # Seulement si isCopier=true
declare -g  dko_isFromCache=false;          # ne (re)crée pas le cache, utilise les caches donnés en prefixe
declare -g  dko_cacheName='';               # nom du cache actuel (calculer selon dir/file)

#declare -gr testDir="$DISKORGA_TMP_REP/test";

declare -gA dko_filtres;                    # tableau contenant les fonctions de filtre
#declare -g  filtreNo="";                   # indique le nom du filtre actif
#declare filtresActifs="";                  # dko_filtres qui ont été activés 
declare -g  dko_repertoire_source='';       # repertoire INITIAL contenant les fichiers a renommer defini en argument ou a copier
declare -g  dko_repertoire_destination='';  # repertoire ou sera copié le repertoire source. uitilisé uniquement si -c présent
declare -g  dko_tabulationMotif='-';        # motif de decalage pour chaque sous rep (modifié aussi par setTabulationDl)
declare -g  dko_tabulation='';              # cumul du tabMotifFct# conflit avec gtt
declare -gi dko_repertoireTotal=0;          # nombre total de repertoire scannés

declare -g  dko_fichierMotif=''             # motif de recherche pour les fichiers

declare -gi dko_profondeurNb=0;             # niveau de repertoire (le rep de base est compté)
declare -gr dko_PROFONDEUR_MAX=9;           # 0: desactive; 1: que le rep root 2: root+1 rep; etc
                                            # ne pas depasser 9 car lit 10 comme 1

declare -gi dko_inodeRepTotal=0;declare -gi dko_inodeRepLu=0;declare -gi dko_inodeRepPct=0;
declare -gi dko_inodeFicTotal=0;declare -gi dko_inodeFicLu=0;declare -gi inodeFicPct=0;
declare -gA dko_inodeCompteurs; #tableau associatif
declare -gi dko_inodeCompteurs['repertoires_lus']=0;
declare -gi dko_inodeCompteurs['repertoires_modifies']=0;
declare -gi dko_inodeCompteurs['repertoires_modifies_total']=0;
declare -gi dko_inodeCompteurs['fichiers_lus']=0;
declare -gi dko_inodeCompteurs['fichiers_modifies']=0;
declare -gi dko_inodeCompteurs['fichiers_modifies_total']=0;
declare -gi dko_inodeCompteurs['fichiers_supprimes']=0;
declare -gi dko_inodeCompteurs['liens']=0;
declare -gi dko_inodeCompteurs['repertoires_copies']=0;
declare -gi dko_inodeCompteurs['repertoires_non_copies']=0;
declare -gi dko_inodeCompteurs['repertoires_copies_existant']=0;
declare -gi dko_inodeCompteurs['fichiers_copies']=0;
declare -gi dko_inodeCompteurs['fichiers_non_copies']=0;
declare -gi dko_inodeCompteurs['fichiers_copies_existant']=0;
declare -gi dko_inodeCompteurs['convert_exist']=0;
declare -gi dko_inodeCompteurs['convert_ok']=0;
declare -gi dko_inodeCompteurs['convert_fail']=0;

# tag interne
declare -g  dko_isDeleted=false;            # fichier a supprimé (utiliser par les dko_filtres)

# tag de comportement
declare -gi dko_recursifLevel=0;
declare -g  dko_isDironly=false;    # ne copier que les repertoires
declare -g  dko_isFileonly=false;   # ne copier que les fichiers

# FONCTIONS #
declare -g  dko_isCopier=false;      # Le fonction de copie doit elle être appelé a la place de la fonction renommer?
declare -g  dko_isRenommer=false;     # desactiver la fonction de renommage
declare -g  dko_isComparer=false;    # compare la presence/absence des fichiers/rep entre source et destination
declare -g  dko_isComparerStats=false;# comparer les sats de source et destination
declare -g  dko_isConvertir=false;    # convertit le repertoire donnée defuat './'
declare -g  dko_convertirFrom='m4a';
declare -g  dko_converirtTo='ogg';

declare -g  dko_isStats=false;      # afficher les stats des repertoires
declare -gA dko_stats_analyseResults;
