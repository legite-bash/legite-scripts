echo_D "$BASH_SOURCE" 1;


function dko_titreStats() {
    TITRE1 "${1}";
}

execOnce "$DISKORGA_REP/diskorga.core-renommer.sh";
execOnce "$DISKORGA_REP/diskorga.core-copier.sh";
execOnce "$DISKORGA_REP/diskorga.core-convertir.sh";
execOnce "$DISKORGA_REP/diskorga.core-comparer.sh";


#########
# stats #
#########
    function dko_resumer() {
        local -i _fctIn_lvl=$fctIn_lvl_DEFAUT;
        fctIn "$*" $_fctIn_lvl;

        if $isTrapCAsk; then fctOut "$FUNCNAME($E_CTRL_C)" $_fctIn_lvl;return $E_CTRL_C; fi

        #displayVar $LINENO:'dko_actionNb' "$dko_actionNb";
        if [ $dko_actionNb -eq 0 ]
        then
            #if [ "$dko_repertoire_source" != "$dko_repertoire_destination" ]
            #then
            #    dko_isComparerStats=true;    # Le repertoire source et destination sont différentes donc fournis donc activation de la comparaison
            #else
                echo_d "Pas de résumé car aucune action n'a été effectué";
                fctOut "$FUNCNAME(0)" $_fctIn_lvl;return 0;
            #fi
        fi
        isTrapCAsk=false; #reinitailiser le ctrlC
        titre1 'RÉSUMÉ';

        dko_comparerStats;

        if $dko_isRenommer; then dko_resumerRenommer; fi
        if $dko_isConvertir;then dko_resumerConvertir;fi
        if $dko_isCopier;   then dko_resumerCopier;   fi

        if $dko_isKeepCache
        then
            titre4 'Caches';
            if [ -f "$dko_cachePrefixe-dir.txt" ];then  ls -l "${dko_cachePrefixe}-dir.txt";fi
            if [ -f "$dko_cachePrefixe-dir.txt" ];then  ls -l "${dko_cachePrefixe}-file.txt";fi
        fi
        fctOut "$FUNCNAME(0)" $_fctIn_lvl;return 0;
    }


    function dko_resumerRenommer() {
        fctIn;
        dko_titreStats 'RENOMMER';
        echo -e "      \t lu \t modifié";
        echo -e "REP   \t ${dko_inodeCompteurs['repertoires_lus']} \t ${dko_inodeCompteurs['repertoires_modifies']}";
        echo -e "FIC   \t ${dko_inodeCompteurs['fichiers_lus']} \t ${dko_inodeCompteurs['fichiers_modifies']}";
        echo -e "LIENS \t ${dko_inodeCompteurs['liens']}";
        echo ". Les modifications indiquent le nombre total des modifications. Un repertoire (ou fichier) peut etre modifié par plusieurs filtres."
        fctOut "$FUNCNAME(0)";return 0;
    }


    function dko_resumerCopier() {
        fctIn;
        dko_titreStats 'COPIER';
        echo -e "         % \t ok \t exist \t fail";
        echo -e "REP   $dko_inodeRepPct \t ${dko_inodeCompteurs['repertoires_copies']} \t  ${dko_inodeCompteurs['repertoires_copies_existant']} \t ${dko_inodeCompteurs['repertoires_non_copies']}";
        echo -e "FIC   $inodeFicPct \t ${dko_inodeCompteurs['fichiers_copies']} \t  ${dko_inodeCompteurs['fichiers_copies_existant']} \t ${dko_inodeCompteurs['fichiers_non_copies']}";
        fctOut "$FUNCNAME(0)";return 0;
    }


    function dko_resumerConvertir() {
        fctIn;
        dko_titreStats "CONVERTIR";
        echo -e "          ok \t exist \t fail ";
        echo -e "FIC     ${dko_inodeCompteurs['convert_ok']} \t ${dko_inodeCompteurs['convert_exist']} \t  ${dko_inodeCompteurs['convert_fail']}";
        fctOut "$FUNCNAME(0)";return 0;
    }


    #stats ($repertoire)
    function dko_stats_analyse() {
        local -i _fctIn_lvl=$fctIn_lvl_DEFAUT;
        fctIn "$*" $_fctIn_lvl;
        if [ $# -ne 1 ]
        then
            erreursAddEcho "$FUNCNAME: Neccessite un répertoires en parametre.";
            fctOut "$FUNCNAME($E_ARG_REQUIRE)" $_fctIn_lvl;return $E_ARG_REQUIRE;
        fi

        if $isTrapCAsk; then fctOut "$FUNCNAME($E_CTRL_C)" $_fctIn_lvl;return $E_CTRL_C; fi

        #displayVar $LINENO:'dko_actionNb' "$dko_actionNb" 'dko_isStats' "$dko_isStats";
        if [ $dko_actionNb -eq 0 ] && [ $dko_isStats = false ];then fctOut "$FUNCNAME($E_F1)" $_fctIn_lvl; return $E_F1; fi
        local _repertoire="$1";
        local _srcDest='src';

        # on determine si c'est la source ou la destination
        if [ "$_repertoire" = "$dko_repertoire_destination" ]; then _srcDest='Dest'; fi
        
        _repertoire=${_repertoire%/}; # Suppression du slashe de fin si existe

        dko_titreStats "Analyse du répertoire:'$_repertoire' (copie du resultat dans $dko_cachePrefixe-$_srcDest-stats.txt)"

        # initialisation de la variable global de resultat
        dko_stats_analyseResults['inodeTotal']=0;
        dko_stats_analyseResults['repNb']=0;
        dko_stats_analyseResults['fileNb']=0;
        dko_stats_analyseResults['linkNb']=0;
        dko_stats_analyseResults['otherNb']=0;
        dko_stats_analyseResults['sizeTotal']=0;

        echo '' > "$dko_cachePrefixe-stats.txt";

        if [ ! $# -eq 1 ]
        then
            titreUsage 'Usage stats_analyseRepertoire(_repertoire).'
            fctOut "$FUNCNAME($E_F1)" $_fctIn_lvl;return $E_F1;
        fi

        if [ ! -d "$_repertoire" ]
        then
            titreWarn "'$repertoire' n'est pas un repertoire.";
            fctOut "$FUNCNAME($E_INODE_NOT_EXIST)" $_fctIn_lvl;return $E_INODE_NOT_EXIST;
        fi
        dko_stats_analyseRepertoire "$_repertoire";

        # afficher le resultat
        local _result="Total=${dko_stats_analyseResults['inodeTotal']} repertoires=${dko_stats_analyseResults['repNb']} fichiers=${dko_stats_analyseResults['fileNb']}" Liens="${dko_stats_analyseResults['linkNb']}" Autres="${dko_stats_analyseResults['otherNb']}"; #sizeTotal=${dko_stats_analyseResults['sizeTotal']};"
        titreInfo "$_result";
        echo "$_result" >> "$dko_cachePrefixe-stats.txt";
        fctOut "$FUNCNAME(0)" $_fctIn_lvl;return 0;
    }


    # fait un decompte des types d'inode dans le repertoire FOURNIS
    function dko_stats_analyseRepertoire() {
        #fctIn "$*"; # fonction recursive
        if [ $# -ne 1 ]
        then
            erreursAddEcho "$FUNCNAME: Neccessite un répertoire en parametre.";
            fctOut "$FUNCNAME($E_ARG_REQUIRE)" $_fctIn_lvl;return $E_ARG_REQUIRE;
        fi

        if $isTrapCAsk
        then return $E_CTRL_C;
        fi

        local _repertoireLocal="$1";
        local _inode;
        for _inode in "$_repertoireLocal"/*       # '$' = ?
        do
            if $isTrapCAsk
            then
                erreursAddEcho "$FUNCNAME: Interuption par Ctrl C";
                break;
            fi

            ((dko_stats_analyseResults['inodeTotal']++))
            echo_d "_inode(${dko_stats_analyseResults['inodeTotal']}): $_inode" 3;

            if [ -d "$_inode" ]
            then
                ((dko_stats_analyseResults['repNb']++));
                if [ $recursifLevel -gt 0 ]
                then
                    #echo "DIR_: $_inode" >> $dko_cachePrefixe-stats.txt
                    dko_stats_analyseRepertoire "$_inode";
                fi
            elif [ -L "$_inode" ]
            then
                ((dko_stats_analyseResults['linkNb']++));
                #echo $INFO"LINK: $_inode"$NORMAL
                echo "LINK: $_inode" >> $dko_cachePrefixe-stats.txt
            elif [ -f "$_inode" ]
            then
                ((dko_stats_analyseResults['fileNb']++));
            else
                ((dko_stats_analyseResults['otherNb']++));
                #echo $INFO"OTHE: $_inode"$NORMAL
                echo "OTHE: $_inode" >> $dko_cachePrefixe-stats.txt
            fi


        #local -i tailleFichier=$(stat -c "%s" "$_inode")
        #echo "taille fichier stat="$tailleFichier
        #tailleFichier=$(wc -c <  "$_inode")
        #echo "taille fichier wc="$tailleFichier
        #tailleFichier=$(du -b "$_inode")
        #echo "taille fichier du="$tailleFichier
            
        #dko_stats_analyseResults['sizeTotal']=$((${dko_stats_analyseResults['sizeTotal']}+$tailleFichier));
        done;
        #fctOut "$FUNCNAME"; # fonction recursive
        return 0;
    }
#

