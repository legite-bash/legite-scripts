echo_D "$BASH_SOURCE" 1;

#########
# cache #
#########
#dko_createCache(["$type"='d'] ["$dko_repertoire_source"] )
function dko_createCache() {
    local -i _fctIn_lvl=1;
    fctIn "$*" $_fctIn_lvl;


    if $dko_isFromCache
    then
        echo_d "dko_isFromCache=$dko_isFromCache"
        fctOut "$FUNCNAME($E_FALSE)" $_fctIn_lvl;return $E_FALSE;
    fi
    if [ $dko_isCopier = false ]
    then
        echo_d "dko_isCopier=$dko_isCopier"
        fctOut "$FUNCNAME($E_FALSE)" $_fctIn_lvl;return $E_FALSE;
    fi

    local _type="${1:-"d"}"; # par defaut: Creation du cache des repertoires
    local _repSrc="${2:-"$dko_repertoire_source"}";

    titre2_d $FUNCNAME':Création du cache des répertoires ou des fichiers' _fctIn_lvl;

    case "$_type" in
        'd')
            titre2 'Création du cache des repertoires';
            if $dko_isFileonly || [ $dko_recursifLevel -eq 0 ]
            then
                notifsAddEcho   "Creation du cache des repertoires désactivée";
                displayVarDebug 'dko_isFileonly' "$dko_isFileonly" 'dko_recursifLevel' "$dko_recursifLevel";
                setCodeRetour $E_F1 'Creation du cache des repertoires désactivée';
                fctOut "$FUNCNAME($codeRetour)" $_fctIn_lvl; return $codeRetour;
            fi
            dko_cacheName="${dko_cachePrefixe}-dir.txt";
            ;;
        'f')
            titre2 'Création du cache des fichiers';
            #displayVarDebug 'dko_isDironly' "$dko_isDironly"
            if $dko_isDironly
            then
                titreInfo 'La Création du cache des fichiers est désactivée (--dironly)';
                setCodeRetour $E_F2 'La Création du cache des fichiers est désactivée (--dironly)';
                fctOut "$FUNCNAME($codeRetour)" $_fctIn_lvl; return $codeRetour; 
            fi

            dko_cacheName="${dko_cachePrefixe}-file.txt";
        ;;
        * )
            erreursAddEcho "Mauvais type de cache";
            displayVarDebug '_type' "$_type";
            setCodeRetour $E_F5 "Mauvais type de cache ('$_type')";
            fctOut "$FUNCNAME($codeRetour)" $_fctIn_lvl; return $codeRetour;

    esac

    echo_d "Construction EFFECTIVE du cache avec le motif: '$dko_fichierMotif'" $_fctIn_lvl;
    if [ -z "$dko_fichierMotif"  ]
    then
        echo_d "find \"$_repSrc\" -type $_type";
        find "$_repSrc" -type $_type                            2>/dev/null   > "$dko_cacheName";
    else
        echo_d "find \"$_repSrc\" -type $_type | grep \"$dko_fichierMotif\"";
        find "$_repSrc" -type $_type | grep "$dko_fichierMotif" 2>/dev/null   > "$dko_cacheName";
    fi
    
    if [ ! -f "$dko_cacheName" ]
    then
        erreursAddEcho 'Création du cache des fichiers';
        setCodeRetour $E_F2 'Erreur lors de la création du cache des fichiers';
        fctOut "$FUNCNAME($codeRetour)" $_fctIn_lvl; return $codeRetour;
    fi

    local _inodeNb=$(wc --lines $dko_cacheName);

    case "$_type" in
        'd')
            dko_inodeRepTotal=${_inodeNb% *};
            ((dko_inodeRepTotal--));
            ;;
        'f')
            dko_inodeFicTotal=${_inodeNb% *};
            ((dko_inodeFicTotal--));

            # si pas recursif alors suppression des sous elements
            if [ $dko_recursifLevel -eq 0 ]
            then
                echo "Suppression des sous elements";
            fi
            ;;
    esac

    echo "$dko_cacheName crée avec $dko_inodeRepTotal répertoires et $dko_inodeFicTotal fichiers";
    #ls -l "$dko_cacheName"


    ## -- trie du cache -- ##
    titre4 'trie du cache';

    ### --- Recuperation du cache brut --- ###
    titre4_d  "Recuperation du cache brut:_fichierLists"
    local     _fichierLists;
    readarray _fichierLists < "$dko_cacheName";
    #displayTableau '_fichierLists' "${_fichierLists[@]}"
    

    titre4_d  "Contenu du tableau trié:_donneesTriees"
    IFS='\n';
    #local _donneesTriees=($(sort --ignore-case <<< "${fichierList[*]}")) # les parentheses crée une liste
    local _donneesTriees=$(sort --ignore-case --ignore-leading-blanks <<< "${_fichierLists[@]}"); # @ est le bon
    IFS=$IFS_DEFAULT;
    unset _fichierLists;
    #echo_D "$_donneesTriees";


    titre4_d " Préparation du fichier '${dko_cacheName}.tri'";
    local _cacheNameTri="${dko_cacheName}.tri";
    if [ -f "$_cacheNameTri" ]
    then
        rm "$_cacheNameTri";
    fi
    touch "$_cacheNameTri";

    titre4_d " Préparation du fichier '${dko_cacheName}.rel'";
    local _cacheNameRel="${dko_cacheName}.rel";
    if [ -f "$_cacheNameRel" ]
    then
        rm "$_cacheNameRel";
    fi
    touch "$_cacheNameRel";


    titre4_d " Copie du tableau _donneesTriees[${#_donneesTriees}] dans $dko_cacheName";
    local _inode;
    for _inode in ${_donneesTriees[@]}  # NE PAS QUOTER sinon cela renvoie tout une fois
    do
        if [ -z "$_inode" ];     then continue;fi
        if [ "$_inode" == "\n" ];then continue;fi
        if [ "$_inode" == './' ];then continue;fi
        _inode="${_inode# }";   # suppression de l'espace en }er caractère
        #displayVar '_inode' "$_inode";

        echo "$_inode" >> "$_cacheNameTri";

        #cache relatif
        _inode="${_inode#"$_repSrc/"}";
        #displayVar '_inode' "$_inode";
        echo "$_inode" >> "$_cacheNameRel";


    done
    #eval_echo_d "ls -l $_cacheNameTri $_cacheNameRel" 2;
    
    
    #titre4_d "Contenu du cache:$_cacheNameTri" 3;
    #eval_echo_d "cat $_cacheNameTri" 3;

    titre4_d "Contenu du cache:$_cacheNameRel" 3;
    #eval_echo_d "cat $_cacheNameRel" 3;

    ((dko_actionNb++))
    fctOut "$FUNCNAME(0)" $_fctIn_lvl;return 0;
}


##########
# copier #
##########
function dko_copier() {
    local -i _fctIn_lvl=$fctIn_lvl_DEFAUT;
    fctIn "$*" $_fctIn_lvl;
    titre1 'OPERATION DE COPIE';

    local _repScr="${1:-"$dko_repertoire_source"}";
    local _repDes="${2:-"$dko_repertoire_destination"}";

    if [ ! -d "$_repScr" ]
    then
        erreursAddEcho "$FUNCNAME: Le repertoire source: '$_repScr' n'est pas un repertoire";
        fctOut "$FUNCNAME($E_INODE_NOT_EXIST)" $_fctIn_lvl;return $E_INODE_NOT_EXIST;
    fi

    if [ "$_repScr" = "$_repDes" ]
    then
        erreursAddEcho "$FUNCNAME:Les repertoires source et destination sont identiques: '$_repScr'";
        fctOut "$FUNCNAME($E_ARG_BAD)" $_fctIn_lvl; return $E_ARG_BAD;
    fi

    # NE PAS CREER LE REPERTOIRE DE DESTINATION
    ((dko_actionNb++))

    #dko_createCache 'd';   # est appellé par dko_copierRepertoires()
    dko_copierRepertoires "$_repScr" "$_repDes";

    dko_createCache 'f';
    dko_copierFichiers "$_repScr" "$_repDes";
    fctOut "$FUNCNAME(0)" $_fctIn_lvl;return 0;
}


# Copie des REPERTOIRES #
function dko_copierRepertoires() {
    local -i _fctIn_lvl=1;
    fctIn "$*" $_fctIn_lvl;

    # copie des sous repertoires uniquement si la recursivité est demandé
    #displayVar 'dko_recursifLevel' "$dko_recursifLevel";
    #displayVar 'dko_isFileonly' "$dko_isFileonly";
    if [ $dko_recursifLevel -eq 0 ] ||  [ $dko_isFileonly = true ]
    then
        echo 'Recursivité désactivée ou fileOnly'
        fctOut "$FUNCNAME($E_FALSE)" $_fctIn_lvl;return $E_FALSE;
    fi

    ((dko_actionNb++))

    local _repSrc="${1:-"$dko_repertoire_source"}";
    local _repDes="${2:-"$dko_repertoire_destination"}";

    if [ "$_repSrc" = "$_repDes" ]
    then
        notifsAddEcho "$FUNCNAME():Les repertoires source et destination sont les mêmes ($_repSrc)."
        fctOut "$FUNCNAME($E_TRUE)" $_fctIn_lvl;return $E_TRUE; # Pas d'erreur de copie: les fichiers sont bien à destination
    fi

    displayVar 'PWD' "$PWD";
    titre2 "Copie des REPERTOIRES '$_repSrc' vers '$_repDes' (en créant/utilisant le cache)";

    #titre3 'Chargement du cache des repertoires triés';
    local _cacheName="$dko_cachePrefixe-dir.txt";
    local _cacheNameTri="${_cacheName}.tri";
    local _cacheNameRel="${_cacheName}.rel";
    displayVar '_cacheName' "$_cacheName";
    displayVar '_cacheNameTri' "$_cacheNameTri";
    displayVar '_cacheNameRel' "$_cacheNameRel";

    local _donneesTriees;
    #########################################
    # (CREATION/)CHARGEMENT DU CACHE ABSOLU #
    #########################################
    function loadCacheAbs(){
        # Creation du cache (si inexistant)
        if [ ! -f "$_cacheNameTri" ]
        then
            if dko_createCache 'd'
            then
                echo_d "Cache '$_cacheNameTri' crée.";
            else
                erreursAddEcho "Cache: Erreur lors de la creation du cache ($_cacheName) -> Sortie de la copie";
                fctOut "$FUNCNAME($E_INODE_NOT_EXIST)" $_fctIn_lvl;return $E_INODE_NOT_EXIST;
            fi
        fi

        titre3 "Chargement du cache trié '$_cacheNameTri'";
        readarray _donneesTriees < "$_cacheNameTri";
    }

    ##########################################
    # (CREATION/)CHARGEMENT DU CACHE RELATIF #
    ##########################################
    local _cacheSelect="$_cacheNameRel";
    displayVar '_cacheSelect' "$_cacheSelect";

    titre3 "Chargement du cache '$_cacheSelect'";

    # Selection du type de cache (--option a implémenter) #

    if $dko_isFromCache
    then
        echo_d "dko_isFromCache=true:Chargement à partir du cache.";
        if [ ! -f "$_cacheSelect" ]
        then
            erreursAddEcho "$FUNCNAME(): dko_isFromCache=true Mais cache inexistant -> sortie de la copie.";
            fctOut "$FUNCNAME($E_INODE_NOT_EXIST)" $_fctIn_lvl;return $E_INODE_NOT_EXIST;
        fi
        echo_d "Le cache '$_cacheSelect' existe.";

    else  
        if dko_createCache 'd'
        then
            echo_d "Cache '$_cacheSelect' crée.";
        else
            erreursAddEcho "Cache: Erreur lors de la creation du cache ($_cacheSelect) -> Sortie de la copie";
            fctOut "$FUNCNAME($E_INODE_NOT_EXIST)" $_fctIn_lvl;return $E_INODE_NOT_EXIST;
        fi
    fi

    titre3 "Chargement du cache '$_cacheSelect'";
    readarray _donneesTriees < "$_cacheSelect";

    #########################
    # COPIE DES REPERTOIRES #
    #########################
    titre3 'Copies des répertoires';
    local _repertoire;  #for
    local _repertoire_source_relative='';
    local _repertoire_final='';
    local _realpath_src='';
    local _realpath_des='';


    #displayTableau '${_donneesTriees[*]}' "${_donneesTriees[*]}"
    displayVar 'PWD' "$PWD";
    for _repertoire in "${_donneesTriees[@]}"
    do
        if $isTrapCAsk; then echo_d "$FUNCNAME: Interuption par Ctrl C"; break; fi

        _repertoire=$(echo $_repertoire | tr -d '\n'); # supprime les sauts de ligne
        if [ -z "$_repertoire" ];then continue; fi

        _repertoire="${_repertoire#/}";# suppression du slash de debut
     
        _repertoire_source_relative="${_repertoire/$_repSrc}";          # suppression de la source root
        _repertoire_source_relative="${_repertoire_source_relative#/}"; # suppression du slashe root
        _repertoire_final="$_repDes/$_repertoire_source_relative";

        #_realpath_src="$(realpath "$_repSrc")";
        #_realpath_des="$(realpath "$_repertoire_final")";

        #displayVar '_repertoire' "$_repertoire";
        #displayVar '_repertoire_source_relative' "$_repertoire_source_relative";
        #displayVar '_repertoire_final' "$_repertoire_final";
        #displayVar '_realpath_src' "$_realpath_src";
        #displayVar '_realpath_des' "$_realpath_des";

        if [ -d "$_repertoire_final" ]
        then
            ((dko_inodeCompteurs['repertoires_copies_existant']++))
            ((dko_inodeRepLu++));
            if [ $dko_inodeRepTotal -eq 0 ]
            then dko_inodeRepPct=0;
            else dko_inodeRepPct=$((dko_inodeRepLu * 100 / dko_inodeRepTotal));
            fi
            echo "[$dko_inodeRepPct%]'$_repertoire_final' existe deja."

        else
            mkdir -p "$_repertoire_final" 2>$dko_cachePrefixe-cperror-last
            local -i _result=$?
            if [ $_result -eq 0 ]
            then
                ((dko_inodeCompteurs['repertoires_copies']++));
                ((dko_inodeRepLu++));
            if [ $dko_inodeRepTotal -eq 0 ]
            then dko_inodeRepPct=0;
            else dko_inodeRepPct=$((dko_inodeRepLu * 100 / dko_inodeRepTotal));
            fi
                notifsAddEcho "[$dko_inodeRepPct%] mkdir -p '$_repertoire_final'"; #  > 100% -> corriger le bug
            else
                local _error=$(cat $dko_cachePrefixe-cperror-last)
                ((dko_inodeCompteurs['repertoires_non_copies']++));
                erreursAddEcho "[$dko_inodeRepPct%] mkdir -p '$_repertoire_final': $_error";
            fi
        fi
   done

    ###########
    # RAPPORT #
    ###########
    echo '';
    #if [ $debugLevel -ne 0 ]
    #then
        echo "mc \"$_repSrc\" \"$_repDes\""; 
    #fi


    ########################
    # SUPPRESSION DU CACHE #
    ########################
    if [  $dko_isKeepCache = false ]
    then
        titre3 'Suppression des caches';
        if [ -f "$_cacheName" ];    then evalEcho "rm '$_cacheName'";fi
        if [ -f "$_cacheNameTri" ]; then evalEcho "rm '$_cacheNameTri'";fi
        if [ -f "$_cacheNameRel" ]; then evalEcho "rm '$_cacheNameRel'";fi
    fi

    fctOut "$FUNCNAME(0)" $_fctIn_lvl;return 0;
} #dko_copierRepertoires()


# Copie des FICHIERS #
# function dko_copierFichiers(  'repertorie_source' 'repertoire_de_destination' )
# Copie des FICHIERS du cache dans '$1'
function dko_copierFichiers() {
    local -i _fctIn_lvl=1;
    fctIn "$*" $_fctIn_lvl;

    #if [ $dko_isDironly = false ] && $dko_isCreateCache    # pourquoi dko_isCreateCache ?
    if [ $dko_isDironly ]; then fctOut "$FUNCNAME($E_FALSE)" $_fctIn_lvl;return $E_FALSE; fi

    local _repScr="${1:-"$dko_repertoire_source"}";
    local _repDes="${2:-"$dko_repertoire_destination"}";

    titre2 "Copie des FICHIERS du cache dans '$_repDes'.";

    ## - Chargement du cache - ##
    local _cacheName="${dko_cachePrefixe}-file.txt";
    local _cacheNameTri="${_cacheName}.tri";
    displayVar '_cacheName' "$_cacheName" '_cacheNameTri' "$_cacheNameTri";

    if [ ! -f "$_cacheNameTri" ]
    then
        if dko_createCache 'f';
        then
            echo_d "Création du cache des fichiers '$_cacheNameTri'.";
        else
            erreursAddEcho "Cache: Erreur lors de la creation du cache ($_cacheNameTri) -> Sortie de la copie";
            fctOut "$FUNCNAME" $_fctIn_lvl;return $E_INODE_NOT_EXIST;
        fi
    fi


    titre3 'Création du repertoire de destination';
    if [ ! -d "$_repDes" ]
    then
        echo_d "Repertoire de destination inexistant -> création";
        evalEcho "mkdir -p \"$_repDes\"";
        if [ $? -ne 0 ]
        then
            erreursAddEcho 'Impossible de créer le repertoire de destination: '$_repDes;
            fctOut "$FUNCNAME($E_INODE_NOT_EXIST)" $_fctIn_lvl; return $E_INODE_NOT_EXIST;
        fi
    fi


    titre3 'Chargement du cache des fichiers triés';
    local _donneesTriees;
    readarray _donneesTriees < "$_cacheNameTri";

    if [ $dko_recursifLevel -eq 0 ]
    then
        titreInfo 'En mode non recursif les sous elements ne seront pas copiés.';
    fi

    titre3 'Copies des fichiers';
    #local out='';
    #local rep='';
    #local filename='';
    local -i _slashNb=0;
    local fichier_source_relatif='';    # 
    local _fichier_path_destination=''  # 

    for _fichierPath in "${_donneesTriees[@]}"
    do
        if $isTrapCAsk
        then
            erreursAddEcho "$FUNCNAME: Interuption par Ctrl C";
            fctOut "$FUNCNAME($E_FALSE)" $_fctIn_lvl; return $E_FALSE;
        fi

        _fichierPath=$(echo $_fichierPath | tr -d '\n'); # suppression des sauts de ligne
        if [ -z "$_fichierPath" ];then continue; fi

        _fichierPath="${_fichierPath#/}";   # suppression du slash de debut

        # - calcul de la source relative - #
        fichier_source_relatif="${_fichierPath/$repSrc}";       # suppression du repertoire source dans le chemin du fichier)
        fichier_source_relatif=${fichier_source_relatif#/};     # suppression du slash du debut (pour eviter le root)
        echo_d 'fichier_source_relatif' "$fichier_source_relatif" 3;

        # si pas recursif et qu'il y a un slash alors on passe au fichier suivant
        if [ $dko_recursifLevel -eq 0 ]
        then
            _slashNb=$(echo "$fichier_source_relatif" | grep -o '/' | wc -l);
            if [ $_slashNb -gt 0 ]; then echo -n '-'; continue; fi
        fi

        _fichier_path_destination="$_repDes$fichier_source_relatif";
        displayVarDebug '_fichier_path_destination' "$_fichier_path_destination";

        if [ ! -f "$_fichier_path_destination" ]
        then
            echo -e "[$inodeFicPct%]cp \"$_fichier_path_destination\"\c";
            if cp "$_fichierPath" "$_fichier_path_destination" 2>${dko_cachePrefixe}-cperror;
            then
                echo $INFO'ok'$NORMAL
                notifsAdd "[$inodeFicPct%]cp '$_fichier_path_destination'";
                ((dko_inodeCompteurs['fichiers_copies']++));
                ((dko_inodeFicLu++));
                if [ $dko_inodeFicTotal -eq 0 ]
                then inodeFicPct=0;
                else inodeFicPct=$((dko_inodeFicLu * 100 / dko_inodeFicTotal));
                fi
            else
                echo $WARN'fail'$NORMAL
                local error=$(cat $dko_cachePrefixe-cperror);
                ((dko_inodeCompteurs['fichiers_non_copies']++));
                erreursAdd "[$inodeFicPct%]cp '$_fichier_path_destination': $error";
            fi
        else
            # comparer les tailles et si differentes lancer la copie
            # ...

            #si taille egale
            echo "[$inodeFicPct%]'$_fichier_path_destination' existe deja.";
            ((dko_inodeCompteurs['fichiers_copies_existant']++))
            ((dko_inodeFicLu++));
            if [ $dko_inodeFicTotal -eq 0 ]
            then inodeFicPct=0;
            else inodeFicPct=$((dko_inodeFicLu * 100 / dko_inodeFicTotal));
            fi
        fi
        #((index++)) # for debug

    done;

    if [ ! $dko_isKeepCache  ]
    then
        titre3 'Suppression des caches';
        if [ -f "$_cacheName" ];    then evalEcho "rm '$_cacheName'";fi
        if [ -f "$_cacheNameTri" ]; then evalEcho "rm '$_cacheNameTri'";fi
    fi
    
    ((dko_actionNb++))

    fctOut "$FUNCNAME(0)" $_fctIn_lvl; return 0;
} # copieFile()

