echo_D "$BASH_SOURCE" 1;

###########
# compare #
###########
    function dko_comparerStats() {
        fct-in;
        dko_stats_analyse "$dko_repertoire_source";

        # - affichier les stats de la destination uniquement en mode copie - #
        if $dko_isStats;then stats_analyse "$dko_repertoire_destination";fi
        fctOut "$FUNCNAME(0)";return 0;
    }


    function dko_comparer() {
        fct-in;
        #compareInode "$dko_repertoire_source" "$dko_repertoire_destination";
        echo "$FUNCNAME: non implementé.";
        fctOut "$FUNCNAME(0)";return 0;
    }


    #compare 2 inodes
    function dko_comparerInode() {
        local -i _fctIn_lvl=$fctIn_lvl_DEFAUT;
        fct-in "$*" $_fctIn_lvl;
        if [ $# -ne 2 ]
        then
            erreursAddEcho "$FUNCNAME: Neccessite deux inodes (répertoire|fichier) en parametre.";
            fctOut "$FUNCNAME($E_ARG_REQUIRE)" $_fctIn_lvl;return $E_ARG_REQUIRE;
        fi

        if $isTrapCAsk; then fctOut "$FUNCNAME($E_CTRL_C)" $_fctIn_lvl;return $E_CTRL_C; fi

        ((dko_actionNb++))

        local _src="$1";    local _dest="$2";
        local -A _srcAttr;  local -A _destAttr;

        _srcAttr[0]='n/a';

        if [ ! -e "$_src" ]
        then
            _srcAttr['type']='n/a';
        fi
        if   [ -d "$_src" ];  then _srcAttr['type']='D'
        elif [ -L "$_src" ];  then _srcAttr['type']='L'
        elif [ -f "$_src" ];  then _srcAttr['type']='F'
        else _srcAttr['type']='O'
        fi

        # - taille - #
        _srcAttr['sizeWC']=$(wc -c < "$_src")
        _srcAttr['sizeSTAT']=$(stat -c%s "$_src")

        # Rendu
        displayVar '_src' "$_src" 'type' "${_srcAttr['type']}" 'sizeWC' "${_srcAttr[sizeWC]}" 'sizeSTAT' "${_srcAttr[sizeSTAT]}"

        fctOut "$FUNCNAME(0)" $_fctIn_lvl;return 0;
    }
#
