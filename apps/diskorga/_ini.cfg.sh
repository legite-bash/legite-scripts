echo_D "$BASH_SOURCE" 1;
# Ce fichier est la main.sh de l'application
declare -gr DISKORGA_VERSION_SELECT=${DISKORGA_VERSION_SELECT:-'sid'};
declare -gr DISKORGA_REP="$APPS_REP/diskorga/$DISKORGA_VERSION_SELECT";

requireOnce "${DISKORGA_REP}/diskorga.sh";