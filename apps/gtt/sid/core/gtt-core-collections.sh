echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


#############################
# AFFICHAGE DES COLLECTIONS #
#############################
    # - Affiche les collections du repertoire 'collections' - #
    function showCollections() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $isShowCollections = false ];then fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;fi

        titre1 "Les collections ($COLLECTIONS_REP)";
        if [ ! -d "$COLLECTIONS_REP" ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}(:Le repertoire '$COLLECTIONS_REP' existe pas.";
            fct-pile-gtt-out $E_INODE_NOT_EXIST $_fct_pile_gtt_level; return $E_INODE_NOT_EXIST;
        fi

        
        #titre2_D "Chargement des sous rep via find" 2;
        # _repListe: tableau contenant les chemins COMPLET des repertoires
        local -a _repListe=$(find "$COLLECTIONS_REP" -type d );  #find sous repertoires


        # - calcul du nombre de sous repertoire de $COLLECTIONS_REP - #
        local    _slashs="${COLLECTIONS_REP//[^\/]}";      # on ne garde que les slash (/)
        local -i _COLLECTIONS_REP_sousRepNb=${#_slashs};


        #titre2_D "Trie des sous repertoires" 2;
        local -i _repNb=0;  
        for _rep in $(sort <<< "${_repListe[*]}") # NE PAS QUOTER sinon renvoi une seule chaine de caractere
        do
            if $isTrapCAsk;then fct-pile-gtt-out $E_CTRL_C $_fct_pile_app_level; return $E_CTRL_C; fi

            # ne pas afficher la 1ere ligne
            ((_repNb++ ))
            if [ $_repNb -eq 1 ];then continue; fi


            #display-vars '_rep' "$_rep";

            # - forme raccourci - #
            # calcul du nombre de sous repertoire
            #_slashs="${_rep//[^\/]}";      # on ne garde que les slash (/)
            #local -i _sousRepNb=${#_slashs};
            #_sousRepNb=$((_sousRepNb-_APPS_REP_sousRepNb));
            #local _sep="$(dupliqueNCar $_sousRepNb '-')";
            #echo -n "$_sep";

            # - forme chemin relatif - #
            local _repRel="${_rep#$COLLECTIONS_REP/}";  # supprime le root
            _repRel="${_repRel%/*}/";                   # on supprime le dernier repertoire
            #echo -n $_repRel;
            #display-vars '_repRel' "$_repRel" '_rep' "$_rep";

            # ne pas afficher le repertoire .git
            if [ "${_repRel:0:4}" = ".git" ];then continue; fi

            fs-ls-sh "$_rep" "$_repRel";

        done

        fct-pile-gtt-out 0 $_fct_pile_gtt_level; return 0;
    }
#


##############################
# CHARGEMENT DES COLLECTIONS #
##############################

    #loadCollection( '_PSPActuel' )
    # Charge une collection si le fichier homonyme.sh existe dans $COLLECTIONS_REP
    # charge autommatiquement la librairie homonyme si elle est définie par addLibrairie ET si $isCollectionExecLibSelf=true
    # sauf si --no-auto-lib=1 est défini
    # code retour:
    # $E_TRUE: La collection est chargée
    # $E_FALSE: lib seule ou erreur
    function loadCollection() {
        if $isTrapCAsk;then return $E_CTRL_C;fi

        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -ne 1 ]
        then
            titre2_D "${FUNCNAME[0]}(($*):END" 2;
            fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;
        fi
        titre2_D "${FUNCNAME[0]}(($*)" 2;

        collectionNom="$1";              # global

        if [ -z "$collectionNom" ]
        then
            titre2_D "${FUNCNAME[0]}(($*):END" 2;
            fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;
        fi

        if [ ! "${collectionNom: -3}" = ".sh" ];then collectionNom+=".sh";fi
        #display-vars 'collectionNom' "$collectionNom";


        ##########################################
        titre3_D "La collection existe t'elle?" 2;
        ##########################################
        if isCollectionExist "$collectionNom"
        then
            echo-D "Oui, la collection existe." 3;
        else
            echo-D "'$collectionNom' n'est pas une collection" 3;
            titre2_D "${FUNCNAME[0]}(($*):END" 2;
            fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;
        fi

        #######################################################
        titre3_D "La collection a t'elle déjà été chargée ?" 3;
        #######################################################
        if isCollectionExec "$collectionNom"
        then
            titre2_D "${FUNCNAME[0]}(($*):END" 2;
            fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
        fi
        echo-D "Non. la collection n'a jamais été executée." 3;


        ###################################################
        local _sousRep="${collectionNom%/*}";
        titre3_D "CHARGEMENT des ini.cfg de la collection ($_sousRep)" 2 #
        ###################################################
        searchRepertoiresCfg "$_sousRep";
        unset _sousRep;


        ################################################
        titre3_D "Est-ce une collection existante ?" 2 #
        ################################################
        local _retour=$E_FALSE;
        if isCollectionExist "$collectionNom" # formes possibles 'rep/collection' ou 'collection'
        then
            echo-D "Oui, la collection existe." 3;

            # Chargement du fichier de configuration de la collection
            loadCollectionCfg "$collectionNom";
            
            # Chargement de la collection
            local _collectionPath="$COLLECTIONS_REP/$collectionNom";
            exec-file-once "$_collectionPath";
            _retour=$?;
            # marquée la collection comme chargée, même en cas d'erreur pour éviter de la recharger
            setCollectionExec "$collectionNom" 2;
            if [ $_retour -eq $E_TRUE ]\
            || [ $_retour -eq $E_FALSE ]
            then # la collection est chargée

                # Execution de la librairie homonyme (autoexecution)
                #display-vars 'isCollectionExecLibSelf' "$isCollectionExecLibSelf";
                if [ $isCollectionExecLibSelf = true ] # gestion d'un booléen [0|1]
                then
                    local _lib="${collectionNom##*/}";
                    display-vars "$LINENO:Execution automatique de la librairie homonyme ($_lib)";
                    execLib "$_lib";
                    _retour=$?; # pas d'erreur: la lib est executé (ou non)
                fi
            else
                erreurs-pile-add-echo "${FUNCNAME[0]}: Erreur lors du chargement de la collection existante '$_collectionPath'"
            fi
        fi
        titre2_D "${FUNCNAME[0]}(($*):END" 2;

        fct-pile-gtt-out $_retour $_fct_pile_gtt_level; return $_retour;
    }


###################################################
# CHARGEMENT DE LA CONFIGURATION DE LA COLLECTION #
# fichier.cfg
###################################################
    # loadCollectionCfg ( '[collectionNom/]collectionNom' )
    function loadCollectionCfg() {
        if $isTrapCAsk;then return $E_CTRL_C;fi

        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -ne 1 ];then  fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;fi
        local _retour=$E_FALSE;

        local _confNom="$1.cfg";  #    [rep/]collection.cfg
        local _confPath="$COLLECTIONS_REP/$_confNom.sh";


        titre3_D "${FUNCNAME[0]}():La configuration de la collection '$_confNom' a t'elle déjà été chargée?" 2;
        if isCollectionExec "$_confNom"
        then
            echo-D "Oui, elle a déjà été chargée." 3;
            fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
        fi
        echo-D "Non. Elle n'a jamais été chargée" 3;


        titre3_D "la collection existe t'elle?" 3;
        if isCollectionExist "$_confNom"
        then # la collection existe
            #echo "$BASH_SOURCE:$LINENO: (exec-file $_confPath)"
            exec-file "$_confPath";     # Chargement de la configuration
            _retour=$?;
            if [ $_retour -eq 0 ]
            then # la configuration, est chargée
                echo-D  "La configuration de la collection '$_confNom' est chargée." 3;
                setCollectionExec "$_confNom" 1;       # La config est chargée..
                fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
            else
                erreurs-pile-add-echo "Erreur lors du chargement de la collection existante '$_confPath'"
            fi
        else
            echo-D "Non, elle n'existe pas." 3;
        fi

        fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;
    }

    #isCollectionOnly ( 'nom' )    # collection ou lib
    # renvoie $E_TRUE si l'argument est une collection MAIS PAS une lib
    function isCollectionOnly() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -ne 1 ];then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;fi
        local _nom="$1";
        isAppExist        "$_nom";    local -i _isAppExist=$?;
        isCollectionExist "$_nom";    local -i _isCollectionExist=$?;
        isLibExist        "$_nom";    local -i _isLibExist=$?;
            #display-vars '_nom' "$_nom" '_isCollectionExist' "$_isCollectionExist" '_isLibExist' "$_isLibExist";

        if [ $_isCollectionExist -eq $E_TRUE ]\
        && [ $_isAppExist        -ne $E_TRUE ]\
        && [ $_isLibExist        -ne $E_TRUE ]
        then fct-pile-gtt-out $E_TRUE  $_fct_pile_gtt_level; return $E_TRUE;
        else fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;
        fi
    }


    #function isCollectionExist( '_collectionPathRel[.sh]' )
    # Verifie l'existance du fichier collection donné en param et le rend lisible et executable si neccessaire
    # renvoie E_TRUE si le parametre est un fichier de collection lisible et executable
    # sinon $E_INODE_NOT_EXIST, $E_INODE_NOT_R, E_INODE_NOT_X
    # Ajoute automatiquement l'extention .sh lors du test
    function isCollectionExist() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -eq 0 ]; then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;fi
        local _PathRel="$1"

        # Ajoute l'extention .sh si neccessaire pour le test
        if [ "${_PathRel##*.}" != 'sh' ]
        then
            _PathRel="${_PathRel}.sh";
        fi

        local _Path="$COLLECTIONS_REP/$_PathRel"


        titre3_D "le fichier '$_Path' existe t'il?" 3;
        if [ -f "$_Path"  ]
        then
            echo-D "Oui, le fichier existe." 3;
        else            
            echo-D "Non. le fichier n'existe pas" 3;
            fct-pile-gtt-out $E_INODE_NOT_EXIST $_fct_pile_gtt_level; return $E_INODE_NOT_EXIST;
            fi

        titre3_D "le fichier est il readable?" 3;
        if [ -r "$_Path"  ]
        then
            echo-D "Oui, le fichier est lisible" 3;
        else
            echo-D "Non, le fichier n'est pas lisible" 3;
            if chmod +r "$_Path"
            then
                echo-D "Le fichier a été fixé à lisible (+r)" 3;
            else
                erreurs-pile-add-echo "Le fichier '$_Path' ne peut être fixé à lisible (+r)"
                fct-pile-gtt-out $E_INODE_NOT_EXIST $_fct_pile_gtt_level; return $E_INODE_NOT_EXIST;
            fi
        fi

        titre3_D "le fichier est il executable?" 3;
        if [ -x "$_Path"  ]
        then
            echo-D "Oui, le fichier est excutable." 3;
        else
            echo-D "Non, le fichier n'est pas executable" 3;
            if chmod +x "$_Path"
            then
                echo-D "Oui, le fichier est maintenant executable." 3;
            else
                erreurs-pile-add-echo "Le fichier '$_Path' ne peut être fixé à executable (+x).":
                fct-pile-gtt-out $E_INODE_NOT_X $_fct_pile_gtt_level; return $E_INODE_NOT_X;
                fi
        fi

        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }

    function msgCollectionFail() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $isCollectionFail -eq 1 ]
        then
            erreurs-pile-add-echo "La collection '$collectionNom' a eu une erreur critique. Les librairies de cette collection sont désactivées.";
            fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;
        fi

       fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }

    function setCollectionFail() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        local -i _is=${1:-1};
        isCollectionFail=$_is;
        display-vars-D 'isCollectionFail' "$isCollectionFail";

        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }
#


##########################################
# TABLEAU DES COLLECTIONS DEJA EXECUTÉES # 
##########################################
    function showCollectionExec() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        local _datas='';
        for key in "${!collectionExecTbl[@]}"
        do
            _datas+=", '$key':'${collectionExecTbl[$key]}' ";
        done
        display-tableau-datas "collectionExecTbl[@]" "${_datas#?}"; # supppresion du 1er car(',')

        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }


    # Indique que la collection a déjà été executée (ou non).
    # $1: nom de la lib
    # $2(option):
    # 0: La collectiion n'a jamais été executé
    # 1: La collection adéjà été executé
    function setCollectionExec() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -eq 0 ];then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;fi

        local   _collectionNom="$1";    # requis
        local -i _value="${2:-1}";      # setter a 1 si non précisé
        collectionExecTbl["$_collectionNom"]=$_value;

        fct-pile-gtt-out $_value $_fct_pile_gtt_level; return $_value;
    }

    # renvoie $E_TRUE (0)  si la collection a deja été executé
    # renvoie $E_FALSE(97) si la collection n'a jamais été executé
    # $1: nom de la collection
    function isCollectionExec() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -ne 1 ];then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;fi

        local   _collectionNom="$1";         # requis
        if [ ${collectionExecTbl["$_collectionNom"]:-0} -eq 0 ]
        then fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;
        else fct-pile-gtt-out $E_TRUE  $_fct_pile_gtt_level; return $E_TRUE;
        fi
    }
#