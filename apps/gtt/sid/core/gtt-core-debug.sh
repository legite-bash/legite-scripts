#echo "$BASH_SOURCE"; # echo-D pas encore chargé

# gtt.sh 
# date de création    : ?
# date de modification: 2024.09.28
# Description: 


##########################
# obj: debug-gtt-level-* #
##########################
    declare -i DEBUG_GTT_LEVEL_DEFAUT=3;    # Niveau par défaut du niveau de D (0:Afficher tout)
    declare -i debug_gtt_level=0;           # niveau d'affichage de debug (demandé par l'utilisateur -D)
    declare -i debug_gtt_level_bak=0;       # sauvegarde de debug_gtt_level saveDebugGTTLevel()/restaureDebugGTTLevel()

    #declare -i fctIn_lvl_DEFAUT=$DEBUG_GTT_LEVEL_DEFAUT;

    function debug-gtt-level-init() {
        debug_gtt_level=0;
        debug_gtt_level_bak=0;
        return 0;
    }

    function debug-gtt-level-display-vars() {
        display-vars 'DEBUG_GTT_LEVEL_DEFAUT' "$DEBUG_GTT_LEVEL_DEFAUT" 'debug_gtt_level' "$debug_gtt_level" 'debug_gtt_level_bak' "$debug_gtt_level_bak";
        return 0;
    }

    # savegarde l'ancienne valeur et set la nouvelle avec $1
    function debug-gtt-level-save() {
        if [ $# -eq 0 ];then erreurs-pile-add-echo "${FUNCNAME[0]}: neccessite un parametre";return $E_ARG_REQUIRE; fi
        debug_gtt_level_bak=$debug_gtt_level;
        debug_gtt_level=$1;
        return 0;
    }

    function debug-gtt-level-switch() {
        local -i _debug_gtt_levelAncien=debug_gtt_level;
        debug_gtt_level=$debug_gtt_level_bak;
        debug_gtt_level_bak=$_debug_gtt_levelAncien;  # devient l'ancien debug_gtt_level_bak
        return 0;
    }

    function debug-gtt-level-restaure() {
        debug_gtt_level=$debug_gtt_level_bak;
        return 0;
    }
#


##########################
# obj: debug-app-level-* #
##########################
    declare -i DEBUG_APP_LEVEL_DEFAUT=3;    # Niveau par défaut du niveau de D(0:Afficher tout)
    declare -i debug_app_level=0;       # niveau d'affichage de debug (demandé par l'utilisateur)
    #local  -i _debug_app_level=0;          # niveau local de debug qui sera afficher si <= debug_app_level)
    declare -i debug_app_level_bak=0;       # sauvegarde de debug_app_level saveDebugLevel()/restaureDebugLevel()

    function debug-app-level-init() {
        debug_app_level=0;
        debug_app_level_bak=0;
        return 0;
    }

    function debug-app-level-display-vars() {
        display-vars 'DEBUG_APP_LEVEL_DEFAUT' "$DEBUG_APP_LEVEL_DEFAUT" 'debug_app_level' "$debug_app_level" 'debug_app_level_bak' "$debug_app_level_bak";
        return 0;
    }

    function debug-app-level-save() {
        if [ $# -eq 0 ];then erreurs-pile-add-echo "${FUNCNAME[0]}: neccessite un parametre";return $E_ARG_REQUIRE; fi
        debug_app_level_bak=$debug_app_level;
        debug_app_level=$1;
        return 0;
    }

    function debug-app-level-switch() {
        local -i _debug_app_levelAncien=debug_app_level;
        debug_app_level=$debug_app_level_bak;
        debug_app_level_bak=$_debug_app_levelAncien;  #devient l'ancien debug_app_level_bak
        return 0;
    }

    function debug-app-level-restaure() {
        debug_app_level=$debug_app_level_bak;
        return 0;
    }
#


##############################
# obj: debug-verbose-level-* #
##############################
    declare -i DEBUT_VERBOSE_LEVEL_DEFAUT=3;       # Niveau par défaut du niveau de v(0:Afficher tout)
    declare -i debug_verbose_level=0;              # niveau d'affichage de verbose (demandé par l'utilisateur)
    #local  -i _debug_verbose_level=0;             # niveau local de verbose qui sera afficher debug_verbose_level => $i)
    declare -i debug_verbose_level_bak=0;           # sauvegarde de debug_verbose_level savedebug_verbose_level()/restauredebug_verbose_level()

    function debug-verbose-level-init() {
        debug_verbose_level=0;
        debug_verbose_level_bak=0;
        return 0;
    }

    function debug-verbose-level-display-vars() {
        display-vars 'DEBUG_APP_LEVEL_DEFAUT' "$DEBUG_APP_LEVEL_DEFAUT" 'debug_app_level' "$debug_app_level" 'debug_app_level_bak' "$debug_app_level_bak";
        return 0;
    }

    function debug-verbose-level-save() {
        if [ $# -eq 0 ];then erreurs-pile-add-echo "${FUNCNAME[0]}: neccessite un parametre";return $E_ARG_REQUIRE; fi
        debug_verbose_level_bak=$debug_verbose_level;
        debug_verbose_level=$1;
        return 0;
    }

    function debug-verbose-level-switch() {
        local -i _verbose_levelAncien=debug_verbose_level;
        debug_verbose_level=$debug_verbose_level_bak;
        debug_verbose_level_bak=$_verbose_levelAncien;  #devient l'ancien debug_verbose_level_bak
        return 0;
    }

    function debug-verbose-level-restaure() {
        debug_verbose_level=$debug_verbose_level_bak;
        return 0;
    }
#


#######################################
#               eval-*                #
#######################################

    ##########
    # EVAL-x #
    ##########
    # Affiche jamais
    # execute sous condition
        #eval-d( 'code' [_debug_app_level] )
        # si la consigne demandé (debug_app_level en >= à la consigne de la fonction (_debug_app_level)
        function eval-d() {
            if [ $# -eq 0 ];then return $E_ARG_REQUIRE; fi
            local _cmd="$1";
            local -i _debug_app_level=${2:-$DEBUG_APP_LEVEL_DEFAUT};

            #display-vars '_debug_app_level' "$_debug_app_level" 'debug_app_level' "$debug_app_level" '_cmd' "$_cmd";
            if [ $debug_app_level -ge $_debug_app_level ]
            then
                eval "$_cmd";
            fi
            erreur-set;          # initialise erreur_no avec $?
            return $erreur_no;
        }
    #


    #############
    # eval-echo #
    #############
    # Affiche toujours
    # execute toujours

        #eval-echo('cmd' [_preTxt] [_postTxt])
        function eval-echo() {
            if [ $# -eq 0 ];then return $E_ARG_REQUIRE; fi

            local _cmd="$1";
            local _preTxt="${2:-""}";                  # pré texte: $LINENO
            local _postTxt="${3:-""}";

            echo "${COLOR_DEBUG}$_preTxt$COLOR_CMD$_cmd$COLOR_NORMAL$_postTxt";
            #display-vars '_cmd' "$_cmd" '_preTxt' "$_preTxt" '_postTxt' "$_postTxt";
            eval "$_cmd";
            erreur-set;
            return $erreur_no;
        }
    #


    ###############
    # eval-echo-x #
    ###############
    # affiche sous condition
    # execute toujours

        #eval-echo-D( 'cmd'[_debug_gtt_level] [_preTxt] [_postTxt])
        # si la consigne demandé (debug_gtt_level en >= à la consigne de la fonction (_debug_gtt_level)
        function eval-echo-D() {
            if [ $# -eq 0 ];then return $E_ARG_REQUIRE; fi
            local _cmd="$1";
            local -i _debug_gtt_level=${2:-$DEBUG_GTT_LEVEL_DEFAUT};
            local _preTxt="${3:-""}";                  # pré texte: $LINENO
            local _postTxt="${4:-""}";

            display-vars 'DEBUG_GTT_LEVEL_DEFAUT' "$DEBUG_GTT_LEVEL_DEFAUT" '_debug_gtt_level' "$_debug_gtt_level" 'debug_gtt_level' "$debug_gtt_level" '_cmd' "$_cmd" '_preTxt' "$_preTxt" '_postTxt' "$_postTxt";
            if [ $debug_gtt_level -ge $_debug_gtt_level ]
            then
                echo "${COLOR_DEBUG}$_preTxt$COLOR_CMD$_cmd$COLOR_NORMAL$_postTxt";
            fi
            eval "$_cmd";
            erreur-set;
            return $erreur_no;

        }


        #eval-echo-d('cmd' [_debug_app_level] [_preTxt] [_postTxt])
        # si la consigne demandé (debug_app_level en >= à la consigne de la fonction (_debug_app_level)
        function eval-echo-d() {
            if [ $# -eq 0 ];then return $E_ARG_REQUIRE; fi
            local _cmd="$1";
            local -i _debug_app_level=${2:-$DEBUG_APP_LEVEL_DEFAUT};
            local _preTxt="${3:-""}";                  # pré texte: $LINENO
            local _postTxt="${4:-""}";

            #display-vars '_debug_app_level' "$_debug_app_level" 'debug_app_level' "$debug_app_level" '_cmd' "$_cmd" '_preTxt' "$_preTxt" '_postTxt' "$_postTxt";
            if [ $debug_app_level -ge $_debug_app_level ]
            then
                echo "${COLOR_DEBUG}$_preTxt$COLOR_CMD$_cmd$COLOR_NORMAL$_postTxt";
            fi
            eval "$_cmd";
            erreur-set;
            return $erreur_no;

        }

        #eval-echo-v( 'cmd' [_debug_verbose_level] [_preTxt] [_postTxt])
        # si la consigne demandé (debug_verbose_level en >= à la consigne de la fonction (_debug_verbose_level)
        function eval-echo-v() {
            if [ $# -eq 0 ];then return $E_ARG_REQUIRE; fi
            local _cmd="$1";
            local -i _debug_verbose_level=${2:-$DEBUT_VERBOSE_LEVEL_DEFAUT};
            local _preTxt="${3:-""}";                  # pré texte: $LINENO
            local _postTxt="${4:-""}";

            if [ $debug_verbose_level -ge $_debug_verbose_level ]
            then
                echo "${COLOR_DEBUG}$_preTxt$COLOR_CMD$_cmd$COLOR_NORMAL$_postTxt";
            fi
            eval "$_cmd";
            erreur-set;
            return $erreur_no;

        }
    #


    #################
    # eval-x-echo-x #
    #################
    # affiche sous condition
    # execute sous condition

        #eval-echo-d('cmd' [_debug_app_level] [_preTxt] [_postTxt])
        # si la consigne demandé (debug_app_level en >= à la consigne de la fonction (_debug_app_level)
        function eval-d-echo-d() {
            if [ $# -eq 0 ];then return $E_ARG_REQUIRE; fi
            local _cmd="$1";
            local -i _debug_app_level=${2:-$DEBUG_APP_LEVEL_DEFAUT};
            local _preTxt="${3:-""}";                  # pré texte: $LINENO
            local _postTxt="${4:-""}";

            #display-vars '_debug_app_level' "$_debug_app_level" 'debug_app_level' "$debug_app_level" '_cmd' "$_cmd" '_preTxt' "$_preTxt" '_postTxt' "$_postTxt";
            if [ $debug_app_level -ge $_debug_app_level ]
            then
                echo "${COLOR_DEBUG}$_preTxt$COLOR_CMD$_cmd$COLOR_NORMAL$_postTxt";
                eval "$_cmd";
            fi
            erreur-set;
            return $erreur_no;

        }
#


#######################################
#               echo-*                #
#######################################
    #echo-D( 'txt' _debug_gtt_level 'pre' 'post')
    function echo-D(){
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE; fi

        local    _txt="$1";
        local -i _debug_gtt_level=${2:-$DEBUG_GTT_LEVEL_DEFAUT};
        #display-vars '_debug_gtt_level' "$_debug_gtt_level";
        if [ $debug_gtt_level  -eq 0 ];then return 0; fi
        if [ $_debug_gtt_level -eq 0 ];then return 0; fi
        if [ $debug_gtt_level  -ge $_debug_gtt_level ]
        then
            local _pre="${3:-""}";
            local _post="${4:-""}";
            echo -e "$_pre$_txt$_post";
        fi
        return 0;
    }

    function echo-d(){
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE; fi

        local    _txt="$1";
        local -i _debug_app_level=${2:-$DEBUG_APP_LEVEL_DEFAUT};

        if [ $debug_app_level  -eq 0 ];then return 0; fi
        if [ $_debug_app_level -eq 0 ];then return 0; fi
        if [ $debug_app_level  -ge $_debug_app_level ]
        then
            local _pre="${3:-""}";
            local _post="${4:-""}";
            echo -e "$_pre$_txt$_post";
        fi
        return 0;
    }

    function echo-v(){
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE; fi

        local    _txt="$1";
        local -i _debug_verbose_level=${2:-$DEBUT_VERBOSE_LEVEL_DEFAUT};

        if [ $debug_verbose_level  -eq 0 ];then return 0; fi
        if [ $_debug_verbose_level -eq 0 ];then return 0; fi
        if [ $debug_verbose_level  -ge $_debug_verbose_level ]
        then
            local _pre="${3:-""}";
            local _post="${4:-""}";
            echo -e "$_pre$_txt$_post";
        fi
        return 0;
    }
#


#######################################
#            display-*                #
#######################################
    #################
    # display-vars-* #
    #################
        #display-vars "nom" "val"  ["nom" "val"] [...]
        function display-vars() {
            local out='';
            if [ $# -lt 2 ];then  return $E_ARG_REQUIRE; fi

            while [ $# -gt 1 ]
            do
                out="$out$COLOR_INFO$1:$COLOR_NORMAL'$2'\t";
                shift 2;
            done
            echo -e "$out";
            return 0;
        }

        # display-var-not_null "nom" "val" 
        # return E_FALSE: si null, E_TRUE si non null
        function display-var-not_null() {
            if [ $# -lt 2   ];then  return $E_ARG_REQUIRE; fi
            if [ "$2" = ''  ]; then return $E_FALSE; fi
            if [ "$2" = '0' ]; then return $E_FALSE; fi
            #if [ "$2" =  0  ]; then return $E_FALSE; fi
            display-vars "$1" "$2";
            return $?;
        }

        
        function display-vars-D() {
            if [ $# -lt 3 ];then return $E_ARG_REQUIRE; fi
            local -i _debug_gtt_level=$1;
            shift;
            if [ $_debug_gtt_level -eq 0 ];then return 0; fi

            if [ $debug_gtt_level -ge $_debug_gtt_level ]
            then
                display-vars "$@";
            fi
            return $?;
        }


        function display-vars-d() {
            if [ $# -lt 3 ];then return $E_ARG_REQUIRE; fi
            local -i _debug_app_level=$1;
            shift;
            if [ $_debug_app_level -eq 0 ];then return 0; fi

            if [ $debug_app_level -ge $_debug_app_level ]
            then
                display-vars "$@";
            fi
            return $?;
        }
    #


    #####################
    # display-tableau-* #
    #####################
        # clear; gtt.sh --show-libsLevel2 apps/gtt/sid/tests/core-debug/test-obj-display-tableau.sh


        #########################
        # display-tableau-datas #
        #########################

        # clear; gtt.sh apps/gtt/sid/tests/core-debug/test-obj-display-tableau.sh test-fct-display-tableau-datas
        # 
        #function display-tableau-datas( "nom_du_tableau" "${tableau[@]}" )
        function display-tableau-datas() {
            if [ $# -lt 2 ];then  fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE; fi
            local _tableau_nom="$1";
            local _tableau_datas="$2";
            _tableau_datas="${_tableau_datas#,}"; # supppresion du 1er car(',')
            echo -e "${COLOR_INFO}${_tableau_nom}[@]=(${COLOR_NORMAL}$2${COLOR_INFO})${COLOR_NORMAL}";
            return 0;
        }

        
        #####################
        # display-tableau-a #
        #####################
        # Afficher sous forme ['key']: 'value'
        function display-tableau-a-format-key-data() {
            if [ $# -lt 2 ];then  fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE; fi
            local _tableau_nom="$1";
            shift
            local -i _arg_nb=$#;
            local -i _arg_no=1;
            local -i _key_no=0;
            local _out="";
            display-vars '_arg_no' "$_arg_no" '/_arg_nb' "$_arg_nb" '_key_no' "$_key_no";
            while true
            do
                if [ $_arg_no -gt $_arg_nb ]; then break; fi

                _key_no=$((_arg_no - 1));
                _out+=", ['$_key_no']='$1'";
                display-vars '_arg_no' "$_arg_no" '/_arg_nb' "$_arg_nb"  '_key_no' "$_key_no" '$1' "$1";

                ((_arg_no++))
                shift;
            done
            echo -e "${COLOR_INFO}${_tableau_nom}[@]=(${COLOR_NORMAL}$_out${COLOR_INFO})${COLOR_NORMAL}";
            return 0;
        }


    # Pas de méthode connue!
    function display-tableau-A-format-key-data() {
        #for key in "${!_tableau_A[@]}"
        #do
        #    _datas+=", '$key':'${_tableau_A[$key]}' ";
        #done

        return 0;
    }


        # affiche les champs d'un tableau
        # clear; gtt.sh apps/gtt/sid/tests/core-debug/test-obj-display-tableau.sh test-fct-display-tableau-a
        # function display-tableau 'nom_du_tableau' "${tableau[@]}" de type numérique
        # Lire un élément dans un tableau_a": display-tableau-a '${_tableau_a[0]}'  "${_tableau_a[0]}";
        function display-tableau-a() {
            local -i _fct_pile_gtt_level=5;
            fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

            if [ $# -eq 0 ];then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE; fi
            if [ $# -eq 1 ]
            then
                echo  ${COLOR_INFO}"$1[0]"${COLOR_NORMAL};
                fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
            fi
            if [ -z  "$2" ] # tableau vide
            then
                echo  ${COLOR_INFO}"$1=( ${COLOR_NORMAL}''${COLOR_INFO} )${COLOR_NORMAL}";
                fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
            fi


            local _tableau_nom="$1"; shift 1;
            local -i _tableau_nb=$#;        # Apres le shift du nom mais avant ceux des données

            local _out="";                  # indiquer le nombre d'argument. Correspond au nombre d'indice dans le tableau
            for _value in "$@"              # ici c'est bien "$@"
            do
                _out+="'$1' ";
                shift 1;
            done
            echo  ${COLOR_INFO}"${_tableau_nom}[$_tableau_nb]=(${COLOR_NORMAL}${_out}${COLOR_INFO})"${COLOR_NORMAL};

            fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
        }


        #####################
        # display-tableau-A #
        #####################

        # affiche les champs d'un tableau associatif
        # clear; gtt.sh apps/gtt/sid/tests/core-debug/test-obj-display-tableau.sh test-fct-display-tableau-A
        # Lire un élément dans un tableau_a": il faut passer par le mécanisme for
        #function display-tableau-A 'nom_du_tableau' "${tableau[$@]}" de type associatif" 
        function display-tableau-A() {
            local -i _fct_pile_gtt_level=2;
            fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

            if [ $# -lt 2 ];then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE; fi

            local _tableau_nom="$1"; shift 1;
            local _out='';
            for _value in "$@"                  # ici c'est bien "$@"
            do
                _out+="'$1' ";
                shift 1;
            done
            echo  ${COLOR_INFO}"${_tableau_nom}=(${COLOR_NORMAL}$_out${COLOR_INFO})"${COLOR_NORMAL};

            fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
        }
#


##########
# divers #
##########
    # Calcul et Affiche la durée d'execution du script
    function SCRIPT-DUREE-show() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        #display-vars 'isShowDuree' "$isShowDuree";
        declare -gr -i SCRIPT_FIN=$(date +%s);
        declare -gr -i SCRIPT_DUREE=$((SCRIPT_FIN-SCRIPT_DEBUT));
        if $isShowDuree
        then
            local _s='';
            if [ $SCRIPT_DUREE -gt 1 ];then _s='s'; fi
            echo "$SCRIPT_DUREE seconde${_s}";
        fi

        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }

    #Affiche la durée d'execution du script
    function SCRIPT-DUREE-display-vars() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        display-vars 'SCRIPT_DEBUT' "$SCRIPT_DEBUT";
        display-vars 'SCRIPT_FIN  ' "$SCRIPT_FIN";
        display-vars 'SCRIPT_DUREE' "$SCRIPT_DUREE";

        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }
#
