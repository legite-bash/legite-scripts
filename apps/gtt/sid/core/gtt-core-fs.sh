echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.22
# Description: fonctions disques utilisées par gtt.sh en interne
# Les autres fonctions disques sont dans: apps/linux/disques/_ini.cfg.sh


#############
# - fs-mk - #
#############
    #createLink ('inodeexistant'  'lien_à_crée')
    function fs-mk-link() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        local _src="${1:-""}";	
        if [ "$_src" = '' ];then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;fi

        local _dest="${2:-"$_src"}";
        local _src_pathname="$_src";
        local _dest_pathname="$_dest";

        if [ ! -e "$_src_pathname" ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}(): La source $_src_pathname est introuvable.";
            fct-pile-gtt-out $E_INODE_NOT_EXIST $_fct_pile_gtt_level; return $E_INODE_NOT_EXIST;
        fi
        if [ -f "$_dest_pathname" ]
        then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;
        fi

        # - Creation du lien - #
        ln -s "$_src_pathname" "$_dest_pathname";
        local _err=$?

        # - Affichage du retour - #
        if [ $_err -eq 0 ]
        then notifs-pile-add-echo  "Création du lien '$_dest_pathname' qui pointe vers '$_src_pathname'";
        else erreurs-pile-add-echo "Erreur lors de la création du lien '$_dest_pathname' qui pointe vers '$_src_pathname'";
             fct-pile-gtt-out $_err $_fct_pile_gtt_level; return $_err;
        fi

        fct-pile-gtt-out 0 $_fct_pile_gtt_level; return 0;
    }

    function fs-mk-rep() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -eq 0 ]
        then
            titreWarn "${FUNCNAME[0]}():Le nombre d'argument requis est: $#/"
            fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;
        fi
        if [ -d "$1" ]
        then titreInfo "Le repertoire '$1' existe déjà.";
        else eval-echo "mkdir -p \"$1\"";
        fi
    
        fct-pile-gtt-out $erreur_no $_fct_pile_gtt_level; return $erreur_no;
    }
#



##############
# fs-ls-sh() #
##############
# Affiche les fichiers *.sh du repertoire $1
# Fonction interne à gtt
# Utilisé par: --show-collections, --show-apps
# function fs-ls-sh( 'repertoire' 'repertoire_relatif')
    function fs-ls-sh() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -eq 0 ]; then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE; fi

        local _repActuel="${1}";    # Seulement le nom du repertoire
        local _repRel="${2:-""}";   # repertoire relatif (utile pour l'affichege)
        _repRel="${_repRel%/}";     # on supprime le dernier slash

        local _repNomActuel="${_repActuel##*/}";    # Seulement le nom du repertoire
        #display-vars '_repRel' "$_repRel" '_repNomActuel' "$_repNomActuel";

        # afficher le nom du repertoire  uniquement si different du relatif
        if [ "$_repNomActuel" = "$_repRel" ]
        then
            echo -n "$COLOR_INFO${_repRel}:$COLOR_NORMAL"
        else
            echo -n "$COLOR_INFO${_repRel}/${_repNomActuel}:$COLOR_NORMAL";
        fi

        # _fichiersListe tableau contenant que les noms de fichiers 
        local -a _fichiersListe=$(ls -1 --almost-all --ignore-backups  "$_repActuel"/*.sh  2>/dev/null );
        local -i fichiersLocalNb=${#_fichiersListe[@]};
        
        if [ "${_fichiersListe[*]}" = "" ]
        then
            echo ''; # Termine la ligne en la laissant vide
        else

            # -- Affichage en ligne des noms des fichiers seuls -- #
            local _fichiersCumul='';
            local _fichierNomTmp='';
            for _fichierDuRep in $(sort <<< "${_fichiersListe[*]}")
            do
                #display-vars "_fichierDuRep" "$_fichierDuRep"
                if $isTrapCAsk;then break; fi
                _fichierNomTmp="${_fichierDuRep##*/}";      # nom.ext
                _fichiersCumul+=" ${_fichierNomTmp%.sh}";   # cumul avec suppression de l'extention
            done
            echo "$_fichiersCumul";
        fi  # liste de fichiers non vide

        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }
#



#######################
# fs-disk-free-show() #
# old: fs-disk-free-show() #
#######################
    # fs-disk-free-show 'rep1 'rep2' ...
    function fs-disk-free-show() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -eq 0 ];then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;fi

        titre3 'Espaces libres restants';

        for _rep in "$@"
        do
            if [ ! -e "$_rep" ];then continue;fi

            #afficher l'espace libre du repertoire donné en parametre
            local _df_result=$(df -h "$_rep" | grep /)
            _df_result=$(echo "$_df_result" | sed 's/  */ /g') # Ne laisser qu'un espace entre chaque terme
            #display-vars-D 'df_result' "$_df_result"

            local _df_support="$(echo "$_df_result"   | cut --delimiter=' ' --fields=1)"
            #display-vars-D '_df_support' "$_df_support"

            local _df_taille="$(echo "$_df_result"    | cut --delimiter=' ' --fields=2)"
            #display-vars-D '_df_taille' "$_df_taille"

            #local _df_utiliser=$(echo "$_df_result" | cut --delimiter=" " --fields=3)
            #display-vars-D '_df_utiliser' "$_df_utiliser"

            local _df_libre="$(echo "$_df_result"     | cut --delimiter=' ' --fields=4)"
            #display-vars-D '_df_libre' "$_df_libre"

            #_df_utiliserPct="$(echo "$df_result" | cut --delimiter=' '' --fields=5)""
            #display-vars-D '_df_utiliserPct' "$_df_utiliserPct"

            local _df_monterSur="$(echo "$_df_result" | cut --delimiter=' ' --fields=6)"
            #display-vars-D '_df_monterSur' "$_df_monterSur"

            #echo  "${COLOR_INFO}$_df_support${COLOR_NORMAL}(${COLOR_INFO}$_df_monterSur${COLOR_NORMAL}) Libre: ${COLOR_INFO}$_df_libre${COLOR_NORMAL}/${COLOR_INFO}$_df_taille${COLOR_NORMAL}"
            display-vars "$_df_monterSur" "$_df_support" 'Libre' "$_df_libre" 'Taille' "$_df_taille"
        done

        fct-pile-gtt-out 0 $_fct_pile_gtt_level; return 0;
    }
#

return 0;