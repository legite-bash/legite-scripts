echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

# Fichier de test: apps/gtt/sid/tests/divers/test-obj-colonnes.sh

#https://www.shellscript.sh/tips/echo/

# Certain systeme n'interprete pas le le parametre -n  de echo mais utilise a la place \c en fin de chaine
# definit les variables globales n et c selon si le systeme interprete le parametre -n  de echo
# exemple d'utilisation: echo $n "Enter your name: $c"
#if [ "$(echo -n)" = '-n' ]
#then n='';   c="\c";
#else n="-n"; c=''
#fi
#


###################
# objet: Colonnes #
###################
    declare -gi  colonnes_nb=0;                     # no de colonne en cours
    declare -gi  colonnes_no=0;                     # no de colonne en cours
    declare -ga  colonnes_entetes_values=();        # Tableau contenant le titre des colonnes
    declare -ga  colonnes_datas_values=();          # Tableau contenant le text des données d'une ligne de données
    declare -gi  colonnes_lg_defaut=50;             # longeur par defaut d'une colonnes si pas précisé
    declare -gai colonnes_lg=(0);                   # Tableau contenant les longueurs des colonnes
    declare -gri COLONNES_COLAUTO_LGMAX=100;
    declare -gi  colonnes_colAuto_lgMax=$COLONNES_COLAUTO_LGMAX;        # Largeur maximal de sortie
    declare -gai colonnes_colAuto_lg=(5);           # x: longueur minimal de x -x:longueur maximal de x (trunc)
    declare -gi  colonnes_colAuto_lgReel=0;         # longueur cumulé de la ligne
    declare -ga  colonnes_dir=('L');                # direction L:eft|C:enter|R:ight
    declare -ga  colonnes_sep=('|');
    declare -g   colonnes_out='/dev/stdout';
    #declare -g   colonnes_isStdout=true;           # variable interne de protection
    declare -g   colonnes_isKeepOutStdout=false;    # envoie une copie dans la sortie standard meme si colonnes_out!='/dev/stdout'}
    declare -g   Colonnes_isShowTail=true;          # Affiche le message tail -f

    #function colonnes_init(){
    # fonction à surcharger par l'utilisateur
    function colonnes-init(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        colonnes_nb=0;
        colonnes_no=0;
        colonnes_entetes_values=();
        colonnes_entetes_values[1]='TITRE';
        colonnes_datas_values=();
        colonnes_datas_values[1]='';
        colonnes_lg=();
        colonnes_colAuto_lgMax=$COLONNES_COLAUTO_LGMAX;
        colonnes_colAuto_lg=-1;         # Remplissage au maximum de l'espace de l'écran restant limité à $colonnes_colAuto_lgMax
        colonnes_dir=('L');
        colonnes_sep=('|');
        colonnes-setOut;
        colonnes_isKeepOutStdout=false;
        Colonnes_isShowTail=true;

        fct-pile-app-out 0; return 0;
    }

    function colonnes-display-vars(){
        display-vars     'colonnes_nb'              "$colonnes_nb";
        display-vars     'colonnes_no'              "$colonnes_no";
        display-tableau '${colonnes_entetes[@]}'   "${colonnes_entetes[@]}";
        display-tableau '${colonnes_lg[@]}'        "${colonnes_lg[@]}";
        display-vars     'colonnes_colAuto_lgMax'   "$colonnes_colAuto_lgMax";
        display-vars     'colonnes_colAuto_lg'      "$colonnes_colAuto_lg";
        display-tableau '${colonnes_dir[@]}'       "${colonnes_dir[@]}";
        display-tableau '${colonnes_sep[@]}'       "${colonnes_sep[@]}";

        display-tableau '${colonnes_entetes[@]}'   "${colonnes_entetes[@]}";
        display-tableau 'colonnes_isKeepOutStdout' "$colonnes_isKeepOutStdout";
        display-tableau 'Colonnes_isShowTail'      "$Colonnes_isShowTail";

        colonnes-setOut;
        return 0;
    }


    #function colonnes_setOut( [colonnes_out="/dev/stdout"]  ) 
    # Définit la sortie
    function colonnes-setOut(){
        colonnes_out="${1:-"/dev/stdout"}";
        #display-vars 'colonnes_out' "$colonnes_out";
        if [ $colonnes_out != '/dev/stdout' ]
        then
            if  [ -f "$colonnes_out" ]
            then
                #echo "${FUNCNAME[0]}(): Nettoyage de '$colonnes_out'";
                rm    "$colonnes_out";
                touch "$colonnes_out";
            fi
        fi
        return 0;
    }


    # Longueur de la colonne auto
    function colonnes-colAuto-lgMax(){
        colonnes_colAuto_lgMax=${1:-$COLONNES_COLAUTO_LGMAX};
    }


    function colonnes-colAuto-lg(){
        colonnes_colAuto_lg=${1:-$COLONNES_COLAUTO_LGMAX};
        if [ $colonnes_colAuto_lg -gt $colonnes_colAuto_lgMax ];then colonnes_colAuto_lg=$colonnes_colAuto_lgMax;fi
    }


    # Définit les longueurs des colonnes et calcul colAuto_*
    function colonnes-lg(){
        local -i _col_nu=0;
        for _value in "$@"
        do
            ((_col_nu++))   # On commence à 1
            #display-vars '_col_nu' "$_col_nu";
            colonnes_lg[$_col_nu]="$_value";
            #display-vars '_col_nu' "$_col_nu" '${colonnes_lg[$_col_nu]}' "${colonnes_lg[$_col_nu]}" ;
        done
        colonnes_nb=${#colonnes_lg[@]};  # initialisation globale
        #display-vars 'colonnes_nb' "$colonnes_nb";
        #display-tableau '${colonnes_lg[@]}'  "${colonnes_lg[@]}";
        
        # Calcul de col_auto
        colonnes-colAuto-lg;return 
        return 0;
    }


    function colonnes-dir(){
        
        if [ $? -eq 1 ]
        then
            colonnes_dir=();
            
            # SI une seule valeur de fournis on la copie dans le tableau
            local _col_nu=1;    # On commence à l'index [1]
            while true
            do
                #display-vars  '_col_nu'  "$_col_nu";
                colonnes_dir[$_col_nu]=( "$1" );
                ((_col_nu++));
                if [ $_col_nu -gt $colonnes_nb ];then break;fi
            done
            #display-tableau '${colonnes_dir[@]}' "${colonnes_dir[@]}" ;
            return 0;
        fi

        local -i _col_nu=0;
        for _value in "$@"
        do
            ((_col_nu++))
            colonnes_dir[$_col_nu]="$_value";
            #display-vars '_col_nu' "$_col_nu" '${colonnes_dir[$_col_nu]}' "${colonnes_dir[$_col_nu]}" ;
        done
        #display-tableau '${colonnes_dir[@]}' "${colonnes_dir[@]}" ;

        return 0;
    }

    function colonnes-sep(){
        #colonnes_nb=${#@};
        local -i _col_nu=0;
        for _value in "$@"
        do
            ((_col_nu++))
            colonnes_sep[$_col_nu]="$_value";
            #display-vars '_col_nu'  "$_col_nu" '${colonnes_sep[$_col_nu]}' "${colonnes_sep[$_col_nu]}";
        done
        return 0;
    }

    # - colonnes-entete - #
    function colonnes-entetes-values-show(){
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE;fi
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;
        colonnes-entetes-values $@;         # pas de double quote
        colonnes-entetes-show;
        fct-pile-app-out 0; return 0
    }

    function colonnes-entetes-values(){
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE;fi

        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;
    
        local -i _col_nu=0;
        #colonnes_entetes_values=();
        for _value in "$@"
        do
            ((_col_nu++))
            colonnes_entetes_values[$_col_nu]="$_value";
            #display-vars  '_col_nu'  "$_col_nu" '${colonnes_entetes_values[$_col_nu]}' "${colonnes_entetes_values[$_col_nu]}";
        done
        #display-tableau '${colonnes_entetes_values[@]}' "${colonnes_entetes_values[@]}";
        colonnes_nb=${#colonnes_entetes_values[@]};
        #display-vars 'colonnes_nb' "$colonnes_nb";

        fct-pile-app-out 0; return 0;
    }

    function colonnes-entetes-show(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        if [ "$colonnes_out" != '/dev/stdout' ] && $isColonnes_showTail;then echo "tail -f \"$colonnes_out\"";fi

        local _col_nu=1;    # On commence à l'index [1]
        while true
        do
            #display-vars  '_col_nu'  "$_col_nu";
            colonnes-entetes-show-code "$_col_nu";
            ((_col_nu++));
            if [ $_col_nu -gt $colonnes_nb ];then break;fi
        done
        echo '' >> "$colonnes_out";
        fct-pile-app-out 0; return 0;
    }

    # Affiche une entete de la ligne
    function colonnes-entetes-show-code(){
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE;fi
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local -i _col_nu=$1;
        local    _txt="${colonnes_entetes_values[$_col_nu]:-"${colonnes_entetes_values[1]}"}";                          # display-vars '_txt' "$_txt";
        local -i _txt_lg=${#_txt};
        local    _carEspaces='';
        local -i _col_lg=${colonnes_lg[$_col_nu]:-$colonnes_lg_defaut};             # display-vars '_col_lg' "$_col_lg";
        local     _dir="${colonnes_dir[$_col_nu]:-"${colonnes_dir[1]}"}";           # par défaut dir de l'index1        # display-vars '_dir' "$_dir";
        local     _sep="${colonnes_sep[$_col_nu]:-"${colonnes_sep[1]}"}";           # par défaut sep de l'index1        # display-vars '_sep' "$_sep";

        # Geston d'une longueur négative
        if [ $_col_lg -lt 0 ];then _col_lg=$((_col_lg*-1));fi

        if [ $_col_lg -gt 0 ]
        then
            # - On coupe en cas de depassement - #
            if [ $_txt_lg -gt $_col_lg ];then _txt="${_txt:0:$_col_lg}";fi
       fi
        #display-vars $LINENO:'_txt_lg' "$_txt_lg/" '_col_lg' "$_col_lg" '_txt' "$_txt" 
        
        # - centrage - #
        local espaceNb_before=$(( (_col_lg-_txt_lg)/2));
        local espaceNb_after=$((_col_lg-_txt_lg-espaceNb_before));
        local _out="$(car-duplique-n $espaceNb_before ' ')$_txt$(car-duplique-n $espaceNb_after ' ')$_sep";
        echo -ne "$_out" >> "$colonnes_out"; 
        
        # - faire une copie sur stdin - #
        if [ "$colonnes_out" != '/dev/stdout' ] && $colonnes_isKeepOutStdout; then echo -ne "$_out"; fi  

        fct-pile-app-out 0; return 0;
    }

    # - colones-datas - #
    function colonnes-datas-values-show(){
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE;fi
        colonnes-datas-values $@;         # pas de double quote
        colonnes-datas-show;
    }

    function colonnes-datas-values(){
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE;fi

        local -i _col_nu=0;
        #colonnes_datas_values=();
        for _value in "$@"
        do
            ((_col_nu++))
            colonnes_datas_values[$_col_nu]="$_value";
            #display-vars  '_col_nu'  "$_col_nu" '${colonnes_datas_values[$_col_nu]}' "${colonnes_datas_values[$_col_nu]}";
        done
        #display-tableau '${colonnes_datas_values[@]}' "${colonnes_datas_values[@]}";
        colonnes_nb=${#colonnes_datas_values[@]};
        #display-vars 'colonnes_nb' "$colonnes_nb";
        return 0;
    }

    function colonnes-datas-show(){
        if [ "$colonnes_out" != '/dev/stdout' ] && $isColonnes_showTail;then echo "tail -f \"$colonnes_out\"";fi
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _col_nu=1;    # On commence à l'index [1]
        while true
        do
            #display-vars  '_col_nu'  "$_col_nu";
            colonnes-datas-show-code "$_col_nu";
            ((_col_nu++));
            if [ $_col_nu -gt $colonnes_nb ];then break;fi
        done
        echo '' >> "$colonnes_out";
        fct-pile-app-out 0; return 0;
    }

    # Affiche une data de la ligne
    function colonnes-datas-show-code(){
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE;fi

        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;
    
        local -i _col_nu=$1;
        local    _txt="${colonnes_datas_values[$_col_nu]:-"${colonnes_datas_values[$_col_nu]}"}";   # display-vars '_txt' "$_txt";
        local -i _txt_lg=${#_txt};
        local    _carEspaces='';
        local -i _col_lg=${colonnes_lg[$_col_nu]:-$colonnes_lg_defaut};              # display-vars '_col_lg' "$_col_lg";
        local     _dir="${colonnes_dir[$_col_nu]:-"${colonnes_dir[1]}"}";            # par défaut dir de l'index1        # display-vars '_dir' "$_dir";
        local     _sep="${colonnes_sep[$_col_nu]:-"${colonnes_sep[1]}"}";            # par défaut sep de l'index1        # display-vars '_sep' "$_sep";


        # Geston d'une longueur négative
        if [ $_col_lg -lt 0 ];then _col_lg=$((_col_lg*-1));fi

        if [ $_col_lg -gt 0 ]
        then
            # - On coupe en cas de depassement - #
            if [ $_txt_lg -gt $_col_lg ];then _txt="${_txt:0:$_col_lg}";fi
       fi
        #display-vars $LINENO:'_txt_lg' "$_txt_lg/" '_col_lg' "$_col_lg" '_txt' "$_txt" 
        
        # Calcul du nombre de caractere de remplissage
        if [ $_txt_lg -lt $_col_lg ]
        then
            for (( _i=_txt_lg;_i<_col_lg;_i++))
            do
                _carEspaces+=' ';
            done
        fi

        #echo '';
        
        #display-vars '_col_lg' "$_col_lg";
        #display-vars '_txt_lg' "$_txt_lg" '${#_txt_lg}' "${#_txt_lg}";
        #display-vars '_txt' "$_txt";
        case "$_dir" in
            
            'L') _out="$_txt$_carEspaces$_sep";  ;;
            'C')
                local espaceNb_before=$(( (_col_lg-_txt_lg)/2 ));               # display-vars 'espaceNb_before' "$espaceNb_before";
                local espaceNb_after=$((_col_lg-_txt_lg-espaceNb_before));      # display-vars 'espaceNb_after' "$espaceNb_after";
                local _out="$(car-duplique-n $espaceNb_before ' ')$_txt$(car-duplique-n $espaceNb_after ' ')$_sep";
                #echo -ne "$_out"; 
                ;;
            'R') _out="$_carEspaces$_txt$_sep";  ;;
        esac
        echo -ne "$_out" >> "$colonnes_out"; 

        
        # - faire une copie sur stdin - #
        if [ "$colonnes_out" != '/dev/stdout' ] && $colonnes_isKeepOutStdout; then echo -ne "$_out"; fi  

        fct-pile-app-out 0; return 0;
    }
#




#################
# DUPLICATE CAR #
# OBSOLETE : remplacé par car-duplique-n()
#################
   
   
    # duplique-n-Car duplique $1 fois le car $2
    # $3: echo: true
    declare _g duplique_n_Car="";
    #function duplique-n-Car() {
   #     local -r _echo=${3:-true};
   #     if [ $# -lt 2 ]
   #     then
   #         erreurs-pile-add-echo "${FUNCNAME[0]}[$#/2](Nbre_de_car 'C') ($*)";
   #         return $E_ARG_REQUIRE;
   #     fi
   #     local -i _N=$1;
   #     local C="$2";
   #     duplique_n_Car='';
   #     for ((i = 0 ; i < $_N ; i++)); do duplique_n_Car+="$C";done
   #     if $_echo ;then  echo "$duplique_n_Car"*; fi
   #     return 0;
   # }
#

######
# HR #
######
    function HR(){
        car-duplique-n $terminal_largeur "${1:-"*"}";
        return 0;
    }

    function hr(){
        car-duplique-n $terminal_largeur "${2:-"*"}";
        return 0;
    }
#


##########
# DIVERS #
##########





