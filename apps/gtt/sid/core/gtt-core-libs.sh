echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


###################
#  LIB_EXEC_AUTO  #
# collection-Auto #
###################
    #function setCollectionIntegrale()  # deprecated
    function setLibExecAuto() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$isShowCollections:$*" $_fct_pile_gtt_level;

        local -i _is=${1:-$E_TRUE};
        isLibExecAuto=$_is;
        display-vars 'isLibExecAuto' "$isLibExecAuto";
        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }

    # execute la librairie $1 si $isLibExecAuto  -eq $E_TRUE
    # --libExecAuto | -Auto
    function libExecAuto() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -ne 1 ];then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE; fi

        local _libNom="$1";

        isLibCall "$_libNom";
        local _isLibCall=$?;
        #display-vars "$LINENO:  lib:$_libNom, _isLibCall" "$_isLibCall" 'isLibExecAuto' "$isLibExecAuto";

        if [[ $isLibExecAuto -eq $E_TRUE || $_isLibCall -eq $E_TRUE ]] # si la collection est en mode -auto ou que la librairie a été appéllée
        then
            execLib "$_libNom";
        fi

        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }
#


#############################
# AFFICHAGES DES LIBRAIRIES #
#############################

    # - Affiche (triés) les noms  des librairies chargées- #
    function showLibs() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $showLibsLevel = 0 ];then fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE; fi

        local librairiesTblIndex=''; # liste des index du tableau librairiesTbl (trié)

        # trie alphabetique du tableau sur la clef
        IFS=$'\n';
        librairiesTblIndex=($(sort <<< "${!librairiesTbl[*]}"));
        IFS="$IFS_DEFAULT";

        # Affichage du tableau trié
        if [ $showLibsLevel -eq 1 ]
        then
            titre0 'Librairies possibles (liste)';
            local out='';
            for value in "${librairiesTblIndex[@]}";
            do
                out+=" $value"
            done;
            echo "$out";
        elif [ $showLibsLevel -ge 2 ]
        then
            titre0 'Librairies possibles (détail)';
            for value in "${librairiesTblIndex[@]}"; # la valeur est la clef du tableau librairiesTbl
            do
                local description=${librairiesDescr["$value"]:-''}
                display-vars "$value" "$description"
            done;
        fi
        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }


    ##
    # Affiche, pour chaque applications(triés), les librairies (triées) la concernant
    # Non opérationelle, sera supprimée
    function show-apps-libs() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;


        # - Controle des accèes  - #
        if [ $isShowAppsLibs = false ];then fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE; fi


        # - Recherche des repertoires  - #
    
        # _repListe: tableau contenant les chemins COMPLET des repertoires
        local -a _repListe=$(find "$APPS_REP" -type d );  # Recherche de tous les fichiers
        

        # calcul du nombre de sous repertoires de $APPS_REP #
        local    _slashs="${APPS_REP//[^\/]}";      # on ne garde que les slashs (/)
        local -i _APPS_REP_sousRepNb=${#_slashs};

        local -i _repNb=0;    
        

        # Boucle sur repertoires triés
        for _rep in $(sort <<< "${_repListe[*]}") # NE PAS QUOTER sinon renvoi une seule chaine de caractere
        do
            if $isTrapCAsk;then return $E_CTRL_C; fi


            (( _repNb++ ))

            # ne pas afficher la 1ere ligne
            #if [ $_repNb -eq 1 ];then continue; fi
            #display-vars '_rep' "$_rep";

            # - Affiche le repertoire: forme chemin relatif - #
            local _repRel="${_rep#$APPS_REP/}";             # supprime le root
            titreInfo "$_repRel"
            #continue # for test

            # Recherche des fichiers dans le repertoire     #
            local -a _fichiersListe=$(ls -1 --almost-all --ignore-backups  "$_rep"/*.sh  2>/dev/null );
            local -i fichiersLocalNb=${#_fichiersListe[@]};


            if [ "${_fichiersListe[*]}" = "" ]
            then
                echo ''; # Termine la ligne en la laissant vide
                continue;   # pas de fichier dans le sous rep -> rep suivant
            fi


            # -- Boucle sur les fichiers du sous rep -- #
            local _fichiersCumul='';
            local _fichierName='';
            local _fichierNom='';
            local _fichierExt='';
            for _fichierActuel in $(sort <<< "${_fichiersListe[*]}")
            do
                if $isTrapCAsk;then break; fi

                # Si l'app/collection/fichier a demander SA sortie
                if $isPSPNextAsk;
                then
                    echo "$LINENO: isPSPNextAsk==$isPSPNextAsk -> continue;";
                    isPSPNextAsk=false;
                    continue;
                fi

                _fichierName="${_fichierActuel##*/}";       # nom.ext
                _fichierNom="${_fichierName%.*}";           # nom
                _fichierExt="${_fichierName##*.}";          # ext
                _fichiersCumul+=" ${_fichierName%.sh}";     # cumul avec suppression de l'extention
                display-vars '_fichierName' "$_fichierName";
                #display-vars '_fichierNom' "$_fichierNom";
                #display-vars '_fichierExt' "$_fichierExt";
                #display-vars '_fichiersCumul' "$_fichiersCumul";

                # Gestion des fichiers exlus
                case "$_fichierName" in
                    'dl.core.sh'| 'dl.sh' | 'test-dl.sh' | \
                    'gtt-vars.sh' | 'fct-recursivite.sh' | \
                    'declarations.sh' | 'read.sh' | 'variables.sh' |\
                    'gtt-core-libs.sh'\
                    )
                        continue;
                esac

                # Affiche les librairies du fichier
                local librairiesTblIndex=''; # liste des index du tableau librairiesTbl (trié)
                IFS=$'\n';
                librairiesTblIndex=($(sort <<< "${!librairiesTbl[*]}"));
                IFS="$IFS_DEFAULT";

                # Vider le tableau des librairies #
                librairiesTbl=();


                # Charger le fichier 
                . "${_fichierActuel}"

                # si pas de librairie dans le fichier -> next fichier
                if [ ${#librairiesTblIndex[@]} -eq 0 ];then continue; fi

                # Debut de l'affichage #
                titre3 "$_fichierActuel";

                # -- Boucle sur les librairies du fichier actuel -- #
                #echo ${librairiesTblIndex[@]}
                for _libActuel in "${librairiesTblIndex[@]}";# la valeur est la clef du tableau librairiesTbl
                do
                    local _description="${librairiesDescr["$_libActuel"]:-""}";
                    display-vars "$_libActuel" "$_description"
                done;

            done
        done
        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }


##########################
# GESTION DES LIBRAIRIES #
##########################
    # renvoie:
    # E_ARG_REQUIRE: si pas d'argument
    # $E_FALSE si lib == ""
    # $E_TRUE  si lib existe
    # $E_FALSE si lib non existe
    function isLibExist() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -eq 0 ]; then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE; fi
        #local _lib="$1";
        #display-vars '1' "$1";
        local _lib="${1##*/}";        # supprimmer les sous rep (domaine)
        #echo-D 'Est ce que la lib existe?' "$_lib";
        if [[ "$_lib" == '' ]];then fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE; fi
        for key in "${!librairiesTbl[@]}";
        do
            if [[ "$key" == "$_lib" ]]; then fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE; fi
        done;
        #echo 'Non la lib existes existe pas';
        
        fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;
    }


    # renvoie:
    # E_ARG_REQUIRE: si pas d'argument
    # $E_TRUE  si la librairie est appellé via PSP_calls (et existe)
    # $E_FALSE si lib non appellé
    function isLibCall() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -ne 1 ];then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE; fi
        
        local _lib="$1";
        for _value in "${PSP_calls[@]}";
        do
            if [ "$_value" = "$_lib" ]; then fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE; fi
        done;

        fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;
    }


    # addLibrairie( 'libNom' ['description'] ['libCmd'=libNom])
    function addLibrairie() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -eq 0 ]
        then
            erreurs-pile-add-echo "${BASH_SOURCE[0]}:$LINENO:${FUNCNAME[0]}('libNom' ['description'] ['libCmd'=libNom]) ($*). Appellé par ${FUNCNAME[1]}()";
            fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;
        fi

        local _libNom="$1";
        if [[ "$_libNom" == '' ]];then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE; fi
        local _libDescr=${2:-''};
        local _libCmd="${3:-"$_libNom"}";
        librairiesTbl["$_libNom"]="$_libCmd";
        librairiesDescr["$_libNom"]="$_libDescr";
        librairies_sources["$_libNom"]="${BASH_SOURCE[1]}";
        setLibExec "$_libNom" 0;           # La lib n'a pas encore été appelllée.
        echo-D "Ajout de la librairie '$_libNom'." 3;

        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }


    #function exeLib ( 'libNom' )
    # execute la librairie si existante (et si pas deja executé)
    # retour:
    # $E_TRUE : la librairie existe et a été executée
    # $E_FALSE: ereur
    function execLib() {
        if $isTrapCAsk;then return $E_CTRL_C; fi

        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -eq 0 ]
        then
            erreurs-pile-add-echo "${BASH_SOURCE[0]}:$LINENO:${FUNCNAME[0]}( 'libNom' ) ($*). Appellé par ${FUNCNAME[1]}()";
            fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;
        fi

        local _libNom="${1##*/}";        # supprimer les sous rep (domaine)
        #local _libNom="$1";

        if [ $isCollectionFail -eq 1 ]
        then
            erreurs-pile-add-echoV  "La librairie  '$_libNom' est désactivée du à une erreur de la collection";
            fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;
        fi

        if isLibExec "$_libNom"
        then
            echo-D "La librairie  '$_libNom' a déjà été executée" 3;
            fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;
        else
            echo-D "La librairie '$_libNom' n'a jamais été exécutée." 3;
        fi
        
        if isLibExist "$_libNom"
        then
            titreLib_D "$_libNom(${FUNCNAME[0]})" 1;
            eval "${librairiesTbl["$_libNom"]}"; #eval eval du bash et non eval-*
            setLibExec "$_libNom" 1;
            fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
        fi

        fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;
    }
#


#########################################
# TABLEAU DES LIBRAIRIES DEJA EXECUTÉES # 
#########################################
    function showLibrairieExec(){
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        local _datas='';
        for key in "${!libExecTbl[@]}"
        do
            _datas+=", '$key':'${libExecTbl[$key]}' ";
        done
        display-tableau-datas "libExecTbl[@]" "${_datas#?}"; # supppresion du 1er car(',')

        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }


    # Indique que la lib a déjà été executée (ou non).
    # $1: nom de la lib
    # $2(option):
    # 0: n'a jamais été executé
    # 1: a déjà été executé
    # renvoie value

    function setLibExec(){
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -eq 0 ];then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE; fi

        local   _libNom="$1";           # requis
        local -i _value="${2:-1}";     # setter a 1 si non précisé
        libExecTbl["$_libNom"]=$_value;
        #echo-D "isLibExec '$_libNom': ${libExecTbl["$_libNom"]}" 2;

        fct-pile-gtt-out $_value $_fct_pile_gtt_level; return $_value;
    }

    # renvoie $E_TRUE (0)  si la librairie a deja été executé
    # renvoie $E_FALSE(97) si la librairie à jamais étéexecuté
    # ATTENTION: c'est l'inverse de setLibExec()
    # $1: nom de la lib
    function isLibExec(){
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -ne 1 ];then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE; fi

        local   _libNom="$1";         # requis
        if [ -z "$_libNom" ];then fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE; fi
        #display-vars '_libNom' "$_libNom";
        #display-vars '${libExecTbl["$_libNom"]' "${libExecTbl["$_libNom"]}";

        if [[ ${libExecTbl["$_libNom"]:-0} -eq 0 ]];then fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE; fi

        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }

    #isLibOnly ( 'nom' )            # collection ou lib
    # renvoie $E_TRUE si l'argement est une librairie MAIS PAS une collection
    function isLibOnly(){
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -ne 1 ];then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE; fi
        local _collectionNom="$1";
        isCollectionExist "$_collectionNom";    local -i _isCollectionExist=$?;
        isLibExist "$_collectionNom";           local -i _isLibExist=$?;
    
        #display-vars '_collectionNom' "$_collectionNom" '_isCollectionExist' "$_isCollectionExist" '_isLibExist' "$_isLibExist";

        if [ $_isCollectionExist -ne $E_TRUE ] && [ $_isLibExist -eq $E_TRUE ]
        then fct-pile-gtt-out $E_TRUE  $_fct_pile_gtt_level; return $E_TRUE;
        else fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;
        fi
    }
#

return 0;