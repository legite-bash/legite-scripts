#echo-D "${BASH_SOURCE[0]}" 1;

# gtt-vars.sh 
# date de création    : ?
# date de modification: 2024.08.19
# Description: 



# déclaré dans gtt.sh:
#declare -r SCRIPT_PATH="$(realpath "$0")";  # chemin  absolu complet du script rep + nom
#declare -r SCRIPT_REP="${SCRIPT_PATH%/*}";  # Repertoire absolu du script (pas de slash de fin)

declare -r SCRIPT_FILENAME="${0##*/}";          # /path/nom.ext.ext -> nom.ext
declare -r SCRIPT_NAME="${SCRIPT_FILENAME%.*}"; # nom.pasext.ext ->             # Uniquement le nom
declare -r SCRIPT_EXT="${SCRIPT_FILENAME##*.}"; # nom.pasext.ext -> ext         # Uniquement l'extention
#

###########
# app:gtt #
###########
    # déclaré avant
    #declare -r GTT_VERSION_SELECT=${GTT_VERSION_SELECT:-'sid'};
    #declare -r GTT_REP="$APPS_REP/gtt/$GTT_VERSION_SELECT";


##################
# TMP_ROOT - LOG #
##################
# tmp-*
# log-*


#############
# VARIABLES #
#  GLOBALES #
#############

    # --- variables systemes --- #
    # http://redsymbol.net/articles/unofficial-bash-strict-mode/
    declare -r IFS_DEFAULT=$'\n\t';
    IFS="$IFS_DEFAULT";

    if [ "$(id -u)" = "0" ]
    then declare -r IS_ROOT=true;
    else declare -r IS_ROOT=false;
    fi

    # --- tag d'interuption --- #
    declare isTrapCAsk=false;        # demande d'interuption par control C


    # --- variables des includes --- #
    declare -r UPDATE_HTTP_ROOT='http://updates.legite.org';
    declare -r UPDATE_HTTP="$UPDATE_HTTP_ROOT/$SCRIPT_NAME";

    declare CONF_ROOT="${CONF_ROOT:-"$HOME/.config/legite"}";     # Charge la variable d'environnement sinon valeur par défaut
    declare CONF_REP="${CONF_REP:-"$CONF_ROOT/$SCRIPT_NAME"}";    # idem

    #declare CONF_SRC_PATH="$SCRIPT_REP/$SCRIPT_NAME.ori.sh";   # obsolete
    declare CONF_ORI_PATH="$SCRIPT_REP/$SCRIPT_NAME.ori.sh";
    declare CONF_USR_PATH="$CONF_REP/$SCRIPT_NAME.cfg.sh";


    # --- variables generiques --- #
    declare -r -i ARG_NB=$#;
    IFS=' ';read -ra ARG_TBL <<< "$@";IFS="$IFS_DEFAULT";
    #declare -a ARG_ALL=("$@");         # Apllatit $@ ($@ est un mécanisme spécial qui double quote ses éléments)
    declare    argument='';             # argument actuel ($1)
    declare    parametre='';            # parametre actuel ($2)
    declare    isUpdate=false;          # demande de update
    declare -i aide_level=0;            # niveau d'affichage d'aide
    declare -i backupGTTLevel=0;        # backup de l'app gtt
    declare -i backupLevel=0;           # backup des app (les collections n'ont pas de systemes de backup)


    declare -gi  testLevel=0;           # Niveau de test des app/collection
    declare -gi  recursifLevel=0;       # Niveau de recursivité
    declare -gri RECUR_NO_LIMIT=-1;     # récursivité sans limite
    declare -gri RECUR_NONE=0;          # récursivité désactivé (répertoire courant uniquement)
    #declare -gri RECUR_CURRENT=0;      # récursivité limité au répertoire d'appel

    declare -g  isVersion=false;        # Affiche la version des programmes chargés

    # - verbose - #
    #declare -i verbosity=0;            # niveau de verbosité defini via la ligne de cmd
    declare -i  VERBOSE_LEVEL_DEFAUT=3; # 0:Afficher tout
    declare -i  debug_verbose_level=0;
    #local  -i  _debug_verbose_level=0;
    #declare -i vic=0;                  # Verbode In Code: n niveau par defaut de verbose pour echoV


    declare -i isShowVars=0;            # montrer les variables
    #declare    isShowLibs=0;           # 1:vue trier des index. 2: vue triés index+description
    declare    showLibsLevel=0;         # 1:vue trier des index. 2: vue triés index+description

    declare    isShowDuree=false;       # Affiche la duree du script
    declare    isSimulation=false       # 
    declare    isExecFileQuiet=true;    # Pas de sortie en cas d'erreur

    # - exec- #
    declare -A execOnceTbl=();          # $E_TRUE ou $E_FALSE(ou non définit)


    # - PSP-calls - #
    declare -A PSP_Calls=();            # (array) liste des PSP(datas) non PSP_params (collections/librairies/autres) appellées pour etre executées
    PSP_calls[0]='';                    # meta info ne contient pas de nom de PSP (reservé pour un usage futur)
    declare -i PSP_calls_nb=0;          # nb(-1) de PSP dans le tableau PSP_calls
    declare -i PSP_calls_index=-1;      # index courant du tableau PSP_calls # commence par -1 (PSP-calls-add incremente avant)


    # - Apps - #
    declare    APPS_REP="$SCRIPT_REP/apps";
    declare    is_PSPActuelApp=false;   # Est-ce une app? #obsolete?
    declare    isPSPActuelAppAsk=false  # true si l'appli est appellée explicitement par ^apps (information pour l'utilisateur, pas utilisé par gtt)
                                        # l'état n'est valide que dans la boucle PSPExecCall et pour la PSP courant
    declare    isAppExecLibSelf=true;   # Par défaut la librairie homonyme à l'application est automatiquement chargée
    #declare -i isAppExecAuto=0;        # setIntegral(): l'app doit elle s'excuter entierement?
    declare -i isAppFail=0;             # indique qu'il y  un problème en amont dans la collection (desactive le lancement des libs de la collection actuelle)
    declare -i isListeModuls=0;         # 
    declare    appNom='';               # nom d l'app actuelle (loadApp)
    declare -A appExecTbl=();           # tableau: La collection a t'elle déjà été executée?

    # - App: themes - #
    declare THEMES_REP="$SCRIPT_REP/themes";
    declare THEME_DEFAUT='defaut';
    declare THEME_NOM="$THEME_DEFAUT";


    # - collections - #
    declare    COLLECTIONS_REP="$CONF_REP/collections";
    declare    is_PSPActuelCollection=false;    # Est-ce une collection?
    declare    isPSPActuelCollectionAsk=false;  # true si la collection est appellée explicitement par ^collections (information pour l'utilisateur, pas utilisé par gtt)
                                                # l'état n'est valide que dans la boucle PSPExecCall et pour le PSP courant
    declare    isCollectionExecLibSelf=true;# Par défaut la librairie homonyme à la collection est automatiquement chargée
    declare -i isLibExecAuto=$E_FALSE;      # mode collection auto demandé via le PSP ou -Auto?

    declare -i isCollectionFail=0;          # indique qu'il y  un problème en amont dans la collection (desactive le lancement des libs de la collection actuelle)
    declare -i isListeCollections=0;        # 
    declare    collectionNom='';            # nom de la collection actuelle (loadCollection)
    declare -A collectionExecTbl=();        # tableau: La collection a t'elle déjà été executée?


    # - librairies - #
    declare    librairie='';                # librairie en cours
    declare -A librairiesTbl=();            # table des librairies-fonctions qui sont enregistrées (et pouvant etre executées)
    declare -A librairiesDescr=();          # description de chaque librairie
    declare -A librairies_sources=();       # pathname du fuchier source de chaque librairie
    declare -A libExecTbl=();               # tableau: La librairie a t'elle déjà été executée?

    # - librairies: listing - #
    declare    isShowLibs=false;            # true si $isShowAllLibs || $isShowAppsLibs || $isShowCollectionsLibs
    declare    isShowAllLibs=false;         # Affiche les librairies de chaque app et collection
    declare    isShowApps=false;            #
    declare    isShowAppsLibs=false;        # Affiche les librairies de chaque app
    declare    isShowCollections=false;     # Affiche les fichiers du repertoire $COLLECTIONS_REP;
    declare    isShowCollectionsLibs=false; # Affiche les librairies de chaque collection


    # - PSP_params - #
    declare -i isPSP_params=0;              # les parametres en cours sont t'ils des parametres pour les librairies?
    #declare -i isPSPSup=0;                 # obsolete (new: isPSP_params)
    declare -A PSP_params=();               # tableau associatifs contenant les parametres supplémentaires (après le '-')
    #declare -A libParams;                  # abosolete (PSP_params)
    declare    isPSPNextAsk=false;          # si true; fait un continue dans execPSP()

    # - Divers - #
    declare -g isPause=false;               # L'user doit appuyer sur enter pour continuer(hitEnter)
#
