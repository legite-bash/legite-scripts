echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.22
# Description: fonctions disques utilisées par gtt.sh en interne
# Les autres fonctions disques sont dans: apps/linux/disques/_ini.cfg.sh

##################
# objet: inode-* #
##################
# inode: repertoire ou fichier ou lien symbolique
# initialise les variables attachées
    declare -g inode_pathname='';           # chemin complet /rep/er/toire/inodeNom.inodeExt
    declare -g inode_rep='';                # /rep/er/toire sans slash de fin
    declare -g inode_name='';               # inodeNom.inodeExt
    declare -g inode_nom='';                # nom de l'inode sans l'extention
    declare -g inode_ext='';                # extention de l'inode sans le le point
    #declare -g inode_log_pathname='';       # chemin du fichier de log
    declare -g inode_isShadowFile=false;     # le nom de fihcier commence  il par un point '.' ?

    function inode-init(){
        inode_pathname='';
        inode_rep='';
        inode_name='';
        inode_nom='';
        inode_ext='';
        inode_isShadowFile=false;
        return 0;
    }

    # inode-display-vars()
    # Affiche sous forme de display-vars les variables d'informations
    function inode-display-vars(){
        display-vars 'inode_pathname   ' "$inode_pathname";
        display-vars 'inode_rep        ' "$inode_rep";
        display-vars 'inode_name       ' "$inode_name";
        display-vars 'inode_nom        ' "$inode_nom";
        display-vars 'inode_ext        ' "$inode_ext";
        return 0;
    }


    #inode-pathname( pathname )
    # definit les variables de l'inode
    function inode-pathname(){
        inode-init;
        if [ "${1:-""}" = ""  ];then inode-init;return $E_FALSE;fi
        inode_pathname="$1";
        #inode_pathname="$1";
        inode_rep="${inode_pathname%/*}";       # display-vars 'inode_rep' "$inode_rep";
        inode_name="${inode_pathname##*/}";     # display-vars 'inode_name' "$inode_name";
        inode_nom="${inode_name%.*}";           # display-vars 'inode_nom' "$inode_nom";
        inode_ext="${inode_name##*.}";


        # - filtre:nom - #
        if [ "${inode_name:0:1}" = '.' ]        # est-ce un fichier caché ? (.htaccess[.extention])
        then
            if [ "$inode_nom" = '' ]            # est ce un fichier caché seul (.htaccess)?
            then inode_nom="${inode_name}";     # 
            #else  inode_nom="${inode_nom}";    # non (.fichierCaché.extention). On change pas le nom, il est dajà filtré
            fi
            if [ "$inode_ext" = "${inode_nom:1}" ]  # Si l'extention egal au nom alors il y a pas d'extention
            then
                inode_ext='';
            fi
        fi


        # - filtre:rep - #
        if [ "$inode_rep" = "$inode_nom" ]\
        || [ "$inode_rep" = "${inode_nom}." ]   # Fichier seul qui finit par un point
        then inode_rep='';fi                    # pathname ne contient aucun répertoire

        if [ -z "$inode_rep"  ]\
        && [ "${inode_pathname:0:1}" = '/' ]
        then inode_rep='/'; fi # Le fichier est à la racine


        # - filtre:ext - #
        if [ "$inode_ext" = "$inode_nom" ];then inode_ext='';fi    # L'extention est vide
        return 0;
    }

    function inode-rep(){
            inode_rep="${1:-""}";
            if [ "$inode_rep" != '/' ]                              # si pas root ('/')
            then inode_rep="${inode_rep%/}";                        # on supprime le dernier slash si indiqué
            fi
            if [ "$inode_rep" = '' ]
            then  inode_pathname="${inode_name}"
            else  inode_pathname="${inode_rep}/${inode_name}"
            fi
            return 0;
        }
    #

    function inode-nom(){
        if [ "${1:-""}" = ""  ];then return $E_FALSE;fi # le nom ne peut etre vide
        inode_nom="${1}";
        if [ "$inode_ext" = '' ]
        then  inode_name="${inode_nom}"
        else  inode_name="${inode_nom}.${inode_ext}"
        fi
        if [ "$inode_rep" = '' ]
        then  inode_pathname="${inode_name}"
        else  inode_pathname="${inode_rep}/${inode_name}"
        fi
        return 0;
    }

    function inode-ext(){
        inode_ext="${1:-""}";
        if [ "$inode_ext" = '' ]
        then  inode_name=".${inode_nom}"
        else  inode_name="${inode_nom}.${inode_ext}"
        fi
        if [ "$inode_rep" = '' ]
        then  inode_pathname="${inode_name}"
        else  inode_pathname="${inode_rep}/${inode_name}"
        fi
        return 0;
    }
#


####################
# objet: fichier-* #
####################
# fichier: repertoire ou fichier ou lien symbolique
# initialise les variables attachées
    declare -g fichier_pathname='';           # chemin complet /rep/er/toire/inodeNom.inodeExt
    declare -g fichier_rep='';                # /rep/er/toire sans slash de fin
    declare -g fichier_name='';               # fichierNom.fichierExt
    declare -g fichier_nom='';                # nom de l'inode sans l'extention
    declare -g fichier_ext='';                # extention de l'inode sans le le point
    #declare -g inode_log_pathname='';       # chemin du fichier de log
    declare -g fichier_isShadowFile=false;     # le nom de fihcier commence  il par un point '.' ?

    function fichier-init(){
        fichier_pathname='';
        fichier_rep='';
        fichier_name='';
        fichier_nom='';
        fichier_ext='';
        fichier_isShadowFile=false;
        return 0;
    }

    # fichier-display-vars()
    # Affiche sous forme de display-vars les variables d'informations
    function fichier-display-vars(){
        display-vars 'fichier_pathname   ' "$fichier_pathname";
        display-vars 'fichier_rep        ' "$fichier_rep";
        display-vars 'fichier_name       ' "$fichier_name";
        display-vars 'fichier_nom        ' "$fichier_nom";
        display-vars 'fichier_ext        ' "$fichier_ext";
        return 0;
    }


    #fichier-pathname( pathname )
    # definit les variables de l'inode
    function fichier-pathname(){
        fichier-init;
        if [ "${1:-""}" = ""  ];then fichier-init;return $E_FALSE;fi
        fichier_pathname="$1";
        #fichier_pathname="$1";
        fichier_rep="${fichier_pathname%/*}";       #display-vars 'fichier_rep' "$fichier_rep";
        fichier_name="${fichier_pathname##*/}";     #display-vars 'fichier_name' "$fichier_name";
        fichier_nom="${fichier_name%.*}";           #display-vars 'fichier_nom' "$fichier_nom";
        fichier_ext="${fichier_name##*.}";


        # - filtre:nom - #
        if [ "${fichier_name:((${#fichier_name} - 1))}" = '.' ]
        then
            fichier_nom+='.'; # Le nom d'origine finit par un point: on rajoute ce point.
        fi

        if [ "${fichier_name:0:1}" = '.' ] # est-ce un fichier caché ? (.htaccess[.extention])
        then
            if [ "$fichier_nom" = '' ]            # est ce un fichier caché seul (.htaccess)?
            then fichier_nom="${fichier_name}";     # 
            #else  fichier_nom="${fichier_nom}";    # non (.fichierCaché.extention). On change pas le nom, il est dajà filtré
            fi
            if [ "$fichier_ext" = "${fichier_nom:1}" ]  # Si l'extention egal au nom alors il y a pas d'extention
            then
                fichier_ext='';
            fi
        fi


        # - filtre:rep - #
        if [ "$fichier_rep" = "$fichier_nom" ];then fichier_rep='';fi    # pathname ne contient aucun répertoire
        if [ "$fichier_rep" = "" ]&&\
           [ "${fichier_pathname:0:1}" = '/' ]
        then
            fichier_rep='/';  # Le fichier est à la racine
        fi

        # - filtre:ext - #
        if [ "$fichier_ext" = "$fichier_nom" ];then fichier_ext='';fi    # L'extention est vide
        return 0;
    }

    function fichier-rep(){
            fichier_rep="${1:-""}";
            if [ "$fichier_rep" != '/' ]           # si pas root ('/')
            then fichier_rep="${fichier_rep%/}";    # on supprime le dernier slash si indiqué
            fi
            if [ "$fichier_rep" = '' ]
            then  fichier_pathname="${fichier_name}"
            else  fichier_pathname="${fichier_rep}/${fichier_name}"
            fi
            return 0;
        }
    #

    function fichier-nom(){
        if [ "${1:-""}" = ""  ];then return $E_FALSE;fi # le nom ne peut etre vide
        fichier_nom="${1}";
        if [ "$fichier_ext" = '' ]
        then  fichier_name="${fichier_nom}"
        else  fichier_name="${fichier_nom}.${fichier_ext}"
        fi
        if [ "$fichier_rep" = '' ]
        then  fichier_pathname="${fichier_name}"
        else  fichier_pathname="${fichier_rep}/${fichier_name}"
        fi
        return 0;
    }

    function fichier-ext(){
        fichier_ext="${1:-""}";
        if [ "$fichier_ext" = '' ]
        then  fichier_name=".${fichier_nom}"
        else  fichier_name="${fichier_nom}.${fichier_ext}"
        fi
        if [ "$fichier_rep" = '' ]
        then  fichier_pathname="${fichier_name}"
        else  fichier_pathname="${fichier_rep}/${fichier_name}"
        fi
        return 0;
    }


#


################
# objet: src-* #
################
# src: repertoire ou fichier ou lien symbolique
# initialise les variables attachées
# Utilisé par - src=""
    declare -g src_pathname='';           # chemin complet /rep/er/toire/srcNom.srcExt
    declare -g src_rep='';                # /rep/er/toire sans slash de fin
    declare -g src_name='';               # srcNom.srcExt
    declare -g src_nom='';                # nom de l'src sans l'extention
    declare -g src_ext='';                # extention de l'src sans le le point
    #declare -g src_log_pathname='';       # chemin du fichier de log
    declare -g src_isShadowFile=false;     # le nom de fihcier commence  il par un point '.' ?

    function src-init(){
        src_pathname='';
        src_rep='';
        src_name='';
        src_nom='';
        src_ext='';
        src_isShadowFile=false;
        return 0;
    }

    # src-display-vars()
    # Affiche sous forme de display-vars les variables d'informations
    function src-display-vars(){
        display-vars 'src_pathname   ' "$src_pathname";
        display-vars 'src_rep        ' "$src_rep";
        display-vars 'src_name       ' "$src_name";
        display-vars 'src_nom        ' "$src_nom";
        display-vars 'src_ext        ' "$src_ext";
        return 0;
    }


    #src-pathname( pathname )
    # definit les variables de l'src
    function src-pathname(){
        src-init;
        if [ "${1:-""}" = ""  ];then src-init;return $E_FALSE;fi
        src_pathname="$1";
        #src_pathname="$1";
        src_rep="${src_pathname%/*}";       #display-vars 'src_rep' "$src_rep";
        src_name="${src_pathname##*/}";     #display-vars 'src_name' "$src_name";
        src_nom="${src_name%.*}";           #display-vars 'src_nom' "$src_nom";
        src_ext="${src_name##*.}";


        # - filtre:nom - #
        if [ "${src_name:((${#src_name} - 1))}" = '.' ]
        then
            src_nom+='.'; # Le nom d'origine finit par un point: on rajoute ce point.
        fi

        if [ "${src_name:0:1}" = '.' ] # est-ce un fichier caché ? (.htaccess[.extention])
        then
            if [ "$src_nom" = '' ]            # est ce un fichier caché seul (.htaccess)?
            then src_nom="${src_name}";     # 
            #else  src_nom="${src_nom}";    # non (.fichierCaché.extention). On change pas le nom, il est dajà filtré
            fi
            if [ "$src_ext" = "${src_nom:1}" ]  # Si l'extention egal au nom alors il y a pas d'extention
            then
                src_ext='';
            fi
        fi


        # - filtre:rep - #
        if [ "$src_rep" = "$src_nom" ];then src_rep='';fi    # pathname ne contient aucun répertoire
        if [ "$src_rep" = "" ]&&\
           [ "${src_pathname:0:1}" = '/' ]
        then
            src_rep='/';  # Le fichier est à la racine
        fi

        # - filtre:ext - #
        if [ "$src_ext" = "$src_nom" ];then src_ext='';fi    # L'extention est vide
        return 0;
    }

    function src-rep(){
            src_rep="${1:-""}";
            if [ "$src_rep" != '/' ]           # si pas root ('/')
            then src_rep="${src_rep%/}";    # on supprime le dernier slash si indiqué
            fi
            if [ "$src_rep" = '' ]
            then  src_pathname="${src_name}"
            else  src_pathname="${src_rep}/${src_name}"
            fi
            return 0;
        }
    #

    function src-nom(){
        if [ "${1:-""}" = ""  ];then return $E_FALSE;fi # le nom ne peut etre vide
        src_nom="${1}";
        if [ "$src_ext" = '' ]
        then  src_name="${src_nom}"
        else  src_name="${src_nom}.${src_ext}"
        fi
        if [ "$src_rep" = '' ]
        then  src_pathname="${src_name}"
        else  src_pathname="${src_rep}/${src_name}"
        fi
        return 0;
    }

    function src-ext(){
        src_ext="${1:-""}";
        if [ "$src_ext" = '' ]
        then  src_name=".${src_nom}"
        else  src_name="${src_nom}.${src_ext}"
        fi
        if [ "$src_rep" = '' ]
        then  src_pathname="${src_name}"
        else  src_pathname="${src_rep}/${src_name}"
        fi
        return 0;
    }




#


#################
# objet: dest-* #
#################
# dest: repertoire ou fichier ou lien symbolique
# initialise les variables attachées
# Utilisé par - dest=""

    declare -g dest_pathname='';           # chemin complet /rep/er/toire/destNom.destExt
    declare -g dest_rep='';                # /rep/er/toire sans slash de fin
    declare -g dest_name='';               # destNom.destExt
    declare -g dest_nom='';                # nom de l'dest sans l'extention
    declare -g dest_ext='';                # extention de l'dest sans le le point
    #declare -g dest_log_pathname='';       # chemin du fichier de log
    declare -g dest_isShadowFile=false;     # le nom de fihcier commence  il par un point '.' ?

    function dest-init(){
        dest_pathname='';
        dest_rep='';
        dest_name='';
        dest_nom='';
        dest_ext='';
        dest_isShadowFile=false;
        return 0;
    }

    #dest-pathname( pathname )
    # definit les variables de l'dest
    function dest-pathname(){
        dest-init;
        if [ "${1:-""}" = ""  ];then dest-init;return $E_FALSE;fi
        dest_pathname="$1";
        #dest_pathname="$1";
        dest_rep="${dest_pathname%/*}";       #display-vars 'dest_rep' "$dest_rep";
        dest_name="${dest_pathname##*/}";     #display-vars 'dest_name' "$dest_name";
        dest_nom="${dest_name%.*}";           #display-vars 'dest_nom' "$dest_nom";
        dest_ext="${dest_name##*.}";


        # - filtre:nom - #
        if [ "${dest_name:((${#dest_name} - 1))}" = '.' ]
        then
            dest_nom+='.'; # Le nom d'origine finit par un point: on rajoute ce point.
        fi

        if [ "${dest_name:0:1}" = '.' ] # est-ce un fichier caché ? (.htaccess[.extention])
        then
            if [ "$dest_nom" = '' ]            # est ce un fichier caché seul (.htaccess)?
            then dest_nom="${dest_name}";     # 
            #else  dest_nom="${dest_nom}";    # non (.fichierCaché.extention). On change pas le nom, il est dajà filtré
            fi
            if [ "$dest_ext" = "${dest_nom:1}" ]  # Si l'extention egal au nom alors il y a pas d'extention
            then
                dest_ext='';
            fi
        fi


        # - filtre:rep - #
        if [ "$dest_rep" = "$dest_nom" ];then dest_rep='';fi    # pathname ne contient aucun répertoire
        if [ "$dest_rep" = "" ]&&\
           [ "${dest_pathname:0:1}" = '/' ]
        then
            dest_rep='/';  # Le fichier est à la racine
        fi

        # - filtre:ext - #
        if [ "$dest_ext" = "$dest_nom" ];then dest_ext='';fi    # L'extention est vide
        return 0;
    }

    function dest-rep(){
            dest_rep="${1:-""}";
            if [ "$dest_rep" != '/' ]           # si pas root ('/')
            then dest_rep="${dest_rep%/}";    # on supprime le dernier slash si indiqué
            fi
            if [ "$dest_rep" = '' ]
            then  dest_pathname="${dest_name}"
            else  dest_pathname="${dest_rep}/${dest_name}"
            fi
            return 0;
        }
    #

    function dest-nom(){
        if [ "${1:-""}" = ""  ];then return $E_FALSE;fi # le nom ne peut etre vide
        dest_nom="${1}";
        if [ "$dest_ext" = '' ]
        then  dest_name="${dest_nom}"
        else  dest_name="${dest_nom}.${dest_ext}"
        fi
        if [ "$dest_rep" = '' ]
        then  dest_pathname="${dest_name}"
        else  dest_pathname="${dest_rep}/${dest_name}"
        fi
        return 0;
    }

    function dest-ext(){
        dest_ext="${1:-""}";
        if [ "$dest_ext" = '' ]
        then  dest_name=".${dest_nom}"
        else  dest_name="${dest_nom}.${dest_ext}"
        fi
        if [ "$dest_rep" = '' ]
        then  dest_pathname="${dest_name}"
        else  dest_pathname="${dest_rep}/${dest_name}"
        fi
        return 0;
    }


    #dest-display-vars()
    # Affiche sous forme de display-vars les variables d'informations
    function dest-display-vars(){
        display-vars 'dest_pathname   ' "$dest_pathname";
        display-vars 'dest_rep        ' "$dest_rep";
        display-vars 'dest_name       ' "$dest_name";
        display-vars 'dest_nom        ' "$dest_nom";
        display-vars 'dest_ext        ' "$dest_ext";
        return 0;
    }
#

return 0;
