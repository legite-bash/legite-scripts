echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.17
# Description: 


##################
# fct-pile-gtt-* #
##################
    # clear; gtt.sh -d --libExecAuto apps/gtt/sid/tests/test-fctPile test-fctPile
    declare FCT_PILE_GTT_LEVEL_DEFAUT=3;
    declare fct_pile_gtt_motif='-';          # motif de decalage pour chaque sous rep (modifié aussi par setTabulation)
    declare fct_pile_gtt_motif_cumul='';     # cumul du motif 

    # Construit la chaine des separateur en fonction du nombre dans la pile des fonctions appellées
    function fct-pile-gtt-motif-cumul-calc() {
        local -i _fctNb=${#FUNCNAME[*]};((_fctNb--));       # display-vars "${FUNCNAME[0]}():$LINENO:_fctNb" "$_fctNb";
        fct_pile_gtt_motif_cumul='';
        local _i=3;                                         # on en enleve 3
        while [ $_i -lt $_fctNb  ];do fct_pile_gtt_motif_cumul+="$fct_pile_gtt_motif";((_i++));  done

        return 0;
    }


    #function fct-pile-gtt-in( "$*" _fct_pile_gtt_level )
    function fct-pile-gtt-in() {
        local -i _fct_pile_gtt_level=${2:-$FCT_PILE_GTT_LEVEL_DEFAUT};
        if [ $_fct_pile_gtt_level -eq 0 ];then return 0;fi
        if [ $_fct_pile_gtt_level -le $debug_gtt_level  ]           # debug_app_level
        then
            #display-vars 'BEGIN:debug_app_level' "$debug_app_level" '_fct_pile_gtt_level' "$_fct_pile_gtt_level";

            local _fctParams="${1:-""}";                # renvoie le nom de la function et non un parametre
            _fctParams="$(echo $_fctParams | tr -d '\n' )";
            _fctParams="${GREEN}$_fctParams";
            #display-vars '_fctParams' "$_fctParams"

            local _fctActuelle="${FUNCNAME[1]}";
            local _fctNoms='';

            fct-pile-gtt-motif-cumul-calc;               # recalcul de fct-pile-gtt-motif-cumul
            
            # Construction de la pile pour affichage
            local -i _fctNb=${#FUNCNAME[@]}
            if [ $_fctNb -ge 2 ]
            then
                for (( _i=2; _i<$_fctNb; _i++ ));    # _i=2:on enleve fct-pile-gtt-in et la fonction actuelle du debut de la pile
                do
                    local _fct="${FUNCNAME[$_i]}";
                    _fctNoms+="${_fct:-''} ";
                done
            fi
            #if [[ ! -z "$_fctNoms" ]]; then _fctNoms=" [ $_fctNoms]";fi
            echo -e $COLOR_FUNCTION"$fct_pile_gtt_motif_cumul$_fctActuelle($_fctParams${COLOR_FUNCTION})${COLOR_NORMAL}[ $_fctNoms]"${COLOR_NORMAL};
        fi
        return 0;
    }

    
    #fct-pile-gtt-out erreur_no $_fct_pile_gtt_level;        # v2     -> Tous les gttelles devront être réécrit
    function fct-pile-gtt-out() {
        local -i _fct_pile_gtt_level=${2:-$FCT_PILE_GTT_LEVEL_DEFAUT};
        if [ $_fct_pile_gtt_level -eq 0 ];then return 0;fi
        if [ $_fct_pile_gtt_level -le $debug_gtt_level ]
        then
            local -i _fctNb=${#FUNCNAME[*]}
            fct-pile-gtt-motif-cumul-calc;           # recalcul de fct-pile-gtt-motif-cumul

            # v2
            local -i _erreur_no=${1:-0};
            local    _fctActuelle="${FUNCNAME[1]}";
            
            # Coloriser la sortie de _erreur_no
            local _color="";
            if [ $_erreur_no -ne $E_TRUE ]
            then _color=$COLOR_WARN;
            else _color=$COLOR_OK;
            fi

            local _erreur_description="${erreur_descriptions[$_erreur_no]:-""}";
            if [ ! "$_erreur_description" = "" ];then _erreur_description=":${_erreur_description}";fi
            _erreur_description="$_color$_erreur_no$_erreur_description$COLOR_NORMAL";
            #if [ -n "$_erreur_description" ];then echo "$_color$_erreur_description$COLOR_NORMAL";fi

            # Afficher la sortie
            echo ${COLOR_FUNCTION}"${fct_pile_gtt_motif_cumul}${_fctActuelle}(${_erreur_description})"${COLOR_FUNCTION}${COLOR_NORMAL};
        fi
        return 0;
    }
#


##################
# fct-pile-app-* #
##################
    # clear; gtt.sh -d --libExecAuto apps/gtt/sid/tests/test-fctPile test-app-fctPile
    declare FCT_PILE_APP_LEVEL_DEFAUT=3;
    declare fct_pile_app_motif='-';          # motif de decalage pour chaque sous rep (modifié aussi par setTabulation)
    declare fct_pile_app_motif_cumul='';     # cumul du motif 

    # Construit la chaine des separateur en fonction du nombre dans la pile des fonctions appellées
    function fct-pile-app-motif-cumul-calc() {
        local -i _fctNb=${#FUNCNAME[*]};
        _fctNb=$((_fctNb-5));                # Ne pas afficher les fonctions gtt
        fct_pile_app_motif_cumul='';
        local _i=0
        while [ $_i -lt $_fctNb  ];do fct_pile_app_motif_cumul+="$fct_pile_app_motif";((_i++));  done
        #display-vars ${FUNCNAME[0]}'fct_pile_app_motif_cumul' "$fct_pile_app_motif_cumul";
        return 0;
    }

    #function fct-pile-app-in( "$*" _fct_pile_app_level )
    function fct-pile-app-in() {
        local -i _fct_pile_app_level=${2:-$FCT_PILE_APP_LEVEL_DEFAUT};
        if [ $_fct_pile_app_level -eq 0 ];then return 0;fi
        if [ $_fct_pile_app_level  -le $debug_app_level  ]   # debug_app_level
        then
            #display-vars 'BEGIN:debug_app_level' "$debug_app_level" '_fct_pile_app_level' "$_fct_pile_app_level";

            local _fctParams="${1:-""}";     # renvoie le nom de la function et non un parametre
            _fctParams="$(echo $_fctParams | tr -d '\n' )";
            #display-vars '_fctParams' "$_fctParams"
            _fctParams=${GREEN}$_fctParams
            local _fctActuelle="${FUNCNAME[1]}";
            local _fctNoms='';

            fct-pile-app-motif-cumul-calc;           # recalcul de fct-pile-app-motif-cumul
            
            # Construction de la pile pour affichage
            local -i _fctNb=${#FUNCNAME[@]}
            
            if [ $_fctNb -ge 2 ]
            then
                local _baseIndex=$((_fctNb-4));         # Suppression des niveaux gtt
                for (( _i=2; _i<_baseIndex; _i++ )); # _i=2:on eneleve fctIn et la fonction actuelle
                do
                    local _fct="${FUNCNAME[$_i]}";
                    _fctNoms+="${_fct:-''} ";
                done
            fi
            #if [[ ! -z "$_fctNoms" ]]; then _fctNoms=" [ $_fctNoms]";fi
            echo -e $COLOR_FUNCTION"$fct_pile_app_motif_cumul$_fctActuelle($_fctParams${COLOR_FUNCTION})${COLOR_NORMAL}[ $_fctNoms]"${COLOR_NORMAL};
        fi
        return 0;
    }

    
    #fct-pile-app-out erreur_no $_fct_pile_app_level;                 # v2     -> Tous les appelles devront être réécrit
    function fct-pile-app-out() {
        local -i _fct_pile_app_level=${2:-$FCT_PILE_APP_LEVEL_DEFAUT};
        if [ $_fct_pile_app_level -eq 0 ];then return 0;fi
        if [ $_fct_pile_app_level -le $debug_app_level ]
        then
            local -i _fctNb=${#FUNCNAME[*]}
            fct-pile-app-motif-cumul-calc;           # recalcul de fct-pile-app-motif-cumul

            # v2
            erreur-set ${1:-0};
            local _fctActuelle="${FUNCNAME[1]}";
            
            # Coloriser la sortie de _erreur_no
            local _color="";
            if [ $erreur_no -ne $E_TRUE ]
            then _color=$COLOR_WARN;
            else _color=$COLOR_OK;fi
            local _erreur_description="";
            _erreur_description="$_color$erreur_no:${erreur_descriptions[$erreur_no]:-""}$COLOR_NORMAL";
            #if [ -n "$_erreur_description" ];then echo "$_color$_erreur_description$COLOR_NORMAL";fi

            # Afficher la sortie
            echo "${COLOR_FUNCTION}${fct_pile_app_motif_cumul}${_fctActuelle}(${_erreur_description})${COLOR_FUNCTION}:END"${COLOR_NORMAL};
        fi
        return 0;
    }
#

return 0;