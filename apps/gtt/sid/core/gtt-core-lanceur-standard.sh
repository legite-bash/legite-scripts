echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


# ##### #
# USAGE #
# ##### #
    function usage() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $aide_level -gt 0 ]
        then
            titreInfo "$SCRIPT_FILENAME [-Ddv] [--show-vars]\
            \n   [--show-apps] [--show-collections] [--show-libsLevel1] [--show-libsLevel2] [--show-apps-libs] [--show-collections-libs] [--show-all-libs] [--show-vars]\
            \n   [application] [collection] [lib] [fichier]\
            \n   [-] [paramLib1] [paramLib2=valeur]";
            #echo "--update: update le programme legite-scripts.sh lui meme (via $UPDATE_HTTP)."
            #echo '--show-vars: Afficher les variables du programmes.'
            echo 'application|collection|lib: execute une application puis/ou une collection puis/ou librairie.'
            #echo '';
            #echo "$SCRIPT_FILENAME --show-collections # pour voir les collections présentes"
            #echo "$SCRIPT_FILENAME --show-libs # pour voir les librairies présentes (dépend des collections chargées)."
            #usage-local;
        fi
        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }

    # A surcharger par le projet
    #function usage-local() {
    #    local -i _fct_pile_gtt_level=2;
    #    fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

    #    fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    #}
#


# ######## #
# show-vars #
# ######## #
    function show-vars() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $isShowVars -ne 0 ]
        then
            titre1 "${FUNCNAME[0]}()"

            #display-vars " ARG_NB=$ARG_NB"
            display-vars 'SCRIPT_PATH           ' "$SCRIPT_PATH";
            display-vars 'SCRIPT_REP            ' "$SCRIPT_REP";
            display-vars 'SCRIPT_FILENAME       ' "$SCRIPT_FILENAME" 'SCRIPT_NAME' "$SCRIPT_NAME"  'SCRIPT_EXT' "$SCRIPT_EXT";
            #display-vars 'IFS_DEFAULT' "$IFS_DEFAULT" 'IFS' "$IFS"

            display-vars 'GTT_VERSION_SELECT    ' "$GTT_VERSION_SELECT";
            display-vars 'GTT_CORE_REP          ' "$GTT_CORE_REP";

            titre4 'DEBUG';
            display-vars 'IS_ROOT               ' "$IS_ROOT"   'isUpdate' "$isUpdate";
            display-vars 'aide_level            ' "$aide_level";
            display-vars 'DEBUG_GTT_LEVEL_DEFAUT' "$DEBUG_GTT_LEVEL_DEFAUT" 'debug_gtt_level ' "$debug_gtt_level";
            display-vars 'DEBUG_APP_LEVEL_DEFAUT' "$DEBUG_APP_LEVEL_DEFAUT" 'debug_app_level ' "$debug_app_level";
            display-vars 'debug_verbose_level   ' "$debug_verbose_level"
            display-vars 'isShowVars            ' "$isShowVars" 'isShowDuree' "$isShowDuree";
            display-vars 'backupGTTLevel        ' "$backupGTTLevel" 'backupLevel' "$backupLevel";

            titre4 'répertoires';
            display-vars 'tmp_pathname          ' "$tmp_pathname";
            display-vars 'log_pathname          ' "$log_pathname";
            display-vars 'UPDATE_HTTP_ROOT      ' "$UPDATE_HTTP_ROOT";
            display-vars 'UPDATE_HTTP           ' "$UPDATE_HTTP";
            display-vars 'CONF_REP              ' "$CONF_REP";
            display-vars 'APPS_REP              ' "$APPS_REP";
            display-vars 'COLLECTIONS_REP       ' "$COLLECTIONS_REP";
            display-vars 'CONF_ORI_PATH         ' "$CONF_ORI_PATH";
            display-vars 'CONF_USR_PATH         ' "$CONF_USR_PATH";

            display-vars 'THEMES_REP            ' "$THEMES_REP" 'THEME_DEFAUT' "$THEME_DEFAUT" 'THEME_NOM' "$THEME_NOM";

            titre4 'fctPile';
            display-vars 'FCT_PILE_GTT_LEVEL_DEFAUT' "$FCT_PILE_GTT_LEVEL_DEFAUT" 'fct_pile_gtt_motif' "$fct_pile_gtt_motif" 'fct_pile_gtt_motif_cumul' "$fct_pile_gtt_motif_cumul";
            display-vars 'FCT_PILE_APP_LEVEL_DEFAUT' "$FCT_PILE_APP_LEVEL_DEFAUT" 'fct_pile_app_motif' "$fct_pile_app_motif" 'fct_pile_app_motif_cumul' "$fct_pile_app_motif_cumul";

            titre4 'ARGS';
            ARG_TBL-display;
            display-vars 'isSimulation          ' "$isSimulation";
            display-vars 'isPause               ' "$isPause";

            titre4 'PSP-calls';
            #PSP-calls-show;
            PSP-calls-display;
            display-vars 'isPSPActuelAppAsk     ' "$isPSPActuelAppAsk" 'isPSPActuelCollectionAsk' "$isPSPActuelCollectionAsk";

            titre4 'PSP_params';
            PSP-params-display;

            titre4 'parametres communs';
            display-vars 'testLevel             '  "$testLevel" 'recursifLevel'  "$recursifLevel" 'isSimulation'  "$isSimulation"  'isPause'  "$isPause";

            titre4 'apps/collections/libs';
            display-vars 'isShowCollections     ' "$isShowCollections" "isListeCollections" "$isListeCollections";

            display-vars "isShowLibs            " "$isShowLibs" 'isShowCollections' "$isShowCollections" "isListeCollections" "$isListeCollections";
            display-vars "showLibsLevel         " "$showLibsLevel";

            display-vars 'isCollectionExecLibSelf' "$isCollectionExecLibSelf";

            display-vars 'isCollectionFail      ' "$isCollectionFail";
            display-vars 'isLibExecAuto         ' "$isLibExecAuto";
            display-vars 'librairiesTbl[index*] ' "${!librairiesTbl[*]}";
            display-vars 'librairiesCall[*]     ' "${librairiesCall[*]}";
        fi

        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }


    function show-vars-post() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $isShowVars -eq 1 ]
        then
            titre1 "${FUNCNAME[0]}()";
            display-vars 'isCollectionExecLibSelf (global)     ' "$isCollectionExecLibSelf" ;
            display-vars 'isLibExecAuto(pour chaque PSP_Actuel)' "$isLibExecAuto";
            display-vars 'isCollectionFail' "$isCollectionFail";
            showCollectionExec;
            showLibrairieExec;
        fi

        # Afficher la durée du script
        #SCRIPT-DUREE-show;
        #SCRIPT-DUREE-display-vars;

        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }
#


#############
# BEGIN/END #
#############
    function lanceur-standard-begin() {
        # echo "${BASH_SOURCE[0]}:$LINENO:${FUNCNAME[0]}()";
        #psp-analyse;    # 1er action avant toutes autres

        #if [ $debug_gtt_level -eq 1 ];then clear; fi

        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;


        titre0-D "${FUNCNAME[0]}" 0;
        echo-D "$SCRIPT_PATH $GTT_VERSION" 1;

        # - Création de TMP_ROOT: $tmp_rep est définit dans gtt.ori.sh - #
        declare -gr TMP_ROOT="$tmp_rep";


        #echo-D " === CHARGEMENT DES THEMES ===" 2;
        # Chargé par gtt.sh
        #theme-load-defaut;
        #theme-load "$THEME_NOM";

        #echo-D " === CREATION DU REPERTOIRE TEMPORAIRE ET DE LOG ===" 2;


        echo-D " === usage() ===" 2;
        usage;

        #echo-D " === update() ===" 1;
        #gtt_update;

        echo-D " === shower() ===" 2;
        show-vars;
        show-apps-libs;
        showApps;
        showCollections;


        echo-D " === Backup de l'app (--backupGTT) ===" 2;
        if [ $backupGTTLevel -ne 0 ]
        then
            loadRepertoiresCfg '_backup' "$GTT_REP/";
            backup-sidToToday;  # backup l'app gtt
        fi

        if $isSimulation;then titreInfo "[SIMULATION: ON]"; fi

        PSP-calls-exec;

        showLibs;  #afficher une fois toutes les libs chargées (apres loadConfs et PSP-calls-exec)

        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }


    function lanceur-standard-end() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        titre0-D "${FUNCNAME[0]}" 0;
        notifs-pile-show;
        erreurs-pile-show;
        trap-pile-show;
        show-vars-post;
        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }
#

return 0;