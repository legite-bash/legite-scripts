echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : 2024.08.22
# date de modification: 2024.08.22
# Description: fonctions d'analyses recursives de répertoires
# test: gtt.sh --show-libsLevel2 apps/gtt/sid/tests/
#
# Utilisé par:rien. obsolete?

################################
# - OBJET: recursif_methode1 - #
################################
    # - methode blob - #
    # récursivité simple selon le blob *
    # trie: aucun
    # Changement de repertoire: non
    # defaut: renvoie rep/* pour les repertoirres vide
    # niveau de profondeur: 0: le rep courant. n: repcourant + n
    # Les callbacks doivent contenir le nom d'une fonction et non du code
    declare -g    recursif_methode1_inode='';           # inode courant dans la boucle
    declare -gi   recursif_methode1_levelMax=-1;        # -1: illimité 0: le repertoire courant uniquement
    declare -gi   recursif_methode1_level=0;            # niveau actuel de recursivité
    declare -g    recursif_methode1_fct='';             # nom de la fonction code a répéter
    declare -g    isRecursif_methode1_fctBefore=true;  # la fonction code est appellée AVANT la récursivité
    declare -g      recursif_methode1_fctBefore='';     # si vide alors = "${FUNCNAME[0]}-code"
    declare -g    isRecursif_methode1_fctAfter=false;   # la fonction code est appellée AFTER la récursivité
    declare -g      recursif_methode1_fctAfter='';      # si vide alors = "${FUNCNAME[0]}-code"
    declare -gi   recursif_methode1_repNb=0;
    declare -gi   recursif_methode1_fichierNb=0;


    function      recursif_methode1-init(){
        recursif_methode1_inode='';             # inode courant dans la boucle
        recursif_methode1_levelMax=-1;
        recursif_methode1_level=0;
        isRecursif_methode1_fctBefore=true;     # Appel le callback si true  Affiche par defaut le nom du rep en titre4
          recursif_methode1_fctBefore='';       # Callback en début de boucle
          recursif_methode1_fct='';             # Callback dans la boucle (ce callback est toujours appellé)
        isRecursif_methode1_fctAfter=false;     # Appel le callback si true
          recursif_methode1_fctAfter='';        # Callback en fin de boucle
        recursif_methode1_repNb=0;
        recursif_methode1_fichierNb=0;
    }

    function      recursif_methode1-display-var(){
        display-vars 'recursif_methode1_inode'        "$recursif_methode1_inode";
        display-vars 'recursif_methode1_level'        "$recursif_methode1_level";
        display-vars 'recursif_methode1_repNb'        "$recursif_methode1_repNb";
        display-vars 'recursif_methode1_fichierNb'    "$recursif_methode1_fichierNb"
    }


    # - Code modele - #
    function recursif_methode1-code(){
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        #echo "Ce code sera exécuté pour chaque item";
        #display-vars ${FUNCNAME[0]}:'$@' "$@";
        echo "$1";

        fct-pile-gtt-out 0 $_fct_pile_gtt_level; return 0;
    }

    function recursif_methode1-fctBefore(){
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        #echo "Ce code sera exécuté au début de chaque repertoire";
        titre4 "$1";    # $1: recoit l'inode en cour -> Affiche $1

        fct-pile-gtt-out 0 $_fct_pile_gtt_level; return 0;
    }

    addLibrairie 'recursif_methode1';                    # -R
    function      recursif_methode1(){
        if $isTrapCAsk;then return $E_CTRL_C;fi
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        local _src="${1:-""}";  # parametre obligatoire
        if [ "$_src" = "" ];then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE; fi
        #display-vars '_src' "$_src";

        if [ "$recursif_methode1_fct"       = '' ]; then recursif_methode1_fct="${FUNCNAME[0]}-code";                   fi; #display-vars 'recursif_methode1_fct'        "$recursif_methode1_fct";
        if [ "$recursif_methode1_fctBefore" = '' ]; then recursif_methode1_fctBefore="recursif_methode1-fctBefore";     fi; #display-vars 'recursif_methode1_fctBefore'  "$recursif_methode1_fctBefore";
        if [ "$recursif_methode1_fctAfter"  = '' ]; then recursif_methode1_fctAfter="void";                             fi; #display-vars 'recursif_methode1_fctAfter'   "$recursif_methode1_fctAfter";

        if [ -d "$_src" ]
        then
            #titre4 "$_src";

            for recursif_methode1_inode in "$_src"/*
            do
                #display-vars 'recursif_methode1_inode' "$recursif_methode1_inode";
                if $isTrapCAsk;then fct-pile-gtt-out $E_CTRL_C $_fct_pile_app_level; return $E_CTRL_C; fi

                 # Repertoire vide? ( renvoie le schéma si pas de concordance)
                if [ "$recursif_methode1_inode" = "$_src"'/*' ];then continue;fi


                # Les liens sympboliques se comportent comme leur type cible 
                # Un lien existant vers une cible inexistante renvoie VRAI
                # Il faut donc tester si la cible existe vraiment
                if [ ! -e "$recursif_methode1_inode" ];then continue;fi

                if   [ -d "$recursif_methode1_inode" ]
                then
                    ((recursif_methode1_repNb++))
                    if [ $recursif_methode1_levelMax -eq $RECUR_NONE ]
                    then
                        echo-D "Récursivité désactivé (recursif_methode1_levelMax=$recursif_methode1_levelMax)" 1;
                        continue;
                    fi

                    #if [ $recursif_methode1_levelMax -eq $RECUR_CURRENT ]
                    #then
                    #    echo-D "Récursivité limité au répretoire courant (recursif_methode1_levelMax=$recursif_methode1_levelMax)" 1;
                    #    continue;
                    #fi

                    (( recursif_methode1_level++ ))

                    #display-vars 'recursif_methode1_level' "$recursif_methode1_level/$recursif_methode1_level";
                    #display-vars 'RECUR_NO_LIMIT' "$RECUR_NO_LIMIT";
                    if [ $recursif_methode1_levelMax -ne $RECUR_NO_LIMIT ]
                    then
                        #display-vars 'recursif_methode1_level' "$recursif_methode1_level";
                        if [ $recursif_methode1_level -gt $recursif_methode1_levelMax ]
                        then
                            #display-vars '$debugLevel' "$debugLevel"
                            echo-D "niveau de récursivité maximal atteint (recursif_methode1_level/recursif_methode1_levelMax : $recursif_methode1_level/$recursif_methode1_levelMax)" ;
                            (( recursif_methode1_level-- ))
                            continue;
                        fi
                    fi

                    if $isRecursif_methode1_fctBefore;
                    then
                        $recursif_methode1_fctBefore "$recursif_methode1_inode";    # callback
                    fi

                    ${FUNCNAME[0]} "$recursif_methode1_inode";                      # Recursivité

                    if $isRecursif_methode1_fctAfter
                    then
                        #echo 'AFTER';
                        $recursif_methode1_fctAfter "$recursif_methode1_inode";     # callback
                        fi

                    (( recursif_methode1_level-- ))

                elif [ -f "$recursif_methode1_inode" ]
                then
                    ((recursif_methode1_fichierNb++))
                     $recursif_methode1_fct  "$recursif_methode1_inode";            # callback du fichier
                else
                    echo "'$recursif_methode1_inode' est ni un repertoire, ni un fichier".
                fi
            done
        elif [ -f "$_src" ]
        then
            ((recursif_methode1_fichierNb++))
            $recursif_methode1_fct "$_src";                                         # callback du fichier
        fi

        fct-pile-gtt-out 0 $_fct_pile_gtt_level; return 0;
    }
#

return 0;