echo-D "${BASH_SOURCE[0]}" 1;
# gtt-core-erreur.sh 
# date de création    : 2024.08.19
# date de modification: 2024.11.30
# Description: gestion des erreurs, erreur_no, erreur_description



####################
# CODES DE SORTIES #
####################
    # https://man7.org/linux/man-pages/man7/signal.7.html
    # http://www.gnu.org/software/bash/manual/html_node/Exit-Status.html#Exit-Status
    # https://abs.traduc.org/abs-5.0-fr/apd.html#exitcodesref
    # https://tldp.org/LDP/abs/html/exitcodes.html

    declare -i erreur_no=0;             # code retour à usage commun. utilisé par erreur-no-description-echo)
    declare    erreur_info='';          # complement d'information venant de la derniere erreur
    declare    erreur_description="";   # decription correspondant à $erreur_descriptions[$erreur_no]
    declare -a erreur_descriptions=();
    erreur_descriptions[0]='OK';

    declare -r -i E_MISC=1;             erreur_descriptions[1]='Erreur générale ou opération interdite ou FAUX logique';

    # 64..113: USER EXIT (compatible C)
    declare -r -i E_NORMAL=64;          erreur_descriptions[$E_NORMAL]='E_NORMAL: Mais qu est ce donc une erreur normale?';
    declare -r -i E_FILE_REQUIRE=65;    erreur_descriptions[$E_FILE_REQUIRE]='E_FILE_REQUIRE: Fichier requis introuvable.';
    declare -r -i E_ARG_BAD=66;         erreur_descriptions[$E_ARG_BAD]='E_ARG_BAD: Mauvais argument';
    declare -r -i E_ARG_REQUIRE=67;     erreur_descriptions[$E_ARG_REQUIRE]='E_ARG_REQUIRE: Argument requis non présent';

    declare -r -i E_INTERNE=70;         erreur_descriptions[$E_INTERNE]='Erreur interne (fonction vide, pb de permission,etc)';

    declare -r -i E_TRUE=0;             erreur_descriptions[$E_TRUE]='E_TRUE:Vraie';
    declare -r -i E_FALSE=97;           erreur_descriptions[$E_FALSE]='E_FALSE:Faux';
    declare -r -i E_NOT_ROOT=98;        erreur_descriptions[$E_NOT_ROOT]='E_NOT_ROOT: Neccessite un droit root.';
    declare -r -i E_UPDATE=99;          erreur_descriptions[$E_UPDATE]='E_UPDATE: indique que la mise à jours a été faite.';

    #100..113 (sortie de fonction)
    declare -r -i E_F0=100;             erreur_descriptions[$E_F0]="E_F0:Sortie d'une fonction avec l'erreur personalisée 100";
    declare -r -i E_F1=101;             erreur_descriptions[$E_F1]="E_F1:Sortie d'une fonction avec l'erreur personalisée 101";
    declare -r -i E_F2=102;             erreur_descriptions[$E_F2]="E_F2:Sortie d'une fonction avec l'erreur personalisée 102";
    declare -r -i E_F3=103;             erreur_descriptions[$E_F3]="E_F3:Sortie d'une fonction avec l'erreur personalisée 103";
    declare -r -i E_F4=104;             erreur_descriptions[$E_F4]="E_F4:Sortie d'une fonction avec l'erreur personalisée 104";
    declare -r -i E_F5=105;             erreur_descriptions[$E_F5]="E_F5:Sortie d'une fonction avec l'erreur personalisée 105";
    declare -r -i E_F6=106;             erreur_descriptions[$E_F6]="E_F6:Sortie d'une fonction avec l'erreur personalisée 106";
    declare -r -i E_F7=107;             erreur_descriptions[$E_F7]="E_F7:Sortie d'une fonction avec l'erreur personalisée 107";
    declare -r -i E_F8=108;             erreur_descriptions[$E_F8]="E_F8:Sortie d'une fonction avec l'erreur personalisée 108";
    declare -r -i E_F9=109;             erreur_descriptions[$E_F9]="E_F9:Sortie d'une fonction avec l'erreur personalisée 109";

    declare -r -i E_INODE_NOT_EXIST=110;erreur_descriptions[$E_INODE_NOT_EXIST]="E_INODE_NOT_EXIST: Erreur lors de l'accès à un fichier ou à un répertoire.";
    declare -r -i E_INODE_NOT_R=111;    erreur_descriptions[$E_INODE_NOT_R]="Fichier/repertoire non lisible (Not Readable)";
    declare -r -i E_INODE_NOT_W=112;    erreur_descriptions[$E_INODE_NOT_W]="Fichier/repertoire non inscriptible (Not Writable)";

    #>=200.255 FATAL_ERROR
    declare -r -i E_CTRL_C=200;         erreur_descriptions[$E_CTRL_C]='E_CTRL_C';
    declare -r -i E_CMD_CANT_EXEC=201;  erreur_descriptions[$E_CMD_CANT_EXEC]='E_CMD_CANT_EXEC';
    declare -r -i E_CMD_NOT_FOUND=202;  erreur_descriptions[$E_CMD_NOT_FOUND]='E_CMD_NOT_FOUND';


    #fonction erreur-set( erreurNo  erreur_info)
    # priorité: $1, $?
    function erreur-set() {
        erreur_no=${1:-$?};
        erreur_info="${2:-""}";
        erreur_description="${erreur_descriptions["$erreur_no"]:-'Pas de description.'}";
        return 0;
    }

    # priorité: $1,$erreur_no
    function erreur-no-display-var() {

        # si pas d'argument on utilise $?
        local -i _erreur_no=0;
        if [ $# -eq 1 ]
        then _erreur_no=$1;
        else _erreur_no=$erreur_no;
        fi
        local _txt="${erreur_descriptions["$_erreur_no"]:-'Pas de description.'}";
        display-vars $_erreur_no "$_txt";

        return 0;
    }


    # priorité: $1,$erreur_no
    # return _erreur_no (passthrought)
    function erreur-no-description-echo() {
        local -i _erreur_no=0;
        if [ $# -eq 1 ]
        then _erreur_no=$1;
        else _erreur_no=$erreur_no;
        fi

        local _color=$COLOR_OK;
        if [ $_erreur_no -eq $E_TRUE ]
        then _color=$COLOR_OK;
        else _color=$COLOR_WARN;
        fi
        local _out="";
        _out+="$_erreur_no:${erreur_descriptions[$_erreur_no]:-""}";
        if [ -n "$_out" ];then echo "$_color$_out$COLOR_NORMAL";fi
    
        return $_erreur_no;
    }


#

