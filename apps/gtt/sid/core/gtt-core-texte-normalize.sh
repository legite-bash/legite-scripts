echo-D "${BASH_SOURCE[0]}" 1;

##########################
# OBJET: texte-normalize #
##########################
# app-core-texte-normalize.sh 
# date de création    : ?
# date de modification: 2024.08.20
# Utilise: indépendant
# Description: 

declare -g  texte_normalize='';                                                                 # Texte normalisé en sortie de fonction
declare -g  texte_normalize_callback_before='';                 # callback avant   l'analyse
declare -g  texte_normalize_callback_reduce_before='';          # callback pendant l'analyse
declare -g  texte_normalize_callback_after='';                  # callback après   l'analyse

# - init - #
function texte-normalize-init(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    texte_normalize='';
    texte_normalize_callback_before='';
    texte_normalize_callback_reduce_before='';
    texte_normalize_callback_after='';

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


function texte-normalize-display-vars(){
    display-vars 'texte_normalize_callback_before       ' "$texte_normalize_callback_before";
    display-vars 'texte_normalize_callback_reduce_before' "$texte_normalize_callback_reduce_before";
    display-vars 'texte_normalize_callback_after        ' "$texte_normalize_callback_after";
    display-vars 'texte_normalize                       ' "$texte_normalize";
}


# - callback:Initialise les variables homonymes. ATTENTION: ce ne sont PAS les callback eux même - #
function texte-normalize-callback-before(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    texte_normalize_callback_before="${1:-""}";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

function texte-normalize-callback-reduce-before(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    texte_normalize_callback_reduce_before="${1:-""}";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

function texte-normalize-callback-after(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    texte_normalize_callback_after="${1:-""}";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# - core - #
function texte-normalize(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    # extglob
    #https://www.linuxjournal.com/content/bash-extended-globbing
    shopt -s extglob
    # , dotglob
    # https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html
    # https://www.gnu.org/software/bash/manual/html_node/Pattern-Matching.html


    texte_normalize="${1:-""}";
    #display-vars 'texte_normalize' "$texte_normalize";

    if [ "$texte_normalize" = '' ]
    then
        fct-pile-app-out $E_ARG_REQUIRE $_fct_pile_app_level; return $E_ARG_REQUIRE;
    fi

    #display-vars 'texte_normalize' "$texte_normalize";

    # - Callback - #
    if [ "${texte_normalize_callback_before}" != "" ]; then eval "${texte_normalize_callback_before}";fi
    #display-vars 'texte_normalize' "$texte_normalize";

    # - Core - #
    texte_normalize="$(echo $texte_normalize | tr -d '?')";
    texte_normalize="$(echo $texte_normalize | tr -d '!')";
    texte_normalize="$(echo $texte_normalize | tr -d '$')";
    texte_normalize="$(echo $texte_normalize | tr -d '@')";
    #texte_normalize="$(echo $texte_normalize | tr -d '#'");
    
    #texte_normalize="$(echo $texte_normalize | sed 's/[âäà]/a/g')";
    texte_normalize="${texte_normalize//à/a}";
    texte_normalize="${texte_normalize//â/a}";
    texte_normalize="${texte_normalize//â/a}";
    texte_normalize="${texte_normalize//ä/a}";
    
    #texte_normalize=$(echo $texte_normalize | sed 's/[ÂÀÂ]/a/g');
    texte_normalize="${texte_normalize//Â/A}";
    texte_normalize="${texte_normalize//À/A}";
    texte_normalize="${texte_normalize//Â/A}";
    
    #texte_normalize=$(echo $texte_normalize | sed 's/[éèêë]/e/g');
    texte_normalize="${texte_normalize//é/e}";
    texte_normalize="${texte_normalize//è/e}";
    texte_normalize="${texte_normalize//ê/e}";
    texte_normalize="${texte_normalize//ë/e}";

    #texte_normalize=$(echo $texte_normalize | sed 's/[ÉÈÊË]/E/g');
    texte_normalize="${texte_normalize//É/E}";
    texte_normalize="${texte_normalize//È/E}";
    texte_normalize="${texte_normalize//Ê/E}";
    texte_normalize="${texte_normalize//Ë/E}";

    #texte_normalize=$(echo $texte_normalize | sed 's/[îï]/i/g');
    texte_normalize="${texte_normalize//î/i}";
    texte_normalize="${texte_normalize//ï/i}";

    #texte_normalize=$(echo $texte_normalize | sed 's/[ÎÏ]/I/g');
    texte_normalize="${texte_normalize//Ï/I}";
    texte_normalize="${texte_normalize//Î/I}";

    #texte_normalize=$(echo $texte_normalize | sed 's/[ôö]/o/g');
    texte_normalize="${texte_normalize//ô/o}";
    texte_normalize="${texte_normalize//ö/o}";

    #texte_normalize=$(echo $texte_normalize | sed 's/[ÔÖ]/O/g');
    texte_normalize="${texte_normalize//Ô/O}";
    texte_normalize="${texte_normalize//Ö/O}";

    #texte_normalize=$(echo $texte_normalize | sed 's/[ûüù]/u/g');
    texte_normalize="${texte_normalize//û/u}";
    texte_normalize="${texte_normalize//ü/u}";
    texte_normalize="${texte_normalize//ù/u}";

    #texte_normalize=$(echo $texte_normalize | sed 's/[ÛÜÙ]/U/g');
    texte_normalize="${texte_normalize//Û/U}";
    texte_normalize="${texte_normalize//Ü/U}";
    texte_normalize="${texte_normalize//Ù/U}";

    #texte_normalize=$(echo $texte_normalize | sed 's/[çÇ]/c/g');
    texte_normalize="${texte_normalize//ç/c}";
    texte_normalize="${texte_normalize//Ç/C}";

    #texte_normalize=$(echo $texte_normalize | tr "œ" 'oe');
    texte_normalize="${texte_normalize//œ/oe}";

    #texte_normalize=$(echo $texte_normalize | tr "Œ" 'OE');
    texte_normalize="${texte_normalize//Œ/OE}";

    # avant '-'
    #texte_normalize=$(echo $texte_normalize | tr '’' '_');
    #texte_normalize=$(echo $texte_normalize | tr '′' '_');
    #texte_normalize=$(echo $texte_normalize | tr "'" '_');
    texte_normalize="${texte_normalize//[\’\′\']/_}";

    texte_normalize=$(echo $texte_normalize | sed 's/  */_/g');
    texte_normalize=$(echo $texte_normalize | sed -e "s/[ °]/_/g");

    # '-'
    texte_normalize="${texte_normalize//–/-}";
    texte_normalize="${texte_normalize//®/-}";
    texte_normalize="${texte_normalize//\"/-}";
    texte_normalize="${texte_normalize//\[/-}";
    texte_normalize="${texte_normalize//\]/-}";
    texte_normalize="${texte_normalize/«/-}";
    texte_normalize="${texte_normalize/»/-}";

    texte_normalize="$(echo $texte_normalize | sed -e "s/[\@\~#+\^\:;\`\{\}\(\)\|\,\=]/-/g")";
    
    ##texte_normalize="$(echo $texte_normalize | sed -e "s/@=]/-/g")";
    
    #texte_normalize=$(echo $texte_normalize | tr '&' '_et_');
    texte_normalize="${texte_normalize//&/_et_}";

    # - Callback - #
    if [ "$texte_normalize_callback_reduce_before" != "" ];then eval "$texte_normalize_callback_reduce_before";fi
    #display-vars 'texte_normalize' "$texte_normalize";


    # - reduction - #
    texte_normalize="${texte_normalize//.-./-}";
    texte_normalize="${texte_normalize//-./-}";
    texte_normalize="${texte_normalize//.-/-}";

    texte_normalize="${texte_normalize//_-_/-}";
    texte_normalize="${texte_normalize//-_/-}";
    texte_normalize="${texte_normalize//_-/-}";


    ##texte_normalize="$(echo $texte_normalize | sed -e 's/--*\././g')";
    texte_normalize="${texte_normalize//+(.)/.}";
    texte_normalize="${texte_normalize//+(-)/-}";

    ##texte_normalize="$(echo $texte_normalize | sed -e 's/__*\././g')";
    texte_normalize="${texte_normalize//+(_)/_}";

    #texte_normalize="$(echo $texte_normalize | sed 's/__*-__*/-/g')";
    #texte_normalize="${texte_normalize//}";

    #texte_normalize="$(echo $texte_normalize | sed 's/_-/-/g')";
    #texte_normalize="${texte_normalize//}";

    #texte_normalize="$(echo $texte_normalize | sed 's/-_/-/g')";
    #texte_normalize="${texte_normalize//}";

    #texte_normalize="$(echo $texte_normalize | sed 's/\._*/./g')";
    #texte_normalize="${texte_normalize//}";

    #texte_normalize="$(echo $texte_normalize | sed 's/__*/_/g')";
    #texte_normalize="${texte_normalize//}";

    #texte_normalize="$(echo $texte_normalize | sed 's/--*/-/g')";
    #texte_normalize="${texte_normalize/--*/-}";


    # - Suppression des caractères de debut - #
    texte_normalize="${texte_normalize#-}";
    texte_normalize="${texte_normalize#_}";

    # - Suppression des caractères de fin - #
    texte_normalize="${texte_normalize%.}";
    texte_normalize="${texte_normalize%-}";
    texte_normalize="${texte_normalize%_}";


    # - Callback - #
    if [ "$texte_normalize_callback_after" != "" ];then eval "$texte_normalize_callback_after";fi

    #display-vars 'texte_normalize' "$texte_normalize";

    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}


# Execute texte-normalize et affiche la variable texte_normalize
function texte-normalize-echo() {
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    local _texte="${1:-""}";
    if [ "$_texte" != "" ];  then texte-normalize "$_texte";fi      # Modifier la variable
    echo -n "$texte_normalize";                                     # Afficher la variables 

    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}


return 0;