echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.10.01
# Description: Fonctions génériques

#https://www.shellscript.sh/tips/echo/

# Certain systeme n'interprete pas le parametre -n de echo mais utilise a la place \c en fin de chaine
# definit les variables globales n et c selon si le systeme interprete le parametre -n  de echo
# exemple d'utilisation: echo $n "Enter your name: $c"
#if [ "$(echo -n)" = '-n' ]
#then n='';   c="\c";
#else n="-n"; c=''
#fi
#


########
# void #
########
    function void(){
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        #echo 'Je suis dans le néant!';

        fct-pile-gtt-out 0 $_fct_pile_gtt_level; return 0;
    }
#


################
# LEADING ZERO #
################
    declare texte_lz='';
    function texte-lz() {
        if [ $# -ne 1 ]
        then
            erreurs-pile-add-echo "${BASH_SOURCE[1]}:$LINENO:${FUNCNAME[0]}($@)  (\"codeYoutube\"). Appellé par ${FUNCNAME[1]}()"
            return $E_ARG_REQUIRE;
        fi
        texte_lz='';
        local str="$1"
        # https://neologfr.blogspot.com/2007/12/value-too-great-for-base.html
        local -i chiffre=10#$str;   # forcer la base 10 # le format de chiffre 0n est codé en octal

        #display-vars 'str' "$str"
        #display-vars 'chiffre' "$chiffre"

        texte_lz="$chiffre";
        if [ $chiffre -lt 10 ]
        then
            texte_lz="0$chiffre";
        fi
        #display-vars 'texte_lz' "$texte_lz"
        
        return 0;
    }
#


######
# HR #
######
    function HR(){
        car-duplique-n $terminal_largeur "${1:-"*"}";
        return 0;
    }

    function hr(){
        car-duplique-n $1 "${2:-"*"}";
        return 0;
    }
#


##########
# DIVERS #
##########

    function isNumeric(){
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;


        if [ $# -eq 0 ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}($@)  (\"valeur\"). Appellé par ${FUNCNAME[1]}()"
            fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;
        fi

        case $1 in
            *[!0-9]* | "") return $E_FALSE;  ;;
        esac


        fct-pile-gtt-out 0 $_fct_pile_gtt_level; return 0;
    }


    # Teste si le *premier caractère* de la chaîne est alphabétique.
    function isFirstAlphanumeric(){
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;


        if [ $# -eq 0 ]
        then
            erreurs-pile-add-echo "${BASH_SOURCE[0}}:$LINENO:${FUNCNAME[0]}($@)  (\"valeur\"). Appellé par ${FUNCNAME[1]}()"
            fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;
        fi

        case $1 in
            *[!a-zA-Z]*|"") return $E_FALSE;    ;;

        esac

        fct-pile-gtt-out 0 $_fct_pile_gtt_level; return 0;
    } 


    # active une pause par "press any key" si isPause=true
    function pause-hit() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if $isPause
        then
            local _txt="${1:-"Appuyer sur une Entrée"}";
            #echo "${_txt}";
            read -r -p "$_txt";
        fi

        fct-pile-gtt-out 0 $_fct_pile_gtt_level; return 0;
    }
#


#################
# texte-car-pos #
#################

    declare -gi  texte_car_pos=-1;
    #function texte-car-pos( 'texte' 'car' )
    # [1] est le 1er caractere    
    #addLibrairie 'texte-car-pos' "Recherche la position du caractère 'car' dans 'texte' puis met à jours 'texte_car_pos'. Index du 1èr car:1";
    function      texte-car-pos(){
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -ne 2 ]
        then
            erreurs-pile-add-echo "${BASH_SOURCE[0]}:$LINENO:${FUNCNAME[0]}($*)  (\"valeur\"). Appellé par ${FUNCNAME[1]}()";
            fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;
        fi


        if [ -z "$1" ] || [ -z "$2" ]
        then
            erreurs-pile-add-echo "${BASH_SOURCE[0]}:$LINENO:${FUNCNAME[0]}($*)  (\"valeur\"). Appellé par ${FUNCNAME[1]}()";
            fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;
        fi

        local    _chaine="$1";              # display-vars '_chaine' "$_chaine";
        local    _car="$2";                 # display-vars '_car' "$_car";
        local -i _chaine_lg=${#_chaine};    # display-vars '_chaine_lg' "$_chaine_lg";
        local -i _posNu=1;                  # cut compte a partir de 1
        local _carActuel='';
        texte_car_pos=-1;
        while :
        do
            if [ $_posNu  -gt $_chaine_lg ]; then break;fi
            #display-vars '_posNu' "$_posNu";
            _carActuel="$(echo $_chaine | cut -c$_posNu)";
            #display-vars "_carActuel" "$_carActuel";

            if [ "$_carActuel" = "$_car" ]
            then
                #echo "$_car trouvé!";
                texte_car_pos=$_posNu;
                fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
            fi

            ((_posNu++))
        done

        fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;
    }


    function texte-car-pos-echo(){
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -ne 2 ]
        then
            erreurs-pile-add-echo "${BASH_SOURCE[0}}:$LINENO:${FUNCNAME[0]}($@)  (\"valeur\"). Appellé par ${FUNCNAME[1]}()"
            fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;
        fi

        if [ -z "$1" ] || [ -z "$2" ]
        then
            erreurs-pile-add-echo "${BASH_SOURCE[0}}:$LINENO:${FUNCNAME[0]}($@)  (\"valeur\"). Appellé par ${FUNCNAME[1]}()"
            fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;
        fi

        echo -n "$texte_car_pos";

        fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;
    }



declare -gri TERMINAL_LARGEUR=$(tput cols);
declare -gi  terminal_largeur=TERMINAL_LARGEUR;

##################
# car-duplique-* #
##################
    # dupliqueNCar duplique $1 fois le car $2 [$3: echo:true]
    declare -g car_duplique_n='';
    function car-duplique-n() {
        local _isEcho=${3:-true};
        if [ $# -lt 2 ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}[$#/2](Nbre_de_car 'C' [echo=false])";
            return $E_ARG_REQUIRE;
        fi
        local -i _N=$1;
        local C="$2"
        local _out=''
        car_duplique_n='';
        for ((i = 0 ; i < $_N ; i++)); do car_duplique_n+="$C";done
        if $_isEcho; then echo "$car_duplique_n"; fi
        return 0;
    }


    # dupliqueNCar duplique $1 fois le car $2 avec un maximum la largeur du terminal
    function car-duplique-max_term() {
        if [ $# -ne 2 ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}[$#/2](Nbre_de_car 'C') ($*)";
            return $E_ARG_REQUIRE;
        fi

        local -i _N=$1;
        local C="$2";
        local _out='';
        for (( i = 0 ; i < $_N ; i++))
        do
            if [ $i -gt $terminal_largeur ];then break; fi
            _out="$_out$C";
        done
        echo "$_out"
        return 0;
    }
#

#####################
# texte-justify-left #
#####################
declare -g texte_justify_left='';
# Ajoute des espace en fin de l'justify pour que le fin du texte s'aligne sur le bord droit du terminal
# function texte-justify-left "ORIGINAL_TXT" "APPEND_TXT"
function texte-justify-left(){
    #local _ORIGINAL_TXT="${1:-""}";
    local _APPEND_TXT="${1:-""}";
    local _MOTIF="${2:-" "}";   # Par defaut motif espace
    local -ri _APPEND_LG=${#_APPEND_TXT};
    local -i  _diff_lg=0;         # Difference entre la taille du teste de la derniere ligne et la largeur du terminal
    texte_justify_left='';
    #display-vars "TERMINAL_LARGEUR        " "$TERMINAL_LARGEUR";
    #display-vars '_ORIGINAL_LG            ' "$_ORIGINAL_LG";
    #echo "$_ORIGINAL_TXT";
    #display-vars "_APPEND_TXT:$_APPEND_LG" "$_APPEND_TXT";

    # L'ajout est inféreiur à la largeur du terminal
    if [ ${_APPEND_LG} -lt $TERMINAL_LARGEUR ]
    then
        local -i _diff_lg=$(( TERMINAL_LARGEUR - _APPEND_LG  ));
        car-duplique-n $_diff_lg "$_MOTIF" false;
        texte_justify_left="$_APPEND_TXT$car_duplique_n";
        return 0;
    fi

    if [ ${_APPEND_LG} -eq $TERMINAL_LARGEUR ]
    then
        texte_justify_left="$_APPEND_TXT";
        return 0;
    fi

    # L'ajout est dépasse ou est égale à la largeur du terminal
        local -i  _APPEND_LIGNE_NB=$(( (_APPEND_LG      / TERMINAL_LARGEUR )  ));
        local -i  _APPEND_PART_1_LG=$(( TERMINAL_LARGEUR * _APPEND_LIGNE_NB));      # Longueur des lignes entieres
        local -i  _APPEND_PART_2_LG=$(( _APPEND_LG       - _APPEND_PART_1_LG ));    # Longueur du texte de la derniere ligne
        _diff_lg=$(( TERMINAL_LARGEUR - _APPEND_PART_2_LG ));
        #display-vars '_APPEND_LIGNE_NB' "$_APPEND_LIGNE_NB";
        #display-vars "_APPEND_PART_1_LG      " "$_APPEND_PART_1_LG" "_APPEND_PART_2_LG" "$_APPEND_PART_2_LG" '_diff_lg' "$_diff_lg";
        car-duplique-n $_diff_lg "$_MOTIF" false;
        texte_justify_left="$_APPEND_TXT$car_duplique_n";
}



return 0;