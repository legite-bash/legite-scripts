echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


######################
# AFFICHAGE DES APPS #
######################
    # - Affiche les applications du repertoire '$APPS_REP' - #
    # si --isShowApps=true
    function showApps() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $isShowApps = false ];then fct-pile-gtt-out 0 $_fct_pile_gtt_level; return 0;fi

        titre1 "Les applications ($APPS_REP)";
        if [ ! -d "$APPS_REP" ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}:Le repertoire '$APPS_REP' existe pas.";
            fct-pile-gtt-out $E_INODE_NOT_EXIST $_fct_pile_gtt_level; return $E_INODE_NOT_EXIST;
        fi

        #titre2_D "Chargement des sous rep via find" 1;
        # _repListe: tableau contenant les chemins COMPLET des repertoires
        local -a _repListe=$(find "$APPS_REP" -type d );  #find sous repertoires

        # calcul du nombre de sous repertoire de $APPS_REP
        local    _slashs="${APPS_REP//[^\/]}";      # on ne garde que les slash (/)
        local -i _APPS_REP_sousRepNb=${#_slashs};

        local -i _repNb=0;    
        #titre2_D "Trie des sous repertoires" 1;
        for _rep in $(sort <<< "${_repListe[*]}") # NE PAS QUOTER sinon renvoi une seule chaine de caractere
        do
            if $isTrapCAsk;then fct-pile-gtt-out $E_CTRL_C $_fct_pile_app_level; return $E_CTRL_C; fi

            # ne pas afficher la 1ere ligne
            ((_repNb++ ))
            if [ $_repNb -eq 1 ];then continue; fi
            #display-vars '_rep' "$_rep";

            # - forme raccourci - #
            # calcul du nombre de sous repertoire
            #_slashs="${_rep//[^\/]}";      # on ne garde que les slash (/)
            #local -i _sousRepNb=${#_slashs};
            #_sousRepNb=$((_sousRepNb-_APPS_REP_sousRepNb));
            #local _sep="$(car-duplique-n $_sousRepNb '-')";
            #echo -n "$_sep";

            # - forme chemin relatif - #
            local _repRel="${_rep#$APPS_REP/}";  # supprime le root
            _repRel="${_repRel%/*}/";                   # on supprime le dernier repertoire
            #display-vars '_repRel' "${_repRel:0:4}";
            #echo -n $_repRel;
            #display-vars '_repRel' "$_repRel" '_rep' "$_rep";

            if [ "${_repRel:0:4}" != 'gtt/' ];then fs-ls-sh "$_rep" "$_repRel";fi
        done

        fct-pile-gtt-out 0 $_fct_pile_gtt_level; return 0;
    }
#


#######################
# CHARGEMENT DES APPS #
#######################

    #loadApp( '_PSPActuel' )
    # Charge une application si le fichier homonyme.sh existe dans $APPS_REP
    # charge autommatiquement la librairie homonyme si elle est défini par addLibrairie ET si $isAppExecLibSelf=1
    # sauf si --no-auto-lib=1 est défini
    # code retour:
    # $E_TRUE: L'application est chargée
    # $E_FALSE: lib seule ou erreur
    function loadApplication() {
        if $isTrapCAsk;then return $E_CTRL_C;fi

        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;


        titre2_D "${FUNCNAME[0]}($*)" 2;
        if [ $# -ne 1 ]
        then
            titre2_D "${FUNCNAME[0]}($*):END" 2;
            fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;
        fi

        applicationNom="$1";              # global

        if [ -z "$applicationNom" ]
        then
            titre2_D "${FUNCNAME[0]}($*):END" 2;
            fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;
        fi

        # Ajout de l'extention si non présente
        if [ ! "${applicationNom: -3}" = ".sh" ];then applicationNom+=".sh";fi
        #display-vars 'applicationNom' "$applicationNom";

        ##########################################
        titre3_D "L'application existe t'elle?" 2;
        ##########################################        
        if isApplicationExist "$applicationNom"
        then
            echo-D "Oui, l'application existe." 3;
        else
            echo-D "'$applicationNom' n'est pas une application" 3;
            titre2_D "${FUNCNAME[0]}($*):END" 2;
            fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;
        fi


        #######################################################
        titre3_D "L'application a t'elle déjà été chargée ?" 2;
        #######################################################
        if isApplicationExec "$applicationNom"
        then
            titre2_D "${FUNCNAME[0]}($*):END" 2;
            fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
        fi
        echo-D "Non, l'application n'a jamais été executée." 3;


        ###################################################
        titre3_D "CHARGEMENT des ini.cfg (REPERTOIRES)" 2 #
        ###################################################
        local _sousRep="${applicationNom%/*}";
        searchRepertoiresCfg "$_sousRep";
        unset _sousRep;


        #################################################
        titre3_D "Est-ce une application existante ?" 2 #
        #################################################
        erreur_no=$E_FALSE;
        if isApplicationExist "$applicationNom" # formes possibles 'rep/app' ou 'app'
        then
            echo-D "Oui, l'application' existe." 3;

            # Chargement du fichier de configuration de l'application
            loadApplicationCfg "$applicationNom";
            
            # Chargement de l'application
            local _applicationPath="$APPS_REP/$applicationNom";
            exec-file-once "$_applicationPath";
            erreur_no=$?;
            # marquer l'application comme chargée même en cas d'erreur pour éviter de la recharger
            setApplicationExec "$applicationNom" 1;
            if [ $erreur_no -eq $E_TRUE ]\
            || [ $erreur_no -eq $E_FALSE ]
            then # l'application est chargée

                # Execution de la librairie homonyme (autoexecution)
                #display-vars 'isAppExecLibSelf' "$isAppExecLibSelf";
                if $isAppExecLibSelf # gestion d'un booléen [0|1]
                then
                    local _lib="${applicationNom##*/}";
                    display-vars "$LINENO:Execution automatique de la librairie homonyme ($_lib)";
                    execLib "$_lib";
                    erreur_no=$?; # pas d'erreur: la lib est executé (ou non)
                fi
            else
                erreurs-pile-add-echo "${FUNCNAME[0]}: Erreur lors du chargement de l'application existante '$_applicationPath'";            
                erreurs-pile-add-echo "Erreur ${COLOR_INFO}$(erreur-description-echo)${COLOR_NORMAL}"
            fi
        fi
        titre2_D "${FUNCNAME[0]}($*):END" 2;
        fct-pile-gtt-out $erreur_no $_fct_pile_gtt_level; return $erreur_no;
    }
#


###########################################
# CHARGEMENT DE LA CONFIGURATION DE L'APP #
###########################################
    # loadApplicationCfg ( '[appNom/]appNom' )
    function loadApplicationCfg(){
        if $isTrapCAsk;then return $E_CTRL_C;fi

        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -ne 1 ];then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;fi
        erreur_no=$E_FALSE;

        local _confNom="$1.cfg";  #    [rep/]application.cfg
        local _confPath="$APPS_REP/$_confNom.sh";


        titre3_D "${FUNCNAME[0]}():La configuration de l'application '$_confNom' a t'elle déjà été chargée?" 2;
        if isApplicationExec "$_confNom"
        then
            echo-D "Oui, elle a déjà été chargée." 3;
            fct-pile-gtt-out 0 $_fct_pile_gtt_level; return 0;
        fi
        echo-D "Non. Elle n'a jamais été chargée" 3;


        titre3_D "l'apllication existe t'elle?" 3;
        if isApplicationExist "$_confNom"
        then # l'application existe
        echo "${BASH_SOURCE[0]}:$LINENO: (exec-file $_confPath)"
            exec-file "$_confPath";     # Chargement de la configuration
            erreur_no=$?;
            if [ $erreur_no -eq 0 ]
            then # la configuration est chargée
                echo-D  "La configuration de l'application '$_confNom' est chargée." 3;
                setApplicationExec "$_confNom" 1;       # La config est chargée..
                fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
            else
                erreurs-pile-add-echo "Erreur lors du chargement de l'application existante '$_confPath'";
            fi
        else
            echo-D "Non, elle n'existe pas." 3;
        fi
        fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;

    }

    #isAppOnly ( 'nom' )    # app ou lib
    # renvoie $E_TRUE si l'argement est une app MAIS PAS une lib
    function isAppOnly(){
        if [ $# -ne 1 ];then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE; fi
        local _nom="$1";
        isApplicationExist        "$_nom";    local -i _isApplicationExist=$?;
        isCollectionExist "$_nom";    local -i _isCollectionExist=$?;
        isLibExist        "$_nom";    local -i _isLibExist=$?;
            #display-vars '_nom' "$_nom" '_isCollectionExist' "$_isCollectionExist" '_isLibExist' "$_isLibExist";

        if [ $_isCollectionExist -ne $E_TRUE ]\
        && [ $_isApplicationExist        -eq $E_TRUE ]\
        && [ $_isLibExist        -ne $E_TRUE ]
        then
            fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;
        fi
        fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;
    }

    #function isApplicationExist( '_app' )
    # Verifie l'existance du repertoire de l'app donné en param et le rend lisible et executable si neccessaire
    # renvoie E_TRUE si le parametre est un fichier app lisible et executable
    # sinon $E_INODE_NOT_EXIST, $E_INODE_NOT_R, E_INODE_NOT_X
    function isApplicationExist(){
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -eq 0 ]; then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;fi
        local _PathRel="$1"
        local _Path="$APPS_REP/$_PathRel";
        titre3_D "le repertoire '$_Path' existe t'il?" 2;
        #if [ -d "$APPS_REP/$_PathRel" ]
        #then
        #    echo-D "$APPS_REP/$_PathRel est un repertoire d'application.";
        #    fctOut "${FUNCNAME[0]}($E_TRUE)" $_fctIn_lvl; return $E_TRUE;
        #fi

        titre3_D "le fichier '$_Path' existe t'il?" 2;
        if [ -f "$_Path"  ]
        then
            echo-D "Oui, le fichier existe." 3;
        else            
            echo-D "Non  le fichier n'existe pas" 3;
            fct-pile-gtt-out $E_INODE_NOT_EXIST $_fct_pile_gtt_level; return $E_INODE_NOT_EXIST;
            fi

        titre3_D "le fichier est il readable?" 3;
        if [ -r "$_Path"  ]
        then
            echo-D "Oui, le fichier est lisible" 3;
        else
            echo-D "Non, le fichier n'est pas lisible" 3;
            if chmod +r "$_Path"
            then
                echo-D "Le fichier a été fixé à lisible (+r)" 3;
            else
                erreurs-pile-add-echo "Le fichier '$_Path' ne peut être fixé à lisible (+r)"
                fct-pile-gtt-out $E_INODE_NOT_R $_fct_pile_gtt_level; return $E_INODE_NOT_R;
            fi
        fi

        titre3_D "le fichier est il executable?" 3;
        if [ -x "$_Path"  ]
        then
            echo-D "Oui, le fichier est excutable." 3;
        else
            echo-D "Non, le fichier n'est pas executable" 3;
            if chmod +x "$_Path"
            then
                echo-D "Oui, le fichier est devenu executable" 3;
            else
                erreurs-pile-add-echo "Le fichier '$_Path' ne peut être fixé à executable (+x)"
                fct-pile-gtt-out $E_INODE_NOT_X $_fct_pile_gtt_level; return $E_INODE_NOT_X;
                fi
        fi

        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }

    function msgApplicationFail(){
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $isAppFail -eq 1 ]
        then
            erreurs-pile-add-echo "L'app '$appNom' a eu une erreur critique. Les librairies de cette app sont désactivées.";
        fi

        fct-pile-gtt-out 0 $_fct_pile_gtt_level; return 0;
    }

    function setApplicationFail(){
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        local -i _is=${1:-1};
        isAppFail=$_is;
        display-vars-D 'isAppFail' "$isAppFail";

        fct-pile-gtt-out 0 $_fct_pile_gtt_level; return 0;
    }
#


###################################
# TABLEAU DES APPS DEJA EXECUTÉES # 
###################################
    function showAppExec(){
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        local _datas='';
        for key in "${!appExecTbl[@]}"
        do
            _datas+=", '$key':'${appExecTbl[$key]}' ";
        done
        display-tableau-datas "appExecTbl[@]" "${_datas#?}"; # supppresion du 1er car(',')

        fct-pile-gtt-out 0 $_fct_pile_gtt_level; return 0;
    }


    # Indique que l'app a déjà été executée (ou non).
    # $1: nom de la lib
    # $2(option):
    # 0: L'app n'a jamais été executé
    # 1: L'app a déjà été executé
    function setApplicationExec(){
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -eq 0 ];then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;fi

        local   _appNom="$1";           # requis
        local -i _value="${2:-1}";      # setter a 1 si non précisé
        appExecTbl["$_appNom"]=$_value;

        fct-pile-gtt-out $_value $_fct_pile_gtt_level; return $_value;
    }


    # renvoie $E_TRUE (0)  si l'app a deja été executé
    # renvoie $E_FALSE(97) si l'app n'a jamais été executé
    # $1: nom de l'app
    function isApplicationExec(){
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -ne 1 ];then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;fi

        local   _appNom="$1";         # requis
        if [ ${appExecTbl["$_appNom"]:-0} -eq 0 ]
        then fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;
        else fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level;  return $E_TRUE;
        fi
    }
#