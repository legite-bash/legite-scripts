echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.19
# Description: 



################
# MISE A JOURS #
################
    # telecharge un fichier et archive la version local
    # updateFile ("src" "dest") # chemin absolu
    # E_F1: en cas d'echec
    function gtt_updateFile() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [[ $# -ne 2 ]]
        then
            titreWarn "Le nombre d'argument requis est: $#/2"
            fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE;
        fi
        local src="$1"; local dest="$2";
        titre3 "Update de '$src'"

        local _dbak=$(date +'%m-%d-%HH%Mm');
        eval-echo "cp '$dest' '$dest-$_dbak.bak'";     # Fait une sauvegarde
        eval-echo "rm '$dest'";

        eval-echo "wget -O '$dest' '$src'"
        eval-echo "chmod +x '$dest'"
        if [ $? -ne 0 ]
        then
            erreurs-pile-add-echo "Erreur lors de l'installation de $dest."
            eval-echo "mv '$dest.bak' '$dest'"
            fct-pile-gtt-out $E_F1 $_fct_pile_gtt_level; return $E_F1;
        fi

        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }

    # - telecharge et installe la derniere version dans le repertoire du script en remplacant le script qui a lancer l'update - #
    function gtt_update () {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if $isUpdate
        then
            titre1 "mise a jours du programme";
            #if [ ! -d "$COLLECTIONS_REP" ];then mkdir -p "$COLLECTIONS_REP"; fi
            if ! cd "$TEMP_ROOT"
            then
                erreurs-pile-add-echo "Impossible d'aller dans le repertoire temporaire '$TEMP_ROOT'";
                exit $E_INODE_NOT_EXIST;
            else
                wget http://updates.legite.org/gtt/installGTT.sh;
                chmod +x installGTT.sh;
                if [ "$(./installGTT.sh)" = 'asulex' ] 
                then
                    erreurs-pile-add-echo "Instalation impossible sur la machine de dev";
                    fct-pile-gtt-out $E_FALSE $_fct_pile_gtt_level; return $E_FALSE;
                fi
                if ! ./installGTT0.sh
                then
                    erreurs-pile-add-echo "Erreur lors de l'execution de './installGTT.sh'";
                else
                    
                    notifsadd-echo "Mise à jours de $GTT_VERSION vers $($BASH_SOURCE -V)";
                    selfUpdateLocal;
                fi
            fi


            exit $E_UPDATE; # sortie normal avec indicateur E_UPDATE
        fi

        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }

    # a surcharger par le projet
    function selfUpdateLocal() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if $isUpdate
        then
            titre3 "mise a jours specifique au programme";

            if $IS_ROOT
            then
                echo-D '';
            fi
        fi

        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }
#





return 0;