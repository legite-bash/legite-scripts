echo-D "${BASH_SOURCE[0]}" 1;

# gtt-core-PSP-rep-cfg.sh 
# date de création    : ?
# date de modification: 2024.08.22
# Description: 


####################################################
# searchRepertoiresCfg: Chargement des _ini.cfg.sh #
####################################################

##
# Execute loadRepertoiresCfg() avec le repertoire '$APPS_REP' puis avec le repertoire '$COLLECTIONS_REP'
# $1: repertoire relatif (Il ajoutera $APPS_REP puis $COLLECTIONS_APP)
# Les _ini.cfg.sh seront cherchés dans les repertoires parents
function searchRepertoiresCfg() {
    local -i _fct_pile_gtt_level=2;
    fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

    if [ $# -eq 0 ]; then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE; fi

    local _confRep="${1:-""}";           # chemin relatif (APP/col) ou absolu
    if [ "$_confRep" = "" ]; then fct-pile-gtt-out $E_ARG_REQUIRE $_fct_pile_gtt_level; return $E_ARG_REQUIRE; fi
    #display-vars '_confRep' "$_confRep";

    local    _isLoad=false;        # true si au moins un _ini.cfg.sh est chargé
    local -i _lastErrNu=0;

    echo-D "==== Chargement d'un repertoire d'app ====" 2;
    #debug-gtt-level-save 2;
    #debug-app-level-save 1;
    loadRepertoiresCfg "$_confRep" "$APPS_REP";
    if [ $? -eq $E_TRUE ];then _isLoad=true; fi
    #debug-gtt-level-restaure;
    #debug-app-level-restaure;


    echo-D "==== Chargement d'un repertoire de collection ====" 2;
    loadRepertoiresCfg "$_confRep" "$COLLECTIONS_REP";
    if [ $? -eq $E_TRUE ];then _isLoad=true; fi

    
    #display-vars-D "_isLoad" "$_isLoad" 1;
    if $_isLoad # au moins 1 fichier de chargé
    then erreur-set $E_TRUE;
    else erreur-set $E_FALSE;
    fct-pile-gtt-out $erreur_no $_fct_pile_gtt_level; return $erreur_no;
    fi
}


##
# Lecture recursive des répertoires parents du repertoire cible
# loadRepertoiresCfg( '_confRep' , '_base_rep'){
# $1 : 
#  - chemin absolu  du repertoire: dans ce cas ne gere pas la gestion des APPS ou COLLECTIONS)
#  - chemin relatif du repertoire: dans ce cas l'arug $_base_rep ($2) est préfixé
# $2: _base_rep: $APPS_REP|$COLLECTIONS_REP
# retour:
# E_TRUE: Si au moins 1 fichier _ini.cfg.sh est chargé (mais pas forcément celui du repertoire cible)
# E_FALSE:si aucun fichier _ini.cfg.sh est chargé
function loadRepertoiresCfg() {
    local -i _fct_pile_gtt_level=2;
    fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

    if [ $# -eq 0 ];            then erreur-set $E_ARG_REQUIRE;fct-pile-gtt-out $erreur_no $_fct_pile_gtt_level; return $erreur_no; fi

    local _confRep="${1:-""}";           # chemin relatif (APP/col) ou absolu
    if [ "$_confRep" = "" ];    then erreur-set $E_ARG_REQUIRE;fct-pile-gtt-out $erreur_no $_fct_pile_gtt_level; return $erreur_no; fi 

    local _base_rep="${2:-""}";
    #display-vars '_confRep' "$_confRep" '(_base_rep' "$_base_rep)";

    local _confSousRep="${_confRep%/*}";
    #display-vars '_confSousRep' "$LINENO:$_confSousRep";


    # - Split des sous repertoires - #
    local -a repTbl=();
    IFS='/';read -ra repTbl <<< "$_confRep";IFS="$IFS_DEFAULT";
    #display_tableau 'repTbl[@]' "${repTbl[@]}";  # pour verifier l'ordre

    # - Analyse des sous repertoires - #
    local _repCumul="";
    local -i _loadRepertoiresNb=0; # Nb de repertoire chargés
    for rep in "${repTbl[@]}"       # ATTENTION:n'est pas forcement l'ordre reel!
    do
        if [ "$rep" = "" ]; then continue; fi
        _repCumul+="/$rep";
        _repCumul="${_repCumul#/}"; # suppression du 1er slash
        #display-vars 'rep' "$rep" '_repCumul' "$_repCumul";
        
        echo-D "$LINENO: est ce que '$_base_rep/$_repCumul' est un repertoire?" 3;
        if [ ! -d "$_base_rep/$_repCumul" ]
        then
            echo-D "'$_base_rep/$_repCumul' n'est pas un repertoire" 3;
        else
            #display-vars "$LINENO:$_base_rep/$_repCumul" "est un sous repertoire";
            echo-D "$LINENO: loadRepertoireCfg '$_base_rep/$_repCumul';"  3;
            if loadRepertoireCfg "$_base_rep/$_repCumul"
            then ((_loadRepertoiresNb++))
            fi

        fi
    done
    if [ $_loadRepertoiresNb -eq 0 ]
    then erreur-set $E_TRUE;
    else erreur-set $E_FALSE;
    fct-pile-gtt-out $erreur_no $_fct_pile_gtt_level; return $erreur_no;
    fi
}
#


##
# Chargement du fichier de configuration (_ini.cfg.sh) d'un repertoire
# function loadRepertoireCfg( '_confRoot' ) #
# $1 : chemin absolu du repertoire (ne gere pas la gestion des APPS ou COLLECTIONS)
# retour: $E_TRUE si (déjà) chargé

function loadRepertoireCfg() {
    local -i _fct_pile_gtt_level=2;
    fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

    if [ $# -eq 0 ];            then erreur-set $E_ARG_REQUIRE;fct-pile-gtt-out $erreur_no $_fct_pile_gtt_level; return $erreur_no; fi

    local _confRoot="${1:-""}";           # chemin relatif (APP/col) ou absolu
    if [ "$_confRoot" = "" ];   then erreur-set $E_ARG_REQUIRE;fct-pile-gtt-out $erreur_no $_fct_pile_gtt_level; return $erreur_no; fi
    local _confPath="$_confRoot/_ini.cfg.sh";
    #display-vars "$LINENO:_confPath" "$_confPath";

    exec-file-once "$_confPath";                      # Path absolu
    erreur-set $?;

    fct-pile-gtt-out $erreur_no $_fct_pile_gtt_level; return $erreur_no;
}


return 0;