echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.22
# Description: 


##
# Rend executable un fichier et Execute une seul fois un fichier
# exec-file(_input_pathname){
# return:
# # $E_ARG_BAD: $1 est vide
# # $E_INODE_NOT_EXIST: existe pas ou n'est pas un fichier
# # le code retour du fichier source executé
function exec-file() {
    local -i _pile_fct_gtt_level=2;
    fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

    echo-D "${FUNCNAME[0]}($*):" 2;


    local  _input_pathname="${1:-""}";

    if [ -z "$_input_pathname" ]
    then
        echo-D "${FUNCNAME[0]}($*):END" 2;
        fct-pile-gtt-out $E_ARG_REQUIRE $_pile_fct_gtt_level; return $E_ARG_REQUIRE;
    fi
    if [ ! -f "$_input_pathname" ]
    then
        echo-D "'$_input_pathname' n'existe pas." 3;
        echo-D "${FUNCNAME[0]}($*):END" 2;
        fct-pile-gtt-out $E_INODE_NOT_EXIST $_pile_fct_gtt_level; return $E_INODE_NOT_EXIST;
    fi

    if [ ! -x "$_input_pathname" ];then eval-echo "chmod +x $_input_pathname;"; fi
    # shellcheck source=/dev/null
    
    source "$_input_pathname"; # source ecrit le path du fichier dans le flux! Mais pas toujours!
    erreur-set;
    if [ $erreur_no -eq 0 ]\
    || [ $erreur_no -eq $E_FALSE ] # $E_FALSE NE provoque PAS d'erreur
    then
        echo-D "Le fichier '$_input_pathname' A BIEN ETE executé." 3;
    else
        if ! $isExecFileQuiet
        then
            erreurs-pile-add-echo "${FUNCNAME[0]}: Problème lors du chargement du fichier: '$_input_pathname'";
        fi
    fi

    fct-pile-gtt-out $erreur_no $_pile_fct_gtt_level; return $erreur_no;
}

# Execute une seule fois un fichier
# exec-file-once(_input_pathname){
function exec-file-once() {
    local -i _pile_fct_gtt_level=2;
    fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

    echo-D "${FUNCNAME[0]}($*)" 2;
    local _input_pathname="${1:-""}";
    if [ "$_input_pathname" = '' ]
    then
        echo-D "${FUNCNAME[0]}($*):$LINENO:$E_ARG_BAD:END" 2;
        fct-pile-gtt-out $E_ARG_REQUIRE $_pile_fct_gtt_level; return $E_ARG_REQUIRE;
    fi

    if [ ! -e "$_input_pathname"  ]
    then
        echo-D "${FUNCNAME[0]}($*):$LINENO:$E_INODE_NOT_EXIST:END" 2;
        fct-pile-gtt-out $E_INODE_NOT_EXIST $_pile_fct_gtt_level; return $E_INODE_NOT_EXIST;
    fi

    _input_pathname="$(realpath "$_input_pathname")"; # cible le reel fichier en cas d'appel par lien (ou autre)
    local -i _isExecOnce=${execOnceTbl["$_input_pathname"]:-$E_FALSE};
    #erreur-set $_isExecOnce;

    if [ $_isExecOnce -eq $E_FALSE ]
    then
        #display-vars '_input_pathname' "$_input_pathname";
        #display-vars '_input_pathname' "$_input_pathname";
        echo-D "Le fichier '$_input_pathname' n'a JAMAIS ETE executé" 3;
        echo-D "exec-file ('$_input_pathname')" 3;
        # marqué comme lu pour éviter qu'il puisse s'appeller lui même
        execOnceTbl["$_input_pathname"]=$E_TRUE;
        # executer le fichier
        exec-file "$_input_pathname";
        erreur-set $?;
        echo-D "exec-file (): code retour '$erreur_no')" 3;
        if [ $erreur_no -eq $E_TRUE ]\
        || [ $erreur_no -eq $E_FALSE ]  # E_FALSE Ne provoque pas d'erreur
        then
            execOnceTbl["$_input_pathname"]=$E_TRUE;
            echo-D "${FUNCNAME[0]}($*):END" 2;
            erreur-set $E_TRUE;
            fct-pile-gtt-out $erreur_no $_pile_fct_gtt_level; return $erreur_no;
        fi
    else
        echo-v "Le fichier '$_input_pathname' a DEJA ETE executé" 1;
    fi
    echo-D "${FUNCNAME[0]}($*):END" 2;
    erreur-set $E_FALSE;
    fct-pile-gtt-out $erreur_no $_pile_fct_gtt_level; return $erreur_no;
}


# Charge un fichier et met fin au programme en cas d'échec.
function require() {
    local -i _pile_fct_gtt_level=2;
    fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

    local localName="${1:-""}";
    if [    "$localName" = '' ]; then fct-pile-gtt-out $E_ARG_REQUIRE $_pile_fct_gtt_level; return $E_ARG_REQUIRE; fi
    if [ -f "$localName"      ]
    then
        if [[ ! -x "$localName" ]];then eval-echo "chmod +x $localName;"; fi
        #echo "nano $localName # Pour modifier le fichier"
        source $localName;
        local -i _erreur_no=$?;
        if [ $_erreur_no -eq 0 ]
        then
            #notifsAdd "Le fichier requis '$localName' est chargé";
            erreur-set $E_TRUE;
            fct-pile-gtt-out $erreur_no $_pile_fct_gtt_level; return $erreur_no;
        fi

        erreur-set $E_FALSE;
        fct-pile-gtt-out $_erreur_no $_pile_fct_gtt_level; return $_erreur_no;
        
    fi
    erreurs-pile-add-echo "Le fichier requis '$localName' est introuvable";
    lanceur-standard-end;
    
    erreur-set $E_FILE_REQUIRE;
    fct-pile-gtt-out $erreur_no $_pile_fct_gtt_level; exit $erreur_no;
}


###
# $1: fichier à charger
# return:
# # $E_INODE_NOT_EXIST: 
#
function require-once() {
    local -i _pile_fct_gtt_level=2;
    fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

    local _input_pathname="${1:-""}";
    local -i _isExecOnce=${execOnceTbl["$_input_pathname"]:-$E_FALSE};
    if [ $_isExecOnce -eq $E_FALSE ]
    then
        # marquer comme lu pour éviter qu'il puisse s'appeller lui même
        execOnceTbl["$_input_pathname"]=$E_TRUE;
        require "$_input_pathname";
        if [ $? -eq $E_TRUE ]
        then
            execOnceTbl["$_input_pathname"]=$E_TRUE;
        fi
            erreur-set $E_TRUE;
            fct-pile-gtt-out $erreur_no $_pile_fct_gtt_level; return $erreur_no;
    else
        fct-pile-gtt-out $E_TRUE $_pile_fct_gtt_level; return  $E_TRUE;  # Déjà chargé
    fi

    erreur-set $E_INODE_NOT_EXIST;
    fct-pile-gtt-out $erreur_no $_pile_fct_gtt_level; exit $erreur_no;
}


return 0;