echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.09.17
# Description: 



#################
# psp-analyse() #
#################
    function psp-analyse(){
        #titre1_D "psp-analyse" 1;
        #echo '$@     :'"$@";
        #display_tableau '${ARG_TBL[@]}' "${ARG_TBL[@]}";

        #declare -i  _argNb=0;
        #_PSP=$(getopt \
        #    --options hVUvBDdTRVhnS- \
        #    --long help,version,update,backupGTT,show-vars,\
        #show-apps,show-collections,show-libsLevel1,show-libsLevel2,libExecAuto,libExecNoAuto,\
        #show-apps-libs,show-collections-libs,show-all-libs,\
        #show-duree,realpath\
        #,pause \
        #-- "${ARG_TBL[@]}" 2>/dev/null);

        _PSP=$(getopt \
            --options hVUvBDdTRVhnS- \
            --longoptions help,version,update,backupGTT,show-vars,show-apps,show-collections,show-libsLevel,show-libsLevel1,show-libsLevel2,libExecAuto,libExecNoAuto,show-apps-libs,show-collections-libs,show-all-libs,show-duree,realpath,pause \
            -- "${ARG_TBL[@]}" );

        eval set -- "$_PSP"
        #display-vars 'psp-analyse: _PSP' "$_PSP";
        unset _PSP;

        #echo "";
        while true
        do
            if $isTrapCAsk;then break;fi
            _argument="${1:-""}";
            _parametre="${2:-""}";

            #titre4 "$_argument";
            if [ "${_argument: -1}" = "," ] || [ "${_argument}"     = "--" ]
            then
                #echo ',';
                shift 1;
                continue;
            fi
            #display-vars 'psp-analyse: _argument' "$_argument" '_parametre' "$_parametre";
            if [ -z "$_argument" ];then break;fi 

            #((_argNb++))
            case "$_argument" in

                # - fonctions generiques - #
                --realpath)   echo "$SCRIPT_REP"; exit 0;   ;;
                -h|--help)    ((aide_level++));              ;;
                -V|--version)
                    isVersion=true;
                    echo "$GTT_VERSION";
                    ;;
                -v) ((debug_verbose_level++));                      ;;
                --backupGTT) ((backupGTTLevel++));           ;;
                -B) ((backupLevel++));                       ;;

                -D) ((debug_gtt_level++));     isShowDuree=true; ;;    #isShowVars=1;
                -d) ((debug_app_level++));                       ;;

                -T) ((testLevel++));                         ;;
                -R) ((recursifLevel++));                     ;;
                -U | -update)        isUpdate=true;          ;;
                --show-vars)         isShowVars=1;           ;;

                # listing
                --show-apps)         isShowApps=true;       isShowLibs=true;        ;;
                --show-apps-libs)    isShowAppsLibs=true;   isShowLibs=true;    ;;
                --show-collections)  isShowCollections=true;isShowLibs=true; ;;
                --show-collections-libs)  isShowCollectionsLibs=true;isShowLibs=true; ;;
                --show-all-libs )    isShowAllLibs=true;        ;;
                --show-libsLevel|--show-libsLevel1 )
                                     showLibsLevel=1;           ;;
                --show-libsLevel2)   showLibsLevel=2;           ;;
                --show-duree )       isShowDuree=true;          ;;
                --libExecAuto )     isLibExecAuto=$E_TRUE;      ;;
                --libExecNoAuto )   isLibExecAuto=$E_FALSE;     ;;

                -S)                 isSimulation=true;          ;;
                
                --pause)            isPause=true;               ;;
                #--) 
                    # créer pat getopt; ignorer ce _parametre
                #    ;;

                -)  # indicateur des _parametres pour les librairies/collections
                    #_argNb+=-1;
                    isPSP_params+=1;
                    ;;

                #--) # parcours des _arguments supplementaires


                *) 
                    # default $_argument,$_parametre"
                    if [ -z "$_argument" ];then break;fi

                    if [ $isPSP_params -eq 0 ]
                    then PSP-calls-add  "$_argument";
                    else PSP-params-add "$_argument"; 
                    fi
                    ;;
            esac

            shift 1;
            if [ -z "$_parametre" ];then break;fi
        done

        if [ $ARG_NB -eq 0 ];then ((aide_level++));fi
        #display-vars '_argNb' "$_argNb";
        unset librairie param _argument _parametre;
        #unset _argNb;

        #display-tableau '${PSP_params[@]}'  "'${PSP_params[@]}";
    }
#


#################
# psp-communs() #
#################
    # Affiche, au format display, les valeur de ARG_TBL
    # Garantie l'ordre
    function ARG_TBL-display() {
        local -i _pile_fct_gtt_level=5;
        fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

        local _keyNu=0;
        local _out='';
        local _keyNb=${#ARG_TBL[@]};
        #display-vars '_keyNb' "$_keyNb";
        while true
        do
            if [ $_keyNu -ge $_keyNb ];then break; fi
            _out+=" $_keyNu:'${ARG_TBL[$_keyNu]}' ,";
            ((_keyNu++))
        done
        echo "${COLOR_INFO}ARG_TBL[@]${COLOR_NORMAL}:(${_out%,})";

        fct-pile-gtt-out $E_TRUE $_pile_fct_gtt_level; return $E_TRUE;
    }
#


########################
# PSP-calls: AFFICHAGE #
########################
    # Affiche, au format display, les valeurs de PSP_calls
    # Garantie l'ordre
    function PSP-calls-display() {
        local -i _pile_fct_gtt_level=5;
        fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

        local     _keyNu=0;
        local     _out='';
        local -i  _keyNb=${#PSP_calls[@]};
        #display-vars '_keyNb' "$_keyNb";
        while true
        do
            if [ $_keyNu -ge $_keyNb ];then break; fi
            _out+=" $_keyNu:'${PSP_calls[$_keyNu]}' ,";
            ((_keyNu++))
        done
        echo "${COLOR_INFO}PSP_calls[@]${COLOR_NORMAL}:(${_out%,})";

        fct-pile-gtt-out $E_TRUE $_pile_fct_gtt_level; return $E_TRUE;
    }

    
    # Affiche, en brut, tous les paramaetres
    # Garantie l'ordre
    function PSP-calls-echo() {
        local -i _pile_fct_gtt_level=2;
        fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

        local    _keyNu=0;
        local    _out='';
        local -i _keyNb=${#PSP_calls[@]};
        #display-vars '_keyNb' "$_keyNb";
        while true
        do
            if [ $_keyNu -ge $_keyNb ];then break; fi
            _out+=" ${PSP_calls[$_keyNu]}";
            ((_keyNu++))
        done
        echo "${_out# }"; # supprime le 1er espace

        fct-pile-gtt-out $E_TRUE $_pile_fct_gtt_level; return $E_TRUE;
    }
#


#################################
# PSP-calls: GESTION DES APPELS #
#################################
    # Ajoute dans le tabelau des appels $1
    function PSP-calls-add() {
        local -i _pile_fct_gtt_level=2;
        fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

        if [ $# -eq 0 ];then fct-pile-gtt-out $E_ARG_REQUIRE $_pile_fct_gtt_level; return $E_ARG_REQUIRE; fi
        if [ -z "$1"  ];then fct-pile-gtt-out $E_ARG_REQUIRE $_pile_fct_gtt_level; return $E_ARG_REQUIRE; fi
        ((PSP_calls_index++))
        PSP_calls[$PSP_calls_index]="$1";
        PSP_calls_nb=$PSP_calls_index;
    
        fct-pile-gtt-out $E_TRUE $_pile_fct_gtt_level; return $E_TRUE;
    }

    ##
    # execPSPCall()
    # interprete les valeurs de PSP_calls et charge:
    # # la librairie si existe puis quitte
    # # les fichiers de configurations _ini.cfg.sh de tous les sous répertoires même si le dernier repertoire existe pas
    # # l'application si existe
    # # la collection si existe
    # # # lance la fonction homonyne à la librairie si existe
    # # # lance les fonctions appellées via libExecAuto si --libExecAuto ou -Auto présent)
    function PSP-calls-exec() {
        local -i _pile_fct_gtt_level=2;
        fct-pile-gtt-in "$*" $_pile_fct_gtt_level;


        titre1_D "${FUNCNAME[0]}()" 1;
        #display-vars 'PSP_calls_index' "$PSP_calls_index" 'PSPCallNb' "$PSPCallNb"


        # - Toujours Charger le _ini.cfg.sh de la racine  - #
        #exec-file "$COLLECTIONS_REP/_ini.cfg.sh";                  # Chargé par gtt.sh


        # - initialisation de la boucle execPSPCall() - #
        isPSPActuelAppAsk=false;            # global
        isPSPActuelCollectionAsk=false;      # global
        local _PSPNu=-1;
        local _PSPActuel='';    # acRep, acRep/script[-Auto],lib , autre


        while :
        do
            if $isTrapCAsk;then break; fi
            ((_PSPNu++))
            if [ $_PSPNu -gt $PSP_calls_index ];  then break; fi

            # Le PSP peut etre une (rep/)App[-Auto], (rep/)collection[-Auto], une lib ou autre chose
            _PSPActuel="${PSP_calls[$_PSPNu]:-""}";
            if [ "$_PSPActuel" = '' ]; then break; fi
            titre1_D "${FUNCNAME[0]}(): traitement du PSP:'$_PSPActuel'" 2;


            titre3_D "Est-ce une Librairie?" 2;
            if execLib "$_PSPActuel"
            then
                continue; # C'est une librairie, donc pas besoin de chercher autre chose
            fi

            titre3_D "Est-ce un appel forcé a une app via le chemin?" 2;
            if [ "${_PSPActuel:0:4}" = 'apps' ]
            then
                echo-D "Oui" 3;
                isPSPActuelAppAsk=true;
                _PSPActuel="${_PSPActuel#apps}";
                _PSPActuel="${_PSPActuel#/}";
                #display-vars '_PSPActuel' "$_PSPActuel"
            fi


            titre3_D "est-ce un appel forcé a une collection via le chemin?" 2;
            # l'état est calculé mais n'a pas de rôle implémenté
            
            if [ "${_PSPActuel:0:11}" = 'collections' ]
            then
                echo-D "Oui" 3;
                isPSPActuelCollectionAsk=true;
                _PSPActuel="${_PSPActuel#collections}";
                _PSPActuel="${_PSPActuel#/}";
                #display-vars '_PSPActuel' "$_PSPActuel"
            fi

            # Suppression de  l'extention si  présent
            _PSPActuel="${_PSPActuel%.sh}";


            titre3_D "mode Auto demandé?" 2;
            # si auto pas deja demandé via le PSP -> vérifié la présence d'une demande par '-Auto'
            if [ $isLibExecAuto -eq $E_FALSE ]\
            && [ "${_PSPActuel: -5}" = '-Auto' ]    # laisser l'espace entre ':' et '-5'
            then
                setLibExecAuto;                     # Activation de l'indicateur d'execution (via -Auto) des execLibAuto
                _PSPActuel=${_PSPActuel%-Auto};     # suppression du suffixe '-Auto' 
                echo-D "Oui: _PSPActuel=$_PSPActuel" 3;
            fi


            # Le PSP peut etre:
            # * un repertoire seul d'une app ou d'une collection
            # * une (rep/)App,
            # * (rep/)collection,
            # * une lib


            titre3_D "Chargement des '_ini.cfg.sh' ?" 2;
            if searchRepertoiresCfg "$_PSPActuel"
            then
                if [ $? -eq $E_TRUE ]
                then
                    #echo "$FUNCNAME:$LINENO: **************************************************"
                    #echo-D "SI TRUE alors PSP next";
                    #echo-D "Fichier _ini.cfg.sh' trouvé -> PSP suivant." 3
                    echo-D "Fichier _ini.cfg.sh' trouvé" 3;
                fi
            fi

            # obsolete ?
            #isAppExist        "$_PSPActuel"; local -i _isAppExist=$?;
            #isCollectionExist "$_PSPActuel"; local -i _isCollectionExist=$?;
            #isLibExist        "$_PSPActuel"; local -i _isLibExist=$?;
            #display-vars "_isCollectionExist($_PSPActuel))" "$_isCollectionExist" "_isLibExist($_PSPActuel)" "$_isLibExist";

            if $isPSPNextAsk;then continue; fi


            #############################
            titre3_D "Est-ce une APP?" 2;
            #############################
            #display-vars 'isPSPActuelCollectionAsk' "$isPSPActuelCollectionAsk";
             if ! $isPSPActuelCollectionAsk
            then
                loadApplication "$_PSPActuel" # chargera l'app
            fi
            if $isPSPNextAsk;then continue; fi


            ############################################################
            titre3_D "Afficher un conseil si collection-Auto est off" 2;
            ############################################################
            if [ $isLibExecAuto -eq $E_TRUE ]     #  (--libExecAuto | collection-Auto)
            then 
                echo 'Mode auto:On';
            else
                echo-D 'Mode auto:Off' 3;
                isCollectionExist "${_PSPActuel}"; local -i _isCollectionExist=$?;
                isCollectionExec  "${_PSPActuel}"; local -i _isCollectionExec=$?;
                if [ $_isCollectionExist -eq $E_TRUE ] && [ $_isCollectionExec -eq $E_FALSE ]
                then
                    titreInfo "Lancer '$SCRIPT_FILENAME $_PSPActuel-Auto' ou --libExecAuto pour executer les librairies lancées avec libExecAuto";
                fi
            fi


            ####################################
            titre3_D "Est-ce une collection?" 2;
            ####################################
            #display-vars 'isPSPActuelAppAsk' "$isPSPActuelAppAsk";
            if ! $isPSPActuelAppAsk
            then
                loadCollection "$_PSPActuel"     # charge la collection (et la lib homonyme si collection-Auto)
            fi
            if $isPSPNextAsk;then continue; fi


            ###############################
            titre3_D "Est-ce un ficher?" 2;
            ###############################
            if [ -f "$_PSPActuel" ]
            then
                exec-file-once "$_PSPActuel";
                continue;
            fi
        done

        fct-pile-gtt-out $E_TRUE $_pile_fct_gtt_level; return $E_TRUE;
    }
#


#######################
# PSP-params: GESTION #
#######################
    # Ajoute le parametre (et un seul) dans PSP_params[] (qui sont apres '-')
    function PSP-params-add() {
        local -i _pile_fct_gtt_level=3;
        fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

        local _param="${1-""}"; #display-vars '_param' "$_param";
    
        if [ -z "$_param" ]
        then
            fct-pile-gtt-out $E_ARG_REQUIRE $_pile_fct_gtt_level; return $E_ARG_REQUIRE;
        fi

        echo-D "Ajout de $_param" 3;

        if [[ "$_param" = *=* ]]  # y a t'il un *=* -> key=valeur # syntaxe bash
        then
            echo-D "Le parametre $_param contient '=' -> on split" 3;

            texte-car-pos "$_param" '=';
            local -i _egalPos=$texte_car_pos;
            if [ $_egalPos -gt 0 ]
            then
                ((_egalPos--));
                local _paramNom="${_param:0:$_egalPos}";
                ((_egalPos++));
                local _paramValue="${_param:$_egalPos}";
                #display-vars '_paramNom' "$_paramNom" '_paramValue' "$_paramValue";

                # suppression du 1er et dernier ' "
                _paramValue=${_paramValue#\'};   _paramValue=${_paramValue%\'};
                _paramValue=${_paramValue#\"};   _paramValue=${_paramValue%\"};
                #display-vars '_paramValue' "$_paramValue";
                PSP_params["$_paramNom"]="$_paramValue";
            fi
        
        else
            PSP_params["$_param"]=1;
            #display-vars-D "PSP_params['$_param']" "${PSP_params["$_param"]}";
        fi

        fct-pile-gtt-out $E_TRUE $_pile_fct_gtt_level; return $E_TRUE;

    }
#


##########################
# PSP-params:  AFFICHAGE #
##########################
    ##
    # function displayLibParams() {
    function PSP-params-display() {
        local -i _pile_fct_gtt_level=5;
        fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

        local _out='';
        for key in "${!PSP_params[@]}"
        do
            _out+=" ['$key']:'${PSP_params[$key]}' ,";
        done
        echo "${COLOR_INFO}PSP_params[@]${COLOR_NORMAL}:(${_out%,})";

        fct-pile-gtt-out $E_TRUE $_pile_fct_gtt_level; return $E_TRUE;
    }


    # Affiche en brut tous les parametres de $PSP_params
    # equivaut à PSP_params[@]
    function PSP_params-echo() {
        local -i _pile_fct_gtt_level=5;
        fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

        local _keyNu=0;
        local _out='';
        local _keyNb=${#PSP_params[@]};
        #display-vars '_keyNb' "$_keyNb";
        while true
        do
            if [ $_keyNu -ge $_keyNb ];then break; fi
            _out+=" ${PSP_params[$_keyNu]}";
            ((_keyNu++))
        done
        echo "${_out# }"; # supprime le 1er espace

        fct-pile-gtt-out $E_TRUE $_pile_fct_gtt_level; return $E_TRUE;
    }


    # Affiche en brut la valeur d'un parametre de $PSP_params
    function PSP-param-echo() {
        local -i _pile_fct_gtt_level=5;
        fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

        local _paramIndex="${1-""}";
        if [ -z "$_paramIndex" ]; then fct-pile-gtt-out $E_ARG_REQUIRE $_pile_fct_gtt_level; return $E_ARG_REQUIRE; fi
        
        local _paramValue="${PSP_params["$_paramIndex"]:-""}";
        if [ -z "$_paramValue" ]; then fct-pile-gtt-out $E_ARG_REQUIRE $_pile_fct_gtt_level; return $E_ARG_REQUIRE; fi

        echo "$_paramValue";

        fct-pile-gtt-out $E_TRUE $_pile_fct_gtt_level; return $E_TRUE;
    }
#

return 0;