echo-D "${BASH_SOURCE[0]}" 1;

#############################
# OBJET: inode-normalize-*  #
#############################
# app-core-inode-normalize.sh 
# date de création    : 2024.08.20
# date de modification: 2024.08.23
# Utilise: texte-normalize, fs-recursif-find
# Description: Normalize les inodes récursivement


declare -gi   inode_normalize_change_nb=0;          # Nombre de changements effectués (ou simulés)
declare -gi   inode_normalize_collision_nb=0;       # Nombre dont la destination existe déjà
declare -gi   inode_normalize_change_pb=0;          # Erreur lors de la ommande mv

# - callbakc pour text-normalize - #
declare -g    inode_normalize_texte_normalize_callback_before='';
declare -g    inode_normalize_texte_normalize_callback_reduce_before='';
declare -g    inode_normalize_texte_normalize_callback_after='';

declare -g    inode_normalize_pathname='';
declare -g    inode_normalize_rep='';
declare -g    inode_normalize_name='';
declare -g    inode_normalize_nom='';
declare -g    inode_normalize_ext='';
    


function      inode-normalize-init(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    inode_normalize_change_nb=0;
    inode_normalize_collision_nb=0;
    inode_normalize_change_pb=0;

    inode_normalize_texte_normalize_callback_before='';
    inode_normalize_texte_normalize_callback_reduce_before='';
    inode_normalize_texte_normalize_callback_after='';

    inode_normalize_pathname='';
    inode_normalize_rep='';
    inode_normalize_name='';
    inode_normalize_nom='';
    inode_normalize_ext='';

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# - display-vars - #
function inode-normalize-display-vars(){
    fs-recursif-find-display-vars;

    display-vars 'inode_normalize_change_nb             ' "$inode_normalize_change_nb";
    display-vars 'inode_normalize_collision_nb          ' "$inode_normalize_collision_nb";
    display-vars 'inode_normalize_change_pb             ' "$inode_normalize_change_pb";
    display-vars 'inode_normalize_texte_normalize_callback_before       ' "$inode_normalize_texte_normalize_callback_before";
    display-vars 'inode_normalize_texte_normalize_callback_reduce_before' "$inode_normalize_texte_normalize_callback_reduce_before";
    display-vars 'inode_normalize_texte_normalize_callback_after        ' "$inode_normalize_texte_normalize_callback_after";

    display-vars 'inode_normalize_pathname              ' "$inode_normalize_pathname";
    display-vars 'inode_normalize_rep                   ' "$inode_normalize_rep";
    display-vars 'inode_normalize_name                  ' "$inode_normalize_name";
    display-vars 'inode_normalize_nom                   ' "$inode_normalize_nom";
    display-vars 'inode_normalize_ext                   ' "$inode_normalize_ext";

    texte-normalize-display-vars;

    return 0;
}


# - callback:Initialise les variables homonymes. ATTENTION: e ne sont PAS les callback eux même - #
function inode-normalize-texte-normalize-callback-before(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    inode_normalize_texte_normalize_callback_before="${1:-""}";
    texte-normalize-callback-before "$inode_normalize_texte_normalize_callback_before";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

function inode-normalize-texte-normalize-callback-reduce-before(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    inode_normalize_texte_normalize_callback_reduce_before="${1:-""}";
    texte-normalize-callback-reduce-before "$inode_normalize_texte_normalize_callback_reduce_before";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

function inode-normalize-texte-normalize-callback-after(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    inode_normalize_texte_normalize_callback_after="${1:-""}";
    texte-normalize-callback-after "$inode_normalize_texte_normalize_callback_after";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# - core - #
addLibrairie 'inode-normalize' "Normalise le fichier|repertoire(mais pas ses fichiers) - src";
function      inode-normalize(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    local _src="${1:-""}";
    if [ "$_src" = '' ]
    then
        _src="${PSP_params['src']:-""}";
        if [ "$_src" = '' ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]} - src=pathanme";
            fct-pile-app-out $E_ARG_REQUIRE $_fct_pile_app_level; return $E_ARG_REQUIRE;
        fi
    fi

    local inode_normalize_pathname="$_src";

    inode_normalize_rep="${inode_normalize_pathname%/*}";            
    inode_normalize_name="${inode_normalize_pathname##*/}";
    inode_normalize_nom="${inode_normalize_name%.*}";
    inode_normalize_ext="${inode_normalize_name##*.}";
    if [ "$inode_normalize_rep" = "$inode_normalize_name" ]\
    || [ "$inode_normalize_ext" = "$inode_normalize_name" ];then inode_normalize_ext='';fi # en cas d'absence d'ext

    # - filtre:nom - #
    if [ "${inode_normalize_name:((${#inode_normalize_name} - 1))}" = '.' ]
    then
        inode_normalize_nom+='.'; # Le nom d'origine finit par un point: on rajoute ce point.
    fi

    if [ "${inode_normalize_name:0:2}" = './' ]                     # fichier local?
    then
        #retouver le code
        inode_normalize_rep='./';
        inode_normalize_name="${inode_normalize_name:0:2:}";
    elif [ "${inode_normalize_name:0:1}" = '.' ]                    # est-ce un fichier caché ? (.htaccess[.extention])
    then
        if [ "$inode_normalize_nom" = '' ]                          # est ce un fichier caché seul (.htaccess)?
        then inode_normalize_nom="${inode_normalize_name}";         # 
        #else  inode_normalize_nom="${inode_normalize_nom}";        # non (.fichierCaché.extention). On change pas le nom, il est dajà filtré
        fi
        if [ "$inode_normalize_ext" = "${inode_normalize_nom:1}" ]  # Si l'extention egal au nom alors il y a pas d'extention
        then
            inode_normalize_ext='';
        fi
    fi

    # - filtre:rep - #
    if [ "$inode_normalize_rep" = "$inode_normalize_nom" ];then inode_normalize_rep='';fi    # pathname ne contient aucun répertoire
    if [ "$inode_normalize_rep" = "" ]\
    && [ "${inode_normalize_pathname:0:1}" = '/' ]
    then
        inode_normalize_rep='/';  # Le fichier est à la racine
    fi

    # - filtre:ext - #
    if [ "$inode_normalize_ext" = "$inode_normalize_nom" ];then inode_normalize_ext='';fi    # L'extention est vide

   # - MINIMISATION DE L'EXTENTION -  #
        echo-d "# - MINIMISATION DE L'EXTENTION - #" 2;
        inode_normalize_ext="${inode_normalize_ext,,}";     # minimize l'extention


   # - NORMALISATION DU NOM-  #
        echo-d "# - ${FUNCNAME[0]}():NORMALISATION DU NOM - #" 2;

        #texte-normalize-init;                              # Ne pas initialiser!
        texte-normalize-callback-before        "$inode_normalize_texte_normalize_callback_before";
        texte-normalize-callback-reduce-before "$inode_normalize_texte_normalize_callback_reduce_before";
        texte-normalize-callback-after         "$inode_normalize_texte_normalize_callback_after";
        texte-normalize "$inode_normalize_nom";             # normalise le nom

    # - INTEGRATION: NOM/EXT/PATHNAME- #
        echo-d "# - ${FUNCNAME[0]}():INTEGRATION: NOM/PATHNAME - #" 2;

        inode_normalize_nom="$texte_normalize";     # $texte_normalize: provient de texte-normalize()
        if [ $debug_verbose_level -gt 0 ]
        then
            display-vars 'inode_normalize_nom' "$inode_normalize_nom";
        fi

        if [ "$inode_normalize_ext" = '' ]
        then  inode_normalize_name="${inode_normalize_nom}";
        else  inode_normalize_name="${inode_normalize_nom}.${inode_normalize_ext}";
        fi
        if [ "$inode_normalize_rep" = '' ]
        then  inode_normalize_pathname="${inode_normalize_name}";
        else  inode_normalize_pathname="${inode_normalize_rep}/${inode_normalize_name}";
        fi

    # - APPLICATION - #
        echo-d "# - ${FUNCNAME[0]}(): INTEGRATION: NOM/EXT/PATHNAME - #" 2;
        # tester si un fichier portant le meme nom de destination existe deja
        # ...
        if [ -e "$inode_normalize_pathname" ]
        then
            echo-v "'$inode_normalize_pathname' existe déjà" 2;
            ((inode_normalize_collision_nb++))
            fct-pile-app-out 0 $_fct_pile_app_level; return 0;
        fi

        if [ "$inode_normalize_pathname" != "$_src" ]
        then
            if $isSimulation
            then
                titreCmd "mv \"$_src\"  \"$inode_normalize_pathname\" #SIMULATION";
            else
                eval-echo "mv  \"$_src\"   \"$inode_normalize_pathname\"";
                if [ $erreur_no -ne 0 ]
                then
                ((inode_normalize_change_pb++))
                    erreurs-pile-add-echo "${FUNCNAME[0]}(): Problème avec la commande: 'mv  \"$_src\"   \"$inode_normalize_pathname\"'";
                else
                    ((inode_normalize_change_nb++));
                fi
            fi
        else
            echo-v "'$_src': pas de changement" 1;
        fi

    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}


# Utilise fs-recursif-find pour la récursivité
addLibrairie 'inode-normalize-all' "Normalise le fichier|repertoire(et ses fichiers) - src";
function      inode-normalize-all(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;


    local _src="${1:-""}";
    if [ "$_src" = '' ] # parametre obligatoire
    then
        _src="${PSP_params['src']:-""}";
        if [ "$_src" = '' ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]} - src=pathanme";
            fct-pile-app-out $E_ARG_REQUIRE $_fct_pile_app_level; return $E_ARG_REQUIRE;
        fi
    fi


    # - - #
    titre1 "${FUNCNAME[0]}: $_src";
    if [ ! -e "$_src" ]
    then
        erreurs-pile-add-echo "'$_src' existe pas";
    fi

    # - display-vars - #
    #inode-normalize-display-vars;


    # - Execution - #
    #inode-normalize-init;  # Ne pas initialiser: cela détruirait la personalisation de $COLLECTIONS
    #texte-normalize-init;

    texte-normalize-callback-before        "$inode_normalize_texte_normalize_callback_before";
    texte-normalize-callback-reduce-before "$inode_normalize_texte_normalize_callback_reduce_before";
    texte-normalize-callback-after         "$inode_normalize_texte_normalize_callback_after";
    fs-recursif-find-callback-fichier "inode-normalize";
    fs-recursif-find "$_src";

    #inode-normalize-display-vars;

    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
}

return 0;