echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.18
# Description: 


################
# objet: tmp-* #
################
# tmp: repertoire ou fichier ou lien symbolique
# initialise les variables attachées
    declare -g tmp_pathname='';           # chemin complet /rep/er/toire/tmpNom.tmpExt
    declare -g tmp_rep='';                # /rep/er/toire sans slash de fin
    declare -g tmp_name='';               # tmpNom.tmpExt
    declare -g tmp_nom='';                # nom de l'tmp sans l'extention
    declare -g tmp_ext='';                # extention de l'tmp sans le le point
    declare -g tmp_isShadowFile=false;    # le nom de fichier commence t-il par un point '.' ?


    function tmp-init(){
        tmp_pathname='';
        tmp_rep='';
        tmp_name='';
        tmp_nom='';
        tmp_ext='';
        tmp_isShadowFile=false;
        return 0;
    }


    #tmp-pathname( pathname )
    # definit les variables de tmp
    function tmp-pathname(){
        tmp-init;
        if [ "${1:-""}" = ""  ];then tmp-init;return $E_FALSE;fi
        tmp_pathname="$1";
        #tmp_pathname="$1";
        tmp_rep="${tmp_pathname%/*}";       #display-vars 'tmp_rep' "$tmp_rep";
        tmp_name="${tmp_pathname##*/}";     #display-vars 'tmp_name' "$tmp_name";
        tmp_nom="${tmp_name%.*}";           #display-vars 'tmp_nom' "$tmp_nom";
        tmp_ext="${tmp_name##*.}";


        # - filtre:nom - #
        if [ "${tmp_name:((${#tmp_name} - 1))}" = '.' ]
        then
            tmp_nom+='.'; # Le nom d'origine finit par un point: on rajoute ce point.
        fi

        if [ "${tmp_name:0:1}" = '.' ] # est-ce un fichier caché ? (.htaccess[.extention])
        then
            if [ "$tmp_nom" = '' ]            # est ce un fichier caché seul (.htaccess)?
            then tmp_nom="${tmp_name}";     # 
            #else  tmp_nom="${tmp_nom}";    # non (.fichierCaché.extention). On change pas le nom, il est dajà filtré
            fi
            if [ "$tmp_ext" = "${tmp_nom:1}" ]  # Si l'extention egal au nom alors il y a pas d'extention
            then
                tmp_ext='';
            fi
        fi


        # - filtre:rep - #
        if [ "$tmp_rep" = "$tmp_nom" ];then tmp_rep='';fi    # pathname ne contient aucun répertoire
        if [ "$tmp_rep" = "" ]&&\
           [ "${tmp_pathname:0:1}" = '/' ]
        then
            tmp_rep='/';  # Le fichier est à la racine
        fi

        # - filtre:ext - #
        if [ "$tmp_ext" = "$tmp_nom" ];then tmp_ext='';fi    # L'extention est vide

        # - Création du répertoire - #
        tmp-create;

        return $?;
    }


    function tmp-rep(){
        tmp_rep="${1:-""}";
        if [ "$tmp_rep" != '/' ]           # si pas root ('/')
        then tmp_rep="${tmp_rep%/}";    # on supprime le dernier slash si indiqué
        fi
        if [ "$tmp_rep" = '' ]
        then  tmp_pathname="${tmp_name}"
        else  tmp_pathname="${tmp_rep}/${tmp_name}"
        fi

        # - Création du répertoire - #
        tmp-mkdir;

        return $?;
    }


    function tmp-nom(){
        if [ "${1:-""}" = ""  ];then return $E_FALSE;fi # le nom ne peut etre vide
        tmp_nom="${1}";
        if [ "$tmp_ext" = '' ]
        then  tmp_name="${tmp_nom}"
        else  tmp_name="${tmp_nom}.${tmp_ext}"
        fi
        if [ "$tmp_rep" = '' ]
        then  tmp_pathname="${tmp_name}"
        else  tmp_pathname="${tmp_rep}/${tmp_name}"
        fi
        return 0;
    }


    function tmp-ext(){
        tmp_ext="${1:-""}";
        if [ "$tmp_ext" = '' ]
        then  tmp_name=".${tmp_nom}"
        else  tmp_name="${tmp_nom}.${tmp_ext}"
        fi
        if [ "$tmp_rep" = '' ]
        then  tmp_pathname="${tmp_name}"
        else  tmp_pathname="${tmp_rep}/${tmp_name}"
        fi
        return 0;
    }


    #tmp-display-var()
    # Affiche sous forme de display-var les variables d'informations
    function tmp-display-vars(){
        display-vars 'tmp_pathname   ' "$tmp_pathname";
        display-vars 'tmp_rep        ' "$tmp_rep";
        display-vars 'tmp_name       ' "$tmp_name";
        display-vars 'tmp_nom        ' "$tmp_nom";
        display-vars 'tmp_ext        ' "$tmp_ext";
        return 0;
    }


    ##
    # Cré le repertoire temporaire
    # return: $E_INODE_NOT_EXIST, $E_INODE_NOT_W, $E_TRUE, $E_FALSE
    function tmp-mkdir(){
        # https://unix.stackexchange.com/questions/352107/generic-way-to-get-temp-path
        # https://superuser.com/questions/332610/where-is-the-temporary-directory-in-linux
        # https://refspecs.linuxfoundation.org/fhs.shtml
        #local -i _pile_fct_gtt_level=2;             # fct-pile-fctgtt-in pas encore chargé
        #fct-pile-fctgtt-in "$*" $_pile_fct_gtt_level;   # fct-pile-fctgtt-in pas encore chargé

        if [ ! -d "$tmp_rep" ]
        then
            if ! mkdir -p "$tmp_rep"
            then
                #fct-pile-fctgtt-out $E_tmp_NOT_EXIST $_pile_fct_gtt_level; retun $E_tmp_NOT_EXIST;
                retun $E_INODE_NOT_EXIST;
            fi
        fi
        if [ ! -w "$tmp_rep" ]
        then
            return $E_INODE_NOT_W;
        fi
        #fct-pile-fctgtt-out $E_FALSE $_pile_fct_gtt_level; return $E_FALSE;

        return $E_TRUE;
    }


    function tmp-cd(){
        # https://unix.stackexchange.com/questions/352107/generic-way-to-get-temp-path
        # https://superuser.com/questions/332610/where-is-the-temporary-directory-in-linux
        # https://refspecs.linuxfoundation.org/fhs.shtml
        #local -i _pile_fct_gtt_level=2;             # fct-pile-fctgtt-in pas encore chargé
        #fct-pile-fctgtt-in "$*" $_pile_fct_gtt_level;   # fct-pile-fctgtt-in pas encore chargé

        if cd "$tmp_rep" 
        then
            retun $E_TRUE;
        else
            erreurs-pile-add-echo "Impossible d'entrée dans le repertoire temporaire '$tmp_pathname'";
            retun $E_FALSE;
        fi
    }

#

return 0;