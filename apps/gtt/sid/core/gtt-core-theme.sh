echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


##
# Définit le nom et le chemin du thème courant
# $1: nom du répertoire du thème
# @return:
# $E_TRUE: le répertoire existe et THEME_NOM est initialisé
# $E_FALSE: si le répertoire n'exoiste pas
function theme-set() {
    local -i _pile_fct_gtt_level=2;
    fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

    local _theme_nom="$1";
    local _theme_rep="$THEMES_REP/$_theme_nom/";
    if [ -d "$_theme_rep" ]
    then
        THEME_NOM="$1";
        fct-pile-gtt-out $E_TRUE $_pile_fct_gtt_level; return $E_TRUE;
    else
        echo "Le theme '$1'($_theme_rep) n'existe pas!";
        fct-pile-gtt-out $E_FALSE $_pile_fct_gtt_level; return $E_FALSE;
    fi
    
    fct-pile-gtt-out $E_TRUE $_pile_fct_gtt_level; return $E_TRUE;
}


#function theme-load-defaut() {
#    local -i _pile_fct_gtt_level=2;
#    fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

#    if [ $# -eq 0 ];then fct-pile-gtt-out $E_ARG_REQUIRE $_pile_fct_gtt_level; return $E_ARG_REQUIRE; fi

#    exec-file "$THEMES_REP/$THEME_DEFAUT/theme-${THEME_DEFAUT}.sh";

#    fct-pile-gtt-out $E_TRUE $_pile_fct_gtt_level; return $E_TRUE;
#}


function theme-load() {
    local -i _pile_fct_gtt_level=2;
    fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

    #if [ $# -eq 0 ];then fct-pile-gtt-out $E_ARG_REQUIRE $_pile_fct_gtt_level; return $E_ARG_REQUIRE; fi
    

    local _theme_nom="${1:-""}";
    if [ "$_theme_nom" != '' ]
    then
        theme-set "$_theme_nom";
        if [ $? -eq $E_FALSE ]; then fct-pile-gtt-out $E_FALSE $_pile_fct_gtt_level; return $E_FALSE; fi
    fi
    #if [ -e "$THEMES_REP/$THEME_NOM/$THEME_NOM.sh" ]
    #then
        exec-file "$THEMES_REP/$THEME_NOM/theme-${THEME_NOM}.sh";
    #fi

    fct-pile-gtt-out $E_TRUE $_pile_fct_gtt_level; return $E_TRUE;
}


return 0;