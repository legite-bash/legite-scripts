echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


# ##### #
# TRAPS #
# ##### #

#http://www.gnu.org/software/bash/manual/html_node/Bourne-Shell-Builtins.html#Bourne-Shell-Builtins
#trap -l #trap --help
#https://linuxhint.com/bash_error_handling/
#https://tldp.org/LDP/Bash-Beginners-Guide/html/sect_12_02.html


#SIGHUP (1) : Signal envoyé quand le terminal se déconnecte.
#SIGINT (2) : Signal envoyé par l'utilisateur avec Ctrl+C pour interrompre le programme.
#SIGQUIT (3) : Signal envoyé par l'utilisateur avec Ctrl+\ pour quitter le programme et générer un core dump.
#SIGKILL (9) : Signal pour forcer l'arrêt du programme (ne peut pas être intercepté par trap).
#SIGTERM (15) : Signal par défaut pour demander l'arrêt d'un programme.
#SIGUSR1 et SIGUSR2 (10, 12) : Signaux utilisateur définis librement pour des usages spécifiques.
#SIGSTOP (19) : Signal pour arrêter un processus temporairement (ne peut pas être intercepté par trap).
#SIGCONT (18) : Signal pour continuer un processus après un SIGSTOP.


# initialise  les interceptions
# Les traps doivent etre initialisées apres la fonction shift car elle renvoie 1 et provoque donc une exeption générale
function trap-init() {
    local -i _pile_fct_gtt_level=2;
    fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

    #    '------------------c-o-m-m-a-n-d----------------------------------------------------'  signal
    trap 'trap-pile-add $? $LINENO "${BASH_SOURCE}" "${FUNCNAME[*]}";'                           ERR;
    trap 'trap-pile-add $? $LINENO "${BASH_SOURCE}" "${FUNCNAME[*]}" "SIGHUP";isTrapCAsk=true;'  SIGHUP;
    trap 'trap-pile-add $? $LINENO "${BASH_SOURCE}" "${FUNCNAME[*]}" "SIGINT";isTrapCAsk=true;'  SIGINT;
    trap 'trap-pile-add $? $LINENO "${BASH_SOURCE}" "${FUNCNAME[*]}" "SIGQUIT";isTrapCAsk=true;' SIGQUIT;
    #trap 'trap-pile-add $? $LINENO "${BASH_SOURCE}" "${FUNCNAME[*]}": debug'                DEBUG;

    fct-pile-gtt-out $E_TRUE $_pile_fct_gtt_level; return $E_TRUE;
}

function trap-init-2() {
    local -i _pile_fct_gtt_level=2;
    fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

    #    '------------------c-o-m-m-a-n-d----------------------------------------------------'  signal
    trap 'trap-pile-add $? $LINENO "${BASH_SOURCE}" "${FUNCNAME[*]}": debug'                     DEBUG;
    fct-pile-gtt-out $E_TRUE $_pile_fct_gtt_level; return $E_TRUE;
}

#function trap-ctrl_c() {
#    local -i _pile_fct_gtt_level=2;
#    fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

#    isTrapCAsk=true;

#    fct-pile-gtt-out $E_TRUE $_pile_fct_gtt_level; return $E_TRUE;
#}


# - TRAP: PILE - #
declare -a trap_pile;           # tableau contenant la pile de notifications
trap_pile[0]='vide';            # Remplir avec du vide pour ne pas avoir un tableau vide ()
declare -i trap_pile_index=0;   # index utilisé pour le remplissage de la pile


#trap-Erreur-add( "$txt" ) # ajoute une interception dans la pile
function trap-pile-add() {
    local -i _pile_fct_gtt_level=2;
    fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

    local -i _erreur_no=$1;
    #if [[ $_erreur_no -eq 0 ]];then return 0; fi
    if [ $_erreur_no -eq $E_FALSE ];then return 0; fi
    local -i _ligneNo="$2";
    local    _src="${3:-""}";
    local    _fonction="${4:-""}";
    local    _funcName="${5:-""};";
    local    _erreur__description="${erreur_descriptions[$_erreur_no]:-'aucune'}";
    local    _out="$_src:$_ligneNo. ${_funcName}() Erreur no:${_erreur_no}: $_erreur__description.";
    display-vars 'trap' "${COLOR_WARN}$_out${COLOR_NORMAL}";

    trap_pile[$trap_pile_index]="$_out";
    ((trap_pile_index++))                   # incrementation apres pour commencer à [0]}

    fct-pile-gtt-out $E_TRUE $_pile_fct_gtt_level; return $E_TRUE;
}


#trap-pile-show($separateur="\n") # affiche la pile
function trap-pile-show() {
    local -i _pile_fct_gtt_level=2;
    fct-pile-gtt-in "$*" $_pile_fct_gtt_level;

    if [ $trap_pile_index -ne 0 ]   # utiliser $trap_pile_index pout tester si vide (la pile esr remplis à 1 par defaut)
    then
        titre1_D "TRAPS($trap_pile_index)";
        local separateur="${1:-\n}";
        echo -n "${COLOR_TRAP}";
        for valeur in "${trap_pile[@]}"
        do
            echo -ne "  $valeur$separateur";
        done
        echo -n "${COLOR_NORMAL}";
    fi

    fct-pile-gtt-out $E_TRUE $_pile_fct_gtt_level; return $E_TRUE;
}

########
# MAIN #
########
trap-init;

