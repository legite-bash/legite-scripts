echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : 2024.08.19
# date de modification: 2024.08.19
# Description: 


################
# objet: log-* #
################
# log: repertoire ou fichier ou lien symbolique
# initialise les variables attachées
    declare -g log_pathname='';           # chemin complet /rep/er/toire/logNom.logExt
    declare -g log_rep='';                # /rep/er/toire sans slash de fin
    declare -g log_name='';               # logNom.logExt
    declare -g log_nom='';                # nom       du fichier sans l'extention
    declare -g log_ext='';                # extention du fichier sans le le point
    declare -g log_isShadowFile=false;    # le nom de fichier commence t-il par un point '.' ?


    function log-init(){
        log_pathname='';
        log_rep='';
        log_name='';
        log_nom='';
        log_ext='';
        log_isShadowFile=false;
        return 0;
    }


    #log-pathname( pathname )
    # definit les variables de log
    function log-pathname(){
        log-init;
        if [ "${1:-""}" = ""  ];then log-init;return $E_FALSE;fi
        log_pathname="$1";
        #log_pathname="$1";
        log_rep="${log_pathname%/*}";       #display-vars 'log_rep' "$log_rep";
        log_name="${log_pathname##*/}";     #display-vars 'log_name' "$log_name";
        log_nom="${log_name%.*}";           #display-vars 'log_nom' "$log_nom";
        log_ext="${log_name##*.}";


        # - filtre:nom - #
        if [ "${log_name:((${#log_name} - 1))}" = '.' ]
        then
            log_nom+='.'; # Le nom d'origine finit par un point: on rajoute ce point.
        fi

        if [ "${log_name:0:1}" = '.' ] # est-ce un fichier caché ? (.htaccess[.extention])
        then
            if [ "$log_nom" = '' ]            # est ce un fichier caché seul (.htaccess)?
            then log_nom="${log_name}";     # 
            #else  log_nom="${log_nom}";    # non (.fichierCaché.extention). On change pas le nom, il est dajà filtré
            fi
            if [ "$log_ext" = "${log_nom:1}" ]  # Si l'extention egal au nom alors il y a pas d'extention
            then
                log_ext='';
            fi
        fi


        # - filtre:rep - #
        if [ "$log_rep" = "$log_nom" ];then log_rep='';fi    # pathname ne contient aucun répertoire
        if [ "$log_rep" = "" ]&&\
           [ "${log_pathname:0:1}" = '/' ]
        then
            log_rep='/';  # Le fichier est à la racine
        fi

        # - filtre:ext - #
        if [ "$log_ext" = "$log_nom" ];then log_ext='';fi    # L'extention est vide

        # - Création du répertoire - #
        log-create;

        return $?;
    }


    function log-setRep(){
        log_rep="${1:-""}";
        if [ "$log_rep" != '/' ]           # si pas root ('/')
        then log_rep="${log_rep%/}";    # on supprime le dernier slash si indiqué
        fi
        if [ "$log_rep" = '' ]
        then  log_pathname="${log_name}"
        else  log_pathname="${log_rep}/${log_name}"
        fi

        # - Création du répertoire - #
        log-create;

        return $?;
    }


    function log-setNom(){
        if [ "${1:-""}" = ""  ];then return $E_FALSE;fi # le nom ne peut etre vide
        log_nom="${1}";
        if [ "$log_ext" = '' ]
        then  log_name="${log_nom}"
        else  log_name="${log_nom}.${log_ext}"
        fi
        if [ "$log_rep" = '' ]
        then  log_pathname="${log_name}"
        else  log_pathname="${log_rep}/${log_name}"
        fi
        return 0;
    }


    function log-setExt(){
        log_ext="${1:-""}";
        if [ "$log_ext" = '' ]
        then  log_name=".${log_nom}"
        else  log_name="${log_nom}.${log_ext}"
        fi
        if [ "$log_rep" = '' ]
        then  log_pathname="${log_name}"
        else  log_pathname="${log_rep}/${log_name}"
        fi
        return 0;
    }


    #log-display-var()
    # Affiche sous forme de display-var les variables d'informations
    function log-display-vars(){
        display-vars 'log_pathname   ' "$log_pathname";
        display-vars 'log_rep        ' "$log_rep";
        display-vars 'log_name       ' "$log_name";
        display-vars 'log_nom        ' "$log_nom";
        display-vars 'log_ext        ' "$log_ext";
        return 0;
    }


    ##
    # définit la variable log_ROOT
    # $1: nouvelle valeur de la variable
    # si c'est un répertoire en écriture alors log_ROOT=$1
    # si ce n'est pas un repertoire il est crée.
    # return: $E_INODE_NOT_EXIST, $E_INODE_NOT_W, $E_TRUE, $E_FALSE
    function log-create(){
        # https://unix.stackexchange.com/questions/352107/generic-way-to-get-temp-path
        # https://superuser.com/questions/332610/where-is-the-temporary-directory-in-linux
        # https://refspecs.linuxfoundation.org/fhs.shtml
        #local -i _pile_fct_gtt_level=2;             # fct-pile-fctgtt-in pas encore chargé
        #fct-pile-fctgtt-in "$*" $_pile_fct_gtt_level;   # fct-pile-fctgtt-in pas encore chargé

        if [ ! -d "$log_rep" ]
        then
            if ! mkdir -p "$log_rep"
            then
                #fct-pile-fctgtt-out $E_log_NOT_EXIST $_pile_fct_gtt_level; retun $E_log_NOT_EXIST;
                retun $E_INODE_NOT_EXIST;
            fi
        fi
        if [ ! -w "$log_rep" ]
        then
            return $E_INODE_NOT_W;
        fi
        #fct-pile-fctgtt-out $E_FALSE $_pile_fct_gtt_level; return $E_FALSE;

        return $E_TRUE;
    }


    function log-cd(){
        # https://unix.stackexchange.com/questions/352107/generic-way-to-get-temp-path
        # https://superuser.com/questions/332610/where-is-the-temporary-directory-in-linux
        # https://refspecs.linuxfoundation.org/fhs.shtml
        #local -i _pile_fct_gtt_level=2;             # fct-pile-fctgtt-in pas encore chargé
        #fct-pile-fctgtt-in "$*" $_pile_fct_gtt_level;   # fct-pile-fctgtt-in pas encore chargé

        if cd "$log_rep" 
        then
            retun $E_TRUE;
        else
            erreurs-pile-add-echo "Impossible d'entrée dans le repertoire temporaire '$log_pathname'";
            retun $E_FALSE;
        fi
    }

#


return 0;