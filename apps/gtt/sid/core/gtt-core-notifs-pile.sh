echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.19
# Description: 


#################
# notifs-pile-* #
#################
    declare -a notifs_pile;          # tableau contenant la pile de notifications
    declare -i notifs_pile_index=0;       # index utilisé pour le remplissage de la pile de notifs

    #notifs-pile-add($txt)                # ajoute une notif dans la pile de notifs
    function notifs-pile-add() {
        local _notif="${1:-""}";
        if [ "$_notif"  == '' ];then  return 0;   fi

        notifs_pile[$notifs_pile_index]="$_notif";
        ((notifs_pile_index++))

        return 0;
    }

    function notifs-pile-add-echo() {
        local _notif="${1:-""}"
        if [[ "$_notif"  == '' ]];then  return 0;   fi

        notifs_pile[$notifs_pile_index]="$_notif";
        echo "$_notif" ${COLOR_INFO}'ok'${COLOR_NORMAL};
        ((notifs_pile_index++))

        return 0;
    }

    #notifs-pile-add-Echo-v ( _verbose_level "_notifTxt" )
    function notifs-pile-add-Echo-v() {
        if [ $# -lt 2 ];then return $E_ARG_REQUIRE; fi

        local -i _verbose_level=$1
        local    _notifTxt=${2:-""};
        
        if [ $_verbose_level -gt $verbose_level ];then return $E_FALSE; fi 
        notifs-pile-add-echo "$_notifTxt";
        
        return $E_TRUE;
    }

    #notifs-pile-show($separateur="\n") # affiche la pile des notifs
    function notifs-pile-show() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [[ -z "${notifs_pile[*]}" ]];then fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE; fi
        local _separateur="${1:-\n}";           # PAS D'APOSTROPHE
        titre0 "Notifs($notifs_pile_index)";
        for _notif in "${notifs_pile[@]}"
        do
            echo -ne "$_notif$_separateur";
        done
        echo "## Fin des notifications";

        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }
#


##################
# erreurs-pile-* #
##################
    declare -a erreurs_pile;              # tableau contenant la pile de notifications
    declare -i erreurs_pile_index=0;       # index utilisé pour le remplissage de la pile des erreurs

    #erreurs-pile-add($txt) # ajoute une notif dans la pile de notifs
    function erreurs-pile-add() {
        local _erreurTxt="${1:-""}";
        if [[ "$_erreurTxt"  == '' ]]; then  return $E_ARG_REQUIRE; fi
    
        erreurs_pile[$erreurs_pile_index]="$_erreurTxt"
        ((erreurs_pile_index++))

        return 0;
    }

    function erreurs-pile-add-echo() {
        local _erreurTxt="${1:-""}";
        if [[ "$_erreurTxt"  == '' ]];then  return $E_ARG_REQUIRE; fi

        erreurs_pile[$erreurs_pile_index]="$_erreurTxt";
        echo $COLOR_WARN"$_erreurTxt"${COLOR_NORMAL}
        ((erreurs_pile_index++))

        return 0;
    }

    function erreurs-pile-add-echo-v() {

        if [ $# -lt 2 ];then return $E_ARG_REQUIRE; fi
        
        local -i _verbose_level=$1;
        local    _erreurTxt="${2:-""}";
        if [ $_verbose_level -gt $verbose_level ];then return $E_FALSE; fi 
        erreurs-pile-add-echo "$_erreurTxt";

        return $E_TRUE;
    }

    #erreurs-pile-show($separateur="\n") # affiche la pile des erreurs
    function erreurs-pile-show() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ -z "${erreurs_pile[*]}" ];then fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE; fi
        local _separateur="${1:-\n}";               # PAS D'APOSTROPHE
        titre0 "Erreurs($erreurs_pile_index)";
        for _erreurTxt in "${erreurs_pile[@]}"
        do
            echo -ne "$_erreurTxt$_separateur";
        done
        echo "## Fin des erreurs";

        fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
    }
#

return 0;