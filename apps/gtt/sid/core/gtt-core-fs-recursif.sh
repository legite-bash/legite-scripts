echo-d "${BASH_SOURCE[0]}" 1;

# app.sh 
# date de création    : 2024.08.21
# date de modification: 2024.08.21
# Description: fonctions d'analyses recursives de répertoires
# test: app.sh --show-libsLevel2 apps/app/sid/tests/core-fs-recursif/


###############################
# - OBJET: fs-recursif-find - #
###############################
    # Analyse récursive de repertoire qui utilise l'outils standard find
    # Callback: fs_recursif_find_callback_fichier, fs_recursif_find_callback_rep_in, fs_recursif_find_callback_rep_out
    # Dépend de: void()
    # récursivité selon find -f puis ls -1 if [ -d ]
    # trié: OUI
    # Changement de repertoire: non
    # Prise en charge des liens symboliques: NON
    declare -gi   fs_recursif_find_level_max=-1;                    # -1: illimité 0: le repertoire courant uniquement
    declare -gi   fs_recursif_find_level=0;                         # niveau actuel de recursivité
    declare -g    fs_recursif_find_pext='';                         # pattern pour l'extention (avec le '.')

    declare -g    fs_recursif_find_pathname='';                     # nom du fichier actuel dans la boucle
    declare -gi   fs_recursif_find_rep_nb=0;
    declare -gi   fs_recursif_find_fic_nb=0;

    declare -g    fs_recursif_find_callback_rep_in_enable=true;     # la fonction code est appellée AVANT la récursivité
    declare -g    fs_recursif_find_callback_rep_in='';              # si vide alors = "void"
    declare -g    fs_recursif_find_callback_fichier='';             # nom de la fonction code a répéter
    declare -g    fs_recursif_find_callback_rep_out_enable=false;   # la fonction code est appellée AFTER la récursivité
    declare -g    fs_recursif_find_callback_rep_out='';             # si vide alors = "void"


    function      fs-recursif-find-init(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        fs_recursif_find_level_max=-1;
        fs_recursif_find_level=0;
        fs_recursif_find_pext='';
        fs_recursif_find_pathname='';
        fs_recursif_find_rep_nb=0;
        fs_recursif_find_fic_nb=0;

        fs_recursif_find_callback_rep_in_enable=true;               # # Appel le callback si true  Affiche par defaut le nom du rep en titre4
        fs_recursif_find_callback_rep_in='';
        fs_recursif_find_callback_fichier='';
        fs_recursif_find_callback_rep_out_enable=false;
        fs_recursif_find_callback_rep_out='';
        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }

    function fs-recursif-find-display-vars(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        display-vars 'fs_recursif_find_level_max              ' "$fs_recursif_find_level/$fs_recursif_find_level_max";
        display-vars 'fs_recursif_find_pext                   ' "$fs_recursif_find_pext";         # Point Ext
        display-vars 'fs_recursif_find_callback_fichier       ' "$fs_recursif_find_callback_fichier";
        display-vars 'fs_recursif_find_callback_rep_in_enable ' "$fs_recursif_find_callback_rep_in_enable" 'fs_recursif_find_callback_rep_in' "$fs_recursif_find_callback_rep_in";
        display-vars 'fs_recursif_find_pathname               ' "$fs_recursif_find_pathname";
        display-vars 'fs_recursif_find_callback_rep_out_enable' "$fs_recursif_find_callback_rep_out_enable"  'fs_recursif_find_callback_rep_out'  "$fs_recursif_find_callback_rep_out";
        display-vars 'fs_recursif_find_rep_nb                 ' "$fs_recursif_find_rep_nb";
        display-vars 'fs_recursif_find_fic_nb                 ' "$fs_recursif_find_fic_nb";

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }


    # - callback:Initialise les variables homonymes. ATTENTION: e ne sont PAS les callback eux même - #
    function      fs-recursif-find-level-max-set(){
        fs_recursif_find_level_max=${1:--1};
        return 0;
    }

    # - callback-rep-in - #
    function      fs-recursif-find-callback-rep-in-enable(){
        local _is=${1:-true};
        fs_recursif_find_callback_rep_in_enable=$_is;
        return 0;
    }

    function      fs-recursif-find-callback-rep-in(){
        local _name="${1:-"void"}";
        fs_recursif_find_callback_rep_in="$_name";
        if [ "$fs_recursif_find_callback_rep_in" = "void" ]
        then fs_recursif_find_callback_rep_in_enable=false;
        else fs_recursif_find_callback_rep_in_enable=true;
        fi
        return 0;
    }

    function      fs-recursif-find-callback-rep-in-core(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        echo "$CYAN$1$NORMAL";    # $1: recoit l'inode en cour -> Affiche $1

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }


    # - callback-fichier - #
    function      fs-recursif-find-callback-fichier(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _name=${1:-"void"};
        fs_recursif_find_callback_fichier="$_name";

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }

    function      fs-recursif-find-callback-fichier-core(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        #echo "Ce code sera exécuté pour chaque item";
        #display-vars ${FUNCNAME[0]}:'$@' "$@";
        echo "$1";

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }


    # - callback-rep-out - #
    function      fs-recursif-find-callback-rep-out-enable(){
        local _is=${1:-true};
        fs_recursif_find_callback_rep_out_enable=$_is;
        return 0;
    }


    function      fs-recursif-find-callback-rep-out(){
        local _name=${1:-"void"};
        fs_recursif_find_callback_rep_out="$_name";
        if [ "$fs_recursif_find_callback_rep_out" = void ]
        then fs_recursif_find_callback_rep_out_enable=false;
        else fs_recursif_find_callback_rep_out_enable=true;
        fi
        return 0;
    }

    function      fs-recursif-find-callback-rep-out-core(){
        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        titre4 "$1";    # $1: recoit l'inode en cour -> Affiche $1

        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }


    #addLibrairie 'fs-recursif-find';           # -R # ne peut etre appeler via la LdC
    function      fs-recursif-find(){
        if $isTrapCAsk;then return $E_CTRL_C;fi

        local -i _fct_pile_app_level=2;
        fct-pile-app-in "$*" $_fct_pile_app_level;

        local _src="${1:-""}";
        if [ "$_src" = '' ];then fct-pile-app-out $E_ARG_REQUIRE $_fct_pile_app_level; return $E_ARG_REQUIRE;fi

        if [ "$fs_recursif_find_callback_rep_in"  = '' ]; then fs_recursif_find_callback_rep_in="${FUNCNAME[0]}-callback-rep-in-core"; fi;
        if [ "$fs_recursif_find_callback_fichier" = '' ]; then fs_recursif_find_callback_fichier="${FUNCNAME[0]}-callback-fichier-core"; fi;
        if [ "$fs_recursif_find_callback_rep_out" = '' ]; then fs_recursif_find_callback_rep_out="void"; fi;


        if [ -d "$_src" ]
        then

            titre4_d '# - Traitement des fichiers du repertoire local - #' 2;
            local _fichiersListe=$(find "$_src/"*"$fs_recursif_find_pext" -maxdepth 0 -type f 2>/dev/null);   # ne prend pas les liens symboliques
            local _fichiersListeTries=$(echo "$_fichiersListe" | sort);
            _fichiersListe='';
            (( fs_recursif_find_rep_nb++ ))
            (( fs_recursif_find_level++ ));


            titre4_d '# - callback-rep-in - #' 2;
            if $fs_recursif_find_callback_rep_in_enable
            then
                $fs_recursif_find_callback_rep_in "$_src";                  # Variable qui contient le nom de la fonction de callback rep_in
            fi


            # - Parcour des inodes - #
            for fs_recursif_find_pathname in $_fichiersListeTries   # (fs_recursif_find_pathname avec find, _fichier_name avec ls)
            do
                if $isTrapCAsk;then fct-pile-app-out $E_CTRL_C $_fct_pile_app_level; return $E_CTRL_C; fi

                ((fs_recursif_find_fic_nb++))

                titre4_d '# - callback-fichier - #' 2;
                ${fs_recursif_find_callback_fichier} "$fs_recursif_find_pathname";    # callback
            done
            _fichiersListeTries='';         # unset ?
            fs_recursif_find_pathname='';


            titre4_d '# - Traitement des sous repertoires - #' 2;

            if [ $fs_recursif_find_level_max -eq $RECUR_NONE ]
            then
                echo-v "Récursivité désactivée (fs_recursif_find_level_max=$fs_recursif_find_level_max)" 1;
                fct-pile-app-out 0 $_fct_pile_app_level; return 0;
            fi

            # Si pas illimité: controle de la limite
            if [ $fs_recursif_find_level_max -ne $RECUR_NO_LIMIT ]
            then # controle de la limite
                if [ $fs_recursif_find_level -ge $fs_recursif_find_level_max ]
                then
                    echo-v "Récursivité maximale atteinte(fs_recursif_find_level > $fs_recursif_find_level_max)" 1;
                    (( fs_recursif_find_level-- ));    #display-vars '$fs_recursif_find_level' "$fs_recursif_find_level";
                    fct-pile-app-out 0 $_fct_pile_app_level; return 0;
                fi
            fi

            local _sousRep="";
            local _repertoiresListe=$(ls -1 "$_src/" );

            local _inode='';
            for _inode in $_repertoiresListe
            do
                if $isTrapCAsk;then fct-pile-app-out $E_CTRL_C $_fct_pile_app_level; return $E_CTRL_C; fi
                _inode="$_src/$_inode";
                echo-d "${FUNCNAME[0]}(): $_inode";

                # - L'inode est il un répertoire? Non -> continue  - #
                if [ ! -d "$_inode" ]
                then
                    #titreInfo "'$_inode' est PAS un réperrtoire";
                    continue;
                fi

                # - L'inode est un répertoire - #
                local _repertoire="$_inode";
                echo-d  "'$_repertoire' est un réperrtoire" 5;
                _sousRep="$_repertoire";

                ${FUNCNAME[0]} "$_sousRep";                     # recursivité
            done


            titre4_d '# - callback-rep-out - #' 2;
            if $fs_recursif_find_callback_rep_out_enable;
            then
                #echo 'AFTER';
                $fs_recursif_find_callback_rep_out "$_src";    # callback avant la sortie de repertoire
            fi

            (( fs_recursif_find_level-- ));#    display-vars '$fs_recursif_find_level' "$fs_recursif_find_level";

        elif [ -f "$_src" ]
        then
            echo-d "$LINENO:C'est un fichier."
            ((fs_recursif_find_fic_nb++))
            fs_recursif_find_pathname="$_src";
            ${fs_recursif_find_callback_fichier} "$_src";                      # callback du fichier
        fi
    
        fct-pile-app-out 0 $_fct_pile_app_level; return 0;
    }
#


return 0;