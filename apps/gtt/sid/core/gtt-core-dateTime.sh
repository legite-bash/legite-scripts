echo-D "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 

############
# HORODATE #
############

    # - horodate-before- #
    # Utilisé par: rien

    declare -g horodate_before='';
    declare -g horodate_before_date_format='%Y.%m.%d.%H.%M-';

    function horodate-before-date-format(){
        horodate_before_date_format="${1:-""}";
        return 0;
    }


    #horodate-before-set ( 'txt' )
    function horodate-before() {
        horodate_before='';        # reset dans tous les cas (pour ne pas renvoyer l'ancienne valeur)
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE;fi
        local _txt="${1:-""}";
        horodate_before="$(date +"$horodate_before_date_format")$_txt";
        return 0;
    }

    function horodate-before-display-vars() {
        display-vars 'horodate_before' "$horodate_before";
        return 0;
    }


    # - - #
     # - horodate-before- #
    # Utilisé par: rien

    declare -g horodate_after='';
    declare -g horodate_after_date_format="-%Y.%m.%d.%H.%M";

    function horodate-after-date-format(){
        horodate_after_date_format="${1:-""}";
        return 0;
    }


    #horodate-after-set ( 'txt' )
    function horodate-after() {
        horodate_after='';        # reset dans tous les cas (pour ne pas renvoyer l'ancienne valeur)
        if [ $# -eq 0 ];then return $E_ARG_REQUIRE;fi
        local _txt="${1:-""}";
        horodate_after="$_txt$(date +"$horodate_after_date_format")";
        return 0;
    }

    function horodate-after-display-vars() {
        display-vars 'horodate_after' "$horodate_after";
        return 0;
    }
#


######################
# objet:HHMMSS-split #
######################
    # split le parametre et set HHMMSS_split_hh,HHMMSS_split_mm,HHMMSS_split_ss et HHMMSS_split_str
    declare -i HHMMSS_split_hh=0;
    declare -i HHMMSS_split_mm=0;
    declare -i HHMMSS_split_ss=0;
    declare -i HHMMSS_split_secondes=0
    declare    HHMMSS_split_str='';

    # HHMMSS-split ([hh:][mm:]ss)
    function HHMMSS-split() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        if [ $# -ne 1 ]
        then
            erreurs-pile-add-echo "$LINENO:${FUNCNAME[0]}('[hh:][mm:]ss')($*). Appellé par ${FUNCNAME[1]}()";
            return $E_ARG_REQUIRE;
        fi

        local _HHMMSS="$1"
        local _HHMMSS_split_tbl=($(echo $_HHMMSS | tr ":" "\n"))
        local _HHMMSS_split_tbl_nb=${#_HHMMSS_split_tbl[@]}

        # - global - #
        HHMMSS_split_hh='';HHMMSS_split_mm='';HHMMSS_split_ss='';HHMMSS_split_str='';

        # - leadind zero - #
        local _HH='';
        local _MM='';
        local _SS='';

        if [ $_HHMMSS_split_tbl_nb -eq 1 ]
        then
            HHMMSS_split_ss=${_HHMMSS_split_tbl[0]};
            texte-lz $HHMMSS_split_ss; _SS="texte_lz";
            HHMMSS_split_str="$_SS";
        elif [ $_HHMMSS_split_tbl_nb -eq 2 ]
        then
            HHMMSS_split_mm=${_HHMMSS_split_tbl[0]};
            HHMMSS_split_ss=${_HHMMSS_split_tbl[1]};
            texte-lz $HHMMSS_split_mm; _MM="texte_lz";
            texte-lz $HHMMSS_split_ss; _SS="texte_lz";
            HHMMSS_split_str="$_MM:$_SS";
        elif [ $_HHMMSS_split_tbl_nb -eq 3 ]
        then
            HHMMSS_split_hh=10#${_HHMMSS_split_tbl[0]};
            HHMMSS_split_mm=10#${_HHMMSS_split_tbl[1]}; # forcer la base 10 sinon pb avec 8x
            HHMMSS_split_ss=10#${_HHMMSS_split_tbl[2]};
            texte-lz $HHMMSS_split_hh; _HH="texte_lz"
            texte-lz $HHMMSS_split_mm; _MM="texte_lz"
            texte-lz $HHMMSS_split_ss; _SS="texte_lz"
            HHMMSS_split_str="$_HH:$_MM:$_SS"
        fi
        HHMMSS_split_secondes=$(( HHMMSS_split_hh *3600 + HHMMSS_split_mm * 60 + HHMMSS_split_ss ))

        fct-pile-gtt-out 0 $_fct_pile_gtt_level; return 0;
    }

    function HHMMSS-split-display-vars() {
        display-vars 'HHMMSS_split_hh      ' "$HHMMSS_split_hh";
        display-vars 'HHMMSS_split_mm      ' "$HHMMSS_split_mm";
        display-vars 'HHMMSS_split_ss      ' "$HHMMSS_split_ss";
        display-vars 'HHMMSS_split_secondes' "$HHMMSS_split_secondes";
        display-vars 'HHMMSS_split_str     ' "$HHMMSS_split_str";
        return 0;
    }
#


############################
# objet:secondes-to-HHMMSS #
############################
    # Description: Transforme le nombre de seconde en HHMMSS
    # Utilisé par: gtt-duree-*
    declare -i secondes_to_HHMMSS_secondes=0;
    declare    secondes_to_HHMMSS_hh=0;
    declare    secondes_to_HHMMSS_mm=0;
    declare    secondes_to_HHMMSS_ss=0;
    declare    secondes_to_HHMMSS_str='';

    #
    addLibrairie 'secondes-to-HHMMSS' "- secondes=secondes: Met à jours les variables \$secondes_to_HHMMSS_*";
    function      secondes-to-HHMMSS() {
        local -i _fct_pile_gtt_level=2;
        fct-pile-gtt-in "$*" $_fct_pile_gtt_level;

        # - Controle des entrées - #
            # 1- $1
            # 2- ${PSP_params['s'] 
            
            secondes_to_HHMMSS_secondes="${1-0}";                  # 
            if [ $secondes_to_HHMMSS_secondes -eq 0 ]
            then
                secondes_to_HHMMSS_secondes="${PSP_params['secondes']:-0}";
            fi
        #


        # - leadind zero - #
        local -i  _secRestantes=0;
        secondes_to_HHMMSS_hh=$((secondes_to_HHMMSS_secondes/(60*60)));                
        _secRestantes=$((secondes_to_HHMMSS_secondes - (secondes_to_HHMMSS_hh*60*60)));    #display-vars '_secRestantes' "$_secRestantes";

        secondes_to_HHMMSS_mm=$((_secRestantes/60));
        if [ $secondes_to_HHMMSS_mm -le 0 ]
        then secondes_to_HHMMSS_mm=0;
        else _secRestantes=$((_secRestantes - (secondes_to_HHMMSS_mm*60)));    #display-vars '_secRestantes' "$_secRestantes";
        fi
        secondes_to_HHMMSS_ss=$_secRestantes;

        secondes_to_HHMMSS_str='';
        texte-lz $secondes_to_HHMMSS_hh;    secondes_to_HHMMSS_str+="${texte_lz}H";
        texte-lz $secondes_to_HHMMSS_mm;    secondes_to_HHMMSS_str+="${texte_lz}m";
        texte-lz $secondes_to_HHMMSS_ss;    secondes_to_HHMMSS_str+="${texte_lz}s";

        fct-pile-gtt-out 0 $_fct_pile_gtt_level; return 0;
    }


    function secondes-to-HHMMSS-display-vars() {
        display-vars 'secondes_to_HHMMSS_hh      ' "$secondes_to_HHMMSS_hh";
        display-vars 'secondes_to_HHMMSS_mm      ' "$secondes_to_HHMMSS_mm";
        display-vars 'secondes_to_HHMMSS_ss      ' "$secondes_to_HHMMSS_ss";
        display-vars 'secondes_to_HHMMSS_secondes' "$secondes_to_HHMMSS_secondes";
        display-vars 'secondes_to_HHMMSS_str     ' "$secondes_to_HHMMSS_str";
        return 0;
    }


    addLibrairie 'secondes-to-HHMMSS-str-echo' "- echo -n sur la variable \$secondes_to_HHMMSS_str";
    function      secondes-to-HHMMSS-str-echo() {
        echo -n "$secondes_to_HHMMSS_str";
        return 0;
    }


#


####################
# objet: gtt-duree #
####################
    declare -gi gtt_duree_debut=0;          # en seconde
    declare -gi gtt_duree_fin=0;            # en seconde
    declare -gi gtt_duree_duree=0;          # en seconde
    declare -g  gtt_duree_sep_decimal=',';
    declare -g  gtt_duree_sep_millier='.';
    declare -g  gtt_duree_HHMMSS_str='';
    function gtt-duree-debut(){
        gtt_duree_debut=$(date +%s); 
    }

    function gtt-duree-fin(){
        gtt_duree_fin=$(date +%s); 
        gtt_duree_duree=$((gtt_duree_fin-gtt_duree_debut)); 
    }

    function gtt-duree-show(){
        
        echo $gtt_duree_duree;
    }

    function gtt-duree-HHMMSS(){
        secondes-to-HHMMSS $gtt_duree_duree;
        gtt_duree_HHMMSS_str="$secondes_to_HHMMSS_str";
        #echo -n $gtt_duree_HHMMSS;
    }

#



