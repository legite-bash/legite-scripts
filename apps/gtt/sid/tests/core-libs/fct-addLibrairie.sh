echo "${BASH_SOURCE[0]}";


#addLibrairie 'test-libs': # pas d'execution -All
function       test-libs(){
    fctIn "$*";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

###################
# SYNTAXE D'APPEL #
###################
# forme minimal
addLibrairie 'nom_de_la_librairie';

# forme nom + description
addLibrairie 'fctDirect' 'Ceci est une librairie';

# forme nom + description + code (inline)
addLibrairie 'A_et_B' 'Execute la fonction A puis B' 'functionA;functionB'
addLibrairie 'code inline' 'Execute la fonction A puis B' 'echo "$LINENO:${FUNCNAME[0]}: code inline"' # FUNCNAME='evamCmd'
#addLibrairie 'code inline' 'Execute la fonction A puis B' "echo '$LINENO:${FUNCNAME[0]}: code inline'" # FUNCNAME existe pas


#############################
#  FONCTIONS NON LIBRAIRIES #
#############################
function fctDirect() {
    if $isTrapCAsk;then return $E_CTRL_C;fi
    fctIn "$*"
    if $IS_ROOT;then echo $WARN"${FUNCNAME[0]}($@) doit être lancé en root!"$NORMAL;fctOut "${FUNCNAME[0]}"; return $E_NOT_ROOT; fi
    if [ $# -ne 0 ];      then erreurs-pile-add-echo "${BASH_SOURCE[0]}:$LINENO:${FUNCNAME[0]}($@)( 'arg1' 'arg2' ) ($*) .Appellé par ${FUNCNAME[1]}()";       fctOut "${FUNCNAME[0]}"; return $E_ARG_REQUIRE;fi

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

function functionA() {
    if $isTrapCAsk;then return $E_CTRL_C;fi
    fctIn "${BASH_SOURCE[0]}:$LINENO:$*"
    #if [[ $IS_ROOT -eq 0 ]];then echo $WARN"${FUNCNAME[0]} doit être lancé en root!"$NORMAL;fctOut "${FUNCNAME[0]}"; return $E_NOT_ROOT; fi
    #if [[ $# -ne 0 ]];      then erreurs-pile-add-echo "${FUNCNAME[0]} ( 'arg1' 'arg2'  ).";       fctOut "${FUNCNAME[0]}"; return $E_ARG_REQUIRE;fi

    echo "${BASH_SOURCE[0]}:$LINENO:${FUNCNAME[0]}($*)"
    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

function functionB() {
    if $isTrapCAsk;then return $E_CTRL_C;fi
    fctIn "${BASH_SOURCE[0]}:$LINENO:$*"
    #if [[ $IS_ROOT -eq 0 ]];then echo $WARN"${FUNCNAME[0]} doit être lancé en root!"$NORMAL;fctOut "${FUNCNAME[0]}"; return $E_NOT_ROOT; fi
    #if [[ $# -ne 0 ]];      then erreurs-pile-add-echo "${FUNCNAME[0]} ( 'arg1' 'arg2'  ).";       fctOut "${FUNCNAME[0]}"; return $E_ARG_REQUIRE;fi

    echo "${BASH_SOURCE[0]}:$LINENO:${FUNCNAME[0]}($*)"

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}


##############
# LIBRAIRIES #
##############
addLibrairie 'fct-execLib';
function      fct-execLib(){
    fctIn
    execLib 'A_et_B'        # execute une liibrairie ajouté
    fctOut "${FUNCNAME[0]}(0)";return 0;
}

addLibrairie 'fct-isLibCall';
function      fct-isLibCall(){
    if $isTrapCAsk;then return $E_CTRL_C;fi
    fctIn "${BASH_SOURCE[0]}:$LINENO:$*"

    display-vars ''
    local _lib='_test_color';
    if isLibCall "$_lib"
    then notifs-pile-add-echo "La lib '$_lib' est appeller"
    else erreurs-pile-add-echo "La lib '$_lib' N'est PAS appeller"
    fi

    fctOut "${FUNCNAME[0]}(0)"; return 0;

}

function A() {
    if $isTrapCAsk;then return $E_CTRL_C;fi
    fctIn "$*"
    #if [ $IS_ROOT -eq 0 ];then echo $WARN"${FUNCNAME[0]} doit être lancé en root!"$NORMAL;fctOut; return $E_NOT_ROOT; fi
    #if [ $# -ne 0 ];      then erreurs-pile-add-echo "${FUNCNAME[0]} ( 'arg1' 'arg2'  ).";       fctOut; return $E_ARG_REQUIRE;fi

    echo "$LINENO${FUNCNAME[0]}($*)"
    fctOut; return 0;
}

function B() {
    if $isTrapCAsk;then return $E_CTRL_C;fi
    fctIn "$*"
    #if [ $IS_ROOT -eq 0 ];then echo $WARN"${FUNCNAME[0]} doit être lancé en root!"$NORMAL;fctOut; return $E_NOT_ROOT; fi
    #if [ $# -ne 0 ];      then erreurs-pile-add-echo "${FUNCNAME[0]} ( 'arg1' 'arg2'  ).";       fctOut; return $E_ARG_REQUIRE;fi

    echo "$LINENO${FUNCNAME[0]}($*)"

    fctOut; return 0;
}

addLibrairie 'A_et_B' 'A;B' 'Execute la fonction A puis B B'


addLibrairie 'lib de test avec espace' 'Le nom de la librairie contient un espace'
function lib_de_test_avec_espace(){
    fctIn
    #echo "Description de la librairie: ${librairiesDescr[*]}"
    echo "Description de la librairie: ${librairiesDescr['lib de test avec espace']}"

    fctOut;return 0;
}

addLibrairie 'lib_de_test' 'fonction standard qui sert pour les tests';
function lib_de_test(){
    fctIn;
    echo "Description de la librairie: ${librairiesDescr[${FUNCNAME[0]}]}";

    fctOut;return 0;
}