echo "${BASH_SOURCE[0]}";

addLibrairie 'fct-lib';
function      fct-lib(){
    fctIn "$*";

    libExecAuto 'fct-isLibOnly';
    libExecAuto 'fct-isLibExist';
    libExecAuto 'fct-setLibFail';

    fctOut "$${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-isLibOnly';
function      fct-isLibOnly(){
    fctIn "$*";

    titre3 'isLibOnly()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    isLibOnly '';
    display-vars '$?' "$?";

    fctOut "$${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-isLibExist';
function      fct-isLibExist(){
    fctIn "$*";

    titre3 'isLibExist()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    isLibExist '';
    display-vars '$?' "$?";

    fctOut "$${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-setLibFail';
function      fct-setLibFail(){
    fctIn "$*";

    titre3 'setLibFail()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    setLibFail '';
    display-vars '$?' "$?";

    fctOut "$${FUNCNAME[0]}(0)"; return 0;
}

