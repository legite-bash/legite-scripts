echo "${BASH_SOURCE[0]}";

##
# gtt.sh apps/gtt/sid/tests/fct-exec.sh


local un_local_est_t_il_possible_dans_une_source="?";
export SELF_REP="${BASH_SOURCE%/*}";
display-vars 'SELF_REP' "$SELF_REP";

_temoinPath="$COLLECTIONS_REP/gtt/tests/inc/temoin1.sh";

addLibrairie 'fct-exec';
function      fct-exec(){
    fctIn "$*";

    titreInfo 'Pour le mode Auto ';
    titreInfo 'Faites les tests avec différent -d';
    libExecAuto "fct-execFile";
    libExecAuto "fct-exec-file-once";
    libExecAuto "fct-require";
    libExecAuto "fct-require-once";

    echo "Fin des tests de ${FUNCNAME[0]}";
    
    fctOut "${FUNCNAME[0]}(0)"; return 0;
}


addLibrairie 'fct-execFile';
function      fct-execFile(){
    fctIn;

    titre4 'Appel de Fichier Inexistant':
    execFile 'Fichier Inexistant';

    titre4 'Appel de temoin1:1':
    execFile "$_temoinPath";
    titre4 'Appel de temoin1:2':
    execFile "$_temoinPath";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-exec-file-once';
function      fct-exec-file-once(){
    fctIn;

    titre4 'Appel de Fichier Inexistant':
    exec-file-once 'Fichier Inexistant';

    titre4 'Appel de temoin1:1':
    exec-file-once "$_temoinPath";
    titre4 'Appel de temoin1:2':
    exec-file-once "$_temoinPath";


    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-require';
function      fct-require(){
    fctIn;

    #titre4 'Appel de Fichier Inexistant':
    #require 'Fichier Inexistant';

    titre4 'Appel de temoin2:1':
    require "$_temoinPath";
    titre4 'Appel de temoin2:2':
    require "$_temoinPath";


    fctOut "${FUNCNAME[0]}(0)"; return 0;
}


addLibrairie 'fct-require-once';
function      fct-require-once(){
    fctIn;

    #titre4 'Appel de Fichier Inexistant':
    #require-once 'Fichier Inexistant';

    titre4 'Appel de temoin2:1':
    require-once "$_temoinPath";
    titre4 'Appel de temoin2:2':
    require-once "$_temoinPath";


    fctOut "${FUNCNAME[0]}(0)"; return 0;
}
