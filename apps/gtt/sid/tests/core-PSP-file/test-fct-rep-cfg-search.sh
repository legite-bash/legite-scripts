echo "${BASH_SOURCE[0]}";

# date de création    : 2024.08.22
# date de modification: 2024.08.22
# Description: 

local _source_rep="${BASH_SOURCE%/*.*}";        # display-vars '_source_rep' "$_source_rep";
local _source_rep_last="${_source_rep##*/}";    # display-vars '_source_rep_last' "$_source_rep_last";
local _source_name="${BASH_SOURCE##*/}";        # display-vars '_source_name' "$_source_name";
local _source_nom="${_source_name%.*}";         # display-vars '_source_nom' "$_source_nom";
local _source_ext="${_source_name##*.}";        # display-vars '_source_ext' "$_source_ext";

local _test_rep_rel="${_source_rep##$GTT_VERSION_SELECT_REP}";  # display-vars '_test_rep_rel' "$_test_rep_rel";  # /tests/./


# clear;gtt.sh --libExecAuto apps/gtt/sid/tests/core-PSP-file/ test-fct-rep-cfg-search
addLibrairie 'test-fct-rep-cfg-search';
function      test-fct-rep-cfg-search(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre0 "${FUNCNAME[0]}()";
    titreInfo  '';

    libExecAuto 'test-fct-searchRepertoiresCfg--vide';
    libExecAuto 'test-fct-searchRepertoiresCfg--repertoire_inexistant';
    #libExecAuto 'test-fct-loadRepertoireCfg--autres';
    
    fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
}


# clear;gtt.sh apps/gtt/sid/tests/core-PSP-file/ test-fct-searchRepertoiresCfg--vide
addLibrairie 'test-fct-searchRepertoiresCfg--vide';
function      test-fct-searchRepertoiresCfg--vide(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    local _programme="searchRepertoiresCfg";

    titre2 "${_programme}()";

    titre4 'sans argument':
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide':
    eval-echo "$_programme ''";
    erreur-no-description-echo;

    fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
}


# clear;gtt.sh apps/gtt/sid/tests/core-PSP-file/ test-fct-searchRepertoiresCfg--repertoire_inexistant
addLibrairie 'test-fct-searchRepertoiresCfg--repertoire_inexistant';
function      test-fct-searchRepertoiresCfg--repertoire_inexistant(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre4 'repertoire_inexistant';

    local _programme="searchRepertoiresCfg";
    
    local _rep="${tmp_rep}/repertoire_inexistant-eheqhttrjyrj";
    eval-echo "$_programme '$_rep'";
    erreur-no-description-echo $?;

    fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
}


# clear;gtt.sh apps/gtt/sid/tests/core-PSP-file/ test-fct-searchRepertoiresCfg--autres
addLibrairie 'test-fct-searchRepertoiresCfg--autres';
function      test-fct-searchRepertoiresCfg--autres(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre4 'autres';
    local _programme="loadRepertoireCfg";
    

    # Sans objet ici puisqu'il ne recherche pas dans les repertoire parents
    #local _rep="$APPS_REP/rep_inexistant-khbjvk;b";
    #titre2 'Répertoire parents existant avec _ini.cfg.sh mais repertoire cible inexistant':
    #eval-echo "loadRepertoireCfg '$_rep'";
    #erreur-no-description-echo $?;


    local _rep="${_source_rep}/fct-searchRepertoiresCfg-rep";
    titre4 "repertoire '$_rep'":
    eval-echo "$_programme '$_rep'";
    erreur-no-description-echo $?;


    #local _rep="gtt/_tests";
    #titre2 "repertoire collection 1er niveau: ($_rep)":
    #eval-echo "loadRepertoireCfg '$_rep'";
    #erreur-no-description-echo $?;

    #local _rep="install/debian";
    #titre2 "repertoire app:($_rep)":
    #eval-echo "loadRepertoireCfg '$_rep';";
    #erreur-no-description-echo $?;


    #local _rep="gtt/_tests/essais";
    #titre2 "repertoire collection 1er niveau: ($_rep)"":
    #loadRepertoireCfg "$_rep";
    #displayVar '$?' "$?";


    local _rep="/tmp";
    titre2 "repertoire absolu '$_rep' existant mais sans _ini.cfg.sh":
    eval-echo "loadRepertoiresCfg '$_rep'";
    erreur-no-description-echo $?;


    local _rep="$APPS_REP/install/";
    titre2 "repertoire absolu: '$_rep'":
    eval-echo "loadRepertoiresCfg '$_rep';";
    erreur-no-description-echo $?;

    local _rep="apps/install/logiciels/";
    titre2 "repertoire relatifcommencant par apps: ($_rep) SANS INDIQUER la base_rep":
    eval-echo "loadRepertoiresCfg '$_rep';";
    erreur-no-description-echo $?;

    local _rep="install/logiciels/";
    titre2 "repertoire relatifcommencant par apps: ($_rep) EN INDIQUANT la base_rep":
    eval-echo "loadRepertoiresCfg '$_rep' '$APPS_REP';";
    erreur-no-description-echo $?;



    local _rep="gtt/sid/tests/fct/fct-loadRepertoireCfg-rep";
    titre2 "repertoire '$_rep'":
    eval-echo "searchRepertoiresCfg '$_rep'";
    erreur-no-description-echo $?;


    local _rep="install/logiciels/firefox.sh";
    titre2 "repertoire app:($_rep)":
    eval-echo "searchRepertoiresCfg '$_rep';";
    erreur-no-description-echo $?;




    fct-pile-gtt-out $E_TRUE $_fct_pile_gtt_level; return $E_TRUE;
}

return 0;



