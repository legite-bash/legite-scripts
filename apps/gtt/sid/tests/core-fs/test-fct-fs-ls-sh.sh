echo "${BASH_SOURCE[0]}";

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.23
# Description: 
# clear; gtt.sh --show-libsLevel2 apps/gtt/sid/tests/core-fs/ 


local _test_rep_rel="${_source_rep##$GTT_VERSION_SELECT_REP}";  # display-vars '_test_rep_rel' "$_test_rep_rel";  # /tests/./


# clear; gtt.sh --libExecAuto apps/gtt/sid/tests/core-fs/ test-fct-fs-ls-sh
addLibrairie 'test-fct-fs-ls-sh' "";
function      test-fct-fs-ls-sh(){

    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;


    local _source_rep="${BASH_SOURCE%/*.*}";                        # display-vars '_source_rep' "$_source_rep";
    local _source_rep_last="${_source_rep##*/}";                    # display-vars '_source_rep_last' "$_source_rep_last";
    local _source_name="${BASH_SOURCE##*/}";                        # display-vars '_source_name' "$_source_name";
    local _source_nom="${_source_name%.*}";                         # display-vars '_source_nom' "$_source_nom";
    local _source_ext="${_source_name##*.}";                        # display-vars '_source_ext' "$_source_ext";


    local _programme="fs-ls-sh";
    titre0 "${FUNCNAME[0]}";

    titre4 'sans argument':
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide':
    eval-echo "$_programme ''";
    erreur-no-description-echo;


    titre4 'Un fichier':
    eval-echo "$_programme '$VIDEOTEST_PATHNAME'";
    erreur-no-description-echo;


    titre4 'Un répertoire':
    eval-echo "$_programme $_source_rep";
    erreur-no-description-echo;


    titre4 'Un répertoire avec destination indiquée':
    eval-echo "$_programme '$_source_rep' '$VIDEOTEST_REP'";
    erreur-no-description-echo;


    titre4 'echo':
    eval-echo "$_programme";
    erreur-no-description-echo;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

return 0;