echo "${BASH_SOURCE[0]}";

# gtt.sh 
# date de création    : ?
# date de modification: 2024.09.17
# Description: 
# clear; gtt.sh --show-libsLevel2 apps/gtt/sid/tests/core-fs/


# clear;gtt.sh  --libExecAuto apps/gtt/sid/tests/core-fs/ test-fct-fs-mk
addLibrairie 'test-fct-fs-mk';
function      test-fct-fs-mk(){
    titre1 "${FUNCNAME[0]}";

    libExecAuto test-fct-fs-mk-link;
    libExecAuto test-fct-fs-mk-rep;

    return 0;
}


# clear;gtt.sh  apps/gtt/sid/tests/core-fs/ test-fct-fs-mk-link
addLibrairie 'test-fct-fs-mk-link';
function      test-fct-fs-mk-link(){

    local _programme="fs-mk-link";
    titre2 "${_programme}()";

    titre3 "vide";
    titre4 'sans argument':
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide':
    eval-echo "$_programme ''";
    erreur-no-description-echo;

    titre3 "execution";

    titre4 '1 argument seul':
    eval-echo "$_programme '$VIDEOTEST_PATHNAME'";
    erreur-no-description-echo;

    titre4 '1 source NON existante vers une destination exitante ':
    local _src_nomExistant_rep="${tmp_rep}/rep_inexistant-rvrebutkè_è-u";
    eval-echo "$_programme '$_src_nomExistant_rep' '$VIDEOTEST_REP'";
    erreur-no-description-echo;


    titre4 '1 source existante vers une destination non exitante -> renommer le lien':
    eval-echo "$_programme '$VIDEOTEST_PATHNAME' '${tmp_rep}/rep_inexistant'";
    erreur-no-description-echo;

    titre4 '1 source existante vers une sous répertoire non exitante':
    eval-echo "$_programme '$VIDEOTEST_PATHNAME' '${tmp_rep}//rep_qui_existe_pas/rep_inexistant'";
    erreur-no-description-echo;


    titre4 '1 source existante vers une destination':
    local destination_1="${tmp_rep}/mk-link-destination";
    if [ -e "${destination_1}" ]; then unlink "${destination_1}"; fi

    eval-echo "$_programme '$VIDEOTEST_PATHNAME' '${destination_1}'";
    erreur-no-description-echo;


    titre4 '1 source existante vers une destination exitante':
    eval-echo "$_programme '$VIDEOTEST_PATHNAME' '${destination_1}'";
    erreur-no-description-echo;

    return 0;
}


return 0;