echo-d "${BASH_SOURCE[0]}" 1;

# date de création    : ?
# date de modification: 2024.10.28
# Description: 
# clear; gtt.sh --show-libsLevel2 gtt/sid/tests/core-inode/ 

# clear; gtt.sh --libExecAuto gtt/sid/tests/core-inode/ test-obj-inode
addLibrairie 'test-obj-inode';
function      test-obj-inode(){

    titre1 "obj: inode-*";

    inode-init;     # facultatif
    libExecAuto test-inode-pathname;
    libExecAuto test-inode-rep;
    libExecAuto test-inode-setNom;
    libExecAuto test-inode-setExt;

    return 0;
}


# clear;gtt.sh gtt/sid/tests/core-inode/ test-inode-pathname
addLibrairie 'test-inode-pathname';
function      test-inode-pathname(){
    local _obj='inode';

    titre2 "${_obj}()";
    ${_obj}-display-vars

    titre4 'Appel sans argument';
    eval-echo "${_obj}-pathname";
    ${_obj}-display-vars

    titre4 'Appel argument vide';
    eval-echo "${_obj}-pathname ''";
    ${_obj}-display-vars


    titre3 'répertoire';
    titre4 "Repertoire racine seul";
    eval-echo "${_obj}-pathname '/'";
    ${_obj}-display-vars

    titre4 "Repertoire seul sans slash de fin: c'est un fichier";
    eval-echo "${_obj}-pathname '/root'";
    ${_obj}-display-vars

    titre4 "Repertoire seul AVEC slash de fin: ";
    eval-echo "${_obj}-pathname '/root/'";
    ${_obj}-display-vars

    titre4 'Répertoire local (qui commence par un ./)';
    eval-echo "${_obj}-pathname './fichier.extention'";
    ${_obj}-display-vars
    eval-echo "${_obj}-pathname './répertoire/fichier.extention'";
    ${_obj}-display-vars
    eval-echo "${_obj}-pathname './.répertoire/fichier.extention'";
    ${_obj}-display-vars


    titre4 "Répertoire relatif (qui n'a poas de slash racine)";
    eval-echo "${_obj}-pathname 'rep1/rep2/rep3/'";
    ${_obj}-display-vars
    eval-echo "${_obj}-pathname 'rep1/rep2/rep3/fichier.extention'";
    ${_obj}-display-vars


    titre4 "Repertoires multiples: ";
    eval-echo "${_obj}-pathname '/root/rep1/rep2.ext/fichier.extention/'";
    ${_obj}-display-vars



    titre3 'nom';
    titre4 'Appel nom seul';
    eval-echo "${_obj}-pathname 'fichierSeul'";
    ${_obj}-display-vars


    titre4 'Appel nom seul qui finit par un Point';
    eval-echo "${_obj}-pathname 'fichierSeul.'";
    ${_obj}-display-vars

    titre4 'Appel rep/fichier. (qui) finit par un Point';
    eval-echo "${_obj}-pathname '/root/fichierSeul.'";
    ${_obj}-display-vars


    titre4 'Fichier avec des points';
    eval-echo "${_obj}-pathname '/root/fichier.avec.des.points.extention'";
    ${_obj}-display-vars


    titre3 'Fichier caché (qui commence par un point)';
    titre4 "Appel extention (fichier caché, commence par un Point '.')";
    eval-echo "${_obj}-pathname '.htaccess'";
    ${_obj}-display-vars


    titre4 "Appel repertoire/.fichier_caché";
    eval-echo "${_obj}-pathname '/root/.htacess2'";
    ${_obj}-display-vars

    titre4 'Fichier caché avec une extention';
    eval-echo "${_obj}-pathname '/root/rep1/rep2.ext/.fichier.extention'";
    ${_obj}-display-vars

    titre4 'Fichier caché, qui finit par un point, sans extention';
    eval-echo "${_obj}-pathname '/root/rep1/rep2.ext/.fichier.'";
    ${_obj}-display-vars

    titre4 'Fichier caché, qui finit par un point, AVEC extention';
    eval-echo "${_obj}-pathname '/root/rep1/rep2.ext/.fichier..extention'";
    ${_obj}-display-vars


    titre3 'Appel complet';
    eval-echo "${_obj}-pathname '/root/rep1/rep2/fichier.extention'";
    ${_obj}-display-vars

    titre4 '';
    eval-echo "${_obj}-pathname '/root/rep1/rep2.ext/fichier.extention'";
    ${_obj}-display-vars

    titre4 'Fichier qui finit par un point';
    eval-echo "${_obj}-pathname '/root/rep1/rep2.ext/fichier..extention'";
    ${_obj}-display-vars


    titre3 'Cas particulier';
    eval-echo "${_obj}-pathname '/repEgalNom/repEgalNom'";
    ${_obj}-display-vars

    eval-echo "${_obj}-pathname '/repEgalNom./repEgalNom.'";
    ${_obj}-display-vars

    eval-echo "${_obj}-pathname '/.repEgalNom/.repEgalNom'";
    ${_obj}-display-vars

    return 0;
}


# clear;gtt.sh gtt/sid/tests/core-inode/ test-inode-rep
addLibrairie 'test-inode-rep';
function      test-inode-rep(){
    local _obj='inode';
    titre2 "${_obj}-rep()";

    titreInfo "Initialisation de l'objet avec pathname"
    eval-echo "${_obj}-rep '/ancien_root/_rep1/fichier.extention';";
    ${_obj}-display-vars;


    titre2 'Appel sans argument';
    eval-echo "${_obj}-rep";
    ${_obj}-display-vars;

    titre2 'Appel argument vide';
    eval-echo "${_obj}-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "${_obj}-rep ''";
    ${_obj}-display-vars;


    titre2 'répertoire';

    titre3 "Repertoire racine seul";
    eval-echo "${_obj}-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "${_obj}-rep '/'";
    ${_obj}-display-vars;

    titre3 "Repertoire seul sans slash de fin";
    eval-echo "${_obj}-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "${_obj}-rep '/root'";
    ${_obj}-display-vars;

    titre3 "Repertoire seul AVEC slash de fin: ";
    eval-echo "${_obj}-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "${_obj}-rep '/root/'";
    ${_obj}-display-vars;

    titre3 "Repertoires multiples: ";
    eval-echo "${_obj}-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "${_obj}-rep '/root/rep1/rep2.ext/'";
    ${_obj}-display-vars;

    return 0;
}


# clear;gtt.sh gtt/sid/tests/core-inode/ test-inode-nom
addLibrairie 'test-inode-nom';
function      test-inode-nom(){
    local _obj='inode';
    titre2 "${_obj}-nom()";

    titreInfo "Initialisation de l'objet avec pathname"
    eval-echo "${_obj}-pathname '/ancien_root/_rep1/fichier.extention';";
    ${_obj}-display-vars;


    titre3 'Appel sans argument';
    eval-echo "${_obj}-nom";
    ${_obj}-display-vars;

    titre3 'Appel argument vide';
    eval-echo "${_obj}pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "${_obj}-nom ''";
    ${_obj}-display-vars;


    titre3 "nom seul";
    eval-echo "${_obj}-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "${_obj}-nom 'nom seul'";
    ${_obj}-display-vars;

    titre3 "nom caché";
    eval-echo "${_obj}-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "${_obj}-nom '.nom caché'";
    ${_obj}-display-vars;

    eval-echo "${_obj}-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "${_obj}-nom '.nom caché. Qui finit avec un .'";
    ${_obj}-display-vars;

    return 0;
}

# clear;gtt.sh gtt/sid/tests/core-inode/ test-inode-ext
addLibrairie 'test-inode-ext';
function      test-inode-ext(){
    local _obj='inode';
    titre2 "${_obj}-ext()";

    titreInfo "Initialisation de l'objet avec pathname"
    eval-echo "${_obj}-pathname '/ancien_root/_rep1/fichier.extention';";
    ${_obj}-display-vars;


    titre3 'Appel sans argument';
    eval-echo "${_obj}-ext";
    ${_obj}-display-vars;

    titre3 'Appel argument vide';
    eval-echo "${_obj}-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "${_obj}-ext ''";
    ${_obj}-display-vars;


    titre3 "extention seule";
    eval-echo "inode-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "${_obj}-ext 'extention seul'";
    ${_obj}-display-vars;

    titre3 "extention qui commence avec un .";
    eval-echo "inode-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "inode-ext '.extention'";
    ${_obj}-display-vars;

    eval-echo "${_obj}-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "${_obj}-ext '.extention.'";
    ${_obj}-display-vars;


    return 0;
}