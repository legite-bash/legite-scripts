echo "${BASH_SOURCE[0]}";

# date de création    : ?
# date de modification: 2024.09.04
# Description: 


# clear;gtt.sh  --libExecAuto apps/gtt/sid/tests/core-inode/ test-obj-src
addLibrairie 'test-obj-src';
function      test-obj-src(){

    titre1 "obj: src_*";
    src-init;     # facultatif
    libExecAuto test-src-pathname;
    libExecAuto test-src-rep;
    libExecAuto test-src-nom;
    libExecAuto test-src-ext;

    return 0;
}


# clear;gtt.sh  apps/gtt/sid/tests/core-inode/ test-src-pathname
addLibrairie 'test-src-pathname';
function      test-src-pathname(){

    titre2 'src-pathname()';
    src-display-vars;

    titre4 'Appel sans argument';
    eval-echo "src-pathname;";
    src-display-vars;

    titre4 'Appel argument vide';
    eval-echo "src-pathname '';";
    src-display-vars;


    titre3 'répertoire';
    titre4 "Repertoire racine seul";
    eval-echo "src-pathname '/'";
    src-display-vars;

    titre4 "Repertoire seul sans slash de fin: c'est un fichier";
    eval-echo "src-pathname '/root'";
    src-display-vars;

    titre4 "Repertoire seul AVEC slash de fin: ";
    eval-echo "src-pathname '/root/'";
    src-display-vars;

    titre4 'Répertoire local (qui commence par un ./)';
    eval-echo "src-pathname './fichier.extention'";
    src-display-vars;
    eval-echo "src-pathname './répertoire/fichier.extention'";
    src-display-vars;
    eval-echo "src-pathname './.répertoire/fichier.extention'";
    src-display-vars;


    titre4 "Repertoires multiples: ";
    eval-echo "src-pathname '/root/rep1/rep2.ext/fichier.extention/'";
    src-display-vars;


    titre3 'nom';
    titre4 'Appel nom seul';
    eval-echo "src-pathname 'fichierSeul'";
    src-display-vars;


    titre4 'Appel nom seul qui finit par un Point';
    eval-echo "src-pathname 'fichierSeul.'";
    src-display-vars;

    titre4 'Appel rep/fichier. (qui) finit par un Point';
    eval-echo "src-pathname '/root/fichierSeul.'";
    src-display-vars;


    titre4 'Fichier avec des points';
    eval-echo "src-pathname '/root/fichier.avec.des.points.extention'";
    src-display-vars;



    titre3 'Fichier caché (qui commence par un point)';
    titre4 "Appel extention (fichier caché, commence par un Point '.')";
    eval-echo "src-pathname '.htaccess'";
    src-display-vars;


    titre4 "Appel repertoire/.fichier_caché";
    eval-echo "src-pathname '/root/.htacess2'";
    src-display-vars;

    titre4 'Fichier caché avec une extention';
    eval-echo "src-pathname '/root/rep1/rep2.ext/.fichier.extention'";
    src-display-vars;

    titre4 'Fichier caché, qui finit par un point, sans extention';
    eval-echo "src-pathname '/root/rep1/rep2.ext/.fichier.'";
    src-display-vars;

    titre4 'Fichier caché, qui finit par un point, AVEC extention';
    eval-echo "src-pathname '/root/rep1/rep2.ext/.fichier..extention'";
    src-display-vars;


    titre3 'Appel complet';
    eval-echo "src-pathname '/root/rep1/rep2/fichier.extention'";
    src-display-vars;

    titre4 '';
    eval-echo "src-pathname '/root/rep1/rep2.ext/fichier.extention'";
    src-display-vars;

    titre4 'Fichier qui finit par un point';
    eval-echo "src-pathname '/root/rep1/rep2.ext/fichier..extention'";
    src-display-vars;


    titre3 'Cas particulier';
    eval-echo "src-pathname '/repEgalNom/repEgalNom'";
    src-display-vars;

    eval-echo "src-pathname '/repEgalNom./repEgalNom.'";
    src-display-vars;

    eval-echo "src-pathname '/.repEgalNom/.repEgalNom'";
    src-display-vars;

    return 0;
}


# clear;gtt.sh  apps/gtt/sid/tests/core-inode/ test-src-rep
addLibrairie 'test-src-rep';
function      test-src-rep(){

    titre2 'src-rep()';

    titreInfo "Initialisation de l'objet avec pathname"
    eval-echo "src-pathname '/ancien_root/_rep1/fichier.extention';";
    src-display-vars;


    titre2 'Appel sans argument';
    eval-echo "src-rep;";
    src-display-vars;

    titre2 'Appel argument vide';
    eval-echo "src-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "src-rep '';";
    src-display-vars;


    titre2 'répertoire';

    titre3 "Repertoire racine seul";
    eval-echo "src-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "src-rep '/'";
    src-display-vars;

    titre3 "Repertoire seul sans slash de fin";
    eval-echo "src-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "src-rep '/root'";
    src-display-vars;

    titre3 "Repertoire seul AVEC slash de fin: ";
    eval-echo "src-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "src-rep '/root/'";
    src-display-vars;

    titre3 "Repertoires multiples: ";
    eval-echo "src-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "src-rep '/root/rep1/rep2.ext/'";
    src-display-vars;

    return 0;
}


# clear;gtt.sh  apps/gtt/sid/tests/core-inode/ test-src-nom
addLibrairie 'test-src-nom';
function      test-src-nom(){

    titre2 'src-nom()';

    titreInfo "Initialisation de l'objet avec pathname"
    eval-echo "src-pathname '/ancien_root/_rep1/fichier.extention';";
    src-display-vars;


    titre3 'Appel sans argument';
    eval-echo "src-nom;";
    src-display-vars;

    titre3 'Appel argument vide';
    eval-echo "src-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "src-nom '';";
    src-display-vars;


    titre3 "nom seul";
    eval-echo "src-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "src-nom 'nom seul'";
    src-display-vars;

    titre3 "nom caché";
    eval-echo "src-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "src-nom '.nom caché'";
    src-display-vars;

    eval-echo "src-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "src-nom '.nom caché. Qui finit avec un .'";
    src-display-vars;

    return 0;
}

# clear;gtt.sh  apps/gtt/sid/tests/core-inode/ test-src-ext
addLibrairie 'test-src-ext';
function      test-src-ext(){


    titre2 'src-ext()';

    titreInfo "Initialisation de l'objet avec pathname"
    eval-echo "src-pathname '/ancien_root/_rep1/fichier.extention';";
    src-display-vars;


    titre3 'Appel sans argument';
    eval-echo "src-ext;";
    src-display-vars;

    titre3 'Appel argument vide';
    eval-echo "src-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "src-ext '';";
    src-display-vars;


    titre3 "extention seule";
    eval-echo "src-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "src-ext 'extention seul'";
    src-display-vars;

    titre3 "extention qui commence avec un .";
    eval-echo "src-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "src-ext '.extention'";
    src-display-vars;

    eval-echo "src-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "src-ext '.extention.'";
    src-display-vars;


    return 0;
}