echo "${BASH_SOURCE[0]}";

# date de création    : ?
# date de modification: 2024.09.04
# Description: 


# clear;gtt.sh  --libExecAuto apps/gtt/sid/tests/core-inode/ test-obj-dest
addLibrairie 'test-obj-dest';
function      test-obj-dest(){
    titre1 "obj: inode-*";
    dest-init;     # facultatif
    libExecAuto test-dest-pathname;
    libExecAuto test-dest-rep;
    libExecAuto test-dest-nom;
    libExecAuto test-dest-ext;

    return 0;
}


# clear;gtt.sh  apps/gtt/sid/tests/core-inode/ test-dest-pathname
addLibrairie 'test-dest-pathname';
function      test-dest-pathname(){

    titre2 'dest-pathname()';
    dest-display-vars;

    titre4 'Appel sans argument';
    eval-echo "dest-pathname;";
    dest-display-vars;

    titre4 'Appel argument vide';
    eval-echo "dest-pathname '';";
    dest-display-vars;


    titre3 'répertoire';
    titre4 "Repertoire racine seul";
    eval-echo "dest-pathname '/'";
    dest-display-vars;

    titre4 "Repertoire seul sans slash de fin: c'est un fichier";
    eval-echo "dest-pathname '/root'";
    dest-display-vars;

    titre4 "Repertoire seul AVEC slash de fin: ";
    eval-echo "dest-pathname '/root/'";
    dest-display-vars;

    titre4 'Répertoire local (qui commence par un ./)';
    eval-echo "dest-pathname './fichier.extention'";
    dest-display-vars;
    eval-echo "dest-pathname './répertoire/fichier.extention'";
    dest-display-vars;
    eval-echo "dest-pathname './.répertoire/fichier.extention'";
    dest-display-vars;


    titre4 "Repertoires multiples: ";
    eval-echo "dest-pathname '/root/rep1/rep2.ext/fichier.extention/'";
    dest-display-vars;


    titre3 'nom';
    titre4 'Appel nom seul';
    eval-echo "dest-pathname 'fichierSeul'";
    dest-display-vars;


    titre4 'Appel nom seul qui finit par un Point';
    eval-echo "dest-pathname 'fichierSeul.'";
    dest-display-vars;

    titre4 'Appel rep/fichier. (qui) finit par un Point';
    eval-echo "dest-pathname '/root/fichierSeul.'";
    dest-display-vars;


    titre4 'Fichier avec des points';
    eval-echo "dest-pathname '/root/fichier.avec.des.points.extention'";
    dest-display-vars;



    titre3 'Fichier caché (qui commence par un point)';
    titre4 "Appel extention (fichier caché, commence par un Point '.')";
    eval-echo "dest-pathname '.htaccess'";
    dest-display-vars;


    titre4 "Appel repertoire/.fichier_caché";
    eval-echo "dest-pathname '/root/.htacess2'";
    dest-display-vars;

    titre4 'Fichier caché avec une extention';
    eval-echo "dest-pathname '/root/rep1/rep2.ext/.fichier.extention'";
    dest-display-vars;

    titre4 'Fichier caché, qui finit par un point, sans extention';
    eval-echo "dest-pathname '/root/rep1/rep2.ext/.fichier.'";
    dest-display-vars;

    titre4 'Fichier caché, qui finit par un point, AVEC extention';
    eval-echo "dest-pathname '/root/rep1/rep2.ext/.fichier..extention'";
    dest-display-vars;


    titre3 'Appel complet';
    eval-echo "dest-pathname '/root/rep1/rep2/fichier.extention'";
    dest-display-vars;

    titre4 '';
    eval-echo "dest-pathname '/root/rep1/rep2.ext/fichier.extention'";
    dest-display-vars;

    titre4 'Fichier qui finit par un point';
    eval-echo "dest-pathname '/root/rep1/rep2.ext/fichier..extention'";
    dest-display-vars;


    titre3 'Cas particulier';
    eval-echo "dest-pathname '/repEgalNom/repEgalNom'";
    dest-display-vars;

    eval-echo "dest-pathname '/repEgalNom./repEgalNom.'";
    dest-display-vars;

    eval-echo "dest-pathname '/.repEgalNom/.repEgalNom'";
    dest-display-vars;


    return 0;
}


# clear;gtt.sh  apps/gtt/sid/tests test-dest-rep
addLibrairie 'test-dest-rep';
function      test-dest-rep(){

    titre2 'dest-rep()';

    titreInfo "Initialisation de l'objet avec dest-pathname()"
    eval-echo "dest-pathname '/ancien_root/_rep1/fichier.extention';";
    dest-display-vars;


    titre2 'Appel sans argument';
    eval-echo "dest-rep;";
    dest-display-vars;

    titre2 'Appel argument vide';
    eval-echo "dest-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "dest-rep '';";
    dest-display-vars;


    titre2 'répertoire';

    titre3 "Repertoire racine seul";
    eval-echo "dest-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "dest-rep '/'";
    dest-display-vars;

    titre3 "Repertoire seul sans slash de fin";
    eval-echo "dest-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "dest-rep '/root'";
    dest-display-vars;

    titre3 "Repertoire seul AVEC slash de fin: ";
    eval-echo "dest-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "dest-rep '/root/'";
    dest-display-vars;

    titre3 "Repertoires multiples: ";
    eval-echo "dest-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "dest-rep '/root/rep1/rep2.ext/'";
    dest-display-vars;

    return 0;
}


# clear;gtt.sh  apps/gtt/sid/tests test-dest-nom
addLibrairie 'test-dest-nom';
function      test-dest-nom(){

    titre2 'dest-nom()';

    titreInfo "Initialisation de l'objet avec pathname"
    eval-echo "dest-pathname '/ancien_root/_rep1/fichier.extention';";
    dest-display-vars;


    titre3 'Appel sans argument';
    eval-echo "dest-nom;";
    dest-display-vars;

    titre3 'Appel argument vide';
    eval-echo "dest-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "dest-nom '';";
    dest-display-vars;


    titre3 "nom seul";
    eval-echo "dest-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "dest-nom 'nom seul'";
    dest-display-vars;

    titre3 "nom caché";
    eval-echo "dest-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "dest-nom '.nom caché'";
    dest-display-vars;

    eval-echo "dest-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "dest-nom '.nom caché. Qui finit avec un .'";
    dest-display-vars;

    return 0;
}

# clear;gtt.sh  apps/gtt/sid/tests test-dest-ext
addLibrairie 'test-dest-ext';
function      test-dest-ext(){

    titre2 'dest-ext()';

    titreInfo "Initialisation de l'objet avec pathname"
    eval-echo "dest-pathname '/ancien_root/_rep1/fichier.extention';";
    dest-display-vars;


    titre3 'Appel sans argument';
    eval-echo "dest-ext;";
    dest-display-vars;

    titre3 'Appel argument vide';
    eval-echo "dest-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "dest-ext '';";
    dest-display-vars;


    titre3 "extention seule";
    eval-echo "dest-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "dest-ext 'extention seul'";
    dest-display-vars;

    titre3 "extention qui commence avec un .";
    eval-echo "dest-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "dest-ext '.extention'";
    dest-display-vars;

    eval-echo "dest-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "dest-ext '.extention.'";
    dest-display-vars;


    return 0;
}