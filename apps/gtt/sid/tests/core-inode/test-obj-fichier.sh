echo "${BASH_SOURCE[0]}";

# date de création    : ?
# date de modification: 2024.09.04
# Description: 


# clear; gtt.sh  --libExecAuto apps/gtt/sid/tests/core-inode/ test-obj-fichier
addLibrairie 'test-obj-fichier';
function      test-obj-fichier(){

    titre1 "obj: fichier-*";
    fichier-init;   # facultatif
    
    libExecAuto test-fichier-pathname;
    libExecAuto test-fichier-rep;
    libExecAuto test-fichier-nom;
    libExecAuto test-fichier-ext;

    return 0;
}


# clear;gtt.sh  apps/gtt/sid/tests/core-inode/ test-fichier-pathname
addLibrairie 'test-fichier-pathname';
function      test-fichier-pathname(){

    titre2 'fichier-pathname()';
    fichier-display-vars;

    titre4 'Appel sans argument';
    eval-echo "fichier-pathname;";
    fichier-display-vars;

    titre4 'Appel argument vide';
    eval-echo "fichier-pathname '';";
    fichier-display-vars;


    titre3 'répertoire';
    titre4 "Repertoire racine seul";
    eval-echo "fichier-pathname '/'";
    fichier-display-vars;

    titre4 "Repertoire seul sans slash de fin: c'est un fichier";
    eval-echo "fichier-pathname '/root'";
    fichier-display-vars;

    titre4 "Repertoire seul AVEC slash de fin: ";
    eval-echo "fichier-pathname '/root/'";
    fichier-display-vars;

    titre4 'Répertoire local (qui commence par un ./)';
    eval-echo "fichier-pathname './fichier.extention'";
    fichier-display-vars;
    eval-echo "fichier-pathname './répertoire/fichier.extention'";
    fichier-display-vars;
    eval-echo "fichier-pathname './.répertoire/fichier.extention'";
    fichier-display-vars;


    titre4 "Repertoires multiples: ";
    eval-echo "fichier-pathname '/root/rep1/rep2.ext/fichier.extention/'";
    fichier-display-vars;



    titre3 'nom';
    titre4 'Appel nom seul';
    eval-echo "fichier-pathname 'fichierSeul'";
    fichier-display-vars;


    titre4 'Appel nom seul qui finit par un Point';
    eval-echo "fichier-pathname 'fichierSeul.'";
    fichier-display-vars;

    titre4 'Appel rep/fichier. (qui) finit par un Point';
    eval-echo "fichier-pathname '/root/fichierSeul.'";
    fichier-display-vars;


    titre4 'Fichier avec des points';
    eval-echo "fichier-pathname '/root/fichier.avec.des.points.extention'";
    fichier-display-vars;



    titre3 'Fichier caché (qui commence par un point)';
    titre4 "Appel extention (fichier caché, commence par un Point '.')";
    eval-echo "fichier-pathname '.htaccess'";
    fichier-display-vars;


    titre4 "Appel repertoire/.fichier_caché";
    eval-echo "fichier-pathname '/root/.htacess2'";
    fichier-display-vars;

    titre4 'Fichier caché avec une extention';
    eval-echo "fichier-pathname '/root/rep1/rep2.ext/.fichier.extention'";
    fichier-display-vars;

    titre4 'Fichier caché, qui finit par un point, sans extention';
    eval-echo "fichier-pathname '/root/rep1/rep2.ext/.fichier.'";
    fichier-display-vars;

    titre4 'Fichier caché, qui finit par un point, AVEC extention';
    eval-echo "fichier-pathname '/root/rep1/rep2.ext/.fichier..extention'";
    fichier-display-vars;


    titre3 'Appel complet';
    eval-echo "fichier-pathname '/root/rep1/rep2/fichier.extention'";
    fichier-display-vars;

    titre4 '';
    eval-echo "fichier-pathname '/root/rep1/rep2.ext/fichier.extention'";
    fichier-display-vars;

    titre4 'Fichier qui finit par un point';
    eval-echo "fichier-pathname '/root/rep1/rep2.ext/fichier..extention'";
    fichier-display-vars;


    titre3 'Cas particulier';
    eval-echo "fichier-pathname '/repEgalNom/repEgalNom'";
    fichier-display-vars;

    eval-echo "fichier-pathname '/repEgalNom./repEgalNom.'";
    fichier-display-vars;

    eval-echo "fichier-pathname '/.repEgalNom/.repEgalNom'";
    fichier-display-vars;


    return 0;
}


# clear;gtt.sh  apps/gtt/sid/tests/core-inode/ test-fichier-rep
addLibrairie 'test-fichier-rep';
function      test-fichier-rep(){

    titre2 'fichier-rep()';

    titreInfo "Initialisation de l'objet avec pathname"
    eval-echo "fichier-pathname '/ancien_root/_rep1/fichier.extention';";
    fichier-display-vars;


    titre2 'Appel sans argument';
    eval-echo "fichier-rep;";
    fichier-display-vars;

    titre2 'Appel argument vide';
    eval-echo "fichier-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "fichier-rep '';";
    fichier-display-vars;


    titre2 'répertoire';

    titre3 "Repertoire racine seul";
    eval-echo "fichier-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "fichier-rep '/'";
    fichier-display-vars;

    titre3 "Repertoire seul sans slash de fin";
    eval-echo "fichier-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "fichier-rep '/root'";
    fichier-display-vars;

    titre3 "Repertoire seul AVEC slash de fin: ";
    eval-echo "fichier-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "fichier-rep '/root/'";
    fichier-display-vars;

    titre3 "Repertoires multiples: ";
    eval-echo "fichier-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "fichier-rep '/root/rep1/rep2.ext/'";
    fichier-display-vars;

    return 0;
}


# clear;gtt.sh  apps/gtt/sid/tests/core-inode/ test-fichier-nom
addLibrairie 'test-fichier-nom';
function      test-fichier-nom(){

    titre2 'fichier-nom()';

    titreInfo "Initialisation de l'objet avec pathname"
    eval-echo "fichier-pathname '/ancien_root/_rep1/fichier.extention';";
    fichier-display-vars;


    titre3 'Appel sans argument';
    eval-echo "fichier-nom;";
    fichier-display-vars;

    titre3 'Appel argument vide';
    eval-echo "fichier-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "fichier-nom '';";
    fichier-display-vars;


    titre3 "nom seul";
    eval-echo "fichier-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "fichier-nom 'nom seul'";
    fichier-display-vars;

    titre3 "nom caché";
    eval-echo "fichier-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "fichier-nom '.nom caché'";
    fichier-display-vars;

    eval-echo "fichier-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "fichier-nom '.nom caché. Qui finit avec un .'";
    fichier-display-vars;

    return 0;
}


# clear;gtt.sh  apps/gtt/sid/tests/core-inode/ test-fichier-ext
addLibrairie 'test-fichier-ext';
function      test-fichier-ext(){

    titre2 'fichier-ext()';

    titreInfo "Initialisation de l'objet avec pathname"
    eval-echo "fichier-pathname '/ancien_root/_rep1/fichier.extention';";
    fichier-display-vars;


    titre3 'Appel sans argument';
    eval-echo "fichier-ext;";
    fichier-display-vars;

    titre3 'Appel argument vide';
    eval-echo "fichier-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "fichier-ext '';";
    fichier-display-vars;


    titre3 "extention seule";
    eval-echo "fichier-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "fichier-ext 'extention seul'";
    fichier-display-vars;

    titre3 "extention qui commence avec un .";
    eval-echo "fichier-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "fichier-ext '.extention'";
    fichier-display-vars;

    eval-echo "fichier-pathname '/ancien_root/_rep1/fichier.extention';";
    eval-echo "fichier-ext '.extention.'";
    fichier-display-vars;

    return 0;
}