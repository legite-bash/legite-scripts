echo "${BASH_SOURCE[0]}";

# date de création    : ?
# date de modification: 2024.08.22
# Description: 
# clear;gtt.sh  --show-libsLevel2 apps/gtt/sid/tests/core-debug/obj-display-vars.sh

# clear;gtt.sh  --libExecAuto apps/gtt/sid/tests/core-debug/obj-display-vars-.sh test-obj-display-vars
addLibrairie 'test-obj-display-vars';
function      test-obj-display-vars-(){

    titre0 "${FUNCNAME[0]}()";

    libExecAuto 'test-fct-display-vars';
    libExecAuto 'test-fct-display-var-not_null';
    libExecAuto 'test-fct-display-vars-D';
    libExecAuto 'test-fct-display-vars-d';

    return 0;
}



# clear;gtt.sh  --libExecAuto apps/gtt/sid/tests/core-debug/ test-fct-display-vars
addLibrairie 'test-fct-display-vars';
function      test-fct-display-vars(){

    titre2 "${FUNCNAME[0]}()";
    titreInfo "Les tableaux associatifs ne ressortent pas dans l'ordre de l'initialisation.";

    local _programme="display-vars";

    titre4 'sans argument':
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide':
    eval-echo "$_programme ''";
    erreur-no-description-echo;
    
    titre4 '1 argument':
    eval-echo "$_programme 'arg1_texte'";
    erreur-no-description-echo;

    titre4 '2 arguments':
    eval-echo "$_programme 'arg1_texte' \"arg1_valeur\"";
    erreur-no-description-echo;

    titre4 '3 arguments':
    eval-echo "$_programme 'arg1_texte' \"arg1_valeur\" \"arg2_texte\"";
    erreur-no-description-echo;

    titre4 '4 arguments':
    eval-echo "$_programme 'arg1_texte' \"arg1_valeur\" 'arg2_texte' \"arg2_valeur\"";
    erreur-no-description-echo;

    return 0;
}


# clear;gtt.sh  --libExecAuto apps/gtt/sid/tests/core-debug/ test-fct-display-var-not_null
addLibrairie 'test-fct-display-var-not_null';
function      test-tct-display-var-not_null(){

    titre2 "${FUNCNAME[0]}()";
    titreInfo "Affiche si non nul.";

    local    _programme="display-var-not_null";
    local -i _debug_gtt_level=0;

    titre4 'sans argument':
    eval-echo "$_programme";
    erreur-no-description-echo;


    titre4 'argument vide':
    eval-echo "$_programme ''";
    erreur-no-description-echo;

    titre4 "valeur: 0";
    eval-echo "$_programme 'valeur' 0";
    erreur-no-description-echo;

    titre4 "valeur: '0'";
    eval-echo "$_programme 'valeur' '0'";
    erreur-no-description-echo;

    titre4 "valeur: 1000";
    eval-echo "$_programme 'valeur' 1000";
    erreur-no-description-echo;

    titre4 "valeur: '1000'";
    eval-echo "$_programme 'valeur' '1000'";
    erreur-no-description-echo;

    return 0;
}


# clear;gtt.sh  --libExecAuto apps/gtt/sid/tests/core-debug/ test-fct-display-vars-D
addLibrairie 'test-fct-display-vars-D';
function      test-fct-display-vars-D(){

    titre2 "${FUNCNAME[0]}()";
    display-vars 'DEBUG_GTT_LEVEL_DEFAUT' "$DEBUG_GTT_LEVEL_DEFAUT" 'debug_gtt_level' "$debug_gtt_level";

    local    _programme="display-vars-D";
    local -i _debug_gtt_level=0;

    titre4 'sans argument':
    eval-echo "$_programme";
    erreur-no-description-echo;


    titre4 'argument vide':
    eval-echo "$_programme ''";
    erreur-no-description-echo;


    titre3 'display-vars_d(): $_debug_gtt_level='$_debug_gtt_level;

    debug-gtt-level-save 0;
    eval-echo "$_programme $_debug_gtt_level 'debug_gtt_level' \"$debug_gtt_level\" '_debug_gtt_level' \"$_debug_gtt_level\"";
    erreur-no-description-echo $?;

    debug-gtt-level-save 1;
    eval-echo "$_programme $_debug_gtt_level 'debug_gtt_level' \"$debug_gtt_level\" '_debug_gtt_level' \"$_debug_gtt_level\"";
    erreur-no-description-echo $?;


    _debug_gtt_level=1;
    titre3 'display-vars_d(): $_debug_gtt_level='$_debug_gtt_level;

    debug-gtt-level-save 0;
    eval-echo "$_programme $_debug_gtt_level 'debug_gtt_level' \"$debug_gtt_level\" '_debug_gtt_level' \"$_debug_gtt_level\"";
    erreur-no-description-echo $?;

    debug-gtt-level-save 1;
    eval-echo "$_programme  $_debug_gtt_level 'debug_gtt_level' \"$debug_gtt_level\" '_debug_gtt_level' \"$_debug_gtt_level\"";
    erreur-no-description-echo $?;

    debug-gtt-level-save 2;
    eval-echo "$_programme  $_debug_gtt_level 'debug_gtt_level' \"$debug_gtt_level\" '_debug_gtt_level' \"$_debug_gtt_level\"";
    erreur-no-description-echo $?;



    _debug_gtt_level=2;
    titre3 'display-vars_d(): $_debug_gtt_level='$_debug_gtt_level;

    debug-gtt-level-save 0;
    eval-echo "$_programme  $_debug_gtt_level 'debug_gtt_level' \"$debug_gtt_level\" '_debug_gtt_level' \"$_debug_gtt_level\"";
    erreur-no-description-echo $?;

    debug-gtt-level-save 1;
    eval-echo "$_programme  $_debug_gtt_level 'debug_gtt_level' \"$debug_gtt_level\" '_debug_gtt_level' \"$_debug_gtt_level\"";
    erreur-no-description-echo $?;

    debug-gtt-level-save 2;
    eval-echo "$_programme  $_debug_gtt_level 'debug_gtt_level' \"$debug_gtt_level\" '_debug_gtt_level' \"$_debug_gtt_level\"";
    erreur-no-description-echo $?;

    debug-gtt-level-save 3;
    eval-echo "$_programme  $_debug_gtt_level 'debug_gtt_level' \"$debug_gtt_level\" '_debug_gtt_level' \"$_debug_gtt_level\"";
    erreur-no-description-echo $?;

    # reinit
    debug-gtt-level-save 0;

    return 0;
}


# clear;gtt.sh  --libExecAuto apps/gtt/sid/tests/core-debug/ test-fct-display-vars-d
addLibrairie 'test-fct-display-vars-d';
function      test-fct-display-vars-d(){
    titre2 "${FUNCNAME[0]}()";
    display-vars 'debug_app_level' "$debug_app_level";

    local    _programme="display-vars-d";
    local -i _debug_app_level=0;

    titre4 'sans argument':
    eval-echo "$_programme";
    erreur-no-description-echo;


    titre4 'argument vide':
    eval-echo "$_programme ''";
    erreur-no-description-echo;


    titre3 'display-vars_d(): $_debug_app_level='$_debug_app_level;

    debug-app-level-save 0;
    eval-echo "$_programme $_debug_app_level 'debug_app_level' \"$debug_app_level\" '_debug_app_level' \"$_debug_app_level\"";
    erreur-no-description-echo $?;

    debug-app-level-save 1;
    eval-echo "$_programme $_debug_app_level 'debug_app_level' \"$debug_app_level\" '_debug_app_level' \"$_debug_app_level\"";
    erreur-no-description-echo $?;


    _debug_app_level=1;
    titre3 'display-vars_d(): $_debug_app_level='$_debug_app_level;

    debug-app-level-save 0;
    eval-echo "$_programme $_debug_app_level 'debug_app_level' \"$debug_app_level\" '_debug_app_level' \"$_debug_app_level\"";
    erreur-no-description-echo $?;

    debug-app-level-save 1;
    eval-echo "$_programme  $_debug_app_level 'debug_app_level' \"$debug_app_level\" '_debug_app_level' \"$_debug_app_level\"";
    erreur-no-description-echo $?;

    debug-app-level-save 2;
    eval-echo "$_programme  $_debug_app_level 'debug_app_level' \"$debug_app_level\" '_debug_app_level' \"$_debug_app_level\"";
    erreur-no-description-echo $?;



    _debug_app_level=2;
    titre3 'display-vars_d(): $_debug_app_level='$_debug_app_level;

    debug-app-level-save 0;
    eval-echo "$_programme  $_debug_app_level 'debug_app_level' \"$debug_app_level\" '_debug_app_level' \"$_debug_app_level\"";
    erreur-no-description-echo $?;

    debug-app-level-save 1;
    eval-echo "$_programme  $_debug_app_level 'debug_app_level' \"$debug_app_level\" '_debug_app_level' \"$_debug_app_level\"";
    erreur-no-description-echo $?;

    debug-app-level-save 2;
    eval-echo "$_programme  $_debug_app_level 'debug_app_level' \"$debug_app_level\" '_debug_app_level' \"$_debug_app_level\"";
    erreur-no-description-echo $?;

    debug-app-level-save 3;
    eval-echo "$_programme  $_debug_app_level 'debug_app_level' \"$debug_app_level\" '_debug_app_level' \"$_debug_app_level\"";
    erreur-no-description-echo $?;

    # reinit
    debug-gtt-level-save 0;

    return 0;
}

return 0;
