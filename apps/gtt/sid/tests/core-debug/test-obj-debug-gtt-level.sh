echo "${BASH_SOURCE[0]}";

# date de création    : 2024.08.22
# date de modification: 2024.08.22
# Description: 
# clear; gtt.sh --show-libsLevel2 apps/gtt/sid/tests/core-debug/

# clear; gtt.sh --libExecAuto apps/gtt/sid/tests/core-debug/ test-obj-debug-gtt-level
addLibrairie 'test-obj-debug-gtt-level';
function      test-obj-debug-gtt-level(){
    titre0 "${FUNCNAME[0]}()";
    titreInfo  '"';

    libExecAuto "test-fct-debug-gtt-level-save";
    libExecAuto "test-fct-debug-gtt-level-switch";
    libExecAuto "test-fct-debug-gtt-level-restaure";

    return 0;
}


# clear;gtt.sh  apps/gtt/sid/tests/core-debug/ test-fct-debug-gtt-level-save
addLibrairie 'test-fct-debug-gtt-level-save';
function      test-fct-debug-gtt-level-save(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    local _programme="debug-gtt-level-save";

    titre2 "${_programme}()";

    debug-gtt-level-display-vars;

    titre4 'sans argument';
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide';
    eval-echo "$_programme ''";
    erreur-no-description-echo;

    titre4 ':';
    eval-echo "$_programme $DEBUG_GTT_LEVEL_DEFAUT";
    erreur-no-description-echo;
    debug-gtt-level-display-vars;

    eval-echo "$_programme 5";
    erreur-no-description-echo;
    debug-gtt-level-display-vars;

    eval-echo "$_programme -1";
    erreur-no-description-echo;
    debug-gtt-level-display-vars;

    # - Nettoyage - #
    eval-echo "debug-gtt-level-init;"

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}



# clear;gtt.sh  apps/gtt/sid/tests/core-debug/ test-fct-debug-gtt-level-switch
addLibrairie 'test-fct-debug-gtt-level-switch';
function      test-fct-debug-gtt-level-switch(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    local _programme="debug-gtt-level-switch";

    titre2 "${_programme}()";

    debug-gtt-level-display-vars;

    titre4 'sans argument';
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide';
    eval-echo "$_programme ''";
    erreur-no-description-echo;

 
    titre4 'save 10';
    eval-echo "debug-gtt-level-save 10";
    erreur-no-description-echo;
    debug-gtt-level-display-vars;


    titre4 'save 9';
    eval-echo "debug-gtt-level-save 9";
    erreur-no-description-echo;
    debug-gtt-level-display-vars;


    titre4 "$_programme";
    eval-echo "$_programme ''";
    erreur-no-description-echo;
    debug-gtt-level-display-vars;

    # - Nettoyage - #
    eval-echo "debug-gtt-level-init;"

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear;gtt.sh  apps/gtt/sid/tests/core-debug/ test-fct-debug-gtt-level-restaure
addLibrairie 'test-fct-debug-gtt-level-restaure';
function      test-fct-debug-gtt-level-restaure(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    local _programme="debug-gtt-level-restaure";

    titre2 "${_programme}()";

    debug-gtt-level-display-vars;

    titre4 'sans argument';
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide';
    eval-echo "$_programme ''";
    erreur-no-description-echo;


    titre4 'save 10';
    eval-echo "debug-gtt-level-save 10";
    erreur-no-description-echo;
    debug-gtt-level-display-vars;


    titre4 'save 9';
    eval-echo "debug-gtt-level-save 9";
    erreur-no-description-echo;
    debug-gtt-level-display-vars;


    titre4 "$_programme";
    eval-echo "$_programme ''";
    erreur-no-description-echo;
    debug-gtt-level-display-vars;

    # - Nettoyage - #
    eval-echo "debug-gtt-level-init;"

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

return 0;