echo "${BASH_SOURCE[0]}";

# date de création    : 2024.08.22
# date de modification: 2024.08.22
# Description: 
# clear; gtt.sh --show-libsLevel2 apps/gtt/sid/tests/core-debug/test-debug-app-level.sh

# clear; gtt.sh --libExecAuto apps/gtt/sid/tests/core-debug/ test-obj-debug-app-level
addLibrairie 'test-obj-debug-app-level';
function      test-obj-debug-app-level(){

    titre0 "${FUNCNAME[0]}()";
    titreInfo  '"';

    libExecAuto "test-fct-debug-app-level-save";
    libExecAuto "test-fct-debug-app-level-switch";
    libExecAuto "test-fct-debug-app-level-restaure";

    return 0;
}


# clear;gtt.sh  apps/gtt/sid/tests/core-debug/ test-fct-debug-app-level-save
addLibrairie 'test-fct-debug-app-level-save';
function      test-fct-debug-app-level-save(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    local _programme="debug-app-level-save";

    titre2 "${_programme}()";

    debug-app-level-display-vars;

    titre4 'sans argument';
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide';
    eval-echo "$_programme ''";
    erreur-no-description-echo;

    titre4 ':';
    eval-echo "$_programme $DEBUG_GTT_LEVEL_DEFAUT";
    erreur-no-description-echo;
    debug-app-level-display-vars;

    eval-echo "$_programme 5";
    erreur-no-description-echo;
    debug-app-level-display-vars;

    eval-echo "$_programme -1";
    erreur-no-description-echo;
    debug-app-level-display-vars;

    # - Nettoyage - #
    eval-echo "debug-app-level-init;"

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}



# clear;gtt.sh  apps/gtt/sid/tests/core-debug/ test-fct-debug-app-level-switch
addLibrairie 'test-fct-debug-app-level-switch';
function      test-fct-debug-app-level-switch(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    local _programme="debug-app-level-switch";

    titre2 "${_programme}()";

    debug-app-level-display-vars;

    titre4 'sans argument';
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide';
    eval-echo "$_programme ''";
    erreur-no-description-echo;

 
    titre4 'save 10';
    eval-echo "debug-app-level-save 10";
    erreur-no-description-echo;
    debug-app-level-display-vars;


    titre4 'save 9';
    eval-echo "debug-app-level-save 9";
    erreur-no-description-echo;
    debug-app-level-display-vars;


    titre4 "$_programme";
    eval-echo "$_programme ''";
    erreur-no-description-echo;
    debug-app-level-display-vars;

    # - Nettoyage - #
    eval-echo "debug-app-level-init;"

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear;gtt.sh  apps/gtt/sid/tests/core-debug/ test-fct-debug-app-level-restaure
addLibrairie 'test-fct-debug-app-level-restaure';
function      test-fct-debug-app-level-restaure(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    local _programme="debug-app-level-restaure";

    titre2 "${_programme}()";

    debug-app-level-display-vars;

    titre4 'sans argument';
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide';
    eval-echo "$_programme ''";
    erreur-no-description-echo;


    titre4 'save 10';
    eval-echo "debug-app-level-save 10";
    erreur-no-description-echo;
    debug-app-level-display-vars;


    titre4 'save 9';
    eval-echo "debug-app-level-save 9";
    erreur-no-description-echo;
    debug-app-level-display-vars;


    titre4 "$_programme";
    eval-echo "$_programme ''";
    erreur-no-description-echo;
    debug-app-level-display-vars;

    # - Nettoyage - #
    eval-echo "debug-app-level-init;"

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

return 0;