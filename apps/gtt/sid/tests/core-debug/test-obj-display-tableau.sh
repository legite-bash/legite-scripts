echo "${BASH_SOURCE[0]}";

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.22
# Description: 
# clear; gtt.sh --show-libsLevel2 apps/gtt/sid/tests/core-debug/test-obj-display-tableau.sh

# clear; gtt.sh --libExecAuto apps/gtt/sid/tests/core-debug/test-obj-display-tableau.sh test-obj-display-tableau
addLibrairie 'test-obj-display-tableau';
function      test-obj-display-tableau(){

    titre0 "${FUNCNAME[0]}()";

    titre4 'Création des tableaux';
    titreInfo "Les tableaux associatifs ne ressortent pas dans l'ordre de l'initialisation."

    # declare --help
    # -p	affiche les attributs et la valeur de chaque NOM
    local -pa   _tableau_a=( "ZERO" "UN" "DEUX" "TROIS 3.1" "QUATRE" 5);
    local -pA   _tableau_A=( ['zero']="ZERO" ['un']="UN" ["deux"]="DEUX" ['trois']="TROIS 3.1" ['quatre sur quatre']="QUATRE SUR QUATRE" [5]=5 );

    libExecAuto "test-fct-display-tableau-a-format-key-data";
    libExecAuto "test-fct-display-tableau-A-format-key-data";

    libExecAuto "test-fct-display-tableau-datas";
    libExecAuto "test-fct-display-tableau-a";
    libExecAuto "test-fct-display-tableau-A";

    return 0
}    


#########################
# display-tableau-datas #
#########################

# clear; gtt.sh apps/gtt/sid/tests/core-debug/test-obj-display-tableau.sh test-fct-display-tableau-datas
addLibrairie 'test-fct-display-tableau-datas';
function      test-fct-display-tableau-datas(){

    local _programme="display-tableau-datas";
    titre2 "${_programme}()";

    titre4 'Création des tableaux';
    titreInfo "Les tableaux associatifs ne ressortent pas dans l'ordre de l'initialisation."
    local -pa   _tableau_a=( "ZERO" "UN" "DEUX" "TROIS 3.1" "QUATRE" 5);
    local -pA   _tableau_A=( ['zero']="ZERO" ['un']="UN" ["deux"]="DEUX" ['trois']="TROIS 3.1" ['quatre sur quatre']="QUATRE SUR QUATRE" [5]=5 );


    titre4 'sans argument';
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide';
    eval-echo "$_programme ''";
    erreur-no-description-echo;

    titre4 'tableau -a: indéxé';

    echo $_programme '${_tableau_a[@]}  "${_tableau_a[@]}"';
         $_programme '${_tableau_a[@]}' "${_tableau_a[@]}";
    erreur-no-description-echo $?;

    echo $_programme '${_tableau_a[*]}  "${_tableau_a[*]}"';
         $_programme '${_tableau_a[*]}' "${_tableau_a[*]}";
    erreur-no-description-echo $?;


    titre4 'tableau -A Associatif';
    echo $_programme '${_tableau_A[@]}  "${_tableau_A[@]}"';
         $_programme '${_tableau_A[@]}' "${_tableau_A[@]}";
    erreur-no-description-echo $?;

    echo    $_programme '${_tableau_A[*]}  "${_tableau_A[*]}"';
            $_programme '${_tableau_A[*]}' "${_tableau_A[*]}";
    erreur-no-description-echo $?;

    return 0;
}


#####################
# display-tableau-a #
#####################

# clear; gtt.sh apps/gtt/sid/tests/core-debug/test-obj-display-tableau.sh test-fct-display-tableau-a-format-key-data
addLibrairie 'test-fct-display-tableau-a-format-key-data';
function      test-fct-display-tableau-a-format-key-data(){

    local _programme="display-tableau-a-format-key-data";
    titre2 "${_programme}()";

    titre4 'Création des tableaux';
    titreInfo "Les tableaux associatifs ne ressortent pas dans l'ordre de l'initialisation."
    local -pa   _tableau_a=( "ZERO" "UN" "DEUX" "TROIS 3.1" "QUATRE" 5);
    local -pA   _tableau_A=( ['zero']="ZERO" ['un']="UN" ["deux"]="DEUX" ['trois']="TROIS 3.1" ['quatre sur quatre']="QUATRE SUR QUATRE" [5]=5 );

    titre4 'sans argument';
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide';
    eval-echo "$_programme ''";
    erreur-no-description-echo;

    titre4 'tableau -a: indéxé';
    $_programme '_tableau_a' "${_tableau_a[@]}";

    titre4 'tableau -A Associatif';
    $_programme '_tableau_A' "${_tableau_A[@]}";
    return 0;
}


# clear; gtt.sh apps/gtt/sid/tests/core-debug/test-obj-display-tableau.sh test-fct-display-tableau-A-format-key-data
addLibrairie 'test-fct-display-tableau-A-format-key-data';
function      test-fct-display-tableau-A-format-key-data(){

    local _programme="display-tableau-A-format-key-data";
    titre2 "${_programme}()";
    titreWarn "ATTENTION: La fonction $_programme() existe pas. C'est un code intégré qui fait le travail.";


    titre4 'Création des tableaux';
    titreInfo "Les tableaux associatifs ne ressortent pas dans l'ordre de l'initialisation."
    local -pA   _tableau_A=( ['zero']="ZERO" ['un']="UN" ["deux"]="DEUX" ['trois']="TROIS 3.1" ['quatre sur quatre']="QUATRE SUR QUATRE" [5]=5 );

    titre4 'tableau -A Associatif';
    local _out='';
    for key in "${!_tableau_A[@]}"
    do
        _out+=", '$key':'${_tableau_A[$key]}' ";
    done
    display-tableau-datas '_tableau_A[@]' "${_out#?}"; # supppresion du 1er car(',')
    return 0;
}




# clear; gtt.sh apps/gtt/sid/tests/core-debug/test-obj-display-tableau.sh test-fct-display-tableau-a
addLibrairie 'test-fct-display-tableau-a';
function      test-fct-display-tableau-a(){

    local _programme="display-tableau-a";
    titre2 "${_programme}()";

    titre4 'Création des tableaux';
    titreInfo "Les tableaux associatifs ne ressortent pas dans l'ordre de l'initialisation."
    local -pa   _tableau_a=( "ZERO" "UN" "DEUX" "TROIS 3.1" "QUATRE" 5);
    local -pA   _tableau_A=( ['zero']="ZERO" ['un']="UN" ["deux"]="DEUX" ['trois']="TROIS 3.1" ['quatre sur quatre']="QUATRE SUR QUATRE" [5]=5 );


    titre4 'sans argument';
    eval-echo "$_programme";
    erreur-no-description-echo;


    titre4 'argument vide';
    eval-echo "$_programme ''";
    erreur-no-description-echo;


    titre4 'tableau -a: indéxé';

    echo $_programme '${_tableau_a[@]}  "${_tableau_a[@]}"';
         $_programme '${_tableau_a[@]}' "${_tableau_a[@]}";
    erreur-no-description-echo $?;

    echo $_programme '${_tableau_a[*]}  "${_tableau_a[*]}"';
         $_programme '${_tableau_a[*]}' "${_tableau_a[*]}";
    erreur-no-description-echo $?;


    titre4 'tableau -A Associatif';
    echo $_programme '${_tableau_A[@]}  "${_tableau_A[@]}"';
         $_programme '${_tableau_A[@]}' "${_tableau_A[@]}";
    erreur-no-description-echo $?;

    echo    $_programme '${_tableau_A[*]}  "${_tableau_A[*]}"';
            $_programme '${_tableau_A[*]}' "${_tableau_A[*]}";
    erreur-no-description-echo $?;


    titre4 "Ajout d'un élément dans un tableau_a";
    eval-echo ' _tableau_a=(${_tableau_a[@]} "new" )';
    erreur-no-description-echo $?;
    $_programme '${_tableau_a[@]}'  "${_tableau_a[@]}";


    titre4 "Lire un élément dans un tableau_a";
    $_programme '${_tableau_a[0]}'  "${_tableau_a[0]}";


    titre4 "Remplacer un élément dans un tableau_a";
    eval-echo '_tableau_a[1]="new index1"';
    $_programme '${_tableau_a[@]}'  "${_tableau_a[@]}";

    
    titre4 "_tableau_a: Formater l'affichage index:valeur";
    local _datas='';
    for key in "${!_tableau_a[@]}"
    do
        _datas+=", '$key':'${_tableau_a[$key]}' ";
    done
    display-tableau-datas "_tableau_a[@]" "${_datas#?}"; # supppresion du 1er car(',')


    return 0;
}


#####################
# display-tableau-A #
#####################

# clear; gtt.sh apps/gtt/sid/tests/core-debug/test-obj-display-tableau.sh test-fct-display-tableau-A
addLibrairie 'test-fct-display-tableau-A';
function      test-fct-display-tableau-A(){

    local _programme="display-tableau-A";
    titre2 "${_programme}()";

    titre4 'Création des tableaux';
    titreInfo "Les tableaux associatifs ne ressortent pas dans l'ordre de l'initialisation."
    local -pa   _tableau_a=( "ZERO" "UN" "DEUX" "TROIS 3.1" "QUATRE" 5);
    local -pA   _tableau_A=( ['zero']="ZERO" ['un']="UN" ["deux"]="DEUX" ['trois']="TROIS 3.1" ['quatre sur quatre']="QUATRE SUR QUATRE" [5]=5 );


    titre4 'sans argument';
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide';
    eval-echo "$_programme ''";
    erreur-no-description-echo;


    titre4 "_tableau_A: Formater l'affichage index:valeur - initial";
    local _datas='';
    for key in "${!_tableau_A[@]}"
    do
        _datas+=", '$key':'${_tableau_A[$key]}' ";
    done
    display-tableau-datas "_tableau_A[@]" "${_datas#?}"; # supppresion du 1er car(',')


    titre4 "Remplacer un élément dans un tableau_A";
    eval-echo '_tableau_A["deux"]="new index 2"';
    $_programme '${_tableau_A[@]}'  "${_tableau_A[@]}";


    # - - #
    titre4 'tableau -a: indéxé';

    echo $_programme '${_tableau_a[@]}  "${_tableau_a[@]}"';
         $_programme '${_tableau_a[@]}' "${_tableau_a[@]}";
    erreur-no-description-echo $?;

    echo $_programme '${_tableau_a[*]}  "${_tableau_a[*]}"';
         $_programme '${_tableau_a[*]}' "${_tableau_a[*]}";
    erreur-no-description-echo $?;


    # - - #
    titre4 'tableau -A Associatif';
    echo $_programme '${_tableau_A[@]}  "${_tableau_A[@]}"';
         $_programme '${_tableau_A[@]}' "${_tableau_A[@]}";
    erreur-no-description-echo $?;

    echo    $_programme '${_tableau_A[*]}  "${_tableau_A[*]}"';
            $_programme '${_tableau_A[*]}' "${_tableau_A[*]}";
    erreur-no-description-echo $?;


    # - - #
    titre4 "Lire un élément (\"deux\") dans un tableau_A";
    titreWarn "Pas de méthode fonctionelle connue!";
    #$_programme '${_tableau_A["deux"]}'  "${_tableau_A['deux']}";


    # - - #
    titre4 "Remplacer un élément dans un tableau_A";
    eval-echo '_tableau_A["deux"]="new index 2"';
    $_programme '${_tableau_A[@]}'  "${_tableau_A[@]}";


    # - - #
    titre4 "Ajout d'un élément dans un tableau_A";
    titreWarn "Pas de méthode fonctionelle connue!";
    eval-echo ' _tableau_A=(${_tableau_A[@]} ["new index"]="NEW VALEUR" )';
    erreur-no-description-echo $?;
    $_programme '${_tableau_A[@]}'  "${_tableau_A[@]}";

    titre4 "_tableau_A: Formater l'affichage index:valeur -final";
    local _datas='';
    for key in "${!_tableau_A[@]}"
    do
        _datas+=", '$key':'${_tableau_A[$key]}' ";
    done
    display-tableau-datas '_tableau_A[@]' "${_datas#?}"; # supppresion du 1er car(',')

    return 0;
}

return 0;