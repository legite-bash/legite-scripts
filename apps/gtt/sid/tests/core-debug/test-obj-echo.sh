echo "${BASH_SOURCE[0]}";

# date de création    : ?
# date de modification: 2024.08.22
# Description: 
# clear; gtt.sh --show-libsLevel2 apps/gtt/sid/tests/core-debug

# clear; gtt.sh --libExecAuto apps/gtt/sid/tests/core-debug/ test-obj-echo
addLibrairie 'test-obj-echo';
function      test-obj-echo(){

    titre0 "${FUNCNAME[0]}()";
    titreInfo  '';

    libExecAuto 'test-fct-echo-D';
    libExecAuto 'test-fct-echo-d';
    libExecAuto 'test-fct-echo-v';

    return 0;
}


# clear; gtt.sh apps/gtt/sid/tests/core-debug/ test-fct-echo-D
addLibrairie 'test-fct-echo-D';
function      test-fct-echo-D(){

    titre2 "${FUNCNAME[0]}()";
    titreInfo "Le texte est affiché sous condition.";
    #display-vars 'DEBUG_GTT_LEVEL_DEFAUT' "$DEBUG_GTT_LEVEL_DEFAUT" 'debug_gtt_level' "$debug_gtt_level";
    debug-gtt-level-display-vars;

    local -i _debug_gtt_level=0;
    titre3 'echo-D(): $_debug_gtt_level='$_debug_gtt_level;

        debug-gtt-level-save 0;
        display-vars 'debug_gtt_level' "$debug_gtt_level" '_debug_gtt_level' "$_debug_gtt_level";
        echo-D "Cette phrase apparait quand debug_gtt_level >= _debug_gtt_level=$_debug_gtt_level" $_debug_gtt_level;

        debug-gtt-level-save 1;
        display-vars 'debug_gtt_level' "$debug_gtt_level" '_debug_gtt_level' "$_debug_gtt_level";
        echo-D "Cette phrase apparait quand debug_gtt_level >= _debug_gtt_level=$_debug_gtt_level" $_debug_gtt_level;

    
    _debug_gtt_level=1;
    titre3 'echo-D(): $_debug_gtt_level='$_debug_gtt_level;

        debug-gtt-level-save 0;
        display-vars 'debug_gtt_level' "$debug_gtt_level" '_debug_gtt_level' "$_debug_gtt_level";
        echo-D "Cette phrase apparait quand debug_gtt_level >= _debug_gtt_level=$_debug_gtt_level" $_debug_gtt_level;

        debug-gtt-level-save 1;
        display-vars 'debug_gtt_level' "$debug_gtt_level" '_debug_gtt_level' "$_debug_gtt_level";
        echo-D "Cette phrase apparait quand debug_gtt_level >= _debug_gtt_level=$_debug_gtt_level" $_debug_gtt_level;

        debug-gtt-level-save 2;
        display-vars 'debug_gtt_level' "$debug_gtt_level" '_debug_gtt_level' "$_debug_gtt_level";
        echo-D "Cette phrase apparait quand debug_gtt_level >= _debug_gtt_level=$_debug_gtt_level" $_debug_gtt_level;


    _debug_gtt_level=2;
    titre3 'echo-D(): $_debug_gtt_level='$_debug_gtt_level;
    
        debug-gtt-level-save 0;
        display-vars 'debug_gtt_level' "$debug_gtt_level" '_debug_gtt_level' "$_debug_gtt_level";
        echo-D "Cette phrase apparait quand debug_gtt_level >= _debug_gtt_level=$_debug_gtt_level" $_debug_gtt_level;

        debug-gtt-level-save 1;
        display-vars 'debug_gtt_level' "$debug_gtt_level" '_debug_gtt_level' "$_debug_gtt_level";
        echo-D "Cette phrase apparait quand debug_gtt_level >= _debug_gtt_level=$_debug_gtt_level" $_debug_gtt_level;

        debug-gtt-level-save 2;
        display-vars 'debug_gtt_level' "$debug_gtt_level" '_debug_gtt_level' "$_debug_gtt_level";
        echo-D "Cette phrase apparait quand debug_gtt_level >= _debug_gtt_level=$_debug_gtt_level" $_debug_gtt_level;

        debug-gtt-level-save 3;
        display-vars 'debug_gtt_level' "$debug_gtt_level" '_debug_gtt_level' "$_debug_gtt_level";
        echo-D "Cette phrase apparait quand debug_gtt_level >= _debug_gtt_level=$_debug_gtt_level" $_debug_gtt_level;

    # reinit
    debug-gtt-level-save 0;

    return 0;
}


# clear; gtt.sh apps/gtt/sid/tests/core-debug/ test-fct-echo-d
addLibrairie 'test-fct-echo-d';
function      test-fct-echo-d(){

    titre2 "${FUNCNAME[0]}()";
    titreInfo "Le texte est affiché sous condition.";
    debug-app-level-display-vars;

    local -i _debug_app_level=0;
    titre3 'echo-d(): $_debug_app_level='$_debug_app_level;

        debug-app-level-save 0;
        display-vars 'debug_app_level' "$debug_app_level" '_debug_app_level' "$_debug_app_level";
        echo-d "Cette phrase apparait quand debug_app_level >= _debug_app_level=$_debug_app_level" $_debug_app_level;

        debug-app-level-save 1;
        display-vars 'debug_app_level' "$debug_app_level" '_debug_app_level' "$_debug_app_level";
        echo-d "Cette phrase apparait quand debug_app_level >= _debug_app_level=$_debug_app_level" $_debug_app_level;

    
    _debug_app_level=1;
    titre3 'echo-d(): $_debug_app_level='$_debug_app_level;

        debug-app-level-save 0;
        display-vars 'debug_app_level' "$debug_app_level" '_debug_app_level' "$_debug_app_level";
        echo-d "Cette phrase apparait quand debug_app_level >= _debug_app_level=$_debug_app_level" $_debug_app_level;

        debug-app-level-save 1;
        display-vars 'debug_app_level' "$debug_app_level" '_debug_app_level' "$_debug_app_level";
        echo-d "Cette phrase apparait quand debug_app_level >= _debug_app_level=$_debug_app_level" $_debug_app_level;

        debug-app-level-save 2;
        display-vars 'debug_app_level' "$debug_app_level" '_debug_app_level' "$_debug_app_level";
        echo-d "Cette phrase apparait quand debug_app_level >= _debug_app_level=$_debug_app_level" $_debug_app_level;


    _debug_app_level=2;
    titre3 'echo-d(): $_debug_app_level='$_debug_app_level;
    
        debug-app-level-save 0;
        display-vars 'debug_app_level' "$debug_app_level" '_debug_app_level' "$_debug_app_level";
        echo-d "Cette phrase apparait quand debug_app_level >= _debug_app_level=$_debug_app_level" $_debug_app_level;

        debug-app-level-save 1;
        display-vars 'debug_app_level' "$debug_app_level" '_debug_app_level' "$_debug_app_level";
        echo-d "Cette phrase apparait quand debug_app_level >= _debug_app_level=$_debug_app_level" $_debug_app_level;

        debug-app-level-save 2;
        display-vars 'debug_app_level' "$debug_app_level" '_debug_app_level' "$_debug_app_level";
        echo-d "Cette phrase apparait quand debug_app_level >= _debug_app_level=$_debug_app_level" $_debug_app_level;

        debug-app-level-save 3;
        display-vars 'debug_app_level' "$debug_app_level" '_debug_app_level' "$_debug_app_level";
        echo-d "Cette phrase apparait quand debug_app_level >= _debug_app_level=$_debug_app_level" $_debug_app_level;

    # reinit
    debug-app-level-save 0;

    return 0;
}


# clear; gtt.sh apps/gtt/sid/tests/core-debug/ test-fct-echo-v
addLibrairie 'test-fct-echo-v';
function      test-fct-echo-v(){

    titre2 "${FUNCNAME[0]}()";

    titreInfo "Le texte est affiché sous condition.";
    debug-verbose-level-display-vars;

    local -i _debug_verbose_level=0;
    titre3 'echo-v(): $_debug_verbose_level='$_debug_verbose_level;

        debug-verbose-level-save 0;
        display-vars 'debug_verbose_level' "$debug_verbose_level" '_debug_verbose_level' "$_debug_verbose_level";
        echo-v "Cette phrase apparait quand debug_verbose_level >= _debug_verbose_level=$_debug_verbose_level" $_debug_verbose_level;

        debug-verbose-level-save 1;
        display-vars 'debug_verbose_level' "$debug_verbose_level" '_debug_verbose_level' "$_debug_verbose_level";
        echo-v "Cette phrase apparait quand debug_verbose_level >= _debug_verbose_level=$_debug_verbose_level" $_debug_verbose_level;

    
    _debug_verbose_level=1;
    titre3 'echo-v(): $_debug_verbose_level='$_debug_verbose_level;

        debug-verbose-level-save 0;
        display-vars 'debug_verbose_level' "$debug_verbose_level" '_debug_verbose_level' "$_debug_verbose_level";
        echo-v "Cette phrase apparait quand debug_verbose_level >= _debug_verbose_level=$_debug_verbose_level" $_debug_verbose_level;

        debug-verbose-level-save 1;
        display-vars 'debug_verbose_level' "$debug_verbose_level" '_debug_verbose_level' "$_debug_verbose_level";
        echo-v "Cette phrase apparait quand debug_verbose_level >= _debug_verbose_level=$_debug_verbose_level" $_debug_verbose_level;

        debug-verbose-level-save 2;
        display-vars 'debug_verbose_level' "$debug_verbose_level" '_debug_verbose_level' "$_debug_verbose_level";
        echo-v "Cette phrase apparait quand debug_verbose_level >= _debug_verbose_level=$_debug_verbose_level" $_debug_verbose_level;


    _debug_verbose_level=2;
    titre3 'echo-v(): $_debug_verbose_level='$_debug_verbose_level;
    
        debug-verbose-level-save 0;
        display-vars 'debug_verbose_level' "$debug_verbose_level" '_debug_verbose_level' "$_debug_verbose_level";
        echo-v "Cette phrase apparait quand debug_verbose_level >= _debug_verbose_level=$_debug_verbose_level" $_debug_verbose_level;

        debug-verbose-level-save 1;
        display-vars 'debug_verbose_level' "$debug_verbose_level" '_debug_verbose_level' "$_debug_verbose_level";
        echo-v "Cette phrase apparait quand debug_verbose_level >= _debug_verbose_level=$_debug_verbose_level" $_debug_verbose_level;

        debug-verbose-level-save 2;
        display-vars 'debug_verbose_level' "$debug_verbose_level" '_debug_verbose_level' "$_debug_verbose_level";
        echo-v "Cette phrase apparait quand debug_verbose_level >= _debug_verbose_level=$_debug_verbose_level" $_debug_verbose_level;

        debug-verbose-level-save 3;
        display-vars 'debug_verbose_level' "$debug_verbose_level" '_debug_verbose_level' "$_debug_verbose_level";
        echo-v "Cette phrase apparait quand debug_verbose_level >= _debug_verbose_level=$_debug_verbose_level" $_debug_verbose_level;

    # reinit
    debug-verbose-level-save 0;

    return 0;
}

return 0;