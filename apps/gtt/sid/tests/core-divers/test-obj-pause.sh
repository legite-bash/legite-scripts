echo "${BASH_SOURCE[0]}";

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.22
# Description: 
# code: 
# clear;gtt.sh --show-libsLevel2  apps/gtt/sid/tests/core-divers/


# clear;gtt.sh  --libExecAuto apps/gtt/sid/tests/core-divers/ test-obj-pause
addLibrairie 'test-obj-pause';
function      test-obj-pause(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre0 "${FUNCNAME[0]}()";
    local _programme="pause";

    libExecAuto "test-fct-pause";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear;gtt.sh --pause apps/gtt/sid/tests/core-divers/ test-fct-pause
addLibrairie 'test-fct-pause';
function      test-fct-pause(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";
    titreInfo "";

    local _programme="pause";

    display-vars 'isPause' "$isPause";

    titre4 'pause-hit: press any key ':
    eval-echo "${_programme}-hit";


    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


return 0;
