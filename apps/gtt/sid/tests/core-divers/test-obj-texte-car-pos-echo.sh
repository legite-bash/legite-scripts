echo "${BASH_SOURCE[0]}";

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.22
# Description: 
# code: 
# clear;gtt.sh --show-libsLevel2  apps/gtt/sid/tests/core-divers/


# clear;gtt.sh  --libExecAuto apps/gtt/sid/tests/core-divers/ test-obj-texte-car-pos
addLibrairie 'test-obj-texte-car-pos';
function      test-obj-texte-car-pos(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre0 "${FUNCNAME[0]}()";
    local _programme="texte-car-pos-echo";

    libExecAuto "test-fct-texte-car-pos--vide";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear;gtt.sh apps/gtt/sid/tests/core-divers/ test-fct-texte-car-pos--vide
addLibrairie 'test-fct-texte-car-pos--vide';
function      test-fct-texte-car-pos--vide(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";
    titreInfo "";

    local _programme="texte-car-pos";

    titre4 'sans argument':
    eval-echo "${_programme}";
    
    eval-echo "$_programme";
    erreur-no-description-echo;
    display-vars 'texte_car_pos' "$texte_car_pos";

    titre4 'argument vide':
    eval-echo "$_programme ''";
    erreur-no-description-echo;
    display-vars 'texte_car_pos' "$texte_car_pos";
    
    titre4 'valeur: 0':

    eval-echo "$_programme 0";
    erreur-no-description-echo;
    display-vars 'texte_car_pos' "$texte_car_pos";

    titre4 'valeur: -1':
    eval-echo "$_programme -1";
    erreur-no-description-echo;
    display-vars 'texte_car_pos' "$texte_car_pos";

    titre4 'valeur: --1':
    eval-echo "$_programme --1";
    erreur-no-description-echo;
    display-vars 'texte_car_pos' "$texte_car_pos";

    titre4 'valeur: $E_FALSE':
    eval-echo "$_programme $E_FALSE";
    erreur-no-description-echo;
    display-vars 'texte_car_pos' "$texte_car_pos";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear;gtt.sh apps/gtt/sid/tests/core-divers/ test-fct-texte-car-pos--normal
addLibrairie 'test-fct-texte-car-pos--normal';
function      test-fct-texte-car-pos--normal(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";
    titreInfo "";

    local _programme="texte-car-pos";

    titre4 'Texte normal' 'T':
    eval-echo "${_programme} 'Texte normal' 'T'";
    erreur-no-description-echo;
    display-vars 'texte_car_pos' "$texte_car_pos";

    titre4 'Texte normal' '?':
    eval-echo "${_programme} 'Texte normal' '?'";
    erreur-no-description-echo;
    display-vars 'texte_car_pos' "$texte_car_pos";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


return 0;
