echo "${BASH_SOURCE[0]}";

# gtt.sh 
# date de création    : 2024.10.01
# date de modification: 2024.10.01
# Description: 
# code: 
# clear;gtt.sh --show-libsLevel2  apps/gtt/sid/tests/core-divers/


# clear;gtt.sh  --libExecAuto apps/gtt/sid/tests/core-divers/ test-obj-texte-justify-left
addLibrairie 'test-obj-texte-justify-left';
function      test-obj-texte-justify-left(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre0 "${FUNCNAME[0]}()";
    local _programme="texte-justify-left-echo";

    libExecAuto "test-fct-texte-justify-left--vide";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear;gtt.sh apps/gtt/sid/tests/core-divers/ test-fct-texte-justify-left--vide
addLibrairie 'test-fct-texte-justify-left--vide';
function      test-fct-texte-justify-left--vide(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";
    titreInfo "";

    local _programme="texte-justify-left";

    titre4 'sans argument':
    eval-echo "${_programme}";
    
    eval-echo "$_programme";
    erreur-no-description-echo;
    echo "$texte_justify_left|";

    titre4 'argument vide':
    eval-echo "$_programme ''";
    erreur-no-description-echo;
    echo "$texte_justify_left|";
    



    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear;gtt.sh apps/gtt/sid/tests/core-divers/ test-fct-texte-justify-left--normal
addLibrairie 'test-fct-texte-justify-left--normal';
function      test-fct-texte-justify-left--normal(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";
    titreInfo "";

    local _programme="texte-justify-left";
    local _original='';
    
    _original='';
    titre4 "$_original";
    eval-echo "${_programme} '$_original' '¤'";
    erreur-no-description-echo;
    echo "$texte_justify_left";

    _original='Texte qui depasse pas la largeur du terminal';
    titre4 "$_original";
    eval-echo "${_programme} '$_original' '¤'";
    erreur-no-description-echo;
    echo "$texte_justify_left";


    car-duplique-n $TERMINAL_LARGEUR 'A';
    _original="$car_duplique_n";
    titre4 "largeur du texte = largeur du terminal";
    eval-echo "${_programme} '$_original' '¤'";
    erreur-no-description-echo;
    echo "$texte_justify_left";


    _original='GRAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAND TREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEES GRANDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD Texte qui depasse pas la largeur du terminal';
    titre4 "$_original";
    eval-echo "${_programme} '$_original' '¤'";
    erreur-no-description-echo;
    echo "$texte_justify_left";

    titre4 "Cumul";
    local _cumul='';

    eval-echo "${_programme} 'ligne 1' '¤'";
    _cumul+="$texte_justify_left";

    eval-echo "${_programme} 'ligne 2' 'X'";
    _cumul+="$texte_justify_left";

    echo "$_cumul";


    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


return 0;
