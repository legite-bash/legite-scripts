echo "${BASH_SOURCE[0]}";

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.22
# Description: 
# code:  apps/gtt/sid/core/gtt-core-erreur.sh

local _source_rep="${BASH_SOURCE%/*.*}";        # display-vars '_source_rep' "$_source_rep";
local _source_rep_last="${_source_rep##*/}";    # display-vars '_source_rep_last' "$_source_rep_last";
local _source_name="${BASH_SOURCE##*/}";        # display-vars '_source_name' "$_source_name";
local _source_nom="${_source_name%.*}";         # display-vars '_source_nom' "$_source_nom";
local _source_ext="${_source_name##*.}";        # display-vars '_source_ext' "$_source_ext";

local _test_rep_rel="${_source_rep##$GTT_VERSION_SELECT_REP}";  # display-vars '_test_rep_rel' "$_test_rep_rel";  # /tests/./

# clear;gtt.sh --show-libsLevel1 apps/gtt/sid/tests/core-texte-normalize/

# clear;gtt.sh --libExecAuto -dd apps/gtt/sid/tests/core-texte-normalize/ test-obj-texte-normalize
addLibrairie 'test-obj-texte-normalize';
function      test-obj-texte-normalize(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre0 "${FUNCNAME[0]}()";

    titre4 'initialisation':
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-init";

    libExecAuto 'test-fct-texte-normalize--vide';
    libExecAuto 'test-fct-texte-normalize';
    libExecAuto 'test-fct-texte-normalize--accent';
    libExecAuto 'test-fct-texte-normalize--caractere_speciaux';
    libExecAuto 'test-fct-texte-normalize--extention';
    libExecAuto 'test-fct-texte-normalize--limite';

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear;gtt.sh -dd apps/gtt/sid/tests/core-texte-normalize/ test-obj-texte-normalize-init
addLibrairie 'test-obj-texte-normalize-init';
function      test-obj-texte-normalize-init(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre0 "${FUNCNAME[0]}()";

    local _programme="texte-normalize";

    titre4 'initialisation':
    eval-echo "${_programme}-init";
    ${_programme}-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear;gtt.sh  -dd apps/gtt/sid/tests/core-texte-normalize/ test-fct-texte-normalize--vide
addLibrairie 'test-fct-texte-normalize--vide';
function      test-fct-texte-normalize--vide(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";
    titreInfo "";

    local _programme="texte-normalize";


    titre4 'sans argument':
    eval-echo "${_programme}-init";
    eval-echo "$_programme";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    titre4 'argument vide':
    eval-echo "${_programme}-init";
    eval-echo "$_programme ''";
    erreur-no-description-echo;
    ${_programme}-display-vars;
    
    titre4 'valeur: 0':
    eval-echo "${_programme}-init";
    eval-echo "$_programme 0";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    titre4 'valeur: -1':
    eval-echo "${_programme}-init";
    eval-echo "$_programme -1";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    titre4 'valeur: --1':
    eval-echo "${_programme}-init";
    eval-echo "$_programme --1";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    titre4 'valeur: $E_FALSE':
    eval-echo "${_programme}-init";
    eval-echo "$_programme $E_FALSE";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh -dd apps/gtt/sid/tests/core-texte-normalize/ test-fct-texte-normalize
addLibrairie 'test-fct-texte-normalize'
function      test-fct-texte-normalize(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;
    titre2 "${FUNCNAME[0]}() - sans callback";

    local _programme="texte-normalize";
    eval-echo "${_programme}-init";

    local _texte="";
    titre3 'Exemple simple';
    _texte="Exemple simple avec quelques caratéres spéciaux!?";
    texte-normalize-echo "$_texte"; 
    ${_programme}-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh -dd apps/gtt/sid/tests/core-texte-normalize/ test-fct-texte-normalize-echo
# pas de retour à la ligne dans la réponse
addLibrairie 'test-fct-texte-normalize-echo';
function      test-fct-texte-normalize-echo(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;
    titre2 "${FUNCNAME[0]}() - sans callback";

    local _programme="texte-normalize";
    eval-echo "${_programme}-init";

    local _texte="";
    titre3 'Test de la fonction echo();';
    _texte="Test de la fonction echo();";
    texte-normalize-echo "$_texte";
    echo '';
    ${_programme}-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh -dd apps/gtt/sid/tests/core-texte-normalize/ test-fct-texte-normalize--accent
addLibrairie 'test-fct-texte-normalize--accent'
function      test-fct-texte-normalize--accent(){

    titre2 "${FUNCNAME[0]}()";

    local _programme="texte-normalize";
    eval-echo "${_programme}-init";

    local _texte="";
    titre3 'Accents';
    _texte="É é ë è - À Ä ä à Ç ç -> É é ë è - À Ä ä à Ç ç";        display-vars "$_texte" "$(texte-normalize-echo "$_texte")"; 
    _texte="a   à â ä - a   à â ä";                                 display-vars "$_texte" "$(texte-normalize-echo "$_texte")"; 
    _texte="A   À Â Ä - A   À Â Ä";                                 display-vars "$_texte" "$(texte-normalize-echo "$_texte")"; 
    _texte="e é è ê ë - e é è ê ë";                                 display-vars "$_texte" "$(texte-normalize-echo "$_texte")"; 
    _texte="E É È Ê Ë - E É È Ê Ë";                                 display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte="i     î ï - i     î ï";                                 display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte="I     Î Ï - I     Î Ï";                                 display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte="o     ô ö - o     ô ö";                                 display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte="O     Ô Ö - O     Ô Ö";                                 display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte="u   ù û ü - u   ù û ü";                                 display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte="U   Ù Û Û - U   Ù Û Û";                                 display-vars "$_texte" "$(texte-normalize-echo "$_texte")";

    #${_programme}-display-vars;

    return 0;
}


# clear; gtt.sh -dd apps/gtt/sid/tests/core-texte-normalize/ test-fct-texte-normalize--caractere_speciaux
addLibrairie 'test-fct-texte-normalize--caractere_speciaux'
function      test-fct-texte-normalize--caractere_speciaux(){

    titre2 "${FUNCNAME[0]}()";

    local _programme="texte-normalize";
    eval-echo "${_programme}-init";


    titre3 'Phrase';
    _texte="Une phrase normale."
                                                display-vars "$_texte" "$(texte-normalize-echo "$_texte")"; 
    _texte="Deux phrases normales. Font un paragraphe.Fin"
                                                display-vars "$_texte" "$(texte-normalize-echo "$_texte")"; 

    titre3 'Test des caracteres speciaux';
    _texte="(test) - (test)";                   display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte="@= - @=";                           display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte="~#{[|\`\\^@]}-~#{[|\`\\^@]}";       display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte=" ~ - ~";                            display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte=" { - {";                            display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte=" } - }";                            display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte=" # - #";                            display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte=" [ - [";                            display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte=" ] - ]";                            display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte=" | - |";                            display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte=" \\ - \\}";                         display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte=" / - /";                            display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte=" ^ - ^ ";                           display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte=" @ - @ ";                           display-vars "$_texte" "$(texte-normalize-echo "$_texte")";

    return 0;
}


# clear; gtt.sh -dd apps/gtt/sid/tests/core-texte-normalize/ test-fct-texte-normalize--extention
addLibrairie 'test-fct-texte-normalize--extention';
function      test-fct-texte-normalize--extention(){

    titre2 "${FUNCNAME[0]}()";

    local _programme="texte-normalize";
    eval-echo "${_programme}-init";

    _texte="---- a ___ _ b --- _- -- c __-__ d _-_ - ---- a ___ _ b --- _- -- c __-__ d _-_";
                                                display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte="---- œ ____- ---- œ ____";          display-vars "$_texte" "$(texte-normalize-echo "$_texte")";

    _texte="test du et & ca _&_";               display-vars "$_texte" "$(texte-normalize-echo "$_texte")";

    titre3 "Test d'extention";
    _texte=".mp4";                              display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte="a.b.c--.mp4";                       display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte="a.b.c__.mp4";                       display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte="a.b.c__-.mp4";                      display-vars "$_texte" "$(texte-normalize-echo "$_texte")";

    titre3 "Test des tags";
    _texte="on #tag chez @ ";                   display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte="on #tag chez @moi";                 display-vars "$_texte" "$(texte-normalize-echo "$_texte")";

    titre3 "Test des fins";
    _texte="Un undescore à la fin_";            display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte="Un espace à la fin ";               display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte="Un tiret à la fin-";                display-vars "$_texte" "$(texte-normalize-echo "$_texte")";

    titre3 "Test des extensions";
    _texte="nom.AVI";                           display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte="nom..ext en sion";                  display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte="nom..ext-en sion";                  display-vars "$_texte" "$(texte-normalize-echo "$_texte")";

    return 0;
}


# clear; gtt.sh -dd apps/gtt/sid/tests/core-texte-normalize/ test-fct-texte-normalize--limite
addLibrairie 'test-fct-texte-normalize--limite';
function      test-fct-texte-normalize--limite(){

    titre2 "${FUNCNAME[0]}()";
    titreInfo "";

    local _programme="texte-normalize";
    eval-echo "${_programme}-init";
    local _texte="";
    _texte='__ undescore __';           display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte='  espace  ';                display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte='-- tiret --';               display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte='++ plus --';                display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte='.. point ..';               display-vars "$_texte" "$(texte-normalize-echo "$_texte")";
    _texte='?? intero ??';              display-vars "$_texte" "$(texte-normalize-echo "$_texte")";

    return 0;
}


# clear; gtt.sh -dd apps/gtt/sid/tests/core-texte-normalize/ test-fct-texte-normalize--callback
addLibrairie 'test-fct-texte-normalize--callback'
function      test-fct-texte-normalize--callback(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;
    titre2 "${FUNCNAME[0]}()";

    local _programme="texte-normalize";
    eval-echo "${_programme}-init";
    ${_programme}-display-vars;


    titre3 'callback: normalize-callback-mediatheques-before';
    eval-echo "${_programme}-callback-before 'normalize-callback-mediatheques-before'";

    local _texte="";
    _texte="Ce pack de 1CD est disponible sur le store!"; 
    texte-normalize "$_texte"; 

    ${_programme}-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

return 0;

