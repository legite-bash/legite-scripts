echo "${BASH_SOURCE[0]}";

# date de création    : ?
# date de modification: 2024.08.22
# code:  apps/gtt/sid/core/gtt-core-erreur.sh
# Description: Ge*ère le numéro d'erreur courant
# clear; gtt.sh --show-libsLevel2 apps/gtt/sid/tests/core-erreur/

# clear; gtt.sh  --libExecAuto apps/gtt/sid/tests/core-erreur/ test-obj-erreur
addLibrairie 'test-obj-erreur';
function      test-obj-erreur(){

    titre0 "${FUNCNAME[0]}()";

    libExecAuto 'test-fct-erreur-set';
    libExecAuto 'test-fct-erreur-no-display-var';
    libExecAuto 'test-fct-erreur-no-description-echo';

    return 0;
}


# clear;gtt.sh  apps/gtt/sid/tests/core-erreur/ test-fct-erreur-set
addLibrairie 'test-fct-erreur-set';
function      test-fct-erreur-set(){

    local _programme="erreur-set";
    titre2 "${_programme}()";
    titreInfo "";

    titre4 'sans argument';
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide';
    eval-echo "$_programme ''";
    erreur-no-description-echo;


    titre4 'valeur: 0';
    eval-echo "$_programme 0";
    erreur-no-description-echo;

    titre4 'valeur: -1';
    eval-echo "$_programme -1";
    erreur-no-description-echo;

    titre4 'valeur: $E_FALSE';
    eval-echo "$_programme $E_FALSE";
    erreur-no-description-echo;

    return 0;
}


# clear;gtt.sh  apps/gtt/sid/tests/core-erreur/ test-fct-erreur-no-display-var
addLibrairie 'test-fct-erreur-no-display-var';
function      test-fct-erreur-no-display-var(){

    local _programme="erreur-no-display-var";
    titre2 "${_programme}()";
    titreInfo "";

    titre4 'sans argument';
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide';
    eval-echo "$_programme ''";
    erreur-no-description-echo;

    
    titre4 'valeur: 0':
    eval-echo "$_programme 0";
    erreur-no-description-echo;

    titre4 'valeur: -1':
    eval-echo "$_programme -1";
    erreur-no-description-echo;

    titre4 'valeur: $E_FALSE':
    eval-echo "$_programme $E_FALSE";
    erreur-no-description-echo;

    return 0;
}



# clear;gtt.sh  apps/gtt/sid/tests/core-erreur/ test-fct-erreur-no-description-echo
addLibrairie 'test-fct-erreur-no-description-echo';
function      test-fct-erreur-no-description-echo(){

    local _programme="erreur-no-description-echo";
    titre2 "${_programme}()";
    titreInfo "";

    titre4 'sans argument';
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide';
    eval-echo "$_programme ''";
    erreur-no-description-echo;
    
    titre4 'valeur: 0':
    eval-echo "$_programme 0";
    erreur-no-description-echo;

    titre4 'valeur: -1':
    eval-echo "$_programme -1";
    erreur-no-description-echo;

    titre4 'valeur: $E_FALSE':
    eval-echo "$_programme $E_FALSE";
    erreur-no-description-echo;

    return 0;
}


return 0;

