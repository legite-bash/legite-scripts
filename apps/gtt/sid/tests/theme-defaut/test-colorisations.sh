echo "${BASH_SOURCE[0]}";

#addLibrairie 'color';
#function      color(){
#    fctIn "$*";
#    libExecAuto 'test-colorisations';
#    fctOut "${FUNCNAME[0]}(0)"; return 0;
#}

# clear;gtt.sh  apps/gtt/sid/tests test-colorisations
addLibrairie 'test-colorisations';
function      test-colorisations(){
    fctIn;

    titre1 'titre1';    TITRE1 'titre1';
    titre2 'titre2';    TITRE2 'titre2';
    titre3 'titre3';    TITRE3 'titre3';
    titre4 'titre4';    TITRE4 'titre4';

    titreLib    'titreLib';
    titreUsage  'titreUsage';
    titreCmd    'titreCmd';
    titreInfo   'titreInfo';
    titreWarn   'titreWarn';

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}
