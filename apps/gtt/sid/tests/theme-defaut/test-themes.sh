echo "${BASH_SOURCE[0]}";

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.12
# Description: 


#clear; gtt.sh apps/gtt/sid/tests/theme-defaut/test-themes.sh test-themes
addLibrairie 'test-themes';
function      test-themes(){

    test-themes-texte;
    #test-themes-titres;
}


#clear; gtt.sh apps/gtt/sid/tests/theme-defaut/test-themes.sh test-themes-texte-centre-echo
addLibrairie 'test-themes-texte-centre-echo';
function      test-themes-texte-centre-echo(){
    titre1 "titre1()";

    eval-echo "texte-centre-echo;";
    eval-echo "texte-centre-echo 'Mon texte: largeur non défini=\$terminal_largeur';";
    eval-echo "texte-centre-echo 'larg_txt plus petit que largeur' 50;";
    eval-echo "texte-centre-echo 'larg_txt plus grand que largeur' 10;";
    eval-echo "texte-centre-echo 'larg_txt et largeur=0' 0;";

    eval-echo "texte-centre 'Test de texte-centre()'";
    display-vars 'texte_centre' "";
    echo "$texte_centre";

    echo '--';
}


#clear; gtt.sh apps/gtt/sid/tests/theme-defaut/test-themes.sh test-themes-titre
addLibrairie  'test-themes-titre';
function      test-themes-titre(){

    # - titre* - #
    TITRE0 "Titre en capitale";
    titre0 "titre0()";


    TITRE1 "TITRE1()";
    titre1 "titre1()";
    TITRE1 "Un tres tres tres tres tres tres tres tres tres tres tres tres tres tres long TITRE2";
    titre1 "Un tres tres tres tres tres tres tres tres tres tres tres tres tres tres long titre1";
    TITRE1_D "TITRE1_D()"
    titre1_D "titre1_D()"
    #TITRE1_d "TITRE1_d()"
    titre1_d "titre1_d()"

    TITRE2 "TITRE2()";
    titre2 "titre2()";
    TITRE2 "Un tres tres tres tres tres tres tres tres tres tres tres tres tres tres long TITRE2";
    titre2 "Un tres tres tres tres tres tres tres tres tres tres tres tres tres tres long titre2";
    #TITRE2_D "TITRE2_D()"
    titre2_D "titre2_D()"
    #TITRE2_d "TITRE2_d()"
    titre2_d "titre2_d()"


    TITRE3 "TITRE3()";
    titre3 "titre3()";
    TITRE3 "Un tres tres tres tres tres tres tres tres tres tres tres tres tres tres long TITRE3";
    titre3 "Un tres tres tres tres tres tres tres tres tres tres tres tres tres tres long titre3";
    #TITRE3_D "TITRE3_D()"
    titre3_D "titre3_D()"
    #TITRE3_d "TITRE3_d()"
    titre3_d "titre3_d()"


    TITRE4 "TITRE4()";
    titre4 "titre4()";
    TITRE4 "Un tres tres tres tres tres tres tres tres tres tres tres tres tres tres long TITRE4";
    titre4 "Un tres tres tres tres tres tres tres tres tres tres tres tres tres tres long titre4";
    #TITRE4_D "TITRE4_D()"
    titre4_D "titre4_D()"
    #TITRE4_d "TITRE4_d()"
    titre4_d "titre4_d()"






    # - titreUsage() - #
    titreUsage "titrtitreUsagee_usage()";

    # - titreCmd() - #
    titreCmd "titreCmd()";

    # - titreInfo() - #
    titreInfo "titreInfo()";

    # - TITREINFO() - #
    TITREINFO "TITREINFO()";

    # - titreWarn() - #
    titreWarn "titreWarn()";

    # - titreLib() - #
    titreLib "titreLib()";

}


