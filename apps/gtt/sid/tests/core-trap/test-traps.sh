echo "${BASH_SOURCE[0]}";

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.20
# Description: 


addLibrairie 'test-traps': # pas d'execution -All
function      test-traps(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    #fctOut "${FUNCNAME[0]}";return 1;
    #fctOut "${FUNCNAME[0]}";return $E_INTERNE;
    #fctOut "${FUNCNAME[0]}";return $E_ARG_REQUIRE;
    fctOut "${FUNCNAME[0]}";return $E_INODE_NOT_EXIST;
    #fctOut "${FUNCNAME[0]}";return $E_NORMAL;
    #fctOut "${FUNCNAME[0]}";return $E_ARG_NONE;
    #fctOut "${FUNCNAME[0]}";return $E_ARG_BAD;
    test_trapC;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}

addLibrairie 'test-trapC'
function      test-trapC(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    while true
    do
        if $isTrapCAsk;then break;fi
    done

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}



################
## TEST TRAPS ##
################
    test-traps(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

        
    #echo 'test avec un return 1';   return 1;
    fail 1;display-vars '$?' "$?"        
    fail 2;display-vars '$?' "$?"
    fail 3;display-vars '$?' "$?"

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
    }

return 0;