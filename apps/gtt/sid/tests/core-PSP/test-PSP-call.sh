echo "${BASH_SOURCE[0]}";

function localToCall(){
    fctIn "$*";

    echo '*:' "$*"
    echo '@:' "$@"


    fctOut "${FUNCNAME[0]}(0)"; return 0;

}

addLibrairie 'fct-fctIn';
function      fct-fctIn(){
    fctIn "$*";

    localToCall 'A';
    localToCall 'A' 'B';
    localToCall 'A' 'B' 'C';

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-isAppOnly';
function      fct-isAppOnly(){
    fctIn "$*";

    titre3 'isAppOnly()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    isAppOnly '';
    display-vars '$?' "$?";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}



addLibrairie 'fct-isAppExist';
function      fct-isAppExist(){
    fctIn "$*";

    titre3 'isAppExist()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    isAppExist '';
    display-vars '$?' "$?";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-setAppFail';
function      fct-setAppFail(){
    fctIn "$*";

    titre3 'setCollecsetAppFailtionFail()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    setAppFail '';
    display-vars '$?' "$?";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-showModeAppIntegrale';
function      fct-showModeAppIntegrale(){
    fctIn "$*";

    titre3 'showModeAppIntegrale()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    showModeAppIntegrale '';
    display-vars '$?' "$?";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-setAppIntegrale';
function      fct-setAppIntegrale(){
    fctIn "$*";

    titre3 'setCollectionIntegrale()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    setCollectionIntegrale '';
    display-vars '$?' "$?";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-loadApp';
function      fct-loadApp(){
    fctIn;

    titre3 "loadApp";
    prog='rez';                     # rez
    display-vars 'prog' "$prog";

    titre4 'sans argument':
    loadApp;
    display-vars '$?' "$?";
    #display-vars '' "$";

    titre4 'argument vide':
    loadApp '';
    display-vars '$?' "$?";
    #display-vars '' "$";

    titre4  'gtt-tests/test-simple/':
    loadApp 'gtt-tests/test-simple/';
    display-vars '$?' "$?";
    #display-vars '' "$";




    fctOut "${FUNCNAME[0]}(0)"; return 0;
}