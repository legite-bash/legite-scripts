echo_d "${BASH_SOURCE[0]}" 1;

addLibrairie 'fct-PSPParams';
function      fct-PSPParams(){
    fctIn "$*";

    libExecAuto 'fct-displayPSPParams';
    libExecAuto 'fct-showPSPParams';
    libExecAuto 'fct-addPSPParams';
 
    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-displayPSPParams';
function      fct-displayPSPParams(){
    fctIn "$*";

    titre3 'displayPSPParams()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    displayPSPParams '';
    display-vars '$?' "$?";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-showPSPParams';
function      fct-showPSPParams(){
    fctIn "$*";

    titre3 'showPSPParams()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    showPSPParams '';
    display-vars '$?' "$?";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}




addLibrairie 'fct-addPSPParams';
function      fct-addPSPParams(){
    fctIn "$*";

    titre3 'addPSPParams()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    addPSPParams '';
    display-vars '$?' "$?";
    echoPSParam "";

    titre4 "un param sans valeur";
    eval-echo "addPSPParams 'param_sans_valeur'";
    display-vars '$?' "$?";
    echoPSParam "param_sans_valeur";

    titre4 "un param avec un mot";
    eval-echo "addPSPParams 'param_avec_valeur=mot'";
    display-vars '$?' "$?";
    echoPSParam "param_avec_valeur";

    titre4 "un param avec une phrase double quote";
    local _txt="c'est une phrase avec double quote";
    addPSPParams "param_phraseDouble=\"$_txt\"";
    display-vars '$?' "$?";
    echoPSParam "param_phraseDouble";

    titre4 "un param avec une phrase simple quote";
    _txt="c'est est une phrase avec simple quote";
    addPSPParams "param_phraseSimple='$_txt'";
    display-vars '$?' "$?";
    echoPSParam "param_phraseSimple";

    titre4 "un param dont la valeur contient '=' ";
    eval-echo "addPSPParams \"param_valeur_contient_egale1='a=1'\"";
    display-vars '$?' "$?";
    echoPSParam "param_valeur_contient_egale1";

    titre4 "un param dont la valeur contient '=' ";
    eval-echo "addPSPParams \"param_valeur_contient_egale2='abcdef=1234567'\"";
    display-vars '$?' "$?";
    echoPSParam 'param_valeur_contient_egale2';


    titre4 "Utilisation des PSPParams";

    eval-echo "addPSPParams \"chiffre1=1000\"";
    eval-echo "addPSPParams \"chiffre2=50\"";
    eval-echo "addPSPParams \"chiffre3=1000000\"";
    local _chiffre1=$(echoPSParam 'chiffre1');
    local _chiffre2=$(echoPSParam 'chiffre2');
    display-vars '_chiffre1 + _chiffre2' "$(( _chiffre1 + _chiffre2 ))";


    titre4 "showPSPParams() ";
    displayPSPParams;


    fctOut "${FUNCNAME[0]}(0)"; return 0;

}

