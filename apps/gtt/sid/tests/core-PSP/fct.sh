echo "${BASH_SOURCE[0]}";

addLibrairie 'test-fonctions';
function      test-fonctions(){
    fct-gtt-in;

    libExecAuto 'test-isCollectionExistRW';
    libExecAuto 'test-isCollectionExistNotR';
    libExecAuto 'testIn';
    libExecAuto 'test-splitHHMMSS';
    libExecAuto 'test-echoV';

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}


addLibrairie 'test-isCollectionExistRW';
function      test-isCollectionExistRW(){
    fct-gtt-in "$*";
    titre3 'Tester une collection lisible et executable';
    local _collectionFilename="$COLLECTIONS_REP/collectionRW.sh";
    touch "$_collectionFilename";
    chmod +rx "$_collectionFilename"; # on force les droits rx
    echo 'droits du fichier avant isCollectionExist()';
    ls -l "$_collectionFilename";
    echo 'Appel de isCollectionExist()';
    isCollectionExist "$_collectionFilename";
    echo 'droits du fichier après isCollectionExist()';
    ls -l "$_collectionFilename";
    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'test-isCollectionExistNotR';
function      test-isCollectionExistNotR(){
    fct-gtt-in "$*";

    titre3 'Tester une collection Non lisible';
    local _collectionFilename="$COLLECTIONS_REP/collectionNotR.sh";

    titre4 'Creation de la librairie et mise ne lecture seule';
    touch "$_collectionFilename";
    chmod -r "$_collectionFilename"; # suppression de son droit de lecture

    titre4 'Verificationd desdroits du fichier avant isCollectionExist()';
    ls -l "$_collectionFilename";

    titre4 'Appel de isCollectionExist()';
    isCollectionExist "$_collectionFilename";
    display-vars '$?' "$?";

    titre4 'droits du fichier après isCollectionExist()';
    ls -l "$_collectionFilename";
    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'testIn' "librairie de test de difference entre le nom de la lib et le nom de la fiction appellé par le code" 'testIn';
function      testIn() {
    fct-gtt-in;
    date >>/dev/shm/d;
    fctOut "${FUNCNAME[0]}(0)"; return 0;
}



################
## TEST SPLIT ##
################

addLibrairie 'test-splitHHMMSS';
function      test-splitHHMMSS(){
    fct-gtt-in "$*";

    eval-echo "splitHHMMSS"
    eval-echo "splitHHMMSS '0'"
    eval-echo "splitHHMMSS 5"
    eval-echo "splitHHMMSS 45"

    eval-echo "splitHHMMSS '8:8'"
    eval-echo "splitHHMMSS '0:1:6'"

    eval-echo "splitHHMMSS '18:8:6'"
    fctOut "${FUNCNAME[0]}(0)"; return 0;
}