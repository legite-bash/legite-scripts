echo "${BASH_SOURCE[0]}";

addLibrairie 'fct-app';
function      fct-app(){
    fctIn "$*";

    libExecAuto 'fct-isAppOnly';
    libExecAuto 'fct-isAppExist';
    libExecAuto 'fct-setAppFail';
    libExecAuto 'fct-showModeAppIntegrale';
    libExecAuto 'fct-setAppIntegrale';

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-isAppOnly';
function      fct-isAppOnly(){
    fctIn "$*";

    titre3 'isAppOnly()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    isAppOnly '';
    display-vars '$?' "$?";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}


addLibrairie 'fct-isAppExist';
function      fct-isAppExist(){
    fctIn "$*";

    titre3 'isAppExist()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    isAppExist '';
    display-vars '$?' "$?";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-setAppFail';
function      fct-setAppFail(){
    fctIn "$*";

    titre3 'setCollecsetAppFailtionFail()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    setAppFail '';
    display-vars '$?' "$?";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

#addLibrairie 'fct-showModeAppIntegrale';
#function      fct-showModeAppIntegrale(){
 #   fctIn "$*";

 #   titre3 'showModeAppIntegrale()';
 #   titreInfo "";
 #   #display-vars '' "";

 #   titre4 "par défaut";
 #   showModeAppIntegrale '';
 #   display-vars '$?' "$?";

 #   fctOut "${FUNCNAME[0]}(0)"; return 0;
#}

#addLibrairie 'fct-setAppIntegrale';
#function      fct-setAppIntegrale(){
#    fctIn "$*";

#    titre3 'setCollectionIntegrale()';
#    titreInfo "";
    #display-vars '' "";

#    titre4 "par défaut";
#    setCollectionIntegrale '';
#    display-vars '$?' "$?";

#    fctOut "${FUNCNAME[0]}(0)"; return 0;
#}

#clear; gtt.sh apps/gtt/sid/tests/fct/fct-app.sh fct-loadApplication;
addLibrairie 'fct-loadApplications';
function      fct-loadApplications(){
    fctIn;

    titre1 "loadApp";
    local _appPath="";

    debug-gtt-level-save 1;
    debug-app-level-save 1;

    titre2 'sans argument':
    eval-echo "loadApplication ";
    erreur-no-description-echo $?;
    
    titre2 'argument vide':
    eval-echo "loadApplication ''";
    erreur-no-description-echo $?;

    _appPath="ffmpeg/tests/test-ffmpeg_infos.sh";
    titre2  "app: $_appPath":
    eval-echo "loadApplication '$_appPath'";
    erreur-no-description-echo $?;

    debug-app-level-restaure;
    debug-gtt-level-restaure;
    TITRE1 "Fin des tests de ${FUNCNAME[0]}";
    fctOut "${FUNCNAME[0]}(0)"; return 0;
}