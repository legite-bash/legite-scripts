echo "${BASH_SOURCE[0]}";

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.22
# Description: 
# clear;gtt.sh --show-libsLevel2 apps/gtt/sid/tests

local _source_rep="${BASH_SOURCE%/*.*}";                        # display-vars '_source_rep' "$_source_rep";
local _source_rep_last="${_source_rep##*/}";                    # display-vars '_source_rep_last' "$_source_rep_last";
local _source_name="${BASH_SOURCE##*/}";                        # display-vars '_source_name' "$_source_name";
local _source_nom="${_source_name%.*}";                         # display-vars '_source_nom' "$_source_nom";
local _source_ext="${_source_name##*.}";                        # display-vars '_source_ext' "$_source_ext";

local _test_rep_rel="${_source_rep##$GTT_VERSION_SELECT_REP}";  # display-vars '_test_rep_rel' "$_test_rep_rel";  # /tests/./


#################################################
titre1 "Préparations de tests: ${_test_rep_rel}";
#################################################


#############################################################
titre2 "Création du repertoire temporaire: ${_test_rep_rel}";
#############################################################
tmp-rep "$TMP_ROOT${_test_rep_rel}";
tmp-display-vars;


##############################################################
#titre2 "Initialisation de l'objet VIDEOTEST_* pour les tests";
##############################################################
#yt-dlp -o "/media/mediatheques/CHARGE-Blender_Open_Movie.mp4" --format 18 https://www.youtube.com/watch?v=UXqq0ZvbOnk
declare -g VIDEOTEST_REP="/media/mediatheques";                     display-vars 'VIDEOTEST_REP     ' "$VIDEOTEST_REP";
declare -g VIDEOTEST_NOM='CHARGE-Blender_Open_Movie';               display-vars 'VIDEOTEST_NOM     ' "$VIDEOTEST_NOM";
declare -g VIDEOTEST_EXT='mp4';                                     display-vars 'VIDEOTEST_EXT     ' "$VIDEOTEST_EXT";
declare -g VIDEOTEST_NAME="${VIDEOTEST_NOM}.${VIDEOTEST_EXT}";      display-vars 'VIDEOTEST_NAME    ' "$VIDEOTEST_NAME";
declare -g VIDEOTEST_PATHNAME="$VIDEOTEST_REP/$VIDEOTEST_NAME";     display-vars 'VIDEOTEST_PATHNAME' "$VIDEOTEST_PATHNAME";


############################################################
titre2 "CHARGEMENT DES FICHIERS DE TESTS: ${_test_rep_rel}";
############################################################
for _fichier_pathname in $_source_rep/*.sh
do
    if [ "${_fichier_pathname##*/}" = '_ini.cfg.sh' ];then continue;fi  
    require-once "$_fichier_pathname";
done

titreInfo "Pour connaitre les librairies tests disponibles: gtt.sh --show-libsLevel2 apps/gtt/sid/${_test_rep_rel}";



########
# MAIN #
########
return 0;
