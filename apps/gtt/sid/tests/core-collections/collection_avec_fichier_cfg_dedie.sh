echo "${BASH_SOURCE[0]}";

##
# gtt.sh collections/gtt/sid/tests/essais/dom


addLibrairie 'collection_avec_fichier_cfg_dedie';
function      collection_avec_fichier_cfg_dedie() {
    fctIn;

    echo "JE SUIS DANS '${FUNCNAME[0]}'":
    #displayVar 'isCollectionExecLibSelf' "$isCollectionExecLibSelf";

    libExecAuto "${FUNCNAME[0]}-dom1";   # collection-Auto

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'dom-dom1'
function      dom-dom1() {
    fctIn "$*"

    echo "${FUNCNAME[0]}";
    echo "JE SUIS DANS '${FUNCNAME[0]}'":
    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'dom-domAuto'
function      dom-domAuto() {
    fctIn "$*"

    echo "${FUNCNAME[0]}";
    echo "JE SUIS DANS '${FUNCNAME[0]}'":
    fctOut "${FUNCNAME[0]}(0)"; return 0;
}