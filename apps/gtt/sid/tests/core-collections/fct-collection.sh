echo "${BASH_SOURCE[0]}";

addLibrairie 'fct-collection';
function      fct-collection(){
    fctIn "$*";

    libExecAuto 'fct-isCollectionOnly';
    libExecAuto 'fct-isCollectionExist';
    libExecAuto 'fct-setCollectionFail';
    libExecAuto 'fct-showModeCollectionIntegrale';
    libExecAuto 'fct-setCollectionIntegrale';

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-isCollectionOnly';
function      fct-isCollectionOnly(){
    fctIn "$*";

    titre3 'isCollectionOnly()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    isCollectionOnly '';
    display-vars '$?' "$?";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-isCollectionExist';
function      fct-isCollectionExist(){
    fctIn "$*";

    titre3 'isCollectionExist()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    isCollectionExist '';
    display-vars '$?' "$?";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-setCollectionFail';
function      fct-setCollectionFail(){
    fctIn "$*";

    titre3 'setCollectionFail()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    setCollectionFail '';
    display-vars '$?' "$?";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

#addLibrairie 'fct-showModeCollectionIntegrale';
#function      fct-showModeCollectionIntegrale(){
#    fctIn "$*";

#    titre3 'showModeCollectionIntegrale()';
#    titreInfo "";
    #display-vars '' "";

#    titre4 "par défaut";
#    showModeCollectionIntegrale '';
#    display-vars '$?' "$?";

#    fctOut "${FUNCNAME[0]}(0)"; return 0;
#}

#addLibrairie 'fct-setCollectionIntegrale';
#function      fct-setCollectionIntegrale(){
 #   fctIn "$*";

#    titre3 'setCollectionIntegrale()';
#    titreInfo "";
    #display-vars '' "";

#    titre4 "par défaut";
#    setCollectionIntegrale '';
#    display-vars '$?' "$?";

#    fctOut "${FUNCNAME[0]}(0)"; return 0;
#}

# clear; gtt.sh apps/gtt/sid/tests/fct/fct-collection.sh fct-loadCollection;
addLibrairie 'fct-loadCollection';
function      fct-loadCollection(){
    fctIn;

    TITRE1 "DEBUTS des tests de ${FUNCNAME[0]}";
    local _collectionPath="";
    
    
    titre2 'sans argument':
    eval-echo "loadCollection";
    erreur-no-description-echo $?;

    titre2 'argument vide':
    eval-echo "loadCollection ''";
    erreur-no-description-echo $?;

    #prog='rez';                     # rez
    #display-vars 'prog' "$prog";


    _collectionPath="gtt/sid/tests/essais/collection_avec_librairie_homonyme";
    titre2 "collection: $_collectionPath":
    debugGTT_level-save 1;
    debug_level-save 1;
    eval-echo "loadCollection '$_collectionPath'";
    erreur-no-description-echo $?;


    debug_level-restaure;
    debugGTT_level-restaure;
    TITRE1 "Fin des tests de ${FUNCNAME[0]}";
    fctOut "${FUNCNAME[0]}(0)"; return 0;
}