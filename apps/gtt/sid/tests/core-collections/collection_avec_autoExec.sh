echo "${BASH_SOURCE[0]}";
##
# gtt.sh gtt/tests/essais/collection_avec_autorun

# si addLibrairie est absent la fonction ne sera jamais lancé via le PSP
# clear; gtt.sh               apps/gtt/sid/tests/collection_avec_autoExec.sh 
# clear; gtt.sh --libExecAuto apps/gtt/sid/tests/collection_avec_autoExec.sh
# clear; gtt.sh --libExecAuto apps/gtt/sid/tests/collection_avec_autoExec.sh collection_avec_autoExec
addLibrairie 'collection_avec_autoExec' 'collection ayant une fonction homonyme';
function      collection_avec_autoExec(){
    fctIn;
    titre1 "${FUNCNAME[0]}():Ceci est la librairie homonyme";

    libExecAuto "${FUNCNAME[0]}-1";
    libExecAuto "${FUNCNAME[0]}-2";

    fctOut "${FUNCNAME[0]}";return 0;
}

addLibrairie 'collection_avec_autoExec-1';
function      collection_avec_autoExec-1(){
    fctIn;
    echo "JE SUIS DANS LA FONCTION ${FUNCNAME[0]}() du fichier: '${BASH_SOURCE[0]}'";
    fctOut "${FUNCNAME[0]}";return 0;
}
addLibrairie 'collection_avec_autoExec-2';
function      collection_avec_autoExec-2(){
    fctIn;
    echo "JE SUIS DANS LA FONCTION ${FUNCNAME[0]}() du fichier: '${BASH_SOURCE[0]}'";
    fctOut "${FUNCNAME[0]}";return 0;
}
