echo "$BASH_SOURCE";

# clear; gtt.sh apps/gtt/sid/tests/collection_sans_librairie_homonyme.sh collection_sans_autorun
# clear; gtt.sh apps/gtt/sid/tests/collection_sans_librairie_homonyme    collection_sans_autorun
# clear; gtt.sh apps/gtt/sid/tests/collection_sans_librairie_homonyme.sh
# clear; gtt.sh apps/gtt/sid/tests/collection_sans_librairie_homonyme
addLibrairie 'collection_sans_autorun' 'Juste pour voir si la lib est chargé';
function      collection_sans_autorun {
    echo "${BASH_SOURCE[0]}:$LINENO:${FUNCNAME[0]}";
    titre1 "La librairie '${FUNCNAME[0]}' est bien appellée.";
    return 0;
}