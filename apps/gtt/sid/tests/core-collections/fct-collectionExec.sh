echo "${BASH_SOURCE[0]}";

addLibrairie 'fct-collectionExec';
function      fct-collectionExec(){
    fctIn "$*";

    libExecAuto 'fct-showCollectionExec';
    libExecAuto 'fct-setCollectionExec';
    libExecAuto 'fct-isCollectionExec';
    libExecAuto 'fct-showModeCollectionIntegrale';

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-showCollectionExec';
function      fct-showCollectionExec(){
    fctIn "$*";

    titre3 'showCollectionExec()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    showCollectionExec '';
    display-vars '$?' "$?";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-setCollectionExec';
function      fct-setCollectionExec(){
    fctIn "$*";

    titre3 'setCollectionExec()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    setCollectionExec '';
    display-vars '$?' "$?";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-isCollectionExec';
function      fct-isCollectionExec(){
    fctIn "$*";

    titre3 'isCollectionExec()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    isCollectionExec '';
    display-vars '$?' "$?";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-setCollectionFail';
function      fct-setCollectionFail(){
    fctIn "$*";

    titre3 'setCollectionFail()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    setCollectionFail '';
    display-vars '$?' "$?";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

addLibrairie 'fct-setCollectionIntegrale';
function      fct-setCollectionIntegrale(){
    fctIn "$*";

    titre3 'setCollectionIntegrale()';
    titreInfo "";
    #display-vars '' "";

    titre4 "par défaut";
    setCollectionIntegrale '';
    display-vars '$?' "$?";

    fctOut "${FUNCNAME[0]}(0)"; return 0;
}

