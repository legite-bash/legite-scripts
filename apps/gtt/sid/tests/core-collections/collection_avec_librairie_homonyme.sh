echo "${BASH_SOURCE[0]}";
##
# gtt.sh gtt/tests/essais/collection_avec_autorun

# si addLibrairie est absent la fonctione ne sera jamais lancé via le PSP
# clear; gtt.sh apps/gtt/sid/tests/collection_avec_librairie_homonyme.sh collection_avec_librairie_homonyme
# clear; gtt.sh apps/gtt/sid/tests/collection_avec_librairie_homonyme    collection_avec_librairie_homonyme
# clear; gtt.sh apps/gtt/sid/tests/collection_avec_librairie_homonyme.sh
# clear; gtt.sh apps/gtt/sid/tests/collection_avec_librairie_homonyme
addLibrairie 'collection_avec_librairie_homonyme' 'collection ayant une fonction homonyme';
function      collection_avec_librairie_homonyme(){
    fctIn;
    echo "${BASH_SOURCE[0]}:$LINENO:${FUNCNAME[0]}";
    titre1 "La librairie '${FUNCNAME[0]}' est bien appellée.";
    fctOut "${FUNCNAME[0]}";return 0;
}
