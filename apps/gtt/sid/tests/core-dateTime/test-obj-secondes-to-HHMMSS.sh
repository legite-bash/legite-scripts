echo "${BASH_SOURCE[0]}";

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.23
# Description: 
# clear;gtt.sh  --show-ibsLevels2 apps/gtt/sid/tests/core-dateTime/ 

local _source_rep="${BASH_SOURCE%/*.*}";        # display-vars '_source_rep' "$_source_rep";
local _source_rep_last="${_source_rep##*/}";    # display-vars '_source_rep_last' "$_source_rep_last";
local _source_name="${BASH_SOURCE##*/}";        # display-vars '_source_name' "$_source_name";
local _source_nom="${_source_name%.*}";         # display-vars '_source_nom' "$_source_nom";
local _source_ext="${_source_name##*.}";        # display-vars '_source_ext' "$_source_ext";

local _test_rep_rel="${_source_rep##$GTT_VERSION_SELECT_REP}";  # display-vars '_test_rep_rel' "$_test_rep_rel";  # /tests/./



# clear;gtt.sh  --libExecAuto apps/gtt/sid/tests/core-dateTime/ test-obj-secondes-to-HHMMSS
addLibrairie 'test-obj-secondes-to-HHMMSS';
function      test-obj-secondes-to-HHMMSS(){

    libExecAuto 'test-fct-secondes-to-HHMMSS';

    return 0;
}


# clear;gtt.sh   apps/gtt/sid/tests/core-dateTime/ test-fct-secondes-to-HHMMSS
addLibrairie 'test-fct-secondes-to-HHMMSS';
function      test-fct-secondes-to-HHMMSS(){

    local _programme="secondes-to-HHMMSS";

    titre2 "${_programme}()";

    titre4 'sans argument':
    eval-echo "${_programme}";
;

    titre4 'argument vide':
    eval-echo "${_programme} ''";
    erreur-no-description-echo;

    # - - #
    local _HHMMSS="0";
    titre4 "format: $_HHMMSS";
    eval-echo "${_programme} '$_HHMMSS'";
    erreur-no-description-echo;
    eval-echo "${_programme}-display-vars";

    # - - #
    _HHMMSS="00";
    titre4 "format: $_HHMMSS";
    eval-echo "${_programme} '$_HHMMSS'";
    erreur-no-description-echo;
    eval-echo "${_programme}-display-vars";

    # - - #
    _HHMMSS="60";
    titre4 "format: $_HHMMSS";
    eval-echo "${_programme} '$_HHMMSS'";
    erreur-no-description-echo;
    eval-echo "${_programme}-display-vars";

    _HHMMSS="0120";
    titre4 "format: $_HHMMSS";
    eval-echo "${_programme} '$_HHMMSS'";
    erreur-no-description-echo;
    eval-echo "${_programme}-display-vars";

    _HHMMSS="7200";
    titre4 "format: $_HHMMSS";
    eval-echo "${_programme} '$_HHMMSS'";
    erreur-no-description-echo;
    eval-echo "${_programme}-display-vars";

    _HHMMSS="18145";
    titre4 "format: $_HHMMSS";
    eval-echo "${_programme} '$_HHMMSS'";
    erreur-no-description-echo;
    eval-echo "${_programme}-display-vars";

    return 0;
}


return 0;