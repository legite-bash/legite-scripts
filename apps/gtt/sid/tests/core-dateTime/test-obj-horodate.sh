echo "${BASH_SOURCE[0]}";


# clear;gtt.sh  --libExecAuto apps/gtt/sid/tests/core-dateTime/ test-obj-horodate
addLibrairie 'test-obj-horodate';
function      test-obj-horodate(){

    libExecAuto 'test-fct-horodate-before';
    libExecAuto 'test-fct-horodate-after';


    return 0;
}


# clear;gtt.sh   apps/gtt/sid/tests/core-dateTime/ test-fct-horodate-before
addLibrairie 'test-fct-horodate-before';
function      test-fct-horodate-before(){

    local _programme="horodate-before";

    titre2 "${_programme}()";

    titre4 'sans argument':
    eval-echo "${_programme}";
    erreur-no-description-echo;

    titre4 'argument vide':
    eval-echo "${_programme} ''";
    erreur-no-description-echo;

    # - - #
    titre4 'format_par_defaut';
    eval-echo "${_programme} 'fichier'";
    erreur-no-description-echo;
    eval-echo "${_programme}-display-vars";

    titre4 'format_personalise';
    eval-echo "${_programme}-date-format \"%Y.%m.%d-\"";  # \" obligatoire sinon date double les "
    eval-echo "${_programme} 'format_personalise'";
    erreur-no-description-echo;
    eval-echo "${_programme}-display-vars";

    return 0;
}


# clear;gtt.sh   apps/gtt/sid/tests/core-dateTime/ test-fct-horodate-after
addLibrairie 'test-fct-horodate-after';
function      test-fct-horodate-after(){

    local _programme="horodate-after";

    titre2 "${_programme}()";

    titre4 'sans argument':
    eval-echo "${_programme}";
    erreur-no-description-echo;

    titre4 'argument vide':
    eval-echo "${_programme} ''";
    erreur-no-description-echo;

    # - - #
    titre4 'format_par_defaut';
    eval-echo "${_programme} 'fichier'";
    erreur-no-description-echo;
    eval-echo "${_programme}-display-vars";

    titre4 'format_personalise';
    eval-echo "${_programme}-date-format \"-%Y.%m.%d\"";  # \" obligatoire sinon date double les "
    eval-echo "${_programme} 'format_personalise'";
    erreur-no-description-echo;
    eval-echo "${_programme}-display-vars";



    return 0;
}



return 0;