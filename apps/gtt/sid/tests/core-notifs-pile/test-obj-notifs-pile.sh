echo "${BASH_SOURCE[0]}";

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.19
# Description: 


# clear; gtt.sh --libExecAuto apps/gtt/sid/tests/gtt-core-notifs-pile/test-obj-notifs-pile test-obj-notifs-pile
addLibrairie 'test-obj-notifs-pile';
function      test-obj-notifs-pile(){
    local -i _pile_fct_app_level=1;
    fct-pile-app-in "$*" $_pile_fct_app_level;

    titreInfo 'Faites les tests avec différent -d';
    libExecAuto 'test-obj-notifs-pile-add';
    libExecAuto 'test-obj-notifs-pile-show';

    echo "Fin des tests de ${FUNCNAME[0]}";
    
    fct-pile-app-out $E_TRUE $_pile_fct_app_level;
    #debug-app-level-restaure;  # Fin de la simulation de -d

    return 0;
}


# clear; gtt.sh apps/gtt/sid/tests/gtt-core-notifs-pile/ test-obj-notifs-pile-add
addLibrairie 'test-obj-notifs-pile-add';
function      test-obj-notifs-pile-add(){
    local -i _pile_fct_app_level=1;
    fct-pile-app-in "$*" $_pile_fct_app_level;

    titre4 'sans argument':
    eval-echo "notifs-pile-add";
    erreur-no-description-echo $?;

    titre4 'argument vide':
    eval-echo "notifs-pile-add";
    erreur-no-description-echo $?;

    titre4 'un texte':
    eval-echo "notifs-pile-add 'notifs 1'";
    erreur-no-description-echo $?;

    titre4 'un chiffre':
    eval-echo "notifs-pile-add 19949596";
    erreur-no-description-echo $?;

    titre4 '':
    eval-echo "notifs-pile-add";
    erreur-no-description-echo $?;

    titre4 '':
    eval-echo "notifs-pile-add";
    erreur-no-description-echo $?;

    fct-pile-app-out $E_TRUE $_pile_fct_app_level;
    #debug-app-level-restaure;  # Fin de la simulation de -d
    return 0;
}


# clear; gtt.sh apps/gtt/sid/tests/gtt-core-notifs-pile/ test-obj-notifs-pile-show
addLibrairie 'test-obj-notifs-pile-show';
function      test-obj-notifs-pile-show(){
    local -i _pile_fct_app_level=1;
    fct-pile-app-in "$*" $_pile_fct_app_level;

    titre4 'Affichage des notifs':
    notifs-pile-show;

    fct-pile-app-out $E_TRUE $_pile_fct_app_level;
    #debug-app-level-restaure;  # Fin de la simulation de -d
    return 0;
}

return 0;