echo "${BASH_SOURCE[0]}";

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.19
# Description: 


# clear; gtt.sh --libExecAuto apps/gtt/sid/tests/gtt-core-notifs-pile/test-obj-erreurs-pile test-obj-erreurs-pile
addLibrairie 'test-obj-erreurs-pile';
function      test-obj-erreurs-pile(){
    local -i _pile_fct_app_level=1;
    fct-pile-app-in "$*" $_pile_fct_app_level;

    titreInfo 'Faites les tests avec différent -d';
    libExecAuto 'test-obj-erreurs-pile-add';
    libExecAuto 'test-obj-erreurs-pile-show';

    echo "Fin des tests de ${FUNCNAME[0]}";
    
    fct-pile-app-out $E_TRUE $_pile_fct_app_level;
    #debug-app-level-restaure;  # Fin de la simulation de -d

    return 0;
}


# clear; gtt.sh apps/gtt/sid/tests/gtt-core-notifs-pile/ test-obj-erreurs-pile-add
addLibrairie 'test-obj-erreurs-pile-add';
function      test-obj-erreurs-pile-add(){
    local -i _pile_fct_app_level=1;
    fct-pile-app-in "$*" $_pile_fct_app_level;

    titre4 'sans argument':
    eval-echo "erreurs-pile-add";
    erreur-no-description-echo $?;

    titre4 'argument vide':
    eval-echo "erreurs-pile-add";
    erreur-no-description-echo $?;

    titre4 'un texte':
    eval-echo "erreurs-pile-add 'erreurs 1'";
    erreur-no-description-echo $?;

    titre4 'un chiffre':
    eval-echo "erreurs-pile-add 19949596";
    erreur-no-description-echo $?;

    titre4 '':
    eval-echo "erreurs-pile-add";
    erreur-no-description-echo $?;

    titre4 '':
    eval-echo "erreurs-pile-add";
    erreur-no-description-echo $?;

    fct-pile-app-out $E_TRUE $_pile_fct_app_level;
    #debug-app-level-restaure;  # Fin de la simulation de -d
    return 0;
}


# clear; gtt.sh apps/gtt/sid/tests/gtt-core-notifs-pile/ test-obj-erreurs-pile-show
addLibrairie 'test-obj-erreurs-pile-show';
function      test-obj-erreurs-pile-show(){
    local -i _pile_fct_app_level=1;
    fct-pile-app-in "$*" $_pile_fct_app_level;

    titre4 'Affichage des erreurs':
    erreurs-pile-show;

    fct-pile-app-out $E_TRUE $_pile_fct_app_level;
    #debug-app-level-restaure;  # Fin de la simulation de -d
    return 0;
}

return 0;