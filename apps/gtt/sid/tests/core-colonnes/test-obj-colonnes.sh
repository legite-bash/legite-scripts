echo "${BASH_SOURCE[0]}";

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 
# clear;gtt.sh  --show-libsLevel apps/gtt/sid/tests/core-colonnes test-obj-colonnes

# clear;gtt.sh  --libExecAuto apps/gtt/sid/tests/core-colonnes test-obj-colonnes
addLibrairie 'test-obj-colonnes';
function      test-obj-colonnes(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    libExecAuto 'test-fct-colonnes-1';
    libExecAuto 'test-fct-colonnes-1C';
    libExecAuto 'test-fct-colonnes-1R';
    libExecAuto 'test-fct-colonnes-3LCR';
    libExecAuto 'test-fct-colonnes-_-show';

    fct-pile-app-out $E_TRUE $_fct_pile_app_level;
    #debug-app-level-restaure;  # Fin de la simulation de -d
    return 0;

}


###################
# objet: colonnes #
###################
# clear;gtt.sh   apps/gtt/sid/tests/divers/ test-fct-colonnes-1
addLibrairie 'test-fct-colonnes-1';
function      test-fct-colonnes-1(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;
    
    titre1 "${FUNCNAME[0]}: Une colonne en Left";
    eval-echo 'colonnes-init';

    eval-echo 'colonnes-colAuto-lgMax  50';
    eval-echo 'colonnes-colAuto-lg     50';
 
    #titre2 "colonnes-lg $colonnes_colAuto_lg";
    eval-echo 'colonnes-lg   50';      
 
    #titre2 "colonnes-dir 'L'";
    eval-echo 'colonnes-dir  L';
 
    #titre2 "colonnes-sep 'L'";
    eval-echo 'colonnes-sep "|"';

    #titre2 "colonnes-entetes";
    eval-echo 'colonnes-entetes-values "Name"';

    #titre2 "colonnes-entetes-show";
    eval-echo 'colonnes-entetes-show';

    #titre2 "colonnes-datas";
    eval-echo "colonnes-datas-values 'Ligne1 col1'";
    eval-echo 'colonnes-datas-show';
    eval-echo "colonnes-datas-values 'Ligne2 col1'";
    eval-echo 'colonnes-datas-show';

    fct-pile-app-out $E_TRUE $_fct_pile_app_level;
    #debug-app-level-restaure;  # Fin de la simulation de -d
    return 0;
}
#


# clear;gtt.sh   apps/gtt/sid/tests/divers/ test-fct-colonnes-1C
addLibrairie 'test-fct-colonnes-1C';
function      test-fct-colonnes-1C(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:Une colonne avec remplissage sur tout l'écran";
    colonnes-init;

    eval-echo 'colonnes-colAuto-lgMax  50';
    eval-echo 'colonnes-colAuto-lg     50';
 
    #titre2 "colonnes-lg $colonnes_colAuto_lg";
    eval-echo 'colonnes-lg   50';    
 
    #titre2 "colonnes-dir 'C'";
    eval-echo 'colonnes-dir  C';
 
    #titre2 "colonnes-sep '|'";
    eval-echo 'colonnes-sep "|"';

    #titre2 "colonnes-entetes";
    eval-echo 'colonnes-entetes-values "Name"';

    #titre2 "colonnes-entetes-show";
    eval-echo 'colonnes-entetes-show';

    #titre2 "colonnes-datas";
    eval-echo "colonnes-datas-values 'Ligne1 col1'";
    eval-echo 'colonnes-datas-show';
    eval-echo "colonnes-datas-values 'Ligne2 col1'";
    eval-echo 'colonnes-datas-show';

    fct-pile-app-out $E_TRUE $_fct_pile_app_level;
    #debug-app-level-restaure;  # Fin de la simulation de -d
    return 0;

}


# clear;gtt.sh   apps/gtt/sid/tests/divers/ test-fct-colonnes-1R
addLibrairie 'test-fct-colonnes-1R';
function      test-fct-colonnes-1R(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;
    
    titre1 "${FUNCNAME[0]}:Une colonne avec remplissage sur tout l'écran";
    colonnes-init;

    eval-echo 'colonnes-colAuto-lgMax  50';
    eval-echo 'colonnes-colAuto-lg     50';
 
    #titre2 "colonnes-lg $colonnes_colAuto_lg";
    eval-echo 'colonnes-lg   50';    
 
    #titre2 "colonnes-dir 'R'";
    eval-echo 'colonnes-dir  R';
 
    #titre2 "colonnes-sep '|'";
    eval-echo 'colonnes-sep "|"';

    #titre2 "colonnes-entetes";
    eval-echo 'colonnes-entetes-values "Name"';

    #titre2 "colonnes-entetes-show";
    eval-echo 'colonnes-entetes-show';

    #titre2 "colonnes-datas";
    eval-echo "colonnes-datas-values 'Ligne1 col1'";
    eval-echo 'colonnes-datas-show';
    eval-echo "colonnes-datas-values 'Ligne2 col1'";
    eval-echo 'colonnes-datas-show';

    fct-pile-app-out $E_TRUE $_fct_pile_app_level;
    #debug-app-level-restaure;  # Fin de la simulation de -d
    return 0;
}

# clear;gtt.sh   apps/gtt/sid/tests/divers/ test-fct-colonnes-3LCR
addLibrairie 'test-fct-colonnes-3LCR';
function      test-fct-colonnes-3LCR(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;
    
    titre1 "${FUNCNAME[0]}: 3 colonnes Left Center Rigth";
    colonnes-init;

    eval-echo 'colonnes-colAuto-lgMax  50';
    eval-echo 'colonnes-colAuto-lg     50';
 
    #titre2 "colonnes-lg $colonnes_colAuto_lg";
    eval-echo 'colonnes-lg   50';    
 
    #titre2 "colonnes-dir L C R";
    eval-echo 'colonnes-dir L C R';
 
    #titre2 "colonnes-sep '|'";
    eval-echo 'colonnes-sep "|"';

    #titre2 "colonnes-entetes";
    eval-echo 'colonnes-entetes-values "ENTETE1" ENETETE2 ENTETE3';
    eval-echo 'colonnes-entetes-show';

    eval-echo "colonnes-entetes-values 'Ligne1 col1' 'Long ENTETE mais vraiment beaucoup beaucoup beaucoup beaucoup beaucoup trop long'";
    eval-echo 'colonnes-entetes-show';

    #titre2 "colonnes-datas";
    eval-echo "colonnes-datas-values 'Ligne2 col1' 'Ligne1 col2' 'Ligne1 col3'";
    eval-echo 'colonnes-datas-show';
    eval-echo "colonnes-datas-values 'Ligne3 col1' 'Long texte mais vraiment beaucoup beaucoup beaucoup beaucoup beaucoup trop long'";
    eval-echo 'colonnes-datas-show';

    colonnes-datas-values 'Un texte vide au centre' '' 'Suivit de la droite';
    eval-echo 'colonnes-datas-show';

    fct-pile-app-out $E_TRUE $_fct_pile_app_level;
    #debug-app-level-restaure;  # Fin de la simulation de -d
    return 0;
}


# clear;gtt.sh   apps/gtt/sid/tests/divers/ test-fct-colonnes-_-show
addLibrairie 'test-fct-colonnes-_-show';
function      test-fct-colonnes-_-show(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;
    
    titre1 "${FUNCNAME[0]}: Test de entetes|datas-values-show()";
    colonnes-init;
    colonnes-colAuto-lgMax  50;
    colonnes-colAuto-lg     50;
    colonnes-lg   50 5 7 25;
    colonnes-dir C;
    colonnes-sep "|" X , =;
    colonnes-entetes-values-show "ENTETE1" ENETETE2 ENTETE3 'dernier entete';
    colonnes-datas-values-show   'Ligne1 col1' 'Ligne1 col2' 'Ligne1 col3' 'Ligne1 col4';

    fct-pile-app-out $E_TRUE $_fct_pile_app_level;
    #debug-app-level-restaure;  # Fin de la simulation de -d
    return 0;
}

