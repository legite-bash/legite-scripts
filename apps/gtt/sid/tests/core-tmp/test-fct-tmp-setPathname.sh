echo "${BASH_SOURCE[0]}";

# gtt.sh 
# date de création    : 2024.08.22
# date de modification: 2024.08.22
# Description: 


local _source_rep="${BASH_SOURCE%/*.*}";        # display-vars '_source_rep' "$_source_rep";
local _source_rep_last="${_source_rep##*/}";    # display-vars '_source_rep_last' "$_source_rep_last";
local _source_name="${BASH_SOURCE##*/}";        # display-vars '_source_name' "$_source_name";
local _source_nom="${_source_name%.*}";         # display-vars '_source_nom' "$_source_nom";
local _source_ext="${_source_name##*.}";        # display-vars '_source_ext' "$_source_ext";

local _test_rep_rel="${_source_rep##$GTT_VERSION_SELECT_REP}";  # display-vars '_test_rep_rel' "$_test_rep_rel";  # /tests/./


# clear;gtt.sh  --libExecAuto apps/gtt/sid/tests/core-tmp/ test-fct-tmp-setPathname
addLibrairie 'test-fct-tmp-pathname';
function      test-fct-tmp-pathname(){

    titre0 "${FUNCNAME[0]}()";

    libExecAuto 'test-fct--vide';
    #libExecAuto 'test-fct-erreur-';
    #libExecAuto 'test-fct-erreur-';
    #libExecAuto 'test-fct-erreur-';

    return 0;
}



# clear; gtt.sh -d --libExecAuto apps/gtt/sid/tests test-fct-tmp-setPathname--vide
addLibrairie 'test-fct-tmp-setPathname--vide' "";
function      test-fct-tmp-setPathname--vide(){
    #debug_gtt_level-save 1;  # simuler -D

    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    local _programme="tmp-setPathname";

    titre2 "${_programme}()";


    titre4 'sans argument':
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 'argument vide':
    eval-echo "$_programme ''";
    erreur-no-description-echo;

    
    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
    #debug_gtt_level-restaure;  # Fin de la simulation de -D
}

# clear; gtt.sh -d --libExecAuto apps/gtt/sid/tests
#addLibrairie 'test-fct-tmp-setPathname' "";
function       test-fct-tmp-setPathname-value(){
    #debug_gtt_level-save 1;  # simuler -D

    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    local _programme="tmp-setPathname";

    titre2 "${_programme}()";


    titre4 'Racine /';
    eval-echo "${_programme}-init";
    eval-echo "$_programme '/'";
    erreur-no-description-echo;

    titre4 "$TMP_ROOT/gtt/setPathname.log";
    eval-echo "${_programme}-init";
    eval-echo "$_programme";
    erreur-no-description-echo;

    titre4 '';
    eval-echo "${_programme}-init";
    eval-echo "$_programme";
    erreur-no-description-echo;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
    #debug_gtt_level-restaure;  # Fin de la simulation de -D
}

return 0;