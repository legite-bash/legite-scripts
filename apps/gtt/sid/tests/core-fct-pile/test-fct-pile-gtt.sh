echo "${BASH_SOURCE[0]}";

# gtt.sh --show-libsLevel2 apps/ffmpeg/tests/test-fct-ffmpeg.sh lib
# date de création    : 2024.08.04
# date de modification: 2024.08.10
# Description: Enregistrement dans une pile des fonctions avec fonction d'affichage imbriquée


# clear; gtt.sh -Dd --libExecAuto apps/gtt/sid/tests/test-fct-pile-gtt test-fct-pile
addLibrairie 'test-fct-pile';
function      test-fct-pile(){

    debug-gtt-level-save 1;  # simuler -D

    #debug-app-level-save 1;  # simuler -d

    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;


    titre1 "${FUNCNAME[0]}:BEGIN";

    titreInfo "Fichier code: apps/gtt/sid/gtt-core.sh";

    display-vars 'DEBUG_GTT_LEVEL_DEFAUT' "$DEBUG_GTT_LEVEL_DEFAUT";
    display-vars 'debug_gtt_level       ' "$debug_gtt_level";
    display-vars 'debug_gtt_levelBak    ' "$debug_gtt_levelBak";

    test-fct-pile-function1;

    fct-pile-app-out 0 $_fct_pile_app_level;
    #debug_gtt_level-restaure;  # Fin de la simulation de -D
    return 0;
}


function test-fct-pile-function1(){
    #debug-app-level-save 1;  # simuler -d

    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titreInfo "${FUNCNAME[0]}(0)";
    test-fct-pile-function2;

    fct-pile-app-out 0 $_fct_pile_app_level;
    #debug_gtt_level-restaure;  # Fin de la simulation de -D
}

function test-fct-pile-function2(){
    #debug-app-level-save 1;  # simuler -d

    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titreInfo "${FUNCNAME[0]}(0)";

    fct-pile-app-out 0 $_fct_pile_app_level;
    #debug_gtt_level-restaure;  # Fin de la simulation de -D
}



#libExecAuto test-fctPile;

return 0;
