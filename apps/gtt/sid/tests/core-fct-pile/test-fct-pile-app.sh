echo "${BASH_SOURCE[0]}";

# gtt.sh --show-libsLevel2 apps/ffmpeg/tests/test-fct-ffmpeg.sh lib
# date de création    : 2024.08.04
# date de modification: 2024.08.04
# Description: 


# clear; gtt.sh -d --libExecAuto apps/gtt/sid/tests/test-pile-fct-app test-pile-fct-app
addLibrairie 'test-pile-fct-app';
function      test-pile-fct-app(){

    #debug-app-level-save 1;  # simuler -d

    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre1 "${FUNCNAME[0]}:BEGIN
    Tester la fonction avec plusieurs niveau de -d
    clear; gtt.sh -ddd --libExecAuto apps/gtt/sid/tests/test-pile-fct-app test-pile-fct-app";


    titreInfo "Fichier code: apps/gtt/sid/gtt-core.sh";
    display-vars 'DEBUG_LEVEL_DEFAUT  ' "$DEBUG_LEVEL_DEFAUT";
    display-vars 'debug_app_level     ' "$debug_app_level";
    display-vars 'debug_app_level_bak ' "$debug_app_level_bak";

    echo "";
    titreInfo "Tester la fonction avec plusieurs niveau de -d";
    echo "";

    test-pile-fct-app-function1;

    titre1 "${FUNCNAME[0]}:END";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level;
    #debug-app-level-restaure;  # Fin de la simulation de -d
    return 0;
}


function test-pile-fct-app-function1(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;
    echo "${FUNCNAME[0]}():code";
    test-pile-fct-app-function2;
    fct-pile-app-out $E_TRUE $_fct_pile_app_level;return $E_TRUE;
}

function test-pile-fct-app-function2(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;
    echo "${FUNCNAME[0]}():code";
    test-pile-fct-app-function3;
    fct-pile-app-out $E_TRUE $_fct_pile_app_level;return $E_TRUE;
}

function test-pile-fct-app-function3(){
    local -i _fct_pile_app_level=3;
    fct-pile-app-in "$*" $_fct_pile_app_level;
    echo "${FUNCNAME[0]}():code";
    fct-pile-app-out $E_TRUE $_fct_pile_app_level;return 0;
}


#libExecAuto test-pile-fct-app;

return 0;
