echo "${BASH_SOURCE[0]}";

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.22
# Description: 
# code: 
# clear;gtt.sh --show-libsLevel2  apps/gtt/sid/tests/core-inode-normalize/ obj-inode-normalize


# clear;gtt.sh  --libExecAuto apps/gtt/sid/tests/core-inode-normalize/ obj-inode-normalize
addLibrairie 'obj-inode-normalize';
function      obj-inode-normalize(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre0 "${FUNCNAME[0]}()";
    local _programme="inode-normalize";

    test-obj-inode-normalize-init;
    libExecAuto 'test-fct-inode-normalize--vide';

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear;gtt.sh apps/gtt/sid/tests/core-inode-normalize/ test-obj-inode-normalize-init
addLibrairie 'test-obj-inode-normalize-init';
function      test-obj-inode-normalize-init(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";

    local _programme="inode-normalize";

    titre4 'initialisation':
    eval-echo "${_programme}-init";
    ${_programme}-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear;gtt.sh  apps/gtt/sid/tests/core-inode-normalize/ test-fct-inode-normalize--vide
addLibrairie 'test-fct-inode-normalize--vide';
function      test-fct-inode-normalize--vide(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre2 "${FUNCNAME[0]}()";
    titreInfo "";

    local _programme="inode-normalize";

    titre4 'sans argument':
    eval-echo "${_programme}-init";
    
    eval-echo "$_programme";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    titre4 'argument vide':
    eval-echo "${_programme}-init";
    eval-echo "$_programme ''";
    erreur-no-description-echo;
    ${_programme}-display-vars;
    
    titre4 'valeur: 0':
    eval-echo "${_programme}-init";
    eval-echo "$_programme 0";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    titre4 'valeur: -1':
    eval-echo "${_programme}-init";
    eval-echo "$_programme -1";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    titre4 'valeur: --1':
    eval-echo "${_programme}-init";
    eval-echo "$_programme --1";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    titre4 'valeur: $E_FALSE':
    eval-echo "${_programme}-init";
    eval-echo "$_programme $E_FALSE";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}



# clear;gtt.sh  apps/gtt/sid/tests/core-inode-normalize/ test-fct-inode-normalize-random_file
addLibrairie 'test-fct-inode-normalize-random_file';
function      test-fct-inode-normalize-random_file(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    local _programme="inode-normalize";

    local _random_file_pathname="$tmp_rep/Mon nom à moi.EXT";
    rm "$_random_file_pathname";
    eval-echo "touch \"$_random_file_pathname\"";

    titre4 "random_file: $_random_file_pathname";
    #eval-echo "${_programme}-init";
    
    eval-echo "$_programme \"$_random_file_pathname\"";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}



# clear;gtt.sh  apps/gtt/sid/tests/core-inode-normalize/ test-fct-inode-normalize-# clear;gtt.sh  apps/gtt/sid/tests/core-inode-normalize/ test-fct-inode-normalize-VIDEOTEST_PATHNAME
addLibrairie 'test-fct-inode-normalize-VIDEOTEST_PATHNAME';
function      test-fct-inode-normalize-VIDEOTEST_PATHNAME(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;


    local _programme="inode-normalize";

    titre4 'VIDEOTEST_PATHNAME':
    #eval-echo "${_programme}-init";
    
    eval-echo "$_programme $VIDEOTEST_PATHNAME";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


addLibrairie 'test-fct-inode-normalize-VIDEOTEST_PATHNAME'
function      test-fct-inode-normalize-VIDEOTEST_PATHNAME(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;


    local _programme="inode-normalize";

    titre4 'VIDEOTEST_PATHNAME':
    #eval-echo "${_programme}-init";
    
    eval-echo "$_programme $VIDEOTEST_PATHNAME";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear;gtt.sh  apps/gtt/sid/tests/core-inode-normalize/ test-fct-inode-normalize-VIDEOTEST_REP
addLibrairie 'test-fct-inode-normalize-VIDEOTEST_REP'
function      test-fct-inode-normalize-VIDEOTEST_REP(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;


    local _programme="inode-normalize";

    titre4 'VIDEOTEST_REP':
    #eval-echo "${_programme}-init";
    
    eval-echo "$_programme $VIDEOTEST_REP/";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}



# clear;gtt.sh  apps/gtt/sid/tests/core-inode-normalize/ test-fct-inode-normalize-all-VIDEOTEST_REP
addLibrairie 'test-fct-inode-normalize-all-VIDEOTEST_REP'
function      test-fct-inode-normalize-all-VIDEOTEST_REP(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;


    local _programme="inode-normalize";

    titre4 'VIDEOTEST_REP':
    #eval-echo "${_programme}-init";
    
    eval-echo "${_programme}-all /media/mediatheques/Films";
    erreur-no-description-echo;
    ${_programme}-display-vars;

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


return 0;
