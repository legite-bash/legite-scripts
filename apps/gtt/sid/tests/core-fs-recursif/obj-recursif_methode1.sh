echo "${BASH_SOURCE[0]}";

# clear;gtt.sh --show-libsLevel2 --libExecAuto apps/gtt/sid/tests/fs/ obj-recursif_methode1
addLibrairie 'obj-recursif_methode1';
function      obj-recursif_methode1(){

    titre1 'objet: recursif_methode1';
    libExecAuto 'test-obj-recursif_methode1-void';
    libExecAuto 'test-obj-recursif_methode1-defaut';
    libExecAuto 'test-obj-recursif_methode1-surcharge';
    libExecAuto 'test-obj-recursif_methode1-inodeOnly';

    return 0;
}


# clear;gtt.sh apps/gtt/sid/tests/fs/  test-obj-recursif_methode1-void
addLibrairie 'test-obj-recursif_methode1-void' "Callbacks: void'";
function      test-obj-recursif_methode1-void(){

    titre2 'objet: recursif_methode1';

    titre3 "Initialisation de l'objet";
    eval-echo "recursif_methode1-init";

    titre3 "initialisation des callbacks";
    eval-echo "isRecursif_methode1_fctBefore=false";
    eval-echo "  recursif_methode1_fctBefore='void'";

    eval-echo "  recursif_methode1_fct='void';";    

    eval-echo "isRecursif_methode1_fctAfter=false";
    eval-echo "  recursif_methode1_fctAfter='void'";

    recursif_methode1 "/media/mediatheques";

    recursif_methode1-displayVars;

    return 0;
}


# clear;gtt.sh apps/gtt/sid/tests/fs/  test-obj-recursif_methode1-defaut
addLibrairie 'test-obj-recursif_methode1-defaut'
function      test-obj-recursif_methode1-defaut(){

    titre2 'objet: recursif_methode1';

    titre3 "Initialisation de l'objet";
    eval-echo "recursif_methode1-init";

    titre3 "Initialisation des callbacks";
    echo 'Aucune Initialisation-> callback par défaut';

    recursif_methode1 "/media/mediatheques";


    recursif_methode1-displayVars;

    return 0;
}

# clear;gtt.sh apps/gtt/sid/tests/fs/  test-obj-recursif_methode1
addLibrairie 'test-obj-recursif_methode1' "Précise le callback 'recursif_methode1_fct'";
function      test-obj-recursif_methode1(){

    function _fct-before(){ titre4 "$1";}
    function _fct-after(){  echo \"AFTER:$1\";}
    function _fct(){  echo "$1";}                 # Affiche l'inode courant de la boucle

    titre2 'objet: recursif_methode1';

    titre3 "Initialisation de l'objet";
    eval-echo "recursif_methode1-init";

    titre3 "initialisation des callbacks";
    eval-echo "isRecursif_methode1_fctBefore=true";
    eval-echo "  recursif_methode1_fctBefore='_fct-before'";

    eval-echo "  recursif_methode1_fct='_fct';";    

    eval-echo "isRecursif_methode1_fctAfter=true";
    eval-echo "  recursif_methode1_fctAfter='_fct-after'";

    recursif_methode1 "/media/mediatheques";

    recursif_methode1-displayVars;

    return 0;
}

# clear;gtt.sh apps/gtt/sid/tests/fs/  test-obj-recursif_methode1
addLibrairie 'test-obj-recursif_methode1-inodeOnly' "Affiche uniquement le nom de l'inode";
function      test-obj-recursif_methode1-inodeOnly(){

    #function _fct-before(){ titre4 "$1";}
    function _fct(){  echo "$1";}                 # Affiche l'inode courant de la boucle
    #function _fct-after(){  echo \"AFTER:$1\";}

    titre2 'objet: recursif_methode1';

    titre3 "Initialisation de l'objet";
    eval-echo "recursif_methode1-init";

    titre3 "initialisation des callbacks";
    eval-echo "isRecursif_methode1_fctBefore=false";
    eval-echo "  recursif_methode1_fctBefore='void";

    eval-echo "  recursif_methode1_fct='_fct';";    

    eval-echo "isRecursif_methode1_fctAfter=false";
    eval-echo "  recursif_methode1_fctAfter='void'";

    recursif_methode1 "/media/mediatheques";

    recursif_methode1-displayVars;

    return 0;
}
