#saveDebugGTTLevel $DEBUG_GTT_LEVEL_DEFAUT;
#saveDebugLevel $DEBUG_LEVEL_DEFAUT;
echo "${BASH_SOURCE[0]}";
#display-vars 'debugLevel' "$debugLevel"
local _source_fullRep="${BASH_SOURCE%/*.*}";
local _source_lastRep="${_source_fullRep##*/}";
local _source_fullname="${BASH_SOURCE##*/}";
local _source_name="${_source_fullname%.*}";
local _source_ext="${_source_fullname##*.}";


#titre1 "$_source_fullname\nModèle de récursivité";

addLibrairie 'test-recursivite_methode1' "";
function      test-recursivite_methode1(){ 

    titre1 "Test sur un fichier"
      recursivite_methode1_fct='ls -1 ';
    recursivite_methode1 "${BASH_SOURCE[0]}";

    titre1 "recursivite_methode1_levelMax=$RECUR_NONE: récursivité désactivé"
    recursivite_methode1_levelMax=$RECUR_NONE;      display-vars 'recursivite_methode1_levelMax' "$recursivite_methode1_levelMax";
    isRecursivite_methode1_fctBefore=true;          display-vars 'isRecursivite_methode1_fctBefore' "$isRecursivite_methode1_fctBefore";
      recursivite_methode1_fctBefore='void';
    isRecursivite_methode1_fctAfter=true;           display-vars 'isRecursivite_methode1_fctAfter' "$isRecursivite_methode1_fctAfter";
      recursivite_methode1_fctBefore='void';
    #recursivite_methode1 "$tmp_rep";

    titre1 "recursivite_methode1_levelMax=$RECUR_CURRENT: repertoire courant uniquement"
    recursivite_methode1_levelMax=$RECUR_CURRENT;   display-vars 'recursivite_methode1_levelMax' "$recursivite_methode1_levelMax";
    isRecursivite_methode1_fctBefore=true;          display-vars 'isRecursivite_methode1_fctBefore' "$isRecursivite_methode1_fctBefore";
    recursivite_methode1_fctBefore='void';
    isRecursivite_methode1_fctAfter=true;           display-vars 'isRecursivite_methode1_fctAfter' "$isRecursivite_methode1_fctAfter";
    recursivite_methode1_fctBefore='void';
    #recursivite_methode1 "$tmp_rep";


    titre1 "recursivite_methode1_levelMax=1"
    recursivite_methode1_levelMax=1;                display-vars 'recursivite_methode1_levelMax' "$recursivite_methode1_levelMax":
    isRecursivite_methode1_fctBefore=true;          display-vars 'isRecursivite_methode1_fctBefore' "$isRecursivite_methode1_fctBefore";
    recursivite_methode1_fctBefore='void';
    isRecursivite_methode1_fctAfter=true;           display-vars 'isRecursivite_methode1_fctAfter' "$isRecursivite_methode1_fctAfter";
    recursivite_methode1_fctBefore='void';
    #recursivite_methode1 "$tmp_rep";

    titre1 "recursivite_methode1_levelMax=2"
    recursivite_methode1_levelMax=2;                display-vars 'recursivite_methode1_levelMax' "$recursivite_methode1_levelMax":
    isRecursivite_methode1_fctBefore=true;          display-vars 'isRecursivite_methode1_fctBefore' "$isRecursivite_methode1_fctBefore";
    recursivite_methode1_fctBefore='void';
    isRecursivite_methode1_fctAfter=true;           display-vars 'isRecursivite_methode1_fctAfter' "$isRecursivite_methode1_fctAfter";
    recursivite_methode1_fctBefore='void';
    #recursivite_methode1 "$tmp_rep";

    titre1 "recursivite_methode1_levelMax=$RECUR_NO_LIMIT"
    recursivite_methode1_levelMax=$RECUR_NO_LIMIT;  display-vars 'recursivite_methode1_levelMax' "$recursivite_methode1_levelMax":
    isRecursivite_methode1_fctBefore=true;          display-vars 'isRecursivite_methode1_fctBefore' "$isRecursivite_methode1_fctBefore";
    recursivite_methode1_fctBefore='void';
    isRecursivite_methode1_fctAfter=true;           display-vars 'isRecursivite_methode1_fctAfter' "$isRecursivite_methode1_fctAfter";
    recursivite_methode1_fctBefore='void';
    #recursivite_methode1 "$tmp_rep";


    titre1 "recursivite_methode1_levelMax=$RECUR_NO_LIMIT ls"
    recursivite_methode1_levelMax=$RECUR_NO_LIMIT;  display-vars 'recursivite_methode1_levelMax' "$recursivite_methode1_levelMax":
    isRecursivite_methode1_fctBefore=true;          display-vars 'isRecursivite_methode1_fctBefore' "$isRecursivite_methode1_fctBefore";
      recursivite_methode1_fctBefore='void';
      recursivite_methode1_fct='ls -1 ';
    isRecursivite_methode1_fctAfter=true;           display-vars 'isRecursivite_methode1_fctAfter' "$isRecursivite_methode1_fctAfter";
      recursivite_methode1_fctBefore='void';
    recursivite_methode1 "$tmp_rep/Series/StarWars/StarWars-The_Mandalorian";
    return 0;
}


addLibrairie 'test-recursivite_find1' "";
function      test-recursivite_find1(){ 

    titre1 "Test sur un fichier";
    recursivite_find1_init;
      recursivite_find1_fct='ls -1 ';
    #recursivite_find1 "${BASH_SOURCE[0]}";
    #display-vars 'recursivite_find1_fichierNb' "$recursivite_find1_fichierNb";
    #display-vars 'recursivite_find1_repNb' "$recursivite_find1_repNb";


    titre1 "recursivite_find1_levelMax=$RECUR_NONE: récursivité désactivé";
    recursivite_find1_init;
    recursivite_find1_levelMax=$RECUR_NONE;         display-vars 'recursivite_find1_levelMax' "$recursivite_find1_levelMax";
      recursivite_find1_fct='ls -1 ';
    #recursivite_find1 "$tmp_rep";
    #display-vars 'recursivite_find1_fichierNb' "$recursivite_find1_fichierNb";
    #display-vars 'recursivite_find1_repNb' "$recursivite_find1_repNb";


    titre1 "recursivite_find1_levelMax=$RECUR_NO_LIMIT : récursivité sans limite (NE FONCTIONNE PAS!)";
    recursivite_find1_init;
    recursivite_find1_levelMax=$RECUR_NO_LIMIT;     display-vars 'recursivite_find1_levelMax' "$recursivite_find1_levelMax";
      recursivite_find1_fct='ls -1 ';   # $1=_fichier_nom
    isRecursivite_find1_fctBefore=false;            display-vars 'isRecursivite_find1_fctBefore' "$isRecursivite_find1_fctBefore";
      recursivite_find1_fctBefore='void';
    isRecursivite_find1_fctAfter=false;             display-vars 'isRecursivite_find1_fctAfter' "$isRecursivite_find1_fctAfter";
      recursivite_find1_fctAfter='void';
    recursivite_find1 "$tmp_rep";
    display-vars 'recursivite_find1_fichierNb' "$recursivite_find1_fichierNb";
    display-vars 'recursivite_find1_repNb'     "$recursivite_find1_repNb";

    return 0;
}


#######
# END #
#######
#echo "${BASH_SOURCE[0]}:END";
#restaureDebugGTTLevel;
#restaureDebugLevel;
return 0;