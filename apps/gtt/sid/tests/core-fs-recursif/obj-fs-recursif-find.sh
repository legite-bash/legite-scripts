echo "${BASH_SOURCE[0]}";

# clear;gtt.sh --show-libsLevel2--libExecAuto apps/gtt/sid/tests/core-fs-recursif/ test-obj-fs-recursif-find
addLibrairie 'test-obj-fs-recursif-find';
function      test-obj-fs-recursif-find(){

    libExecAuto 'test-fct-fs-recursif-find-void';
    libExecAuto 'test-fct-fs-recursif-find-defaut';
    libExecAuto 'test-fct-fs-recursif-find-custom-1';
    
    return 0;
}


# clear;gtt.sh apps/gtt/sid/tests/core-fs-recursif/ test-fct-fs-recursif-find-void
addLibrairie 'test-fct-fs-recursif-find-void' "Callbacks: void'";
function      test-fct-fs-recursif-find-void(){

    titre1 "${FUNCNAME[0]}()";
    local _programme='fs-recursif-find';

    titre3 "Initialisation de l'objet";
    eval-echo "${_programme}-init";
    erreur-no-description-echo;

    titre3 "Initialisation des callbacks";
    eval-echo "fs-recursif-find-callback-rep-in-enable false";
    eval-echo "fs-recursif-find-callback-rep-out-enable false";

    titre3 "Execution";
    eval-echo "$_programme  \"/media/mediatheques/Series/\"";
    erreur-no-description-echo;

    titre3 "display-vars";
    fs-recursif-find-display-vars;

    return 0;
}


# clear;gtt.sh apps/gtt/sid/tests/core-fs-recursif/ test-fct-fs-recursif-find-fichier-VIDEOTEST_PATHNAME
addLibrairie 'test-fct-fs-recursif-find-fichier-VIDEOTEST_PATHNAME' "test avec le fichier \$VIDEOTEST_PATHNAME'";
function      test-fct-fs-recursif-find-fichier-VIDEOTEST_PATHNAME(){

    titre1 "${FUNCNAME[0]}()";
    local _programme='fs-recursif-find';

    titre3 "Initialisation de l'objet";
    eval-echo "${_programme}-init";

    titre3 "Execution";
    eval-echo "$_programme  \"$VIDEOTEST_PATHNAME\"";
    erreur-no-description-echo;

    titre3 "display-vars";
    fs-recursif-find-display-vars;

    return 0;
}

# clear;gtt.sh apps/gtt/sid/tests/core-fs-recursif/ test-fct-fs-recursif-find-defaut
addLibrairie 'test-fct-fs-recursif-find-defaut'
function      test-fct-fs-recursif-find-defaut(){

    titre1 "${FUNCNAME[0]}()";
    local _programme='fs-recursif-find';

    titre3 "Initialisation de l'objet";
    eval-echo "${_programme}-init";
    erreur-no-description-echo;

    titre3 "Execution";
    eval-echo "$_programme  \"/media/mediatheques/Series/\"";
    erreur-no-description-echo;

    titre3 "display-vars";
    fs-recursif-find-display-vars;

    return 0;
}


# clear;gtt.sh apps/gtt/sid/tests/core-fs-recursif/ test-fct-fs-recursif-find-custom-1
addLibrairie 'test-fct-fs-recursif-find-custom-1' "Précise les callbacks";
function      test-fct-fs-recursif-find-custom-1(){

    function callback-rep-in(){  echo "${BLUE}rep-in:  ${1:-""}${WHITE}";}
    function callback-fichier(){ echo "FICHIER: ${1:-""}";}                 # Affiche l'inode courant de la boucle
    function callback-rep-out(){ echo "${GREEN}rep-out: ${1:-""}${WHITE}";}

    titre1 "${FUNCNAME[0]}()";
    local _programme='fs-recursif-find';

    titre3 "Initialisation de l'objet";
    eval-echo "${_programme}-init";

    titre3 "initialisation des callbacks";
    #eval-echo "fs-recursif-find-callback-rep-in-enable true";
    eval-echo "fs-recursif-find-callback-rep-in  \"callback-rep-in\"";
    eval-echo "fs-recursif-find-callback-fichier \"callback-fichier\";";    
    #eval-echo "fs-recursif-find-callback-rep-out-enable true";
    eval-echo "fs-recursif-find-callback-rep-out \"callback-rep-out\"";

    # - Execution - #
    eval-echo "$_programme  \"/media/mediatheques/Series\"";
    erreur-no-description-echo;

    fs-recursif-find-display-vars;

    return 0;
}


# clear;gtt.sh apps/gtt/sid/tests/core-fs-recursif/ test-fct-fs-recursif-find-level-max-set--1
addLibrairie 'test-fct-fs-recursif-find-level-max-set--1' "Test la variable level_max=-1";
function      test-fct-fs-recursif-find-level-max-set--1(){

    titre1 "${FUNCNAME[0]}()";
    local _programme='fs-recursif-find';

    # - Execution - #
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-level-max-set -1";
    eval-echo "$_programme  \"/media/mediatheques/\"";
    erreur-no-description-echo;

    fs-recursif-find-display-vars;

    return 0;
}


# clear;gtt.sh apps/gtt/sid/tests/core-fs-recursif/ test-fct-fs-recursif-find-level-max-set-1
addLibrairie 'test-fct-fs-recursif-find-level-max-set-1' "Test la variable level_max=1";
function      test-fct-fs-recursif-find-level-max-set-1(){

    titre1 "${FUNCNAME[0]}()";
    local _programme='fs-recursif-find';

    # - Execution - #
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-level-max-set 1";
    eval-echo "$_programme  \"/media/mediatheques/\"";
    erreur-no-description-echo;

    fs-recursif-find-display-vars;

    return 0;
}


# clear;gtt.sh apps/gtt/sid/tests/core-fs-recursif/ test-fct-fs-recursif-find-level-max-set-2
addLibrairie 'test-fct-fs-recursif-find-level-max-set-2' "Test la variable level_max=2";
function      test-fct-fs-recursif-find-level-max-set-2(){

    titre1 "${FUNCNAME[0]}()";
    local _programme='fs-recursif-find';

    # - Execution - #
    eval-echo "${_programme}-init";
    eval-echo "${_programme}-level-max-set 2";
    eval-echo "$_programme  \"/media/mediatheques/\"";
    erreur-no-description-echo;

    fs-recursif-find-display-vars;

    return 0;
}


return 0;
