#!/bin/bash

echo "$BASH_SOURCE: JE SUIS INCLUT":
declare -g var_globale="Je suis une variabla globale from $BASH_SOURCE";
declare -p var_globale;

declare -gi int_globale=1001;
declare -p  int_globale;

declare -gA tableau_A_globale=( ['_je']="je"  ['_viens']="viens" ['_de']="de" ['BASH_SOURCE']="$BASH_SOURCE" );
declare -p  tableau_A_globale;

declare -ga tableau_a_globale=( "je" "viens" "de" "$BASH_SOURCE" );
declare -p  tableau_a_globale;
