echo-d "${BASH_SOURCE[0]}:BEGIN";

# gtt.sh --show-libsLevel2 apps/ffmpeg/tests/test-fct-ffmpeg.sh lib
# date de création    : ?
# date de modification: 2024.08.10
# Description: Install une app dans gtt.sh


debug-level-save $DEBUG_LEVEL_DEFAUT;

# app-install


local _source_fullRep="${BASH_SOURCE%/*.*}";
local _source_lastRep="${_source_fullRep##*/}";
local _source_fullname="${BASH_SOURCE##*/}";
local _source_name="${_source_fullname%.*}";
local _source_ext="${_source_fullname##*.}";

display-vars '_source_fullRep' "$_source_fullRep";
display-vars '_source_lastRep' "$_source_lastRep";
display-vars '_source_fullname' "$_source_fullname";
display-vars '_source_name' "$_source_name";
display-vars '_source_ext' "$_source_ext";

# test #
# clear;gtt.sh install/app - src="http://updates.legite.org/hplip/hplip-3.21.8.run";
# clear;gtt.sh install/app - src="http://updates.legite.org/add_ln/add_ln.sh";

function install-app-usage() {
    erreurs-pile-add-echo "gtt.sh install/app - src='http://|repertoire|fichier_compressé|fichier' "
    return 0;
}

#############
# -  MAIN - #
#############

local _appDwPath="${PSPParams['src']:-""}";
local _appDwFullname="";    # nom.ext de l'application a telecharger
local _appDwName="";        # nom de l'application a telecharger
local _appDwExt="";         # extention du fichier (compressé ou non) à télécharger

echo '';
display-vars '_appDwPath' "$_appDwPath";
display-vars '_appDwFullname' "$_appDwFullname";
display-vars '_appDwName' "$_appDwName";
display-vars '_appDwExt' "$_appDwExt";

if [ -z "$_appDwPath" ]
then
    install-app-usage;
    return $E_FALSE;
fi


##############################
titre2_d '# TELECHARGEMENT #';
##############################
    # est-ce un protocole http?
    # format http[s]://site/rep/application.ext
    if [ "${_appDwPath:0:4}" = 'http' ]
    then
        if ! tmp-cd
        then
            return $E_INODE_NOT_EXIST;
        fi

        _appDwFullname="${_appDwPath##*/}";
        _appDwName="${_appDwFullname%.*}";
        _appDwExt="${_appDwFullname##*.}";

        if [ -f "$tmp_rep/$_appDwFullname" ]
        then
            echo-d "Le fichier '$tmp_rep/$_appDwFullname' est déjà téléchargé.";
            _appDwPath="$tmp_rep/$_appDwFullname"; # le fichier (compréssé) téléchargé devient le chemin a décompressé
        else

            echo "wget -O \"$tmp_rep/$_appDwFullname\" \"$_appDwPath\"";
            if wget -O "$tmp_rep/$_appDwFullname" "$_appDwPath";
            then
                _appDwPath="$tmp_rep/$_appDwFullname"; # le fichier compréssé téléchargé devient le chemin a décompressé
                echo "Téléchargement réussi";
            else
                erreurs-pile-add-echo "$_source_lastRep(): Erreur lors du téléchargement de '$_appDwPath'";
                return $E_FALSE;
            fi
        fi
    fi
#


#############################
titre2_d '# DECOMPRESSION #';
#############################
    display-vars '_appDwPath' "$_appDwPath";
    display-vars '_appDwFullname' "$_appDwFullname";
    display-vars '_appDwName' "$_appDwName";
    display-vars '_appDwExt' "$_appDwExt";


    if [ -f "$_appDwPath" ]
    then
        case "$_appDwExt" in
            'zip')
                echo-d "decompression zip dans le repertoire";
                ;;
            'tar')
                echo-d "decompression tar";
                ;;
            'gzip')
                echo-d "decompression gzip";
                ;;
            *)
                echo-d " Le fichier $_appDwPath n'est pas un fichier compréssé.";
        esac
    fi
#


############################
titre2_d '# FICHIER .run #';
############################
    if [ "$_appDwExt" = 'run' ]
    then
        echo-d 'Fichier de type .run -> chmod +x';
        chmod +x "$_appDwPath";

        titre3 'Fichier de type .run -> execution';
        echo -n "Executer le script '$_appDwFullname' (y/N)";
        read;
        if [ "${REPLY^^}" = 'Y' ]
        then
            echo '';
            "$_appDwPath";        # execution du script
        else
            echo 'Abandon.';
        fi
        return 0;
    else
        echo-d "Le fichier '$_appDwFullname' n'est pas de type .run";
    fi
#


#########################################
titre2_d '# REPERTOIRE DE DESTINATION #';
#########################################
local _appRepInstall="$APPS_REP/$_appDwName";
display-vars '_appRepInstall' "$_appRepInstall";

if [ ! -d "$_appRepInstall" ]
then
    echo-d "Création du repertoire: $_appRepInstall";
    mkdir -p "$_appRepInstall" && cd "$_appRepInstall";
    if [ $? -ne 0 ]
    then
        erreurs-pile-add-echo "Erreur lors de la création du repertoire d'installation: $_appRepInstall";
        return $E_INODE_NOT_EXIST;
    fi
fi

#display-vars 'PWD' '$PWD';


################################################
titre2_d '# Installation du repertoire/fichier #';
################################################
    if [ -d "$_appDwPath" ]
    then
        echo-d "'$_appDwPath' est un repertoire.";
        echo-d "cp -R \"$_appDwPath\" \"$_appRepInstall/$_appDwFullname\"";
        cp -R "$_appDwPath" "$_appRepInstall/$_appDwFullname";


    elif [ -f "$_appDwPath" ]
    then
        echo-d "'$_appDwPath' est un fichier.";
        echo-d 'Fichier chmod +x';
        chmod +x "$_appDwPath";

        titre3 "Copie vers: $_appRepInstall";
        eval-echo "mv   \"$_appDwPath\" \"$_appRepInstall/$_appDwFullname\"";
    fi
#


#######
# END #
#######
echo-d "${BASH_SOURCE[0]}:END";
restaureDebugGTTLevel;
debug-app-level-restaure;
return 0;