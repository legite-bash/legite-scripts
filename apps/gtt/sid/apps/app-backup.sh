echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh --show-libsLevel2 apps/ffmpeg/tests/test-fct-ffmpeg.sh lib
# date de création    : 2024.08.04
# date de modification: 2024.08.04
# Description: 


# clear; gtt.sh apps/gtt/sid/apps/app-backup/ backup-sidToVersion
addLibrairie 'backup-sidToVersion' "backup-sidToVersion ['app_nom'='sid']";
function      backup-sidToVersion(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    local _appNom="${1:-"gtt"}";
    titreInfo "Copie le repertoire sid vers la version actuelle"
    if cp -R "$APPS_REP/$_appNom/sid" "$APPS_REP/$_appNom/$GTT_VERSION"
    then
        notifs-pile-add-echo "L'application '$_appNom' a bien été sauvée vers '$APPS_REP/$_appNom/$GTT_VERSION'";
    else
        erreurs-pile-add-echo "Erreur lors de la sauvegarde de l'application '$_appNom' vers '$APPS_REP/$_appNom/$GTT_VERSION'";
    fi

    fct-pile-app-out $E_TRUE $_fct_pile_app_level;return $E_TRUE;
}


# clear; gtt.sh apps/gtt/sid/apps/app-backup/ backup-sidToToday
addLibrairie 'backup-sidToToday' "backup-sidToToday ['app_nom'='sid'] # Copie le repertoire sid avec la date";
function      backup-sidToToday(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    local _appNom="${1:-"gtt"}";
    titreInfo "Copie le repertoire sid avec la date"
    local _date="$(date +%Y.%m.%d)";
    if cp -R "$APPS_REP/$_appNom/sid" "$APPS_REP/$_appNom/$_date"
    then notifs-pile-add-echo "L'application '$_appNom' a bien été sauvée vers '$APPS_REP/$_appNom/$_date'";
    else erreurs-pile-add-echo "Erreur lors de la sauvegarde de l'application '$_appNom' vers '$APPS_REP/$_appNom/$_date'";
    fi

    fct-pile-app-out $E_TRUE $_fct_pile_app_level;return $E_TRUE;
}


# clear; gtt.sh apps/gtt/sid/apps/app-backup/ backup-sidToTH
addLibrairie 'backup-sidToTH' "backup-sidToTH ['app_nom'='sid'] # Copie le repertoire sid avec la date et heure";
function      backup-sidToTH(){
    local -i _fct_pile_app_level=2;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    local _appNom="${1:-"gtt"}";
    titreInfo "Copie le repertoire sid avec la date et heure"
    local _date="$(date +%Y.%m.%d-%H.%M)";
    cp -R "$APPS_REP/$_appNom/sid" "$APPS_REP/$_appNom/$_date";
    if cp -R "$APPS_REP/$_appNom/sid" "$APPS_REP/$_appNom/$_date"
    then
        notifs-pile-add-echo "L'application '$_appNom' a bien été sauvée vers '$APPS_REP/$_appNom/$_date'";
    else
        erreurs-pile-add-echo "Erreur lors de la sauvegarde de l'application '$_appNom' vers '$APPS_REP/$_appNom/$_date'";
    fi

    fct-pile-app-out $E_TRUE $_fct_pile_app_level;return $E_TRUE;
}