echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


############
# * cron * #
############
# clear; gtt.sh -d -R -v apps/linux/cron cron-edit
addLibrairie 'cron-edit' 'Modifie un fichier et l applique avec crontab (et sauvegarde ce fichier avec horodatage)'
function      cron-edit() {
    titre1 "Edit un fichier cron et l'utilise pour initialiser avec crontab";

    local _cronRep="$CONF_ROOT/cron";
    titre4 "Création du répertorie '$_cronRep'.";
    if [ ! -d "$_cronRep" ]
    then
        if ! eval-echo "mkdir $_cronRep"
        then
            erreurs-pile-add-echo "Impossible de créer le répertoire '$_cronRep'";
            return $E_INODE_NOT_EXIST;
        fi
    fi


    local _dt=$(date +%Y.%d-%H.%M);
    local _cronOri="$_cronRep/cron.ori";
    local _lastPath="$_cronRep/cron.last"
    local _archivePath="$_cronRep/$_dt.cron"

    #display-vars '_cronOri' "$_cronOri"
    #display-vars '_archivePath' "$_archivePath"
    #display-vars '_lastPath' "$_lastPath"

    # si pas d'archive (last) -> creer le _cronOri vide et le last
    if [ ! -f "$_lastPath" ]
    then
        titre4 "Création des fichiers initiaux.";
        touch "$_cronOri";
        chmod +wx "$_cronOri";
        cp "$_cronOri" "$_lastPath"
    fi


    titre4 "Editer la derniere version";
    eval-echo "nano $_lastPath"
    local -i _errReturn=$?;
    #display-vars '_errReturn' "$_errReturn"
    if [ $_errReturn -ne 0 ]
    then
        erreurs-pile-add-echo "Erreur lors de 'nano $_lastPath'"
        return $E_INODE_NOT_EXIST;
    fi

    titre4 "Appliquer la derniere edition comme fichier de cron";
    eval-echo "crontab \"$_lastPath\""
    _errReturn=$?
    if [ $_errReturn -ne 0 ]
    then
        erreurs-pile-add-echo "Erreur lors de 'crontab $_lastPath': $_errReturn"
        return $E_INODE_NOT_EXIST;
    else
        titre4 "Archiver la derniere version";
        eval-echo "cp \"$_lastPath\" \"$_archivePath\""
    fi
    return 0;
}
