echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


############
# * WIFI * #
############

    addLibrairie 'cfg_wifi' 'Methode pour configurer le wifi en ligne de commande' ''
    function      cfg_wifi(){
        titre3 "Methode pour configurer le wifi en ligne de commande"
        if [ $IS_ROOT = false ];then titreWarn "$FUNCNAME doit être lancé en root!"fctOut "$FUNCNAME($E_NOT_ROOT)"; return $E_NOT_ROOT; fi
        evalCmd "aptitude -y install wpasupplicant"
        echo "
        vi /etc/network/interfaces

        auto wlan0
        iface wlan0 inet static
            wpa-ssid <mon ssid>
            wpa-psk <ma clé wifi>
            address <adresse IP de la machine>
            netmask <masque de sous-réseau>
            gateway <IP de la passerelle>
            dns-nameservers <IP de la passerelle>
    "
        return 0;
    }

    addLibrairie 'cfg_wifi-legite' 'Methode pour configurer le wifi en ligne de commande' ''
    function      cfg_wifi-legite(){
        titre3 "Methode pour configurer le wifi en ligne de commande"
        if [ $IS_ROOT = false ];then titreWarn "$FUNCNAME doit être lancé en root!"fctOut "$FUNCNAME($E_NOT_ROOT)"; return $E_NOT_ROOT; fi
        evalCmd "aptitude -y install wpasupplicant"
        echo "
        #https://debian-facile.org/doc:reseau:interfaces:wifi

        # monitoring
        iwlist scan | grep ESSID
        iwmon (depend de  nlman)


        vim /etc/network/interfaces
        auto wlan0
        iface wifi-maison inet dhcp
            wpa-ssid 'NomDuReseau'
            wpa-psk  'CleDeChiffrement'

        #auto wlan0
        #iface wlan0 inet static
        #    wpa-ssid <mon ssid>
        #    wpa-psk <ma clé wifi>
        #    address <adresse IP de la machine>
        #    netmask <masque de sous-réseau>
        #    gateway <IP de la passerelle>
        #    dns-nameservers <IP de la passerelle>
    
         redemarrer l'interface reseau
         ifconfig wlan0 down
         ou, pour buster
         ip link set wlan0 down

        ifup wlp3s0=wifi-maison
        " # echo end
        return 0;
    }