echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


#local _source_fullRep="${BASH_SOURCE%/*.*}";
#local _source_lastRep="${_source_fullRep##*/}";
#local _source_fullname="${BASH_SOURCE##*/}";
#local _source_name="${_source_fullname%.*}";
#local _source_ext="${_source_fullname##*.}";

#display-vars '_source_fullRep' "$_source_fullRep";
#display-vars '_source_lastRep' "$_source_lastRep";
#display-vars '_source_fullname' "$_source_fullname";
#display-vars '_source_name' "$_source_name";
#display-vars '_source_ext' "$_source_ext";


###################
# -  REFERENCES - #
###################
# https://www.youtube.com/watch?v=Drk4-lUd0Dg


#############
# -  MAIN - #
#############
#titre1 "$_source_fullname";


# clear; gtt.sh -d apps/linux/disques/defrag defrag-auto - src='/dev/shm'
addLibrairie 'defrag-auto' "- src='/dev/shm' # Défragmente la partition de type ext4|xfs|btrfs";
function      defrag-auto(){
    local -i _pile_fct_app_level=2;
    fct-pile-app-in "$*" $_pile_fct_app_level;

    # Inutile de démonter le disque avant le défrag

    # - Controle des entrées - #
    local _src="${1:-""}";
    if [ "$_src" = "" ]
    then
        _src="${PSP_params['src']:-""}";
        if [ "$_src" = '' ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]} - src=/dev/shm";
            fct-pile-app-out $E_ARG_REQUIRE $_pile_fct_app_level; return $E_ARG_REQUIRE;
        fi
    fi
    display-vars '_src' "$_src";

    local _verbose="";
    if [ $debug_verbose_level -gt 0 ];then _verbose='-v';fi;


    # - determiner le type de la partition - #
    
    local _fstype="$(df --output='fstype' $_src)";
    _fstype="${_fstype#*e}";                    # supprimer le titre de la colonne
    _fstype="$(echo $_fstype | tr -d "\n" )";   # Supprimer le retour à la ligne
    #display-vars '_fstype' "$_fstype";

    case "$_fstype" in
        ext4)   defrag-ext4;     ;;
        xfs)    defrag-xfs;      ;;
        btrfs)  defrag-btrfs;    ;;
        *)
            erreurs-pile-add-echo "Le type $_fstype n'est pas pris en charge.";
            fct-pile-app-out $E_FALSE $_pile_fct_app_level; return $E_FALSE;
    esac

    fct-pile-app-out $E_TRUE $_pile_fct_app_level; return $E_TRUE;
}


# clear; gtt.sh -d apps/linux/disques/defrag defrag-ext4 - src='/dev/shm'
addLibrairie 'defrag-ext4' "- src='/dev/shm' # Défragmente la partition de type ext4 monté";
function      defrag-ext4(){
    local -i _pile_fct_app_level=2;
    fct-pile-app-in "$*" $_pile_fct_app_level;

    titreInfo 'e4defrag fait partie du paquet: e2fsprogs'

    # - Controle des entrées - #
    local _src="${1:-""}";
    if [ "$_src" = "" ]
    then
        _src="${PSP_params['src']:-""}";
        if [ "$_src" = '' ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]} - src=/dev/shm";
            fct-pile-app-out $E_ARG_REQUIRE $_pile_fct_app_level; return $E_ARG_REQUIRE;
        fi
    fi
    display-vars '_src' "$_src";

    local _verbose="";
    if [ $debug_verbose_level -gt 0 ];then _verbose='-v';fi;


    # La partition DOIT etre monté
    # programmer le trap pour intercepter lerreur command introuvable
    titre4 "Etat de la fragmentation avant"
    eval-echo "sudo e2freefrag $_src";

    titre4 "Defrag"
    titreInfo "e4defrag  $_verbose $_src";

    eval-echo "sudo e4defrag -c  $_verbose $_src";
    erreur-no-description-echo;

    if [ $erreur_no -ne 0 ]
    then 
        erreurs-pile-add-echo "La partition '$_src' n'est pas monté";
        fct-pile-app-out $E_FALSE $_pile_fct_app_level; return $E_FALSE;
    else
        titre4 "Etat de la fragmentation APRES"
        sudo  e2freefrag "$_src";
    fi

    fct-pile-app-out $E_TRUE $_pile_fct_app_level; return $E_TRUE;
}


# clear; gtt.sh -d apps/linux/disques/defrag defrag-xfs - src='/dev/shm'
addLibrairie 'defrag-xfs' "- src='/dev/shm' # Défragmente la partition de type xfs";
function      defrag-xfs(){
    local -i _pile_fct_app_level=2;
    fct-pile-app-in "$*" $_pile_fct_app_level;


    # - Controle des entrées - #
    local _src="${1:-""}";
    if [ "$_src" = "" ]
    then
        _src="${PSP_params['src']:-""}";
        if [ "$_src" = '' ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]} - src=/dev/shm";
            fct-pile-app-out $E_ARG_REQUIRE $_pile_fct_app_level; return $E_ARG_REQUIRE;
        fi
    fi
    display-vars '_src' "$_src";

    local _verbose="";
    if [ $debug_verbose_level -gt 0 ];then _verbose='-v';fi;


    # La partition DOIT etre monté
    titreInfo "xfs_fsr  $_verbose \"$_src\"";
    sudo xfs_fsr $_verbose "$_src";

    fct-pile-app-out $E_TRUE $_pile_fct_app_level; return $E_TRUE;
}


# clear; gtt.sh -d apps/linux/disques/defrag defrag-btrfs - src='/dev/shm'
addLibrairie 'defrag-btrfs' "Defragmente le repertoire ou le ficher de type btrfs (donné via - src='/dev/part'";
function      defrag-btrfs(){
    local -i _pile_fct_app_level=2;
    fct-pile-app-in "$*" $_pile_fct_app_level;

    # - Controle des entrées - #
    local _src="${1:-""}";
    if [ "$_src" = "" ]
    then
        _src="${PSP_params['src']:-""}";
        if [ "$_src" = '' ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]} - src=/dev/shm";
            fct-pile-app-out $E_ARG_REQUIRE $_pile_fct_app_level; return $E_ARG_REQUIRE;
        fi
    fi
    display-vars '_src' "$_src";

    local _verbose="";
    if [ $debug_verbose_level -gt 0 ];then _verbose='-v';fi;


    # Inutile de démonter le disque avant la défrag
    btrfs filesystem defragment $_verbose -r "$_src";

    fct-pile-app-out $E_TRUE $_pile_fct_app_level; return $E_TRUE;
}



#######
# END #
#######
echo-d "${BASH_SOURCE[0]}:END" 1;
return 0;