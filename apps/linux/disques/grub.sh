echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.08.10
# Description: 


addLibrairie 'grub-passwd' "Protege l'edition de ligne dans grub par mot de passe à grub.";
function      grub-passwd(){
    titre3 "${librairiesDescr["$FUNCNAME"]}"

    titre4 'Creation du mot de passe chiffré';
    titreCmd "grub2-mkpasswd-pbkdf2"
    echo 'Le mot de passe chiffré sera affiché à la suite de la commande.'
    grub2-mkpasswd-pbkdf2


    titre4 'Édition du fichier de configuration';
    titreInfo "Texte a ecrire dans /etc/grub.d/40_custom"
    echo 'set superuser="root";' #>> /etc/grub.d/40_custom
    echo 'set superuser="root";' #>> /etc/grub.d/40_custom

    eval-echo "vim /etc/grub.d/40_custom";
    echo "password_pbkdf2 root %Mot de passe fournis par la commande grub2-mkpasswd-pbkdf2%";


    titre4 'Rechargement du fichier de configuration';
    titreCmd "grub2-mkconfig -o /boot/grub2/grub.cfg"
    grub2-mkconfig -o /boot/grub2/grub.cfg

    return 0;
}
