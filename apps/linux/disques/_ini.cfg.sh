echo-d "${BASH_SOURCE[0]}" 1;

# gtt.sh 
# date de création    : ?
# date de modification: 2024.09.17
# Description: 


# clear; gtt.sh -d apps/linux/disques/ eraseDisque - src=/dev/shm
addLibrairie 'eraseDisque' "Nettoie le disque home avec des caracteres aléatoires sur tout l'espace libre. Ne detruit rien.";
function      eraseDisque(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;

    titre3 "${librairiesDescr["${FUNCNAME[0]}"]}";
    titreInfo 'Ne pas le faire sur un disque SSD car cela use prématurément les cellules'


    # - Controle des entrées - #
    local _src="${1:-""}";
    if [ "$_src" = "" ]
    then
        _src="${PSPParams['src']:-""}";
        if [ "$_src" = '' ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]} - src=/repertoire/a/remplir";
            fct-pile-app-out $E_ARG_REQUIRE $_fct_pile_app_level; return $E_ARG_REQUIRE; return $E_ARG_REQUIRE;
        fi
    fi

    titre4 "Remplissage de l'espace libre"
    time eval-echo "cat /dev/urandom > $_src/random.txt"


    titre4 'Suppression du fichier de remplissage';
    rm "$_src/random.txt";

    titre4 'Espace libre sur /home';
    df -h "$_src";

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}


# clear; gtt.sh -d -R -v apps/linux/disques/ ch-inode - src=/dev/shm [mod_file=744] [mod_dir=755] [owner_user=""] [owner_grp=""]
# clear; gtt.sh -d -R -v apps/linux/disques/ ch-inode - src=/dev/shm [mod_file=744] [mod_dir=755] [owner_user=""] [owner_grp=""]
addLibrairie 'ch-inode' 'Change récursivement,les répertoires en 755 et  les fichiers en 744';
function      ch-inode(){
    local -i _fct_pile_app_level=1;
    fct-pile-app-in "$*" $_fct_pile_app_level;


    # - Controle des entrées - #
    local _src="${1:-""}";      # obligatoire
    if [ "$_src" = '' ]
    then
        _src="${PSPParams['src']:-""}";
        if [ "$_src" = '' ]
        then
            erreurs-pile-add-echo "${FUNCNAME[0]} [-v] - src=''";
            fct-pile-app-out $E_ARG_REQUIRE $_fct_pile_app_level; return $E_ARG_REQUIRE;
        fi
    fi
    #display-vars '_src' "$_src";

    local _mod_file="${2:-""}";      # facultatif
    if [ "$_mod_file" = '' ]
    then
        _mod_file="${PSPParams['_mod_file']:-"744"}";
    fi

    local _mod_dir="${2:-""}";      # facultatif
    if [ "$_mod_dir" = '' ]
    then
        _mod_dir="${PSPParams['_mod_f_mod_dirile']:-"755"}";
    fi

    local _owner_user="${4:-""}";      # facultatif (vide pas défaut: pas de changement)
    local _owner_grp="${5:-""}";       # facultatif (vide pas défaut: pas de changement)


    # - execution - #
    local _recursif_param='';
    local _datas=$(ls "$_src" -1);
    for _inode in $_datas
    do
        if $isTrapCAsk;then break;fi

        #display-vars '_inode' "$_inode";
        local _inodePath="$_src/$_inode";
        #display-vars '_inodePath' "$_inodePath";
        

        # - Change le propriétaire - #
        #stat  --cached=always -c '%u:%g' "$_inodePath";
        #if [ "$(stat  -c '%u:%G' "$_inodePath" )" != '1000:www-data' ]
        #then
        #    if [ $debug_verbose_level -gt 0 ]
        #    then eval-echo "sudo chown 1000:www-data  \"$_inodePath\";";
        #    else           sudo chmod 1000:www-data   "$_inodePath";
        #    fi
        #fi

        
        if [ -d "$_inodePath" ] # - repertoire - #
        then
            if [ "$(stat  -c '%a' "$_inodePath" )" != "$_mod_dir" ]
            then
                if [ $debug_verbose_level -gt 0 ]
                then eval-echo "chmod  $_mod_dir \"$_inodePath\";";
                else           chmod  $_mod_dir "$_inodePath";
                fi
            fi
            if [ $recursifLevel -gt 0 ];then ${FUNCNAME[0]} "$_inodePath";fi

        elif [ -f "$_inodePath" ]   # - fichier - #
        then
            if [ "$(stat  -c '%a' "$_inodePath")" != "$_mod_file" ]
            then
                if [ $debug_verbose_level -gt 0 ]
                then
                    eval-echo "chmod $_mod_file \"$_inodePath\";";
                else          chmod $_mod_file  "$_inodePath";
                fi
            fi
        fi
    done

    fct-pile-app-out $E_TRUE $_fct_pile_app_level; return $E_TRUE;
}
