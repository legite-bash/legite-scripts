echo-d "${BASH_SOURCE[0]}" 1;

###################
# -  REFERENCES - #
###################
# 


#############
# -  MAIN - #
#############


# clear; sudo gtt.sh -d linux/disques/badblocks.sh  disques-badblocks - src=/dev/...
addLibrairie 'disques-badblocks' "- src=/dev/... # Lance badblock surla partition en utilisant le journal";
function      disques-badblocks(){
    local -i _pile_fct_app_level=2;
    fct-pile-app-in "$*" $_pile_fct_app_level;

    # Inutile de démonter le disque avant le défrag

    # - Controle des entrées - #
    local _src="${1:-""}";
    if [ "$_src" = "" ]
    then
        _src="${PSP_params['src']:-""}";
        #if [ "$_src" = '' ]
        #then
        #    erreurs-pile-add-echo "${FUNCNAME[0]} - src=/dev/shm";
        #    fct-pile-app-out $E_ARG_REQUIRE $_pile_fct_app_level; return $E_ARG_REQUIRE;
        #fi
    fi
    titre0 "${FUNCNAME[0]} $_src";

    local blkid_datas="$(sudo blkid | grep "$_src")"; 
    #display-vars 'blkid_datas' "$blkid_datas";

    if [ ! -e "$_src" ]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]} - périphérique $_src inexistant!";
        erreurs-pile-add-echo "${FUNCNAME[0]} uuid introuvable dans $_src.";
        titre4 "Liste des périphériques"
        echo "$COLOR_INFO$blkid_datas$COLOR_NORMAL";
        fct-pile-app-out $E_INODE_NOT_EXIST $_pile_fct_app_level; return $E_INODE_NOT_EXIST;
    fi

    local _verbose="";
    if [ $debug_verbose_level -gt 0 ];then _verbose='-v';fi;


    titre4 "Chercher l'uuid de la partition"
    local _uuid="$(sudo blkid | grep -v "loop" )"; # display-vars '_uuid' "$_uuid";
    _uuid="${blkid_datas#* UUID=\"}";               # display-vars '_uuid' "$_uuid";
    _uuid="${_uuid%%\"*}";                          # display-vars '_uuid' "$_uuid";

    if [ -z "_uuid" ]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]} uuid introuvable dans $_src.";
        fct-pile-app-out $E_FALSE $_pile_fct_app_level; return $E_FALSE;
    fi

    # - Préparation/vérification du repertoire des journaux- #
    local _badblocks_in_rep="/var/gtt/badblocks";
    if [ ! -d "$_badblocks_in_rep" ]; then  mkdir -p "$_badblocks_in_rep"; fi
    local _badblocks_in_pathname="$_badblocks_in_rep/$_uuid";
    touch "$_badblocks_in_pathname";

    local _badblocks_out_pathname="$_badblocks_in_rep/$_uuid-out";
    touch "$_badblocks_out_pathname";
    display-vars '_badblocks_in_pathname ' "$_badblocks_in_pathname";
    display-vars '_badblocks_out_pathname' "$_badblocks_out_pathname";


    # - badblocks - #
    titre4 "badblocks";
    eval-echo "badblocks -vvvv -n -s -i "$_badblocks_in_pathname" -o "$_badblocks_out_pathname" $_src";
    if [ $erreur_no -ne 0 ]
    then
        erreurs-pile-add-echo "${FUNCNAME[0]} Erreur avec la commande badblocks ($_src).";
        fct-pile-app-out $E_FALSE $_pile_fct_app_level; return $E_FALSE;
    fi

    # - badblocks: deplacement de out vers in - #
    mv "_badblocks_out_pathname" "$_badblocks_in_pathname";
    fct-pile-app-out $E_TRUE $_pile_fct_app_level; return $E_TRUE;
}