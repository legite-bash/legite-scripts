echo-d "$BASH_SOURCE";

#############
# * LINUX * #
#############

    # https://wiki.archlinux.org/title/Linux_console_(Fran%C3%A7ais)/Keyboard_configuration_(Fran%C3%A7ais)
    addLibrairie 'cfg_clavier' 'Configure le clavier en mode console'
    function      cfg_clavier(){
        titre3 'Configuration du clavier'
        if [ $IS_ROOT = false ];then titreWarn "$FUNCNAME doit être lancé en root!";fctOut "$FUNCNAME($E_NOT_ROOT)"; return $E_NOT_ROOT; fi
        evalCmd "dpkg-reconfigure console-setup"
        evalCmd "dpkg-reconfigure keyboard-configuration"
        evalCmd "loadkeys fr"
        return 0;
    }

    addLibrairie 'cfg_french_console' 'Configure la console en francais' ''
    function      cfg_french_console(){
        titre3 'M                                                                                                               &aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaùmise en francais de la console'
        if [ $IS_ROOT = false ];then titreWarn "$FUNCNAME doit être lancé en root!";fctOut "$FUNCNAME($E_NOT_ROOT)"; return $E_NOT_ROOT; fi
        evalCmd "aptitude -y install task-french"
        return 0;
    }

    addLibrairie 'cfg_french_desktop' 'Configure le bureau en francais' ''
    function      cfg_french_desktop(){
        titre3 "Mise en francais de l'environnement graphique"
        if [ $IS_ROOT = false ];then titreWarn "$FUNCNAME doit être lancé en root!";fctOut "$FUNCNAME($E_NOT_ROOT)"; return $E_NOT_ROOT; fi
        evalCmd "aptitude -y install task-french task-french-desktop"
        return 0;
    }
